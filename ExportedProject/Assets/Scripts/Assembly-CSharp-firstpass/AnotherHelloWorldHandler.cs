using System.Collections;
using HTTP;
using HTTP.Server;

public class AnotherHelloWorldHandler : HttpRequestHandler
{
	public override void GET(Request request)
	{
		Response response = request.response;
		Hashtable hashtable = new Hashtable();
		hashtable["boo"] = "ya!";
		response.Text = JsonSerializer.Encode(hashtable);
		response.headers.Set("Content-Type", "application/json");
	}
}
