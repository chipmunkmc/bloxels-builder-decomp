public enum DistanceUnit
{
	Pixels = 0,
	Inches = 1,
	Centimeters = 2
}
