using UnityEngine;

public class FingerDebug : MonoBehaviour
{
	public GUITexture FingerIcon;

	public bool ShowGUI;

	public Rect GuiRect = new Rect(5f, 5f, 500f, 500f);

	private GUITexture[] icons;

	private float distance = -1f;

	private void Start()
	{
		if (!FingerGestures.Instance)
		{
			Debug.LogError("FG instance not present");
			base.enabled = false;
			return;
		}
		icons = new GUITexture[FingerGestures.Instance.MaxFingers];
		for (int i = 0; i < icons.Length; i++)
		{
			GUITexture gUITexture = Object.Instantiate(FingerIcon);
			gUITexture.transform.parent = base.transform;
			gUITexture.enabled = false;
			icons[i] = gUITexture;
		}
		FingerIcon.enabled = false;
	}

	private void Update()
	{
		if ((bool)FingerGestures.Instance)
		{
			if (FingerGestures.Touches.Count >= 2)
			{
				distance = Vector2.Distance(FingerGestures.Touches[0].Position, FingerGestures.Touches[1].Position);
			}
			else
			{
				distance = -1f;
			}
			int i;
			for (i = 0; i < FingerGestures.Touches.Count; i++)
			{
				FingerGestures.Finger finger = FingerGestures.Touches[i];
				Rect pixelInset = icons[i].pixelInset;
				pixelInset.x = finger.Position.x - pixelInset.width / 2f;
				pixelInset.y = finger.Position.y - pixelInset.height / 2f;
				icons[i].pixelInset = pixelInset;
				icons[i].enabled = true;
			}
			for (; i < icons.Length; i++)
			{
				icons[i].enabled = false;
			}
		}
	}

	private void OnGUI()
	{
		if (!ShowGUI || !FingerGestures.Instance)
		{
			return;
		}
		GUILayout.BeginArea(GuiRect);
		GUILayout.BeginVertical();
		GUILayout.Label("Input.Touches: " + Input.touchCount);
		GUILayout.Label("FingerGestures: " + FingerGestures.Touches.Count);
		foreach (FingerGestures.Finger touch in FingerGestures.Touches)
		{
			GUILayout.Label(string.Format("{0} moving:{1}", touch, touch.IsMoving));
			foreach (GestureRecognizer gestureRecognizer in touch.GestureRecognizers)
			{
				GUILayout.Label(touch.ToString() + ": " + gestureRecognizer);
			}
		}
		if (distance >= 0f)
		{
			GUILayout.Label("Finger[0->1] Distance: " + distance.ToString("N0"));
		}
		GUILayout.Space(5f);
		GUILayout.Label("Clusters: " + FingerGestures.DefaultClusterManager.Clusters.Count + " [Pool: " + FingerGestures.DefaultClusterManager.GetClustersPool().Count + "]");
		foreach (FingerClusterManager.Cluster cluster in FingerGestures.DefaultClusterManager.Clusters)
		{
			GUILayout.Label("  -> Cluster #" + cluster.Id + ": " + cluster.Fingers.Count + " fingers");
		}
		GUILayout.EndVertical();
		GUILayout.EndArea();
	}
}
