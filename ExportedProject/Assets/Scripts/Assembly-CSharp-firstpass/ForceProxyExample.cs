using System;
using System.Collections;
using HTTP;
using UnityEngine;

public class ForceProxyExample : MonoBehaviour
{
	private void Start()
	{
		Request.proxy = new Uri("http://127.0.0.1:8888");
		StartCoroutine(FetchAssetBundle());
	}

	private void OnGUI()
	{
		if (GUILayout.Button("Restart"))
		{
			Start();
		}
	}

	private IEnumerator FetchAssetBundle()
	{
		Request r = new Request("GET", "http://differentmethods.com/~simon/uniwebtest.unity3d?xyzzy");
		yield return r.Send();
		if (r.exception == null)
		{
			Debug.Log(r.response.status);
			AssetBundleCreateRequest abcr = r.response.AssetBundleCreateRequest();
			yield return abcr;
			Debug.Log(abcr.assetBundle);
		}
		else
		{
			Debug.LogError(r.exception);
		}
	}
}
