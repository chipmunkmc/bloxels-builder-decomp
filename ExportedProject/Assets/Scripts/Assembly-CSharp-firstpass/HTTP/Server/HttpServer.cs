using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using UniExtensions.Async;
using UnityEngine;

namespace HTTP.Server
{
	public class HttpServer : MonoBehaviour
	{
		public int port = 8080;

		public bool startServerOnLoad = true;

		public bool logRequests = true;

		private Socket listener;

		private HttpRequestHandler[] routes;

		public void StartServing()
		{
			MagicThread.Start(ServeHTTP(), false);
		}

		private void Start()
		{
			Application.runInBackground = true;
			routes = GetComponentsInChildren<HttpRequestHandler>();
			if (startServerOnLoad)
			{
				StartServing();
			}
		}

		private void OnApplicationQuit()
		{
			Shutdown();
		}

		private void Shutdown()
		{
			try
			{
				listener.Shutdown(SocketShutdown.Both);
				listener.Close();
			}
			catch (ThreadAbortException)
			{
			}
			catch (SocketException)
			{
			}
		}

		private void RouteRequest(Request request)
		{
			string absolutePath = request.uri.AbsolutePath;
			bool flag = false;
			HttpRequestHandler[] array = routes;
			foreach (HttpRequestHandler httpRequestHandler in array)
			{
				if (httpRequestHandler.path == absolutePath)
				{
					flag = true;
					httpRequestHandler.Dispatch(request);
				}
			}
			if (!flag)
			{
				request.response.status = 404;
				request.response.message = "Not Found";
				request.response.Text = "Not found!";
			}
		}

		private IEnumerator ServeHTTP()
		{
			yield return null;
			listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
			IPEndPoint endPoint = new IPEndPoint(IPAddress.Any, port);
			listener.Bind(endPoint);
			listener.Listen(5);
			BackgroundTask background = new BackgroundTask();
			ForegroundTask foreground = new ForegroundTask();
			string host = "http://localhost:" + port;
			while (true)
			{
				yield return background;
				Request request2 = null;
				NetworkStream stream2 = null;
				try
				{
					Socket socket = listener.Accept();
					stream2 = new NetworkStream(socket);
					request2 = Request.BuildFromStream(host, stream2);
				}
				catch (HTTPException)
				{
					Shutdown();
					break;
				}
				catch (ThreadAbortException)
				{
					Shutdown();
					break;
				}
				catch (Exception ex3)
				{
					Debug.LogError("Exception in server thread: " + ex3.ToString());
					Shutdown();
					break;
				}
				yield return foreground;
				RouteRequest(request2);
				yield return background;
				try
				{
					request2.response.headers.Set("Connection", "Close");
					HTTPProtocol.WriteResponse(stream2, request2.response.status, request2.response.message, request2.response.headers, request2.response.Bytes);
					stream2.Flush();
					stream2.Dispose();
				}
				catch (HTTPException)
				{
					Shutdown();
					break;
				}
				catch (ThreadAbortException)
				{
					Shutdown();
					break;
				}
				catch (Exception ex6)
				{
					Debug.LogError("Exception in server thread: " + ex6.ToString());
					Shutdown();
					break;
				}
				if (logRequests)
				{
					Debug.Log(string.Format("{0} {1} {2} \"{3}\" {4}", DateTime.Now.ToString("yyyy/mm/dd H:mm:ss zzz"), request2.response.status, request2.method.ToUpper(), request2.uri, request2.response.Bytes.Length));
				}
			}
		}
	}
}
