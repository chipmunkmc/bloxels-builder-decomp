using HTTP;
using HTTP.Server;

public class HelloWorldHandler : HttpRequestHandler
{
	public override void GET(Request request)
	{
		Response response = request.response;
		response.status = 200;
		response.message = "OK";
		response.Text = "Hello, World!";
	}
}
