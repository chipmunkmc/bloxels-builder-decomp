using System.Collections;
using HTTP;
using UnityEngine;

public class KeepAliveExample : MonoBehaviour
{
	private void Start()
	{
		StartCoroutine(TestKeepAlive());
	}

	private void OnGUI()
	{
		if (GUILayout.Button("Restart"))
		{
			Start();
		}
	}

	private IEnumerator TestKeepAlive()
	{
		for (int i = 0; i < 3; i++)
		{
			Request r = new Request("GET", "http://google.com/");
			yield return r.Send();
			if (r.exception == null)
			{
				Debug.Log(r.response.status);
			}
			else
			{
				Debug.LogError(r.exception);
			}
			yield return new WaitForSeconds(1f);
		}
	}
}
