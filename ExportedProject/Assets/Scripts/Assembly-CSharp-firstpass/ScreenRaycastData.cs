using UnityEngine;

public struct ScreenRaycastData
{
	public bool Is2D;

	public RaycastHit Hit3D;

	public RaycastHit2D Hit2D;

	public GameObject GameObject
	{
		get
		{
			if (Is2D)
			{
				return (!Hit2D.collider) ? null : Hit2D.collider.gameObject;
			}
			return (!Hit3D.collider) ? null : Hit3D.collider.gameObject;
		}
	}
}
