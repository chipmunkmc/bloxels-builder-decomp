using System.Security.Cryptography;
using System.Text;
using UnityEngine;

public static class SecurePlayerPrefs
{
	public static void DeleteKey(string key, string password, bool encryptKey = true)
	{
		DESEncryption dESEncryption = new DESEncryption();
		string key2 = ((!encryptKey) ? key : GenerateMD5(key));
		PlayerPrefs.DeleteKey(key2);
	}

	public static void SetString(string key, string value, string password, bool encryptKey = true)
	{
		DESEncryption dESEncryption = new DESEncryption();
		string key2 = key;
		if (encryptKey)
		{
			key2 = GenerateMD5(key);
		}
		string value2 = dESEncryption.Encrypt(value, password);
		PlayerPrefs.SetString(key2, value2);
	}

	public static string GetString(string key, string password, bool decryptKey = true)
	{
		string key2 = ((!decryptKey) ? key : GenerateMD5(key));
		if (PlayerPrefs.HasKey(key2))
		{
			DESEncryption dESEncryption = new DESEncryption();
			string @string = PlayerPrefs.GetString(key2);
			string plainText;
			dESEncryption.TryDecrypt(@string, password, out plainText);
			return plainText;
		}
		return string.Empty;
	}

	public static string GetString(string key, string defaultValue, string password, bool decryptKey = true)
	{
		if (HasKey(key, decryptKey))
		{
			return GetString(key, password, decryptKey);
		}
		return defaultValue;
	}

	public static bool HasKey(string key, bool decryptKey = true)
	{
		string key2 = ((!decryptKey) ? key : GenerateMD5(key));
		return PlayerPrefs.HasKey(key2);
	}

	private static string GenerateMD5(string text)
	{
		MD5CryptoServiceProvider mD5CryptoServiceProvider = new MD5CryptoServiceProvider();
		byte[] bytes = Encoding.UTF8.GetBytes(text);
		byte[] array = mD5CryptoServiceProvider.ComputeHash(bytes);
		StringBuilder stringBuilder = new StringBuilder();
		for (int i = 0; i < array.Length; i++)
		{
			stringBuilder.Append(array[i].ToString("X2"));
		}
		return stringBuilder.ToString();
	}
}
