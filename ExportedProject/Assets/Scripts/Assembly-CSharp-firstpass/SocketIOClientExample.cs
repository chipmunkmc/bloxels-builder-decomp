using System.Collections;
using HTTP.SocketIO;
using UnityEngine;

[RequireComponent(typeof(SocketIOConnection))]
public class SocketIOClientExample : MonoBehaviour
{
	private IEnumerator Start()
	{
		SocketIOConnection io = GetComponent<SocketIOConnection>();
		io.On("news", HandleNewsEvent);
		while (!io.Ready)
		{
			yield return null;
		}
		io.Emit("my other event", "boo!");
	}

	private void HandleNewsEvent(SocketIOConnection socket, SocketIOMessage msg)
	{
		Debug.Log(string.Concat("Received Event: ", msg.eventName, "(", msg.args, ")"));
	}
}
