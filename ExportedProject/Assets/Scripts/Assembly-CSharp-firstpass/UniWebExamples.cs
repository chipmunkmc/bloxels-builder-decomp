using System;
using System.Collections;
using HTTP;
using UnityEngine;

public class UniWebExamples : MonoBehaviour
{
	private void Start()
	{
		StartCoroutine(TestHTTP10());
		StartCoroutine(UseHTTPS());
		StartCoroutine(FetchAssetBundle());
		StartCoroutine(FetchImage());
		StartCoroutine(PostForm());
		StartCoroutine(TimeoutExample());
		WebSocketExample();
	}

	private void WebCacheExample()
	{
		WebCache webCache = new WebCache();
		webCache.Download("test.txt", "http://www.w3.org/Protocols/rfc2616/rfc2616-sec14.html");
	}

	private void OnGUI()
	{
		if (GUILayout.Button("Restart"))
		{
			Start();
		}
	}

	private IEnumerator TestHTTP10()
	{
		Request r = new Request("GET", "http://www.ngdc.noaa.gov/geomag-web/calculators/calculateDeclination?lat1=-36.8518&lon1=174.8554&resultFormat=xml");
		yield return r.Send();
		if (r.exception == null)
		{
			Debug.Log(r.response.status);
			Debug.Log(r.response.Text);
		}
		else
		{
			Debug.LogError(r.exception);
		}
	}

	private IEnumerator UseHTTPS()
	{
		string url = "https://www.google.com/";
		Request r = new Request("GET", url);
		yield return r.Send();
		if (r.exception == null)
		{
			Debug.Log(r.response.status);
		}
		else
		{
			Debug.Log(r.exception);
		}
	}

	private IEnumerator FetchAssetBundle()
	{
		Request r = new Request("GET", "http://differentmethods.com/~simon/uniwebtest.unity3d");
		yield return r.Send();
		if (r.exception == null)
		{
			Debug.Log(r.response.status);
			AssetBundleCreateRequest abcr = r.response.AssetBundleCreateRequest();
			yield return abcr;
			Debug.Log(abcr.assetBundle);
		}
		else
		{
			Debug.LogError(r.exception);
		}
	}

	private void WebSocketExample()
	{
		WebSocket webSocket = new WebSocket();
		StartCoroutine(webSocket.Dispatcher());
		webSocket.Connect("http://echo.websocket.org");
		webSocket.OnTextMessageRecv += delegate(string e)
		{
			Debug.Log("Reply came from server -> " + e);
		};
		webSocket.Send("Hello");
		webSocket.Send("Hello again!");
		webSocket.Send("Goodbye");
	}

	private IEnumerator PostForm()
	{
		WWWForm form = new WWWForm();
		form.AddField("hello", "world");
		form.AddBinaryData("file", new byte[4] { 65, 65, 65, 65 });
		Request r = new Request("http://google.com/", form);
		yield return r.Send();
		if (r.exception != null)
		{
			Debug.Log(r.exception);
			yield break;
		}
		Debug.Log("Response Text:");
		Debug.Log(r.response.Text);
	}

	private IEnumerator FetchImage()
	{
		string url = "http://www.differentmethods.com/wp-content/uploads/2011/05/react.jpg";
		Request r = new Request("GET", url);
		yield return r.Send();
		if (r.exception == null)
		{
			Debug.Log(r.response.status);
			Texture2D texture2D = new Texture2D(512, 512);
			texture2D.LoadImage(r.response.Bytes);
			GetComponent<Renderer>().material.SetTexture("_MainTex", texture2D);
		}
		else
		{
			Debug.Log(r.exception);
		}
	}

	private IEnumerator TimeoutExample()
	{
		float initialTime = Time.realtimeSinceStartup;
		Request r = new Request("GET", "http://krogh.no/wp-content/uploads/2011/09/earth-huge.png")
		{
			acceptGzip = false,
			useCache = false,
			timeout = 1f
		};
		yield return r.Send();
		Debug.Log("Time: " + (Time.realtimeSinceStartup - initialTime) + "s");
		if (r.exception != null)
		{
			if (r.exception is TimeoutException)
			{
				Debug.Log("Request timed out.");
			}
			else
			{
				Debug.Log("Exception occured in request.");
			}
		}
		else
		{
			Debug.Log("Result received within timeout.");
		}
	}
}
