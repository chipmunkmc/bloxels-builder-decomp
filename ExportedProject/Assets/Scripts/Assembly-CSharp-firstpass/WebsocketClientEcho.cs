using System.Collections;
using HTTP;
using UnityEngine;

public class WebsocketClientEcho : MonoBehaviour
{
	public string url = "http://echo.websocket.org";

	private IEnumerator Start()
	{
		WebSocket ws = new WebSocket();
		StartCoroutine(ws.Dispatcher());
		ws.Connect(url);
		yield return ws.Wait();
		if (ws.exception != null)
		{
			Debug.Log("An exception occured when connecting: " + ws.exception);
		}
		Debug.Log("Connected? : " + ws.connected);
		if (ws.connected)
		{
			ws.OnTextMessageRecv += OnStringReceived;
			ws.Send("Hello!");
			ws.Send("Goodbye");
		}
	}

	private void OnStringReceived(string msg)
	{
		Debug.Log("From server -> " + msg);
	}
}
