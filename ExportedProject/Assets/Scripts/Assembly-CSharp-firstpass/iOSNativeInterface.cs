using System.Runtime.InteropServices;
using UnityEngine;

public class iOSNativeInterface : MonoBehaviour
{
	public static iOSNativeInterface instance;

	private bool isCameraOpen;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
	}

	[DllImport("__Internal")]
	private static extern void _openCameraSession();

	[DllImport("__Internal")]
	private static extern void _playCameraSession();

	[DllImport("__Internal")]
	private static extern void _pauseCameraSession();

	[DllImport("__Internal")]
	private static extern void _receiveOpenGLTexturePointer(long ptr);

	[DllImport("__Internal")]
	private static extern void _setExposureValue(double setting);

	[DllImport("__Internal")]
	private static extern void _stopCameraSession();

	[DllImport("__Internal")]
	public static extern string GetSettingsURL();

	[DllImport("__Internal")]
	public static extern void OpenSettings();

	public static void openCameraSession()
	{
		if (Application.platform != 0)
		{
			_openCameraSession();
		}
	}

	public static void stopCameraSession()
	{
		if (Application.platform != 0)
		{
			_stopCameraSession();
		}
	}

	public static void sendOpenGLTexturePointer(long ptr)
	{
		if (Application.platform != 0)
		{
			_receiveOpenGLTexturePointer(ptr);
		}
	}

	public void setCameraStatus(string message)
	{
		if (message == "true")
		{
			isCameraOpen = true;
		}
		else
		{
			isCameraOpen = false;
		}
	}

	public bool getCameraStatus()
	{
		return isCameraOpen;
	}

	public static void PauseCameraSession()
	{
		if (Application.platform != 0)
		{
			_pauseCameraSession();
		}
	}

	public static void PlayCameraSession()
	{
		if (Application.platform != 0)
		{
			_playCameraSession();
		}
	}

	public static void SetExposureValue(double d)
	{
		if (Application.platform != 0)
		{
			_setExposureValue(d);
		}
	}
}
