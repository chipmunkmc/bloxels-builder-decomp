using System.Diagnostics;
using System.Runtime.CompilerServices;
using UnityEngine.UI;

public class AccountAgreementsPanel : RegisterMenu
{
	public Toggle privacyPolicyRadioButton;

	public Toggle promotionalRadioButton;

	private void Start()
	{
		uiButtonNext.interactable = false;
		privacyPolicyRadioButton.onValueChanged.AddListener(DetermineIfReadyToProceed);
	}

	private new void OnDestroy()
	{
		privacyPolicyRadioButton.onValueChanged.RemoveListener(DetermineIfReadyToProceed);
	}

	private void DetermineIfReadyToProceed(bool isOn)
	{
		if (privacyPolicyRadioButton.isOn)
		{
			uiButtonNext.interactable = true;
		}
		else
		{
			uiButtonNext.interactable = false;
		}
	}

	public override void Next()
	{
		GemManager.instance.AddGemsForType(GemEarnable.Type.AccountSetup);
		if (!controller.isOfAge)
		{
			StartCoroutine(BloxelServerRequests.instance.UpdateChildAccount(CurrentUser.instance.account.serverID, controller.chosenEmail, controller.chosenPw, promotionalRadioButton.isOn, delegate(string response)
			{
				if (BloxelServerRequests.ResponseOk(response))
				{
					string userToken = CurrentUser.instance.account.userToken.Clone() as string;
					UserAccount userAccount = new UserAccount(response)
					{
						userToken = userToken
					};
					CurrentUser.instance.Login(userAccount);
					_003CNext_003E__BaseCallProxy0();
				}
			}));
			return;
		}
		StartCoroutine(BloxelServerRequests.instance.RegisterUser(controller.chosenUserName, controller.avatarBoardId, controller.chosenEmail, controller.chosenPw, controller.isOfAge, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				CurrentUser.instance.account = new UserAccount(response);
				_003CNext_003E__BaseCallProxy0();
			}
		}));
	}

	[CompilerGenerated]
	[DebuggerHidden]
	private void _003CNext_003E__BaseCallProxy0()
	{
		base.Next();
	}
}
