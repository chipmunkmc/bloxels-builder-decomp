using System.Collections;
using System.Collections.Generic;
using System.IO;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AccountTransitionSceneController : MonoBehaviour
{
	public TextMeshProUGUI uiTextStatus;

	public RectTransform rectPoop;

	private bool _cloudSyncComplete;

	private DirectoryInfo[] _guestDirectories;

	private void Awake()
	{
		AppStateManager.instance.SetCurrentScene(SceneName.AccountTransition);
		SoundManager.instance.PlayMusic(SoundManager.instance.choice5);
		rectPoop.localScale = Vector3.zero;
		IEnumerator enumerator = null;
		switch (AppStateManager.instance.acctTransitionState)
		{
		case AppStateManager.AccountTransitionState.Login:
			enumerator = initLogin();
			break;
		case AppStateManager.AccountTransitionState.Logout:
			enumerator = initLogout();
			break;
		case AppStateManager.AccountTransitionState.GuestCopy:
			doGuestRoutine();
			return;
		}
		if (enumerator != null)
		{
			StartCoroutine(enumerator);
		}
	}

	private void Start()
	{
		if (BloxelLibrary.instance != null)
		{
			BloxelLibrary.instance.gameObject.SetActive(false);
		}
		if (DeviceManager.isPoopDevice)
		{
			rectPoop.localScale = Vector3.one;
		}
	}

	private void OnDestroy()
	{
		if (BloxelLibrary.instance != null)
		{
			BloxelLibrary.instance.gameObject.SetActive(true);
		}
	}

	private void doGuestRoutine()
	{
		CloudManager.Instance.addToQueueOverride = true;
		uiTextStatus.SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen18", "Syncing Guest Account"));
		DirectoryInfo[] directories = copyGuestFiles();
		queueGuestFilesForSync(directories);
		uiTextStatus.SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen19", "Reinitializing the Library"));
		AssetManager.instance.ReInitPools();
		CloudManager.Instance.addToQueueOverride = false;
		StartCoroutine(DelayLoad());
	}

	private IEnumerator initLogin()
	{
		uiTextStatus.SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen4", "Downloading Cloud Files"));
		yield return StartCoroutine(CloudManager.Instance.GetCloudZip(delegate(byte[] stream)
		{
			uiTextStatus.SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen20", "Decoding Cloud Files"));
			if (stream != null)
			{
				Zipper.ExtractFromStream(stream, AssetManager.baseStoragePath, false);
			}
			if (CurrentUser.syncWithGuest)
			{
				uiTextStatus.SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen21", "Moving Guest Files"));
				_guestDirectories = copyGuestFiles();
			}
			uiTextStatus.SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen3", "Updating the Library"));
			AssetManager.instance.ReInitPools();
			StartCoroutine(DelayLoad());
		}));
	}

	private IEnumerator initLogout()
	{
		yield return SaveManager.Instance.ForceRun();
		uiTextStatus.SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen3", "Updating the Library"));
		AssetManager.instance.ReInitPools();
		yield return StartCoroutine(DelayLoad());
	}

	private DirectoryInfo[] copyGuestFiles()
	{
		DirectoryInfo[] topLevelGuestDirectories = BloxelProject.GetTopLevelGuestDirectories();
		for (int i = 0; i < topLevelGuestDirectories.Length; i++)
		{
			PPUtilities.CopyDirectory(topLevelGuestDirectories[i].FullName, AssetManager.baseStoragePath + topLevelGuestDirectories[i].Name);
		}
		return topLevelGuestDirectories;
	}

	private void queueGuestFilesForSync(DirectoryInfo[] directories)
	{
		foreach (DirectoryInfo directoryInfo in directories)
		{
			IEnumerator<string> enumerator = PPUtilities.GetFileList(directoryInfo.FullName, "*").GetEnumerator();
			while (enumerator.MoveNext())
			{
				DiskPathInfo diskPathInfo = new DiskPathInfo(enumerator.Current, CurrentUser.instance.account);
				ProjectSyncInfo syncInfo = new ProjectSyncInfo(diskPathInfo.diskPathString, diskPathInfo.s3Path, diskPathInfo.guid, diskPathInfo.type);
				CloudManager.Instance.EnqueueFileForSync(syncInfo, false);
			}
		}
	}

	private IEnumerator DelayLoad()
	{
		while (!AssetManager.initComplete)
		{
			yield return new WaitForEndOfFrame();
		}
		if (AppStateManager.instance.acctTransitionState == AppStateManager.AccountTransitionState.Login && CurrentUser.syncWithGuest)
		{
			CloudManager.Instance.addToQueueOverride = true;
			queueGuestFilesForSync(_guestDirectories);
			CloudManager.Instance.addToQueueOverride = false;
			CurrentUser.syncWithGuest = false;
		}
		string text2 = string.Empty;
		switch (AppStateManager.instance.previousScene)
		{
		case SceneName.Home:
			text2 = LocalizationManager.getInstance().getLocalizedText("loadingscreen7", "Reloading Home Screen");
			break;
		case SceneName.Viewer:
			text2 = LocalizationManager.getInstance().getLocalizedText("loadingscreen6", "Reloading Infinity Wall");
			break;
		case SceneName.PixelEditor_iPad:
			text2 = LocalizationManager.getInstance().getLocalizedText("loadingscreen5", "Reloading Editor");
			break;
		default:
			text2 = LocalizationManager.getInstance().getLocalizedText("loadingscreen22", "Reloading Screen");
			break;
		}
		uiTextStatus.SetText(text2);
		if (BloxelLibrary.instance != null)
		{
			BloxelLibrary.instance.ReInitAllLibraries();
		}
		AppStateManager.instance.acctTransitionState = AppStateManager.AccountTransitionState.None;
		StartCoroutine(LoadScene(AppStateManager.instance.previousScene));
	}

	private IEnumerator LoadScene(SceneName name)
	{
		AsyncOperation loader = SceneManager.LoadSceneAsync(name.ToString());
		while (!loader.isDone)
		{
			yield return null;
		}
	}
}
