using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class AgeGateTooltip : UITooltip
{
	public enum Location
	{
		Terms = 0,
		Privacy = 1,
		PasswordReset = 2,
		Website = 3,
		Gems = 4,
		Brains = 5,
		Boards = 6,
		Animations = 7,
		Backgrounds = 8,
		Characters = 9,
		Games = 10,
		Capture = 11,
		Codeboard = 12,
		iWall = 13,
		All = 14,
		Support = 15,
		KickStarter = 16,
		Legal = 17,
		Buy = 18,
		AboutBoard = 19,
		Educators = 20,
		Migration = 21
	}

	[Header("UI")]
	public UIButton uiButtonGo;

	public Text uiTextNumber1;

	public Text uiTextNumber2;

	public InputField uiInputField;

	[Header("Data")]
	public Location location;

	public Dictionary<Location, string> urlLookup;

	[Header("State Tracking")]
	private bool _canProgress;

	private int num1;

	private int num2;

	public bool isOverrideLink;

	public string linkOverride = string.Empty;

	public bool isForCameraSettings;

	private new void Awake()
	{
		base.Awake();
		urlLookup = new Dictionary<Location, string>
		{
			{
				Location.Terms,
				PPUtilities.Link_terms
			},
			{
				Location.Privacy,
				PPUtilities.Link_privacy
			},
			{
				Location.PasswordReset,
				PPUtilities.Link_passwordReset
			},
			{
				Location.Website,
				PPUtilities.Link_website
			},
			{
				Location.Gems,
				PPUtilities.Link_Gems
			},
			{
				Location.Brains,
				PPUtilities.Link_Brains
			},
			{
				Location.Support,
				PPUtilities.Link_support
			},
			{
				Location.KickStarter,
				PPUtilities.Link_kickstarter
			},
			{
				Location.Legal,
				PPUtilities.Link_legal
			},
			{
				Location.AboutBoard,
				PPUtilities.Link_AboutBoard
			},
			{
				Location.Educators,
				PPUtilities.Link_StudentAccounts
			},
			{
				Location.Migration,
				PPUtilities.Link_MigrationFallback
			}
		};
	}

	private void Start()
	{
		toggleButtonState();
		uiInputField.onValueChanged.AddListener(delegate
		{
			StopCoroutine("InputTimer");
			StartCoroutine("InputTimer");
		});
		uiButtonGo.OnClick += GoToLocation;
		uiButtonGo.GetComponentInChildren<TextMeshProUGUI>().rectTransform.localScale = new Vector3(0.85f, 0.85f, 1f);
	}

	private new void OnDestroy()
	{
		uiButtonGo.OnClick -= GoToLocation;
	}

	public override void Activate()
	{
		num1 = Random.Range(0, 9);
		num2 = Random.Range(0, 9);
		uiTextNumber1.text = num1.ToString();
		uiTextNumber2.text = num2.ToString();
		base.Activate();
	}

	private IEnumerator InputTimer()
	{
		yield return new WaitForSeconds(0.35f);
		_canProgress = isCorrect();
		toggleButtonState();
	}

	private bool isCorrect()
	{
		if (string.IsNullOrEmpty(uiInputField.text))
		{
			return false;
		}
		int num = int.Parse(uiInputField.text);
		int num2 = num1 * this.num2;
		return num == num2;
	}

	private void toggleButtonState()
	{
		uiButtonGo.interactable = _canProgress;
	}

	public void SetOverrideLink(string link)
	{
		linkOverride = link;
		isOverrideLink = true;
	}

	private void GoToLocation()
	{
		string empty = string.Empty;
		empty = (isOverrideLink ? linkOverride : urlLookup[location]);
		if (!isForCameraSettings)
		{
			Application.OpenURL(empty);
		}
		else
		{
			iOSNativeInterface.OpenSettings();
		}
		DeActivate();
	}
}
