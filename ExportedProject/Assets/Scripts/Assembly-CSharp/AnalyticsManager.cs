using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Analytics;

public class AnalyticsManager : MonoBehaviour
{
	public enum Event
	{
		NewGame = 0,
		CaptureConfirm = 1,
		SceneOpen = 2,
		ShowCapture = 3,
		Share = 4,
		Help = 5,
		Warp = 6,
		NewsOpen = 7,
		AdjustWarmth = 8,
		AdjustBrightness = 9
	}

	public static void SendEventBool(Event e, bool result)
	{
		Analytics.CustomEvent(e.ToString(), new Dictionary<string, object> { { "Value", result } });
	}

	public static void SendEventString(Event e, string result)
	{
		Analytics.CustomEvent(e.ToString(), new Dictionary<string, object> { { "Value", result } });
	}

	public static void SendEventObject(Event e, string[] results)
	{
		Dictionary<string, object> dictionary = new Dictionary<string, object>();
		for (int i = 0; i < results.Length; i++)
		{
			dictionary.Add("Value" + i, results[i]);
		}
		Analytics.CustomEvent(e.ToString(), dictionary);
	}
}
