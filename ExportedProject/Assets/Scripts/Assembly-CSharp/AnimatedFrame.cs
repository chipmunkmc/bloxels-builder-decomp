using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class AnimatedFrame : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public static AnimatedFrame instance;

	public UIAnimationFrameRate uiFrameRate;

	public UIProjectCover cover;

	public BloxelAnimation bloxelAnimation;

	public RawImage uiRawImage;

	public RectTransform frameRect;

	private Texture baseTex;

	private bool _canShowGemPurchase = true;

	[Header("Gem Locking")]
	public UIGemLock uiGemLock;

	public TextMeshProUGUI uiTextFrameCount;

	public RectTransform rectMaxFrames;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		baseTex = uiRawImage.texture;
		if (frameRect == null)
		{
			frameRect = uiRawImage.GetComponent<RectTransform>();
		}
		rectMaxFrames.localScale = Vector3.zero;
		uiTextFrameCount.transform.localScale = Vector3.zero;
	}

	private void Start()
	{
		BloxelAnimationLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
		uiFrameRate.OnChange += FrameRateChange;
	}

	private void OnDestroy()
	{
		BloxelAnimationLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
		uiFrameRate.OnChange -= FrameRateChange;
	}

	private void FrameRateChange(float _val)
	{
		cover.UpdateFrameRate(_val);
		if (BloxelAnimationLibrary.instance.currentAnimation != null && BloxelAnimationLibrary.instance.currentAnimation.id.Equals(bloxelAnimation.id))
		{
			SavedAnimation selectedProject = BloxelLibraryScrollController.instance.GetSelectedProject<SavedAnimation>();
			if (selectedProject != null)
			{
				selectedProject.cover.UpdateFrameRate(_val);
			}
		}
	}

	public void HandleOnProjectSelect(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.Animator)
		{
			InitFromBloxelAnimation(item.dataModel as BloxelAnimation);
		}
	}

	public void InitFromBloxelAnimation(BloxelAnimation anim)
	{
		cover.transform.localScale = Vector3.zero;
		bloxelAnimation = anim;
		cover.Init(anim, new CoverStyle(Color.clear));
		cover.transform.DOScale(1f, UIAnimationManager.speedFast);
		InitContextualInfo();
	}

	public void InitContextualInfo()
	{
		uiGemLock.Init(new AnimationFrameUnlockable(bloxelAnimation.boards.Count));
		string frameString = " ";
		if (bloxelAnimation.boards.Count > 1)
		{
			frameString += LocalizationManager.getInstance().getLocalizedText("gamebuilder50", "Frames");
		}
		else
		{
			frameString += LocalizationManager.getInstance().getLocalizedText("gamebuilder51", "Frame");
		}
		uiTextFrameCount.transform.DOScale(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiTextFrameCount.SetText(bloxelAnimation.boards.Count + frameString);
		});
		if (bloxelAnimation.boards.Count >= BloxelAnimation.MaxFrames)
		{
			rectMaxFrames.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic);
		}
		else
		{
			rectMaxFrames.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InElastic);
		}
	}

	public void Clear()
	{
		cover.StopAnimating();
		uiRawImage.texture = baseTex;
	}

	public void ShowGemPurchase()
	{
		if (_canShowGemPurchase)
		{
			_canShowGemPurchase = false;
			uiGemLock.Wobble().OnComplete(delegate
			{
				UIOverlayCanvas.instance.GemBuyPopup(uiGemLock.model);
				_canShowGemPurchase = true;
			});
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (uiGemLock.isLocked)
		{
			ShowGemPurchase();
		}
	}
}
