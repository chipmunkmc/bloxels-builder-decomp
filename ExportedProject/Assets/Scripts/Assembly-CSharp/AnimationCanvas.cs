using DG.Tweening;
using UnityEngine;

public class AnimationCanvas : UIMoveable
{
	public static AnimationCanvas instance;

	[Header("| ========= UI ========= |")]
	public CanvasGroup uiGroupStartMenu;

	public UIButton uiButtonStartInit;

	public UIButton uiButtonEdit;

	private bool initComplete;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		base.Awake();
	}

	private new void Start()
	{
		BloxelAnimationLibrary.instance.OnProjectSelect += LibraryItemChanged;
		BloxelAnimationLibrary.instance.OnProjectDelete += LibraryItemDeleted;
		PixelToolPalette.instance.OnChange += HandleToolPaletteVisibility;
		uiButtonStartInit.OnClick += BloxelAnimationLibrary.instance.AddButtonPressed;
		uiButtonEdit.OnClick += ShowLibrary;
		initComplete = true;
	}

	private void OnDestroy()
	{
		uiButtonStartInit.OnClick -= BloxelAnimationLibrary.instance.AddButtonPressed;
		uiButtonEdit.OnClick -= ShowLibrary;
		BloxelAnimationLibrary.instance.OnProjectSelect -= LibraryItemChanged;
		BloxelAnimationLibrary.instance.OnProjectDelete -= LibraryItemDeleted;
		PixelToolPalette.instance.OnChange -= HandleToolPaletteVisibility;
	}

	private void ShowLibrary()
	{
		BloxelLibrary.instance.libraryToggle.Show();
	}

	public new void Show()
	{
		base.Show();
		if (BloxelAnimationLibrary.instance.currentAnimation != null)
		{
			HideStart(true);
			InitWithProject(BloxelAnimationLibrary.instance.currentAnimation);
		}
		else
		{
			ShowStart(true);
		}
	}

	public new void Hide()
	{
		base.Hide();
		AnimationTimeline.instance.Hide();
	}

	private void HandleToolPaletteVisibility(bool _isOpen)
	{
		if (PixelEditorController.instance.mode == CanvasMode.Animator)
		{
			if (_isOpen)
			{
				AnimationTimeline.instance.Hide();
			}
			else
			{
				AnimationTimeline.instance.Show();
			}
		}
	}

	private void LibraryItemDeleted(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.Animator)
		{
			ShowStart(true);
		}
	}

	public void LibraryItemChanged(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.Animator)
		{
			HideStart(true);
			AnimationTimeline.instance.InitFromBloxelAnimation(item.dataModel as BloxelAnimation);
		}
	}

	public void ShowStart(bool shouldHidePaintBoard = false)
	{
		uiGroupStartMenu.gameObject.SetActive(true);
		uiGroupStartMenu.DOFade(1f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			AnimationTimeline.instance.DumpTimeline();
			AnimatedFrame.instance.Clear();
			AnimationTimeline.instance.Hide();
			if (shouldHidePaintBoard)
			{
				PixelEditorPaintBoard.instance.Hide();
			}
			BloxelAnimationLibrary.instance.Reset();
		});
	}

	public void HideStart(bool showPaintBoard = false)
	{
		uiGroupStartMenu.DOFade(0f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			uiGroupStartMenu.gameObject.SetActive(false);
			AnimationTimeline.instance.Show();
			if (showPaintBoard)
			{
				PixelEditorPaintBoard.instance.Show();
			}
		});
	}

	public void InitWithProject(BloxelAnimation animation)
	{
		AnimationTimeline.instance.InitFromBloxelAnimation(animation);
		RemoveAnimationMeshesFromCache(animation);
	}

	private void RemoveAnimationMeshesFromCache(BloxelAnimation animation)
	{
		for (int i = 0; i < animation.boards.Count; i++)
		{
			BloxelBoard bloxelBoard = animation.boards[i];
			CachedMesh value = null;
			if (MeshCacheManager.Instance.cachedMeshes.TryGetValue(bloxelBoard.localID, out value))
			{
				value.RemoveFromCacheImmediately();
			}
		}
	}

	public void CreateNewAnimation()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmA);
		BloxelAnimation bloxelAnimation = BloxelAnimationLibrary.instance.AddProject(new BloxelAnimation(new BloxelBoard()));
		bloxelAnimation.SetTitle(BloxelAnimation.defaultTitle);
	}
}
