public class AnimationFrameUnlockable : GemUnlockable
{
	public int currentCount;

	public AnimationFrameUnlockable(int _currentCount)
	{
		currentCount = _currentCount;
		cost = GemUnlockable.AnimationFramesCost;
		type = Type.BrainBoardSlot;
		uiBuyable = UIBuyable.CreateAnimationFrame();
	}

	public override bool IsLocked()
	{
		return currentCount >= GemManager.MaxAnimationFramesWithoutUnlock && !GemManager.animationFramesUnlocked;
	}

	public override void Unlock()
	{
		GemManager.instance.UnlockAnimationFrames();
		EventUnlock(this);
	}
}
