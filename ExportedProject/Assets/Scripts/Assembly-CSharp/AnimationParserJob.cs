using System;
using System.Threading;

public class AnimationParserJob : ThreadedJob
{
	public delegate void HandleParseComplete(BloxelAnimation _animation);

	public bool isRunning;

	public string responseFromServer;

	public BloxelAnimation animation;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		animation = new BloxelAnimation(responseFromServer, DataSource.JSONString);
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(animation);
		}
	}
}
