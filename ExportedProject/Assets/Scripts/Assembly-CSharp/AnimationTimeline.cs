using System;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class AnimationTimeline : UIMoveable
{
	public delegate void HandleFrameChange(TimelineFrame _frame);

	public delegate void HandleAnimationEdit(BloxelAnimation _theAnimation);

	public delegate void HandleAddFrameAfter();

	public delegate void HandleAddFrameBefore();

	public static AnimationTimeline instance;

	[Header("| ========= UI ========= |")]
	public ScrollRect scrollRect;

	public RectTransform itemContainer;

	public Transform coverContainer;

	public GridLayoutGroup gridLayout;

	public Color[] currentPalette;

	public UnityEngine.Object timelineFramePrefab;

	public UIAnimationFrameRate uiFrameRate;

	[Header("| ========= Data ========= |")]
	public List<TimelineFrame> savedFrames = new List<TimelineFrame>();

	public TimelineFrame currentFrame;

	public BloxelAnimation currentBloxelAnimation;

	public event HandleFrameChange OnFrameChange;

	public event HandleAnimationEdit OnAnimationEdit;

	public event HandleAddFrameAfter OnAddFrameAfter;

	public event HandleAddFrameBefore OnAddFrameBefore;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		timelineFramePrefab = Resources.Load("Prefabs/TimelineFrame");
		scrollRect = GetComponentInChildren<ScrollRect>();
		scrollRect.content = itemContainer;
		gridLayout = itemContainer.GetComponent<GridLayoutGroup>();
	}

	public new Tweener Show()
	{
		rect.DOKill(true);
		BloxelAnimationLibrary.instance.OnProjectDelete += HandleOnProjectDelete;
		AnimatedFrame.instance.cover.Animate();
		return base.Show();
	}

	public new Tweener Hide()
	{
		rect.DOKill(true);
		BloxelAnimationLibrary.instance.OnProjectDelete -= HandleOnProjectDelete;
		return base.Hide();
	}

	private void HandleOnProjectDelete(SavedProject item)
	{
		DumpTimeline();
		AnimatedFrame.instance.Clear();
	}

	public void SetCurrentFrame(TimelineFrame _frame)
	{
		currentFrame = _frame;
		if (this.OnFrameChange != null)
		{
			this.OnFrameChange(currentFrame);
		}
	}

	public void AnimationEdited()
	{
		if (this.OnAnimationEdit != null)
		{
			this.OnAnimationEdit(currentBloxelAnimation);
		}
	}

	private void AdjustScrollRectWidth()
	{
		int count = savedFrames.Count;
		float num = gridLayout.spacing.x + gridLayout.cellSize.x;
		float x = Mathf.Abs((float)count * num) + Mathf.Abs(num);
		RectTransform content = scrollRect.content;
		content.sizeDelta = new Vector2(x, content.sizeDelta.y);
	}

	public void InitFromBloxelAnimation(BloxelAnimation anim)
	{
		currentBloxelAnimation = anim;
		BuildTimeline();
		AnimatedFrame.instance.InitFromBloxelAnimation(currentBloxelAnimation);
		uiFrameRate.UpdateModel(anim);
	}

	public void DumpTimeline()
	{
		savedFrames.Clear();
		foreach (Transform item in itemContainer)
		{
			UnityEngine.Object.Destroy(item.gameObject);
		}
	}

	public void BuildTimeline()
	{
		DumpTimeline();
		int num = 0;
		for (int i = 0; i < currentBloxelAnimation.boards.Count; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(timelineFramePrefab) as GameObject;
			TimelineFrame component = gameObject.GetComponent<TimelineFrame>();
			savedFrames.Add(component);
			component.dataModel = currentBloxelAnimation.boards[i];
			component.rect.SetParent(itemContainer);
			component.rect.localScale = Vector3.one;
			component.rect.localPosition = Vector3.zero;
			component.uiImageOutline.rectTransform.sizeDelta = new Vector2(gridLayout.cellSize.x, gridLayout.cellSize.y);
			if (itemContainer.childCount > 1)
			{
				gameObject.transform.SetAsLastSibling();
			}
			num++;
		}
		savedFrames[0].Select();
		AdjustScrollRectWidth();
	}

	public void UpdateFrames()
	{
		foreach (TimelineFrame savedFrame in savedFrames)
		{
			savedFrame.BuildCover();
		}
	}

	public void AddBeforeActiveFrame()
	{
		BloxelBoard bloxelBoard = new BloxelBoard(currentFrame.dataModel.blockColors, currentFrame.dataModel.toolPaletteChoices);
		bloxelBoard.blockColors = (BlockColor[,])currentFrame.dataModel.blockColors.Clone();
		bloxelBoard.toolPaletteChoices = (Color[])currentFrame.dataModel.toolPaletteChoices.Clone();
		bloxelBoard.BlastUpdate();
		ColorPalette.instance.SetPaletteTexCoordsForBoard(bloxelBoard);
		AddFrameBeforeCurrentFrame(bloxelBoard);
		AnimationEdited();
		if (this.OnAddFrameBefore != null)
		{
			this.OnAddFrameBefore();
		}
	}

	public void AddAfterActiveFrame()
	{
		BloxelBoard bloxelBoard = new BloxelBoard(currentFrame.dataModel.blockColors, currentFrame.dataModel.toolPaletteChoices);
		bloxelBoard.BlastUpdate();
		ColorPalette.instance.SetPaletteTexCoordsForBoard(bloxelBoard);
		AddFrameAfterCurrentFrame(bloxelBoard);
		AnimationEdited();
		if (this.OnAddFrameAfter != null)
		{
			this.OnAddFrameAfter();
		}
	}

	public void AddFrameBeforeCurrentFrame(BloxelBoard b)
	{
		int siblingIndex = currentFrame.transform.GetSiblingIndex();
		currentBloxelAnimation.boards.Insert(siblingIndex, b);
		PixelEditorPaintBoard.instance.currentBloxelBoard = b;
		b.Save();
		GameObject gameObject = UnityEngine.Object.Instantiate(timelineFramePrefab) as GameObject;
		TimelineFrame component = gameObject.GetComponent<TimelineFrame>();
		savedFrames.Add(component);
		component.dataModel = b;
		component.rect.SetParent(itemContainer);
		component.rect.SetSiblingIndex(siblingIndex);
		component.rect.localScale = Vector3.one;
		component.rect.localPosition = Vector3.zero;
		component.uiImageOutline.rectTransform.sizeDelta = new Vector2(gridLayout.cellSize.x, gridLayout.cellSize.y);
		component.Select();
		gameObject.transform.SetSiblingIndex(siblingIndex);
		SetCurrentFrame(component);
		currentPalette = b.toolPaletteChoices;
		PixelToolPalette.instance.UpdatePalette(currentPalette);
		AdjustScrollRectWidth();
		currentBloxelAnimation.Save(false);
	}

	public void AddFrameAfterCurrentFrame(BloxelBoard b)
	{
		int num = currentFrame.transform.GetSiblingIndex() + 1;
		currentBloxelAnimation.boards.Insert(num, b);
		PixelEditorPaintBoard.instance.currentBloxelBoard = b;
		b.Save();
		GameObject gameObject = UnityEngine.Object.Instantiate(timelineFramePrefab) as GameObject;
		TimelineFrame component = gameObject.GetComponent<TimelineFrame>();
		savedFrames.Add(component);
		component.dataModel = b;
		component.rect.SetParent(itemContainer);
		component.rect.SetSiblingIndex(num);
		component.rect.localScale = Vector3.one;
		component.rect.localPosition = Vector3.zero;
		component.uiImageOutline.rectTransform.sizeDelta = new Vector2(gridLayout.cellSize.x, gridLayout.cellSize.y);
		component.Select();
		gameObject.transform.SetSiblingIndex(num);
		SetCurrentFrame(component);
		currentPalette = b.toolPaletteChoices;
		PixelToolPalette.instance.UpdatePalette(currentPalette);
		AdjustScrollRectWidth();
		currentBloxelAnimation.Save(false);
	}

	public void RemoveActiveFrame()
	{
		bool flag = false;
		foreach (TimelineFrame savedFrame in savedFrames)
		{
			if (savedFrame.isActive)
			{
				flag = currentBloxelAnimation.RemoveBoard(savedFrame.dataModel);
				if (flag)
				{
					break;
				}
			}
		}
		if (flag)
		{
			AdjustScrollRectWidth();
			BuildTimeline();
			AnimationEdited();
		}
	}
}
