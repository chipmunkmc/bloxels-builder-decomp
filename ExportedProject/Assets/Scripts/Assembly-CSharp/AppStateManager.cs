using System;
using System.Threading;
using UnityEngine;

public class AppStateManager : MonoBehaviour
{
	public enum AccountTransitionState
	{
		None = 0,
		Login = 1,
		Logout = 2,
		GuestCopy = 3
	}

	public delegate void HandleSceneChange(SceneName scene);

	public delegate void HandleBoardStatus();

	public static AppStateManager instance;

	public static bool initComplete;

	public string appVersion;

	public int internalVersion;

	public bool outDatedVersion;

	private bool _userOwnsBoard;

	public BloxelGame loadedGame;

	public ServerSquare iWallGameSquare;

	public AccountTransitionState acctTransitionState;

	public bool activateChallenges;

	public static bool ActivateFeatured;

	public bool attemptedViewerRefresh;

	public bool userOwnsBoard
	{
		get
		{
			return _userOwnsBoard;
		}
		set
		{
			_userOwnsBoard = value;
			if (_userOwnsBoard && this.OnOwnsBoard != null)
			{
				this.OnOwnsBoard();
			}
		}
	}

	public SceneName currentScene { get; private set; }

	public SceneName previousScene { get; private set; }

	public PPInputController.SliderControllerScheme SliderControllerScheme
	{
		get
		{
			if (PlayerPrefs.HasKey("SliderControllerScheme"))
			{
				return (PPInputController.SliderControllerScheme)PlayerPrefs.GetInt("ControllerScheme");
			}
			return PPInputController.SliderControllerScheme.Fixed;
		}
		set
		{
			PlayerPrefs.SetInt("SliderControllerScheme", (int)value);
		}
	}

	public event HandleSceneChange OnSceneChange;

	public event HandleBoardStatus OnOwnsBoard;

	private void Awake()
	{
		if (!initComplete)
		{
			if (instance == null)
			{
				instance = this;
			}
			else if (instance != null && instance != this)
			{
				UnityEngine.Object.Destroy(base.gameObject);
				return;
			}
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			if (GetComponent<AnalyticsManager>() == null)
			{
				base.gameObject.AddComponent<AnalyticsManager>();
			}
			if (GetComponent<BloxelsSceneManager>() == null)
			{
				base.gameObject.AddComponent<BloxelsSceneManager>();
			}
			if (GetComponent<DeviceManager>() == null)
			{
				base.gameObject.AddComponent<DeviceManager>();
			}
			if (GetComponent<PlayerBankManager>() == null)
			{
				base.gameObject.AddComponent<PlayerBankManager>();
			}
			if (GetComponent<InternetReachabilityVerifier>() == null)
			{
				base.gameObject.AddComponent<InternetReachabilityVerifier>();
			}
			if (GetComponent<CurrentUser>() == null)
			{
				base.gameObject.AddComponent<CurrentUser>();
			}
			if (GetComponent<ExchangeManager>() == null)
			{
				base.gameObject.AddComponent<ExchangeManager>();
			}
			if (GetComponent<ChallengeManager>() == null)
			{
				base.gameObject.AddComponent<ChallengeManager>();
			}
			if (GetComponent<BadgeManager>() == null)
			{
				base.gameObject.AddComponent<BadgeManager>();
			}
			if (GetComponent<CloudManager>() == null)
			{
				base.gameObject.AddComponent<CloudManager>();
			}
			if (GetComponent<SaveManager>() == null)
			{
				base.gameObject.AddComponent<SaveManager>();
			}
			activateChallenges = false;
			outDatedVersion = false;
			userOwnsBoard = PrefsManager.instance.userOwnsBoard;
			initComplete = true;
			GenerateVersion();
		}
	}

	private void Start()
	{
		if (userOwnsBoard)
		{
			GemManager.instance.BulkRoomUnlock();
		}
	}

	private void GenerateVersion()
	{
		string[] array = appVersion.Split('b');
		string text = array[0];
		string[] array2 = text.Split('.');
		int num = 0;
		int num2 = 100;
		for (int i = 0; i < array2.Length; i++)
		{
			num += num2 * int.Parse(array2[i]);
			num2 /= 10;
		}
		internalVersion = num;
	}

	public void SetCurrentScene(SceneName scene)
	{
		currentScene = scene;
		if (this.OnSceneChange != null)
		{
			this.OnSceneChange(scene);
		}
	}

	public void SetPreviousScene(SceneName scene)
	{
		previousScene = scene;
	}

	public SceneName GetCurrentScene()
	{
		return currentScene;
	}

	public SceneName GetPreviousScene()
	{
		return previousScene;
	}

	public bool CopyToClipBoard(string s)
	{
		UniPasteBoard.SetClipBoardString(s);
		if (UniPasteBoard.GetClipBoardString() != s)
		{
			return false;
		}
		return true;
	}

	public string PasteFromClipBoard()
	{
		return UniPasteBoard.GetClipBoardString();
	}
}
