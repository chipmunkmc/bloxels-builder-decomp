using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using UnityEngine;

public class AssetManager : MonoBehaviour
{
	public delegate void HandleInit();

	public static AssetManager instance;

	public static readonly string homeMiniGameID = "0e9c7b15-cd28-49d8-a70c-a05e2919f7e6";

	public static readonly string demoGameID = "7cd3536f-68e3-4076-8372-9099829dfdee";

	public static readonly string demoBackgroundID = "ec0f6182-573b-4f19-914b-4ebcf77a87ec";

	public static readonly string characterPreviewGameID = "9bf3aaa7-fd41-46f2-ade2-4bafe5ac0e58";

	public static readonly string poultryPanicID = "e4675269-681b-4e06-8939-2d057dbfba06";

	public static readonly string poultryPanicVersion2 = "5f276d51-af46-40ea-8ced-a878b83606dd";

	public static readonly string challengeLayout_1_1 = "a30a7f75-d85b-41b7-a616-8525346e9544";

	public static readonly string challengeLayout_1_2 = "691e91b6-e7c7-4445-9364-8795abd725be";

	public static readonly string challengeLayout_2_1_1 = "4cc06233-b810-4164-a940-2903d05c8bb6";

	public static readonly string challengeLayout_2_1_2 = "2223c491-5edb-4c4b-84d2-d2ec0362487d";

	public static readonly string challengeLayout_2_2_1 = "4cc06233-b810-4164-a940-2903d05c8bb6";

	public static readonly string challengeLayout_2_2_2 = "0fe9d34f-7bf5-41ea-8ecc-65908e4c703f";

	public static readonly string challengeLayout_2_3_1 = "4cc06233-b810-4164-a940-2903d05c8bb6";

	public static readonly string challengeLayout_2_3_2 = "8b84cacb-a781-407f-b59b-b58b1971caee";

	public Dictionary<string, BloxelBoard> boardPool = new Dictionary<string, BloxelBoard>();

	public Dictionary<string, BloxelBrain> brainPool = new Dictionary<string, BloxelBrain>();

	public Dictionary<string, BloxelAnimation> animationPool = new Dictionary<string, BloxelAnimation>();

	public Dictionary<string, BloxelMegaBoard> megaBoardPool = new Dictionary<string, BloxelMegaBoard>();

	public Dictionary<string, BloxelCharacter> characterPool = new Dictionary<string, BloxelCharacter>();

	public Dictionary<string, BloxelLevel> levelPool = new Dictionary<string, BloxelLevel>();

	public Dictionary<string, BloxelGame> gamePool = new Dictionary<string, BloxelGame>();

	public Dictionary<AnimationType, BloxelAnimation> defaultCharacterAnimations = new Dictionary<AnimationType, BloxelAnimation>(3);

	public Dictionary<Color, PaletteColor> colorToPaletteColor = new Dictionary<Color, PaletteColor>(64);

	public List<BloxelLevel> borkedLevels = new List<BloxelLevel>();

	public List<BloxelGame> borkedGames = new List<BloxelGame>();

	public Color[] orderedPalette = new Color[64];

	public static BloxelBrain[] globalBrains = new BloxelBrain[6];

	public static BloxelBoard bloxelBoardConstant;

	public BloxelBoard[] wireframeTemplates;

	public BloxelCharacter[] characterTemplates;

	public BloxelMegaBoard[] themeBackgrounds;

	public BloxelGame characterPreviewGame;

	public BloxelGame homeMiniGame;

	public BloxelGame poultryPanicGame;

	public Dictionary<BlockColor, BloxelProject>[] themes;

	public static bool initComplete = false;

	public bool templatesLoaded;

	public static string baseStoragePath;

	public static string streamingAssetsPath;

	public string defaultCharacterID;

	[Header("Colors")]
	public Color bloxelOrange;

	public Color bloxelYellow;

	public Color bloxelGreen;

	public Color bloxelBlue;

	public Color bloxelPurple;

	public Color bloxelPink;

	public Color bloxelRed;

	public static Color clearColor = Color.clear;

	public Color[] unityColorsForBlockColors;

	public static Color32[] UnityLookupColorsForBlockColors;

	[Header("Textures")]
	public Texture2D boardSolidOrange;

	public Texture2D boardSolidYellow;

	public Texture2D boardSolidGreen;

	public Texture2D boardSolidBlue;

	public Texture2D boardSolidPurple;

	public Texture2D boardSolidPink;

	public Texture2D boardSolidRed;

	public Texture2D boardSolidWhite;

	public Texture2D boardSolidBlank;

	public Texture2D boardSolidClear;

	[Header("Color Arrays")]
	public Color32[] pixelBuffer13x13orange;

	public Color32[] pixelBuffer13x13yellow;

	public Color32[] pixelBuffer13x13green;

	public Color32[] pixelBuffer13x13blue;

	public Color32[] pixelBuffer13x13purple;

	public Color32[] pixelBuffer13x13pink;

	public Color32[] pixelBuffer13x13red;

	public Color32[] pixelBuffer13x13white;

	public Color32[] pixelBuffer13x13blank;

	public Color[] clearColors38x38;

	public Color[] clearColors13x13;

	public Color[] clearColors169x169;

	public Color32[] clearDetailLookupColors32;

	public Color[] colorBuffer13x13;

	public static Color32[] ColorBuffer32 = new Color32[169];

	public static List<string> availableTags = new List<string>();

	private List<DirectoryInfo> directoriesWithMissingProjects = new List<DirectoryInfo>();

	[Header("| ========= UI Icons ========= |")]
	public Sprite iconCheckmark;

	public Sprite iconAdd;

	public Sprite iconNull;

	public Sprite iconGames;

	public Sprite iconHome;

	public Sprite iconInfinityWall;

	public Sprite iconEditor;

	public Sprite iconLock;

	public Sprite iconToggleBtnActive;

	public Sprite iconToggleBtnInActive;

	[Header("| ========= Badge Sprites ========= |")]
	public Sprite gameBuilderBadgeEasy;

	public Sprite heroBuilderBadgeEasy;

	public float millisecondsPerChar;

	private BatchBoardJob _batchBoardJob;

	private BatchGameJob _batchGameJob;

	private BatchCharacterJob _batchCharacterJob;

	private BatchAnimationJob _batchAnimationJob;

	private BatchMegaJob _batchMegaJob;

	private BatchLevelJob _batchLevelJob;

	private CharacterParserJob _characterParseJob;

	private BoardParserJob _boardParseJob;

	private AnimationParserJob _animationParseJob;

	public int totalBoardsOnDisk;

	public int boardsLoaded;

	public bool superSimpleManager;

	public static bool SetupComplete = false;

	public event HandleInit OnInit;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			millisecondsPerChar = 0f;
			streamingAssetsPath = Application.streamingAssetsPath;
			BloxelEnemyTile.enemyPlatformMask = LayerMask.GetMask("Default", "Ground", "Destructible");
			templatesLoaded = false;
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void Setup()
	{
		SetBaseStoragePath();
		CloudManager.Instance.SetupSyncFile();
		if (superSimpleManager)
		{
			SetupComplete = true;
			return;
		}
		if (!initComplete)
		{
			GetBrainsFromDisk();
			CleanLevels();
			CleanProject(ProjectType.Game);
			CleanProject(ProjectType.Animation);
			CleanProject(ProjectType.MegaBoard);
			CleanProject(ProjectType.Character);
			CleanProject(ProjectType.Board);
			CreateStandardAssets();
			if (!PrefsManager.instance.hasInstalledDefaultAssetsV1)
			{
				if (InstallAssets())
				{
					Init();
				}
			}
			else
			{
				Init();
			}
		}
		templatesLoaded = false;
		SetupComplete = true;
	}

	public string SetBaseStoragePath()
	{
		baseStoragePath = Application.persistentDataPath + CurrentUser.instance.directoryPath;
		return baseStoragePath;
	}

	private void CreateStandardAssets()
	{
		bloxelOrange = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange);
		bloxelYellow = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Yellow);
		bloxelGreen = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green);
		bloxelBlue = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue);
		bloxelPurple = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Purple);
		bloxelPink = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Pink);
		bloxelRed = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
		boardSolidOrange = BloxelBoard.GetSolidBoard(bloxelOrange);
		boardSolidYellow = BloxelBoard.GetSolidBoard(bloxelYellow);
		boardSolidGreen = BloxelBoard.GetSolidBoard(bloxelGreen);
		boardSolidBlue = BloxelBoard.GetSolidBoard(bloxelBlue);
		boardSolidPurple = BloxelBoard.GetSolidBoard(bloxelPurple);
		boardSolidPink = BloxelBoard.GetSolidBoard(bloxelPink);
		boardSolidRed = BloxelBoard.GetSolidBoard(bloxelRed);
		boardSolidBlank = BloxelBoard.GetSolidBoard(BloxelBoard.baseUIBoardColor);
		boardSolidClear = BloxelBoard.GetSolidBoard(Color.clear);
		boardSolidWhite = BloxelBoard.GetSolidBoard(Color.white);
		InitPixelBuffer(ref pixelBuffer13x13orange, bloxelOrange);
		InitPixelBuffer(ref pixelBuffer13x13yellow, bloxelYellow);
		InitPixelBuffer(ref pixelBuffer13x13green, bloxelGreen);
		InitPixelBuffer(ref pixelBuffer13x13blue, bloxelBlue);
		InitPixelBuffer(ref pixelBuffer13x13purple, bloxelPurple);
		InitPixelBuffer(ref pixelBuffer13x13pink, bloxelPink);
		InitPixelBuffer(ref pixelBuffer13x13red, bloxelRed);
		InitPixelBuffer(ref pixelBuffer13x13white, Color.white);
		InitPixelBuffer(ref pixelBuffer13x13blank, BloxelBoard.baseUIBoardColor32);
		clearColors13x13 = new Color[169];
		clearColors38x38 = new Color[1444];
		clearColors169x169 = new Color[28561];
		clearDetailLookupColors32 = new Color32[28561];
		ResetColorBuffer(clearColors13x13, Color.clear);
		ResetColorBuffer(clearColors38x38, Color.clear);
		ResetColorBuffer(clearColors169x169, Color.clear);
		ResetColorBuffer(clearDetailLookupColors32, BloxelBoard.TransparentLookupColor32);
		colorBuffer13x13 = new Color[169];
		unityColorsForBlockColors = new Color[9]
		{
			bloxelRed,
			bloxelOrange,
			bloxelYellow,
			bloxelGreen,
			bloxelBlue,
			bloxelPurple,
			bloxelPink,
			Color.white,
			Color.clear
		};
		UnityLookupColorsForBlockColors = new Color32[9]
		{
			new Color32(0, 0, 0, 56),
			new Color32(0, 0, 0, 57),
			new Color32(0, 0, 0, 58),
			new Color32(0, 0, 0, 59),
			new Color32(0, 0, 0, 60),
			new Color32(0, 0, 0, 61),
			new Color32(0, 0, 0, 62),
			new Color32(0, 0, 0, 63),
			BloxelBoard.BaseBoardDetailLookupColor32
		};
		PaletteColor[] array = Enum.GetValues(typeof(PaletteColor)) as PaletteColor[];
		for (int i = 0; i < array.Length; i++)
		{
			orderedPalette[i] = ColorUtilities.PaletteColorToUnityColor(array[i]);
			colorToPaletteColor.Add(orderedPalette[i], array[i]);
		}
	}

	private void CleanLevels()
	{
		DirectoryInfo[] savedDirectories = BloxelLevel.GetSavedDirectories();
		for (int i = 0; i < savedDirectories.Length; i++)
		{
			BloxelLevel bloxelLevel = null;
			if (File.Exists(savedDirectories[i].FullName + "/" + BloxelLevel.filename))
			{
				string jsonString = File.ReadAllText(savedDirectories[i].FullName + "/" + BloxelLevel.filename);
				string contents = CleanText(jsonString);
				File.WriteAllText(savedDirectories[i].FullName + "/" + BloxelLevel.filename, contents);
			}
		}
	}

	private void CleanProject(ProjectType type)
	{
		DirectoryInfo[] array = null;
		string text = string.Empty;
		switch (type)
		{
		case ProjectType.Board:
			array = BloxelBoard.GetSavedBoards();
			text = BloxelBoard.filename;
			break;
		case ProjectType.Animation:
			array = BloxelAnimation.GetSavedDirectories();
			text = BloxelAnimation.filename;
			break;
		case ProjectType.Character:
			array = BloxelCharacter.GetSavedProjects();
			text = BloxelCharacter.filename;
			break;
		case ProjectType.MegaBoard:
			array = BloxelMegaBoard.GetSavedProjects();
			text = BloxelMegaBoard.filename;
			break;
		case ProjectType.Game:
			array = BloxelGame.GetSavedDirectories();
			text = BloxelGame.filename;
			break;
		}
		for (int i = 0; i < array.Length; i++)
		{
			if (File.Exists(array[i].FullName + "/" + text))
			{
				string jsonString = File.ReadAllText(array[i].FullName + "/" + text);
				bool isFunkedUp = false;
				string contents = CleanTitle(jsonString, out isFunkedUp);
				if (isFunkedUp)
				{
				}
				File.WriteAllText(array[i].FullName + "/" + text, contents);
			}
		}
	}

	private string CleanTitle(string jsonString, out bool isFunkedUp)
	{
		isFunkedUp = false;
		char[] array = jsonString.ToCharArray();
		string text = "title\":";
		string value = "\",\"owner";
		int num = jsonString.IndexOf(value);
		int i = 0;
		while ((i = jsonString.IndexOf(text, i)) >= 0)
		{
			i += text.Length + 1;
			char c = array[i];
			for (; i < num; i++)
			{
				switch (array[i])
				{
				case '"':
					isFunkedUp = true;
					array[i] = '“';
					break;
				case '\\':
					isFunkedUp = true;
					array[i] = '/';
					break;
				}
			}
		}
		return new string(array);
	}

	private string CleanText(string jsonString)
	{
		char[] array = jsonString.ToCharArray();
		string text = "text\":";
		int length = jsonString.Length;
		int num = 0;
		while ((num = jsonString.IndexOf(text, num)) >= 0)
		{
			num += text.Length + 1;
			char c = array[num];
			while (c != '}' && num < length)
			{
				c = array[num];
				if (c == '"' && array[num + 1] != '}')
				{
					array[num] = '“';
				}
				else if (c == '\\' && array[num + 1] != '}')
				{
					array[num] = '/';
				}
				num++;
			}
		}
		return new string(array);
	}

	public BloxelProject GetProjectFromPool(ProjectType type, string id)
	{
		switch (type)
		{
		case ProjectType.Board:
		{
			BloxelBoard value6 = null;
			if (instance.boardPool.TryGetValue(id, out value6))
			{
				return value6;
			}
			break;
		}
		case ProjectType.Animation:
		{
			BloxelAnimation value2 = null;
			if (instance.animationPool.TryGetValue(id, out value2))
			{
				return value2;
			}
			break;
		}
		case ProjectType.Character:
		{
			BloxelCharacter value4 = null;
			if (instance.characterPool.TryGetValue(id, out value4))
			{
				return value4;
			}
			break;
		}
		case ProjectType.Brain:
		{
			BloxelBrain value7 = null;
			if (instance.brainPool.TryGetValue(id, out value7))
			{
				return value7;
			}
			break;
		}
		case ProjectType.MegaBoard:
		{
			BloxelMegaBoard value5 = null;
			if (instance.megaBoardPool.TryGetValue(id, out value5))
			{
				return value5;
			}
			break;
		}
		case ProjectType.Level:
		{
			BloxelLevel value3 = null;
			if (instance.levelPool.TryGetValue(id, out value3))
			{
				return value3;
			}
			break;
		}
		case ProjectType.Game:
		{
			BloxelGame value = null;
			if (instance.gamePool.TryGetValue(id, out value))
			{
				return value;
			}
			break;
		}
		}
		return null;
	}

	private void GetBrainsFromDisk()
	{
		string[] globalBrainIds = PrefsManager.instance.GetGlobalBrainIds();
		for (int i = 0; i < globalBrainIds.Length; i++)
		{
			if (new Guid(globalBrainIds[i]) != Guid.Empty)
			{
				string path = baseStoragePath + BloxelBrain.rootDirectory + "/" + BloxelBrain.subDirectory + "/" + globalBrainIds[i] + "/" + BloxelBrain.filename;
				if (File.Exists(path))
				{
					globalBrains[i] = BloxelBrain.GetSavedBrainById(globalBrainIds[i]);
				}
			}
		}
	}

	private void InitPixelBuffer(ref Color32[] colorBuffer, Color32 color)
	{
		colorBuffer = new Color32[169];
		int num = colorBuffer.Length;
		for (int i = 0; i < num; i++)
		{
			colorBuffer[i] = color;
		}
	}

	public void ResetColorBuffer(Color[] colorBuffer, Color clearColor)
	{
		for (int i = 0; i < colorBuffer.Length; i++)
		{
			colorBuffer[i] = clearColor;
		}
	}

	public void ResetColorBuffer(Color32[] colorBuffer, Color32 clearColor)
	{
		for (int i = 0; i < colorBuffer.Length; i++)
		{
			colorBuffer[i] = clearColor;
		}
	}

	public bool InstallAssets()
	{
		bool flag = false;
		string text = streamingAssetsPath + "/DefaultAssets.zip";
		string persistentDataPath = Application.persistentDataPath;
		string path = persistentDataPath + "/__MACOSX";
		if (Application.platform == RuntimePlatform.Android)
		{
			WWW wWW = new WWW(text);
			while (!wWW.isDone)
			{
			}
			Zipper.ExtractFromStream(wWW.bytes, persistentDataPath);
		}
		else
		{
			Zipper.Extract(text, persistentDataPath);
		}
		if (Directory.Exists(persistentDataPath + "/" + BloxelAnimation.rootDirectory))
		{
			if (Directory.Exists(path))
			{
				Directory.Delete(path, true);
			}
			flag = true;
		}
		if (flag)
		{
			PrefsManager.instance.hasInstalledDefaultAssetsV1 = true;
			return true;
		}
		return false;
	}

	private void Init()
	{
		SetupBoardPool();
	}

	public void ReInitPools()
	{
		templatesLoaded = false;
		initComplete = false;
		CleanLevels();
		for (int i = 0; i < themes.Length; i++)
		{
			themes[i].Clear();
		}
		defaultCharacterAnimations.Clear();
		gamePool.Clear();
		levelPool.Clear();
		brainPool.Clear();
		animationPool.Clear();
		boardPool.Clear();
		characterPool.Clear();
		megaBoardPool.Clear();
		SetupBoardPool();
	}

	private void SetupBoardPool()
	{
		_batchBoardJob = new BatchBoardJob();
		_batchBoardJob.OnComplete += BoardParseComplete;
		_batchBoardJob.Start();
		StartCoroutine(_batchBoardJob.WaitFor());
	}

	private void SetupBrainPool()
	{
		BatchBrainJob batchBrainJob = new BatchBrainJob();
		batchBrainJob.OnComplete += BrainParseComplete;
		batchBrainJob.Start();
		StartCoroutine(batchBrainJob.WaitFor());
	}

	private void SetupAnimationPool()
	{
		_batchAnimationJob = new BatchAnimationJob();
		_batchAnimationJob.OnComplete += AnimationJobComplete;
		_batchAnimationJob.Start();
		StartCoroutine(_batchAnimationJob.WaitFor());
	}

	private void SetupMegaBoardPool()
	{
		_batchMegaJob = new BatchMegaJob();
		_batchMegaJob.OnComplete += MegaJobComplete;
		_batchMegaJob.Start();
		StartCoroutine(_batchMegaJob.WaitFor());
	}

	private void SetupCharacterPool()
	{
		_batchCharacterJob = new BatchCharacterJob();
		_batchCharacterJob.OnComplete += CharacterJobComplete;
		_batchCharacterJob.Start();
		StartCoroutine(_batchCharacterJob.WaitFor());
	}

	private void SetupLevelPool()
	{
		_batchLevelJob = new BatchLevelJob();
		_batchLevelJob.OnComplete += LevelJobComplete;
		_batchLevelJob.Start();
		StartCoroutine(_batchLevelJob.WaitFor());
	}

	private void SetupGamePool()
	{
		_batchGameJob = new BatchGameJob();
		_batchGameJob.OnComplete += GameJobComplete;
		_batchGameJob.Start();
		StartCoroutine(_batchGameJob.WaitFor());
	}

	private void BoardParseComplete(Dictionary<string, BloxelBoard> pool, List<string> tags)
	{
		boardPool = pool;
		AddTags(tags);
		SetupBrainPool();
	}

	private void BrainParseComplete(Dictionary<string, BloxelBrain> pool, List<string> tags)
	{
		brainPool = pool;
		AddTags(tags);
		SetupAnimationPool();
	}

	private void AnimationJobComplete(Dictionary<string, BloxelAnimation> pool, List<string> tags, List<BloxelBoard> _addedBoards)
	{
		for (int i = 0; i < _addedBoards.Count; i++)
		{
			boardPool.Add(_addedBoards[i].ID(), _addedBoards[i]);
		}
		animationPool = pool;
		AddTags(tags);
		SetupMegaBoardPool();
	}

	private void MegaJobComplete(Dictionary<string, BloxelMegaBoard> pool, List<string> tags)
	{
		megaBoardPool = pool;
		AddTags(tags);
		SetupCharacterPool();
	}

	private void CharacterJobComplete(Dictionary<string, BloxelCharacter> pool, List<string> tags)
	{
		characterPool = pool;
		AddTags(tags);
		GetDefaultCharacter();
	}

	private void LevelJobComplete(Dictionary<string, BloxelLevel> pool, Dictionary<string, BloxelBoard> _extraBoards, List<BloxelLevel> _borked)
	{
		levelPool = pool;
		Dictionary<string, BloxelBoard>.Enumerator enumerator = _extraBoards.GetEnumerator();
		while (enumerator.MoveNext())
		{
			boardPool.Add(enumerator.Current.Key, enumerator.Current.Value);
		}
		SetupGamePool();
	}

	private void GameJobComplete(Dictionary<string, BloxelGame> pool, List<string> _tags)
	{
		gamePool = pool;
		AddTags(_tags);
		for (int i = 0; i < borkedGames.Count; i++)
		{
			if (borkedGames[i].coverBoard == null)
			{
				if (borkedGames[i].levels.Count > 0)
				{
					borkedGames[i].coverBoard = borkedGames[i].levels.First().Value.coverBoard;
					continue;
				}
				gamePool.Remove(borkedGames[i].ID());
				borkedGames[i].Delete();
			}
		}
		StartCoroutine(SetupThemesAndTemplates());
	}

	private void AddTags(List<string> _tags)
	{
		for (int i = 0; i < _tags.Count; i++)
		{
			if (!availableTags.Contains(_tags[i]) && (_tags[i] != "PPHide" || _tags[i] != "PixelTutz"))
			{
				availableTags.Add(_tags[i]);
			}
		}
	}

	private IEnumerator SetupThemesAndTemplates()
	{
		if (!templatesLoaded)
		{
			yield return StartCoroutine(SetupLayoutTemplates());
			yield return StartCoroutine(SetupCharacterTemplates());
			yield return StartCoroutine(SetupThemeTemplates());
			yield return StartCoroutine(SetupBackgroundThemes());
			yield return StartCoroutine(SetupHomeMiniGame());
			yield return StartCoroutine(SetupPoultryPanic());
		}
	}

	private void InitComplete()
	{
		if (CurrentUser.instance.active && !PrefsManager.instance.hasRunInitialS3Sync)
		{
			BigOldHonkinSync();
		}
		PrefsManager.instance.hasRunInitialS3Sync = true;
		templatesLoaded = true;
		initComplete = true;
		if (this.OnInit != null)
		{
			this.OnInit();
		}
	}

	private string GetContents(string filePath)
	{
		string empty = string.Empty;
		if (Application.platform != RuntimePlatform.Android)
		{
			return File.ReadAllText(filePath);
		}
		WWW wWW = new WWW(filePath);
		while (!wWW.isDone)
		{
		}
		return wWW.text;
	}

	private string[] GetFilePaths(string pathToFiles, string ext = "json", bool themes = false)
	{
		if (Application.platform != RuntimePlatform.Android)
		{
			string path = streamingAssetsPath + pathToFiles;
			return Directory.GetFiles(path, "*." + ext);
		}
		List<string> list = new List<string>();
		if (!themes)
		{
			int num = 8;
			string text = streamingAssetsPath + pathToFiles;
			for (int i = 1; i < num; i++)
			{
				WWW wWW = new WWW(text + i + "." + ext);
				while (!wWW.isDone)
				{
				}
				if (wWW.error == null && !string.IsNullOrEmpty(wWW.text))
				{
					list.Add(wWW.url);
					continue;
				}
				break;
			}
		}
		else
		{
			string[] array = new string[2] { "board", "anim" };
			BlockColor[] array2 = Enum.GetValues(typeof(BlockColor)) as BlockColor[];
			for (int j = 0; j < array.Length; j++)
			{
				for (int k = 0; k < array2.Length; k++)
				{
					string url = streamingAssetsPath + pathToFiles + array[j].ToString() + "_" + array2[k].ToString().ToLower() + "." + ext;
					WWW wWW2 = new WWW(url);
					while (!wWW2.isDone)
					{
					}
					if (wWW2.error == null && !string.IsNullOrEmpty(wWW2.text))
					{
						list.Add(wWW2.url);
					}
				}
			}
		}
		return list.ToArray();
	}

	public IEnumerator SetupHomeMiniGame()
	{
		string filePath = streamingAssetsPath + "/homeMiniGame.json";
		if (Application.platform == RuntimePlatform.Android)
		{
			yield return StartCoroutine(GetJSONFromAndroidFileSystem(filePath, delegate(string jsonString)
			{
				if (!string.IsNullOrEmpty(jsonString) && jsonString != "ERROR")
				{
					StartCoroutine(ParseHomeMiniGame(jsonString));
				}
			}));
		}
		else if (File.Exists(filePath))
		{
			yield return StartCoroutine(ParseHomeMiniGame(File.ReadAllText(filePath)));
		}
	}

	private IEnumerator ParseHomeMiniGame(string jsonString)
	{
		GameParserJob job = new GameParserJob();
		job.responseFromServer = jsonString;
		job.OnComplete += delegate(BloxelGame _game)
		{
			homeMiniGame = _game;
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
	}

	public IEnumerator SetupPoultryPanic()
	{
		string filePath = streamingAssetsPath + "/poultryPanic.json";
		if (Application.platform == RuntimePlatform.Android)
		{
			yield return StartCoroutine(GetJSONFromAndroidFileSystem(filePath, delegate(string jsonString)
			{
				if (!string.IsNullOrEmpty(jsonString) && jsonString != "ERROR")
				{
					StartCoroutine(ParsePoultryPanic(jsonString));
				}
			}));
		}
		else if (File.Exists(filePath))
		{
			yield return StartCoroutine(ParsePoultryPanic(File.ReadAllText(filePath)));
		}
	}

	private IEnumerator ParsePoultryPanic(string jsonString)
	{
		GameParserJob job = new GameParserJob();
		job.responseFromServer = jsonString;
		job.OnComplete += delegate(BloxelGame _game)
		{
			poultryPanicGame = _game;
			InitComplete();
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
	}

	private IEnumerator SetupLayoutTemplates()
	{
		string path = "/templates/wireframes/";
		string[] templates = GetFilePaths(path);
		wireframeTemplates = new BloxelBoard[templates.Length];
		for (int i = 0; i < templates.Length; i++)
		{
			string fileContents = GetContents(templates[i]);
			yield return StartCoroutine(BuildLayoutTemplate(fileContents, i));
		}
	}

	private IEnumerator BuildLayoutTemplate(string jsonString, int idx)
	{
		BoardParserJob job = new BoardParserJob();
		job.responseFromServer = jsonString;
		job.OnComplete += delegate(BloxelBoard _board)
		{
			wireframeTemplates[idx] = _board;
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
	}

	private IEnumerator SetupCharacterTemplates()
	{
		string path = "/templates/characters/";
		string[] templates = GetFilePaths(path);
		characterTemplates = new BloxelCharacter[templates.Length];
		for (int i = 0; i < templates.Length; i++)
		{
			string fileContents = GetContents(templates[i]);
			yield return StartCoroutine(BuildCharacterTemplate(fileContents, i));
		}
	}

	private IEnumerator BuildCharacterTemplate(string jsonString, int idx)
	{
		CharacterParserJob job = new CharacterParserJob();
		job.responseFromServer = jsonString;
		job.OnComplete += delegate(BloxelCharacter _character)
		{
			characterTemplates[idx] = _character;
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
	}

	private IEnumerator SetupBackgroundThemes()
	{
		string path = "/templates/backgrounds/";
		string[] templates = GetFilePaths(path);
		themeBackgrounds = new BloxelMegaBoard[templates.Length];
		for (int i = 0; i < templates.Length; i++)
		{
			string fileContents = GetContents(templates[i]);
			yield return StartCoroutine(BuildThemeBackground(fileContents, i));
		}
	}

	private IEnumerator BuildThemeBackground(string jsonString, int idx)
	{
		MegaBoardParserJob job = new MegaBoardParserJob();
		job.responseFromServer = jsonString;
		job.OnComplete += delegate(BloxelMegaBoard _megaBoard)
		{
			themeBackgrounds[idx] = _megaBoard;
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
	}

	private IEnumerator SetupThemeTemplates()
	{
		ThemeType[] themeTypes = Enum.GetValues(typeof(ThemeType)) as ThemeType[];
		Dictionary<ThemeType, string[]> filePaths = new Dictionary<ThemeType, string[]>();
		Dictionary<ThemeType, List<string>> jsonLookup = new Dictionary<ThemeType, List<string>>
		{
			{
				ThemeType.Ice,
				new List<string>()
			},
			{
				ThemeType.Safari,
				new List<string>()
			},
			{
				ThemeType.Toxic,
				new List<string>()
			},
			{
				ThemeType.SkatePark,
				new List<string>()
			}
		};
		for (int i = 0; i < themeTypes.Length; i++)
		{
			string text = themeTypes[i].ToString();
			string text2 = streamingAssetsPath + "/themes/" + text + "/";
			filePaths[themeTypes[i]] = GetFilePaths("/themes/" + text + "/", "json", true);
			for (int j = 0; j < filePaths[themeTypes[i]].Length; j++)
			{
				jsonLookup[themeTypes[i]].Add(GetContents(filePaths[themeTypes[i]][j]));
			}
		}
		ThemeBuilderJob job = new ThemeBuilderJob();
		job.templates = filePaths;
		job.jsonLookup = jsonLookup;
		job.OnComplete += delegate(Dictionary<BlockColor, BloxelProject>[] _themes)
		{
			themes = _themes;
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
	}

	private Dictionary<BlockColor, BloxelProject> ThemeDictionaryInit(ThemeType type)
	{
		Dictionary<BlockColor, BloxelProject> dictionary = new Dictionary<BlockColor, BloxelProject>();
		string text = type.ToString();
		string text2 = streamingAssetsPath + "/themes/" + text + "/";
		string[] filePaths = GetFilePaths("/themes/" + text + "/", "json", true);
		for (int i = 0; i < filePaths.Length; i++)
		{
			string contents = GetContents(filePaths[i]);
			string[] array = filePaths[i].Split('/');
			string text3 = array[array.Length - 1];
			string[] array2 = text3.Split('_');
			BloxelProject value = null;
			switch (array2[0])
			{
			case "board":
			{
				BloxelBoard bloxelBoard = new BloxelBoard(contents, DataSource.JSONString);
				value = bloxelBoard;
				break;
			}
			case "anim":
			{
				BloxelAnimation bloxelAnimation = new BloxelAnimation(contents, DataSource.JSONString);
				value = bloxelAnimation;
				break;
			}
			}
			switch (array2[1])
			{
			case "red.json":
				dictionary[BlockColor.Red] = value;
				break;
			case "orange.json":
				dictionary[BlockColor.Orange] = value;
				break;
			case "yellow.json":
				dictionary[BlockColor.Yellow] = value;
				break;
			case "green.json":
				dictionary[BlockColor.Green] = value;
				break;
			case "blue.json":
				dictionary[BlockColor.Blue] = value;
				break;
			case "purple.json":
				dictionary[BlockColor.Purple] = value;
				break;
			case "pink.json":
				dictionary[BlockColor.Pink] = value;
				break;
			case "white.json":
				dictionary[BlockColor.White] = value;
				break;
			}
		}
		return dictionary;
	}

	private void GetPreviewGame()
	{
		string text = streamingAssetsPath + "/characterPreview.json";
		if (Application.platform == RuntimePlatform.Android)
		{
			StartCoroutine(GetJSONFromAndroidFileSystem(text, delegate(string jsonString)
			{
				if (!string.IsNullOrEmpty(jsonString) && jsonString != "ERROR")
				{
					StartCoroutine(ParsePreviewGame(jsonString));
				}
			}));
		}
		else if (File.Exists(text))
		{
			StartCoroutine(ParsePreviewGame(File.ReadAllText(text)));
		}
	}

	private IEnumerator ParsePreviewGame(string jsonString)
	{
		GameParserJob job = new GameParserJob();
		job.responseFromServer = jsonString;
		job.OnComplete += delegate(BloxelGame _game)
		{
			characterPreviewGame = _game;
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
		SetupLevelPool();
	}

	private IEnumerator SetupDefaultCharacterAnimations(string jsonString)
	{
		CharacterParserJob job = new CharacterParserJob();
		job.responseFromServer = jsonString;
		job.OnComplete += delegate(BloxelCharacter character)
		{
			defaultCharacterAnimations.Add(AnimationType.Idle, character.animations[AnimationType.Idle]);
			defaultCharacterAnimations.Add(AnimationType.Walk, character.animations[AnimationType.Walk]);
			defaultCharacterAnimations.Add(AnimationType.Jump, character.animations[AnimationType.Jump]);
			character.AddTag("PixelTutz");
			character.Save();
			if (!characterPool.ContainsKey(character.ID()))
			{
				characterPool.Add(character.ID(), character);
			}
			defaultCharacterID = character.ID();
			GetPreviewGame();
		};
		job.Start();
		yield return StartCoroutine(job.WaitFor());
	}

	private void GetDefaultCharacter()
	{
		bool flag = false;
		string text = streamingAssetsPath + "/defaultCharacter.json";
		if (Application.platform == RuntimePlatform.Android)
		{
			StartCoroutine(GetJSONFromAndroidFileSystem(text, delegate(string response)
			{
				if (!string.IsNullOrEmpty(response) && response != "ERROR")
				{
					StartCoroutine(SetupDefaultCharacterAnimations(response));
				}
			}));
		}
		else if (File.Exists(text))
		{
			StartCoroutine(SetupDefaultCharacterAnimations(File.ReadAllText(text)));
		}
	}

	private IEnumerator GetJSONFromAndroidFileSystem(string filePath, Action<string> callback)
	{
		WWW reader = new WWW(filePath);
		yield return reader;
		while (!reader.isDone)
		{
		}
		if (reader.error == null && !string.IsNullOrEmpty(reader.text))
		{
			callback(reader.text);
		}
		else
		{
			callback("ERROR");
		}
	}

	private void BigOldHonkinSync()
	{
		CloudManager.Instance.addToQueueOverride = true;
		Dictionary<string, BloxelBoard>.Enumerator enumerator = boardPool.GetEnumerator();
		Dictionary<string, BloxelAnimation>.Enumerator enumerator2 = animationPool.GetEnumerator();
		Dictionary<string, BloxelCharacter>.Enumerator enumerator3 = characterPool.GetEnumerator();
		Dictionary<string, BloxelBrain>.Enumerator enumerator4 = brainPool.GetEnumerator();
		Dictionary<string, BloxelMegaBoard>.Enumerator enumerator5 = megaBoardPool.GetEnumerator();
		Dictionary<string, BloxelLevel>.Enumerator enumerator6 = levelPool.GetEnumerator();
		Dictionary<string, BloxelGame>.Enumerator enumerator7 = gamePool.GetEnumerator();
		List<ProjectSyncInfo> list = new List<ProjectSyncInfo>();
		while (enumerator.MoveNext())
		{
			list.Add(enumerator.Current.Value.syncInfo);
		}
		while (enumerator2.MoveNext())
		{
			list.Add(enumerator2.Current.Value.syncInfo);
		}
		while (enumerator3.MoveNext())
		{
			list.Add(enumerator3.Current.Value.syncInfo);
		}
		while (enumerator4.MoveNext())
		{
			list.Add(enumerator4.Current.Value.syncInfo);
		}
		while (enumerator5.MoveNext())
		{
			list.Add(enumerator5.Current.Value.syncInfo);
		}
		while (enumerator6.MoveNext())
		{
			list.Add(enumerator6.Current.Value.syncInfo);
		}
		while (enumerator7.MoveNext())
		{
			list.Add(enumerator7.Current.Value.syncInfo);
		}
		ProjectSyncInfo[] array = list.ToArray();
		for (int i = 0; i < array.Length; i++)
		{
			CloudManager.Instance.EnqueueFileForSync(array[i], false);
		}
		CloudManager.Instance.WriteSyncFile(array);
		PrefsManager.instance.hasRunInitialS3Sync = true;
		CloudManager.Instance.addToQueueOverride = false;
	}

	public BloxelProject[] GetLibrary()
	{
		Dictionary<string, BloxelBoard>.Enumerator enumerator = boardPool.GetEnumerator();
		Dictionary<string, BloxelAnimation>.Enumerator enumerator2 = animationPool.GetEnumerator();
		Dictionary<string, BloxelCharacter>.Enumerator enumerator3 = characterPool.GetEnumerator();
		Dictionary<string, BloxelBrain>.Enumerator enumerator4 = brainPool.GetEnumerator();
		Dictionary<string, BloxelMegaBoard>.Enumerator enumerator5 = megaBoardPool.GetEnumerator();
		Dictionary<string, BloxelLevel>.Enumerator enumerator6 = levelPool.GetEnumerator();
		Dictionary<string, BloxelGame>.Enumerator enumerator7 = gamePool.GetEnumerator();
		List<BloxelProject> list = new List<BloxelProject>();
		while (enumerator.MoveNext())
		{
			list.Add(enumerator.Current.Value);
		}
		while (enumerator2.MoveNext())
		{
			list.Add(enumerator2.Current.Value);
		}
		while (enumerator3.MoveNext())
		{
			list.Add(enumerator3.Current.Value);
		}
		while (enumerator4.MoveNext())
		{
			list.Add(enumerator4.Current.Value);
		}
		while (enumerator5.MoveNext())
		{
			list.Add(enumerator5.Current.Value);
		}
		while (enumerator6.MoveNext())
		{
			list.Add(enumerator6.Current.Value);
		}
		while (enumerator7.MoveNext())
		{
			list.Add(enumerator7.Current.Value);
		}
		return list.ToArray();
	}
}
