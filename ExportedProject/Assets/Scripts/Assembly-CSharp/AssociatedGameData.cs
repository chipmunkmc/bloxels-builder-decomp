using System;

[Serializable]
public struct AssociatedGameData
{
	public bool ugcApproved;

	public int plays;

	public string[] levels;
}
