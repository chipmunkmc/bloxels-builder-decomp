using System;

[Serializable]
public struct AssociatedSquareData
{
	public string title;

	public AssociatedGameData game;
}
