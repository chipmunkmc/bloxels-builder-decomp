using System;
using UnityEngine;
using UnityEngine.EventSystems;
using VoxelEngine;

public class BackgroundEditorCanvas : MonoBehaviour, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	private int numberOfTiles = 169;

	private bool[] changedSpaces;

	private bool isDragging;

	public BloxelBoard currentBloxelBoard;

	private void Awake()
	{
		changedSpaces = new bool[169];
	}

	private void Start()
	{
		BloxelBoardLibrary.instance.OnProjectSelect += HandleOnBoardChange;
	}

	private void OnEnable()
	{
		if (BloxelBoardLibrary.instance.currentSavedBoard != null)
		{
			currentBloxelBoard = BloxelBoardLibrary.instance.currentSavedBoard;
		}
		else
		{
			currentBloxelBoard = BloxelLibraryScrollController.instance.unfilteredBoardData[0];
		}
	}

	private void HandleOnBoardChange(SavedProject project)
	{
		currentBloxelBoard = project.dataModel as BloxelBoard;
		for (int i = 0; i < GameBuilderCanvas.instance.decorateTools.tools.Length; i++)
		{
			UIDecorateTool uIDecorateTool = GameBuilderCanvas.instance.decorateTools.tools[i];
			uIDecorateTool.DeActivate();
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (GameBuilderCanvas.instance.decorateTools.currentTool == null)
		{
			Array.Clear(changedSpaces, 0, changedSpaces.Length);
			PaintBoardIntoBackground();
		}
		else
		{
			EraseBoardFromBackground();
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		isDragging = true;
		if (GameBuilderCanvas.instance.decorateTools.currentTool == null)
		{
			PaintBoardIntoBackground();
		}
		else
		{
			EraseBoardFromBackground();
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (GameBuilderCanvas.instance.decorateTools.currentTool == null)
		{
			PaintBoardIntoBackground();
		}
		else
		{
			EraseBoardFromBackground();
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		isDragging = false;
	}

	private void EraseBoardFromBackground()
	{
		GridLocation gridLocationFromScreenPosition = WireframeEditorCanvas.GetGridLocationFromScreenPosition();
		int col = Mathf.Clamp(gridLocationFromScreenPosition.c, 0, 12);
		int row = Mathf.Clamp(gridLocationFromScreenPosition.r, 0, 12);
		GridLocation gridLocation = new GridLocation(col, row);
		BloxelBoard value = null;
		if (GameBuilderCanvas.instance.currentBloxelLevel.backgroundTiles.TryGetValue(gridLocation, out value))
		{
			SoundManager.PlayEventSound(SoundEvent.Erase);
			GameBuilderCanvas.instance.currentBloxelLevel.RemoveBackgroundTileAtLocation(gridLocation);
			LevelBackground value2 = null;
			GameplayBuilder.instance.backgrounds.TryGetValue(new WorldPos2D(GameBuilderCanvas.instance.currentBloxelLevel.location.c, GameBuilderCanvas.instance.currentBloxelLevel.location.r), out value2);
			if (value2 != null)
			{
				value2.ErasePixelsAtLocation(gridLocation);
			}
		}
	}

	private void PaintBoardIntoBackground()
	{
		GridLocation gridLocationFromScreenPosition = WireframeEditorCanvas.GetGridLocationFromScreenPosition();
		if (gridLocationFromScreenPosition.c < 0 || gridLocationFromScreenPosition.c > 12 || gridLocationFromScreenPosition.r < 0 || gridLocationFromScreenPosition.r > 12)
		{
			return;
		}
		int num = gridLocationFromScreenPosition.r * 13 + gridLocationFromScreenPosition.c;
		if (!changedSpaces[num])
		{
			changedSpaces[num] = true;
			SoundManager.PlayOneShot(SoundManager.instance.popC);
			GameBuilderCanvas.instance.currentBloxelLevel.SetBackgroundTileAtLocation(gridLocationFromScreenPosition, currentBloxelBoard);
			LevelBackground value = null;
			GameplayBuilder.instance.backgrounds.TryGetValue(new WorldPos2D(GameBuilderCanvas.instance.currentBloxelLevel.location.c, GameBuilderCanvas.instance.currentBloxelLevel.location.r), out value);
			if (value != null)
			{
				value.InitForLevel(GameBuilderCanvas.instance.currentBloxelLevel);
			}
		}
	}
}
