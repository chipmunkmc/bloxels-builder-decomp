using System;
using System.Collections.Generic;
using UnityEngine;

public class BadgeManager : MonoBehaviour
{
	public static BadgeManager instance;

	[Header("Data")]
	public List<Badge> badgesEarned;

	public Badge latestBadge;

	public Dictionary<Challenge, Badge> badgeForChallenge;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		badgesEarned = new List<Badge>();
	}

	private void Start()
	{
		foreach (Badge value in Enum.GetValues(typeof(Badge)))
		{
			if (CheckBadge(value) && !badgesEarned.Contains(value))
			{
				badgesEarned.Add(value);
			}
		}
		badgeForChallenge = new Dictionary<Challenge, Badge>
		{
			{
				Challenge.GameBuilder_Easy,
				Badge.GameBuilder_Bronze
			},
			{
				Challenge.CharacterBuilder_Easy,
				Badge.CharacterBuilder_Bronze
			}
		};
	}

	public bool CheckBadge(Badge badge)
	{
		return PrefsManager.instance.CheckBadge(badge);
	}

	public void BadgeEarned(Badge badge)
	{
		PrefsManager.instance.BadgeEarned(badge);
		if (!badgesEarned.Contains(badge))
		{
			badgesEarned.Add(badge);
		}
	}

	public string GetNameFromType(Badge badge)
	{
		string result = string.Empty;
		switch (badge)
		{
		case Badge.GameBuilder_Bronze:
			result = LocalizationManager.getInstance().getLocalizedText("challenges83", "Game Builder Bronze");
			break;
		case Badge.CharacterBuilder_Bronze:
			result = LocalizationManager.getInstance().getLocalizedText("challenges84", "Hero Builder Bronze");
			break;
		}
		return result;
	}

	public string GetNameFromLatest()
	{
		return GetNameFromType(latestBadge);
	}
}
