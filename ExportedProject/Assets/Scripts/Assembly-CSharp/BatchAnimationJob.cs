using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class BatchAnimationJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<string, BloxelAnimation> _pool, List<string> _tags, List<BloxelBoard> _addedBoards);

	public bool isRunning;

	public Dictionary<string, BloxelAnimation> animationPool;

	public List<DirectoryInfo> directoriesWithMissingProjects;

	public List<BloxelBoard> addedBoards;

	public List<string> tags;

	public string badAnimationName;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupPool();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(animationPool, tags, addedBoards);
		}
	}

	private void SetupPool()
	{
		animationPool = new Dictionary<string, BloxelAnimation>();
		tags = new List<string>();
		addedBoards = new List<BloxelBoard>();
		directoriesWithMissingProjects = new List<DirectoryInfo>();
		DirectoryInfo[] savedDirectories = BloxelAnimation.GetSavedDirectories();
		for (int i = 0; i < savedDirectories.Length; i++)
		{
			BloxelAnimation bloxelAnimation = null;
			if (!File.Exists(savedDirectories[i].FullName + "/" + BloxelAnimation.filename))
			{
				directoriesWithMissingProjects.Add(savedDirectories[i]);
				continue;
			}
			badAnimationName = savedDirectories[i].FullName + "/" + BloxelAnimation.filename;
			try
			{
				using (TextReader reader = File.OpenText(savedDirectories[i].FullName + "/" + BloxelAnimation.filename))
				{
					JsonTextReader reader2 = new JsonTextReader(reader);
					bloxelAnimation = new BloxelAnimation(reader2);
				}
			}
			catch (Exception)
			{
			}
			if (bloxelAnimation == null)
			{
				continue;
			}
			if (bloxelAnimation.ID() == null)
			{
				Directory.Delete(savedDirectories[i].FullName, true);
				continue;
			}
			if (bloxelAnimation.boards.Count == 0)
			{
				BloxelBoard bloxelBoard = new BloxelBoard();
				bloxelAnimation.boards.Add(bloxelBoard);
				bloxelBoard.Save();
				addedBoards.Add(bloxelBoard);
			}
			if (animationPool.ContainsKey(bloxelAnimation.ID()))
			{
				continue;
			}
			animationPool.Add(bloxelAnimation.ID(), bloxelAnimation);
			for (int j = 0; j < bloxelAnimation.tags.Count; j++)
			{
				if (!tags.Contains(bloxelAnimation.tags[j]))
				{
					tags.Add(bloxelAnimation.tags[j]);
				}
			}
		}
		RemoveDeadDirectories();
	}

	private void RemoveDeadDirectories()
	{
		for (int i = 0; i < directoriesWithMissingProjects.Count; i++)
		{
			if (Directory.Exists(directoriesWithMissingProjects[i].FullName))
			{
				directoriesWithMissingProjects[i].Delete(true);
			}
		}
		if (directoriesWithMissingProjects != null && directoriesWithMissingProjects.Count > 0)
		{
			directoriesWithMissingProjects.Clear();
		}
	}
}
