using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class BatchBoardJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<string, BloxelBoard> _pool, List<string> _tags);

	public bool isRunning;

	public Dictionary<string, BloxelBoard> boardPool;

	public List<DirectoryInfo> directoriesWithMissingProjects;

	public List<string> tags;

	public string badBoardName;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupBoardPool();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(boardPool, tags);
		}
	}

	private void SetupBoardPool()
	{
		boardPool = new Dictionary<string, BloxelBoard>();
		tags = new List<string>();
		directoriesWithMissingProjects = new List<DirectoryInfo>();
		DirectoryInfo[] savedBoards = BloxelBoard.GetSavedBoards();
		for (int i = 0; i < savedBoards.Length; i++)
		{
			BloxelBoard bloxelBoard = null;
			if (!File.Exists(savedBoards[i].FullName + "/" + BloxelBoard.filename))
			{
				directoriesWithMissingProjects.Add(savedBoards[i]);
				continue;
			}
			badBoardName = savedBoards[i].FullName + "/" + BloxelBoard.filename;
			try
			{
				using (TextReader reader = File.OpenText(savedBoards[i].FullName + "/" + BloxelBoard.filename))
				{
					JsonTextReader reader2 = new JsonTextReader(reader);
					bloxelBoard = new BloxelBoard(reader2);
				}
			}
			catch (Exception)
			{
			}
			if (bloxelBoard == null)
			{
				continue;
			}
			if (bloxelBoard.ID() == null)
			{
				Directory.Delete(savedBoards[i].FullName, true);
			}
			else
			{
				if (boardPool.ContainsKey(bloxelBoard.ID()))
				{
					continue;
				}
				boardPool.Add(bloxelBoard.ID(), bloxelBoard);
				for (int j = 0; j < bloxelBoard.tags.Count; j++)
				{
					if (!tags.Contains(bloxelBoard.tags[j]))
					{
						tags.Add(bloxelBoard.tags[j]);
					}
				}
			}
		}
		RemoveDeadDirectories();
	}

	private void RemoveDeadDirectories()
	{
		for (int i = 0; i < directoriesWithMissingProjects.Count; i++)
		{
			if (Directory.Exists(directoriesWithMissingProjects[i].FullName))
			{
				directoriesWithMissingProjects[i].Delete(true);
			}
		}
		if (directoriesWithMissingProjects != null && directoriesWithMissingProjects.Count > 0)
		{
			directoriesWithMissingProjects.Clear();
		}
	}
}
