using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class BatchBrainJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<string, BloxelBrain> _pool, List<string> _tags);

	public bool isRunning;

	public Dictionary<string, BloxelBrain> brainPool;

	public List<DirectoryInfo> directoriesWithMissingProjects;

	public List<string> tags;

	public string badBrainName;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupBoardPool();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(brainPool, tags);
		}
	}

	private void SetupBoardPool()
	{
		brainPool = new Dictionary<string, BloxelBrain>();
		tags = new List<string>();
		directoriesWithMissingProjects = new List<DirectoryInfo>();
		DirectoryInfo[] savedBrains = BloxelBrain.GetSavedBrains();
		for (int i = 0; i < savedBrains.Length; i++)
		{
			BloxelBrain bloxelBrain = null;
			if (!File.Exists(savedBrains[i].FullName + "/" + BloxelBrain.filename))
			{
				directoriesWithMissingProjects.Add(savedBrains[i]);
				continue;
			}
			badBrainName = savedBrains[i].FullName + "/" + BloxelBrain.filename;
			try
			{
				using (TextReader reader = File.OpenText(savedBrains[i].FullName + "/" + BloxelBrain.filename))
				{
					JsonTextReader reader2 = new JsonTextReader(reader);
					bloxelBrain = new BloxelBrain(reader2);
				}
			}
			catch (Exception)
			{
			}
			if (bloxelBrain == null)
			{
				continue;
			}
			if (bloxelBrain.ID() == null)
			{
				Directory.Delete(savedBrains[i].FullName, true);
			}
			else
			{
				if (brainPool.ContainsKey(bloxelBrain.ID()))
				{
					continue;
				}
				brainPool.Add(bloxelBrain.ID(), bloxelBrain);
				for (int j = 0; j < bloxelBrain.tags.Count; j++)
				{
					if (!tags.Contains(bloxelBrain.tags[j]))
					{
						tags.Add(bloxelBrain.tags[j]);
					}
				}
			}
		}
		RemoveDeadDirectories();
	}

	private void RemoveDeadDirectories()
	{
		for (int i = 0; i < directoriesWithMissingProjects.Count; i++)
		{
			if (Directory.Exists(directoriesWithMissingProjects[i].FullName))
			{
				directoriesWithMissingProjects[i].Delete(true);
			}
		}
		if (directoriesWithMissingProjects != null && directoriesWithMissingProjects.Count > 0)
		{
			directoriesWithMissingProjects.Clear();
		}
	}
}
