using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class BatchCharacterJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<string, BloxelCharacter> _pool, List<string> _tags);

	public bool isRunning;

	public Dictionary<string, BloxelCharacter> pool;

	public List<DirectoryInfo> directoriesWithMissingProjects;

	public List<string> tags;

	public string badCharacterName;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupPool();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(pool, tags);
		}
	}

	private void SetupPool()
	{
		pool = new Dictionary<string, BloxelCharacter>();
		tags = new List<string>();
		directoriesWithMissingProjects = new List<DirectoryInfo>();
		DirectoryInfo[] savedProjects = BloxelCharacter.GetSavedProjects();
		for (int i = 0; i < savedProjects.Length; i++)
		{
			BloxelCharacter bloxelCharacter = null;
			if (!File.Exists(savedProjects[i].FullName + "/" + BloxelCharacter.filename))
			{
				directoriesWithMissingProjects.Add(savedProjects[i]);
				continue;
			}
			badCharacterName = savedProjects[i].FullName + "/" + BloxelCharacter.filename;
			try
			{
				using (TextReader reader = File.OpenText(savedProjects[i].FullName + "/" + BloxelCharacter.filename))
				{
					JsonTextReader reader2 = new JsonTextReader(reader);
					bloxelCharacter = new BloxelCharacter(reader2);
				}
			}
			catch (Exception)
			{
			}
			if (bloxelCharacter == null)
			{
				continue;
			}
			if (bloxelCharacter.ID() == null)
			{
				Directory.Delete(savedProjects[i].FullName, true);
			}
			else if (bloxelCharacter.animations.Count == 0)
			{
				Directory.Delete(savedProjects[i].FullName, true);
			}
			else
			{
				if (pool.ContainsKey(bloxelCharacter.ID()))
				{
					continue;
				}
				pool.Add(bloxelCharacter.ID(), bloxelCharacter);
				for (int j = 0; j < bloxelCharacter.tags.Count; j++)
				{
					if (!tags.Contains(bloxelCharacter.tags[j]))
					{
						tags.Add(bloxelCharacter.tags[j]);
					}
				}
			}
		}
		RemoveDeadDirectories();
	}

	private void RemoveDeadDirectories()
	{
		for (int i = 0; i < directoriesWithMissingProjects.Count; i++)
		{
			if (Directory.Exists(directoriesWithMissingProjects[i].FullName))
			{
				directoriesWithMissingProjects[i].Delete(true);
			}
		}
		if (directoriesWithMissingProjects != null && directoriesWithMissingProjects.Count > 0)
		{
			directoriesWithMissingProjects.Clear();
		}
	}
}
