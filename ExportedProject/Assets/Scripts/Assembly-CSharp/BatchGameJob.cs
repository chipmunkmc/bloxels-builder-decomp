using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class BatchGameJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<string, BloxelGame> _pool, List<string> _tags);

	public bool isRunning;

	public Dictionary<string, BloxelGame> pool;

	public List<DirectoryInfo> directoriesWithMissingProjects;

	public List<string> tags;

	public string badGameName;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupPool();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(pool, tags);
		}
	}

	private void SetupPool()
	{
		pool = new Dictionary<string, BloxelGame>();
		tags = new List<string>();
		directoriesWithMissingProjects = new List<DirectoryInfo>();
		DirectoryInfo[] savedDirectories = BloxelGame.GetSavedDirectories();
		for (int i = 0; i < savedDirectories.Length; i++)
		{
			BloxelGame bloxelGame = null;
			if (!File.Exists(savedDirectories[i].FullName + "/" + BloxelGame.filename))
			{
				directoriesWithMissingProjects.Add(savedDirectories[i]);
				continue;
			}
			badGameName = savedDirectories[i].FullName + "/" + BloxelGame.filename;
			try
			{
				using (TextReader reader = File.OpenText(savedDirectories[i].FullName + "/" + BloxelGame.filename))
				{
					JsonTextReader reader2 = new JsonTextReader(reader);
					bloxelGame = new BloxelGame(reader2, false);
				}
			}
			catch (Exception)
			{
			}
			if (bloxelGame == null)
			{
				continue;
			}
			if (bloxelGame.ID() == null)
			{
				Directory.Delete(savedDirectories[i].FullName, true);
			}
			else
			{
				if (pool.ContainsKey(bloxelGame.ID()))
				{
					continue;
				}
				pool.Add(bloxelGame.ID(), bloxelGame);
				for (int j = 0; j < bloxelGame.tags.Count; j++)
				{
					if (!tags.Contains(bloxelGame.tags[j]))
					{
						tags.Add(bloxelGame.tags[j]);
					}
				}
			}
		}
		RemoveDeadDirectories();
	}

	private void RemoveDeadDirectories()
	{
		for (int i = 0; i < directoriesWithMissingProjects.Count; i++)
		{
			if (Directory.Exists(directoriesWithMissingProjects[i].FullName))
			{
				directoriesWithMissingProjects[i].Delete(true);
			}
		}
		if (directoriesWithMissingProjects != null && directoriesWithMissingProjects.Count > 0)
		{
			directoriesWithMissingProjects.Clear();
		}
	}
}
