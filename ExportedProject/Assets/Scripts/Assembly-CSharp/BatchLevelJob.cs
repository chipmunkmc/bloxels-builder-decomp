using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class BatchLevelJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<string, BloxelLevel> _pool, Dictionary<string, BloxelBoard> _extraBoards, List<BloxelLevel> _borked);

	public bool isRunning;

	public Dictionary<string, BloxelLevel> pool;

	public List<DirectoryInfo> directoriesWithMissingProjects;

	public Dictionary<string, BloxelBoard> extraBoards;

	public List<BloxelLevel> borkedLevels;

	public string badLevelName;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupPool();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(pool, extraBoards, borkedLevels);
		}
	}

	private void SetupPool()
	{
		pool = new Dictionary<string, BloxelLevel>();
		extraBoards = new Dictionary<string, BloxelBoard>();
		borkedLevels = new List<BloxelLevel>();
		directoriesWithMissingProjects = new List<DirectoryInfo>();
		DirectoryInfo[] savedDirectories = BloxelLevel.GetSavedDirectories();
		for (int i = 0; i < savedDirectories.Length; i++)
		{
			BloxelLevel bloxelLevel = null;
			if (!File.Exists(savedDirectories[i].FullName + "/" + BloxelLevel.filename))
			{
				directoriesWithMissingProjects.Add(savedDirectories[i]);
				continue;
			}
			badLevelName = savedDirectories[i].FullName + "/" + BloxelLevel.filename;
			try
			{
				using (TextReader reader = File.OpenText(savedDirectories[i].FullName + "/" + BloxelLevel.filename))
				{
					JsonTextReader reader2 = new JsonTextReader(reader);
					bloxelLevel = new BloxelLevel(reader2, false);
				}
			}
			catch (Exception)
			{
			}
			if (bloxelLevel == null)
			{
				continue;
			}
			if (bloxelLevel.ID() == null)
			{
				string[] array = savedDirectories[i].FullName.Split('/');
				string g = array[array.Length - 1];
				BloxelBoard bloxelBoard = new BloxelBoard();
				for (int j = 0; j < 13; j++)
				{
					for (int k = 0; k < 13; k++)
					{
						bloxelBoard.blockColors[j, k] = BlockColor.Orange;
					}
				}
				extraBoards.Add(bloxelBoard.ID(), bloxelBoard);
				BloxelLevel bloxelLevel2 = new BloxelLevel(bloxelBoard, false);
				bloxelLevel2.id = new Guid(g);
				bloxelLevel2.projectPath = savedDirectories[i].FullName;
				bloxelLevel2.Save(false);
				bloxelLevel = bloxelLevel2;
			}
			if (!pool.ContainsKey(bloxelLevel.ID()))
			{
				pool.Add(bloxelLevel.ID(), bloxelLevel);
			}
		}
		for (int l = 0; l < borkedLevels.Count; l++)
		{
			pool.Remove(borkedLevels[l].ID());
		}
		RemoveDeadDirectories();
	}

	private void RemoveDeadDirectories()
	{
		for (int i = 0; i < directoriesWithMissingProjects.Count; i++)
		{
			if (Directory.Exists(directoriesWithMissingProjects[i].FullName))
			{
				directoriesWithMissingProjects[i].Delete(true);
			}
		}
		if (directoriesWithMissingProjects != null && directoriesWithMissingProjects.Count > 0)
		{
			directoriesWithMissingProjects.Clear();
		}
	}
}
