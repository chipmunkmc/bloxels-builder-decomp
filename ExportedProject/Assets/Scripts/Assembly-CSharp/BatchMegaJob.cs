using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class BatchMegaJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<string, BloxelMegaBoard> _pool, List<string> _tags);

	public bool isRunning;

	public Dictionary<string, BloxelMegaBoard> pool;

	public List<DirectoryInfo> directoriesWithMissingProjects;

	public List<string> tags;

	public string badMegaBoardName;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupPool();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(pool, tags);
		}
	}

	private void SetupPool()
	{
		pool = new Dictionary<string, BloxelMegaBoard>();
		tags = new List<string>();
		directoriesWithMissingProjects = new List<DirectoryInfo>();
		DirectoryInfo[] savedProjects = BloxelMegaBoard.GetSavedProjects();
		for (int i = 0; i < savedProjects.Length; i++)
		{
			BloxelMegaBoard bloxelMegaBoard = null;
			if (!File.Exists(savedProjects[i].FullName + "/" + BloxelMegaBoard.filename))
			{
				directoriesWithMissingProjects.Add(savedProjects[i]);
				continue;
			}
			badMegaBoardName = savedProjects[i].FullName + "/" + BloxelMegaBoard.filename;
			try
			{
				using (TextReader reader = File.OpenText(savedProjects[i].FullName + "/" + BloxelMegaBoard.filename))
				{
					JsonTextReader reader2 = new JsonTextReader(reader);
					bloxelMegaBoard = new BloxelMegaBoard(reader2, false);
				}
			}
			catch (Exception)
			{
			}
			if (bloxelMegaBoard == null)
			{
				continue;
			}
			if (bloxelMegaBoard.ID() == null)
			{
				Directory.Delete(savedProjects[i].FullName, true);
			}
			else
			{
				if (pool.ContainsKey(bloxelMegaBoard.ID()))
				{
					continue;
				}
				pool.Add(bloxelMegaBoard.ID(), bloxelMegaBoard);
				for (int j = 0; j < bloxelMegaBoard.tags.Count; j++)
				{
					if (!tags.Contains(bloxelMegaBoard.tags[j]))
					{
						tags.Add(bloxelMegaBoard.tags[j]);
					}
				}
			}
		}
		RemoveDeadDirectories();
	}

	private void RemoveDeadDirectories()
	{
		for (int i = 0; i < directoriesWithMissingProjects.Count; i++)
		{
			if (Directory.Exists(directoriesWithMissingProjects[i].FullName))
			{
				directoriesWithMissingProjects[i].Delete(true);
			}
		}
		if (directoriesWithMissingProjects != null && directoriesWithMissingProjects.Count > 0)
		{
			directoriesWithMissingProjects.Clear();
		}
	}
}
