public struct BlockBounds
{
	public bool isEmpty;

	public byte xMin;

	public byte xMax;

	public byte yMin;

	public byte yMax;

	public BlockBounds(bool isEmpty)
	{
		xMin = 14;
		xMax = 0;
		yMin = 14;
		yMax = 0;
		this.isEmpty = true;
	}

	public override string ToString()
	{
		return "BlockBounds: " + xMin + ", " + xMax + " -- " + yMin + ", " + yMax;
	}
}
