public enum BlockColor : byte
{
	Red = 0,
	Orange = 1,
	Yellow = 2,
	Green = 3,
	Blue = 4,
	Purple = 5,
	Pink = 6,
	White = 7,
	Blank = 8
}
