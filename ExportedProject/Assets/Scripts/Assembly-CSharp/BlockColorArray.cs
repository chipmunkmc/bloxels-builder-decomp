using System;
using UnityEngine;

[Serializable]
public class BlockColorArray : ScriptableObject
{
	public byte[] blockColors = new byte[169];

	public void InitWithBlockColors(BlockColor[,] blockColors)
	{
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				this.blockColors[i * 13 + j] = (byte)blockColors[j, i];
			}
		}
	}

	public static void CopyBlockColorsFromSavedArray(string savedArrayID, BlockColor[,] blockColorsOutput)
	{
		string path = "BlockColorArrays/" + savedArrayID + ".blockColors";
		BlockColorArray blockColorArray = Resources.Load<BlockColorArray>(path);
		if (blockColorArray == null)
		{
			return;
		}
		int i = 0;
		int num = 0;
		for (; i < 13; i++)
		{
			int num2 = 0;
			while (num2 < 13)
			{
				blockColorsOutput[num2, i] = (BlockColor)blockColorArray.blockColors[num];
				num2++;
				num++;
			}
		}
	}

	public void CopyBlockColorsFromCurrentData(BlockColor[,] blockColorsOutput)
	{
		int i = 0;
		int num = 0;
		for (; i < 13; i++)
		{
			int num2 = 0;
			while (num2 < 13)
			{
				blockColorsOutput[num2, i] = (BlockColor)blockColors[num];
				num2++;
				num++;
			}
		}
	}
}
