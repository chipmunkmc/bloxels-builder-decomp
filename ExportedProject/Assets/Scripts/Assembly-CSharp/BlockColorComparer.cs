using System.Collections.Generic;
using System.Runtime.InteropServices;

[StructLayout((short)0, Size = 1)]
public struct BlockColorComparer : IEqualityComparer<BlockColor>
{
	public bool Equals(BlockColor x, BlockColor y)
	{
		return x == y;
	}

	public int GetHashCode(BlockColor obj)
	{
		return (int)obj;
	}
}
