using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

public sealed class BloxelAnimation : BloxelProject
{
	public static string filename = "anim.json";

	public static string rootDirectory = "Animations";

	public static string subDirectory = "myAnimations";

	public static float defaultFrameRate = 6f;

	public static float minFrameRate = 1f;

	public static float maxFrameRate = 24f;

	public static int MaxFrames = 30;

	public static string defaultTitle = "My Animation";

	public static Dictionary<AnimationType, string> animStateDisplayText = new Dictionary<AnimationType, string>
	{
		{
			AnimationType.Idle,
			"gamebuilder60"
		},
		{
			AnimationType.Walk,
			"gamebuilder61"
		},
		{
			AnimationType.Jump,
			"gamebuilder62"
		}
	};

	private float _frameRate;

	public new string projectPath;

	public List<BloxelBoard> boards;

	public BlockBounds blockBounds;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map4;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map5;

	public BloxelAnimation(BloxelBoard board)
		: this(new List<BloxelBoard> { board })
	{
	}

	public BloxelAnimation(List<BloxelBoard> boardList)
	{
		type = ProjectType.Animation;
		base.id = Guid.NewGuid();
		projectPath = MyProjectPath() + "/" + ID();
		boards = boardList;
		coverBoard = boards[0];
		_frameRate = defaultFrameRate;
		tags = new List<string>();
		tags.Add("mine");
		SetMinMaxBounds();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelAnimation(JsonTextReader reader, bool fromServer = false)
	{
		type = ProjectType.Animation;
		FromReader(reader, fromServer);
		SetMinMaxBounds();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelAnimation(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		type = ProjectType.Animation;
		FromReaderSlim(reader, projectPool);
		SetMinMaxBounds();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelAnimation(string s, DataSource source)
	{
		type = ProjectType.Animation;
		switch (source)
		{
		case DataSource.FilePath:
			try
			{
				string jsonString = File.ReadAllText(s + "/" + filename);
				FromJSONString(jsonString);
			}
			catch (Exception)
			{
			}
			break;
		case DataSource.JSONString:
			FromJSONString(s, true);
			break;
		}
		SetMinMaxBounds();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public static string BaseStoragePath()
	{
		return AssetManager.baseStoragePath + rootDirectory;
	}

	public static string MyProjectPath()
	{
		return BaseStoragePath() + "/" + subDirectory;
	}

	public void FromReader(JsonTextReader reader, bool fromServer = false)
	{
		string text = string.Empty;
		boards = new List<BloxelBoard>();
		tags = new List<string>();
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					if (text == "boards")
					{
						ParseBoards(reader, fromServer);
					}
					if (text == "board_data")
					{
						ParseBoards(reader, fromServer);
					}
				}
				else
				{
					if (text == null)
					{
						continue;
					}
					if (_003C_003Ef__switch_0024map4 == null)
					{
						Dictionary<string, int> dictionary = new Dictionary<string, int>(8);
						dictionary.Add("id", 0);
						dictionary.Add("title", 1);
						dictionary.Add("frameRate", 2);
						dictionary.Add("boards", 3);
						dictionary.Add("tags", 4);
						dictionary.Add("isDirty", 5);
						dictionary.Add("builtWithVersion", 6);
						dictionary.Add("owner", 7);
						_003C_003Ef__switch_0024map4 = dictionary;
					}
					int value;
					if (!_003C_003Ef__switch_0024map4.TryGetValue(text, out value))
					{
						continue;
					}
					switch (value)
					{
					case 0:
						base.id = new Guid(reader.Value.ToString());
						break;
					case 1:
						title = reader.Value.ToString();
						break;
					case 2:
						_frameRate = float.Parse(reader.Value.ToString());
						if (_frameRate <= 0f)
						{
							_frameRate = minFrameRate;
						}
						break;
					case 3:
						if (!fromServer)
						{
							ParseBoards(reader);
						}
						break;
					case 4:
						tags.Add(reader.Value.ToString());
						break;
					case 5:
						isDirty = bool.Parse(reader.Value.ToString());
						break;
					case 6:
						SetBuildVersion(int.Parse(reader.Value.ToString()));
						break;
					case 7:
						owner = reader.Value.ToString();
						break;
					}
				}
			}
			else
			{
				if (reader.TokenType != JsonToken.EndObject)
				{
					continue;
				}
				projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
				if (boards.Count > 0)
				{
					coverBoard = boards[0];
				}
				else if (!fromServer)
				{
					coverBoard = new BloxelBoard();
					if (!AssetManager.instance.boardPool.ContainsKey(coverBoard.ID()))
					{
						AssetManager.instance.boardPool.Add(coverBoard.ID(), coverBoard);
					}
				}
				break;
			}
		}
	}

	private void FromReaderSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		boards = new List<BloxelBoard>();
		tags = new List<string>();
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					if (text == "boards")
					{
						ParseBoardsSlim(reader, projectPool);
					}
				}
				else
				{
					if (text == null)
					{
						continue;
					}
					if (_003C_003Ef__switch_0024map5 == null)
					{
						Dictionary<string, int> dictionary = new Dictionary<string, int>(8);
						dictionary.Add("_id", 0);
						dictionary.Add("id", 1);
						dictionary.Add("title", 2);
						dictionary.Add("frameRate", 3);
						dictionary.Add("tags", 4);
						dictionary.Add("isDirty", 5);
						dictionary.Add("builtWithVersion", 6);
						dictionary.Add("owner", 7);
						_003C_003Ef__switch_0024map5 = dictionary;
					}
					int value;
					if (!_003C_003Ef__switch_0024map5.TryGetValue(text, out value))
					{
						continue;
					}
					switch (value)
					{
					case 0:
						_serverID = reader.Value.ToString();
						break;
					case 1:
						base.id = new Guid(reader.Value.ToString());
						break;
					case 2:
						title = reader.Value.ToString();
						break;
					case 3:
						_frameRate = float.Parse(reader.Value.ToString());
						if (_frameRate <= 0f)
						{
							_frameRate = minFrameRate;
						}
						break;
					case 4:
						tags.Add(reader.Value.ToString());
						break;
					case 5:
						isDirty = bool.Parse(reader.Value.ToString());
						break;
					case 6:
						SetBuildVersion(int.Parse(reader.Value.ToString()));
						break;
					case 7:
						owner = reader.Value.ToString();
						break;
					}
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
				if (boards.Count > 0)
				{
					coverBoard = boards[0];
				}
				break;
			}
		}
	}

	public void FromJSONString(string jsonString, bool fromServer = false)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader, fromServer);
	}

	private void ParseBoards(JsonTextReader reader, bool fromServer = false)
	{
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.TokenType == JsonToken.StartObject)
				{
					BloxelBoard item = new BloxelBoard(reader);
					boards.Add(item);
				}
				if (reader.TokenType == JsonToken.EndArray)
				{
					break;
				}
			}
			return;
		}
		string empty = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				empty = reader.Value.ToString();
				BloxelBoard value = null;
				if (AssetManager.instance.boardPool.TryGetValue(empty, out value))
				{
					boards.Add(value);
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBoardsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		while (reader.Read())
		{
			if (reader.TokenType == JsonToken.StartArray)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.String)
			{
				string key = reader.Value.ToString();
				BloxelProject value = null;
				if (!projectPool.TryGetValue(key, out value))
				{
					continue;
				}
				if (value.type == ProjectType.Board)
				{
					boards.Add(value as BloxelBoard);
				}
			}
			if (reader.TokenType != JsonToken.EndArray)
			{
				continue;
			}
			break;
		}
	}

	public string ToJSON(bool full = false)
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject jSONObject2 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject3 = new JSONObject(JSONObject.Type.ARRAY);
		foreach (BloxelBoard board in boards)
		{
			jSONObject2.Add(board.ID());
		}
		for (int i = 0; i < tags.Count; i++)
		{
			jSONObject3.Add(tags[i]);
		}
		jSONObject.AddField("id", base.id.ToString());
		jSONObject.AddField("title", title);
		jSONObject.AddField("owner", owner);
		jSONObject.AddField("boards", jSONObject2);
		jSONObject.AddField("frameRate", _frameRate);
		jSONObject.AddField("tags", jSONObject3);
		jSONObject.AddField("isDirty", isDirty);
		jSONObject.AddField("builtWithVersion", builtWithVersion);
		return jSONObject.ToString();
	}

	public override bool Save(bool deepSave = true, bool shouldQueue = true)
	{
		bool flag = false;
		if (shouldQueue)
		{
			if (deepSave)
			{
				for (int i = 0; i < boards.Count; i++)
				{
					boards[i].Save(deepSave, shouldQueue);
				}
			}
			SetMinMaxBounds();
			SaveManager.Instance.Enqueue(this);
			return false;
		}
		if (deepSave)
		{
			for (int j = 0; j < boards.Count; j++)
			{
				boards[j].Save(true, false);
			}
		}
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		string text = ToJSON();
		SetDirty(true);
		if (string.IsNullOrEmpty(text))
		{
			return false;
		}
		if (Directory.Exists(projectPath))
		{
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		else
		{
			Directory.CreateDirectory(projectPath);
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		if (File.Exists(projectPath + "/" + filename))
		{
			return true;
		}
		return false;
	}

	public bool Delete()
	{
		bool flag = false;
		if (Directory.Exists(projectPath))
		{
			Directory.Delete(projectPath, true);
		}
		for (int i = 0; i < boards.Count; i++)
		{
			BloxelBoard.DeleteBoardWithID(boards[i].ID());
		}
		if (Directory.Exists(projectPath))
		{
			flag = false;
		}
		else
		{
			flag = true;
			if (ID() != null)
			{
				AssetManager.instance.animationPool.Remove(ID());
			}
		}
		CloudManager.Instance.RemoveFileFromQueue(syncInfo);
		return flag;
	}

	public override void SetDirty(bool isDirty)
	{
		base.SetDirty(isDirty);
		if (isDirty)
		{
			CloudManager.Instance.EnqueueFileForSync(syncInfo);
		}
	}

	public static DirectoryInfo[] GetSavedDirectories()
	{
		DirectoryInfo directoryInfo = null;
		string path = MyProjectPath();
		if (Directory.Exists(path))
		{
			directoryInfo = new DirectoryInfo(path);
		}
		else
		{
			Directory.CreateDirectory(path);
			directoryInfo = new DirectoryInfo(path);
		}
		return (from p in directoryInfo.GetDirectories()
			orderby p.LastWriteTime descending
			select p).ToArray();
	}

	public BloxelAnimation Clone()
	{
		List<BloxelBoard> list = new List<BloxelBoard>();
		for (int i = 0; i < boards.Count; i++)
		{
			list.Add(boards[i].Clone());
		}
		BloxelAnimation bloxelAnimation = new BloxelAnimation(list);
		bloxelAnimation.title = title;
		bloxelAnimation.tags = tags;
		bloxelAnimation.owner = owner;
		if (!string.IsNullOrEmpty(_serverID))
		{
			bloxelAnimation._serverID = _serverID;
		}
		return bloxelAnimation;
	}

	public float GetFrameRate()
	{
		return _frameRate;
	}

	public void SetFrameRate(float rate, bool shouldsave = true)
	{
		if (rate > 0f)
		{
			_frameRate = rate;
		}
		else
		{
			_frameRate = minFrameRate;
		}
		if (shouldsave)
		{
			Save(false);
		}
	}

	public new void SetTitle(string _title)
	{
		title = _title;
		Save(false);
	}

	public void SetOwner(string _owner, bool shouldsave = true)
	{
		base.SetOwner(_owner);
		if (shouldsave)
		{
			Save(false);
		}
	}

	public new void CheckAndSetMeAsOwner()
	{
		base.CheckAndSetMeAsOwner();
		Save(false);
	}

	public new void AddTag(string tag)
	{
		base.AddTag(tag);
		Save(false);
	}

	public new void AddMultipleTags(List<string> tagsToAdd)
	{
		base.AddMultipleTags(tagsToAdd);
		Save(false);
	}

	public new void RemoveTag(string tag)
	{
		base.RemoveTag(tag);
		Save(false);
	}

	public bool RemoveBoard(BloxelBoard b)
	{
		bool flag = false;
		bool flag2 = false;
		foreach (BloxelBoard board in boards)
		{
			if (board.ID() == b.ID())
			{
				flag = true;
				break;
			}
		}
		if (flag)
		{
			b.Delete();
			flag2 = boards.Remove(b);
		}
		if (flag2)
		{
			Save(false);
			return true;
		}
		return false;
	}

	public Dictionary<string, BloxelBoard> UniqueBoards()
	{
		Dictionary<string, BloxelBoard> dictionary = new Dictionary<string, BloxelBoard>();
		for (int i = 0; i < boards.Count; i++)
		{
			if (!dictionary.ContainsKey(boards[i].ID()))
			{
				dictionary[boards[i].ID()] = boards[i];
			}
		}
		return dictionary;
	}

	public override int GetBloxelCount()
	{
		int num = 0;
		Dictionary<string, BloxelBoard> dictionary = UniqueBoards();
		Dictionary<string, BloxelBoard>.Enumerator enumerator = dictionary.GetEnumerator();
		while (enumerator.MoveNext())
		{
			num += enumerator.Current.Value.GetBloxelCount();
		}
		return num;
	}

	public List<string> GetAssociatedBoardPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelBoard board in boards)
		{
			if (!list.Contains(board.projectPath))
			{
				board.CheckAndSetMeAsOwner();
				list.Add(board.projectPath);
			}
		}
		return list;
	}

	public override ProjectBundle Compress()
	{
		List<string> associatedBoardPaths = GetAssociatedBoardPaths();
		string[] array = new string[associatedBoardPaths.Count];
		string[] array2 = new string[associatedBoardPaths.Count];
		int num = 0;
		int num2 = 0;
		while (num2 < associatedBoardPaths.Count)
		{
			array[num] = associatedBoardPaths[num2] + "/" + BloxelBoard.filename;
			string[] array3 = associatedBoardPaths[num2].Split('/');
			array2[num] = array3[array3.Length - 1];
			num2++;
			num++;
		}
		return new ProjectBundle(Zipper.CreateBytes(array, array2), ToJSON());
	}

	public void SetMinMaxBounds()
	{
		this.blockBounds = new BlockBounds(true);
		bool flag = true;
		int count = boards.Count;
		for (int i = 0; i < count; i++)
		{
			BlockBounds blockBounds = boards[i].blockBounds;
			if (!blockBounds.isEmpty)
			{
				flag = false;
				if (blockBounds.xMin < this.blockBounds.xMin)
				{
					this.blockBounds.xMin = blockBounds.xMin;
				}
				if (blockBounds.xMax >= this.blockBounds.xMax)
				{
					this.blockBounds.xMax = blockBounds.xMax;
				}
				if (blockBounds.yMin < this.blockBounds.yMin)
				{
					this.blockBounds.yMin = blockBounds.yMin;
				}
				if (blockBounds.yMax >= this.blockBounds.yMax)
				{
					this.blockBounds.yMax = blockBounds.yMax;
				}
			}
		}
		this.blockBounds.isEmpty = flag;
		if (flag)
		{
			this.blockBounds.xMin = 0;
			this.blockBounds.yMin = 0;
			this.blockBounds.xMax = 13;
			this.blockBounds.yMax = 13;
		}
	}
}
