using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloxelAnimationLibrary : BloxelLibraryWindow
{
	public static BloxelAnimationLibrary instance;

	public Object savedAnimationPrefab;

	public bool initComplete;

	public BloxelAnimation currentAnimation { get; private set; }

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
	}

	private new void Start()
	{
		if (!initComplete)
		{
			PopulateLibrary();
			base.Start();
			base.OnProjectSelect += SetCurrentProject;
			initComplete = true;
		}
	}

	private void OnDestroy()
	{
		base.OnProjectSelect -= SetCurrentProject;
	}

	public void SetCurrentProject(SavedProject item)
	{
		BloxelAnimation bloxelAnimation = item.dataModel as BloxelAnimation;
		if (currentAnimation == null || currentAnimation != bloxelAnimation)
		{
			currentAnimation = bloxelAnimation;
			((SavedAnimation)item).cover.Animate();
			BloxelLibraryScrollController.instance.DisablePreviousSelectedOutline();
			SoundManager.PlayOneShot(SoundManager.instance.popA);
		}
	}

	private new void DestroyLibrary()
	{
		base.DestroyLibrary();
	}

	public void PopulateLibrary()
	{
		HashSet<string> hashSet = StartIgnoringBoards();
		BloxelLibraryScrollController.instance.unfilteredAnimationData.Clear();
		foreach (BloxelAnimation value in AssetManager.instance.animationPool.Values)
		{
			if (value.tags.Contains("PixelTutz") && !hashSet.Contains(value.ID()))
			{
				hashSet.Add(value.ID());
			}
		}
		foreach (BloxelCharacter value2 in AssetManager.instance.characterPool.Values)
		{
			foreach (BloxelAnimation value3 in value2.animations.Values)
			{
				if (!hashSet.Contains(value3.ID()))
				{
					hashSet.Add(value3.ID());
				}
			}
		}
		foreach (BloxelAnimation value4 in AssetManager.instance.defaultCharacterAnimations.Values)
		{
			if (!hashSet.Contains(value4.ID()))
			{
				hashSet.Add(value4.ID());
			}
		}
		foreach (BloxelAnimation value5 in AssetManager.instance.animationPool.Values)
		{
			if (!hashSet.Contains(value5.ID()))
			{
				BloxelLibraryScrollController.instance.unfilteredAnimationData.Add(value5);
			}
		}
	}

	public override void UpdateScrollerContents()
	{
		BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Animation);
	}

	public new void AddButtonPressed()
	{
		base.AddButtonPressed();
		AnimationCanvas.instance.CreateNewAnimation();
	}

	public BloxelAnimation AddProject(BloxelAnimation _anim)
	{
		if (!AssetManager.instance.animationPool.ContainsKey(_anim.ID()))
		{
			BloxelLibraryScrollController.instance.AddItem(_anim);
			StartCoroutine(DelayedSelectNewProject());
			if (AppStateManager.instance.currentScene == SceneName.PixelEditor_iPad)
			{
				_anim.SetFrameRate(BloxelAnimation.defaultFrameRate);
			}
			_anim.SetBuildVersion(AppStateManager.instance.internalVersion);
			_anim.Save();
			AssetManager.instance.animationPool.Add(_anim.ID(), _anim);
			if (!AssetManager.instance.boardPool.ContainsKey(_anim.coverBoard.ID()))
			{
				AssetManager.instance.boardPool.Add(_anim.coverBoard.ID(), _anim.coverBoard);
			}
			return _anim;
		}
		return null;
	}

	private IEnumerator DelayedSelectNewProject()
	{
		yield return new WaitForEndOfFrame();
		ItemSelect(BloxelLibraryScrollController.instance.GetFirstItem());
	}

	public void Reset()
	{
		if (currentAnimation != null)
		{
			currentAnimation = null;
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Animation);
		}
	}
}
