using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json;
using UnityEngine;

public sealed class BloxelBoard : BloxelProject
{
	public delegate void HandleUpdate();

	public static string filename = "projectFile.json";

	public static string rootDirectory = "Boards";

	public static string subDirectory = "myBoards";

	public static string defaultTitle = "My Board";

	private static int[] _blockCounts = new int[8];

	private static Color32[] _DetailPaletteCoices = new Color32[9]
	{
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, 0),
		new Color32(0, 0, 0, byte.MaxValue)
	};

	public BlockColor[,] blockColors;

	public Color[] toolPaletteChoices;

	public GridLocation[] paletteChoiceTexCoordArray = new GridLocation[8];

	public BlockBounds blockBounds;

	public static readonly Color32 BaseBoardDetailLookupColor32 = new Color32(0, 0, 0, byte.MaxValue);

	public static readonly Color32 TransparentLookupColor32 = new Color32(0, 0, 0, 254);

	private static Color _baseUIBoardColor = new Color(0.20784314f, 0.20784314f, 0.20784314f);

	private static Color32 _baseUIBoardColor32 = new Color(0.20784314f, 0.20784314f, 0.20784314f);

	private static Color[] spriteColorsBuffer = new Color[169];

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map6;

	public static Color baseUIBoardColor
	{
		get
		{
			return _baseUIBoardColor;
		}
		private set
		{
			_baseUIBoardColor = value;
		}
	}

	public static Color32 baseUIBoardColor32
	{
		get
		{
			return _baseUIBoardColor32;
		}
		private set
		{
			_baseUIBoardColor32 = value;
		}
	}

	public event HandleUpdate OnUpdate;

	public BloxelBoard(bool setProjectPath = true)
	{
		type = ProjectType.Board;
		base.id = GenerateRandomID();
		if (setProjectPath)
		{
			projectPath = MyProjectPath() + "/" + ID();
		}
		blockColors = new BlockColor[13, 13];
		tags = new List<string>();
		toolPaletteChoices = new Color[8];
		ResetPaletteChoices();
		title = "My Board";
		tags.Add("mine");
		coverBoard = this;
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				blockColors[i, j] = BlockColor.Blank;
			}
		}
		ColorPalette.instance.SetPaletteTexCoordsForBoard(this);
		SetMinMaxBounds();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelBoard(BlockColor[,] colorArray, bool shouldSave = true)
	{
		type = ProjectType.Board;
		base.id = GenerateRandomID();
		projectPath = MyProjectPath() + "/" + ID();
		tags = new List<string>();
		if (colorArray != null)
		{
			blockColors = colorArray;
		}
		else
		{
			blockColors = new BlockColor[13, 13];
			for (int i = 0; i < blockColors.GetLength(0); i++)
			{
				for (int j = 0; j < blockColors.GetLength(1); j++)
				{
					blockColors[i, j] = BlockColor.Blank;
				}
			}
		}
		toolPaletteChoices = new Color[8];
		ResetPaletteChoices();
		coverBoard = this;
		title = "My Board";
		tags.Add("mine");
		ColorPalette.instance.SetPaletteTexCoordsForBoard(this);
		syncInfo = ProjectSyncInfo.Create(this);
		if (shouldSave)
		{
			if (!Directory.Exists(projectPath))
			{
				Directory.CreateDirectory(projectPath);
			}
			Save();
		}
		else
		{
			SetMinMaxBounds();
		}
	}

	public BloxelBoard(BlockColor[,] colorArray, Color[] paletteChoices, bool shouldSave = false)
	{
		type = ProjectType.Board;
		base.id = GenerateRandomID();
		projectPath = MyProjectPath() + "/" + ID();
		if (colorArray != null)
		{
			blockColors = (BlockColor[,])colorArray.Clone();
		}
		else
		{
			blockColors = new BlockColor[13, 13];
			for (int i = 0; i < blockColors.GetLength(0); i++)
			{
				for (int j = 0; j < blockColors.GetLength(1); j++)
				{
					blockColors[i, j] = BlockColor.Blank;
				}
			}
		}
		toolPaletteChoices = (Color[])paletteChoices.Clone();
		tags = new List<string>();
		coverBoard = this;
		title = "My Board";
		tags.Add("mine");
		ColorPalette.instance.SetPaletteTexCoordsForBoard(this);
		syncInfo = ProjectSyncInfo.Create(this);
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		if (shouldSave)
		{
			Save();
		}
		else
		{
			SetMinMaxBounds();
		}
	}

	public BloxelBoard(JsonTextReader reader)
	{
		type = ProjectType.Board;
		FromReader(reader);
		ColorPalette.instance.SetPaletteTexCoordsForBoard(this);
		syncInfo = ProjectSyncInfo.Create(this);
		SetMinMaxBounds();
	}

	public BloxelBoard(string s, DataSource source)
	{
		type = ProjectType.Board;
		switch (source)
		{
		case DataSource.FilePath:
			try
			{
				string jsonString = File.ReadAllText(s + "/" + filename);
				FromJSONString(jsonString);
			}
			catch (Exception)
			{
			}
			break;
		case DataSource.JSONString:
			FromJSONString(s);
			break;
		}
		coverBoard = this;
		if ((bool)ColorPalette.instance)
		{
			ColorPalette.instance.SetPaletteTexCoordsForBoard(this);
		}
		syncInfo = ProjectSyncInfo.Create(this);
		SetMinMaxBounds();
	}

	public static BloxelBoard DefaultWireframe()
	{
		BlockColor[,] array = new BlockColor[13, 13];
		for (int i = 0; i < array.GetLength(0); i++)
		{
			for (int j = 0; j < array.GetLength(1); j++)
			{
				if (i >= 0 && i <= 12 && j >= 0 && j <= 1)
				{
					array[i, j] = BlockColor.Green;
				}
				else
				{
					array[i, j] = BlockColor.Blank;
				}
			}
		}
		BloxelBoard bloxelBoard = new BloxelBoard(array, false);
		bloxelBoard.AddTag("PPHide");
		bloxelBoard.SetMinMaxBounds();
		return bloxelBoard;
	}

	public static string BaseStoragePath()
	{
		return AssetManager.baseStoragePath + rootDirectory;
	}

	public static string MyProjectPath()
	{
		return BaseStoragePath() + "/" + subDirectory;
	}

	public void FromReader(JsonTextReader reader)
	{
		string text = string.Empty;
		blockColors = new BlockColor[13, 13];
		int num = 0;
		toolPaletteChoices = new Color[8];
		tags = new List<string>();
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					switch (text)
					{
					case "palette":
						ParsePalette(reader);
						break;
					case "tags":
						ParseTags(reader);
						break;
					}
				}
				else
				{
					if (text == null)
					{
						continue;
					}
					if (_003C_003Ef__switch_0024map6 == null)
					{
						Dictionary<string, int> dictionary = new Dictionary<string, int>(7);
						dictionary.Add("_id", 0);
						dictionary.Add("id", 1);
						dictionary.Add("colors", 2);
						dictionary.Add("title", 3);
						dictionary.Add("owner", 4);
						dictionary.Add("isDirty", 5);
						dictionary.Add("builtWithVersion", 6);
						_003C_003Ef__switch_0024map6 = dictionary;
					}
					int value;
					if (_003C_003Ef__switch_0024map6.TryGetValue(text, out value))
					{
						switch (value)
						{
						case 0:
							_serverID = reader.Value.ToString();
							break;
						case 1:
							base.id = new Guid(reader.Value.ToString());
							break;
						case 2:
						{
							int num2 = num % 13;
							int num3 = num / 13;
							blockColors[num3, num2] = (BlockColor)int.Parse(reader.Value.ToString());
							num++;
							break;
						}
						case 3:
							title = reader.Value.ToString();
							break;
						case 4:
							owner = reader.Value.ToString();
							break;
						case 5:
							isDirty = bool.Parse(reader.Value.ToString());
							break;
						case 6:
							SetBuildVersion(int.Parse(reader.Value.ToString()));
							break;
						}
					}
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
				coverBoard = this;
				break;
			}
		}
	}

	public void FromJSONString(string jsonString)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader);
	}

	private void ParseTags(JsonTextReader reader)
	{
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				tags.Add(reader.Value.ToString());
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParsePalette(JsonTextReader reader)
	{
		string text = string.Empty;
		float r = 0f;
		float g = 0f;
		float b = 0f;
		int num = 1;
		int num2 = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "r":
					r = float.Parse(reader.Value.ToString());
					break;
				case "g":
					g = float.Parse(reader.Value.ToString());
					break;
				case "b":
					b = float.Parse(reader.Value.ToString());
					break;
				case "a":
					toolPaletteChoices[num2++] = new Color(r, g, b, num);
					break;
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	public string ToJSON()
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject jSONObject2 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject3 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject4 = new JSONObject(JSONObject.Type.ARRAY);
		for (int i = 0; i < tags.Count; i++)
		{
			jSONObject4.Add(tags[i]);
		}
		for (int j = 0; j < blockColors.GetLength(0); j++)
		{
			for (int k = 0; k < blockColors.GetLength(1); k++)
			{
				jSONObject2.Add(new JSONObject((int)blockColors[j, k]));
			}
		}
		Color[] array = toolPaletteChoices;
		for (int l = 0; l < array.Length; l++)
		{
			Color color = array[l];
			JSONObject jSONObject5 = new JSONObject();
			jSONObject5.AddField("r", color.r);
			jSONObject5.AddField("g", color.g);
			jSONObject5.AddField("b", color.b);
			jSONObject5.AddField("a", color.a);
			jSONObject3.Add(jSONObject5);
		}
		jSONObject.AddField("id", base.id.ToString());
		jSONObject.AddField("owner", owner);
		jSONObject.AddField("colors", jSONObject2);
		jSONObject.AddField("palette", jSONObject3);
		jSONObject.AddField("tags", jSONObject4);
		jSONObject.AddField("builtWithVersion", builtWithVersion);
		jSONObject.AddField("isDirty", isDirty);
		return jSONObject.ToString();
	}

	public override bool Save(bool deepSave = true, bool shouldQueue = true)
	{
		bool flag = false;
		SetMinMaxBounds();
		if (shouldQueue)
		{
			SaveManager.Instance.Enqueue(this);
			return false;
		}
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		string text = ToJSON();
		SetDirty(true);
		if (string.IsNullOrEmpty(text))
		{
			return false;
		}
		if (Directory.Exists(projectPath))
		{
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		else
		{
			Directory.CreateDirectory(projectPath);
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		if (File.Exists(projectPath + "/" + filename))
		{
			return true;
		}
		return false;
	}

	public override void SetDirty(bool isDirty)
	{
		base.SetDirty(isDirty);
		if (isDirty)
		{
			CloudManager.Instance.EnqueueFileForSync(syncInfo);
		}
	}

	public bool Delete()
	{
		bool flag = false;
		if (Directory.Exists(projectPath))
		{
			Directory.Delete(projectPath, true);
		}
		if (Directory.Exists(projectPath))
		{
			flag = false;
		}
		else
		{
			flag = true;
			if (ID() != null)
			{
				AssetManager.instance.boardPool.Remove(ID());
			}
		}
		CloudManager.Instance.RemoveFileFromQueue(syncInfo);
		return flag;
	}

	public static bool DeleteBoardWithID(string boardID)
	{
		bool flag = false;
		string path = MyProjectPath() + "/" + boardID;
		if (Directory.Exists(path))
		{
			Directory.Delete(path, true);
		}
		if (Directory.Exists(path))
		{
			flag = false;
		}
		else
		{
			flag = true;
			if (boardID != null)
			{
				AssetManager.instance.boardPool.Remove(boardID);
			}
		}
		return flag;
	}

	public static DirectoryInfo[] GetSavedBoards()
	{
		DirectoryInfo directoryInfo = null;
		string path = MyProjectPath();
		if (Directory.Exists(path))
		{
			directoryInfo = new DirectoryInfo(path);
			DirectoryInfo[] directories = directoryInfo.GetDirectories();
			int num = directories.Length;
			for (int i = 0; i < num; i++)
			{
				DirectoryInfo directoryInfo2 = directories[i];
				if (!File.Exists(directoryInfo2.FullName + "/" + filename))
				{
					directoryInfo2.Delete();
				}
			}
		}
		else
		{
			Directory.CreateDirectory(path);
			directoryInfo = new DirectoryInfo(path);
		}
		return (from p in directoryInfo.GetDirectories()
			orderby p.LastWriteTime descending
			select p).ToArray();
	}

	public static List<string> GetProjectsForBoardWithID(string boardID)
	{
		List<string> list = new List<string>();
		JSONObject jSONObject = null;
		for (int i = 0; i < BloxelAnimation.GetSavedDirectories().Length; i++)
		{
			try
			{
				string str = File.ReadAllText(BloxelAnimation.GetSavedDirectories()[i].FullName + "/" + BloxelAnimation.filename);
				jSONObject = new JSONObject(str);
			}
			catch (Exception)
			{
			}
			if (jSONObject == null)
			{
				continue;
			}
			JSONObject jSONObject2 = new JSONObject(JSONObject.Type.STRING);
			JSONObject jSONObject3 = new JSONObject(JSONObject.Type.ARRAY);
			for (int j = 0; j < jSONObject.list.Count; j++)
			{
				switch (jSONObject.keys[j])
				{
				case "id":
					jSONObject2 = jSONObject.list[j];
					break;
				case "boards":
					jSONObject3 = jSONObject.list[j];
					break;
				}
			}
			char[] trimChars = new char[1] { '"' };
			for (int k = 0; k < jSONObject3.list.Count; k++)
			{
				if (jSONObject3.list[k].str == boardID)
				{
					list.Add(jSONObject2.ToString().Trim(trimChars));
				}
			}
		}
		for (int l = 0; l < BloxelMegaBoard.GetSavedProjects().Length; l++)
		{
			try
			{
				string str = File.ReadAllText(BloxelMegaBoard.GetSavedProjects()[l].FullName + "/" + BloxelMegaBoard.filename);
				jSONObject = new JSONObject(str);
			}
			catch (Exception)
			{
			}
			if (jSONObject == null)
			{
				continue;
			}
			JSONObject jSONObject4 = new JSONObject(JSONObject.Type.STRING);
			JSONObject jSONObject5 = new JSONObject(JSONObject.Type.ARRAY);
			for (int m = 0; m < jSONObject.list.Count; m++)
			{
				switch (jSONObject.keys[m])
				{
				case "id":
					jSONObject4 = jSONObject.list[m];
					break;
				case "boards":
					jSONObject5 = jSONObject.list[m];
					break;
				}
			}
			char[] trimChars2 = new char[1] { '"' };
			for (int n = 0; n < jSONObject5.list.Count; n++)
			{
				if (jSONObject5.list[n].GetField("id").str == boardID)
				{
					list.Add(jSONObject4.ToString().Trim(trimChars2));
				}
			}
		}
		return list;
	}

	public static bool CheckSaveFileExistsInDirectory(string directoryPath)
	{
		bool result = false;
		if (Directory.Exists(directoryPath))
		{
			if (File.Exists(directoryPath + "/" + filename))
			{
				result = true;
			}
			else
			{
				result = false;
				DeleteDirectory(directoryPath);
			}
		}
		return result;
	}

	public static bool DeleteDirectory(string directoryPath)
	{
		bool flag = false;
		if (Directory.Exists(directoryPath))
		{
			Directory.Delete(directoryPath, true);
		}
		if (Directory.Exists(directoryPath))
		{
			return false;
		}
		return true;
	}

	public static BloxelBoard GetSavedBoardById(string _id)
	{
		return new BloxelBoard(MyProjectPath() + "/" + _id, DataSource.FilePath);
	}

	public new void AddTag(string tag)
	{
		base.AddTag(tag);
		Save();
	}

	public new void AddMultipleTags(List<string> tagsToAdd)
	{
		base.AddMultipleTags(tagsToAdd);
		Save();
	}

	public new void RemoveTag(string tag)
	{
		base.RemoveTag(tag);
		Save();
	}

	public new void SetTitle(string _title)
	{
		title = _title;
		Save();
	}

	public void SetOwner(string _owner, bool shouldSave = true)
	{
		base.SetOwner(_owner);
		if (shouldSave)
		{
			Save();
		}
	}

	public new void CheckAndSetMeAsOwner()
	{
		base.CheckAndSetMeAsOwner();
		Save();
	}

	public BloxelBoard Clone()
	{
		BlockColor[,] colorArray = blockColors.Clone() as BlockColor[,];
		Color[] paletteChoices = toolPaletteChoices.Clone() as Color[];
		BloxelBoard bloxelBoard = new BloxelBoard(colorArray, paletteChoices);
		bloxelBoard.tags = tags;
		bloxelBoard.title = title;
		bloxelBoard.owner = owner;
		if (!string.IsNullOrEmpty(_serverID))
		{
			bloxelBoard._serverID = _serverID;
		}
		return bloxelBoard;
	}

	public void SetTileAtLocation(int idx, BlockColor color, bool shouldSave = true)
	{
		int num = idx % 13;
		int num2 = (int)Mathf.Floor(idx / 13);
		blockColors[num2, num] = color;
		if (shouldSave)
		{
			Save();
		}
		if (this.OnUpdate != null)
		{
			this.OnUpdate();
		}
	}

	public void ChangePaletteChoice(int idx, Color color)
	{
		toolPaletteChoices[idx] = color;
		Save();
		if (this.OnUpdate != null)
		{
			this.OnUpdate();
		}
	}

	public void Reset()
	{
		toolPaletteChoices = new Color[8]
		{
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Yellow),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Pink),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Purple),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.White)
		};
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				blockColors[i, j] = BlockColor.Blank;
			}
		}
		ColorPalette.instance.SetPaletteTexCoordsForBoard(this);
		SetMinMaxBounds();
	}

	public void ClearBlocks()
	{
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				blockColors[i, j] = BlockColor.Blank;
			}
		}
	}

	public void BlastUpdate()
	{
		if (this.OnUpdate != null)
		{
			this.OnUpdate();
		}
	}

	public override ProjectBundle Compress()
	{
		return new ProjectBundle(null, ToJSON());
	}

	public override int GetBloxelCount()
	{
		int num = 0;
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				if (blockColors[i, j] != BlockColor.Blank)
				{
					num++;
				}
			}
		}
		return num;
	}

	public static Texture2D GetSolidBoard(Color boardColor)
	{
		int num = 0;
		int num2 = 2;
		int num3 = 0;
		int num4 = num2;
		int num5 = num * 2 + num2 * 13 + num3 * 12;
		Color[] array = new Color[num5 * num5];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = boardColor;
		}
		Texture2D texture2D = new Texture2D(num5, num5, TextureFormat.ARGB32, false);
		texture2D.filterMode = FilterMode.Point;
		texture2D.wrapMode = TextureWrapMode.Clamp;
		texture2D.SetPixels(array);
		for (int j = 0; j < 13; j++)
		{
			for (int k = 0; k < 13; k++)
			{
				texture2D.SetPixel(j * num4 + num, k * num4 + num, boardColor);
				texture2D.SetPixel(j * num4 + num + 1, k * num4 + num + 1, boardColor);
				texture2D.SetPixel(j * num4 + num, k * num4 + num + 1, boardColor);
				texture2D.SetPixel(j * num4 + num + 1, k * num4 + num, boardColor);
			}
		}
		texture2D.Apply();
		return texture2D;
	}

	public void SetPixelsForSprite13x13(Sprite sprite, Color boardColor, bool blankIsBlack = false)
	{
		int length = blockColors.GetLength(0);
		int length2 = blockColors.GetLength(1);
		for (int i = 0; i < length; i++)
		{
			for (int j = 0; j < length2; j++)
			{
				Color color = default(Color);
				switch (blockColors[i, j])
				{
				case BlockColor.Blank:
					color = (blankIsBlack ? Color.black : boardColor);
					break;
				case BlockColor.Red:
					color = toolPaletteChoices[0];
					break;
				case BlockColor.Blue:
					color = toolPaletteChoices[1];
					break;
				case BlockColor.Green:
					color = toolPaletteChoices[2];
					break;
				case BlockColor.Yellow:
					color = toolPaletteChoices[3];
					break;
				case BlockColor.Orange:
					color = toolPaletteChoices[4];
					break;
				case BlockColor.Pink:
					color = toolPaletteChoices[5];
					break;
				case BlockColor.Purple:
					color = toolPaletteChoices[6];
					break;
				case BlockColor.White:
					color = toolPaletteChoices[7];
					break;
				}
				spriteColorsBuffer[j * 13 + i] = color;
			}
		}
		int x = (int)sprite.rect.x;
		int y = (int)sprite.rect.y;
		sprite.texture.SetPixels(x, y, 13, 13, spriteColorsBuffer);
	}

	public Texture2D UpdateTexture2D(ref Color[] pixels, ref Texture2D texture, CoverStyle coverStyle)
	{
		Color color = default(Color);
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				switch (blockColors[i, j])
				{
				case BlockColor.Blank:
					color = (coverStyle.blankIsBlack ? Color.black : coverStyle.emptyColor);
					break;
				case BlockColor.Red:
					color = toolPaletteChoices[0];
					break;
				case BlockColor.Blue:
					color = toolPaletteChoices[1];
					break;
				case BlockColor.Green:
					color = toolPaletteChoices[2];
					break;
				case BlockColor.Yellow:
					color = toolPaletteChoices[3];
					break;
				case BlockColor.Orange:
					color = toolPaletteChoices[4];
					break;
				case BlockColor.Pink:
					color = toolPaletteChoices[5];
					break;
				case BlockColor.Purple:
					color = toolPaletteChoices[6];
					break;
				case BlockColor.White:
					color = toolPaletteChoices[7];
					break;
				}
				num = i * coverStyle.step + coverStyle.margin;
				num2 = j * coverStyle.step + coverStyle.margin;
				texture.SetPixel(num, num2, color);
				texture.SetPixel(num + 1, num2 + 1, color);
				texture.SetPixel(num, num2 + 1, color);
				texture.SetPixel(num + 1, num2, color);
			}
		}
		texture.Apply();
		return texture;
	}

	public Texture2D GetTexture2D(Color boardColor, int margin = 4, int blockSize = 2, int blockGutter = 0, bool blankIsBlack = false)
	{
		int num = margin * 2 + blockSize * 13 + blockGutter * 12;
		Color[] array = new Color[num * num];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = boardColor;
		}
		int num2 = blockSize + blockGutter;
		Texture2D texture2D = new Texture2D(num, num, TextureFormat.ARGB32, false);
		texture2D.filterMode = FilterMode.Point;
		texture2D.wrapMode = TextureWrapMode.Clamp;
		texture2D.SetPixels(array);
		for (int j = 0; j < blockColors.GetLength(0); j++)
		{
			for (int k = 0; k < blockColors.GetLength(1); k++)
			{
				Color color = default(Color);
				switch (blockColors[j, k])
				{
				case BlockColor.Blank:
					color = (blankIsBlack ? Color.black : boardColor);
					break;
				case BlockColor.Red:
					color = toolPaletteChoices[0];
					break;
				case BlockColor.Blue:
					color = toolPaletteChoices[1];
					break;
				case BlockColor.Green:
					color = toolPaletteChoices[2];
					break;
				case BlockColor.Yellow:
					color = toolPaletteChoices[3];
					break;
				case BlockColor.Orange:
					color = toolPaletteChoices[4];
					break;
				case BlockColor.Pink:
					color = toolPaletteChoices[5];
					break;
				case BlockColor.Purple:
					color = toolPaletteChoices[6];
					break;
				case BlockColor.White:
					color = toolPaletteChoices[7];
					break;
				}
				texture2D.SetPixel(j * num2 + margin, k * num2 + margin, color);
				texture2D.SetPixel(j * num2 + margin + 1, k * num2 + margin + 1, color);
				texture2D.SetPixel(j * num2 + margin, k * num2 + margin + 1, color);
				texture2D.SetPixel(j * num2 + margin + 1, k * num2 + margin, color);
			}
		}
		texture2D.Apply();
		return texture2D;
	}

	public void GetDetailColorsNonAlloc(ref Color32[] colors, bool overwriteWithClear = true)
	{
		InitDetailChoices();
		int i = 0;
		int num = 0;
		for (; i < 13; i++)
		{
			int num2 = 0;
			while (num2 < 13)
			{
				BlockColor blockColor = blockColors[num2, i];
				if (blockColor != BlockColor.Blank || overwriteWithClear)
				{
					colors[num] = _DetailPaletteCoices[(uint)blockColor];
				}
				num2++;
				num++;
			}
		}
	}

	private void InitDetailChoices()
	{
		for (int i = 0; i < 8; i++)
		{
			GridLocation gridLocation = paletteChoiceTexCoordArray[i];
			_DetailPaletteCoices[i].a = (byte)(gridLocation.r * 8 + gridLocation.c);
		}
	}

	public void GetColorsNonAlloc(ref Color[] colors, bool overwriteWithClear = true)
	{
		Color color = Color.clear;
		Color clear = Color.clear;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				switch (blockColors[j, i])
				{
				case BlockColor.Blank:
					if (!overwriteWithClear)
					{
						continue;
					}
					color = clear;
					break;
				case BlockColor.Red:
					color = toolPaletteChoices[0];
					break;
				case BlockColor.Blue:
					color = toolPaletteChoices[1];
					break;
				case BlockColor.Green:
					color = toolPaletteChoices[2];
					break;
				case BlockColor.Yellow:
					color = toolPaletteChoices[3];
					break;
				case BlockColor.Orange:
					color = toolPaletteChoices[4];
					break;
				case BlockColor.Pink:
					color = toolPaletteChoices[5];
					break;
				case BlockColor.Purple:
					color = toolPaletteChoices[6];
					break;
				case BlockColor.White:
					color = toolPaletteChoices[7];
					break;
				}
				colors[i * 13 + j] = color;
			}
		}
	}

	public void ResetPaletteChoices()
	{
		toolPaletteChoices[0] = ColorUtilities.blockColorArrayRGB[0];
		toolPaletteChoices[1] = ColorUtilities.blockColorArrayRGB[4];
		toolPaletteChoices[2] = ColorUtilities.blockColorArrayRGB[3];
		toolPaletteChoices[3] = ColorUtilities.blockColorArrayRGB[2];
		toolPaletteChoices[4] = ColorUtilities.blockColorArrayRGB[1];
		toolPaletteChoices[5] = ColorUtilities.blockColorArrayRGB[6];
		toolPaletteChoices[6] = ColorUtilities.blockColorArrayRGB[5];
		toolPaletteChoices[7] = ColorUtilities.blockColorArrayRGB[7];
	}

	public void SetMinMaxBounds()
	{
		blockBounds = new BlockBounds(true);
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = blockColors[j, i];
				if (blockColor != BlockColor.Blank)
				{
					if (j <= blockBounds.xMin)
					{
						blockBounds.xMin = (byte)j;
					}
					if (j > blockBounds.xMax)
					{
						blockBounds.xMax = (byte)j;
					}
					if (i <= blockBounds.yMin)
					{
						blockBounds.yMin = (byte)i;
					}
					if (i > blockBounds.yMax)
					{
						blockBounds.yMax = (byte)i;
					}
				}
			}
		}
		if (blockBounds.xMax < blockBounds.xMin || blockBounds.yMax < blockBounds.yMin)
		{
			blockBounds.isEmpty = true;
			blockBounds.xMin = 0;
			blockBounds.yMin = 0;
			blockBounds.xMax = 12;
			blockBounds.yMax = 12;
		}
		else
		{
			blockBounds.isEmpty = false;
		}
		blockBounds.xMax++;
		blockBounds.yMax++;
	}

	public static BloxelBoard CreateRandomizedBoard(bool shouldSave = false)
	{
		BlockColor[,] array = new BlockColor[13, 13];
		switch (UnityEngine.Random.Range(0, 4))
		{
		case 0:
		{
			for (int num = 0; num < 13; num++)
			{
				for (int num2 = 0; num2 < 13; num2++)
				{
					if (num == 0)
					{
						array[num2, num] = BlockColor.Red;
					}
					else
					{
						array[num2, num] = BlockColor.Blank;
					}
				}
			}
			break;
		}
		case 1:
		{
			for (int k = 0; k < 13; k++)
			{
				for (int l = 0; l < 13; l++)
				{
					if (l == k)
					{
						array[l, k] = BlockColor.Blue;
					}
					else
					{
						array[l, k] = BlockColor.Blank;
					}
				}
			}
			break;
		}
		case 2:
		{
			for (int m = 0; m < 13; m++)
			{
				for (int n = 0; n < 13; n++)
				{
					if (n == 0)
					{
						array[n, m] = BlockColor.Green;
					}
					else
					{
						array[n, m] = BlockColor.Blank;
					}
				}
			}
			break;
		}
		case 3:
		{
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					if (j == 12 - i)
					{
						array[j, i] = BlockColor.Purple;
					}
					else
					{
						array[j, i] = BlockColor.Blank;
					}
				}
			}
			break;
		}
		}
		return new BloxelBoard(array, shouldSave);
	}

	public void FlipHorizontally(bool shouldSave = true)
	{
		BlockColor blockColor = BlockColor.Blank;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 6; j++)
			{
				blockColor = blockColors[j, i];
				blockColors[j, i] = blockColors[12 - j, i];
				blockColors[12 - j, i] = blockColor;
			}
		}
		if (shouldSave)
		{
			Save();
		}
		BlastUpdate();
	}

	public void FlipVertically(bool shouldSave = true)
	{
		BlockColor blockColor = BlockColor.Blank;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 6; j++)
			{
				blockColor = blockColors[i, j];
				blockColors[i, j] = blockColors[i, 12 - j];
				blockColors[i, 12 - j] = blockColor;
			}
		}
		if (shouldSave)
		{
			Save();
		}
		BlastUpdate();
	}

	public static BloxelBoard GenerateSymmetricalBoard(BloxelBoard b, BlockColor color, int totalBlocks, bool overwriteBlocks = false)
	{
		BlockColor[,] array = b.blockColors;
		HashSet<GridLocation> hashSet = new HashSet<GridLocation>();
		int num = 49;
		if (!overwriteBlocks)
		{
			for (int i = 0; i < 7; i++)
			{
				for (int j = 0; j < 7; j++)
				{
					if (array[i, j] != BlockColor.Blank)
					{
						hashSet.Add(new GridLocation(i, j));
						num--;
					}
				}
			}
		}
		totalBlocks = Mathf.Min(num, totalBlocks);
		int num2 = 0;
		while (num2 < totalBlocks)
		{
			GridLocation item = new GridLocation(UnityEngine.Random.Range(0, 7), UnityEngine.Random.Range(0, 7));
			if (hashSet.Add(item))
			{
				array[item.c, item.r] = ((color != BlockColor.Blank) ? color : ((BlockColor)UnityEngine.Random.Range(0, 7)));
				num2++;
			}
		}
		for (int k = 0; k < 7; k++)
		{
			for (int l = 0; l < 6; l++)
			{
				b.blockColors[12 - l, k] = b.blockColors[l, k];
			}
		}
		for (int m = 0; m < 6; m++)
		{
			for (int n = 0; n < 13; n++)
			{
				b.blockColors[n, 12 - m] = b.blockColors[n, m];
			}
		}
		return b;
	}
}
