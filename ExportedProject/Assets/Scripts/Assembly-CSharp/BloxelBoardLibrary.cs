using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloxelBoardLibrary : BloxelLibraryWindow
{
	public static BloxelBoardLibrary instance;

	public Object savedBoardPrefab;

	private BloxelBoard _currentSavedBoard;

	public bool initComplete;

	public BloxelBoard currentSavedBoard
	{
		get
		{
			return _currentSavedBoard;
		}
		private set
		{
			_currentSavedBoard = value;
		}
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
	}

	private new void Start()
	{
		if (!initComplete)
		{
			PopulateLibrary();
			base.Start();
			base.OnProjectSelect += SetCurrentProject;
			initComplete = true;
		}
	}

	private void OnDestroy()
	{
		base.OnProjectSelect -= SetCurrentProject;
	}

	public void SetCurrentProject(SavedProject item)
	{
		BloxelBoard bloxelBoard = item.dataModel as BloxelBoard;
		if (currentSavedBoard == null || currentSavedBoard != bloxelBoard)
		{
			currentSavedBoard = bloxelBoard;
			BloxelLibraryScrollController.instance.DisablePreviousSelectedOutline();
			SoundManager.PlayOneShot(SoundManager.instance.popA);
		}
	}

	private new void DestroyLibrary()
	{
		base.DestroyLibrary();
	}

	public void PopulateLibrary()
	{
		BloxelLibraryScrollController.instance.unfilteredBoardData.Clear();
		string item = "PixelTutz";
		string item2 = "PPHide";
		HashSet<string> hashSet = StartIgnoringBoards();
		foreach (BloxelBoard value3 in AssetManager.instance.boardPool.Values)
		{
			if (value3.tags.Contains(item) || value3.tags.Contains(item2))
			{
				hashSet.Add(value3.ID());
			}
		}
		foreach (BloxelMegaBoard value4 in AssetManager.instance.megaBoardPool.Values)
		{
			Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator3 = value4.boards.GetEnumerator();
			if (value4.tags.Contains(item) || value4.tags.Contains(item2))
			{
				while (enumerator3.MoveNext())
				{
					BloxelBoard value = enumerator3.Current.Value;
					hashSet.Add(value.ID());
				}
				continue;
			}
			while (enumerator3.MoveNext())
			{
				BloxelBoard value2 = enumerator3.Current.Value;
				if (value2.tags.Contains(item) || value2.tags.Contains(item2))
				{
					hashSet.Add(value2.ID());
				}
			}
		}
		foreach (BloxelAnimation value5 in AssetManager.instance.animationPool.Values)
		{
			for (int i = 0; i < value5.boards.Count; i++)
			{
				hashSet.Add(value5.boards[i].ID());
			}
		}
		foreach (BloxelLevel value6 in AssetManager.instance.levelPool.Values)
		{
			hashSet.Add(value6.coverBoard.ID());
		}
		foreach (BloxelGame value7 in AssetManager.instance.gamePool.Values)
		{
			hashSet.Add(value7.coverBoard.ID());
		}
		foreach (BloxelBoard value8 in AssetManager.instance.boardPool.Values)
		{
			if (!hashSet.Contains(value8.ID()))
			{
				BloxelLibraryScrollController.instance.unfilteredBoardData.Add(value8);
			}
		}
	}

	public override void UpdateScrollerContents()
	{
		BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Board);
	}

	private void Copy()
	{
		PixelEditorController.instance.copyBoardBuffer = currentSavedBoard;
	}

	private void Paste()
	{
		currentSavedBoard.blockColors = (BlockColor[,])PixelEditorController.instance.copyBoardBuffer.blockColors.Clone();
		currentSavedBoard.toolPaletteChoices = (Color[])PixelEditorController.instance.copyBoardBuffer.toolPaletteChoices.Clone();
		BloxelBoard bloxelBoard = currentSavedBoard;
		PixelEditorPaintBoard.instance.SwitchBloxelBoard(bloxelBoard);
		bloxelBoard.Save();
		ColorPalette.instance.SetPaletteTexCoordsForBoard(bloxelBoard);
		if (currentSavedBoard != null)
		{
			currentSavedBoard.blockColors = (BlockColor[,])PixelEditorController.instance.copyBoardBuffer.blockColors.Clone();
			currentSavedBoard.toolPaletteChoices = (Color[])PixelEditorController.instance.copyBoardBuffer.toolPaletteChoices.Clone();
			currentSavedBoard.BlastUpdate();
		}
	}

	public new void AddButtonPressed()
	{
		base.AddButtonPressed();
		BoardCanvas.instance.CreateNewBoard();
	}

	public void AddBoard(BloxelBoard b)
	{
		if (!AssetManager.instance.boardPool.ContainsKey(b.ID()))
		{
			if (UITagSearch.instance.hasFilter)
			{
				UITagSearch.instance.ClearResults();
			}
			BloxelLibraryScrollController.instance.AddItem(b);
			AssetManager.instance.boardPool.Add(b.ID(), b);
			b.SetBuildVersion(AppStateManager.instance.internalVersion);
			b.Save();
			StartCoroutine(DelayedItemSelect());
			ScrollPickerManager.instance.uiBoardPicker.AddBoard(b);
		}
	}

	private IEnumerator DelayedItemSelect()
	{
		yield return new WaitForEndOfFrame();
		ItemSelect(BloxelLibraryScrollController.instance.GetFirstItem());
	}

	public void Reset()
	{
		if (currentSavedBoard != null)
		{
			_currentSavedBoard = null;
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Board);
		}
	}
}
