using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json;
using UnityEngine;

public sealed class BloxelBrain : BloxelProject
{
	public delegate void HandleUpdate();

	public static int ExplosiveDeathThreshold;

	public static int HealthBonusThreshold;

	public static int HealthBurgleThreshold;

	public static int ScaleShrinkThreshold;

	public static int SpreadShotThreshold;

	public static int LaserThreshold;

	public static int LaserTrackingThreshold;

	public static int DryBonesThreshold;

	public static int BulletReflectionThreshold;

	public static int[][] Thresholds;

	public static readonly int HealthAndDamageQuantum = 2;

	private static float totalPossible = 169f;

	public static readonly int totalKitBlocks = 40;

	public static readonly int totalExpansionBlocks = 20;

	public static readonly int MaxBlocks = 20;

	public static readonly int MaxGlobalBrains = 6;

	public BlockColor[,] blockColors;

	public byte[] blockTotals;

	public int attackDamage;

	public bool shouldExplodeOnDeath;

	public int coinBonusQuantum;

	public int coinBurgleQuantum;

	public int healthBonusQuantum;

	public int healthBurgleQuantum;

	public static string filename = "brain.json";

	public static string rootDirectory = "Brains";

	public static string subDirectory = "myBrains";

	public static string defaultTitle = "My Brain";

	private static bool[][,] _patternMatchBlocks = new bool[8][,]
	{
		new bool[13, 13],
		new bool[13, 13],
		new bool[13, 13],
		new bool[13, 13],
		new bool[13, 13],
		new bool[13, 13],
		new bool[13, 13],
		new bool[13, 13]
	};

	private static Dictionary<BlockColor, KeyValuePair<string, string>> _uiTitles = new Dictionary<BlockColor, KeyValuePair<string, string>>
	{
		{
			BlockColor.Red,
			new KeyValuePair<string, string>("capture16", "Damage")
		},
		{
			BlockColor.Orange,
			new KeyValuePair<string, string>("capture20", "Speed")
		},
		{
			BlockColor.Yellow,
			new KeyValuePair<string, string>("capture19", "Bonus")
		},
		{
			BlockColor.Green,
			new KeyValuePair<string, string>("capture18", "Greed")
		},
		{
			BlockColor.Blue,
			new KeyValuePair<string, string>("capture17", "Size")
		},
		{
			BlockColor.Purple,
			new KeyValuePair<string, string>("capture22", "Projectile")
		},
		{
			BlockColor.Pink,
			new KeyValuePair<string, string>("capture21", "Strength")
		},
		{
			BlockColor.White,
			new KeyValuePair<string, string>(string.Empty, string.Empty)
		},
		{
			BlockColor.Blank,
			new KeyValuePair<string, string>(string.Empty, string.Empty)
		}
	};

	[CompilerGenerated]
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private HandleUpdate m_OnUpdate;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map14;

	public event HandleUpdate OnUpdate
	{
		add
		{
			HandleUpdate handleUpdate = this.m_OnUpdate;
			HandleUpdate handleUpdate2;
			do
			{
				handleUpdate2 = handleUpdate;
				handleUpdate = Interlocked.CompareExchange(ref this.m_OnUpdate, (HandleUpdate)Delegate.Combine(handleUpdate2, value), handleUpdate);
			}
			while (handleUpdate != handleUpdate2);
		}
		remove
		{
			HandleUpdate handleUpdate = this.m_OnUpdate;
			HandleUpdate handleUpdate2;
			do
			{
				handleUpdate2 = handleUpdate;
				handleUpdate = Interlocked.CompareExchange(ref this.m_OnUpdate, (HandleUpdate)Delegate.Remove(handleUpdate2, value), handleUpdate);
			}
			while (handleUpdate != handleUpdate2);
		}
	}

	public BloxelBrain(BlockColor[,] blocks)
	{
		blockColors = blocks;
		type = ProjectType.Brain;
		base.id = GenerateRandomID();
		projectPath = MyProjectPath() + "/" + ID();
		tags = new List<string>();
		title = defaultTitle;
		tags.Add("mine");
		coverBoard = new BloxelBoard(blockColors, false);
		CreateCounts();
		CalculateProperties();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelBrain(JsonTextReader reader)
	{
		type = ProjectType.Brain;
		FromReader(reader);
		coverBoard = new BloxelBoard(blockColors, false);
		CreateCounts();
		CalculateProperties();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelBrain(string s, DataSource source)
	{
		type = ProjectType.Brain;
		switch (source)
		{
		case DataSource.FilePath:
			try
			{
				string jsonString = File.ReadAllText(s + "/" + filename);
				FromJSONString(jsonString);
			}
			catch (Exception)
			{
			}
			break;
		case DataSource.JSONString:
			FromJSONString(s);
			break;
		}
		coverBoard = new BloxelBoard(blockColors, false);
		CreateCounts();
		CalculateProperties();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public void UpdateBlockColors(BlockColor[,] blocks)
	{
		Array.Copy(blocks, blockColors, blocks.Length);
		CreateCounts();
		CalculateProperties();
	}

	private void CreateCounts()
	{
		blockTotals = new byte[8];
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				BlockColor blockColor = blockColors[i, j];
				if (blockColor != BlockColor.Blank)
				{
					blockTotals[(uint)blockColor]++;
				}
			}
		}
	}

	public override int GetBloxelCount()
	{
		int num = 0;
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				if (blockColors[i, j] != BlockColor.Blank)
				{
					num++;
				}
			}
		}
		return num;
	}

	private void CalculateProperties()
	{
		attackDamage = 1 + Mathf.FloorToInt((float)(int)blockTotals[0] / (float)HealthAndDamageQuantum);
		DetermineExplosiveDeath();
		CalculateBonusQuanta();
		CalculateBurgleQuanta();
	}

	private void DetermineExplosiveDeath()
	{
		shouldExplodeOnDeath = blockTotals[0] >= ExplosiveDeathThreshold;
	}

	private void CalculateBonusQuanta()
	{
		int num = (coinBonusQuantum = blockTotals[2]);
		if (num >= HealthBonusThreshold)
		{
			healthBonusQuantum = 1 + (num - HealthBonusThreshold);
		}
		else
		{
			healthBonusQuantum = 0;
		}
	}

	private void CalculateBurgleQuanta()
	{
		if ((coinBurgleQuantum = blockTotals[3]) >= HealthBurgleThreshold)
		{
			healthBurgleQuantum = attackDamage;
		}
		else
		{
			healthBurgleQuantum = 0;
		}
	}

	private void MatchPatterns()
	{
		bool[,] pattern = new bool[5, 5]
		{
			{ true, true, true, true, true },
			{ true, false, false, false, true },
			{ true, false, false, false, true },
			{ true, false, false, false, true },
			{ true, true, true, true, true }
		};
		for (int i = 0; i < _patternMatchBlocks.GetLength(0); i++)
		{
			bool[,] brain = _patternMatchBlocks[i];
			for (int j = 0; j < 8; j++)
			{
				for (int k = 0; k < 8; k++)
				{
					if (CheckPattern(k, j, pattern, brain, 5))
					{
					}
				}
			}
		}
	}

	private bool CheckPattern(int startCol, int startRow, bool[,] pattern, bool[,] brain, int patternDimension)
	{
		int num = 0;
		int num2 = 0;
		int num3 = startRow;
		while (num2 < patternDimension && num3 < 13)
		{
			int num4 = 0;
			int num5 = startCol;
			while (num4 < patternDimension && num5 < 13)
			{
				if (pattern[num4, num2] == brain[num5, num3])
				{
					num++;
					num4++;
					num5++;
					continue;
				}
				return false;
			}
			num2++;
			num3++;
		}
		return num == pattern.Length;
	}

	public static string BaseStoragePath()
	{
		return AssetManager.baseStoragePath + rootDirectory;
	}

	public static string MyProjectPath()
	{
		return BaseStoragePath() + "/" + subDirectory;
	}

	public void FromReader(JsonTextReader reader)
	{
		string text = string.Empty;
		blockColors = new BlockColor[13, 13];
		blockTotals = new byte[8];
		int num = 0;
		tags = new List<string>();
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					if (text != null && text == "tags")
					{
						ParseTags(reader);
					}
				}
				else
				{
					if (text == null)
					{
						continue;
					}
					if (_003C_003Ef__switch_0024map14 == null)
					{
						Dictionary<string, int> dictionary = new Dictionary<string, int>(7);
						dictionary.Add("_id", 0);
						dictionary.Add("id", 1);
						dictionary.Add("colors", 2);
						dictionary.Add("title", 3);
						dictionary.Add("owner", 4);
						dictionary.Add("isDirty", 5);
						dictionary.Add("builtWithVersion", 6);
						_003C_003Ef__switch_0024map14 = dictionary;
					}
					int value;
					if (_003C_003Ef__switch_0024map14.TryGetValue(text, out value))
					{
						switch (value)
						{
						case 0:
							_serverID = reader.Value.ToString();
							break;
						case 1:
							base.id = new Guid(reader.Value.ToString());
							break;
						case 2:
						{
							int num2 = num % 13;
							int num3 = num / 13;
							blockColors[num3, num2] = (BlockColor)int.Parse(reader.Value.ToString());
							num++;
							break;
						}
						case 3:
							title = reader.Value.ToString();
							break;
						case 4:
							owner = reader.Value.ToString();
							break;
						case 5:
							isDirty = bool.Parse(reader.Value.ToString());
							break;
						case 6:
							SetBuildVersion(int.Parse(reader.Value.ToString()));
							break;
						}
					}
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
				break;
			}
		}
	}

	public void FromJSONString(string jsonString)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader);
	}

	private void ParseTags(JsonTextReader reader)
	{
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				tags.Add(reader.Value.ToString());
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	public string ToJSON()
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject jSONObject2 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject3 = new JSONObject(JSONObject.Type.ARRAY);
		for (int i = 0; i < tags.Count; i++)
		{
			jSONObject3.Add(tags[i]);
		}
		for (int j = 0; j < blockColors.GetLength(0); j++)
		{
			for (int k = 0; k < blockColors.GetLength(1); k++)
			{
				jSONObject2.Add(new JSONObject((int)blockColors[j, k]));
			}
		}
		jSONObject.AddField("id", base.id.ToString());
		jSONObject.AddField("owner", owner);
		jSONObject.AddField("colors", jSONObject2);
		jSONObject.AddField("tags", jSONObject3);
		jSONObject.AddField("isDirty", isDirty);
		jSONObject.AddField("builtWithVersion", builtWithVersion);
		return jSONObject.ToString();
	}

	public override bool Save(bool ballsDeep = true, bool shouldQueue = true)
	{
		bool flag = false;
		if (shouldQueue)
		{
			SaveManager.Instance.Enqueue(this);
			return false;
		}
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		string text = ToJSON();
		SetDirty(true);
		if (string.IsNullOrEmpty(text))
		{
			return false;
		}
		if (Directory.Exists(projectPath))
		{
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		else
		{
			Directory.CreateDirectory(projectPath);
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		if (File.Exists(projectPath + "/" + filename))
		{
			return true;
		}
		return false;
	}

	public override void SetDirty(bool isDirty)
	{
		base.SetDirty(isDirty);
		if (isDirty)
		{
			CloudManager.Instance.EnqueueFileForSync(syncInfo);
		}
	}

	public bool Delete()
	{
		bool flag = false;
		if (Directory.Exists(projectPath))
		{
			Directory.Delete(projectPath, true);
		}
		if (Directory.Exists(projectPath))
		{
			flag = false;
		}
		else
		{
			flag = true;
			if (ID() != null)
			{
				AssetManager.instance.brainPool.Remove(ID());
			}
		}
		CloudManager.Instance.RemoveFileFromQueue(syncInfo);
		return flag;
	}

	public static bool DeleteBrainWithID(string brainID)
	{
		bool flag = false;
		string path = MyProjectPath() + "/" + brainID;
		if (Directory.Exists(path))
		{
			Directory.Delete(path, true);
		}
		if (Directory.Exists(path))
		{
			flag = false;
		}
		else
		{
			flag = true;
			if (brainID != null)
			{
				AssetManager.instance.brainPool.Remove(brainID);
			}
		}
		return flag;
	}

	public static DirectoryInfo[] GetSavedBrains()
	{
		DirectoryInfo directoryInfo = null;
		string path = MyProjectPath();
		if (Directory.Exists(path))
		{
			directoryInfo = new DirectoryInfo(path);
			DirectoryInfo[] directories = directoryInfo.GetDirectories();
			int num = directories.Length;
			for (int i = 0; i < num; i++)
			{
				DirectoryInfo directoryInfo2 = directories[i];
				if (!File.Exists(directoryInfo2.FullName + "/" + filename))
				{
					directoryInfo2.Delete();
				}
			}
		}
		else
		{
			Directory.CreateDirectory(path);
			directoryInfo = new DirectoryInfo(path);
		}
		return (from p in directoryInfo.GetDirectories()
			orderby p.LastWriteTime descending
			select p).ToArray();
	}

	public static bool CheckSaveFileExistsInDirectory(string directoryPath)
	{
		bool result = false;
		if (Directory.Exists(directoryPath))
		{
			if (File.Exists(directoryPath + "/" + filename))
			{
				result = true;
			}
			else
			{
				result = false;
				DeleteDirectory(directoryPath);
			}
		}
		return result;
	}

	public static bool DeleteDirectory(string directoryPath)
	{
		bool flag = false;
		if (Directory.Exists(directoryPath))
		{
			Directory.Delete(directoryPath, true);
		}
		if (Directory.Exists(directoryPath))
		{
			return false;
		}
		return true;
	}

	public static BloxelBrain GetSavedBrainById(string _id)
	{
		return new BloxelBrain(MyProjectPath() + "/" + _id, DataSource.FilePath);
	}

	public static BloxelBrain GenerateSingleColorBrain(BlockColor color, float percentage)
	{
		BlockColor[,] array = new BlockColor[13, 13];
		for (int i = 0; i < array.GetLength(0); i++)
		{
			for (int j = 0; j < array.GetLength(1); j++)
			{
				array[i, j] = BlockColor.Blank;
			}
		}
		for (int k = 0; k < array.GetLength(0); k++)
		{
			for (int l = 0; l < array.GetLength(1) && !((float)(k * l) >= percentage * 169f); l++)
			{
				array[k, l] = color;
			}
		}
		return new BloxelBrain(array);
	}

	public static string GetTitleFromColor(BlockColor color)
	{
		KeyValuePair<string, string> keyValuePair = _uiTitles[color];
		string key = keyValuePair.Key;
		string value = keyValuePair.Value;
		return LocalizationManager.getInstance().getLocalizedText(key, value);
	}
}
