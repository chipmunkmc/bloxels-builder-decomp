using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class BloxelCamera : MonoBehaviour
{
	public enum CameraMode
	{
		None = 0,
		Gameplay = 1,
		Gameplay2D = 2,
		GameBuilder = 3,
		InfiniteCanvas = 4,
		CharacterConfig = 5,
		GameplayCapture = 6,
		GameplayCaptureCharacter = 7,
		GameplayConfigEnemy = 8
	}

	public delegate void HandleModeChange();

	public static BloxelCamera instance;

	public Transform selfTransform;

	public Camera mainCamera;

	public GameObject target;

	public BloxelCameraController activeController;

	public bool gameplayEnabled;

	public GameplayCameraController _gameplayCameraController;

	private GameplayCameraController2D _gameplayCameraController2D;

	private GameBuilderCameraController _gameBuilderCameraController;

	private GameplayCaptureCameraController _gameplayCaptureCameraController;

	private GameplayConfigEnemyController _gameplayConfigEnemyController;

	private GameplayCaptureCharacterCameraController _gameplayCaptureCharacterCameraController;

	public PerlinShake cameraShake;

	public BlurOptimized blur;

	public bool blurActive;

	public Vector3 positionInfinite = new Vector3(6f, 155f, -100f);

	public float infiniteOrthoSize = 450f;

	public Vector3 standardGameplayOffset = new Vector3(0f, 16f, -80f);

	public Vector3 positionCharacterConfig = new Vector3(-6.5f, 0f, -40f);

	public Vector3 rotationCharacterConfig = new Vector3(0f, 0f, 0f);

	public Tweener blurTween;

	public Tweener deBlurTween;

	public bool processRenderTexture;

	public int blurIterations;

	public float blurSize;

	public RenderTexture renderTexture;

	private bool initComplete;

	public CameraMode mode { get; private set; }

	public event HandleModeChange OnModeChange;

	private void Awake()
	{
		if (initComplete)
		{
			return;
		}
		if (instance != null)
		{
			UnityEngine.Object.DestroyImmediate(base.gameObject);
			return;
		}
		instance = this;
		processRenderTexture = false;
		selfTransform = base.transform;
		mainCamera = GetComponent<Camera>();
		_gameplayCameraController = new GameplayCameraController(selfTransform, mainCamera, standardGameplayOffset);
		_gameplayCameraController2D = new GameplayCameraController2D(selfTransform, mainCamera, new Vector3(0f, 10f, -80f));
		_gameBuilderCameraController = new GameBuilderCameraController(selfTransform, mainCamera, new Vector3(0f, 20f, -80f));
		_gameplayCaptureCameraController = new GameplayCaptureCameraController(selfTransform, mainCamera, Vector3.zero);
		_gameplayCaptureCharacterCameraController = new GameplayCaptureCharacterCameraController(selfTransform, mainCamera, Vector3.zero);
		_gameplayConfigEnemyController = new GameplayConfigEnemyController(selfTransform, mainCamera, Vector3.zero);
		if (blur == null)
		{
			blur = GetComponent<BlurOptimized>();
		}
		if (cameraShake == null)
		{
			cameraShake = GetComponent<PerlinShake>();
		}
		blurTween = null;
		deBlurTween = null;
		if (!DeviceManager.isPoopDevice)
		{
			blurIterations = 2;
		}
		else
		{
			blurIterations = 1;
		}
		blurSize = 4f;
		ResetBlur();
		initComplete = true;
	}

	private void LateUpdate()
	{
		if (mode != 0 && mode != CameraMode.InfiniteCanvas && mode != CameraMode.CharacterConfig)
		{
			activeController.UpdateCameraPosition();
		}
	}

	private void ResetBlur()
	{
		if (DeviceManager.isPoopDevice)
		{
			UnityEngine.Object.Destroy(blur);
			return;
		}
		blur.enabled = false;
		blur.downsample = 0;
		blur.blurSize = 0f;
		blur.blurIterations = 0;
	}

	public Tweener Blur(Graphic graphic)
	{
		if (DeviceManager.isPoopDevice)
		{
			return null;
		}
		if (deBlurTween != null && deBlurTween.IsPlaying())
		{
			deBlurTween.Kill();
		}
		graphic.color = new Color(0f, 0f, 0f, 0f);
		blur.blurIterations = blurIterations;
		blurTween = graphic.DOFade(0.45f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			blur.enabled = true;
			blurActive = true;
			DOTween.To(() => blur.blurSize, delegate(float x)
			{
				blur.blurSize = x;
			}, blurSize, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				blur.downsample = 2;
			});
		});
		return blurTween;
	}

	public Tweener DeBlur(Graphic graphic)
	{
		if (DeviceManager.isPoopDevice)
		{
			return null;
		}
		if (blurTween != null && blurTween.IsPlaying())
		{
			blurTween.Kill();
		}
		deBlurTween = graphic.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			blurActive = false;
			DOTween.To(() => blur.blurSize, delegate(float x)
			{
				blur.blurSize = x;
			}, 0f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				blur.downsample = 0;
				blur.blurIterations = 0;
				blur.enabled = false;
			});
		});
		return deBlurTween;
	}

	public void ForceDeBlur(Graphic graphic = null)
	{
		if (graphic != null)
		{
			graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, 0f);
		}
		blurTween.Kill();
		blurActive = false;
		blur.blurSize = 0f;
		blur.downsample = 0;
		blur.blurIterations = 0;
		blur.enabled = false;
	}

	public void RenderAndUpload()
	{
		renderTexture = new RenderTexture(Screen.width, Screen.height, 24);
		mainCamera.targetTexture = renderTexture;
		mainCamera.Render();
		RenderTexture.active = renderTexture;
		Texture2D texture = new Texture2D(renderTexture.width, renderTexture.height, TextureFormat.RGB24, false);
		texture.ReadPixels(new Rect(0f, 0f, renderTexture.width, renderTexture.height), 0, 0);
		texture.Apply();
		RenderTexture.active = null;
		renderTexture = null;
		mainCamera.targetTexture = null;
		StartCoroutine(BloxelServerRequests.instance.UploadScreenshot(GameplayController.instance.currentGame._serverID, texture, delegate(string res)
		{
			if (!string.IsNullOrEmpty(res) && res != "ERROR")
			{
				texture = null;
				GameObject gameObject = UnityEngine.Object.Instantiate(Resources.Load("Prefabs/ScreenShotPoof")) as GameObject;
				gameObject.transform.SetParent(UIOverlayCanvas.instance.transform);
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.localPosition = Vector3.one;
			}
		}));
	}

	public void SwitchCameraMode(CameraMode newMode)
	{
		if (gameplayEnabled = newMode.Equals(CameraMode.Gameplay2D) || newMode.Equals(CameraMode.Gameplay))
		{
			if (target == null)
			{
				target = GameplayController.instance.heroObject;
			}
			selfTransform.position = new Vector3(target.transform.position.x, target.transform.position.y, selfTransform.position.z);
		}
		if (mode.Equals(newMode))
		{
			return;
		}
		mode = newMode;
		switch (newMode)
		{
		case CameraMode.Gameplay:
			activeController = _gameplayCameraController;
			HandleUnshrink(true);
			break;
		case CameraMode.Gameplay2D:
			activeController = _gameplayCameraController2D;
			break;
		case CameraMode.GameBuilder:
			activeController = _gameBuilderCameraController;
			if (GameBuilderCanvas.instance.currentGame != null)
			{
				GameplayBuilder.instance.Reset();
				SetGameBuilderCamToLevel(GameBuilderCanvas.instance.currentBloxelLevel.location);
			}
			break;
		case CameraMode.GameplayCapture:
			activeController = _gameplayCaptureCameraController;
			break;
		case CameraMode.GameplayCaptureCharacter:
			activeController = _gameplayCaptureCharacterCameraController;
			break;
		case CameraMode.GameplayConfigEnemy:
			activeController = _gameplayConfigEnemyController;
			break;
		case CameraMode.InfiniteCanvas:
			SetToInfinite();
			return;
		case CameraMode.CharacterConfig:
			return;
		case CameraMode.None:
			activeController = null;
			return;
		}
		activeController.ApplyCameraProperties();
		if (this.OnModeChange != null)
		{
			this.OnModeChange();
		}
	}

	public void SetToCharacterConfig()
	{
		mainCamera.orthographic = false;
		base.transform.localPosition = positionCharacterConfig;
		base.transform.DORotate(rotationCharacterConfig, 0f);
	}

	public void SetToInfinite()
	{
		mainCamera.orthographic = true;
		mainCamera.orthographicSize = infiniteOrthoSize;
		base.transform.localPosition = positionInfinite;
		base.transform.rotation = Quaternion.identity;
	}

	public void SetGameBuilderCamToLevel(GridLocation levelLocationInWorld)
	{
		SwitchCameraMode(CameraMode.GameBuilder);
		Vector3 positionInWorldWrapper = new Vector3(((float)levelLocationInWorld.c + 0.5f) * 169f, ((float)levelLocationInWorld.r + 0.5f) * 169f, 0f);
		_gameBuilderCameraController.SetupCameraForTargetPosition(positionInWorldWrapper);
		WorldWrapper.instance.MoveLevelBounds();
		WorldWrapper.instance.MoveCharacterPlacementGrid();
		GameplayBuilder.instance.StartStreaming();
		ResetGameBackground();
	}

	public void SetToLevel(GridLocation levelLocationInWorld)
	{
		Vector3 position = new Vector3(((float)levelLocationInWorld.c + 0.5f) * 169f + 20f, ((float)levelLocationInWorld.r + 0.5f) * 169f + 20f, -45f);
		selfTransform.position = position;
		GameplayBuilder.instance.StartStreaming();
		ResetGameBackground();
	}

	public void MoveToLevelAtLocation(GridLocation levelLocationInWorld)
	{
		ShortcutExtensions.DOMove(endValue: new Vector3(((float)levelLocationInWorld.c + 0.5f) * 169f + 20f, ((float)levelLocationInWorld.r + 0.5f) * 169f + 20f, -45f), target: selfTransform, duration: UIAnimationManager.speedSlowAsCrap);
	}

	private void ResetGameBackground()
	{
		LevelBuilder.instance.gameBackground.transform.rotation = Quaternion.identity;
		float num = mainCamera.orthographicSize * mainCamera.aspect;
		LevelBuilder.instance.gameBackground.transform.localScale = new Vector3(num * 2f * 1.25f, num * 2f * 1.25f, 1f);
		LevelBuilder.instance.gameBackground.transform.position = mainCamera.transform.TransformPoint(new Vector3(0f, 0f, 500f));
		Vector3 backgroundOffset = GetBackgroundOffset();
		LevelBuilder.instance.gameBackground.transform.position = mainCamera.transform.TransformPoint(new Vector3(backgroundOffset.x, backgroundOffset.y, 500f));
	}

	private Vector3 GetBackgroundOffset()
	{
		Rect worldBoundingRect = GameplayBuilder.instance.game.worldBoundingRect;
		Vector3 vector = WorldWrapper.instance.selfTransform.InverseTransformPoint(mainCamera.transform.position);
		Vector2 vector2 = new Vector2(worldBoundingRect.center.x - vector.x, worldBoundingRect.center.y - vector.y);
		Vector2 vector3 = new Vector2(worldBoundingRect.width / 2f, worldBoundingRect.height / 2f);
		float num = mainCamera.orthographicSize * mainCamera.aspect * 2f;
		float x = LevelBuilder.instance.gameBackground.transform.localScale.x;
		float num2 = (x - num) / 2f;
		float x2 = vector2.x / vector3.x * num2;
		float y = vector2.y / vector3.y * num2 / 4f;
		return new Vector3(x2, y, 0f);
	}

	public void HandleShrinkPowerUp(bool immediate = false)
	{
		if (mode == CameraMode.Gameplay)
		{
			((GameplayCameraController)activeController).SetToShrunken(immediate);
		}
	}

	public void HandleUnshrink(bool immediate = false)
	{
		if (mode == CameraMode.Gameplay)
		{
			((GameplayCameraController)activeController).SetToStandard(immediate);
		}
	}
}
