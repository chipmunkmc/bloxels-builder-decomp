using UnityEngine;

public abstract class BloxelCameraController
{
	public GameObject target;

	public Vector3 cameraTargetPosition;

	protected Transform cameraTransform;

	public Camera cam;

	public Vector3 cameraOffset;

	public Vector3 previousCameraOffset;

	public abstract void ApplyCameraProperties();

	public abstract void UpdateCameraPosition();

	public void SetTempCameraOffset(Vector3 tempOffset)
	{
		previousCameraOffset = cameraOffset;
		cameraOffset = tempOffset;
	}

	public void SetCamerOffsetToPrevious()
	{
		cameraOffset = previousCameraOffset;
	}
}
