using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using UnityEngine;

public sealed class BloxelCharacter : BloxelProject
{
	public static string filename = "character.json";

	public static string rootDirectory = "Characters";

	public static string subDirectory = "myCharacters";

	public static string defaultTitle = "My Character";

	public Dictionary<AnimationType, BloxelAnimation> animations;

	public Rect boundingRect;

	public static Rect defaultBoundingRect = new Rect(0f, -2.5f, 8f, 10f);

	public float characterJumpHeight;

	public float characterInertia;

	public float characterRunSpeed;

	public float characterGravity;

	public bool doubleJump = true;

	private static GridLocation defaultMedianColorCoord = new GridLocation(0, 0);

	public Vector2 colliderOffset;

	public Vector2 colliderSize;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map7;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map8;

	public GridLocation medianPaletteCoord { get; private set; }

	public BloxelCharacter(List<BloxelAnimation> anims = null)
	{
		type = ProjectType.Character;
		base.id = GenerateRandomID();
		projectPath = MyProjectPath() + "/" + ID();
		if (anims != null)
		{
			animations = new Dictionary<AnimationType, BloxelAnimation>
			{
				{
					AnimationType.Idle,
					anims[0]
				},
				{
					AnimationType.Walk,
					anims[1]
				},
				{
					AnimationType.Jump,
					anims[2]
				}
			};
			coverBoard = anims[0].coverBoard;
		}
		else
		{
			BloxelBoard board = new BloxelBoard();
			BloxelBoard board2 = new BloxelBoard();
			BloxelBoard board3 = new BloxelBoard();
			BloxelAnimation bloxelAnimation = new BloxelAnimation(board);
			BloxelAnimation value = new BloxelAnimation(board2);
			BloxelAnimation value2 = new BloxelAnimation(board3);
			animations = new Dictionary<AnimationType, BloxelAnimation>
			{
				{
					AnimationType.Idle,
					bloxelAnimation
				},
				{
					AnimationType.Walk,
					value
				},
				{
					AnimationType.Jump,
					value2
				}
			};
			coverBoard = bloxelAnimation.coverBoard;
		}
		boundingRect = defaultBoundingRect;
		tags = new List<string>();
		tags.Add("mine");
		characterJumpHeight = PixelPlayerController.defaultJumpHeight;
		characterInertia = PixelPlayerController.defaultInertia;
		characterRunSpeed = PixelPlayerController.defaultRunSpeed;
		characterGravity = PixelPlayerController.defaultPlayerGravity;
		doubleJump = true;
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelCharacter(string s, DataSource source)
	{
		type = ProjectType.Character;
		if (source == DataSource.FilePath)
		{
			try
			{
				string jsonString = File.ReadAllText(s + "/" + filename);
				FromJSONString(jsonString);
			}
			catch (Exception)
			{
			}
		}
		else
		{
			FromJSONString(s, true);
		}
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelCharacter(JsonTextReader reader, bool fromServer = false)
	{
		type = ProjectType.Character;
		FromReader(reader, fromServer);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelCharacter(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		type = ProjectType.Character;
		FromReaderSlim(reader, projectPool);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public static string BaseStoragePath()
	{
		return AssetManager.baseStoragePath + rootDirectory;
	}

	public static string MyProjectPath()
	{
		return BaseStoragePath() + "/" + subDirectory;
	}

	public void FromReader(JsonTextReader reader, bool fromServer = false)
	{
		string text = string.Empty;
		tags = new List<string>();
		animations = new Dictionary<AnimationType, BloxelAnimation>(3);
		while (reader.Read())
		{
			if (reader.Value == null)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				if (text == "animations")
				{
					ParseAnimations(reader, fromServer);
				}
				if (text == "animation_data")
				{
					ParseAnimations(reader, fromServer);
				}
			}
			else
			{
				if (text == null)
				{
					continue;
				}
				if (_003C_003Ef__switch_0024map7 == null)
				{
					Dictionary<string, int> dictionary = new Dictionary<string, int>(13);
					dictionary.Add("id", 0);
					dictionary.Add("title", 1);
					dictionary.Add("animations", 2);
					dictionary.Add("tags", 3);
					dictionary.Add("owner", 4);
					dictionary.Add("characterJumpHeight", 5);
					dictionary.Add("characterInertia", 6);
					dictionary.Add("characterRunSpeed", 7);
					dictionary.Add("characterGravity", 8);
					dictionary.Add("doubleJump", 9);
					dictionary.Add("isDirty", 10);
					dictionary.Add("builtWithVersion", 11);
					dictionary.Add("boundingRect", 12);
					_003C_003Ef__switch_0024map7 = dictionary;
				}
				int value;
				if (_003C_003Ef__switch_0024map7.TryGetValue(text, out value))
				{
					switch (value)
					{
					case 0:
						base.id = new Guid(reader.Value.ToString());
						break;
					case 1:
						title = reader.Value.ToString();
						break;
					case 2:
						ParseAnimations(reader, fromServer);
						break;
					case 3:
						tags.Add(reader.Value.ToString());
						break;
					case 4:
						owner = reader.Value.ToString();
						break;
					case 5:
						characterJumpHeight = float.Parse(reader.Value.ToString());
						break;
					case 6:
						characterInertia = float.Parse(reader.Value.ToString());
						break;
					case 7:
						characterRunSpeed = float.Parse(reader.Value.ToString());
						break;
					case 8:
						characterGravity = float.Parse(reader.Value.ToString());
						break;
					case 9:
						doubleJump = bool.Parse(reader.Value.ToString());
						break;
					case 10:
						isDirty = bool.Parse(reader.Value.ToString());
						break;
					case 11:
						SetBuildVersion(int.Parse(reader.Value.ToString()));
						break;
					case 12:
						ParseBoundingRect(reader);
						FinalizeCharacter();
						return;
					}
				}
			}
		}
	}

	private void FromReaderSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		tags = new List<string>(1);
		animations = new Dictionary<AnimationType, BloxelAnimation>(3);
		while (reader.Read())
		{
			if (reader.Value == null)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				if (text == "animations")
				{
					ParseAnimationsSlim(reader, projectPool);
				}
			}
			else
			{
				if (text == null)
				{
					continue;
				}
				if (_003C_003Ef__switch_0024map8 == null)
				{
					Dictionary<string, int> dictionary = new Dictionary<string, int>(12);
					dictionary.Add("id", 0);
					dictionary.Add("title", 1);
					dictionary.Add("tags", 2);
					dictionary.Add("owner", 3);
					dictionary.Add("characterJumpHeight", 4);
					dictionary.Add("characterInertia", 5);
					dictionary.Add("characterRunSpeed", 6);
					dictionary.Add("characterGravity", 7);
					dictionary.Add("doubleJump", 8);
					dictionary.Add("isDirty", 9);
					dictionary.Add("builtWithVersion", 10);
					dictionary.Add("boundingRect", 11);
					_003C_003Ef__switch_0024map8 = dictionary;
				}
				int value;
				if (_003C_003Ef__switch_0024map8.TryGetValue(text, out value))
				{
					switch (value)
					{
					case 0:
						base.id = new Guid(reader.Value.ToString());
						break;
					case 1:
						title = reader.Value.ToString();
						break;
					case 2:
						tags.Add(reader.Value.ToString());
						break;
					case 3:
						owner = reader.Value.ToString();
						break;
					case 4:
						characterJumpHeight = float.Parse(reader.Value.ToString());
						break;
					case 5:
						characterInertia = float.Parse(reader.Value.ToString());
						break;
					case 6:
						characterRunSpeed = float.Parse(reader.Value.ToString());
						break;
					case 7:
						characterGravity = float.Parse(reader.Value.ToString());
						break;
					case 8:
						doubleJump = bool.Parse(reader.Value.ToString());
						break;
					case 9:
						isDirty = bool.Parse(reader.Value.ToString());
						break;
					case 10:
						SetBuildVersion(int.Parse(reader.Value.ToString()));
						break;
					case 11:
						ParseBoundingRect(reader);
						FinalizeCharacter();
						return;
					}
				}
			}
		}
	}

	private void FinalizeCharacter()
	{
		projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
		if (characterJumpHeight == 0f)
		{
			characterJumpHeight = PixelPlayerController.defaultJumpHeight;
		}
		if (characterInertia == 0f)
		{
			characterInertia = PixelPlayerController.defaultInertia;
		}
		if (characterRunSpeed == 0f)
		{
			characterRunSpeed = PixelPlayerController.defaultRunSpeed;
		}
		if (characterGravity == 0f)
		{
			characterGravity = PixelPlayerController.defaultPlayerGravity;
		}
		if (animations.Count > 0)
		{
			coverBoard = animations.First().Value.coverBoard;
		}
		else
		{
			coverBoard = new BloxelBoard();
		}
	}

	public void FromJSONString(string jsonString, bool fromServer = false)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader, fromServer);
	}

	private void ParseBoundingRect(JsonTextReader reader)
	{
		float[] array = new float[4];
		int num = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				array[num++] = float.Parse(reader.Value.ToString());
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				if (array != null && array.Length == 4)
				{
					boundingRect = new Rect(array[0], array[1], array[2], array[3]);
				}
				else
				{
					boundingRect = defaultBoundingRect;
				}
				break;
			}
		}
	}

	private void ParseAnimations(JsonTextReader reader, bool fromServer = false)
	{
		if (fromServer)
		{
			int num = 0;
			while (reader.Read())
			{
				if (reader.TokenType == JsonToken.StartObject)
				{
					BloxelAnimation value = new BloxelAnimation(reader, fromServer);
					animations[(AnimationType)(num++)] = value;
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					break;
				}
			}
			return;
		}
		int num2 = 0;
		string empty = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				empty = reader.Value.ToString();
				BloxelAnimation value2 = null;
				if (AssetManager.instance.animationPool.TryGetValue(empty, out value2))
				{
					animations[(AnimationType)(num2++)] = value2;
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseAnimationsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		int num = 0;
		string empty = string.Empty;
		string empty2 = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.String)
				{
					empty = reader.Value.ToString();
					BloxelProject value = null;
					if (projectPool.TryGetValue(empty, out value) && value.type == ProjectType.Animation)
					{
						animations[(AnimationType)(num++)] = value as BloxelAnimation;
					}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void InitColliderSizeFromAnimations()
	{
		int num = 100;
		int num2 = 100;
		int num3 = -1;
		int num4 = -1;
		Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator = animations.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelAnimation value = enumerator.Current.Value;
			int count = value.boards.Count;
			for (int i = 0; i < count; i++)
			{
				BloxelBoard bloxelBoard = value.boards[i];
				for (int j = 0; j < 13; j++)
				{
					for (int k = 0; k < 13; k++)
					{
						BlockColor blockColor = bloxelBoard.blockColors[k, j];
						if (!blockColor.Equals(BlockColor.Blank))
						{
							if (k < num)
							{
								num = k;
							}
							if (k > num3)
							{
								num3 = k;
							}
							if (j < num2)
							{
								num2 = j;
							}
							if (j > num4)
							{
								num4 = j;
							}
						}
					}
				}
			}
		}
		if (num3 < 0)
		{
			colliderOffset = new Vector2(0f, 6f);
			colliderSize = new Vector2(8f, 10f);
		}
	}

	public void InitPaletteCoordModeForIdle()
	{
		BloxelAnimation value = null;
		if (!animations.TryGetValue(AnimationType.Idle, out value))
		{
			medianPaletteCoord = defaultMedianColorCoord;
			return;
		}
		int count = value.boards.Count;
		if (count == 0)
		{
			medianPaletteCoord = defaultMedianColorCoord;
			return;
		}
		int[] array = new int[count];
		for (int i = 0; i < count; i++)
		{
			BlockColor blockColorMode = PPUtilities.GetBlockColorMode(value.boards[i].blockColors);
			GridLocation gridLocation = defaultMedianColorCoord;
			if (blockColorMode != BlockColor.Blank)
			{
				gridLocation = value.boards[i].paletteChoiceTexCoordArray[(uint)blockColorMode];
			}
			array[i] = gridLocation.r * 13 + gridLocation.c;
		}
		int gridLocationModeInt = PPUtilities.GetGridLocationModeInt(array, count);
		int num = gridLocationModeInt % 13;
		int row = (gridLocationModeInt - num) / 13;
		medianPaletteCoord = new GridLocation(num, row);
	}

	public string ToJSON()
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject jSONObject2 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject3 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject4 = new JSONObject(JSONObject.Type.ARRAY);
		foreach (BloxelAnimation value in animations.Values)
		{
			jSONObject2.Add(value.ID());
		}
		jSONObject.AddField("id", base.id.ToString());
		jSONObject.AddField("title", title);
		jSONObject.AddField("owner", owner);
		jSONObject.AddField("animations", jSONObject2);
		for (int i = 0; i < tags.Count; i++)
		{
			jSONObject3.Add(tags[i]);
		}
		jSONObject4.Add(boundingRect.x);
		jSONObject4.Add(boundingRect.y);
		jSONObject4.Add(boundingRect.width);
		jSONObject4.Add(boundingRect.height);
		jSONObject.AddField("characterJumpHeight", characterJumpHeight);
		jSONObject.AddField("characterInertia", characterInertia);
		jSONObject.AddField("characterRunSpeed", characterRunSpeed);
		jSONObject.AddField("characterGravity", characterGravity);
		jSONObject.AddField("doubleJump", doubleJump);
		jSONObject.AddField("tags", jSONObject3);
		jSONObject.AddField("isDirty", isDirty);
		jSONObject.AddField("builtWithVersion", builtWithVersion);
		jSONObject.AddField("boundingRect", jSONObject4);
		return jSONObject.ToString();
	}

	public override bool Save(bool deepSave = true, bool shouldQueue = true)
	{
		bool flag = false;
		if (shouldQueue)
		{
			if (deepSave)
			{
				Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator = animations.GetEnumerator();
				while (enumerator.MoveNext())
				{
					enumerator.Current.Value.Save();
				}
			}
			SaveManager.Instance.Enqueue(this);
			return false;
		}
		if (deepSave)
		{
			Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator2 = animations.GetEnumerator();
			while (enumerator2.MoveNext())
			{
				enumerator2.Current.Value.Save(true, false);
			}
		}
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		string text = ToJSON();
		SetDirty(true);
		if (string.IsNullOrEmpty(text))
		{
			return false;
		}
		if (Directory.Exists(projectPath))
		{
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		else
		{
			Directory.CreateDirectory(projectPath);
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		if (File.Exists(projectPath + "/" + filename))
		{
			return true;
		}
		return false;
	}

	public override void SetDirty(bool isDirty)
	{
		base.SetDirty(isDirty);
		if (isDirty)
		{
			CloudManager.Instance.EnqueueFileForSync(syncInfo);
		}
	}

	public bool Delete()
	{
		bool result = false;
		int num = 0;
		Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator = animations.GetEnumerator();
		while (enumerator.MoveNext())
		{
			if (enumerator.Current.Value.Delete())
			{
				num++;
			}
		}
		if (num < animations.Count)
		{
			return result;
		}
		if (Directory.Exists(projectPath))
		{
			Directory.Delete(projectPath, true);
		}
		if (Directory.Exists(projectPath))
		{
			result = false;
		}
		else
		{
			result = true;
			if (ID() != null)
			{
				AssetManager.instance.characterPool.Remove(ID());
			}
		}
		CloudManager.Instance.RemoveFileFromQueue(syncInfo);
		return result;
	}

	public static DirectoryInfo[] GetSavedProjects()
	{
		DirectoryInfo directoryInfo = null;
		string path = MyProjectPath();
		if (Directory.Exists(path))
		{
			directoryInfo = new DirectoryInfo(path);
		}
		else
		{
			Directory.CreateDirectory(path);
			directoryInfo = new DirectoryInfo(path);
		}
		return (from p in directoryInfo.GetDirectories()
			orderby p.LastWriteTime descending
			select p).ToArray();
	}

	public static BloxelCharacter GetSavedProjectById(string idString)
	{
		return new BloxelCharacter(MyProjectPath() + "/" + idString, DataSource.FilePath);
	}

	public void SetAnimationForState(AnimationType type, BloxelAnimation anim, bool shouldSave = true)
	{
		animations[type] = anim;
		if (type == AnimationType.Idle)
		{
			coverBoard = anim.coverBoard;
		}
		if (shouldSave)
		{
			Save(false);
		}
	}

	public new void SetTitle(string _title)
	{
		title = _title;
		Save(false);
	}

	public new void SetOwner(string _owner)
	{
		base.SetOwner(_owner);
		Save(false);
	}

	public new void CheckAndSetMeAsOwner()
	{
		base.CheckAndSetMeAsOwner();
		Save(false);
	}

	public new void AddTag(string tag)
	{
		base.AddTag(tag);
		Save(false);
	}

	public new void AddMultipleTags(List<string> tagsToAdd)
	{
		base.AddMultipleTags(tagsToAdd);
		Save(false);
	}

	public new void RemoveTag(string tag)
	{
		base.RemoveTag(tag);
		Save(false);
	}

	public void SetBoundingRect(Rect rect)
	{
		boundingRect = rect;
		Save(false);
	}

	public void SetBoundingRect(float x, float y, float width, float height)
	{
		boundingRect = new Rect(x, y, width, height);
		Save(false);
	}

	public void SetCharacterJumpHeight(float _jumpHeight, bool shouldSave = true)
	{
		characterJumpHeight = _jumpHeight;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetCharacterDoubleJump(bool _dblJump, bool shouldSave = true)
	{
		doubleJump = _dblJump;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetCharacterInertia(float _inertia, bool shouldSave = true)
	{
		characterInertia = _inertia;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetCharacterRunSpeed(float _speed, bool shouldSave = true)
	{
		characterRunSpeed = _speed;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetCharacterGravity(float _val, bool shouldSave = true)
	{
		characterGravity = 0f - _val;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public BloxelCharacter Clone()
	{
		List<BloxelAnimation> list = new List<BloxelAnimation>(3);
		Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator = animations.GetEnumerator();
		while (enumerator.MoveNext())
		{
			list.Add(enumerator.Current.Value.Clone());
		}
		BloxelCharacter bloxelCharacter = new BloxelCharacter(list);
		bloxelCharacter.title = title;
		bloxelCharacter.tags = tags;
		bloxelCharacter.owner = owner;
		if (!string.IsNullOrEmpty(_serverID))
		{
			bloxelCharacter._serverID = _serverID;
		}
		return bloxelCharacter;
	}

	public static BloxelCharacter CreateFromCapture(BlockColor[,] blockColors)
	{
		BloxelBoard board = new BloxelBoard((BlockColor[,])blockColors.Clone(), false);
		List<BloxelAnimation> list = new List<BloxelAnimation>();
		list.Add(new BloxelAnimation(board));
		list.Add(new BloxelAnimation(board));
		list.Add(new BloxelAnimation(board));
		List<BloxelAnimation> anims = list;
		return new BloxelCharacter(anims);
	}

	public Dictionary<string, BloxelAnimation> UniqueAnimations()
	{
		Dictionary<string, BloxelAnimation> dictionary = new Dictionary<string, BloxelAnimation>();
		Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator = animations.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelAnimation value = enumerator.Current.Value;
			if (!dictionary.ContainsKey(value.ID()))
			{
				dictionary.Add(value.ID(), value);
			}
		}
		return dictionary;
	}

	public Dictionary<string, BloxelBoard> UniqueBoards()
	{
		Dictionary<string, BloxelBoard> dictionary = new Dictionary<string, BloxelBoard>();
		Dictionary<string, BloxelAnimation> dictionary2 = UniqueAnimations();
		Dictionary<string, BloxelAnimation>.Enumerator enumerator = dictionary2.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelAnimation value = enumerator.Current.Value;
			for (int i = 0; i < value.boards.Count; i++)
			{
				if (!dictionary.ContainsKey(value.boards[i].ID()))
				{
					dictionary.Add(value.boards[i].ID(), value.boards[i]);
				}
			}
		}
		return dictionary;
	}

	public override int GetBloxelCount()
	{
		int num = 0;
		Dictionary<string, BloxelBoard> dictionary = UniqueBoards();
		Dictionary<string, BloxelBoard>.Enumerator enumerator = dictionary.GetEnumerator();
		while (enumerator.MoveNext())
		{
			num += enumerator.Current.Value.GetBloxelCount();
		}
		return num;
	}

	public List<string> GetAssociatedAnimationPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelAnimation value in animations.Values)
		{
			if (!list.Contains(value.projectPath))
			{
				value.CheckAndSetMeAsOwner();
				list.Add(value.projectPath);
			}
		}
		return list;
	}

	public List<string> GetAssociatedBoardPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelAnimation value in animations.Values)
		{
			for (int i = 0; i < value.boards.Count; i++)
			{
				if (!list.Contains(value.boards[i].projectPath))
				{
					value.boards[i].CheckAndSetMeAsOwner();
					list.Add(value.boards[i].projectPath);
				}
			}
		}
		return list;
	}

	public override ProjectBundle Compress()
	{
		List<string> associatedBoardPaths = GetAssociatedBoardPaths();
		List<string> associatedAnimationPaths = GetAssociatedAnimationPaths();
		string[] array = new string[associatedBoardPaths.Count + associatedAnimationPaths.Count];
		string[] array2 = new string[associatedBoardPaths.Count + associatedAnimationPaths.Count];
		int num = 0;
		int num2 = 0;
		while (num2 < associatedBoardPaths.Count)
		{
			array[num] = associatedBoardPaths[num2] + "/" + BloxelBoard.filename;
			string[] array3 = associatedBoardPaths[num2].Split('/');
			array2[num] = array3[array3.Length - 1];
			num2++;
			num++;
		}
		int num3 = 0;
		while (num3 < associatedAnimationPaths.Count)
		{
			array[num] = associatedAnimationPaths[num3] + "/" + BloxelAnimation.filename;
			string[] array4 = associatedAnimationPaths[num3].Split('/');
			array2[num] = array4[array4.Length - 1];
			num3++;
			num++;
		}
		return new ProjectBundle(Zipper.CreateBytes(array, array2), ToJSON());
	}
}
