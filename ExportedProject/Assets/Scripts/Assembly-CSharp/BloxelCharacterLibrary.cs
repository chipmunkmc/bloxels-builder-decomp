using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloxelCharacterLibrary : BloxelLibraryWindow
{
	public static BloxelCharacterLibrary instance;

	public Object savedCharacterPrefab;

	private BloxelCharacter _currentCharacter;

	private bool initComplete;

	public BloxelCharacter currentCharacter
	{
		get
		{
			return _currentCharacter;
		}
		private set
		{
			_currentCharacter = value;
		}
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
	}

	private new void Start()
	{
		if (!initComplete)
		{
			PopulateLibrary();
			base.Start();
			base.OnProjectSelect += SetCurrentProject;
			initComplete = true;
		}
	}

	private void OnDestroy()
	{
		base.OnProjectSelect -= SetCurrentProject;
	}

	public void SetCurrentProject(SavedProject item)
	{
		BloxelCharacter bloxelCharacter = item.dataModel as BloxelCharacter;
		if (currentCharacter == null || currentCharacter != bloxelCharacter)
		{
			currentCharacter = bloxelCharacter;
			BloxelLibraryScrollController.instance.DisablePreviousSelectedOutline();
			SoundManager.PlayOneShot(SoundManager.instance.popA);
		}
	}

	public new void AddButtonPressed()
	{
		base.AddButtonPressed();
		CharacterBuilderCanvas.instance.StartButtonPressed();
	}

	private new void DestroyLibrary()
	{
		base.DestroyLibrary();
	}

	public void PopulateLibrary()
	{
		HashSet<string> hashSet = StartIgnoringBoards();
		BloxelLibraryScrollController.instance.unfilteredCharacterData.Clear();
		foreach (BloxelCharacter value in AssetManager.instance.characterPool.Values)
		{
			if (value.tags.Contains("PixelTutz") && !hashSet.Contains(value.ID()))
			{
				hashSet.Add(value.ID());
			}
		}
		foreach (BloxelCharacter value2 in AssetManager.instance.characterPool.Values)
		{
			if (!hashSet.Contains(value2.ID()))
			{
				BloxelLibraryScrollController.instance.unfilteredCharacterData.Add(value2);
			}
		}
	}

	public override void UpdateScrollerContents()
	{
		BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Character);
	}

	public bool AddProject(BloxelCharacter project, string owner = null, bool shouldQueue = true)
	{
		if (!AssetManager.instance.characterPool.ContainsKey(project.ID()))
		{
			AssetManager.instance.characterPool.Add(project.ID(), project);
			BloxelLibraryScrollController.instance.AddItem(project);
			if (owner != null)
			{
				project.SetOwner(owner);
			}
			else if (CurrentUser.instance.account != null)
			{
				project.SetOwner(CurrentUser.instance.account.serverID);
			}
			project.SetBuildVersion(AppStateManager.instance.internalVersion);
			bool result = project.Save(true, shouldQueue);
			Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator = project.animations.GetEnumerator();
			while (enumerator.MoveNext())
			{
				if (!AssetManager.instance.animationPool.ContainsKey(enumerator.Current.Value.ID()))
				{
					AssetManager.instance.animationPool.Add(enumerator.Current.Value.ID(), enumerator.Current.Value);
				}
				for (int i = 0; i < enumerator.Current.Value.boards.Count; i++)
				{
					if (!AssetManager.instance.boardPool.ContainsKey(enumerator.Current.Value.boards[i].ID()))
					{
						AssetManager.instance.boardPool.Add(enumerator.Current.Value.boards[i].ID(), enumerator.Current.Value.boards[i]);
					}
				}
			}
			StartCoroutine(DelayedItemSelect());
			return result;
		}
		return false;
	}

	private IEnumerator DelayedItemSelect()
	{
		yield return new WaitForEndOfFrame();
		ItemSelect(BloxelLibraryScrollController.instance.GetFirstItem());
	}

	public void Reset()
	{
		if (currentCharacter != null)
		{
			currentCharacter = null;
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Character);
		}
	}
}
