using UnityEngine;

public sealed class BloxelCoinTile : BloxelCollectibleTile
{
	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.coinPool.Push(this);
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(BloxelCollectibleTile.collider2DLayer);
		tileRenderer.colliderIsTrigger = true;
		tileRenderer.tileScale = Vector3.one;
	}
}
