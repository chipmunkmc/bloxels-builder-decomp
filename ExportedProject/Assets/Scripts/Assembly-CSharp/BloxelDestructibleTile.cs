using UnityEngine;

public sealed class BloxelDestructibleTile : BloxelTile
{
	public static string collider2DLayer = "Destructible";

	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.destructiblePool.Push(this);
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(collider2DLayer);
		tileRenderer.colliderIsTrigger = false;
		tileRenderer.tileScale = new Vector3(1f, 1f, 13f);
	}
}
