using System.Collections;
using UnityEngine;

public sealed class BloxelEnemyTile : BloxelTile
{
	public static string collider2DLayer = "Enemy";

	public static int enemyPlatformMask;

	public BoxCollider2D boxCollider;

	public Transform meshTransform;

	public Transform target;

	public bool locked;

	public float waitTime = 0.1f;

	public int health;

	public bool isDead;

	public Vector3 lastHitPosition;

	public bool debug;

	public EnemyType type;

	public bool initComplete;

	public EnemyController controller;

	private Vector2 originalCollderSize;

	public Vector2 originalBoundsSize;

	private Coroutine growRoutine;

	private float nextScale;

	private Vector2 newColliderSize;

	public EnemyHealthIndicator healthIndicator;

	public LayerMask destructionMask;

	private static Collider2D[] bombRadiusResults = new Collider2D[50];

	private Coroutine _blinkRoutine;

	private void OnBecameVisible()
	{
		if (BloxelCamera.instance.gameplayEnabled && !controller.isEnabled)
		{
			controller.Enable();
		}
	}

	private void OnBecameInvisible()
	{
		if (BloxelCamera.instance.gameplayEnabled && controller.isEnabled)
		{
			controller.Disable();
		}
	}

	public override void PrepareSpawn()
	{
		base.PrepareSpawn();
		BloxelCamera.instance.OnModeChange += HandleCameraModeChanged;
		controller.Disable();
		isDead = false;
	}

	public override void Despawn()
	{
		if (controller.laser != null)
		{
			GameplayPool.DespawnLaser(controller.laser);
		}
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.enemyPool.Push(this);
		BloxelCamera.instance.OnModeChange -= HandleCameraModeChanged;
	}

	public override void EarlyAwake()
	{
		if (!(selfTransform != null))
		{
			spawned = false;
			selfTransform = base.transform;
			controller = new EnemyPatrolController(this);
			controller.tile = this;
		}
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(collider2DLayer);
		tileRenderer.colliderIsTrigger = false;
		tileRenderer.tileScale = Vector3.one;
	}

	public override void InitWithNewTileInfo(TileInfo tileInfo)
	{
		base.tileInfo = tileInfo;
		GameplayBuilder.instance.game.levels.TryGetValue(tileInfo.levelLocationInWorld, out bloxelLevel);
		InitConfig();
		CreateTile();
	}

	protected override void PositionBloxelTile()
	{
		Vector3 localPosition = new Vector3((float)tileInfo.worldLocation.x + 6f, (float)tileInfo.worldLocation.y - 0.5f, -0.01f);
		selfTransform.localPosition = localPosition;
		selfTransform.localScale = Vector3.one;
	}

	public override void CreateTile()
	{
		base.CreateTile();
		BloxelBrain value = null;
		tileInfo.bloxelLevel.brains.TryGetValue(tileInfo.locationInLevel, out value);
		controller.InitWithBrain(value);
		health = controller.startingHealth;
		BlockBounds blockBounds = default(BlockBounds);
		if (tileInfo.data is BloxelBoard)
		{
			((BloxelBoard)tileInfo.data).SetMinMaxBounds();
			blockBounds = ((BloxelBoard)tileInfo.data).blockBounds;
		}
		else
		{
			((BloxelAnimation)tileInfo.data).SetMinMaxBounds();
			blockBounds = ((BloxelAnimation)tileInfo.data).blockBounds;
		}
		boxCollider.enabled = true;
		originalBoundsSize = new Vector2(blockBounds.xMax - blockBounds.xMin, blockBounds.yMax - blockBounds.yMin);
		Vector2 size = (originalCollderSize = new Vector2(originalBoundsSize.x * 0.8f, originalBoundsSize.y * 0.9f));
		boxCollider.size = size;
		boxCollider.offset = new Vector2(0f, size.y / 2f);
		Vector2 vector = new Vector2((float)(blockBounds.xMax + blockBounds.xMin) / -2f + 0.5f, (float)(blockBounds.yMax + blockBounds.yMin) / -2f + 0.5f + originalBoundsSize.y / 2f);
		tileRenderer.physicsEnabler.transform.localPosition = Vector3.zero;
		if (controller is EnemyStationaryController)
		{
			meshTransform.localPosition = new Vector3(-6f, 0.5f, 0f);
			Vector2 vector2 = new Vector2(0f - vector.x - 6f, 0f - vector.y + 0.5f);
			boxCollider.offset += vector2;
			tileRenderer.physicsEnabler.circleCollider.offset = vector2;
		}
		else
		{
			meshTransform.localPosition = new Vector3(vector.x, vector.y, 0f);
			tileRenderer.physicsEnabler.circleCollider.offset = new Vector2(0f, originalBoundsSize.y / 2f);
		}
	}

	public void InitConfig()
	{
		bool flag = false;
		if (!tileInfo.bloxelLevel.enemyTypes.TryGetValue(tileInfo.locationInLevel, out type))
		{
			type = EnemyType.Patrol;
		}
		SetEnemyType(type);
	}

	public void SetEnemyType(EnemyType enemyType)
	{
		type = enemyType;
		switch (type)
		{
		case EnemyType.Patrol:
			controller = new EnemyPatrolController(this);
			break;
		case EnemyType.Flyer:
			controller = new EnemyFlyerController(this);
			break;
		case EnemyType.Stationary:
			controller = new EnemyStationaryController(this);
			break;
		default:
			controller = new EnemyPatrolController(this);
			break;
		}
		CreateTile();
	}

	public void UpdateHudForNewBrain()
	{
		int coinBonus = GameplayController.gameState[tileInfo.xTileDataIndex, tileInfo.yTileDataIndex].coinBonus;
		if (coinBonus != controller.coinBonusQuantum)
		{
			GameplayController.gameState[tileInfo.xTileDataIndex, tileInfo.yTileDataIndex].coinBonus = controller.coinBonusQuantum;
			GameHUD.instance.SetTotalCoinsFromConfig();
		}
	}

	private void FixedUpdate()
	{
		if (BloxelCamera.instance.gameplayEnabled && controller.currentFixedUpdateAction != null)
		{
			controller.currentFixedUpdateAction();
		}
	}

	public void TryToGrow()
	{
		growRoutine = StartCoroutine(Grow());
	}

	public void TryNotToGrow()
	{
		if (growRoutine != null)
		{
			StopCoroutine(growRoutine);
			growRoutine = null;
		}
	}

	private IEnumerator Grow()
	{
		while (!Mathf.Approximately(selfTransform.localScale.y, controller.targetScale))
		{
			nextScale = Mathf.Lerp(selfTransform.localScale.y, controller.targetScale, Time.deltaTime * 2f);
			if (GrowIsSafe())
			{
				selfTransform.localScale = new Vector3(Mathf.Sign(selfTransform.localScale.x) * nextScale, nextScale, nextScale);
				boxCollider.offset += new Vector2(0f, (newColliderSize.y - boxCollider.size.y) * 0.5f);
				boxCollider.size = newColliderSize;
				if (controller.laser != null)
				{
					controller.laser.Init(controller, selfTransform, PixelPlayerController.instance.centerTransform, new Vector3(0f, originalBoundsSize.y / 2f, -0.1f - selfTransform.localScale.z / 2f), controller._trackingSpeed);
				}
			}
			yield return null;
		}
	}

	private bool GrowIsSafe()
	{
		Bounds bounds = boxCollider.bounds;
		Vector2 vector = originalBoundsSize * nextScale;
		newColliderSize = vector / nextScale;
		float num = bounds.center.y - bounds.extents.y + 0.015f;
		float x = bounds.center.x;
		Vector2 pointA = new Vector2(x - vector.x / 2f, num);
		Vector2 pointB = new Vector2(x + vector.x / 2f, num + vector.y);
		float z = WorldWrapper.instance.selfTransform.position.z;
		Collider2D collider2D = Physics2D.OverlapArea(pointA, pointB, enemyPlatformMask, z - 1f, z + 1f);
		return collider2D == null;
	}

	private void Update()
	{
		if (!BloxelCamera.instance.gameplayEnabled)
		{
			return;
		}
		Vector3 position = BloxelCamera.instance.selfTransform.position;
		float num = Mathf.Abs(selfTransform.position.x - position.x);
		float num2 = Mathf.Abs(selfTransform.position.y - position.y);
		if (GameplayBuilder.instance.unloadMaxDistanceX * 0.85f <= num || GameplayBuilder.instance.unloadMaxDistanceY * 0.85f <= num2)
		{
			if (controller.isEnabled)
			{
				controller.Disable();
			}
		}
		else if (!controller.isEnabled)
		{
			controller.Enable();
		}
		if (controller.isEnabled && controller.currentUpdateAction != null)
		{
			controller.currentUpdateAction();
		}
	}

	private void HandleCameraModeChanged()
	{
		if (controller != null)
		{
			controller.InitForCurrentCameraMode();
		}
	}

	public void BulletHit(Bullet bullet)
	{
		if (!controller.shouldReflectBullets)
		{
			bullet.Explode();
			TakeDamage(bullet.selfTransform.position);
			return;
		}
		float num = Mathf.Sign(bullet.rigidbodyComponent.velocity.x);
		ReflectedBullet reflectedBullet = GameplayPool.SpawnReflectedBullet();
		reflectedBullet.selfTransform.position = bullet.selfTransform.position + new Vector3(num, 0f, 0f);
		reflectedBullet.rigidbodyComponent.velocity = new Vector2(num * 85.71429f, 0f);
		bullet.explosionParticles.Emit(10);
		bullet.KillBullet();
		SoundManager.PlayOneShot(SoundManager.instance.laser);
	}

	public void BulletHit(ReflectedBullet reflectedBullet)
	{
		if (!controller.shouldReflectBullets)
		{
			reflectedBullet.Explode();
			return;
		}
		float num = Mathf.Sign(reflectedBullet.rigidbodyComponent.velocity.x);
		reflectedBullet.selfTransform.position += new Vector3(num, 0f, 0f);
		reflectedBullet.rigidbodyComponent.velocity = new Vector2(num * 85.71429f, 0f);
		reflectedBullet.explosionParticles.Emit(10);
		SoundManager.PlayOneShot(SoundManager.instance.laser);
	}

	public void TakeDamage(Vector3 hitPosition)
	{
		SoundManager.PlayOneShot(SoundManager.instance.enemyHitSFX);
		lastHitPosition = hitPosition;
		health--;
		if (health <= 0)
		{
			Kill();
			return;
		}
		DisplayHealth();
		StartBlinking();
	}

	public void TakeDamage(Vector3 bombPosition, float explosionForce, int damage)
	{
		lastHitPosition = bombPosition;
		health -= damage;
		if (health <= 0)
		{
			Kill(bombPosition, explosionForce);
			return;
		}
		SoundManager.PlayOneShot(SoundManager.instance.enemyHitSFX);
		DisplayHealth();
		StartBlinking();
	}

	public void DisplayHealth()
	{
		if (healthIndicator == null)
		{
			healthIndicator = GameplayController.SpawnEnemyHealthIndicator();
		}
		healthIndicator.ShowEnemyHealthDecreasing(this);
	}

	public void DisplayHealthIncrease(int amount)
	{
		if (healthIndicator == null)
		{
			healthIndicator = GameplayController.SpawnEnemyHealthIndicator();
		}
		health += healthIndicator.ShowEnemyHealthIncreasing(this, amount);
	}

	public void RemoveHealthBar()
	{
		if (healthIndicator != null)
		{
			healthIndicator.gameObject.Recycle();
			healthIndicator = null;
		}
	}

	public void Kill(Vector3? bombPosition = null, float explosionForce = 3f)
	{
		if (isDead)
		{
			return;
		}
		isDead = true;
		RemoveHealthBar();
		AwardPlayerBonuses();
		StopBlinking();
		bool hasValue = bombPosition.HasValue;
		if (hasValue)
		{
			lastHitPosition = bombPosition.Value;
		}
		SoundManager.PlayOneShot(SoundManager.instance.enemyDeath);
		GameHUD.instance.enemiesDefeated++;
		if (controller.shouldExplodeOnDeath)
		{
			if (!hasValue)
			{
				lastHitPosition = boxCollider.bounds.center + TileExplosionManager.instance.enemyExplosionOffset * selfTransform.localScale.y;
			}
			ExplosiveDeath(hasValue);
		}
		else if (hasValue)
		{
			TileExplosionManager.instance.ExplodeChunk(tileRenderer.currentFrameChunk, meshTransform, rigidbodyComponent, lastHitPosition + TileExplosionManager.instance.bombExplosionOffset, TileExplosionManager.instance.bombExplosionForce, TileExplosionManager.instance.bombExplosionRadius, TileExplosionManager.instance.bombUpwardsModifier, new Vector3(selfTransform.localScale.x, selfTransform.localScale.y, selfTransform.localScale.y));
		}
		else
		{
			TileExplosionManager.instance.ExplodeChunk(tileRenderer.currentFrameChunk, meshTransform, rigidbodyComponent, lastHitPosition, TileExplosionManager.instance.bulletExplosionForce, TileExplosionManager.instance.bulletExplosionRadius * selfTransform.localScale.y, 0f, selfTransform.localScale);
		}
		GameplayBuilder.instance.UnloadTile(this, true);
	}

	private void AwardPlayerBonuses()
	{
		int num = GameplayController.gameState[tileInfo.xTileDataIndex, tileInfo.yTileDataIndex].HandleEnemyDeathAndCalculateCoinDrop();
		if (num != 0)
		{
			ParticleAttractor particleAttractor = GameplayPool.SpawnAttractor();
			particleAttractor.selfTransform.position = boxCollider.bounds.center;
			particleAttractor.GiveCoinsToPlayer(this, num, PixelPlayerController.instance.selfTransform, new Vector3(0f, PixelPlayerController.instance.boxCollider.size.y / 2f, -0.51f));
		}
		int healthBonusQuantum = controller.healthBonusQuantum;
		if (healthBonusQuantum != 0)
		{
			ParticleAttractor particleAttractor2 = GameplayPool.SpawnAttractor();
			particleAttractor2.selfTransform.position = boxCollider.bounds.center;
			particleAttractor2.GiveHealthToPlayer(this, healthBonusQuantum, PixelPlayerController.instance.selfTransform, new Vector3(0f, PixelPlayerController.instance.boxCollider.size.y / 2f, -0.51f));
		}
	}

	public void ExplosiveDeath(bool bombDeath = false)
	{
		SoundManager.PlayOneShot(SoundManager.instance.bombSFX);
		Vector3 center = boxCollider.bounds.center;
		ExplosionFlash explosionFlash = GameplayController.SpawnExplosionFlash();
		explosionFlash.selfTransform.position = center - new Vector3(0f, 0f, selfTransform.localScale.y * 2f);
		explosionFlash.selfTransform.localScale = Vector3.one * (selfTransform.localScale.y * 1.5f);
		explosionFlash.PlayAll();
		BloxelCamera.instance.cameraShake.PlayShake();
		float radius = Mathf.Max(originalBoundsSize.x, originalBoundsSize.y) * selfTransform.localScale.y * 0.5f * 1.5f;
		int num = Physics2D.OverlapCircleNonAlloc(new Vector2(center.x, center.y), radius, bombRadiusResults, destructionMask, center.z - 1f, center.z + 1f);
		for (int i = 0; i < num; i++)
		{
			Collider2D collider2D = bombRadiusResults[i];
			Vector3 b = boxCollider.bounds.center + TileExplosionManager.instance.enemyExplosionOffset * base.transform.localScale.y;
			if (collider2D.gameObject.layer == LayerMask.NameToLayer(PixelPlayerController.collider2DLayer) && !PixelPlayerController.instance.isInvincible)
			{
				PixelPlayerController.instance.lastHitPosition = Vector3.Lerp(PixelPlayerController.instance.boxCollider.bounds.center, b, 0.333f);
				PixelPlayerController.instance.HitPlayer(Bomb.BombDamage);
				continue;
			}
			BloxelTile component = collider2D.GetComponent<BloxelTile>();
			if (!(component != null) || !component.tileInfo.isLoaded)
			{
				continue;
			}
			if (component is BloxelEnemyTile)
			{
				BloxelEnemyTile bloxelEnemyTile = component as BloxelEnemyTile;
				if (bloxelEnemyTile.health <= Bomb.BombDamage)
				{
					GameplayController.EnemiesToKill.Enqueue(new GameplayController.EnemyToKill(bloxelEnemyTile, Time.time + 0.5f));
				}
				else
				{
					bloxelEnemyTile.TakeDamage(Vector3.Lerp(component.selfTransform.position + new Vector3(6f, 6f, 0f), b, TileExplosionManager.instance.enemyExplosionTilePosOffset), TileExplosionManager.instance.tileExplosionForce * selfTransform.localScale.y, Bomb.BombDamage);
				}
				continue;
			}
			bool flag = false;
			if (component is BloxelDestructibleTile || (flag = component is BloxelPowerUpTile))
			{
				TileExplosionManager.instance.ExplodeChunk(component.tileRenderer.currentFrameChunk, component.selfTransform, component.rigidbodyComponent, Vector3.Lerp(component.selfTransform.position + new Vector3(6f, 6f, 0f), b, TileExplosionManager.instance.enemyExplosionTilePosOffset), TileExplosionManager.instance.tileExplosionForce * selfTransform.localScale.y, TileExplosionManager.instance.tileExplosionRadius * selfTransform.localScale.y, 0f, new Vector3(component.selfTransform.localScale.x, component.selfTransform.localScale.y, component.selfTransform.localScale.y), false);
				GameplayBuilder.instance.UnloadTile(component, true);
				if (flag)
				{
					((BloxelPowerUpTile)component).SpawnContents();
				}
			}
		}
		if (bombDeath)
		{
			TileExplosionManager.instance.ExplodeChunk(tileRenderer.currentFrameChunk, meshTransform, rigidbodyComponent, lastHitPosition + TileExplosionManager.instance.bombExplosionOffset, TileExplosionManager.instance.bombExplosionForce, TileExplosionManager.instance.bombExplosionRadius, TileExplosionManager.instance.bombUpwardsModifier, new Vector3(selfTransform.localScale.x, selfTransform.localScale.y, selfTransform.localScale.y), false);
		}
		else
		{
			TileExplosionManager.instance.ExplodeChunk(tileRenderer.currentFrameChunk, meshTransform, rigidbodyComponent, lastHitPosition, TileExplosionManager.instance.enemyExplosionForce * selfTransform.localScale.y, TileExplosionManager.instance.enemyExplosionRadius * selfTransform.localScale.y, TileExplosionManager.instance.enemyExplodeUpwardsModifier, selfTransform.localScale);
		}
	}

	private void StartBlinking()
	{
		if (_blinkRoutine != null)
		{
			StopCoroutine(_blinkRoutine);
		}
		_blinkRoutine = StartCoroutine(Blink());
	}

	private void StopBlinking()
	{
		if (_blinkRoutine != null)
		{
			StopCoroutine(_blinkRoutine);
			_blinkRoutine = null;
		}
		tileRenderer.localMeshRenderer.enabled = true;
	}

	private IEnumerator Blink()
	{
		bool meshEnabled = false;
		for (int i = 0; (float)i < 6f; i++)
		{
			tileRenderer.localMeshRenderer.enabled = meshEnabled;
			meshEnabled = !meshEnabled;
			yield return new WaitForSeconds(0.035f);
		}
	}
}
