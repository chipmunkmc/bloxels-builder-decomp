using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using Newtonsoft.Json;
using UnityEngine;

public sealed class BloxelGame : BloxelProject
{
	public delegate void HandleUpdate();

	public static string filename = "game.json";

	public static string rootDirectory = "Games";

	public static string subDirectory = "myGames";

	public static string defaultTitle = "My Game";

	public static int minMultiRoomLock = 130;

	private BloxelCharacter _hero;

	private WorldLocation _heroStartPosition;

	public Dictionary<GridLocation, BloxelLevel> levels;

	public BloxelMegaBoard background;

	public WorldLocation gameEndFlag;

	public Rect worldBoundingRect;

	private static Color32[] clearDetailColorBuffer;

	private static Color[] boardColorBuffer = new Color[169];

	public MusicChoice musicChoice;

	public BloxelCamera.CameraMode cameraMode;

	public bool gravityOn;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map9;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024mapA;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024mapB;

	public BloxelCharacter hero
	{
		get
		{
			return _hero;
		}
	}

	public WorldLocation heroStartPosition
	{
		get
		{
			return _heroStartPosition;
		}
	}

	public event HandleUpdate OnUpdate;

	public BloxelGame(BloxelLevel level, bool shouldSave = true)
	{
		type = ProjectType.Game;
		base.id = GenerateRandomID();
		coverBoard = level.coverBoard;
		_heroStartPosition = WorldLocation.InvalidLocation();
		levels = new Dictionary<GridLocation, BloxelLevel> { { level.location, level } };
		gameEndFlag = WorldLocation.InvalidLocation();
		background = null;
		worldBoundingRect = GetBoundingRectForLevels();
		musicChoice = MusicChoice.Choice0;
		cameraMode = BloxelCamera.CameraMode.Gameplay;
		gravityOn = true;
		tags = new List<string>();
		tags.Add("mine");
		projectPath = MyProjectPath() + "/" + ID();
		syncInfo = ProjectSyncInfo.Create(this);
		if (shouldSave)
		{
			if (!Directory.Exists(projectPath))
			{
				Directory.CreateDirectory(projectPath);
			}
			Save();
		}
	}

	public BloxelGame(JsonTextReader reader, bool fromServer)
	{
		type = ProjectType.Game;
		FromReader(reader, fromServer);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelGame(JsonTextReader reader)
	{
		type = ProjectType.Game;
		FromReaderSlim(reader);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public static string BaseStoragePath()
	{
		return Application.persistentDataPath + "/" + rootDirectory;
	}

	public static string MyProjectPath()
	{
		return AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory;
	}

	public void FromJSONString(string jsonString, bool fromServer)
	{
		JsonTextReader reader = new JsonTextReader(new StreamReader(jsonString));
		FromReader(reader, fromServer);
	}

	public void FromReaderSlim(JsonTextReader reader)
	{
		Dictionary<string, BloxelProject> projectPool = new Dictionary<string, BloxelProject>(64);
		string empty = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null && reader.TokenType == JsonToken.PropertyName)
			{
				switch (reader.Value.ToString())
				{
				case "boardLookup":
					ParseBoardsSlim(reader, projectPool);
					break;
				case "animationLookup":
					ParseAnimationsSlim(reader, projectPool);
					break;
				case "brainLookup":
					ParseBrainsSlim(reader, projectPool);
					break;
				case "structure":
					FinalizeSlimParsing(reader, projectPool);
					projectPool = null;
					return;
				}
			}
		}
	}

	private void ParseBoardsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string empty = string.Empty;
		while (reader.Read())
		{
			if (reader.TokenType == JsonToken.StartObject)
			{
				BloxelBoard bloxelBoard = new BloxelBoard(reader);
				projectPool[bloxelBoard._serverID] = bloxelBoard;
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseAnimationsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string empty = string.Empty;
		while (reader.Read())
		{
			if (reader.TokenType == JsonToken.StartObject)
			{
				BloxelAnimation bloxelAnimation = new BloxelAnimation(reader, projectPool);
				projectPool[bloxelAnimation._serverID] = bloxelAnimation;
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBrainsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string empty = string.Empty;
		while (reader.Read())
		{
			if (reader.TokenType == JsonToken.StartObject)
			{
				BloxelBrain bloxelBrain = new BloxelBrain(reader);
				projectPool[bloxelBrain._serverID] = bloxelBrain;
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	public void FromReader(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		levels = new Dictionary<GridLocation, BloxelLevel>();
		tags = new List<string>();
		while (reader.Read())
		{
			if (reader.Value == null)
			{
				continue;
			}
			int value;
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				if (text == null)
				{
					continue;
				}
				if (_003C_003Ef__switch_0024map9 == null)
				{
					Dictionary<string, int> dictionary = new Dictionary<string, int>(7);
					dictionary.Add("cover", 0);
					dictionary.Add("background", 1);
					dictionary.Add("hero", 2);
					dictionary.Add("character", 3);
					dictionary.Add("heroStartPosition", 4);
					dictionary.Add("gameEndPosition", 5);
					dictionary.Add("levels", 6);
					_003C_003Ef__switch_0024map9 = dictionary;
				}
				if (_003C_003Ef__switch_0024map9.TryGetValue(text, out value))
				{
					switch (value)
					{
					case 0:
						ParseCoverBoard(reader, fromServer);
						break;
					case 1:
						ParseBackground(reader, fromServer);
						break;
					case 2:
						ParseHero(reader, fromServer);
						break;
					case 3:
						ParseHero(reader, fromServer);
						break;
					case 4:
						ParseHeroStartPosition(reader, fromServer);
						break;
					case 5:
						ParseGameEndPosition(reader, fromServer);
						break;
					case 6:
						ParseLevels(reader, fromServer);
						worldBoundingRect = GetBoundingRectForLevels();
						break;
					}
				}
			}
			else
			{
				if (text == null)
				{
					continue;
				}
				if (_003C_003Ef__switch_0024mapA == null)
				{
					Dictionary<string, int> dictionary = new Dictionary<string, int>(10);
					dictionary.Add("_id", 0);
					dictionary.Add("id", 1);
					dictionary.Add("title", 2);
					dictionary.Add("musicChoice", 3);
					dictionary.Add("cameraMode", 4);
					dictionary.Add("gravityOn", 5);
					dictionary.Add("tags", 6);
					dictionary.Add("builtWithVersion", 7);
					dictionary.Add("owner", 8);
					dictionary.Add("ugcApproved", 9);
					_003C_003Ef__switch_0024mapA = dictionary;
				}
				if (_003C_003Ef__switch_0024mapA.TryGetValue(text, out value))
				{
					switch (value)
					{
					case 0:
						_serverID = reader.Value.ToString();
						break;
					case 1:
						base.id = new Guid(reader.Value.ToString());
						break;
					case 2:
						title = reader.Value.ToString();
						break;
					case 3:
						musicChoice = (MusicChoice)int.Parse(reader.Value.ToString());
						break;
					case 4:
						cameraMode = (BloxelCamera.CameraMode)int.Parse(reader.Value.ToString());
						break;
					case 5:
						gravityOn = bool.Parse(reader.Value.ToString());
						break;
					case 6:
						tags.Add(reader.Value.ToString());
						break;
					case 7:
						SetBuildVersion(int.Parse(reader.Value.ToString()));
						break;
					case 8:
						owner = reader.Value.ToString();
						break;
					case 9:
						ugcApproved = bool.Parse(reader.Value.ToString());
						break;
					}
				}
			}
		}
		projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
		bool flag = false;
		bool flag2 = false;
		while (enumerator.MoveNext() && (!flag || !flag2))
		{
			BloxelLevel value2 = enumerator.Current.Value;
			if (value2.id == heroStartPosition.id)
			{
				flag = true;
				_heroStartPosition.levelLocationInWorld = value2.location;
			}
			if (value2.id == gameEndFlag.id)
			{
				flag2 = true;
				gameEndFlag.levelLocationInWorld = value2.location;
			}
		}
	}

	private void FinalizeSlimParsing(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		levels = new Dictionary<GridLocation, BloxelLevel>(8);
		tags = new List<string>();
		string text = string.Empty;
		while (reader.Read())
		{
			if (reader.Value == null)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				switch (text)
				{
				case "cover":
					ParseCoverBoardSlim(reader, projectPool);
					break;
				case "background":
					ParseBackgroundSlim(reader, projectPool);
					break;
				case "character":
					ParseHeroSlim(reader, projectPool);
					break;
				case "heroStartPosition":
					ParseHeroStartPosition(reader, true);
					break;
				case "gameEndPosition":
					ParseGameEndPosition(reader, true);
					break;
				case "levels":
					ParseLevelsSlim(reader, projectPool);
					worldBoundingRect = GetBoundingRectForLevels();
					break;
				}
			}
			else
			{
				if (text == null)
				{
					continue;
				}
				if (_003C_003Ef__switch_0024mapB == null)
				{
					Dictionary<string, int> dictionary = new Dictionary<string, int>(10);
					dictionary.Add("_id", 0);
					dictionary.Add("id", 1);
					dictionary.Add("title", 2);
					dictionary.Add("musicChoice", 3);
					dictionary.Add("cameraMode", 4);
					dictionary.Add("gravityOn", 5);
					dictionary.Add("tags", 6);
					dictionary.Add("owner", 7);
					dictionary.Add("isDirty", 8);
					dictionary.Add("ugcApproved", 9);
					_003C_003Ef__switch_0024mapB = dictionary;
				}
				int value;
				if (_003C_003Ef__switch_0024mapB.TryGetValue(text, out value))
				{
					switch (value)
					{
					case 0:
						_serverID = reader.Value.ToString();
						break;
					case 1:
						base.id = new Guid(reader.Value.ToString());
						break;
					case 2:
						title = reader.Value.ToString();
						break;
					case 3:
						musicChoice = (MusicChoice)int.Parse(reader.Value.ToString());
						break;
					case 4:
						cameraMode = (BloxelCamera.CameraMode)int.Parse(reader.Value.ToString());
						break;
					case 5:
						gravityOn = bool.Parse(reader.Value.ToString());
						break;
					case 6:
						tags.Add(reader.Value.ToString());
						break;
					case 7:
						owner = reader.Value.ToString();
						break;
					case 8:
						isDirty = bool.Parse(reader.Value.ToString());
						break;
					case 9:
						ugcApproved = bool.Parse(reader.Value.ToString());
						break;
					}
				}
			}
		}
		projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
		bool flag = false;
		bool flag2 = false;
		while (enumerator.MoveNext() && (!flag || !flag2))
		{
			BloxelLevel value2 = enumerator.Current.Value;
			if (value2.id == heroStartPosition.id)
			{
				flag = true;
				_heroStartPosition.levelLocationInWorld = value2.location;
			}
			if (value2.id == gameEndFlag.id)
			{
				flag2 = true;
				gameEndFlag.levelLocationInWorld = value2.location;
			}
		}
	}

	private void ParseHero(JsonTextReader reader, bool fromServer)
	{
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.TokenType == JsonToken.StartObject)
				{
					SetHero(new BloxelCharacter(reader, fromServer), false);
				}
				else if (reader.TokenType == JsonToken.EndObject)
				{
					break;
				}
			}
		}
		else
		{
			reader.Read();
			if (!AssetManager.instance.characterPool.TryGetValue(reader.Value.ToString(), out _hero))
			{
				SetDefaultHero();
			}
		}
	}

	private void ParseHeroSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		BloxelCharacter bloxelCharacter = new BloxelCharacter(reader, projectPool);
		if (bloxelCharacter != null)
		{
			SetHero(bloxelCharacter, false);
		}
		else
		{
			SetDefaultHero();
		}
	}

	public void SetDefaultHero(bool shouldSave = false)
	{
		if (AssetManager.instance.characterPool.TryGetValue(AssetManager.instance.defaultCharacterID, out _hero))
		{
		}
		if (shouldSave)
		{
			Save(false);
		}
	}

	private void ParseCoverBoard(JsonTextReader reader, bool fromServer)
	{
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.TokenType == JsonToken.StartObject)
				{
					coverBoard = new BloxelBoard(reader);
					break;
				}
			}
		}
		else
		{
			reader.Read();
			if (reader.TokenType != JsonToken.Null && !AssetManager.instance.boardPool.TryGetValue(reader.Value.ToString(), out coverBoard))
			{
				AssetManager.instance.borkedGames.Add(this);
			}
		}
	}

	private void ParseCoverBoardSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		reader.Read();
		if (reader.TokenType != JsonToken.Null)
		{
			string key = reader.Value.ToString();
			BloxelProject value = null;
			if (projectPool.TryGetValue(key, out value) && value.type == ProjectType.Board)
			{
				coverBoard = value as BloxelBoard;
			}
		}
	}

	private void ParseHeroStartPosition(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		string empty = string.Empty;
		string g = string.Empty;
		int num = 0;
		int num2 = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					if (text == "gridlocation")
					{
						GridLocation gridLocation = ParseGridLocation(reader, fromServer);
						num = gridLocation.c;
						num2 = gridLocation.r;
					}
				}
				else if (text != null && text == "levelID")
				{
					g = reader.Value.ToString();
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				if (num == -1 && num2 == -1)
				{
					_heroStartPosition = WorldLocation.InvalidLocation();
				}
				else
				{
					_heroStartPosition = new WorldLocation(new Guid(g), new GridLocation(num, num2), GridLocation.InvalidLocation);
				}
				break;
			}
		}
	}

	private GridLocation ParseGridLocation(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					if (reader.TokenType == JsonToken.PropertyName)
					{
						text = reader.Value.ToString();
						continue;
					}
					switch (text)
					{
					case "c":
						col = int.Parse(reader.Value.ToString());
						break;
					case "r":
						row = int.Parse(reader.Value.ToString());
						break;
					}
				}
				else if (reader.TokenType == JsonToken.EndObject)
				{
					return new GridLocation(col, row);
				}
			}
		}
		else
		{
			int num = 0;
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					switch (num)
					{
					case 0:
						col = int.Parse(reader.Value.ToString());
						break;
					case 1:
						row = int.Parse(reader.Value.ToString());
						break;
					}
					num++;
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					return new GridLocation(col, row);
				}
			}
		}
		return GridLocation.InvalidLocation;
	}

	private void ParseGameEndPosition(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		string empty = string.Empty;
		string g = string.Empty;
		int num = 0;
		int num2 = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					if (text == "gridLocation")
					{
						GridLocation gridLocation = ParseGridLocation(reader, fromServer);
						num = gridLocation.c;
						num2 = gridLocation.r;
					}
				}
				else if (text != null && text == "levelID")
				{
					g = reader.Value.ToString();
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				if (num == -1 && num2 == -1)
				{
					gameEndFlag = WorldLocation.InvalidLocation();
				}
				else
				{
					gameEndFlag = new WorldLocation(new Guid(g), new GridLocation(num, num2), GridLocation.InvalidLocation);
				}
				break;
			}
		}
	}

	private void ParseLevels(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					if (reader.TokenType == JsonToken.PropertyName)
					{
						text = reader.Value.ToString();
						if (text == "level")
						{
							levels.Add(new GridLocation(col, row), new BloxelLevel(reader, fromServer));
						}
						continue;
					}
					switch (text)
					{
					case "c":
						col = int.Parse(reader.Value.ToString());
						break;
					case "r":
						row = int.Parse(reader.Value.ToString());
						break;
					}
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					break;
				}
			}
			return;
		}
		string key = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "id":
					key = reader.Value.ToString();
					break;
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
				{
					row = int.Parse(reader.Value.ToString());
					BloxelLevel value = null;
					if (AssetManager.instance.levelPool.TryGetValue(key, out value))
					{
						GridLocation key2 = (value.location = new GridLocation(col, row));
						levels.Add(key2, value);
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseLevelsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					if (text == "level")
					{
						BloxelLevel value = new BloxelLevel(reader, projectPool);
						levels.Add(new GridLocation(col, row), value);
					}
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBackground(JsonTextReader reader, bool fromServer)
	{
		if (fromServer)
		{
			background = new BloxelMegaBoard(reader, true);
			if (background.boards.Count == 0)
			{
				background = null;
			}
		}
		else
		{
			reader.Read();
			if (!AssetManager.instance.megaBoardPool.TryGetValue(reader.Value.ToString(), out background))
			{
			}
		}
	}

	private void ParseBackgroundSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		background = new BloxelMegaBoard(reader, projectPool);
		if (background.boards.Count == 0)
		{
			background = null;
		}
	}

	public string ToJSON()
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject jSONObject2 = new JSONObject(JSONObject.Type.ARRAY);
		if (levels != null)
		{
			foreach (KeyValuePair<GridLocation, BloxelLevel> level in levels)
			{
				JSONObject jSONObject3 = new JSONObject();
				jSONObject3.AddField("id", level.Value.ID());
				jSONObject3.AddField("c", level.Key.c);
				jSONObject3.AddField("r", level.Key.r);
				jSONObject2.Add(jSONObject3);
			}
		}
		JSONObject jSONObject4 = new JSONObject();
		JSONObject jSONObject5 = new JSONObject(JSONObject.Type.ARRAY);
		if (_heroStartPosition != WorldLocation.InvalidLocation())
		{
			jSONObject4.AddField("levelID", _heroStartPosition.id.ToString());
			jSONObject5.Add(_heroStartPosition.locationInBoard.c);
			jSONObject5.Add(_heroStartPosition.locationInBoard.r);
		}
		else
		{
			jSONObject4.AddField("levelID", WorldLocation.InvalidLocation().id.ToString());
			jSONObject5.Add(WorldLocation.InvalidLocation().locationInBoard.c);
			jSONObject5.Add(WorldLocation.InvalidLocation().locationInBoard.r);
		}
		JSONObject jSONObject6 = new JSONObject();
		JSONObject jSONObject7 = new JSONObject(JSONObject.Type.ARRAY);
		if (gameEndFlag != WorldLocation.InvalidLocation())
		{
			jSONObject6.AddField("levelID", gameEndFlag.id.ToString());
			jSONObject7.Add(gameEndFlag.locationInBoard.c);
			jSONObject7.Add(gameEndFlag.locationInBoard.r);
		}
		else
		{
			jSONObject6.AddField("levelID", WorldLocation.InvalidLocation().id.ToString());
			jSONObject7.Add(WorldLocation.InvalidLocation().locationInBoard.c);
			jSONObject7.Add(WorldLocation.InvalidLocation().locationInBoard.r);
		}
		jSONObject4.AddField("gridlocation", jSONObject5);
		jSONObject6.AddField("gridLocation", jSONObject7);
		JSONObject jSONObject8 = new JSONObject(JSONObject.Type.ARRAY);
		jSONObject.AddField("id", base.id.ToString());
		jSONObject.AddField("_id", _serverID);
		jSONObject.AddField("title", title);
		jSONObject.AddField("owner", owner);
		jSONObject.AddField("musicChoice", (int)musicChoice);
		jSONObject.AddField("cameraMode", (int)cameraMode);
		jSONObject.AddField("gravityOn", gravityOn);
		if (_hero != null)
		{
			jSONObject.AddField("hero", _hero.ID());
		}
		else
		{
			jSONObject.AddField("hero", "NULL");
		}
		jSONObject.AddField("heroStartPosition", jSONObject4);
		jSONObject.AddField("gameEndPosition", jSONObject6);
		if (coverBoard != null)
		{
			jSONObject.AddField("cover", coverBoard.ID());
		}
		jSONObject.AddField("levels", jSONObject2);
		string val = "NULL";
		if (background != null)
		{
			val = background.ID();
		}
		jSONObject.AddField("background", val);
		jSONObject.AddField("ugcApproved", ugcApproved);
		for (int i = 0; i < tags.Count; i++)
		{
			jSONObject8.Add(tags[i]);
		}
		jSONObject.AddField("tags", jSONObject8);
		jSONObject.AddField("isDirty", isDirty);
		jSONObject.AddField("builtWithVersion", builtWithVersion);
		return jSONObject.ToString();
	}

	public override bool Save(bool ballsDeep = true, bool shouldQueue = true)
	{
		bool flag = false;
		if (shouldQueue)
		{
			if (ballsDeep)
			{
				Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
				while (enumerator.MoveNext())
				{
					enumerator.Current.Value.Save(ballsDeep, shouldQueue);
				}
			}
			SaveManager.Instance.Enqueue(this);
			return false;
		}
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		string text = ToJSON();
		SetDirty(true);
		if (string.IsNullOrEmpty(text))
		{
			return false;
		}
		if (Directory.Exists(projectPath))
		{
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		else
		{
			Directory.CreateDirectory(projectPath);
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		if (File.Exists(projectPath + "/" + filename))
		{
			return true;
		}
		return false;
	}

	public override void SetDirty(bool isDirty)
	{
		base.SetDirty(isDirty);
		if (isDirty)
		{
			CloudManager.Instance.EnqueueFileForSync(syncInfo);
		}
	}

	public bool Delete()
	{
		bool flag = false;
		if (Directory.Exists(projectPath))
		{
			Directory.Delete(projectPath, true);
		}
		foreach (KeyValuePair<GridLocation, BloxelLevel> level in levels)
		{
			level.Value.Delete();
		}
		if (Directory.Exists(projectPath))
		{
			flag = false;
		}
		else
		{
			flag = true;
			if (ID() != null)
			{
				AssetManager.instance.gamePool.Remove(ID());
			}
		}
		CloudManager.Instance.RemoveFileFromQueue(syncInfo);
		return flag;
	}

	public static DirectoryInfo[] GetSavedDirectories()
	{
		DirectoryInfo directoryInfo = null;
		string path = MyProjectPath();
		if (Directory.Exists(path))
		{
			directoryInfo = new DirectoryInfo(path);
		}
		else
		{
			Directory.CreateDirectory(path);
			directoryInfo = new DirectoryInfo(path);
		}
		return (from p in directoryInfo.GetDirectories()
			orderby p.LastWriteTime descending
			select p).ToArray();
	}

	public void SetTitle(string _title, bool shouldSave = true)
	{
		base.SetTitle(_title);
		if (shouldSave)
		{
			Save(false);
		}
	}

	public new void SetOwner(string _owner)
	{
		base.SetOwner(_owner);
		Save(false);
	}

	public new void CheckAndSetMeAsOwner()
	{
		base.CheckAndSetMeAsOwner();
		Save(false);
	}

	public void SetGameGravity(bool _isOn, bool shouldSave = true)
	{
		gravityOn = _isOn;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetMusicChoice(MusicChoice _choice, bool shouldSave = true)
	{
		musicChoice = _choice;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetCameraMode(BloxelCamera.CameraMode _choice, bool shouldSave = true)
	{
		cameraMode = _choice;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetHero(BloxelCharacter hero, bool shouldSave = true)
	{
		_hero = hero;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetHeroAtLocation(BloxelCharacter hero, WorldLocation worldLocation, bool shouldSave = true)
	{
		_hero = hero;
		_heroStartPosition = worldLocation;
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			if (enumerator.Current.Value.id == worldLocation.id)
			{
				coverBoard = enumerator.Current.Value.coverBoard;
				coverBoard.BlastUpdate();
			}
		}
		if (shouldSave)
		{
			Save(false);
		}
		BlastUpdate();
	}

	public void MoveHero(GridLocation levelLocInWorld, GridLocation locInLevel, bool shouldSave = false)
	{
		Guid empty = Guid.Empty;
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			if (enumerator.Current.Value.location == levelLocInWorld)
			{
				empty = enumerator.Current.Value.id;
			}
		}
		_heroStartPosition = new WorldLocation(empty, locInLevel, levelLocInWorld);
		if (shouldSave)
		{
			Save(false);
		}
		BlastUpdate();
	}

	public void SetGameEndFlagAtLocation(WorldLocation _worldLoc, bool shouldSave = true)
	{
		gameEndFlag = _worldLoc;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void RemoveGameEndFlag(WorldLocation loc, bool shouldSave = true)
	{
		if (!(loc != gameEndFlag))
		{
			gameEndFlag = WorldLocation.InvalidLocation();
			if (shouldSave)
			{
				Save(false);
			}
		}
	}

	public bool MoveLevel(GridLocation current, GridLocation destination)
	{
		if (levels.ContainsKey(destination))
		{
			return false;
		}
		BloxelLevel value = null;
		if (!levels.TryGetValue(current, out value))
		{
			return false;
		}
		levels.Remove(current);
		if (heroStartPosition.id == value.id)
		{
			GridLocation locationInBoard = heroStartPosition.locationInBoard;
			SetHeroAtLocation(hero, new WorldLocation(value.id, locationInBoard, destination), false);
		}
		SetLevelAtLocation(destination, value);
		return true;
	}

	public BloxelLevel GetLevelAtLocation(GridLocation loc)
	{
		BloxelLevel value = null;
		levels.TryGetValue(loc, out value);
		return value;
	}

	public void SetLevelAtLocation(GridLocation loc, BloxelLevel level)
	{
		level.location = loc;
		levels[loc] = level;
		level.Save(false, false);
		worldBoundingRect = GetBoundingRectForLevels();
		Save(false);
		BlastUpdate();
	}

	public bool SetBackground(BloxelMegaBoard megaBoard, bool shouldSave = true)
	{
		background = megaBoard;
		BlastUpdate();
		bool result = false;
		if (shouldSave)
		{
			result = Save(false);
		}
		return result;
	}

	public void RemoveLevelAtLocation(GridLocation loc)
	{
		bool flag = false;
		bool flag2 = false;
		if (levels.Count > 1 && levels.ContainsKey(loc))
		{
			if (coverBoard.id == levels[loc].coverBoard.id)
			{
				flag = true;
			}
			if (heroStartPosition.id == levels[loc].id)
			{
				flag2 = true;
			}
			levels.Remove(loc);
			if (flag)
			{
				coverBoard = levels.First().Value.coverBoard;
			}
			if (flag2)
			{
				BloxelLevel value = levels.First().Value;
				SetHeroAtLocation(hero, new WorldLocation(value.id, heroStartPosition.locationInBoard, value.location), false);
			}
			worldBoundingRect = GetBoundingRectForLevels();
			Save(false);
			BlastUpdate();
		}
	}

	public void BlastUpdate()
	{
		if (this.OnUpdate != null)
		{
			this.OnUpdate();
		}
	}

	public GridLocation GetLevelPositionFromWorldLocation(WorldLocation worldLoc)
	{
		foreach (KeyValuePair<GridLocation, BloxelLevel> level in levels)
		{
			if (level.Value.id == worldLoc.id)
			{
				return level.Key;
			}
		}
		return GridLocation.InvalidLocation;
	}

	public Rect GetBoundingRectForLevels()
	{
		int num = 13;
		int num2 = 0;
		int num3 = 0;
		int num4 = 13;
		foreach (KeyValuePair<GridLocation, BloxelLevel> level in levels)
		{
			if (level.Key.c <= num)
			{
				num = level.Key.c;
			}
			if (level.Key.c >= num2)
			{
				num2 = level.Key.c;
			}
			if (level.Key.r <= num4)
			{
				num4 = level.Key.r;
			}
			if (level.Key.r >= num3)
			{
				num3 = level.Key.r;
			}
		}
		num3++;
		num2++;
		num *= 169;
		num3 *= 169;
		num2 *= 169;
		num4 *= 169;
		return new Rect(num, num4, num2 - num, num3 - num4);
	}

	public int TotalCoinsInGame()
	{
		int num = 0;
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			num += enumerator.Current.Value.TotalCoins();
		}
		return num;
	}

	public int TotalEnemiesInGame()
	{
		int num = 0;
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			num += enumerator.Current.Value.TotalEnemies();
		}
		return num;
	}

	public int TotalDestructible()
	{
		int num = 0;
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			num += enumerator.Current.Value.TotalDestructible();
			num += enumerator.Current.Value.TotalEnemies();
		}
		return num;
	}

	public Dictionary<string, BloxelBoard> UniqueBoards(bool includeBackgroundBoards = true)
	{
		Dictionary<string, BloxelBoard> dictionary = new Dictionary<string, BloxelBoard>();
		foreach (BloxelLevel value in levels.Values)
		{
			Dictionary<string, BloxelBoard> dictionary2 = value.UniqueBoards();
			foreach (BloxelBoard value2 in dictionary2.Values)
			{
				if (!dictionary.ContainsKey(value2.ID()))
				{
					dictionary.Add(value2.ID(), value2);
				}
			}
		}
		if (includeBackgroundBoards && background != null)
		{
			foreach (BloxelBoard value3 in background.boards.Values)
			{
				if (!dictionary.ContainsKey(value3.ID()))
				{
					dictionary.Add(value3.ID(), value3);
				}
			}
		}
		return dictionary;
	}

	public Dictionary<string, BloxelAnimation> UniqueAnimations(bool includeHeroAnimations = true)
	{
		Dictionary<string, BloxelAnimation> dictionary = new Dictionary<string, BloxelAnimation>();
		foreach (BloxelLevel value in levels.Values)
		{
			Dictionary<string, BloxelAnimation> dictionary2 = value.UniqueAnimations();
			Dictionary<string, BloxelAnimation>.Enumerator enumerator2 = dictionary2.GetEnumerator();
			while (enumerator2.MoveNext())
			{
				if (!dictionary.ContainsKey(enumerator2.Current.Key))
				{
					dictionary.Add(enumerator2.Current.Key, enumerator2.Current.Value);
				}
			}
		}
		if (includeHeroAnimations)
		{
			Dictionary<string, BloxelAnimation> dictionary3 = hero.UniqueAnimations();
			foreach (BloxelAnimation value2 in dictionary3.Values)
			{
				if (!dictionary.ContainsKey(value2.ID()))
				{
					dictionary.Add(value2.ID(), value2);
				}
			}
		}
		return dictionary;
	}

	public override int GetBloxelCount()
	{
		int num = 0;
		Dictionary<string, BloxelBoard> dictionary = UniqueBoards();
		Dictionary<string, BloxelBoard>.Enumerator enumerator = dictionary.GetEnumerator();
		while (enumerator.MoveNext())
		{
			num += enumerator.Current.Value.GetBloxelCount();
		}
		return num;
	}

	public List<string> GetAssociatedBoardPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelLevel value in levels.Values)
		{
			List<string> associatedBoardPaths = value.GetAssociatedBoardPaths();
			for (int i = 0; i < associatedBoardPaths.Count; i++)
			{
				if (!list.Contains(associatedBoardPaths[i]))
				{
					list.Add(associatedBoardPaths[i]);
				}
			}
		}
		if (hero != null)
		{
			List<string> associatedBoardPaths2 = hero.GetAssociatedBoardPaths();
			for (int j = 0; j < associatedBoardPaths2.Count; j++)
			{
				if (!list.Contains(associatedBoardPaths2[j]))
				{
					list.Add(associatedBoardPaths2[j]);
				}
			}
		}
		if (background != null)
		{
			foreach (BloxelBoard value2 in background.boards.Values)
			{
				if (!list.Contains(value2.projectPath))
				{
					value2.CheckAndSetMeAsOwner();
					list.Add(value2.projectPath);
				}
			}
		}
		return list;
	}

	public List<string> GetAssociatedAnimationPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelLevel value in levels.Values)
		{
			List<string> associatedAnimationPaths = value.GetAssociatedAnimationPaths();
			for (int i = 0; i < associatedAnimationPaths.Count; i++)
			{
				if (!list.Contains(associatedAnimationPaths[i]))
				{
					list.Add(associatedAnimationPaths[i]);
				}
			}
		}
		if (hero != null)
		{
			foreach (BloxelAnimation value2 in hero.animations.Values)
			{
				if (!list.Contains(value2.projectPath))
				{
					list.Add(value2.projectPath);
				}
			}
		}
		return list;
	}

	public List<string> GetAssociatedLevelPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelLevel value in levels.Values)
		{
			if (!list.Contains(value.projectPath))
			{
				list.Add(value.projectPath);
			}
		}
		return list;
	}

	public List<string> GetAssociatedBrainPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelLevel value in levels.Values)
		{
			List<string> associatedBrainPaths = value.GetAssociatedBrainPaths();
			for (int i = 0; i < associatedBrainPaths.Count; i++)
			{
				if (!list.Contains(associatedBrainPaths[i]))
				{
					list.Add(associatedBrainPaths[i]);
				}
			}
		}
		return list;
	}

	public override ProjectBundle Compress()
	{
		List<string> associatedBoardPaths = GetAssociatedBoardPaths();
		List<string> associatedAnimationPaths = GetAssociatedAnimationPaths();
		List<string> associatedBrainPaths = GetAssociatedBrainPaths();
		List<string> associatedLevelPaths = GetAssociatedLevelPaths();
		List<string> list = new List<string>();
		if (background != null)
		{
			list.Add(background.projectPath);
		}
		List<string> list2 = new List<string>();
		if (hero != null)
		{
			list2.Add(hero.projectPath);
		}
		CheckAndSetMeAsOwner();
		string[] array = new string[associatedBoardPaths.Count + associatedAnimationPaths.Count + associatedLevelPaths.Count + list.Count + list2.Count + associatedBrainPaths.Count];
		string[] array2 = new string[associatedBoardPaths.Count + associatedAnimationPaths.Count + associatedLevelPaths.Count + list.Count + list2.Count + associatedBrainPaths.Count];
		int num = 0;
		int num2 = 0;
		while (num2 < associatedBoardPaths.Count)
		{
			array[num] = associatedBoardPaths[num2] + "/" + BloxelBoard.filename;
			string[] array3 = associatedBoardPaths[num2].Split('/');
			array2[num] = array3[array3.Length - 1];
			num2++;
			num++;
		}
		int num3 = 0;
		while (num3 < associatedAnimationPaths.Count)
		{
			array[num] = associatedAnimationPaths[num3] + "/" + BloxelAnimation.filename;
			string[] array4 = associatedAnimationPaths[num3].Split('/');
			array2[num] = array4[array4.Length - 1];
			num3++;
			num++;
		}
		int num4 = 0;
		while (num4 < associatedBrainPaths.Count)
		{
			array[num] = associatedBrainPaths[num4] + "/" + BloxelBrain.filename;
			string[] array5 = associatedBrainPaths[num4].Split('/');
			array2[num] = array5[array5.Length - 1];
			num4++;
			num++;
		}
		int num5 = 0;
		while (num5 < associatedLevelPaths.Count)
		{
			array[num] = associatedLevelPaths[num5] + "/" + BloxelLevel.filename;
			string[] array6 = associatedLevelPaths[num5].Split('/');
			array2[num] = array6[array6.Length - 1];
			num5++;
			num++;
		}
		int num6 = 0;
		while (num6 < list.Count)
		{
			array[num] = list[num6] + "/" + BloxelMegaBoard.filename;
			string[] array7 = list[num6].Split('/');
			array2[num] = array7[array7.Length - 1];
			num6++;
			num++;
		}
		int num7 = 0;
		while (num7 < list2.Count)
		{
			array[num] = list2[num7] + "/" + BloxelCharacter.filename;
			string[] array8 = list2[num7].Split('/');
			array2[num] = array8[array8.Length - 1];
			num7++;
			num++;
		}
		return new ProjectBundle(Zipper.CreateBytes(array, array2), ToJSON());
	}

	public void SetCoverTexturePixels(Sprite sprite, Color clearColor)
	{
		BloxelLevel value = null;
		if (!levels.TryGetValue(heroStartPosition.levelLocationInWorld, out value))
		{
			return;
		}
		Texture2D texture = sprite.texture;
		int num = (int)sprite.rect.x;
		int num2 = (int)sprite.rect.y;
		BlockColor[,] blockColors = value.coverBoard.blockColors;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				GridLocation gridLocation = new GridLocation(j, i);
				int x = num + j * 13;
				int y = num2 + i * 13;
				BloxelBoard value2 = null;
				if (value.backgroundTiles.TryGetValue(gridLocation, out value2))
				{
					value2.GetColorsNonAlloc(ref boardColorBuffer);
				}
				else
				{
					for (int k = 0; k < 169; k++)
					{
						boardColorBuffer[k] = clearColor;
					}
				}
				BlockColor blockColor = blockColors[j, i];
				if (blockColor == BlockColor.Blank)
				{
					texture.SetPixels(x, y, 13, 13, boardColorBuffer);
					continue;
				}
				BloxelProject projectModelAtLocation = value.GetProjectModelAtLocation(gridLocation);
				if (projectModelAtLocation == null)
				{
					Color color = AssetManager.instance.unityColorsForBlockColors[(uint)blockColor];
					for (int l = 0; l < 169; l++)
					{
						boardColorBuffer[l] = color;
					}
					texture.SetPixels(x, y, 13, 13, boardColorBuffer);
				}
				else
				{
					projectModelAtLocation.coverBoard.GetColorsNonAlloc(ref boardColorBuffer, false);
					texture.SetPixels(x, y, 13, 13, boardColorBuffer);
				}
			}
		}
		texture.Apply();
	}
}
