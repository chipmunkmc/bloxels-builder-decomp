using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloxelGameLibrary : BloxelLibraryWindow
{
	public static BloxelGameLibrary instance;

	public Object savedGamePrefab;

	private bool initComplete;

	public BloxelGame currentSavedGame { get; private set; }

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
	}

	private new void Start()
	{
		if (!initComplete)
		{
			PopulateLibrary();
			base.Start();
			base.OnProjectSelect += SetCurrentProject;
			initComplete = true;
		}
	}

	private void Destroy()
	{
		base.OnProjectSelect -= SetCurrentProject;
	}

	private new void DestroyLibrary()
	{
		base.DestroyLibrary();
	}

	public void SetCurrentProject(SavedProject item)
	{
		BloxelGame bloxelGame = item.dataModel as BloxelGame;
		if (currentSavedGame == null || currentSavedGame != bloxelGame)
		{
			currentSavedGame = bloxelGame;
			UIHelpLink.instance.location = UIHelpLink.Location.Games;
			BloxelLibraryScrollController.instance.DisablePreviousSelectedOutline();
			SoundManager.PlayOneShot(SoundManager.instance.popA);
		}
	}

	public void PopulateLibrary()
	{
		BloxelLibraryScrollController.instance.unfilteredGameData.Clear();
		Dictionary<string, BloxelGame>.ValueCollection.Enumerator enumerator = AssetManager.instance.gamePool.Values.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelLibraryScrollController.instance.unfilteredGameData.Add(enumerator.Current);
		}
	}

	public override void UpdateScrollerContents()
	{
		BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Game);
	}

	public new void AddButtonPressed()
	{
		base.AddButtonPressed();
		GameBuilderCanvas.instance.currentMode = GameBuilderMode.None;
	}

	public void CreateAndSelectFromQuickstart(BloxelGame bloxelGame)
	{
		if (AssetManager.instance.gamePool.ContainsKey(bloxelGame.ID()))
		{
			return;
		}
		AssetManager.instance.gamePool.Add(bloxelGame.ID(), bloxelGame);
		BloxelLibraryScrollController.instance.AddItem(bloxelGame);
		StartCoroutine(DelayedItemSelect());
		BloxelCharacterLibrary.instance.AddProject(bloxelGame.hero);
		BloxelMegaBoardLibrary.instance.AddProject(bloxelGame.background);
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = bloxelGame.levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelLevel value = enumerator.Current.Value;
			string key = value.ID();
			if (!AssetManager.instance.levelPool.ContainsKey(key))
			{
				AssetManager.instance.levelPool.Add(key, value);
			}
			Dictionary<string, BloxelBoard> dictionary = value.UniqueBoards(false);
			Dictionary<string, BloxelBoard>.Enumerator enumerator2 = dictionary.GetEnumerator();
			while (enumerator2.MoveNext())
			{
				if (enumerator2.Current.Value.id != value.coverBoard.id)
				{
					BloxelBoardLibrary.instance.AddBoard(enumerator2.Current.Value);
				}
			}
		}
		Dictionary<string, BloxelAnimation> dictionary2 = bloxelGame.UniqueAnimations(false);
		Dictionary<string, BloxelAnimation>.Enumerator enumerator3 = dictionary2.GetEnumerator();
		while (enumerator3.MoveNext())
		{
			BloxelAnimationLibrary.instance.AddProject(enumerator3.Current.Value);
		}
		StartCoroutine(GameQuickStartMenu.instance.SaveProject(bloxelGame, true));
	}

	public BloxelGame CreateEmptyProject(string title)
	{
		BloxelBoard bloxelBoard = BloxelBoard.DefaultWireframe();
		BloxelLevel bloxelLevel = new BloxelLevel(bloxelBoard);
		BloxelGame bloxelGame = new BloxelGame(bloxelLevel);
		BloxelCharacter value = null;
		if (!AssetManager.instance.characterPool.TryGetValue(AssetManager.instance.defaultCharacterID, out value))
		{
			bloxelBoard.Delete();
			bloxelLevel.Delete();
			bloxelGame.Delete();
			return null;
		}
		bloxelGame.SetBuildVersion(AppStateManager.instance.internalVersion);
		bloxelGame.SetHeroAtLocation(value, new WorldLocation(bloxelLevel.id, new GridLocation(1, 2), bloxelLevel.location), false);
		bloxelGame.SetTitle(title, false);
		bloxelGame.Save();
		BloxelLibraryScrollController.instance.AddItem(bloxelGame);
		if (bloxelLevel.Save() && bloxelGame.Save())
		{
			AssetManager.instance.gamePool.Add(bloxelGame.ID(), bloxelGame);
			AssetManager.instance.levelPool.Add(bloxelLevel.ID(), bloxelLevel);
			AssetManager.instance.boardPool.Add(bloxelBoard.ID(), bloxelBoard);
		}
		StartCoroutine(DelayedItemSelect());
		return bloxelGame;
	}

	private IEnumerator DelayedItemSelect()
	{
		yield return new WaitForEndOfFrame();
		ItemSelect(BloxelLibraryScrollController.instance.GetFirstItem());
		SavedGame savedGame = BloxelLibraryScrollController.instance.GetSelectedProject<SavedGame>();
		if (savedGame != null)
		{
			savedGame.AnimatedInit(currentSavedGame);
		}
	}

	public void Reset()
	{
		if (currentSavedGame != null)
		{
			currentSavedGame = null;
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Game);
		}
	}
}
