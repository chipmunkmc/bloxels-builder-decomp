using UnityEngine;

public sealed class BloxelHazardTile : BloxelTile
{
	public static string collider2DLayer = "Hazard";

	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.hazardPool.Push(this);
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(collider2DLayer);
		tileRenderer.colliderIsTrigger = false;
		tileRenderer.tileScale = new Vector3(1f, 1f, 13f);
	}
}
