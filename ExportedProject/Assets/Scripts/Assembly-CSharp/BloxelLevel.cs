using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using UnityEngine;

public sealed class BloxelLevel : BloxelProject
{
	public static GridLocation defaultLocation = new GridLocation(6, 6);

	public static string filename = "level.json";

	public static string rootDirectory = "Levels";

	public static string subDirectory = "myLevels";

	public static string defaultTitle = "My Level";

	public GridLocation location;

	public Dictionary<GridLocation, BloxelBoard> boards;

	public Dictionary<GridLocation, BloxelCharacter> characters;

	public Dictionary<GridLocation, BloxelAnimation> animations;

	public Dictionary<GridLocation, string> npcTextBlocks;

	public Dictionary<GridLocation, EnemyType> enemyTypes;

	public Dictionary<GridLocation, PowerUpType> powerups;

	public Dictionary<GridLocation, BloxelBoard> backgroundTiles;

	public Dictionary<GridLocation, BloxelBrain> brains;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024mapC;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024mapD;

	public BloxelLevel(BloxelBoard b = null, bool setProjectPath = true)
	{
		type = ProjectType.Level;
		base.id = GenerateRandomID();
		if (setProjectPath)
		{
			projectPath = MyProjectPath() + "/" + ID();
		}
		location = defaultLocation;
		if (b != null)
		{
			coverBoard = b;
		}
		boards = new Dictionary<GridLocation, BloxelBoard>();
		animations = new Dictionary<GridLocation, BloxelAnimation>();
		characters = new Dictionary<GridLocation, BloxelCharacter>();
		npcTextBlocks = new Dictionary<GridLocation, string>();
		enemyTypes = new Dictionary<GridLocation, EnemyType>();
		brains = new Dictionary<GridLocation, BloxelBrain>();
		powerups = new Dictionary<GridLocation, PowerUpType>();
		backgroundTiles = new Dictionary<GridLocation, BloxelBoard>();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelLevel(GridLocation loc, BloxelBoard b)
	{
		type = ProjectType.Level;
		base.id = GenerateRandomID();
		projectPath = MyProjectPath() + "/" + ID();
		location = loc;
		coverBoard = b;
		boards = new Dictionary<GridLocation, BloxelBoard>();
		animations = new Dictionary<GridLocation, BloxelAnimation>();
		characters = new Dictionary<GridLocation, BloxelCharacter>();
		npcTextBlocks = new Dictionary<GridLocation, string>();
		brains = new Dictionary<GridLocation, BloxelBrain>();
		enemyTypes = new Dictionary<GridLocation, EnemyType>();
		powerups = new Dictionary<GridLocation, PowerUpType>();
		backgroundTiles = new Dictionary<GridLocation, BloxelBoard>();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelLevel(string s, DataSource source)
	{
		type = ProjectType.Level;
		if (source == DataSource.FilePath)
		{
			using (TextReader reader = File.OpenText(s + "/" + filename))
			{
				JsonTextReader reader2 = new JsonTextReader(reader);
				FromReader(reader2, false);
			}
		}
		else
		{
			FromJSONString(s, true);
		}
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelLevel(JsonTextReader reader, bool fromServer)
	{
		type = ProjectType.Level;
		FromReader(reader, fromServer);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelLevel(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		type = ProjectType.Level;
		FromReaderSlim(reader, projectPool);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public static string BaseStoragePath()
	{
		return Application.persistentDataPath + "/" + rootDirectory;
	}

	public static string MyProjectPath()
	{
		return AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory;
	}

	public void FromJSONString(string jsonString, bool fromServer)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader, fromServer);
	}

	public void FromReader(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		boards = new Dictionary<GridLocation, BloxelBoard>();
		animations = new Dictionary<GridLocation, BloxelAnimation>();
		characters = new Dictionary<GridLocation, BloxelCharacter>();
		npcTextBlocks = new Dictionary<GridLocation, string>();
		brains = new Dictionary<GridLocation, BloxelBrain>();
		enemyTypes = new Dictionary<GridLocation, EnemyType>();
		powerups = new Dictionary<GridLocation, PowerUpType>();
		backgroundTiles = new Dictionary<GridLocation, BloxelBoard>();
		while (reader.Read())
		{
			if (reader.Value == null)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				if (text == null)
				{
					continue;
				}
				if (_003C_003Ef__switch_0024mapC == null)
				{
					Dictionary<string, int> dictionary = new Dictionary<string, int>(10);
					dictionary.Add("wireframe", 0);
					dictionary.Add("backgroundTiles", 1);
					dictionary.Add("powerups", 2);
					dictionary.Add("enemyTypes", 3);
					dictionary.Add("brains", 4);
					dictionary.Add("boards", 5);
					dictionary.Add("animations", 6);
					dictionary.Add("characters", 7);
					dictionary.Add("npcTextBlocks", 8);
					dictionary.Add("location", 9);
					_003C_003Ef__switch_0024mapC = dictionary;
				}
				int value;
				if (!_003C_003Ef__switch_0024mapC.TryGetValue(text, out value))
				{
					continue;
				}
				switch (value)
				{
				case 0:
					ParseWireframe(reader, fromServer);
					break;
				case 1:
					ParseBackgroundTiles(reader, fromServer);
					break;
				case 2:
					ParsePowerUps(reader);
					break;
				case 3:
					ParseEnemyTypes(reader);
					break;
				case 4:
					ParseBrains(reader, fromServer);
					break;
				case 5:
					ParseBoards(reader, fromServer);
					break;
				case 6:
					ParseAnimations(reader, fromServer);
					break;
				case 8:
					ParseNPCTextBlocks(reader);
					break;
				case 9:
					ParseLocation(reader);
					projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
					if (fromServer)
					{
						return;
					}
					break;
				}
			}
			else
			{
				switch (text)
				{
				case "id":
					base.id = new Guid(reader.Value.ToString());
					break;
				case "isDirty":
					isDirty = bool.Parse(reader.Value.ToString());
					break;
				case "builtWithVersion":
					SetBuildVersion(int.Parse(reader.Value.ToString()));
					break;
				}
			}
		}
	}

	private void FromReaderSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		boards = new Dictionary<GridLocation, BloxelBoard>();
		animations = new Dictionary<GridLocation, BloxelAnimation>();
		characters = new Dictionary<GridLocation, BloxelCharacter>();
		npcTextBlocks = new Dictionary<GridLocation, string>();
		brains = new Dictionary<GridLocation, BloxelBrain>();
		enemyTypes = new Dictionary<GridLocation, EnemyType>();
		powerups = new Dictionary<GridLocation, PowerUpType>();
		backgroundTiles = new Dictionary<GridLocation, BloxelBoard>();
		while (reader.Read())
		{
			if (reader.Value == null)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				if (text == null)
				{
					continue;
				}
				if (_003C_003Ef__switch_0024mapD == null)
				{
					Dictionary<string, int> dictionary = new Dictionary<string, int>(10);
					dictionary.Add("wireframe", 0);
					dictionary.Add("backgroundTiles", 1);
					dictionary.Add("powerups", 2);
					dictionary.Add("enemyTypes", 3);
					dictionary.Add("brains", 4);
					dictionary.Add("boards", 5);
					dictionary.Add("animations", 6);
					dictionary.Add("characters", 7);
					dictionary.Add("npcTextBlocks", 8);
					dictionary.Add("location", 9);
					_003C_003Ef__switch_0024mapD = dictionary;
				}
				int value;
				if (_003C_003Ef__switch_0024mapD.TryGetValue(text, out value))
				{
					switch (value)
					{
					case 0:
						ParseWireframeSlim(reader, projectPool);
						break;
					case 1:
						ParseBackgroundTilesSlim(reader, projectPool);
						break;
					case 2:
						ParsePowerUps(reader);
						break;
					case 3:
						ParseEnemyTypes(reader);
						break;
					case 4:
						ParseBrainsSlim(reader, projectPool);
						break;
					case 5:
						ParseBoardsSlim(reader, projectPool);
						break;
					case 6:
						ParseAnimationsSlim(reader, projectPool);
						break;
					case 8:
						ParseNPCTextBlocks(reader);
						break;
					case 9:
						ParseLocation(reader);
						projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
						return;
					}
				}
			}
			else
			{
				switch (text)
				{
				case "id":
					base.id = new Guid(reader.Value.ToString());
					break;
				case "isDirty":
					isDirty = bool.Parse(reader.Value.ToString());
					break;
				case "builtWithVersion":
					SetBuildVersion(int.Parse(reader.Value.ToString()));
					break;
				}
			}
		}
	}

	private void ParseWireframe(JsonTextReader reader, bool fromServer)
	{
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.TokenType == JsonToken.StartObject)
				{
					coverBoard = new BloxelBoard(reader);
					break;
				}
			}
		}
		else
		{
			reader.Read();
			if (reader.TokenType != JsonToken.Null && !AssetManager.instance.boardPool.TryGetValue(reader.Value.ToString(), out coverBoard))
			{
				coverBoard = new BloxelBoard();
				coverBoard.Save();
			}
		}
	}

	private void ParseWireframeSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		reader.Read();
		if (reader.TokenType != JsonToken.Null)
		{
			BloxelProject value = null;
			if (projectPool.TryGetValue(reader.Value.ToString(), out value) && value.type == ProjectType.Board)
			{
				coverBoard = value as BloxelBoard;
			}
		}
	}

	private void ParseLocation(JsonTextReader reader)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				location = new GridLocation(col, row);
				break;
			}
		}
	}

	private void ParseBoards(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					if (reader.TokenType == JsonToken.PropertyName)
					{
						text = reader.Value.ToString();
						if (text == "board")
						{
							boards.Add(new GridLocation(col, row), new BloxelBoard(reader));
						}
						continue;
					}
					switch (text)
					{
					case "c":
						col = int.Parse(reader.Value.ToString());
						break;
					case "r":
						row = int.Parse(reader.Value.ToString());
						break;
					}
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					break;
				}
			}
			return;
		}
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "id":
				{
					string key = reader.Value.ToString();
					BloxelBoard value = null;
					if (AssetManager.instance.boardPool.TryGetValue(key, out value))
					{
						boards[new GridLocation(col, row)] = value;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBoardsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "board":
				{
					string key = reader.Value.ToString();
					BloxelProject value = null;
					if (projectPool.TryGetValue(key, out value) && value.type == ProjectType.Board)
					{
						boards[new GridLocation(col, row)] = value as BloxelBoard;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseAnimations(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					if (reader.TokenType == JsonToken.PropertyName)
					{
						text = reader.Value.ToString();
						if (reader.Value.ToString() == "animation")
						{
							animations.Add(new GridLocation(col, row), new BloxelAnimation(reader, fromServer));
						}
						continue;
					}
					switch (text)
					{
					case "c":
						col = int.Parse(reader.Value.ToString());
						break;
					case "r":
						row = int.Parse(reader.Value.ToString());
						break;
					}
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					break;
				}
			}
			return;
		}
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "id":
				{
					string key = reader.Value.ToString();
					BloxelAnimation value = null;
					if (AssetManager.instance.animationPool.TryGetValue(key, out value))
					{
						animations[new GridLocation(col, row)] = value;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseAnimationsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "animation":
				{
					string key = reader.Value.ToString();
					BloxelProject value = null;
					if (projectPool.TryGetValue(key, out value) && value.type == ProjectType.Animation)
					{
						animations[new GridLocation(col, row)] = value as BloxelAnimation;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseNPCTextBlocks(JsonTextReader reader)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		string text2 = null;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "textLocKey":
					text2 = reader.Value.ToString();
					break;
				case "text":
					if (text2 != null)
					{
						npcTextBlocks.Add(new GridLocation(col, row), LocalizationManager.getInstance().getLocalizedText(text2, reader.Value.ToString()));
					}
					else
					{
						npcTextBlocks.Add(new GridLocation(col, row), reader.Value.ToString());
					}
					break;
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseEnemyTypes(JsonTextReader reader)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "enemyType":
					enemyTypes.Add(new GridLocation(col, row), (EnemyType)int.Parse(reader.Value.ToString()));
					break;
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBrains(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					if (reader.TokenType == JsonToken.PropertyName)
					{
						text = reader.Value.ToString();
						if (text == "brain")
						{
							brains.Add(new GridLocation(col, row), new BloxelBrain(reader));
						}
						continue;
					}
					switch (text)
					{
					case "c":
						col = int.Parse(reader.Value.ToString());
						break;
					case "r":
						row = int.Parse(reader.Value.ToString());
						break;
					}
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					break;
				}
			}
			return;
		}
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "brain":
				{
					string key = reader.Value.ToString();
					BloxelBrain value = null;
					if (AssetManager.instance.brainPool.TryGetValue(key, out value))
					{
						brains[new GridLocation(col, row)] = value;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBrainsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "brain":
				{
					string key = reader.Value.ToString();
					BloxelProject value = null;
					if (projectPool.TryGetValue(key, out value) && value.type == ProjectType.Brain)
					{
						brains[new GridLocation(col, row)] = value as BloxelBrain;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParsePowerUps(JsonTextReader reader)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "powerup":
					powerups.Add(new GridLocation(col, row), (PowerUpType)int.Parse(reader.Value.ToString()));
					break;
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBackgroundTiles(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					if (reader.TokenType == JsonToken.PropertyName)
					{
						text = reader.Value.ToString();
						if (text == "board")
						{
							backgroundTiles.Add(new GridLocation(col, row), new BloxelBoard(reader));
						}
						continue;
					}
					switch (text)
					{
					case "c":
						col = int.Parse(reader.Value.ToString());
						break;
					case "r":
						row = int.Parse(reader.Value.ToString());
						break;
					}
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					break;
				}
			}
			return;
		}
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "id":
				{
					string key = reader.Value.ToString();
					BloxelBoard value = null;
					if (AssetManager.instance.boardPool.TryGetValue(key, out value))
					{
						backgroundTiles[new GridLocation(col, row)] = value;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	private void ParseBackgroundTilesSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "board":
				{
					string key = reader.Value.ToString();
					BloxelProject value = null;
					if (projectPool.TryGetValue(key, out value) && value.type == ProjectType.Board)
					{
						backgroundTiles[new GridLocation(col, row)] = value as BloxelBoard;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	public string ToJSON()
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject jSONObject2 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject3 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject4 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject5 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject6 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject7 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject8 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject9 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject10 = new JSONObject();
		jSONObject10.AddField("c", location.c);
		jSONObject10.AddField("r", location.r);
		foreach (KeyValuePair<GridLocation, BloxelBoard> board in boards)
		{
			JSONObject jSONObject11 = new JSONObject();
			jSONObject11.AddField("c", board.Key.c);
			jSONObject11.AddField("r", board.Key.r);
			jSONObject11.AddField("id", board.Value.ID());
			jSONObject2.Add(jSONObject11);
		}
		foreach (KeyValuePair<GridLocation, BloxelAnimation> animation in animations)
		{
			JSONObject jSONObject12 = new JSONObject();
			jSONObject12.AddField("c", animation.Key.c);
			jSONObject12.AddField("r", animation.Key.r);
			jSONObject12.AddField("id", animation.Value.ID());
			jSONObject3.Add(jSONObject12);
		}
		foreach (KeyValuePair<GridLocation, BloxelCharacter> character in characters)
		{
			JSONObject jSONObject13 = new JSONObject();
			jSONObject13.AddField("c", character.Key.c);
			jSONObject13.AddField("r", character.Key.r);
			jSONObject13.AddField("id", character.Value.ID());
			jSONObject4.Add(jSONObject13);
		}
		foreach (KeyValuePair<GridLocation, string> npcTextBlock in npcTextBlocks)
		{
			JSONObject jSONObject14 = new JSONObject();
			jSONObject14.AddField("c", npcTextBlock.Key.c);
			jSONObject14.AddField("r", npcTextBlock.Key.r);
			jSONObject14.AddField("text", npcTextBlock.Value);
			jSONObject5.Add(jSONObject14);
		}
		foreach (KeyValuePair<GridLocation, BloxelBrain> brain in brains)
		{
			JSONObject jSONObject15 = new JSONObject();
			jSONObject15.AddField("c", brain.Key.c);
			jSONObject15.AddField("r", brain.Key.r);
			jSONObject15.AddField("brain", brain.Value.ID());
			jSONObject7.Add(jSONObject15);
		}
		foreach (KeyValuePair<GridLocation, EnemyType> enemyType in enemyTypes)
		{
			JSONObject jSONObject16 = new JSONObject();
			jSONObject16.AddField("c", enemyType.Key.c);
			jSONObject16.AddField("r", enemyType.Key.r);
			jSONObject16.AddField("enemyType", (int)enemyType.Value);
			jSONObject6.Add(jSONObject16);
		}
		foreach (KeyValuePair<GridLocation, PowerUpType> powerup in powerups)
		{
			JSONObject jSONObject17 = new JSONObject();
			jSONObject17.AddField("c", powerup.Key.c);
			jSONObject17.AddField("r", powerup.Key.r);
			jSONObject17.AddField("powerup", (int)powerup.Value);
			jSONObject8.Add(jSONObject17);
		}
		foreach (KeyValuePair<GridLocation, BloxelBoard> backgroundTile in backgroundTiles)
		{
			JSONObject jSONObject18 = new JSONObject();
			jSONObject18.AddField("c", backgroundTile.Key.c);
			jSONObject18.AddField("r", backgroundTile.Key.r);
			jSONObject18.AddField("id", backgroundTile.Value.ID());
			jSONObject9.Add(jSONObject18);
		}
		jSONObject.AddField("id", ID());
		jSONObject.AddField("location", jSONObject10);
		jSONObject.AddField("wireframe", coverBoard.ID());
		jSONObject.AddField("boards", jSONObject2);
		jSONObject.AddField("animations", jSONObject3);
		jSONObject.AddField("characters", jSONObject4);
		jSONObject.AddField("npcTextBlocks", jSONObject5);
		jSONObject.AddField("enemyTypes", jSONObject6);
		jSONObject.AddField("brains", jSONObject7);
		jSONObject.AddField("powerups", jSONObject8);
		jSONObject.AddField("backgroundTiles", jSONObject9);
		jSONObject.AddField("isDirty", isDirty);
		jSONObject.AddField("builtWithVersion", builtWithVersion);
		return jSONObject.ToString();
	}

	public override bool Save(bool deepSave = true, bool shouldQueue = true)
	{
		bool flag = false;
		if (shouldQueue)
		{
			coverBoard.Save(deepSave, shouldQueue);
			if (deepSave)
			{
				Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator = boards.GetEnumerator();
				while (enumerator.MoveNext())
				{
					enumerator.Current.Value.Save(deepSave, shouldQueue);
				}
				Dictionary<GridLocation, BloxelBrain>.Enumerator enumerator2 = brains.GetEnumerator();
				while (enumerator2.MoveNext())
				{
					enumerator2.Current.Value.Save(deepSave, shouldQueue);
				}
				Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator3 = backgroundTiles.GetEnumerator();
				while (enumerator3.MoveNext())
				{
					enumerator3.Current.Value.Save(deepSave, shouldQueue);
				}
				Dictionary<GridLocation, BloxelAnimation>.Enumerator enumerator4 = animations.GetEnumerator();
				while (enumerator4.MoveNext())
				{
					enumerator4.Current.Value.Save(deepSave, shouldQueue);
				}
				Dictionary<GridLocation, BloxelCharacter>.Enumerator enumerator5 = characters.GetEnumerator();
				while (enumerator5.MoveNext())
				{
					enumerator5.Current.Value.Save(deepSave, shouldQueue);
				}
			}
			SaveManager.Instance.Enqueue(this);
			return false;
		}
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		string text = ToJSON();
		SetDirty(true);
		if (string.IsNullOrEmpty(text))
		{
			return false;
		}
		if (Directory.Exists(projectPath))
		{
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		else
		{
			Directory.CreateDirectory(projectPath);
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		if (File.Exists(projectPath + "/" + filename))
		{
			return true;
		}
		return false;
	}

	public override void SetDirty(bool isDirty)
	{
		base.SetDirty(isDirty);
		if (isDirty)
		{
			CloudManager.Instance.EnqueueFileForSync(syncInfo);
		}
	}

	public bool Delete()
	{
		bool flag = false;
		if (Directory.Exists(projectPath))
		{
			Directory.Delete(projectPath, true);
		}
		if (Directory.Exists(projectPath))
		{
			flag = false;
		}
		else
		{
			flag = true;
			if (ID() != null)
			{
				AssetManager.instance.levelPool.Remove(ID());
			}
		}
		if (coverBoard != null)
		{
			flag = coverBoard.Delete();
		}
		CloudManager.Instance.RemoveFileFromQueue(syncInfo);
		return flag;
	}

	public static DirectoryInfo[] GetSavedDirectories()
	{
		DirectoryInfo directoryInfo = null;
		string path = MyProjectPath();
		if (Directory.Exists(path))
		{
			directoryInfo = new DirectoryInfo(path);
		}
		else
		{
			Directory.CreateDirectory(path);
			directoryInfo = new DirectoryInfo(path);
		}
		return (from p in directoryInfo.GetDirectories()
			orderby p.LastWriteTime descending
			select p).ToArray();
	}

	public static BloxelLevel GetSavedProjectById(string theID)
	{
		return new BloxelLevel(MyProjectPath() + "/" + theID, DataSource.FilePath);
	}

	public void SetWireframe(BloxelBoard board)
	{
		coverBoard = board;
		Save(false);
	}

	public bool SetNPCBlockAtLocation(GridLocation loc, string text)
	{
		npcTextBlocks[loc] = text;
		return Save(false);
	}

	public bool SetEnemyTypeAtLocation(GridLocation loc, EnemyType type)
	{
		enemyTypes[loc] = type;
		return Save(false);
	}

	public bool SetBrainAtLocation(GridLocation loc, BloxelBrain brain)
	{
		brains[loc] = brain;
		return Save(false);
	}

	public bool SetPowerUpAtLocation(GridLocation loc, PowerUpType type)
	{
		powerups[loc] = type;
		return Save(false);
	}

	public bool SetBackgroundTileAtLocation(GridLocation loc, BloxelBoard board)
	{
		backgroundTiles[loc] = board;
		return Save(false);
	}

	public void RemoveNPCBlockAtLocation(GridLocation loc)
	{
		if (npcTextBlocks.Remove(loc))
		{
			Save(false);
		}
	}

	public void RemoveEnemyTypeAtLocation(GridLocation loc)
	{
		if (enemyTypes.Remove(loc))
		{
			Save(false);
		}
	}

	public void RemoveBrainAtLocation(GridLocation loc)
	{
		if (brains.Remove(loc))
		{
			Save(false);
		}
	}

	public void RemovePowerUpTypeAtLocation(GridLocation loc)
	{
		if (powerups.Remove(loc))
		{
			Save(false);
		}
	}

	public void RemoveBackgroundTileAtLocation(GridLocation loc)
	{
		if (backgroundTiles.Remove(loc))
		{
			Save(false);
		}
	}

	public void SetBoardAtLocation(GridLocation loc, BloxelBoard b, bool shouldSave = true)
	{
		animations.Remove(loc);
		boards[loc] = b;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetAnimationAtLocation(GridLocation loc, BloxelAnimation anim, bool shouldSave = true)
	{
		boards.Remove(loc);
		animations[loc] = anim;
		if (shouldSave)
		{
			Save(false);
		}
	}

	public void SetCharacterAtLocation(GridLocation loc, BloxelCharacter character)
	{
		characters[loc] = character;
		Save(false);
	}

	public BloxelBoard GetCoverBoardAtLocation(GridLocation loc)
	{
		if (boards.ContainsKey(loc))
		{
			return boards[loc];
		}
		if (animations.ContainsKey(loc))
		{
			return animations[loc].boards[0];
		}
		if (characters.ContainsKey(loc))
		{
			if (characters[loc].animations[AnimationType.Idle] != null)
			{
				return characters[loc].animations[AnimationType.Idle].coverBoard;
			}
			return null;
		}
		return null;
	}

	public BloxelProject GetProjectModelAtLocation(GridLocation loc)
	{
		BloxelBoard value = null;
		if (boards.TryGetValue(loc, out value))
		{
			return value;
		}
		BloxelAnimation value2 = null;
		if (animations.TryGetValue(loc, out value2))
		{
			return value2;
		}
		BloxelCharacter value3 = null;
		if (characters.TryGetValue(loc, out value3))
		{
			return value3;
		}
		return null;
	}

	public BloxelProject GetBoardOrAnimationAtLocation(GridLocation loc)
	{
		BloxelBoard value = null;
		if (boards.TryGetValue(loc, out value))
		{
			return value;
		}
		BloxelAnimation value2 = null;
		if (animations.TryGetValue(loc, out value2))
		{
			return value2;
		}
		return null;
	}

	public bool BlockExistsAtLocation(GridLocation locationInLevel)
	{
		BlockColor blockColor = coverBoard.blockColors[locationInLevel.c, locationInLevel.r];
		if (blockColor == BlockColor.Blank)
		{
			return false;
		}
		return true;
	}

	public bool RemoveItemAtLocation(GridLocation loc, bool shouldSave = true)
	{
		BlockColor blockColor = coverBoard.blockColors[loc.c, loc.r];
		if (blockColor == BlockColor.Blank)
		{
			return false;
		}
		bool result = boards.Remove(loc) || animations.Remove(loc) || characters.Remove(loc);
		switch (blockColor)
		{
		case BlockColor.White:
			npcTextBlocks.Remove(loc);
			break;
		case BlockColor.Purple:
			enemyTypes.Remove(loc);
			brains.Remove(loc);
			break;
		case BlockColor.Pink:
			powerups.Remove(loc);
			break;
		}
		if (shouldSave)
		{
			Save(false);
		}
		return result;
	}

	public bool MoveItem(GridLocation fromLoc, GridLocation toLoc, bool shouldSave = true)
	{
		BlockColor blockColor = coverBoard.blockColors[fromLoc.c, fromLoc.r];
		if (blockColor == BlockColor.Blank)
		{
			return false;
		}
		coverBoard.blockColors[toLoc.c, toLoc.r] = blockColor;
		coverBoard.blockColors[fromLoc.c, fromLoc.r] = BlockColor.Blank;
		BloxelBoard value = null;
		BloxelAnimation value2 = null;
		BloxelCharacter value3 = null;
		if (boards.TryGetValue(fromLoc, out value))
		{
			boards.Remove(fromLoc);
			boards[toLoc] = value;
		}
		else if (animations.TryGetValue(fromLoc, out value2))
		{
			animations.Remove(fromLoc);
			animations[toLoc] = value2;
		}
		else if (characters.TryGetValue(fromLoc, out value3))
		{
			characters.Remove(fromLoc);
			characters[toLoc] = value3;
		}
		switch (blockColor)
		{
		case BlockColor.Purple:
		{
			EnemyType value5 = EnemyType.Patrol;
			if (enemyTypes.TryGetValue(fromLoc, out value5))
			{
				enemyTypes.Remove(fromLoc);
				enemyTypes[toLoc] = value5;
			}
			BloxelBrain value6 = null;
			if (brains.TryGetValue(fromLoc, out value6))
			{
				brains.Remove(fromLoc);
				brains[toLoc] = value6;
			}
			break;
		}
		case BlockColor.Pink:
		{
			PowerUpType value7 = PowerUpType.Bomb;
			if (powerups.TryGetValue(fromLoc, out value7))
			{
				powerups.Remove(fromLoc);
				powerups[toLoc] = value7;
			}
			break;
		}
		case BlockColor.White:
		{
			string value4 = null;
			if (npcTextBlocks.TryGetValue(fromLoc, out value4))
			{
				npcTextBlocks.Remove(fromLoc);
				npcTextBlocks[toLoc] = value4;
			}
			break;
		}
		}
		if (shouldSave)
		{
			Save(false);
		}
		return true;
	}

	public int TotalCoins()
	{
		int num = 0;
		for (int i = 0; i < coverBoard.blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < coverBoard.blockColors.GetLength(1); j++)
			{
				if (coverBoard.blockColors[i, j] == BlockColor.Yellow)
				{
					num++;
				}
			}
		}
		Dictionary<GridLocation, BloxelBrain>.Enumerator enumerator = brains.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelBrain value = enumerator.Current.Value;
			num += value.coinBonusQuantum;
		}
		return num;
	}

	public int TotalEnemies()
	{
		int num = 0;
		for (int i = 0; i < coverBoard.blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < coverBoard.blockColors.GetLength(1); j++)
			{
				if (coverBoard.blockColors[i, j] == BlockColor.Purple)
				{
					num++;
				}
			}
		}
		return num;
	}

	public int TotalDestructible()
	{
		int num = 0;
		for (int i = 0; i < coverBoard.blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < coverBoard.blockColors.GetLength(1); j++)
			{
				if (coverBoard.blockColors[i, j] == BlockColor.Orange)
				{
					num++;
				}
			}
		}
		return num;
	}

	public Dictionary<string, BloxelAnimation> UniqueAnimations()
	{
		Dictionary<string, BloxelAnimation> dictionary = new Dictionary<string, BloxelAnimation>();
		foreach (BloxelAnimation value in animations.Values)
		{
			if (!dictionary.ContainsKey(value.ID()))
			{
				dictionary.Add(value.ID(), value);
			}
		}
		return dictionary;
	}

	public Dictionary<string, BloxelBoard> UniqueBoards(bool includeAll = true)
	{
		Dictionary<string, BloxelBoard> dictionary = new Dictionary<string, BloxelBoard>();
		if (includeAll && !dictionary.ContainsKey(coverBoard.ID()))
		{
			dictionary.Add(coverBoard.ID(), coverBoard);
		}
		foreach (BloxelBoard value in boards.Values)
		{
			if (!dictionary.ContainsKey(value.ID()))
			{
				dictionary.Add(value.ID(), value);
			}
		}
		if (includeAll)
		{
			Dictionary<string, BloxelAnimation> dictionary2 = UniqueAnimations();
			foreach (BloxelAnimation value2 in dictionary2.Values)
			{
				for (int i = 0; i < value2.boards.Count; i++)
				{
					if (!dictionary.ContainsKey(value2.boards[i].ID()))
					{
						dictionary.Add(value2.boards[i].ID(), value2.boards[i]);
					}
				}
			}
		}
		foreach (BloxelBoard value3 in backgroundTiles.Values)
		{
			if (!dictionary.ContainsKey(value3.ID()))
			{
				dictionary.Add(value3.ID(), value3);
			}
		}
		return dictionary;
	}

	public override int GetBloxelCount()
	{
		int num = 0;
		Dictionary<string, BloxelBoard> dictionary = UniqueBoards();
		Dictionary<string, BloxelBoard>.Enumerator enumerator = dictionary.GetEnumerator();
		while (enumerator.MoveNext())
		{
			num += enumerator.Current.Value.GetBloxelCount();
		}
		return num;
	}

	public List<string> GetAssociatedBoardPaths()
	{
		List<string> list = new List<string>();
		if (!list.Contains(coverBoard.projectPath))
		{
			coverBoard.CheckAndSetMeAsOwner();
			list.Add(coverBoard.projectPath);
		}
		foreach (BloxelBoard value in boards.Values)
		{
			if (!list.Contains(value.projectPath))
			{
				value.CheckAndSetMeAsOwner();
				list.Add(value.projectPath);
			}
		}
		foreach (BloxelAnimation value2 in animations.Values)
		{
			for (int i = 0; i < value2.boards.Count; i++)
			{
				if (!list.Contains(value2.boards[i].projectPath))
				{
					value2.boards[i].CheckAndSetMeAsOwner();
					list.Add(value2.boards[i].projectPath);
				}
			}
		}
		foreach (BloxelBoard value3 in backgroundTiles.Values)
		{
			if (!list.Contains(value3.projectPath))
			{
				value3.CheckAndSetMeAsOwner();
				list.Add(value3.projectPath);
			}
		}
		return list;
	}

	public List<string> GetAssociatedAnimationPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelAnimation value in animations.Values)
		{
			if (!list.Contains(value.projectPath))
			{
				value.CheckAndSetMeAsOwner();
				list.Add(value.projectPath);
			}
		}
		return list;
	}

	public List<string> GetAssociatedBrainPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelBrain value in brains.Values)
		{
			if (!list.Contains(value.projectPath))
			{
				value.CheckAndSetMeAsOwner();
				list.Add(value.projectPath);
			}
		}
		return list;
	}

	public void SetDetailTextureForSprite(Sprite sprite, Color32 clearColor, bool applyTextureChanges = false)
	{
		Texture2D texture = sprite.texture;
		int num = (int)sprite.rect.x;
		int num2 = (int)sprite.rect.y;
		Color32[] colors = AssetManager.ColorBuffer32;
		BlockColor[,] blockColors = coverBoard.blockColors;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				GridLocation gridLocation = new GridLocation(j, i);
				int x = num + j * 13;
				int y = num2 + i * 13;
				BloxelBoard value = null;
				if (backgroundTiles.TryGetValue(gridLocation, out value))
				{
					value.GetDetailColorsNonAlloc(ref colors);
				}
				else
				{
					for (int k = 0; k < 169; k++)
					{
						colors[k] = clearColor;
					}
				}
				BlockColor blockColor = blockColors[j, i];
				if (blockColor == BlockColor.Blank)
				{
					texture.SetPixels32(x, y, 13, 13, colors);
					continue;
				}
				BloxelProject projectModelAtLocation = GetProjectModelAtLocation(gridLocation);
				if (projectModelAtLocation == null)
				{
					Color32 color = AssetManager.UnityLookupColorsForBlockColors[(uint)blockColor];
					for (int l = 0; l < 169; l++)
					{
						colors[l] = color;
					}
					texture.SetPixels32(x, y, 13, 13, colors);
				}
				else
				{
					projectModelAtLocation.coverBoard.GetDetailColorsNonAlloc(ref colors, false);
					texture.SetPixels32(x, y, 13, 13, colors);
				}
			}
		}
		if (applyTextureChanges)
		{
			texture.Apply();
		}
	}
}
