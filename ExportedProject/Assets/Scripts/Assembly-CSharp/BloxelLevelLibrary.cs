using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class BloxelLevelLibrary : BloxelLibraryWindow
{
	public delegate void HandleLevelChange();

	public static BloxelLevelLibrary instance;

	private UnityEngine.Object savedLevelPrefab;

	public SavedLevel currentSavedLevel;

	private BloxelLevel _currentLevel;

	public List<SavedLevel> savedLevels = new List<SavedLevel>();

	public BloxelLevel currentLevel
	{
		get
		{
			return _currentLevel;
		}
		set
		{
			_currentLevel = value;
			if (this.OnLevelChange != null)
			{
				this.OnLevelChange();
			}
		}
	}

	public event HandleLevelChange OnLevelChange;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		savedLevelPrefab = Resources.Load("Prefabs/SavedLevel");
		PopulateLibrary();
	}

	private new void Start()
	{
	}

	private new void DestroyLibrary()
	{
		base.DestroyLibrary();
		savedLevels.Clear();
	}

	public void RePopulate()
	{
		DestroyLibrary();
		PopulateLibrary();
	}

	private void PopulateLibrary()
	{
		foreach (BloxelLevel value in AssetManager.instance.levelPool.Values)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(savedLevelPrefab) as GameObject;
			SavedLevel component = gameObject.GetComponent<SavedLevel>();
			savedLevels.Add(component);
			component.project = value;
			ReParentLibraryItem(gameObject, false);
		}
	}

	public override void UpdateScrollerContents()
	{
		BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Level);
	}

	public new void AddButtonPressed()
	{
		base.AddButtonPressed();
		CreateEmptyProject();
	}

	public BloxelLevel CreateProjectFromBoard(BloxelBoard b)
	{
		BloxelLevel bloxelLevel = new BloxelLevel(b);
		GameObject gameObject = UnityEngine.Object.Instantiate(savedLevelPrefab) as GameObject;
		SavedLevel component = gameObject.GetComponent<SavedLevel>();
		savedLevels.Add(component);
		component.project = bloxelLevel;
		component.isActive = true;
		ReParentLibraryItem(gameObject);
		currentLevel = bloxelLevel;
		currentLevel.Save();
		return bloxelLevel;
	}

	public BloxelLevel CreateEmptyProject()
	{
		return CreateProjectFromBoard(new BloxelBoard());
	}

	public void SaveCurrentProject()
	{
		currentLevel.Save();
	}

	public void DeleteCurrentProject()
	{
		if (currentLevel.Delete())
		{
			RePopulate();
		}
	}
}
