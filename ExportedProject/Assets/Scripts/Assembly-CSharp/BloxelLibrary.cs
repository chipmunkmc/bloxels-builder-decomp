using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BloxelLibrary : UIMoveable
{
	public delegate void HandleTabChange(int tabID);

	public static BloxelLibrary instance;

	public Vector2 openPosition = new Vector2(0f, 0f);

	public Canvas libraryCanvas;

	public RectTransform scrollRectRectTransform;

	public ScrollRect scrollRect;

	public UITabGroup uiTabGroup;

	public int currentTabID;

	public UITabWindow[] tabWindows;

	public UILibraryToggle libraryToggle;

	public Sprite editorToggleSprite;

	public Sprite iWallToggleSprite;

	public SavedProject currentSavedProject;

	public bool initComplete;

	public event HandleTabChange OnTabChange;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(libraryCanvas.gameObject);
		}
		else
		{
			UnityEngine.Object.Destroy(libraryCanvas.gameObject);
		}
	}

	private new void Start()
	{
		if (!initComplete)
		{
			libraryCanvas.worldCamera = UIOverlayCanvas.instance.overlayCamera;
			BloxelBoardLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
			BloxelAnimationLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
			BloxelMegaBoardLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
			BloxelCharacterLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
			BloxelGameLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
			Init();
		}
	}

	private void OnDestroy()
	{
		BloxelBoardLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
		BloxelAnimationLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
		BloxelMegaBoardLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
		BloxelCharacterLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
		BloxelGameLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
	}

	private void HandleOnProjectSelect(SavedProject item)
	{
		currentSavedProject = item;
		if (AppStateManager.instance.currentScene == SceneName.PixelEditor_iPad && (PixelEditorController.instance.mode != CanvasMode.LevelEditor || (GameBuilderCanvas.instance.currentMode != GameBuilderMode.LevelBackground && GameBuilderCanvas.instance.currentMode != GameBuilderMode.PaintArt && GameBuilderCanvas.instance.autoCloseLibrary)))
		{
			libraryToggle.Hide();
		}
	}

	private void Init()
	{
		uiTabGroup.SwitchTab(currentTabID);
		UITabWindow[] array = tabWindows;
		foreach (UITabWindow uITabWindow in array)
		{
			if (uITabWindow.id != currentTabID)
			{
				uITabWindow.Disable();
			}
			else
			{
				uITabWindow.Activate();
			}
		}
		initComplete = true;
	}

	public void ReInitAllLibraries()
	{
		BloxelBoardLibrary.instance.PopulateLibrary();
		BloxelAnimationLibrary.instance.PopulateLibrary();
		BloxelCharacterLibrary.instance.PopulateLibrary();
		BloxelMegaBoardLibrary.instance.PopulateLibrary();
		BloxelGameLibrary.instance.PopulateLibrary();
	}

	public void ScrollToTop()
	{
	}

	public void ScrollToBottom()
	{
	}

	public void SwitchTabWindows(int _id)
	{
		currentTabID = _id;
		SoundManager.PlayOneShot(SoundManager.instance.sweepB);
		uiTabGroup.SwitchTab(currentTabID);
		UITabWindow[] array = tabWindows;
		foreach (UITabWindow uITabWindow in array)
		{
			if (uITabWindow.id != currentTabID)
			{
				uITabWindow.Disable();
			}
			else
			{
				uITabWindow.Activate();
			}
		}
		if (this.OnTabChange != null)
		{
			this.OnTabChange(currentTabID);
		}
	}

	public void SetPullTabSpriteForInfinityWall()
	{
		libraryToggle.GetComponent<Image>().sprite = iWallToggleSprite;
	}

	public void SetPullTabSpriteForEditor()
	{
		libraryToggle.GetComponent<Image>().sprite = editorToggleSprite;
	}

	public new Tweener Show()
	{
		return base.Show().OnStart(delegate
		{
			isVisible = false;
		});
	}
}
