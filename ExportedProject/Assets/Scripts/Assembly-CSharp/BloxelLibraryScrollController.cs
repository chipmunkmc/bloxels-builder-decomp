using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

public class BloxelLibraryScrollController : MonoBehaviour, IEnhancedScrollerDelegate
{
	public static BloxelLibraryScrollController instance;

	public EnhancedScroller scroller;

	public EnhancedScrollerCellView boardViewPrefab;

	public EnhancedScrollerCellView animationViewPrefab;

	public EnhancedScrollerCellView characterViewPrefab;

	public EnhancedScrollerCellView megaBoardViewPrefab;

	public EnhancedScrollerCellView gameViewPrefab;

	public float gameCellSize;

	public float defaultCellSize;

	private ProjectType currentDataType;

	private List<BloxelBoard> boardData = new List<BloxelBoard>(64);

	private List<BloxelAnimation> animationData = new List<BloxelAnimation>(32);

	private List<BloxelCharacter> characterData = new List<BloxelCharacter>(16);

	private List<BloxelMegaBoard> megaBoardData = new List<BloxelMegaBoard>(16);

	private List<BloxelGame> gameData = new List<BloxelGame>(16);

	public List<BloxelBoard> unfilteredBoardData = new List<BloxelBoard>(64);

	public List<BloxelAnimation> unfilteredAnimationData = new List<BloxelAnimation>(32);

	public List<BloxelCharacter> unfilteredCharacterData = new List<BloxelCharacter>(16);

	public List<BloxelMegaBoard> unfilteredMegaBoardData = new List<BloxelMegaBoard>(16);

	public List<BloxelGame> unfilteredGameData = new List<BloxelGame>(16);

	public List<BloxelBoard> filteredBoardData = new List<BloxelBoard>(64);

	public List<BloxelAnimation> filteredAnimationData = new List<BloxelAnimation>(32);

	public List<BloxelCharacter> filteredCharacterData = new List<BloxelCharacter>(16);

	public List<BloxelMegaBoard> filteredMegaBoardData = new List<BloxelMegaBoard>(16);

	public List<BloxelGame> filteredGameData = new List<BloxelGame>(16);

	public static Texture2D coverTextureAtlas;

	public static CoverSprite[] coverSprites = new CoverSprite[9];

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		InitReusableTextures();
	}

	private void Start()
	{
		scroller.Delegate = this;
		scroller.ScrollRect.content.anchoredPosition = new Vector2(scroller.ScrollRect.content.rect.center.x, 0f);
	}

	private void InitReusableTextures()
	{
		Color clear = Color.clear;
		coverTextureAtlas = new Texture2D(512, 512, TextureFormat.ARGB32, false);
		coverTextureAtlas.filterMode = FilterMode.Point;
		coverTextureAtlas.wrapMode = TextureWrapMode.Clamp;
		for (int i = 0; i < 4; i++)
		{
			for (int j = 0; j < 512; j++)
			{
				coverTextureAtlas.SetPixel(i * 170, j, clear);
			}
		}
		for (int k = 0; k < 4; k++)
		{
			for (int l = 0; l < 512; l++)
			{
				coverTextureAtlas.SetPixel(l, k * 170, clear);
			}
		}
		int m = 0;
		int num = 0;
		for (; m < 3; m++)
		{
			int num2 = 0;
			while (num2 < 3)
			{
				Sprite sprite = Sprite.Create(coverTextureAtlas, new Rect((float)num2 * 169f + (float)(num2 + 1), (float)m * 169f + (float)(m + 1), 169f, 169f), new Vector2(0.5f, 0.5f));
				coverSprites[num] = new CoverSprite(num, sprite);
				num2++;
				num++;
			}
		}
		coverTextureAtlas.Apply();
	}

	public CoverSprite GetUnusedCoverSprite()
	{
		for (int i = 0; i < 9; i++)
		{
			CoverSprite coverSprite = coverSprites[i];
			if (!coverSprite.inUse)
			{
				coverSprite.inUse = true;
				return coverSprite;
			}
		}
		return null;
	}

	public void DisablePreviousSelectedOutline()
	{
		BloxelProject bloxelProject = null;
		switch (currentDataType)
		{
		default:
			return;
		case ProjectType.Board:
			bloxelProject = BloxelBoardLibrary.instance.currentSavedBoard;
			break;
		case ProjectType.Animation:
			bloxelProject = BloxelAnimationLibrary.instance.currentAnimation;
			break;
		case ProjectType.Character:
			bloxelProject = BloxelCharacterLibrary.instance.currentCharacter;
			break;
		case ProjectType.MegaBoard:
			bloxelProject = BloxelMegaBoardLibrary.instance.currentMega;
			break;
		case ProjectType.Game:
			bloxelProject = BloxelGameLibrary.instance.currentSavedGame;
			break;
		case ProjectType.Level:
			return;
		}
		if (bloxelProject == null)
		{
			return;
		}
		for (int i = 0; i < scroller.activeCellViews.Count; i++)
		{
			LibraryScrollElement libraryScrollElement = scroller.activeCellViews[i] as LibraryScrollElement;
			for (int j = 0; j < libraryScrollElement.savedProjectArray.Length; j++)
			{
				SavedProject savedProject = libraryScrollElement.savedProjectArray[j];
				if (savedProject == null)
				{
					break;
				}
				if (!savedProject.outline.enabled || savedProject.dataModel == bloxelProject)
				{
					continue;
				}
				savedProject.outline.enabled = false;
				savedProject.isActive = false;
				if (savedProject is SavedAnimation)
				{
					SavedAnimation savedAnimation = savedProject as SavedAnimation;
					if (savedAnimation.cover.shouldAnimate)
					{
						savedAnimation.cover.StopAnimating();
					}
				}
				return;
			}
		}
	}

	public void SetCurrentDataForProjectType(ProjectType type)
	{
		currentDataType = type;
		int num = 0;
		switch (currentDataType)
		{
		case ProjectType.Board:
			LibraryScrollElement.currentCols = LibraryScrollElement.boardCols;
			boardData = ((!UITagSearch.instance.hasBoardFilter) ? unfilteredBoardData : filteredBoardData);
			break;
		case ProjectType.Animation:
			LibraryScrollElement.currentCols = LibraryScrollElement.animationCols;
			animationData = ((!UITagSearch.instance.hasAnimationFilter) ? unfilteredAnimationData : filteredAnimationData);
			break;
		case ProjectType.Character:
			LibraryScrollElement.currentCols = LibraryScrollElement.characterCols;
			characterData = ((!UITagSearch.instance.hasCharacterFilter) ? unfilteredCharacterData : filteredCharacterData);
			break;
		case ProjectType.MegaBoard:
			LibraryScrollElement.currentCols = LibraryScrollElement.megaBoardCols;
			megaBoardData = ((!UITagSearch.instance.hasMegaBoardFilter) ? unfilteredMegaBoardData : filteredMegaBoardData);
			break;
		case ProjectType.Game:
			LibraryScrollElement.currentCols = LibraryScrollElement.gameCols;
			gameData = ((!UITagSearch.instance.hasGameFilter) ? unfilteredGameData : filteredGameData);
			num = 70;
			break;
		default:
			LibraryScrollElement.currentCols = LibraryScrollElement.boardCols;
			break;
		}
		float width = scroller.ScrollRect.content.rect.width;
		float spacing = scroller.spacing;
		LibraryScrollElement.viewHeight = (width - spacing - spacing / 2f * (float)(LibraryScrollElement.currentCols - 1)) / (float)LibraryScrollElement.currentCols - spacing / 2f + (float)num;
		scroller.ClearAll();
		scroller.ReloadData();
	}

	public int GetNumberOfCells(EnhancedScroller scroller)
	{
		switch (currentDataType)
		{
		case ProjectType.Board:
			return Mathf.CeilToInt((float)boardData.Count / (float)LibraryScrollElement.boardCols);
		case ProjectType.Animation:
			return Mathf.CeilToInt((float)animationData.Count / (float)LibraryScrollElement.animationCols);
		case ProjectType.Character:
			return Mathf.CeilToInt((float)characterData.Count / (float)LibraryScrollElement.characterCols);
		case ProjectType.MegaBoard:
			return Mathf.CeilToInt((float)megaBoardData.Count / (float)LibraryScrollElement.megaBoardCols);
		case ProjectType.Game:
			return Mathf.CeilToInt((float)gameData.Count / (float)LibraryScrollElement.gameCols);
		default:
			return 0;
		}
	}

	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		return LibraryScrollElement.viewHeight;
	}

	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
	{
		BloxelProject bloxelProject = null;
		BloxelProject bloxelProject2 = null;
		LibraryScrollElement libraryScrollElement = null;
		switch (currentDataType)
		{
		case ProjectType.Board:
			bloxelProject = boardData[dataIndex * 2];
			if (dataIndex * 2 + 1 < boardData.Count)
			{
				bloxelProject2 = boardData[dataIndex * 2 + 1];
			}
			libraryScrollElement = scroller.GetCellView(boardViewPrefab) as LibraryScrollElement;
			break;
		case ProjectType.Animation:
			bloxelProject = animationData[dataIndex * 2];
			if (dataIndex * 2 + 1 < animationData.Count)
			{
				bloxelProject2 = animationData[dataIndex * 2 + 1];
			}
			libraryScrollElement = scroller.GetCellView(animationViewPrefab) as LibraryScrollElement;
			break;
		case ProjectType.Character:
			bloxelProject = characterData[dataIndex * 2];
			if (dataIndex * 2 + 1 < characterData.Count)
			{
				bloxelProject2 = characterData[dataIndex * 2 + 1];
			}
			libraryScrollElement = scroller.GetCellView(characterViewPrefab) as LibraryScrollElement;
			break;
		case ProjectType.MegaBoard:
			bloxelProject = megaBoardData[dataIndex];
			libraryScrollElement = scroller.GetCellView(megaBoardViewPrefab) as LibraryScrollElement;
			break;
		case ProjectType.Game:
			bloxelProject = gameData[dataIndex];
			libraryScrollElement = scroller.GetCellView(gameViewPrefab) as LibraryScrollElement;
			break;
		}
		if (bloxelProject == null || libraryScrollElement == null)
		{
			return null;
		}
		if (libraryScrollElement.cols == 1)
		{
			libraryScrollElement.SetData(bloxelProject);
		}
		else
		{
			libraryScrollElement.SetData(bloxelProject, bloxelProject2);
		}
		return libraryScrollElement;
	}

	public SavedProject GetFirstItem()
	{
		if (scroller.ScrollRect.content.childCount <= 2)
		{
			return null;
		}
		Transform child = scroller.ScrollRect.content.GetChild(1);
		SavedProject savedProject = child.GetComponent<LibraryScrollElement>().savedProjectArray[0];
		if (savedProject == null)
		{
			return null;
		}
		return savedProject;
	}

	public T GetSelectedProject<T>() where T : SavedProject
	{
		SavedProject savedProject = null;
		bool flag = false;
		for (int i = 0; i < scroller.activeCellViews.Count; i++)
		{
			if (flag)
			{
				break;
			}
			LibraryScrollElement libraryScrollElement = scroller.activeCellViews[i] as LibraryScrollElement;
			for (int j = 0; j < libraryScrollElement.savedProjectArray.Length; j++)
			{
				SavedProject savedProject2 = libraryScrollElement.savedProjectArray[j];
				if (savedProject2.isActive)
				{
					savedProject = savedProject2;
					flag = true;
					break;
				}
			}
		}
		if (!flag)
		{
			return (T)null;
		}
		if (savedProject is T)
		{
			return savedProject as T;
		}
		return (T)null;
	}

	public SavedProject AddItem(BloxelProject project)
	{
		ClearAllFilters();
		if (project is BloxelBoard)
		{
			BloxelBoard item = project as BloxelBoard;
			boardData.Insert(0, item);
			if (boardData != unfilteredBoardData)
			{
				unfilteredBoardData.Insert(0, item);
			}
		}
		else if (project is BloxelAnimation)
		{
			BloxelAnimation item2 = project as BloxelAnimation;
			animationData.Insert(0, item2);
			if (animationData != unfilteredAnimationData)
			{
				unfilteredAnimationData.Insert(0, item2);
			}
		}
		else if (project is BloxelCharacter)
		{
			BloxelCharacter item3 = project as BloxelCharacter;
			characterData.Insert(0, item3);
			if (characterData != unfilteredCharacterData)
			{
				unfilteredCharacterData.Insert(0, item3);
			}
		}
		else if (project is BloxelMegaBoard)
		{
			BloxelMegaBoard item4 = project as BloxelMegaBoard;
			megaBoardData.Insert(0, item4);
			if (megaBoardData != unfilteredMegaBoardData)
			{
				unfilteredMegaBoardData.Insert(0, item4);
			}
		}
		else if (project is BloxelGame)
		{
			BloxelGame item5 = project as BloxelGame;
			gameData.Insert(0, item5);
			if (gameData != unfilteredGameData)
			{
				unfilteredGameData.Insert(0, item5);
			}
		}
		if (currentDataType == project.type)
		{
			scroller.ClearAll();
			scroller.ReloadData();
			return scroller.ScrollRect.content.GetChild(1).GetComponent<LibraryScrollElement>().savedProjectArray[0];
		}
		return null;
	}

	public void RemoveItem(BloxelProject project)
	{
		if (project is BloxelBoard)
		{
			BloxelBoard item = project as BloxelBoard;
			boardData.Remove(item);
			if (boardData != unfilteredBoardData)
			{
				unfilteredBoardData.Remove(item);
			}
		}
		else if (project is BloxelAnimation)
		{
			BloxelAnimation item2 = project as BloxelAnimation;
			animationData.Remove(item2);
			if (animationData != unfilteredAnimationData)
			{
				unfilteredAnimationData.Remove(item2);
			}
		}
		else if (project is BloxelCharacter)
		{
			BloxelCharacter item3 = project as BloxelCharacter;
			characterData.Remove(item3);
			if (characterData != unfilteredCharacterData)
			{
				unfilteredCharacterData.Remove(item3);
			}
		}
		else if (project is BloxelMegaBoard)
		{
			BloxelMegaBoard item4 = project as BloxelMegaBoard;
			megaBoardData.Remove(item4);
			if (megaBoardData != unfilteredMegaBoardData)
			{
				unfilteredMegaBoardData.Remove(item4);
			}
		}
		else if (project is BloxelGame)
		{
			BloxelGame item5 = project as BloxelGame;
			gameData.Remove(item5);
			if (gameData != unfilteredGameData)
			{
				unfilteredGameData.Remove(item5);
			}
		}
		if (currentDataType == project.type)
		{
			scroller.ClearAll();
			scroller.ReloadData();
		}
	}

	public void ClearAllFilters()
	{
		UITagSearch.instance.activeTags.Clear();
		UITagSearch.instance.hasFilter = false;
		filteredBoardData.Clear();
		filteredAnimationData.Clear();
		filteredCharacterData.Clear();
		filteredMegaBoardData.Clear();
		filteredGameData.Clear();
		boardData = unfilteredBoardData;
		animationData = unfilteredAnimationData;
		characterData = unfilteredCharacterData;
		megaBoardData = unfilteredMegaBoardData;
		gameData = unfilteredGameData;
	}
}
