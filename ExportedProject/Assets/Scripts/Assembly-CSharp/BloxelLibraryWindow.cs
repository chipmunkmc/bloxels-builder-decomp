using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public abstract class BloxelLibraryWindow : MonoBehaviour
{
	public delegate void HandleProjectSelect(SavedProject item);

	public delegate void HandleDoubleTap(SavedProject item);

	public delegate void HandleDelete(SavedProject item);

	public delegate void HandleBeginDrag(SavedProject item, bool isScrolling);

	public delegate void HandleEndDrag(SavedProject item, bool isScrolling);

	public delegate void HandleDrag(SavedProject item);

	public delegate void HandleAdd();

	public SavedProject currentSavedProject;

	public event HandleProjectSelect OnProjectSelect;

	public event HandleDoubleTap OnProjectDoubleTap;

	public event HandleDelete OnProjectDelete;

	public event HandleBeginDrag OnProjectDragStart;

	public event HandleEndDrag OnProjectDragEnd;

	public event HandleDrag OnProjectDragging;

	public event HandleAdd OnAddButtonPressed;

	public void Start()
	{
		Init();
	}

	public void OnEnable()
	{
	}

	private void OnDestroy()
	{
	}

	public void Init()
	{
	}

	public abstract void UpdateScrollerContents();

	public HashSet<string> StartIgnoringBoards()
	{
		HashSet<string> hashSet = new HashSet<string>();
		BloxelProject[] library = AssetManager.instance.GetLibrary();
		foreach (BloxelProject bloxelProject in library)
		{
			if (bloxelProject.GetBloxelCount() <= 0)
			{
				hashSet.Add(bloxelProject.ID());
			}
		}
		return hashSet;
	}

	public void RemoveItemEvent(SavedProject p)
	{
		p.OnDelete -= ItemDelete;
		p.OnSelect -= ItemSelect;
		p.OnDragEnd -= ItemDragEnd;
		p.OnDragging -= ItemDrag;
		p.OnDoubleTap -= ItemDoubleTap;
		p.OnDragStart -= ItemDragStart;
	}

	public void SetupItem(SavedProject p)
	{
		p.OnDelete += ItemDelete;
		p.OnSelect += ItemSelect;
		p.OnDragEnd += ItemDragEnd;
		p.OnDragging += ItemDrag;
		p.OnDoubleTap += ItemDoubleTap;
		p.OnDragStart += ItemDragStart;
		p.libraryWindow = this;
	}

	public void ItemDragEnd(SavedProject item, bool isScrolling)
	{
		if (this.OnProjectDragEnd != null)
		{
			this.OnProjectDragEnd(item, isScrolling);
		}
	}

	public void ItemDrag(SavedProject item)
	{
		if (this.OnProjectDragging != null)
		{
			this.OnProjectDragging(item);
		}
	}

	public void ItemDragStart(SavedProject item, bool isScrolling)
	{
		if (this.OnProjectDragStart != null)
		{
			this.OnProjectDragStart(item, isScrolling);
		}
	}

	public void ItemSelect(SavedProject item, bool sfx = true)
	{
		item.Select(sfx);
		if (this.OnProjectSelect != null)
		{
			this.OnProjectSelect(item);
		}
	}

	public void ItemDoubleTap(SavedProject item)
	{
		if (this.OnProjectDoubleTap != null)
		{
			this.OnProjectDoubleTap(item);
		}
	}

	public void ItemDelete(SavedProject item)
	{
		BloxelLibraryScrollController.instance.RemoveItem(item.dataModel);
		if (this.OnProjectDelete != null)
		{
			this.OnProjectDelete(item);
		}
	}

	public void AddButtonPressed()
	{
		if (this.OnAddButtonPressed != null)
		{
			this.OnAddButtonPressed();
		}
	}

	public void DestroyLibrary()
	{
	}

	public void ReParentLibraryItem(GameObject obj, bool isFirstSibling = true)
	{
	}
}
