using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using Newtonsoft.Json;
using UnityEngine;

public sealed class BloxelMegaBoard : BloxelProject
{
	public delegate void HandleOnUpdate();

	public static string filename = "mega.json";

	public static string rootDirectory = "MegaBoards";

	public static string subDirectory = "myMegaBoards";

	public static string defaultTitle = "My Background";

	public MegaBoardSize size;

	public Dictionary<GridLocation, BloxelBoard> boards;

	public event HandleOnUpdate OnUpdate;

	public BloxelMegaBoard(bool setBoardPath = true)
	{
		type = ProjectType.MegaBoard;
		boards = new Dictionary<GridLocation, BloxelBoard>();
		coverBoard = new BloxelBoard(setBoardPath);
		tags = new List<string>();
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelMegaBoard(MegaBoardSize mbSize)
	{
		type = ProjectType.MegaBoard;
		base.id = Guid.NewGuid();
		projectPath = MyProjectPath() + "/" + ID();
		size = mbSize;
		boards = new Dictionary<GridLocation, BloxelBoard>();
		coverBoard = new BloxelBoard();
		tags = new List<string>();
		tags.Add("mine");
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelMegaBoard(string s, DataSource source)
	{
		type = ProjectType.MegaBoard;
		if (source == DataSource.FilePath)
		{
			try
			{
				string jsonString = File.ReadAllText(s + "/" + filename);
				FromJSONString(jsonString, false);
			}
			catch (Exception)
			{
			}
		}
		else
		{
			FromJSONString(s, true);
		}
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelMegaBoard(JsonTextReader reader, bool fromServer)
	{
		type = ProjectType.MegaBoard;
		FromReader(reader, fromServer);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public BloxelMegaBoard(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		type = ProjectType.MegaBoard;
		FromReaderSlim(reader, projectPool);
		syncInfo = ProjectSyncInfo.Create(this);
	}

	public static string BaseStoragePath()
	{
		return Application.persistentDataPath + "/" + rootDirectory;
	}

	public static string MyProjectPath()
	{
		return AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory;
	}

	public void FromJSONString(string jsonString, bool fromServer)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader, fromServer);
	}

	public void FromReader(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		tags = new List<string>();
		boards = new Dictionary<GridLocation, BloxelBoard>();
		size = new MegaBoardSize(13, 13);
		while (reader.Read() && reader.TokenType != JsonToken.Null)
		{
			if (reader.Value == null)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				if (text == null || !(text == "boards"))
				{
					continue;
				}
				ParseBoards(reader, fromServer);
				projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
				break;
			}
			switch (text)
			{
			case "id":
				base.id = new Guid(reader.Value.ToString());
				break;
			case "title":
				title = reader.Value.ToString();
				break;
			case "tags":
				tags.Add(reader.Value.ToString());
				break;
			case "owner":
				owner = reader.Value.ToString();
				break;
			case "isDirty":
				isDirty = bool.Parse(reader.Value.ToString());
				break;
			case "builtWithVersion":
				SetBuildVersion(int.Parse(reader.Value.ToString()));
				break;
			}
		}
	}

	private void FromReaderSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		tags = new List<string>(1);
		boards = new Dictionary<GridLocation, BloxelBoard>();
		size = new MegaBoardSize(13, 13);
		while (reader.Read() && reader.TokenType != JsonToken.Null)
		{
			if (reader.Value == null)
			{
				continue;
			}
			if (reader.TokenType == JsonToken.PropertyName)
			{
				text = reader.Value.ToString();
				if (text == null || !(text == "boards"))
				{
					continue;
				}
				ParseBoardsSlim(reader, projectPool);
				projectPath = AssetManager.baseStoragePath + rootDirectory + "/" + subDirectory + "/" + ID();
				break;
			}
			switch (text)
			{
			case "id":
				base.id = new Guid(reader.Value.ToString());
				break;
			case "title":
				title = reader.Value.ToString();
				break;
			case "owner":
				owner = reader.Value.ToString();
				break;
			case "isDirty":
				isDirty = bool.Parse(reader.Value.ToString());
				break;
			case "builtWithVersion":
				SetBuildVersion(int.Parse(reader.Value.ToString()));
				break;
			}
		}
	}

	private void ParseBoards(JsonTextReader reader, bool fromServer)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		if (fromServer)
		{
			while (reader.Read())
			{
				if (reader.Value != null)
				{
					if (reader.TokenType == JsonToken.PropertyName)
					{
						text = reader.Value.ToString();
						if (text == "board")
						{
							boards[new GridLocation(col, row)] = new BloxelBoard(reader);
						}
						continue;
					}
					switch (text)
					{
					case "c":
						col = int.Parse(reader.Value.ToString());
						break;
					case "r":
						row = int.Parse(reader.Value.ToString());
						break;
					}
				}
				else if (reader.TokenType == JsonToken.EndArray)
				{
					if (boards.Count > 0)
					{
						coverBoard = boards.First().Value;
					}
					break;
				}
			}
			return;
		}
		int num = 0;
		string empty = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "id":
				{
					string key = reader.Value.ToString();
					BloxelBoard value = null;
					if (AssetManager.instance.boardPool.TryGetValue(key, out value))
					{
						boards[new GridLocation(col, row)] = value;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				if (boards.Count > 0)
				{
					coverBoard = boards.First().Value;
				}
				break;
			}
		}
	}

	private void ParseBoardsSlim(JsonTextReader reader, Dictionary<string, BloxelProject> projectPool)
	{
		string text = string.Empty;
		int col = 0;
		int row = 0;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					continue;
				}
				switch (text)
				{
				case "c":
					col = int.Parse(reader.Value.ToString());
					break;
				case "r":
					row = int.Parse(reader.Value.ToString());
					break;
				case "board":
				{
					string key = reader.Value.ToString();
					BloxelProject value = null;
					if (projectPool.TryGetValue(key, out value) && value.type == ProjectType.Board)
					{
						boards[new GridLocation(col, row)] = value as BloxelBoard;
					}
					break;
				}
				}
			}
			else if (reader.TokenType == JsonToken.EndArray)
			{
				break;
			}
		}
	}

	public string ToJSON()
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject jSONObject2 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject3 = new JSONObject(JSONObject.Type.ARRAY);
		JSONObject jSONObject4 = new JSONObject(JSONObject.Type.ARRAY);
		foreach (KeyValuePair<GridLocation, BloxelBoard> board in boards)
		{
			JSONObject jSONObject5 = new JSONObject();
			jSONObject5.AddField("c", board.Key.c);
			jSONObject5.AddField("r", board.Key.r);
			jSONObject5.AddField("id", board.Value.ID());
			jSONObject2.Add(jSONObject5);
		}
		for (int i = 0; i < tags.Count; i++)
		{
			jSONObject4.Add(tags[i]);
		}
		jSONObject.AddField("tags", jSONObject4);
		jSONObject3.Add(size.c);
		jSONObject3.Add(size.r);
		jSONObject.AddField("id", base.id.ToString());
		jSONObject.AddField("title", title);
		jSONObject.AddField("owner", owner);
		jSONObject.AddField("boards", jSONObject2);
		jSONObject.AddField("size", jSONObject3);
		jSONObject.AddField("isDirty", isDirty);
		jSONObject.AddField("builtWithVersion", builtWithVersion);
		return jSONObject.ToString();
	}

	public override bool Save(bool deepSave = true, bool shouldQueue = true)
	{
		bool flag = false;
		if (shouldQueue)
		{
			if (deepSave)
			{
				Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator = boards.GetEnumerator();
				while (enumerator.MoveNext())
				{
					enumerator.Current.Value.Save(deepSave, shouldQueue);
				}
			}
			SaveManager.Instance.Enqueue(this);
			return false;
		}
		if (!Directory.Exists(projectPath))
		{
			Directory.CreateDirectory(projectPath);
		}
		string text = ToJSON();
		SetDirty(true);
		if (string.IsNullOrEmpty(text))
		{
			return false;
		}
		if (Directory.Exists(projectPath))
		{
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		else
		{
			Directory.CreateDirectory(projectPath);
			File.WriteAllText(projectPath + "/" + filename, text);
		}
		if (File.Exists(projectPath + "/" + filename))
		{
			return true;
		}
		return false;
	}

	public override void SetDirty(bool isDirty)
	{
		base.SetDirty(isDirty);
		if (isDirty)
		{
			CloudManager.Instance.EnqueueFileForSync(syncInfo);
		}
	}

	public bool Delete()
	{
		bool flag = false;
		if (Directory.Exists(projectPath))
		{
			Directory.Delete(projectPath, true);
		}
		flag = ((!Directory.Exists(projectPath)) ? true : false);
		CloudManager.Instance.RemoveFileFromQueue(syncInfo);
		return flag;
	}

	public static DirectoryInfo[] GetSavedProjects()
	{
		DirectoryInfo directoryInfo = null;
		string path = MyProjectPath();
		if (Directory.Exists(path))
		{
			directoryInfo = new DirectoryInfo(path);
		}
		else
		{
			Directory.CreateDirectory(path);
			directoryInfo = new DirectoryInfo(path);
		}
		return (from p in directoryInfo.GetDirectories()
			orderby p.LastWriteTime descending
			select p).ToArray();
	}

	public static BloxelMegaBoard GetSavedProjectById(string idString)
	{
		return new BloxelMegaBoard(MyProjectPath() + "/" + idString, DataSource.FilePath);
	}

	public BloxelBoard GetCoverBoard()
	{
		if (boards.Count > 0)
		{
			return boards.First().Value;
		}
		return null;
	}

	public void SetBoardAtLocation(GridLocation loc, BloxelBoard b)
	{
		bool flag = false;
		if (boards.Count < 1)
		{
			flag = true;
		}
		boards[loc] = b;
		Save(false);
		coverBoard = b;
		BlastUpdate();
	}

	public bool RemoveTileAtLocation(GridLocation loc)
	{
		if (!boards.ContainsKey(loc))
		{
			return false;
		}
		bool flag = false;
		if (boards[loc].ID() == coverBoard.ID())
		{
			flag = true;
		}
		boards.Remove(loc);
		if (flag && boards.Count > 0)
		{
			coverBoard = boards.First().Value;
		}
		else if (flag && boards.Count < 1)
		{
			coverBoard = new BloxelBoard();
		}
		Save(false);
		BlastUpdate();
		return true;
	}

	public new void AddTag(string tag)
	{
		base.AddTag(tag);
		Save();
	}

	public new void AddMultipleTags(List<string> tagsToAdd)
	{
		base.AddMultipleTags(tagsToAdd);
		Save();
	}

	public new void RemoveTag(string tag)
	{
		base.RemoveTag(tag);
		Save();
	}

	public void SetTitle(string _title, bool shouldsave = true)
	{
		title = _title;
		if (shouldsave)
		{
			Save(false);
		}
	}

	public void SetOwner(string _owner, bool shouldsave = true)
	{
		base.SetOwner(_owner);
		if (shouldsave)
		{
			Save(false);
		}
	}

	public new void CheckAndSetMeAsOwner()
	{
		base.CheckAndSetMeAsOwner();
		Save(false);
	}

	public void BlastUpdate()
	{
		if (this.OnUpdate != null)
		{
			this.OnUpdate();
		}
	}

	public Dictionary<string, BloxelBoard> UniqueBoards()
	{
		Dictionary<string, BloxelBoard> dictionary = new Dictionary<string, BloxelBoard>();
		Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator = boards.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelBoard value = enumerator.Current.Value;
			if (!dictionary.ContainsKey(value.ID()))
			{
				dictionary[value.ID()] = value;
			}
		}
		return dictionary;
	}

	public override int GetBloxelCount()
	{
		int num = 0;
		Dictionary<string, BloxelBoard> dictionary = UniqueBoards();
		foreach (BloxelBoard value in dictionary.Values)
		{
			num += value.GetBloxelCount();
		}
		return num;
	}

	public List<string> GetAssociatedBoardPaths()
	{
		List<string> list = new List<string>();
		foreach (BloxelBoard value in boards.Values)
		{
			if (!list.Contains(value.projectPath))
			{
				value.CheckAndSetMeAsOwner();
				list.Add(value.projectPath);
			}
		}
		return list;
	}

	public override ProjectBundle Compress()
	{
		List<string> associatedBoardPaths = GetAssociatedBoardPaths();
		string[] array = new string[associatedBoardPaths.Count];
		string[] array2 = new string[associatedBoardPaths.Count];
		int num = 0;
		int num2 = 0;
		while (num2 < associatedBoardPaths.Count)
		{
			array[num] = associatedBoardPaths[num2] + "/" + BloxelBoard.filename;
			string[] array3 = associatedBoardPaths[num2].Split('/');
			array2[num] = array3[array3.Length - 1];
			num2++;
			num++;
		}
		return new ProjectBundle(Zipper.CreateBytes(array, array2), ToJSON());
	}

	public void SetCoverTexturePixels(Sprite sprite, Color clearColor)
	{
		Texture2D texture = sprite.texture;
		Color[] colors = AssetManager.instance.colorBuffer13x13;
		Color[] clearColors13x = AssetManager.instance.clearColors13x13;
		int num = (int)sprite.rect.x;
		int num2 = (int)sprite.rect.y;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				int x = num + j * 13;
				int y = num2 + i * 13;
				GridLocation key = new GridLocation(j, i);
				BloxelBoard value = null;
				if (boards.TryGetValue(key, out value))
				{
					value.GetColorsNonAlloc(ref colors);
					texture.SetPixels(x, y, 13, 13, colors);
				}
				else
				{
					texture.SetPixels(x, y, 13, 13, clearColors13x);
				}
			}
		}
		texture.Apply();
	}

	public Texture2D GetDetailTexture()
	{
		Texture2D texture2D = new Texture2D(169, 169, TextureFormat.ARGB32, false);
		texture2D.filterMode = FilterMode.Point;
		Color[] colors = AssetManager.instance.colorBuffer13x13;
		Color[] clearColors13x = AssetManager.instance.clearColors13x13;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				int x = j * 13;
				int y = i * 13;
				GridLocation key = new GridLocation(j, i);
				BloxelBoard value = null;
				if (boards.TryGetValue(key, out value))
				{
					value.GetColorsNonAlloc(ref colors);
					texture2D.SetPixels(x, y, 13, 13, colors);
				}
				else
				{
					texture2D.SetPixels(x, y, 13, 13, clearColors13x);
				}
			}
		}
		texture2D.Apply();
		return texture2D;
	}
}
