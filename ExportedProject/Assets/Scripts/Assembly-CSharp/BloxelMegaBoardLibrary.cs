using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BloxelMegaBoardLibrary : BloxelLibraryWindow
{
	public static BloxelMegaBoardLibrary instance;

	public Object savedMegaBoardPrefab;

	private bool initComplete;

	public BloxelMegaBoard currentMega { get; private set; }

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
	}

	private new void Start()
	{
		if (!initComplete)
		{
			PopulateLibrary();
			base.Start();
			base.OnProjectSelect += SetCurrentProject;
			initComplete = true;
		}
	}

	private void OnDestroy()
	{
		base.OnProjectSelect -= SetCurrentProject;
	}

	private void SetCurrentProject(SavedProject item)
	{
		BloxelMegaBoard bloxelMegaBoard = item.dataModel as BloxelMegaBoard;
		if (currentMega == null || currentMega != bloxelMegaBoard)
		{
			currentMega = bloxelMegaBoard;
			BloxelLibraryScrollController.instance.DisablePreviousSelectedOutline();
			SoundManager.PlayOneShot(SoundManager.instance.popA);
		}
	}

	private new void DestroyLibrary()
	{
		base.DestroyLibrary();
	}

	public void PopulateLibrary()
	{
		BloxelLibraryScrollController.instance.unfilteredMegaBoardData.Clear();
		Dictionary<string, BloxelMegaBoard>.ValueCollection.Enumerator enumerator = AssetManager.instance.megaBoardPool.Values.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelLibraryScrollController.instance.unfilteredMegaBoardData.Add(enumerator.Current);
		}
	}

	public override void UpdateScrollerContents()
	{
		BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.MegaBoard);
	}

	public new void AddButtonPressed()
	{
		base.AddButtonPressed();
		MegaBoardCanvas.instance.CreateNewBackground();
	}

	public BloxelMegaBoard AddProject(BloxelMegaBoard project)
	{
		if (!AssetManager.instance.megaBoardPool.ContainsKey(project.ID()))
		{
			BloxelLibraryScrollController.instance.AddItem(project);
			project.SetBuildVersion(AppStateManager.instance.internalVersion);
			project.Save();
			if (!AssetManager.instance.megaBoardPool.ContainsKey(project.ID()))
			{
				AssetManager.instance.megaBoardPool.Add(project.ID(), project);
			}
			StartCoroutine(DelayedItemSelect());
			return project;
		}
		return null;
	}

	private IEnumerator DelayedItemSelect()
	{
		yield return new WaitForEndOfFrame();
		ItemSelect(BloxelLibraryScrollController.instance.GetFirstItem());
	}

	public void Reset()
	{
		if (currentMega != null)
		{
			currentMega = null;
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.MegaBoard);
		}
	}
}
