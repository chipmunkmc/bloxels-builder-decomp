using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public sealed class BloxelNPCTile : BloxelTile
{
	public static string collider2DLayer = "npc";

	public static SpeakBubble speakBubble;

	public NPCIndicator npcIndicator;

	public GameObject particles;

	public bool isGameEnd;

	public static List<NPCIndicator> activeIndicators = new List<NPCIndicator>(8);

	public override void PrepareSpawn()
	{
		isGameEnd = false;
		base.PrepareSpawn();
	}

	private void OnEnable()
	{
		if (spawned)
		{
			StartCoroutine("CheckForPlayer");
		}
	}

	public override void PrepareDespawn()
	{
		DetachSpeechBubble();
		DetachIndicator();
		base.PrepareDespawn();
	}

	private void OnDisable()
	{
		StopCoroutine("CheckForPlayer");
	}

	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.npcPool.Push(this);
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(collider2DLayer);
		tileRenderer.colliderIsTrigger = false;
		tileRenderer.tileScale = Vector3.one;
	}

	private IEnumerator CheckForPlayer()
	{
		while (true)
		{
			if (!PixelPlayerController.instance.movementController.enabled || !GameHUD.instance)
			{
				yield return new WaitForSeconds(0.4f);
				continue;
			}
			if (Vector3.SqrMagnitude(selfTransform.position + new Vector3(6f, 0f, 0f) - PixelPlayerController.instance.transform.position) < 169f)
			{
				AttachIndicator();
			}
			else
			{
				DetachSpeechBubble();
				DetachIndicator();
			}
			yield return new WaitForSeconds(0.4f);
		}
	}

	protected override void PositionBloxelTile()
	{
		Vector3 position = new Vector3(tileInfo.worldLocation.x, tileInfo.worldLocation.y, 2f);
		base.transform.position = GameplayBuilder.instance.worldWrapper.TransformPoint(position);
		base.transform.localScale = Vector3.one;
	}

	public void AttachSpeechBubble()
	{
		if (npcIndicator == null)
		{
			return;
		}
		if (speakBubble != null)
		{
			if (!(speakBubble.tile != this))
			{
				return;
			}
			speakBubble.Despawn();
		}
		speakBubble = SpeakBubblePool.instance.Spawn();
		speakBubble.selfTransform.SetParent(selfTransform);
		speakBubble.selfTransform.localPosition = new Vector3(6f, 18f, -2f);
		speakBubble.selfTransform.localScale = new Vector3(0.2f, 0f, 1.1f);
		speakBubble.tile = this;
		DetachIndicator();
	}

	private void DetachSpeechBubble()
	{
		if (speakBubble != null && speakBubble.tile == this)
		{
			speakBubble.Despawn();
		}
	}

	private void AttachIndicator()
	{
		if ((!(speakBubble != null) || !(speakBubble.tile == this)) && !(npcIndicator != null))
		{
			WorldLocation worldLocation = new WorldLocation(tileInfo.bloxelLevel.id, tileInfo.locationInLevel, tileInfo.bloxelLevel.location);
			if (GameplayBuilder.instance.game.gameEndFlag == worldLocation)
			{
				isGameEnd = true;
			}
			if (isGameEnd)
			{
				StopCoroutine("CheckForPlayer");
				GameHUD.instance.SetControls_Normal();
				GameplayController.instance.ShowGameWin();
				SoundManager.PlayOneShot(SoundManager.instance.gameWin);
				DetachIndicator();
			}
			else
			{
				npcIndicator = NPCIndicatorPool.instance.Spawn();
				npcIndicator.selfTransform.SetParent(selfTransform, false);
				npcIndicator.selfTransform.localPosition = new Vector3(6f, 18f, 0f);
				npcIndicator.selfTransform.localScale = Vector3.one;
				npcIndicator.tile = this;
				npcIndicator.position = Vector2.zero;
				npcIndicator.rect.anchoredPosition = Vector2.zero;
				GameHUD.instance.SetControls_NPCInteraction();
				activeIndicators.Add(npcIndicator);
			}
		}
	}

	private void DetachIndicator()
	{
		if (!(npcIndicator == null))
		{
			npcIndicator.Despawn();
			activeIndicators.Remove(npcIndicator);
			npcIndicator = null;
			if (activeIndicators.Count == 0)
			{
				GameHUD.instance.SetControls_Normal();
			}
		}
	}

	private bool CheckPointSame()
	{
		Vector3 vector = new Vector3(selfTransform.localPosition.x + 6f, selfTransform.localPosition.y, GameplayController.instance.heroObject.transform.localPosition.z);
		return vector == GameplayController.instance.lastCheckpoint;
	}

	public void SetCheckpoint()
	{
		if (!CheckPointSame())
		{
			GameplayController.instance.lastCheckpoint = new Vector3(selfTransform.localPosition.x + 6f, selfTransform.localPosition.y, GameplayController.instance.heroObject.transform.localPosition.z);
			GameHUD.instance.enemiesDefeatedSinceCheckpoint = 0;
			GameHUD.instance.blocksDestroyedSinceCheckpoint = 0;
			GameplayController.instance.inventoryManager.SaveCheckpointInventory();
			if (!(GameplayBuilder.instance.game.gameEndFlag == new WorldLocation(tileInfo.bloxelLevel.id, tileInfo.locationInLevel, tileInfo.bloxelLevel.location)))
			{
				GameObject gameObject = Object.Instantiate(GameplayController.instance.checkPointPrefab);
				gameObject.transform.SetParent(UIOverlayCanvas.instance.transform);
				gameObject.transform.localScale = Vector3.one;
			}
		}
	}

	public void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == PixelPlayerController.instance.colliderLayerValue)
		{
			SetCheckpoint();
		}
	}
}
