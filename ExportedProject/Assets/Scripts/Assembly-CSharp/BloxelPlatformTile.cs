using UnityEngine;

public sealed class BloxelPlatformTile : BloxelTile
{
	public static string collider2DLayer = "Ground";

	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.platformPool.Push(this);
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(collider2DLayer);
		tileRenderer.colliderIsTrigger = false;
		tileRenderer.tileScale = new Vector3(1f, 1f, 13f);
	}
}
