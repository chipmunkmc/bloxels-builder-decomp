using System.Collections.Generic;
using UnityEngine;

public sealed class BloxelPowerUpTile : BloxelTile
{
	public static Dictionary<PowerUpType, KeyValuePair<string, string>> PowerUpNames = new Dictionary<PowerUpType, KeyValuePair<string, string>>
	{
		{
			PowerUpType.Bomb,
			new KeyValuePair<string, string>("gamebuilder138", "Bomb")
		},
		{
			PowerUpType.Health,
			new KeyValuePair<string, string>("gamebuilder139", "Health")
		},
		{
			PowerUpType.Dash,
			new KeyValuePair<string, string>("gamebuilder197", "Dash")
		},
		{
			PowerUpType.BigBro,
			new KeyValuePair<string, string>("gamebuilder198", "BigBro")
		},
		{
			PowerUpType.Jetpack,
			new KeyValuePair<string, string>("gamebuilder140", "Jetpack")
		},
		{
			PowerUpType.Shrink,
			new KeyValuePair<string, string>("gamebuilder141", "Shrink")
		},
		{
			PowerUpType.Invincibility,
			new KeyValuePair<string, string>("gamebuilder142", "Invincibility")
		},
		{
			PowerUpType.Map,
			new KeyValuePair<string, string>("gamebuilder32", "Map")
		}
	};

	public static string collider2DLayer = "PowerUp";

	public PowerUpType type;

	public Object bombPrefab;

	public Object heartPrefab;

	public Object dashPrefab;

	public Object bigBroPrefab;

	public Object jetpackPrefab;

	public Object shrinkPrefab;

	public Object invincibilityPrefab;

	public Object mapPrefab;

	public float spawnOffset = 2.1666667f;

	public override void PrepareSpawn()
	{
		base.PrepareSpawn();
		if (tileInfo == null)
		{
			type = PowerUpType.Bomb;
		}
		else if (!tileInfo.bloxelLevel.powerups.TryGetValue(tileInfo.locationInLevel, out type))
		{
			type = PowerUpType.Bomb;
		}
	}

	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.powerUpPool.Push(this);
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(collider2DLayer);
		tileRenderer.colliderIsTrigger = false;
		tileRenderer.tileScale = new Vector3(1f, 1f, 13f);
	}

	public override void InitWithNewTileInfo(TileInfo tileInfo)
	{
		base.tileInfo = tileInfo;
		GameplayBuilder.instance.game.levels.TryGetValue(tileInfo.levelLocationInWorld, out bloxelLevel);
		if (!tileInfo.bloxelLevel.powerups.TryGetValue(tileInfo.locationInLevel, out type))
		{
			type = PowerUpType.Bomb;
		}
		CreateTile();
	}

	public void SpawnContents()
	{
		float x = base.transform.position.x;
		float y = base.transform.position.y;
		GameObject gameObject = null;
		switch (type)
		{
		case PowerUpType.Health:
			gameObject = Object.Instantiate(heartPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		case PowerUpType.Bomb:
			gameObject = Object.Instantiate(bombPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		case PowerUpType.Dash:
			gameObject = Object.Instantiate(dashPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		case PowerUpType.BigBro:
			gameObject = Object.Instantiate(bigBroPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		case PowerUpType.Jetpack:
			gameObject = Object.Instantiate(jetpackPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		case PowerUpType.Shrink:
			gameObject = Object.Instantiate(shrinkPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		case PowerUpType.Invincibility:
			gameObject = Object.Instantiate(invincibilityPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		case PowerUpType.Map:
			gameObject = Object.Instantiate(mapPrefab, new Vector3(x + spawnOffset, y + spawnOffset, WorldWrapper.instance.selfTransform.position.z), Quaternion.identity) as GameObject;
			break;
		}
		gameObject.transform.SetParent(WorldWrapper.instance.runtimeObjectContainer);
		gameObject.layer = LayerMask.NameToLayer(BloxelCollectibleTile.collider2DLayer);
	}
}
