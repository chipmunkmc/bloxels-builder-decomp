using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using UnityEngine;

public abstract class BloxelProject : IEquatable<BloxelProject>
{
	public delegate void HandleTitleChange();

	public static Dictionary<ProjectType, string> projectTypeDisplayKeys = new Dictionary<ProjectType, string>
	{
		{
			ProjectType.Board,
			"iWall107"
		},
		{
			ProjectType.Animation,
			"iWall108"
		},
		{
			ProjectType.MegaBoard,
			"iWall109"
		},
		{
			ProjectType.Level,
			"iWall110"
		},
		{
			ProjectType.Character,
			"iWall111"
		},
		{
			ProjectType.Game,
			"challenges43"
		},
		{
			ProjectType.Music,
			string.Empty
		},
		{
			ProjectType.Weapon,
			string.Empty
		},
		{
			ProjectType.SFXPack,
			string.Empty
		},
		{
			ProjectType.Program,
			string.Empty
		},
		{
			ProjectType.ProgramBlock,
			string.Empty
		},
		{
			ProjectType.Brain,
			"gamebuilder166"
		},
		{
			ProjectType.None,
			string.Empty
		}
	};

	public static Dictionary<ProjectType, string> projectTypePluralizedDisplayKeys = new Dictionary<ProjectType, string>
	{
		{
			ProjectType.Board,
			"iWall119"
		},
		{
			ProjectType.Animation,
			"iWall120"
		},
		{
			ProjectType.MegaBoard,
			"iWall121"
		},
		{
			ProjectType.Level,
			"iWall122"
		},
		{
			ProjectType.Character,
			"iWall123"
		},
		{
			ProjectType.Game,
			"iWall124"
		},
		{
			ProjectType.Music,
			string.Empty
		},
		{
			ProjectType.Weapon,
			string.Empty
		},
		{
			ProjectType.SFXPack,
			string.Empty
		},
		{
			ProjectType.Program,
			string.Empty
		},
		{
			ProjectType.ProgramBlock,
			string.Empty
		},
		{
			ProjectType.Brain,
			"iWall125"
		},
		{
			ProjectType.None,
			string.Empty
		}
	};

	private static int nextLocalID = int.MinValue;

	public ProjectType type;

	public ProjectSyncInfo syncInfo;

	public List<string> tags = new List<string>();

	private Guid _id;

	public int builtWithVersion;

	public bool ugcApproved;

	private string _idString;

	public string _serverID;

	public static int MAX_TAGS = 6;

	public static Guid INVALID_ID = new Guid("00000000-0000-0000-0000-000000000000");

	public string projectPath;

	public static string downloadDirectory = "downloads";

	public BloxelBoard coverBoard;

	public string title;

	public string owner;

	public bool isDirty;

	public int localID { get; private set; }

	public Guid id
	{
		get
		{
			return _id;
		}
		set
		{
			_id = value;
			_idString = _id.ToString();
			localID = nextLocalID++;
		}
	}

	public event HandleTitleChange OnTitleChange;

	public string ID()
	{
		return _idString;
	}

	public Guid GenerateRandomID()
	{
		return Guid.NewGuid();
	}

	public virtual void SetBuildVersion(int ver)
	{
		builtWithVersion = ver;
	}

	public void AddTag(string tag)
	{
		if (tags.Count < MAX_TAGS)
		{
			tags.Add(tag);
		}
	}

	public void AddMultipleTags(List<string> tagsToAdd)
	{
		if (tags.Count < tags.Count + tagsToAdd.Count)
		{
			tags.AddRange(tagsToAdd);
		}
	}

	public void OverrideTags(List<string> tagsToAdd)
	{
		tags.Clear();
		tags.AddRange(tagsToAdd);
	}

	public void RemoveTag(string tag)
	{
		if (tags.Contains(tag))
		{
			tags.Remove(tag.ToString());
		}
	}

	public void SetTitle(string _title)
	{
		title = _title;
		if (this.OnTitleChange != null)
		{
			this.OnTitleChange();
		}
	}

	public void SetOwner(string _owner)
	{
		owner = _owner;
	}

	public void CheckAndSetMeAsOwner()
	{
		if (CurrentUser.instance.account != null && (owner == null || owner == string.Empty))
		{
			SetOwner(CurrentUser.instance.account.serverID);
		}
	}

	public virtual void SetDirty(bool shouldSync)
	{
		isDirty = shouldSync;
	}

	public virtual ProjectBundle Compress()
	{
		return ProjectBundle.Invalid;
	}

	public abstract bool Save(bool ballsDeep = true, bool shouldQueue = true);

	public abstract int GetBloxelCount();

	public static Color GetColorFromType(ProjectType type)
	{
		Color result = Color.black;
		switch (type)
		{
		case ProjectType.Board:
			result = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange);
			break;
		case ProjectType.Animation:
			result = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue);
			break;
		case ProjectType.Character:
			result = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
			break;
		case ProjectType.MegaBoard:
			result = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green);
			break;
		case ProjectType.Game:
			result = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Purple);
			break;
		}
		return result;
	}

	public static ProjectType GetProjectTypeFromString(string classString)
	{
		ProjectType result = ProjectType.None;
		switch (classString)
		{
		case "Board":
			result = ProjectType.Board;
			break;
		case "Animation":
			result = ProjectType.Animation;
			break;
		case "Character":
			result = ProjectType.Character;
			break;
		case "MegaBoard":
			result = ProjectType.MegaBoard;
			break;
		case "Level":
			result = ProjectType.Level;
			break;
		case "Brain":
			result = ProjectType.Brain;
			break;
		case "Game":
			result = ProjectType.Game;
			break;
		}
		return result;
	}

	public static string GetRelativePathForType(ProjectType _type, string _projectId)
	{
		string result = string.Empty;
		switch (_type)
		{
		case ProjectType.Board:
			result = BloxelBoard.rootDirectory + "/" + BloxelBoard.subDirectory + "/" + _projectId + "/" + BloxelBoard.filename;
			break;
		case ProjectType.Animation:
			result = BloxelAnimation.rootDirectory + "/" + BloxelAnimation.subDirectory + "/" + _projectId + "/" + BloxelAnimation.filename;
			break;
		case ProjectType.Character:
			result = BloxelCharacter.rootDirectory + "/" + BloxelCharacter.subDirectory + "/" + _projectId + "/" + BloxelCharacter.filename;
			break;
		case ProjectType.MegaBoard:
			result = BloxelMegaBoard.rootDirectory + "/" + BloxelMegaBoard.subDirectory + "/" + _projectId + "/" + BloxelMegaBoard.filename;
			break;
		case ProjectType.Level:
			result = BloxelLevel.rootDirectory + "/" + BloxelLevel.subDirectory + "/" + _projectId + "/" + BloxelLevel.filename;
			break;
		case ProjectType.Brain:
			result = BloxelBrain.rootDirectory + "/" + BloxelBrain.subDirectory + "/" + _projectId + "/" + BloxelBrain.filename;
			break;
		case ProjectType.Game:
			result = BloxelGame.rootDirectory + "/" + BloxelGame.subDirectory + "/" + _projectId + "/" + BloxelGame.filename;
			break;
		}
		return result;
	}

	public static string GetAbsolutePathForType(ProjectType _type, string _projectId)
	{
		string result = string.Empty;
		switch (_type)
		{
		case ProjectType.Board:
			result = AssetManager.baseStoragePath + BloxelBoard.rootDirectory + "/" + BloxelBoard.subDirectory + "/" + _projectId + "/" + BloxelBoard.filename;
			break;
		case ProjectType.Animation:
			result = AssetManager.baseStoragePath + BloxelAnimation.rootDirectory + "/" + BloxelAnimation.subDirectory + "/" + _projectId + "/" + BloxelAnimation.filename;
			break;
		case ProjectType.Character:
			result = AssetManager.baseStoragePath + BloxelCharacter.rootDirectory + "/" + BloxelCharacter.subDirectory + "/" + _projectId + "/" + BloxelCharacter.filename;
			break;
		case ProjectType.MegaBoard:
			result = AssetManager.baseStoragePath + BloxelMegaBoard.rootDirectory + "/" + BloxelMegaBoard.subDirectory + "/" + _projectId + "/" + BloxelMegaBoard.filename;
			break;
		case ProjectType.Level:
			result = AssetManager.baseStoragePath + BloxelLevel.rootDirectory + "/" + BloxelLevel.subDirectory + "/" + _projectId + "/" + BloxelLevel.filename;
			break;
		case ProjectType.Brain:
			result = AssetManager.baseStoragePath + BloxelBrain.rootDirectory + "/" + BloxelBrain.subDirectory + "/" + _projectId + "/" + BloxelBrain.filename;
			break;
		case ProjectType.Game:
			result = AssetManager.baseStoragePath + BloxelGame.rootDirectory + "/" + BloxelGame.subDirectory + "/" + _projectId + "/" + BloxelGame.filename;
			break;
		}
		return result;
	}

	public static DirectoryInfo[] GetTopLevelGuestDirectories()
	{
		return new DirectoryInfo[7]
		{
			new DirectoryInfo(Application.persistentDataPath + "/" + BloxelGame.rootDirectory),
			new DirectoryInfo(Application.persistentDataPath + "/" + BloxelLevel.rootDirectory),
			new DirectoryInfo(Application.persistentDataPath + "/" + BloxelMegaBoard.rootDirectory),
			new DirectoryInfo(Application.persistentDataPath + "/" + BloxelCharacter.rootDirectory),
			new DirectoryInfo(Application.persistentDataPath + "/" + BloxelAnimation.rootDirectory),
			new DirectoryInfo(Application.persistentDataPath + "/" + BloxelBrain.rootDirectory),
			new DirectoryInfo(Application.persistentDataPath + "/" + BloxelBoard.rootDirectory)
		};
	}

	public static int GetGuestAssetCount()
	{
		DirectoryInfo[] topLevelGuestDirectories = GetTopLevelGuestDirectories();
		int num = 0;
		DirectoryInfo[] array = topLevelGuestDirectories;
		foreach (DirectoryInfo directoryInfo in array)
		{
			IEnumerator<string> enumerator = PPUtilities.GetFileList(directoryInfo.FullName, "*").GetEnumerator();
			while (enumerator.MoveNext())
			{
				num++;
			}
		}
		return num;
	}

	public bool Equals(BloxelProject other)
	{
		return id.Equals(other.id);
	}

	public override bool Equals(object obj)
	{
		return Equals(obj as BloxelProject);
	}

	public override int GetHashCode()
	{
		return id.GetHashCode();
	}
}
