using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using BestHTTP;
using SocketIO;
using TMPro;
using UnityEngine;
using VoxelEngine;
using WebSocketSharp;

public class BloxelServerInterface : MonoBehaviour
{
	public delegate void HandleSquareUpdate(SocketIOEvent msg);

	public delegate void HandleServerLoginSync(SocketIOEvent msg);

	public delegate void HandleCoinsEarned(SocketIOEvent msg);

	public delegate void HandleUserAccountUpdate(SocketIOEvent msg);

	public delegate void HandleUserFollowed(SocketIOEvent msg);

	public delegate void HandleUserUnFollowed(SocketIOEvent msg);

	public delegate void HandleServerMessage(SocketIOEvent msg);

	public delegate void HandleServerColors();

	public delegate void HandleNewsItems(List<NewsItem> news);

	public delegate void HandleUpdateNews();

	public delegate void HandleStopCoins();

	public delegate void HandleParentApproved();

	public delegate void HandleFeaturedGems(string featuredBool);

	public delegate void HandleBackDoorMessage(string message);

	public static BloxelServerInterface instance;

	public GameObject socketObject;

	public SocketIOComponent socket;

	private string _httpPrefix = "https://";

	private string _webSocketPrefix = "wss://";

	private string _uploadImagesEndpoint = "uploadCover";

	private string _uploadScreenshotEndpoint = "uploadScreenshot";

	private string _reportSquareEndpoint = "reportSquare";

	private string _uploadZipEndpoint = "uploadZip";

	private string _apiEndpoint = "v2";

	public static string configEndpoint = "config";

	public static string serverToken = "uMPPrc8342R9DY5yJ9";

	private const string string_emit_coinCount = "update coins";

	private const string string_emit_removeSquare = "remove square";

	private const string string_emit_userFollowed = "user followed";

	private const string string_emit_updateUserAvatar = "updateUserAvatar";

	private const string string_emit_toggleLike = "toggleLike";

	private const string string_emit_playGame = "play game";

	private const string string_emit_beatGame = "beatGame";

	private const string string_emit_sendCoins = "send money";

	private const string string_emit_takeSquare = "take square";

	private const string string_emit_coordinateUpdate = "coordinate update";

	private const string string_emit_downloadSquare = "downloadSquare";

	private const string string_emit_viewSquare = "viewSquare";

	private const string string_emit_versionUpdate = "versionUpdate";

	private const string string_emit_tempLogin = "tempLogin";

	[HideInInspector]
	public string uploadURL;

	[HideInInspector]
	public string uploadScreenshot;

	[HideInInspector]
	public string uploadZipURL;

	[HideInInspector]
	public string apiURL;

	[HideInInspector]
	public string reportURL;

	[HideInInspector]
	public string reportUserURL;

	[HideInInspector]
	public string reportSquareURL;

	public Server server;

	private string _serverName = "api.bloxelsbuilder.com";

	private string _port = ":80";

	public UnityEngine.Object socketErrorPrefab;

	public UnityEngine.Object updatePrefab;

	public JSONObject yellowHues;

	public List<NewsItem> newsItems = new List<NewsItem>();

	public List<BloxelBoard> defaultAvatars = new List<BloxelBoard>(5);

	public static List<string> featuredGameIds = new List<string>(6);

	public static bool initComplete;

	public bool socketDied;

	public bool receivingUpdates = true;

	private bool shouldCheckForUpdates = true;

	private float timeOfLastUpdate;

	private float maxTimeout = 30f;

	public int connectAttempts;

	public bool attemptingReconnect;

	private bool _hasServerAssets;

	[Header("Environment Indicator")]
	public GameObject env;

	public TextMeshProUGUI envText;

	public event HandleSquareUpdate OnSquareUpdate;

	public event HandleServerLoginSync OnServerLogin;

	public event HandleCoinsEarned OnCoinsEarned;

	public event HandleUserAccountUpdate OnUserAccountUpdate;

	public event HandleUserFollowed OnUserFollowed;

	public event HandleUserUnFollowed OnUserUnFollowed;

	public event HandleServerMessage OnServerMessage;

	public event HandleServerColors OnServerColors;

	public event HandleNewsItems OnNewsItems;

	public event HandleUpdateNews OnUpdateNews;

	public event HandleStopCoins OnStopSendingCoins;

	public event HandleParentApproved OnApproved;

	public event HandleFeaturedGems OnFeaturedGems;

	public event HandleBackDoorMessage OnBackDoor;

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		if (!initComplete)
		{
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			updatePrefab = Resources.Load("Prefabs/UIPopupMessage");
			switch (server)
			{
			case Server.Local:
				_httpPrefix = "http://";
				_webSocketPrefix = "ws://";
				_serverName = "localhost";
				_port = ":5000";
				env.transform.localScale = Vector3.one;
				envText.SetText("L");
				break;
			case Server.Staging:
				_serverName = "staging-lb.bloxelsbuilder.com";
				_port = ":443";
				env.transform.localScale = Vector3.one;
				envText.SetText("S");
				break;
			case Server.Production:
				_serverName = "api.bloxelsbuilder.com";
				_port = ":443";
				env.transform.localScale = Vector3.zero;
				break;
			}
			apiURL = _httpPrefix + _serverName + _port + "/" + _apiEndpoint;
			reportURL = _httpPrefix + _serverName + _port + "/report";
			reportUserURL = _httpPrefix + _serverName + _port + "/reportUser";
			reportSquareURL = _httpPrefix + _serverName + _port + "/" + _reportSquareEndpoint;
			uploadURL = _httpPrefix + _serverName + _port + "/" + _uploadImagesEndpoint;
			uploadScreenshot = _httpPrefix + _serverName + _port + "/" + _uploadScreenshotEndpoint;
			uploadZipURL = _httpPrefix + _serverName + _port + "/" + _uploadZipEndpoint;
			if (socket == null)
			{
				socket = socketObject.AddComponent<SocketIOComponent>();
			}
			if (GetComponent<BloxelServerRequests>() == null)
			{
				base.gameObject.AddComponent<BloxelServerRequests>();
			}
			if (GetComponent<S3Interface>() == null)
			{
				base.gameObject.AddComponent<S3Interface>();
			}
		}
	}

	private void Start()
	{
		if (!initComplete)
		{
			InternetReachabilityVerifier.Instance.statusChangedDelegate += HandleInternetConnection;
			Init();
			if (InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
			{
				StartCoroutine(CheckVersion());
				StartCoroutine(GetServerAssets());
			}
		}
	}

	private void OnDestroy()
	{
		InternetReachabilityVerifier.Instance.statusChangedDelegate -= HandleInternetConnection;
	}

	private void Update()
	{
		if (Time.time - timeOfLastUpdate >= maxTimeout)
		{
			receivingUpdates = false;
		}
	}

	private void HandleInternetConnection(InternetReachabilityVerifier.Status newStatus)
	{
		if (newStatus == InternetReachabilityVerifier.Status.NetVerified && !_hasServerAssets)
		{
			StartCoroutine(CheckVersion());
			StartCoroutine(GetServerAssets());
		}
	}

	public void Init()
	{
		initComplete = false;
		socketObject.SetActive(false);
		reportURL = _httpPrefix + _serverName + _port + "/report";
		reportUserURL = _httpPrefix + _serverName + _port + "/reportUser";
		reportSquareURL = _httpPrefix + _serverName + _port + "/" + _reportSquareEndpoint;
		uploadURL = _httpPrefix + _serverName + _port + "/" + _uploadImagesEndpoint;
		uploadZipURL = _httpPrefix + _serverName + _port + "/" + _uploadZipEndpoint;
		apiURL = _httpPrefix + _serverName + _port + "/" + _apiEndpoint;
		socket.url = _webSocketPrefix + _serverName + _port + "/socket.io/?EIO=4&transport=websocket";
		socketObject.SetActive(true);
		socket.Connect();
		socket.On("square update", BlastSquareUpdate);
		socket.On("coins earned", BlastCoinNotification);
		socket.On("follow notification", BlastUserFollowed);
		socket.On("unfollow notification", BlastUserUnFollowed);
		socket.On("message", MessageHandler);
		socket.On("status", Status);
		socket.On("stopSendingCoins", StopSendingCoins);
		socket.On("news", NewsChange);
		socket.On("versionDenied", Receive_VersionUpdate);
		socket.On("parent_approved", ParentApproved);
		socket.On("featuredGems", GemsEarned);
		socket.On("backdoor", HandleBackDoor);
		socket.socket.OnClose += OnSocketClosed;
		socket.socket.OnOpen += OnSocketOpen;
		shouldCheckForUpdates = true;
		socketDied = false;
		receivingUpdates = true;
		initComplete = true;
	}

	private IEnumerator CheckVersion()
	{
		yield return new WaitForSeconds(1f);
		Emit_VersionUpdate();
	}

	private IEnumerator GetServerAssets()
	{
		yield return StartCoroutine(BloxelServerRequests.instance.FindSlimFeaturedGames(delegate(JSONObject res)
		{
			int num = 0;
			if (res != null)
			{
				num = res.list.Count;
			}
			if (PrefsManager.instance.featuredBooleanString.Length < num)
			{
				PrefsManager.instance.featuredBooleanString = PrefsManager.instance.featuredBooleanString.PadRight(num, '0');
				PrefsManager.instance.SetFeaturedBool(PrefsManager.instance.featuredBooleanString);
			}
			for (int j = 0; j < num; j++)
			{
				if (res.list[j].HasField("squareID") && !featuredGameIds.Contains(res.list[j].GetField("squareID").str))
				{
					featuredGameIds.Add(res.list[j].GetField("squareID").str);
				}
			}
		}));
		yield return new WaitForEndOfFrame();
		yield return StartCoroutine(GetYellowHues(delegate(string res)
		{
			if (!string.IsNullOrEmpty(res) && res != "ERROR")
			{
				yellowHues = new JSONObject(res);
				if (this.OnServerColors != null)
				{
					this.OnServerColors();
				}
			}
			else
			{
				yellowHues = null;
			}
		}));
		yield return new WaitForEndOfFrame();
		yield return StartCoroutine(BloxelServerRequests.instance.FindNewsItems(delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				JSONObject jSONObject = new JSONObject(response);
				for (int i = 0; i < jSONObject.list.Count; i++)
				{
					newsItems.Add(new NewsItem(jSONObject.list[i].ToString()));
				}
				if (this.OnNewsItems != null)
				{
					this.OnNewsItems(newsItems);
				}
			}
		}));
		yield return new WaitForEndOfFrame();
		yield return StartCoroutine(BloxelServerRequests.instance.FindDefaultAvatars(delegate(List<BloxelBoard> boards)
		{
			defaultAvatars = boards;
		}));
		yield return new WaitForEndOfFrame();
		yield return StartCoroutine(BloxelServerRequests.instance.SendDeviceInfo(delegate
		{
		}));
		_hasServerAssets = true;
	}

	public IEnumerator GetYellowHues(Action<string> callback)
	{
		string path = "yellowHues.json";
		string url = instance.apiURL + "/" + path;
		HTTPRequest request = new HTTPRequest(new Uri(url), delegate(HTTPRequest req, HTTPResponse resp)
		{
			switch (req.State)
			{
			case HTTPRequestStates.Finished:
				if (resp.DataAsText.Contains("Cannot"))
				{
					callback("ERROR");
				}
				else
				{
					callback(resp.DataAsText);
				}
				break;
			case HTTPRequestStates.Error:
				callback("ERROR");
				break;
			case HTTPRequestStates.Aborted:
				callback("ERROR");
				break;
			case HTTPRequestStates.ConnectionTimedOut:
				callback("ERROR");
				break;
			case HTTPRequestStates.TimedOut:
				callback("ERROR");
				break;
			}
		})
		{
			Timeout = TimeSpan.FromSeconds(5.0)
		};
		yield return null;
	}

	private void OnSocketOpen(object sender, EventArgs e)
	{
		socket.socket.OnOpen -= OnSocketOpen;
		socket.socket.OnOpen += OnSocketOpen;
		if (attemptingReconnect)
		{
			DoOnMainThread.ExecuteOnMainThread.Enqueue(delegate
			{
				StartCoroutine("ForceSocketAccountSync");
			});
			connectAttempts = 0;
			attemptingReconnect = false;
		}
	}

	private void OnSocketClosed(object sender, CloseEventArgs e)
	{
		socket.socket.OnClose -= OnSocketClosed;
		receivingUpdates = false;
		socketDied = true;
		DoOnMainThread.ExecuteOnMainThread.Enqueue(delegate
		{
			AttemptReconnect();
		});
	}

	public void AttemptReconnect()
	{
		attemptingReconnect = true;
		StopCoroutine("ForceSocketAccountSync");
		if (connectAttempts == 5)
		{
			BailToHomeScene();
			return;
		}
		Init();
		connectAttempts++;
	}

	private IEnumerator ForceSocketAccountSync()
	{
		while (!socket.IsConnected)
		{
			yield return new WaitForEndOfFrame();
		}
		if (CurrentUser.instance.active)
		{
			Emit_TempLogin(CurrentUser.instance.account.serverID);
		}
	}

	public void BailToHomeScene()
	{
		if (AppStateManager.instance.currentScene == SceneName.Viewer)
		{
			AppStateManager.instance.attemptedViewerRefresh = true;
			BloxelsSceneManager.instance.GoTo(SceneName.Home);
		}
	}

	public void Emit_VersionUpdate()
	{
		JSONObject jSONObject = new JSONObject();
		if (AppStateManager.instance != null)
		{
			jSONObject.AddField("version", AppStateManager.instance.appVersion);
		}
		else
		{
			jSONObject.AddField("version", "1.4b1");
		}
		socket.Emit("versionUpdate", jSONObject);
	}

	public void Emit_TempLogin(string mongoId)
	{
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("id", mongoId);
		socket.Emit("tempLogin", jSONObject);
	}

	public void Emit_UpdateCoins(string userAccountID, int coinCount)
	{
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("userID", userAccountID);
		jSONObject.AddField("count", coinCount);
		socket.Emit("update coins", jSONObject);
	}

	public void Emit_RemoveSquare(string squareServerID, string squarePublisherID, string squareOwnerID)
	{
		string data = squareServerID + "," + squarePublisherID + "," + squareOwnerID;
		socket.Emit("remove square", data);
	}

	public void Emit_UpdateUserAvatar(UserAccount user)
	{
		socket.Emit("updateUserAvatar", user.avatar._serverID);
	}

	public void Emit_ToggleLike(JSONObject json)
	{
		socket.Emit("toggleLike", json);
	}

	public void Emit_ViewSquare(string squareID)
	{
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("squareID", squareID);
		socket.Emit("viewSquare", jSONObject);
	}

	public void Emit_DownloadSquare(string userID, string squareID, int val)
	{
		JSONObject jSONObject = new JSONObject();
		jSONObject.AddField("squareID", squareID);
		jSONObject.AddField("userID", userID);
		jSONObject.AddField("cost", val);
		socket.Emit("downloadSquare", jSONObject);
	}

	public void Emit_PlayGame(string gameID, string gameTitle, string creator, string canvasCoordinate)
	{
		string data = gameID + "|" + gameTitle + "|" + creator + "|" + canvasCoordinate;
		socket.Emit("play game", data);
	}

	public void Emit_BeatGame(string squareID)
	{
		socket.Emit("beatGame", squareID);
	}

	public void Emit_SendCoins(int amount, string toUser, string fromUser, ProjectType projectType, string title)
	{
		string data = amount + "," + toUser + "," + fromUser + "," + projectType.ToString() + "," + title;
		socket.Emit("send money", data);
	}

	public void Emit_TakeSquare(string canvasCoordinate, string s3ImageUrl, ProjectType projectType, string projectID, string owner)
	{
		string data = canvasCoordinate + "," + s3ImageUrl + "," + projectType.ToString() + "," + projectID + "," + owner;
		socket.Emit("take square", data);
	}

	public void Emit_CoordinateUpdate(WorldPos2D screenCenter)
	{
		JSONObject jSONObject = new JSONObject(JSONObject.Type.ARRAY);
		jSONObject.Add(screenCenter.x);
		jSONObject.Add(screenCenter.y);
		socket.Emit("coordinate update", jSONObject);
	}

	private void Status(SocketIOEvent msg)
	{
		receivingUpdates = true;
		timeOfLastUpdate = Time.time;
	}

	private void Receive_VersionUpdate(SocketIOEvent msg)
	{
		if (msg.data != null && msg.data.HasField("update"))
		{
			AppStateManager.instance.outDatedVersion = true;
			GameObject gameObject = UIOverlayCanvas.instance.Popup(updatePrefab);
			UIPopupMessage component = gameObject.GetComponent<UIPopupMessage>();
			if (msg.data.HasField("title"))
			{
				component.title = msg.data.GetField("title").str;
			}
			else
			{
				component.title = "UPDATE";
			}
			if (msg.data.HasField("description"))
			{
				component.description = msg.data.GetField("description").str;
			}
			else
			{
				component.description = "You are using an outdated version of the app. To gain access to all areas, please update for free now.";
			}
		}
	}

	private void NewsChange(SocketIOEvent msg)
	{
		if (msg.data != null && msg.data.HasField("newsItems"))
		{
			if (this.OnUpdateNews != null)
			{
				this.OnUpdateNews();
			}
			JSONObject jSONObject = msg.data["newsItems"];
			newsItems.Clear();
			for (int i = 0; i < jSONObject.list.Count; i++)
			{
				NewsItem item = new NewsItem(jSONObject.list[i].ToString());
				newsItems.Add(item);
			}
			if (this.OnNewsItems != null)
			{
				this.OnNewsItems(newsItems);
			}
		}
	}

	private void GemsEarned(SocketIOEvent msg)
	{
		if (msg.data.HasField("featuredBool") && this.OnFeaturedGems != null)
		{
			this.OnFeaturedGems(msg.data.GetField("featuredBool").str);
		}
	}

	private void HandleBackDoor(SocketIOEvent msg)
	{
		if (msg.data.HasField("message") && this.OnBackDoor != null)
		{
			this.OnBackDoor(msg.data.GetField("message").str);
		}
	}

	private void ParentApproved(SocketIOEvent msg)
	{
		if (this.OnApproved != null)
		{
			this.OnApproved();
		}
	}

	private void StopSendingCoins(SocketIOEvent msg)
	{
		if (this.OnStopSendingCoins != null)
		{
			this.OnStopSendingCoins();
		}
	}

	private void MessageHandler(SocketIOEvent msg)
	{
		if (this.OnServerMessage != null)
		{
			this.OnServerMessage(msg);
		}
	}

	private void BlastSquareUpdate(SocketIOEvent msg)
	{
		if (this.OnSquareUpdate != null)
		{
			this.OnSquareUpdate(msg);
		}
	}

	private void BlastCoinNotification(SocketIOEvent msg)
	{
		if (this.OnCoinsEarned != null)
		{
			this.OnCoinsEarned(msg);
		}
	}

	private void BlastUserFollowed(SocketIOEvent msg)
	{
		if (this.OnUserFollowed != null)
		{
			this.OnUserFollowed(msg);
		}
	}

	private void BlastUserUnFollowed(SocketIOEvent msg)
	{
		if (this.OnUserUnFollowed != null)
		{
			this.OnUserUnFollowed(msg);
		}
	}
}
