using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using BestHTTP;
using UnityEngine;

public class BloxelServerRequests : MonoBehaviour
{
	public static BloxelServerRequests instance;

	public string coverBoardS3URL;

	public Dictionary<ProjectType, string> fullDataEndpointLookup;

	public Dictionary<ProjectType, string> simpleModelEndpoints;

	public string accountEndpoint;

	public string squareEndpoint;

	public string featuredSquareEndpoint;

	public string featuredGameSquaresEndpoint;

	public string newsEndpoint;

	public string avatarsEndpoint;

	public string deviceEndpoint;

	public string mostPlaysEndpoint;

	public string gamesByFeaturedCreatorsEndpoint;

	public string mostLikedGamesEndpoint;

	public string bigGamesEndpoint;

	public static readonly string ErrorString = "ERROR";

	public static readonly string LoginRequestAuth = "440eda3f-1e53-4619-b465-96660d09259a";

	public static readonly string AuthBypassString = "0e87d5c4-1da0-4f7e-a5ef-fb0ce2517b48";

	private const string GLU_ADRESS = "http://glu-brain-staging.ymczdksckx.us-west-2.elasticbeanstalk.com";

	private const string GLU_BUILDER_NOTIFICATION_SUFFIX = "/builder/notification";

	private const string GLU_BUILDER_USER_SUFFIX = "/builder/user";

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		fullDataEndpointLookup = new Dictionary<ProjectType, string>
		{
			{
				ProjectType.Board,
				BloxelServerInterface.instance.apiURL + "/boards/"
			},
			{
				ProjectType.Animation,
				BloxelServerInterface.instance.apiURL + "/animations/"
			},
			{
				ProjectType.Character,
				BloxelServerInterface.instance.apiURL + "/fullcharacter/"
			},
			{
				ProjectType.MegaBoard,
				BloxelServerInterface.instance.apiURL + "/fullmegaboard/"
			},
			{
				ProjectType.Game,
				BloxelServerInterface.instance.apiURL + "/fast-game/"
			},
			{
				ProjectType.Level,
				BloxelServerInterface.instance.apiURL + "/levels/"
			},
			{
				ProjectType.Program,
				BloxelServerInterface.instance.apiURL + "/programs/"
			}
		};
		simpleModelEndpoints = new Dictionary<ProjectType, string>
		{
			{
				ProjectType.Board,
				BloxelServerInterface.instance.apiURL + "/boards/"
			},
			{
				ProjectType.Animation,
				BloxelServerInterface.instance.apiURL + "/animations/"
			},
			{
				ProjectType.Character,
				BloxelServerInterface.instance.apiURL + "/characters/"
			},
			{
				ProjectType.MegaBoard,
				BloxelServerInterface.instance.apiURL + "/megaboards/"
			},
			{
				ProjectType.Game,
				BloxelServerInterface.instance.apiURL + "/games/"
			},
			{
				ProjectType.Level,
				BloxelServerInterface.instance.apiURL + "/levels/"
			},
			{
				ProjectType.Program,
				BloxelServerInterface.instance.apiURL + "/programs/"
			}
		};
		accountEndpoint = BloxelServerInterface.instance.apiURL + "/accounts/";
		squareEndpoint = BloxelServerInterface.instance.apiURL + "/squares/";
		featuredSquareEndpoint = BloxelServerInterface.instance.apiURL + "/featuredSquares/";
		featuredGameSquaresEndpoint = BloxelServerInterface.instance.apiURL + "/features/";
		newsEndpoint = BloxelServerInterface.instance.apiURL + "/news/";
		avatarsEndpoint = BloxelServerInterface.instance.apiURL + "/avatars/";
		deviceEndpoint = BloxelServerInterface.instance.apiURL + "/devices";
		mostPlaysEndpoint = BloxelServerInterface.instance.apiURL + "/gameSquares/mostPlays/";
		gamesByFeaturedCreatorsEndpoint = BloxelServerInterface.instance.apiURL + "/gameSquares/gamesByFeaturedCreators/";
		mostLikedGamesEndpoint = BloxelServerInterface.instance.apiURL + "/gameSquares/mostLikes/";
		bigGamesEndpoint = BloxelServerInterface.instance.apiURL + "/gameSquares/bigGames/";
	}

	public void Reset()
	{
		coverBoardS3URL = string.Empty;
	}

	public static bool ResponseOk(string response)
	{
		return !string.IsNullOrEmpty(response) && response != ErrorString;
	}

	public static string EncodeEmail(string email)
	{
		return email.Replace("+", "%2b");
	}

	public IEnumerator SendDeviceInfo(Action<string> callback)
	{
		if (PrefsManager.instance.hasSentDeviceInfo)
		{
			callback("Already Sent");
			yield break;
		}
		JSONObject json = new JSONObject();
		json.AddField("deviceId", SystemInfo.deviceModel);
		string payloadText = json.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(deviceEndpoint), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 201)
			{
				PrefsManager.instance.hasSentDeviceInfo = true;
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payloadText);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator FindDefaultAvatars(Action<List<BloxelBoard>> callback)
	{
		List<BloxelBoard> boards = new List<BloxelBoard>(5);
		WWW www = new WWW(avatarsEndpoint + "?limit=5");
		yield return www;
		if (www.error == null)
		{
			JSONObject json = new JSONObject(Encoding.UTF7.GetString(www.bytes));
			for (int i = 0; i < json.list.Count; i++)
			{
				if (!json.list[i].HasField("projectID"))
				{
					continue;
				}
				yield return StartCoroutine(FindByID(ProjectType.Board, json.list[i].GetField("projectID").str, delegate(string response)
				{
					if (ResponseOk(response))
					{
						boards.Add(new BloxelBoard(response, DataSource.JSONString));
					}
				}));
			}
			callback(boards);
		}
		else
		{
			callback(null);
		}
	}

	public IEnumerator FindByID(ProjectType type, string projectID, Action<string> callback)
	{
		string url = fullDataEndpointLookup[type] + projectID;
		if (type == ProjectType.Animation)
		{
			url += "?populate=boards";
		}
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			callback(Encoding.UTF8.GetString(www.bytes));
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindSquareByID(string squareID, Action<string> callback)
	{
		string url = squareEndpoint + squareID;
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			callback(Encoding.UTF8.GetString(www.bytes));
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindFeaturedSquares(ProjectType type, Action<string> callback)
	{
		string url = string.Concat(str2: type.ToString(), str0: squareEndpoint, str1: "?limit=6&conditions=%7B%22squareClass%22%3A%22", str3: "%22%2C%22status%22%3A1%7D");
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			callback(@string);
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindNewestGames(Action<string> callback)
	{
		string url = squareEndpoint + "?sort=-createdAt&limit=6&conditions=%7B%22squareClass%22%3A%22Game%22%7D";
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			callback(@string);
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindMostPlayedGames(Action<string> callback)
	{
		WWW www = new WWW(mostPlaysEndpoint);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			callback(@string);
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindMostLikedGames(Action<string> callback)
	{
		WWW www = new WWW(mostLikedGamesEndpoint);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			callback(@string);
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindBigGames(Action<string> callback)
	{
		WWW www = new WWW(bigGamesEndpoint);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			callback(@string);
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindGamesByFeaturedCreators(Action<string> callback)
	{
		WWW www = new WWW(gamesByFeaturedCreatorsEndpoint);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			callback(@string);
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindSlimFeaturedGames(Action<JSONObject> callback)
	{
		string url = featuredGameSquaresEndpoint + "?sort=createdAt&select=squareID";
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			callback(new JSONObject(@string));
		}
		else
		{
			callback(null);
		}
		yield return null;
	}

	public IEnumerator FindNewsItems(Action<string> callback)
	{
		WWW www = new WWW(newsEndpoint);
		yield return www;
		if (www.error == null)
		{
			callback(Encoding.UTF8.GetString(www.bytes));
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindNewsItemByID(string id, Action<string> callback)
	{
		string url = newsEndpoint + id;
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			callback(Encoding.UTF8.GetString(www.bytes));
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindUserByToken(Action<string> callback)
	{
		string email = CurrentUser.instance.account.email;
		string token = CurrentUser.instance.loginToken;
		string url = accountEndpoint + "?conditions={\"userEmail\":\"" + email + "\",\"loginToken\":\"" + token + "\"}&populate=userAvatar";
		string encodedUrl2 = Uri.EscapeUriString(url);
		encodedUrl2 = encodedUrl2.Replace("+", "%2b");
		WWW www = new WWW(encodedUrl2);
		yield return www;
		if (www.error == null)
		{
			callback(Encoding.UTF8.GetString(www.bytes));
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindUserByID(string id, Action<string> callback)
	{
		string url = accountEndpoint + id + "?populate=userAvatar";
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			callback(Encoding.UTF8.GetString(www.bytes));
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator FindSimpleUserByID(string id, Action<string> callback)
	{
		string url = accountEndpoint + id + "?select=userName%20userAvatar&populate=userAvatar";
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			callback(Encoding.UTF8.GetString(www.bytes));
		}
		else
		{
			callback(ErrorString);
		}
		yield return null;
	}

	public IEnumerator AccountPropertyExists(string type, string value, Action<bool> callback)
	{
		string url = BloxelServerInterface.instance.apiURL + "/accounts?exists=" + type + "&val=" + value + "&cacheBuster=" + UnityEngine.Random.Range(0, 100000);
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Get, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				if (resp.DataAsText == "true")
				{
					callback(true);
				}
				else
				{
					callback(false);
				}
			}
			else
			{
				callback(true);
			}
		});
		request.Send();
		yield return null;
	}

	public IEnumerator RegisterUser(string name, string avatar, string email, string pw, bool isOfAge, Action<string> callback)
	{
		string path = "accounts";
		string gemBoolString = PrefsManager.instance.gemBooleanString;
		string spendBoolString = PrefsManager.instance.spendBooleanStringv2;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userName", name);
		payloadObject.AddField("userAvatar", avatar);
		payloadObject.AddField("userEmail", email);
		payloadObject.AddField("userPass", pw);
		payloadObject.AddField("isOfAge", isOfAge);
		payloadObject.AddField("spendBoolv2", spendBoolString);
		payloadObject.AddField("gemBool", gemBoolString);
		payloadObject.AddField("ownsBoard", AppStateManager.instance.userOwnsBoard);
		string url = BloxelServerInterface.instance.apiURL + "/" + path;
		string payloadText = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payloadText);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateFormerEDUAccount(Action<string> callback)
	{
		string path = "accounts";
		string name = CurrentUser.instance.account.userName;
		string avatar = CurrentUser.instance.account.avatar._serverID;
		string email = CurrentUser.instance.account.email;
		string parentEmail = CurrentUser.instance.account.parentEmail;
		bool isOfAge = CurrentUser.instance.account.isOfAge;
		string gemBoolString = PrefsManager.instance.gemBooleanString;
		string spendBoolString = PrefsManager.instance.spendBooleanStringv2;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userName", name);
		payloadObject.AddField("userAvatar", avatar);
		payloadObject.AddField("userEmail", email);
		payloadObject.AddField("parentEmail", parentEmail);
		payloadObject.AddField("isOfAge", isOfAge);
		payloadObject.AddField("spendBoolv2", spendBoolString);
		payloadObject.AddField("gemBool", gemBoolString);
		payloadObject.AddField("ownsBoard", AppStateManager.instance.userOwnsBoard);
		payloadObject.AddField("eduAccount", 3);
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + CurrentUser.instance.account.serverID;
		string payloadText = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 201 || resp.StatusCode == 200)
			{
				StartCoroutine(instance.FindUserByToken(delegate(string response)
				{
					if (!string.IsNullOrEmpty(response) && response != "Not Found" && response != "ERROR")
					{
						callback(response);
					}
				}));
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-UserID", CurrentUser.instance.account.serverID);
		request.SetHeader("Auth-Token", CurrentUser.instance.loginToken);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payloadText);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator ValidateUserLoginViaUsername(string username, string pw, Action<UserAccount> callback)
	{
		string path = "accounts";
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userName", username);
		payloadObject.AddField("userPass", pw);
		string payload = payloadObject.ToString();
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "?populate=userAvatar&conditions=" + payload;
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Get, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200)
			{
				JSONObject jSONObject = new JSONObject(resp.DataAsText);
				if (jSONObject[0] != null)
				{
					callback(new UserAccount(resp.DataAsText));
				}
				else
				{
					callback(null);
				}
			}
			else
			{
				callback(null);
			}
		});
		request.SetHeader("Content-Type", "application/json");
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator ValidateUserLogin(string email, string pw, Action<UserAccount> callback)
	{
		string path = "accounts";
		string encodedEmail = email.Replace("+", "%2b");
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userEmail", encodedEmail);
		payloadObject.AddField("userPass", pw);
		string payload = payloadObject.ToString();
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "?populate=userAvatar&conditions=" + payload;
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Get, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200)
			{
				JSONObject jSONObject = new JSONObject(resp.DataAsText);
				if (jSONObject[0] != null)
				{
					callback(new UserAccount(resp.DataAsText));
				}
				else
				{
					callback(null);
				}
			}
			else
			{
				callback(null);
			}
		});
		request.SetHeader("Content-Type", "application/json");
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateUserStatus(string id, string status, Action<string> callback)
	{
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + id;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userStatus", status);
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", CurrentUser.instance.loginToken);
		request.SetHeader("Auth-UserID", CurrentUser.instance.account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator GetToken(string id, Action<string> callback)
	{
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + id;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("login_request", LoginRequestAuth);
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				UserAccount userAccount = new UserAccount(resp.DataAsText);
				callback(userAccount.userToken);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Bypass", AuthBypassString);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator ResendParentEmail(string id, Action<string> callback)
	{
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + id;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("resend", "true");
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", CurrentUser.instance.loginToken);
		request.SetHeader("Auth-UserID", CurrentUser.instance.account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateChildAccount(string id, string email, string pw, bool promoEmails, Action<string> callback)
	{
		string path = "accounts";
		string gemBoolString = PrefsManager.instance.gemBooleanString;
		string spendBoolString = PrefsManager.instance.spendBooleanStringv2;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userEmail", email);
		payloadObject.AddField("shouldGetPromoEmails", promoEmails);
		payloadObject.AddField("gandalf", pw);
		payloadObject.AddField("gemBool", gemBoolString);
		payloadObject.AddField("spendBoolv2", spendBoolString);
		payloadObject.AddField("completed", true);
		payloadObject.AddField("ownsBoard", AppStateManager.instance.userOwnsBoard);
		string payload = payloadObject.ToString();
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + id;
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Bypass", AuthBypassString);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator GetPlays(string id, Action<string> callback)
	{
		string url = simpleModelEndpoints[ProjectType.Game] + id + "?select=play_counter";
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Get, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				JSONObject jSONObject = new JSONObject(resp.DataAsText);
				if (jSONObject.HasField("play_counter"))
				{
					callback(jSONObject["play_counter"].ToString());
				}
				else
				{
					callback(ErrorString);
				}
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator GetFullGameFromURL(string _url, Action<string> callback)
	{
		string path = "fullgame";
		WWW www = new WWW(_url);
		yield return www;
		if (www.error == null)
		{
			callback(www.text);
		}
		else
		{
			callback(www.error);
		}
		yield return null;
	}

	public IEnumerator ReportSquare(string id, JSONObject payload, Action<string> callback)
	{
		string payloadText = payload.ToString();
		string url = BloxelServerInterface.instance.reportSquareURL + "/" + id;
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 201 || resp.StatusCode == 200)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Bypass", AuthBypassString);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payloadText);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator ReportUser(UserAccount user, JSONObject payload, Action<string> callback)
	{
		string url = BloxelServerInterface.instance.reportUserURL + "/" + user.serverID;
		string payloadText = payload.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 201 || resp.StatusCode == 200)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", CurrentUser.instance.loginToken);
		request.SetHeader("Auth-UserID", CurrentUser.instance.account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payloadText);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator SaveBoard(string jsonString, Action<string> callback)
	{
		string url = BloxelServerInterface.instance.apiURL + "/boards";
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 201 || resp.StatusCode == 200)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", CurrentUser.instance.loginToken);
		request.SetHeader("Auth-UserID", CurrentUser.instance.account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(jsonString);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator CompressAndUpload(BloxelProject project, Action<string> callback)
	{
		string type = "?type=" + project.type;
		string id = project.ID();
		ProjectBundle bundle = project.Compress();
		WWWForm form = new WWWForm();
		form.AddField("projectJSON", bundle.json);
		if (project.type != 0)
		{
			form.AddBinaryData("file", bundle.bytes, "upload.zip", "application/zip");
		}
		Dictionary<string, string> headers = form.headers;
		headers["Auth-Token"] = CurrentUser.instance.loginToken;
		headers["Auth-UserID"] = CurrentUser.instance.account.serverID;
		WWW www = new WWW(postData: form.data, url: BloxelServerInterface.instance.uploadZipURL + type, headers: headers);
		yield return www;
		if (www.error != null)
		{
			callback(ErrorString);
			yield break;
		}
		JSONObject jSONObject = new JSONObject(www.text);
		callback(jSONObject["_id"].str);
	}

	public IEnumerator UploadScreenshot(string id, Texture2D texture, Action<string> callback)
	{
		byte[] bytes = texture.EncodeToPNG();
		WWWForm form = new WWWForm();
		string fileName = id + ".png";
		form.AddBinaryData("file", bytes, fileName, "image/png");
		Dictionary<string, string> headers = form.headers;
		headers["Auth-Token"] = CurrentUser.instance.loginToken;
		headers["Auth-UserID"] = CurrentUser.instance.account.serverID;
		WWW www = new WWW(postData: form.data, url: BloxelServerInterface.instance.uploadScreenshot, headers: headers);
		yield return www;
		if (www.error != null)
		{
			callback(ErrorString);
		}
		else
		{
			callback(www.text);
		}
	}

	public IEnumerator GetGluNotifications(Action<string> callback)
	{
		UnityEngine.Debug.Log("starting GetGluNotifications");
		HTTPRequest request = new HTTPRequest(new Uri("http://glu-brain-staging.ymczdksckx.us-west-2.elasticbeanstalk.com/builder/notification"), HTTPMethods.Get, delegate(HTTPRequest req, HTTPResponse resp)
		{
			callback(resp.DataAsText);
		});
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator SubmitMigrationRequest(UIMenuByeByeBloxels.MigrationUserSubmission migrationUserSubmission, Action<string> callback)
	{
		HTTPRequest request = new HTTPRequest(new Uri("http://glu-brain-staging.ymczdksckx.us-west-2.elasticbeanstalk.com/builder/user"), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			callback(resp.DataAsText);
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(JsonUtility.ToJson(migrationUserSubmission));
		request.Send();
		yield return StartCoroutine(request);
	}
}
