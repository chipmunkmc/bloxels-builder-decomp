using System;
using UnityEngine;

[RequireComponent(typeof(TileRenderController))]
public abstract class BloxelTile : PoolableComponent
{
	public TileInfo tileInfo;

	public BloxelLevel bloxelLevel;

	public Material defaultMaterial;

	public Material lowAlphaMaterial;

	public TileRenderController tileRenderer;

	public Rigidbody2D rigidbodyComponent;

	public bool configurable;

	[NonSerialized]
	public float xOffset;

	public override void EarlyAwake()
	{
		if (!(selfTransform != null))
		{
			spawned = false;
			selfTransform = base.transform;
			rigidbodyComponent = GetComponent<Rigidbody2D>();
			tileRenderer = GetComponent<TileRenderController>();
		}
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		selfTransform.SetParent(GameplayBuilder.instance.worldWrapper);
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		base.gameObject.SetActive(false);
		tileRenderer.ReleaseCurrentChunks();
		tileRenderer.shouldGenerateColliders = false;
	}

	public virtual void InitWithNewTileInfo(TileInfo tileInfo)
	{
		this.tileInfo = tileInfo;
		GameplayBuilder.instance.game.levels.TryGetValue(tileInfo.levelLocationInWorld, out bloxelLevel);
		CreateTile();
	}

	public virtual void CreateTile()
	{
		InitilaizeTileBehavior();
		PositionBloxelTile();
		if (tileInfo.data != null)
		{
			tileRenderer.RenderTile(tileInfo.data);
		}
		else
		{
			tileRenderer.RenderTile(LevelBuilder.SolidColorBoards[(uint)tileInfo.wireframeColor]);
		}
	}

	protected virtual void PositionBloxelTile()
	{
		Vector3 position = new Vector3(tileInfo.worldLocation.x, tileInfo.worldLocation.y, 0f);
		base.transform.position = GameplayBuilder.instance.worldWrapper.TransformPoint(position);
		base.transform.localScale = Vector3.one;
	}

	public virtual void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = defaultMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer("Destructible");
		tileRenderer.colliderIsTrigger = false;
		tileRenderer.tileScale = new Vector3(1f, 1f, 13f);
	}
}
