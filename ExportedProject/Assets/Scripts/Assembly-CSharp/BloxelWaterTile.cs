using UnityEngine;

public sealed class BloxelWaterTile : BloxelTile
{
	public static string collider2DLayer = "Water";

	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(TilePool.instance.selfTransform);
		TilePool.instance.waterPool.Push(this);
	}

	public override void InitilaizeTileBehavior()
	{
		tileRenderer.tileMaterial = lowAlphaMaterial;
		tileRenderer.collisionLayer = LayerMask.NameToLayer(collider2DLayer);
		tileRenderer.colliderIsTrigger = true;
		tileRenderer.tileScale = new Vector3(1f, 1f, 13f);
	}
}
