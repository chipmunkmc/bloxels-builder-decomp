using UnityEngine;
using VoxelEngine;

public class BloxelsBlock3D : MonoBehaviour
{
	public Transform selfTransform;

	public Rigidbody rigidbodyComponent;

	public Material blockMaterial;

	private Mesh _decoratedTileMesh;

	public MeshFilter frontFacingMesh;

	public MeshFilter rearFacingMesh;

	public MeshRenderer blockRenderer;

	private float _framesPerSecond;

	private float _frameDuration;

	public int currentFrameIndex;

	public int numberOfFrames;

	private float _timeOfLastFrameChange;

	private ProjectType projectType;

	private BloxelBoard bloxelBoard;

	private BloxelAnimation animationProject;

	public MovableChunk2D currentFrameChunk;

	public Mesh decoratedTileMesh
	{
		get
		{
			return _decoratedTileMesh;
		}
		set
		{
			_decoratedTileMesh = value;
			frontFacingMesh.sharedMesh = _decoratedTileMesh;
			rearFacingMesh.sharedMesh = _decoratedTileMesh;
		}
	}

	public float framesPerSecond
	{
		get
		{
			return _framesPerSecond;
		}
		set
		{
			_framesPerSecond = value;
			_frameDuration = 1f / _framesPerSecond;
		}
	}

	public MovableChunk2D[] frameChunks { get; protected set; }

	public void Init(BloxelProject project, TileRenderController renderController, int blockColor)
	{
		blockRenderer.sharedMaterial = blockMaterial;
		blockMaterial.color = AssetManager.instance.unityColorsForBlockColors[blockColor];
		if (project == null)
		{
			base.enabled = false;
			return;
		}
		_timeOfLastFrameChange = renderController.timeOfLastFrameChange;
		_framesPerSecond = renderController.framesPerSecond;
		renderController.currentFrameIndex = renderController.currentFrameIndex;
		RenderBlock(project);
	}

	private void RenderBlock(BloxelProject projectData)
	{
		switch (projectData.type)
		{
		case ProjectType.Board:
			RenderBoard(projectData as BloxelBoard);
			break;
		case ProjectType.Animation:
			RenderAnimation(projectData as BloxelAnimation);
			break;
		}
	}

	private void RenderBoard(BloxelBoard board)
	{
		bloxelBoard = board;
		projectType = ProjectType.Board;
		currentFrameChunk = MovableChunk2DPool.instance.Spawn();
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(bloxelBoard);
		}
		decoratedTileMesh = currentFrameChunk.GetChunkMesh(bloxelBoard.localID);
		numberOfFrames = 1;
		framesPerSecond = 1f / 3f;
	}

	private void RenderAnimation(BloxelAnimation animation)
	{
		animationProject = animation;
		projectType = ProjectType.Animation;
		numberOfFrames = animationProject.boards.Count;
		if (numberOfFrames == 0)
		{
			base.enabled = false;
			return;
		}
		frameChunks = new MovableChunk2D[numberOfFrames];
		for (int i = 0; i < numberOfFrames; i++)
		{
			MovableChunk2D movableChunk2D = MovableChunk2DPool.instance.Spawn();
			frameChunks[i] = movableChunk2D;
		}
		framesPerSecond = animationProject.GetFrameRate();
		int num = Mathf.FloorToInt(Time.time * framesPerSecond);
		_timeOfLastFrameChange = (float)num * _frameDuration;
		currentFrameIndex = num % numberOfFrames;
		currentFrameChunk = frameChunks[currentFrameIndex];
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(animationProject.boards[currentFrameIndex]);
		}
		decoratedTileMesh = currentFrameChunk.GetChunkMesh(animationProject.localID);
	}

	private void Update()
	{
		if (!(_timeOfLastFrameChange + _frameDuration < Time.time))
		{
			return;
		}
		if (projectType == ProjectType.Board)
		{
			if (!currentFrameChunk.initialized)
			{
				currentFrameChunk.InitWithBoard(this.bloxelBoard);
			}
			decoratedTileMesh = currentFrameChunk.GetChunkMesh(this.bloxelBoard.localID);
			_timeOfLastFrameChange = Time.time;
			return;
		}
		currentFrameIndex = (currentFrameIndex + 1) % numberOfFrames;
		currentFrameChunk = frameChunks[currentFrameIndex];
		BloxelBoard bloxelBoard = animationProject.boards[currentFrameIndex];
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(bloxelBoard);
		}
		decoratedTileMesh = currentFrameChunk.GetChunkMesh(bloxelBoard.localID);
		_timeOfLastFrameChange += _frameDuration;
	}

	private void OnDisale()
	{
		ReleaseCurrentChunks();
	}

	private void ReleaseCurrentChunks()
	{
		if (currentFrameChunk != null && currentFrameChunk.spawned)
		{
			currentFrameChunk.Despawn();
		}
		if (frameChunks == null || frameChunks == null)
		{
			return;
		}
		for (int i = 0; i < frameChunks.Length; i++)
		{
			MovableChunk2D movableChunk2D = frameChunks[i];
			if (movableChunk2D != null && movableChunk2D.spawned)
			{
				movableChunk2D.Despawn();
			}
		}
		frameChunks = null;
	}
}
