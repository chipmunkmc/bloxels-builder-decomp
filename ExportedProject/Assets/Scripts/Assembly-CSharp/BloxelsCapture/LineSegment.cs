using System;
using OpenCVForUnity;

namespace BloxelsCapture
{
	public class LineSegment
	{
		public const double EPSILON = 1E-06;

		public double x1;

		public double y1;

		public double x2;

		public double y2;

		public Point start;

		public Point end;

		public double length;

		public double slope;

		public LineSegment(double _startX, double _startY, double _endX, double _endY)
		{
			x1 = _startX;
			y1 = _startY;
			x2 = _endX;
			y2 = _endY;
			start = new Point(x1, y1);
			end = new Point(x2, y2);
		}

		public LineSegment()
			: this(0.0, 0.0, 0.0, 0.0)
		{
		}

		public LineSegment(Point _start, Point _end)
		{
			start = _start;
			end = _end;
			x1 = _start.x;
			y1 = _start.y;
			x2 = _end.x;
			y2 = _end.y;
			length = GetLength(this);
			slope = GetSlope(this);
		}

		public LineSegment Clone()
		{
			return new LineSegment(x1, y1, x2, y2);
		}

		public void Draw(ref Mat _displayMat, Scalar _color, int _thickness = 1)
		{
			Imgproc.line(_displayMat, start, end, _color, _thickness);
		}

		public static double GetLength(LineSegment line)
		{
			return Math.Sqrt(Math.Pow(line.x2 - line.x1, 2.0) + Math.Pow(line.y2 - line.y1, 2.0));
		}

		public static double GetSlope(LineSegment line)
		{
			return (line.y2 - line.y1) / (line.x2 - line.x1);
		}

		public Point[] GetBoundingBox()
		{
			return new Point[2]
			{
				new Point(Math.Min(start.x, end.x), Math.Min(start.y, end.y)),
				new Point(Math.Max(start.x, end.x), Math.Max(start.y, end.y))
			};
		}

		public static double CrossProduct(Point a, Point b)
		{
			return a.x * b.y - b.x * a.y;
		}

		public static bool DoBoundingBoxesIntersect(Point[] a, Point[] b)
		{
			return a[0].x <= b[1].x && a[1].x >= b[0].x && a[0].y <= b[1].y && a[1].y >= b[0].y;
		}

		public static bool IsPointOnLine(LineSegment a, Point b)
		{
			LineSegment lineSegment = new LineSegment(new Point(0.0, 0.0), new Point(a.end.x - a.start.x, a.end.y - a.start.y));
			Point b2 = new Point(b.x - a.start.x, b.y - a.start.y);
			double value = CrossProduct(lineSegment.end, b2);
			return Math.Abs(value) < 1E-06;
		}

		public static bool IsPointRightOfLine(LineSegment a, Point b)
		{
			LineSegment lineSegment = new LineSegment(new Point(0.0, 0.0), new Point(a.end.x - a.start.x, a.end.y - a.start.y));
			Point b2 = new Point(b.x - a.start.x, b.y - a.start.y);
			return CrossProduct(lineSegment.end, b2) < 0.0;
		}

		public static bool LineSegmentTouchesOrCrossesLine(LineSegment a, LineSegment b)
		{
			return IsPointOnLine(a, b.start) || IsPointOnLine(a, b.end) || (IsPointRightOfLine(a, b.start) ^ IsPointRightOfLine(a, b.end));
		}

		public static bool DoLinesIntersect(LineSegment a, LineSegment b)
		{
			Point[] boundingBox = a.GetBoundingBox();
			Point[] boundingBox2 = b.GetBoundingBox();
			return DoBoundingBoxesIntersect(boundingBox, boundingBox2) && LineSegmentTouchesOrCrossesLine(a, b) && LineSegmentTouchesOrCrossesLine(b, a);
		}

		private static bool IsRightEnd(double x, LineSegment l)
		{
			return x >= l.start.x && x >= l.end.x;
		}

		public static bool IsLeftBend(Point i, Point j, Point k)
		{
			Point point = new Point(i.x, i.y);
			Point point2 = new Point(j.x, j.y);
			Point point3 = new Point(k.x, k.y);
			point2.x -= point.x;
			point3.x -= point.x;
			point2.y -= point.y;
			point3.y -= point.y;
			LineSegment a = new LineSegment(point, point2);
			point3.x -= point2.x;
			point3.y -= point2.y;
			return !IsPointRightOfLine(a, point3) && !IsPointOnLine(a, point3);
		}

		public override string ToString()
		{
			return string.Concat("{", start, ", ", end, "}");
		}
	}
}
