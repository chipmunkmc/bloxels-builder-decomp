public enum BloxelsMigrationUserType
{
	unchosen = 0,
	atHome = 1,
	atSchool = 2,
	atHomeAndSchool = 3,
	asAnEducator = 4,
	other = 5
}
