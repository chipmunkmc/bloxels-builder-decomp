using UnityEngine;

public class BloxelsSceneManager : MonoBehaviour
{
	public static BloxelsSceneManager instance;

	public Object messagePrefab;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		messagePrefab = Resources.Load("Prefabs/UIPopupMessage");
	}

	public void Back()
	{
		SceneName currentScene = AppStateManager.instance.currentScene;
		SceneName previousScene = AppStateManager.instance.previousScene;
		KillAnyPopupMenus();
		if (SoundManager.instance.loopingSFXPlayer.isPlaying)
		{
			SoundManager.instance.StopLoopingSound();
		}
		if (AppStateManager.instance.currentScene != 0)
		{
			PlayerBankManager.instance.SaveCoins();
		}
		if ((bool)WorldWrapper.instance)
		{
			WorldWrapper.instance.Clear();
		}
		PreparePoolsForSceneChange();
		Application.LoadLevelAsync(SceneName.Loading.ToString());
		Application.LoadLevelAsync(previousScene.ToString());
	}

	public void GoTo(SceneName scene)
	{
		KillAnyPopupMenus();
		if (scene == SceneName.Viewer)
		{
			if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified)
			{
				ShowInternetMessage();
				return;
			}
			if (AppStateManager.instance.outDatedVersion)
			{
				ShowVersionUpdateMessage();
				return;
			}
		}
		if ((bool)GameplayBuilder.instance)
		{
			GameplayBuilder.instance.StopStreaming();
		}
		if ((bool)WorldWrapper.instance)
		{
			WorldWrapper.instance.Clear();
		}
		if (SoundManager.instance.loopingSFXPlayer.isPlaying)
		{
			SoundManager.instance.StopLoopingSound();
		}
		if (CurrentUser.instance.active)
		{
			CurrentUser.instance.Save();
		}
		PreparePoolsForSceneChange();
		if (AppStateManager.instance.currentScene != 0)
		{
			PlayerBankManager.instance.SaveCoins();
		}
		AppStateManager.instance.SetPreviousScene(AppStateManager.instance.currentScene);
		Application.LoadLevelAsync(SceneName.Loading.ToString());
		Application.LoadLevelAsync(scene.ToString());
	}

	public void GameplayWithGame(BloxelGame game, ServerSquare square = null)
	{
		if (AppStateManager.instance.currentScene != 0)
		{
			PlayerBankManager.instance.SaveCoins();
		}
		KillAnyPopupMenus();
		AppStateManager.instance.loadedGame = game;
		AppStateManager.instance.iWallGameSquare = square;
		GoTo(SceneName.Gameplay);
	}

	public static void KillAnyPopupMenus()
	{
		if (UIPopup.instance != null)
		{
			Object.Destroy(UIPopup.instance.gameObject);
		}
		if (UIPopupMenu.instance != null)
		{
			Object.Destroy(UIPopupMenu.instance.gameObject);
		}
		if (UIOverlayNavMenu.instance != null)
		{
			Object.Destroy(UIOverlayNavMenu.instance.gameObject);
		}
	}

	public void ShowInternetMessage()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(messagePrefab);
		UIPopupMessage component = gameObject.GetComponent<UIPopupMessage>();
		component.title = LocalizationManager.getInstance().getLocalizedText("misc15", "No Internet");
		component.description = LocalizationManager.getInstance().getLocalizedText("misc13", "Sorry, you must be connected to the internet to access this feature.");
	}

	public void ShowVersionUpdateMessage()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(Resources.Load("Prefabs/UIPopupMessage"));
		UIPopupMessage component = gameObject.GetComponent<UIPopupMessage>();
		component.title = "UPDATE";
		component.description = "You are using an outdated version of the app. To gain access to all areas, please update for free now.";
	}

	private void PreparePoolsForSceneChange()
	{
		if ((bool)GameplayPool.Instance)
		{
			GameplayPool.PrepareForSceneChange();
		}
		if ((bool)LevelBackgroundPool.instance)
		{
			LevelBackgroundPool.instance.PrepareForSceneChange();
		}
		if ((bool)TilePool.instance)
		{
			TilePool.instance.PrepareForSceneChange();
		}
		if ((bool)ConfigIndicatorPool.instance)
		{
			ConfigIndicatorPool.instance.PrepareForSceneChange();
		}
		if ((bool)NPCIndicatorPool.instance)
		{
			NPCIndicatorPool.instance.PrepareForSceneChange();
		}
		if ((bool)SpeakBubblePool.instance)
		{
			SpeakBubblePool.instance.PrepareForSceneChange();
		}
	}
}
