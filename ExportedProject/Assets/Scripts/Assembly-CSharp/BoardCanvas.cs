using DG.Tweening;
using UnityEngine;

public class BoardCanvas : UIMoveable
{
	public static BoardCanvas instance;

	public CanvasGroup uiGroupStartMenu;

	public UIButton uiButtonStart;

	public UIButton uiButtonEdit;

	private bool initComplete;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		base.Awake();
	}

	private new void Start()
	{
		BloxelBoardLibrary.instance.OnProjectSelect += LibraryItemChanged;
		BloxelBoardLibrary.instance.OnProjectDelete += LibraryItemDeleted;
		uiButtonStart.OnClick += BloxelBoardLibrary.instance.AddButtonPressed;
		uiButtonEdit.OnClick += ShowLibrary;
		ShowStart();
		initComplete = true;
	}

	private void OnDestroy()
	{
		BloxelBoardLibrary.instance.OnProjectSelect -= LibraryItemChanged;
		BloxelBoardLibrary.instance.OnProjectDelete -= LibraryItemDeleted;
		uiButtonStart.OnClick -= BloxelBoardLibrary.instance.AddButtonPressed;
		uiButtonEdit.OnClick -= ShowLibrary;
	}

	private void LibraryItemDeleted(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.PaintBoard)
		{
			ShowStart();
		}
	}

	public void LibraryItemChanged(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.PaintBoard)
		{
			HideStart();
		}
	}

	private void ShowLibrary()
	{
		BloxelLibrary.instance.libraryToggle.Show();
	}

	public new void Show()
	{
		base.Show();
		if (BloxelBoardLibrary.instance.currentSavedBoard != null)
		{
			PixelEditorPaintBoard.instance.SwitchBloxelBoard(BloxelBoardLibrary.instance.currentSavedBoard);
			HideStart();
		}
		else
		{
			ShowStart();
		}
	}

	public void ShowStart()
	{
		uiGroupStartMenu.gameObject.SetActive(true);
		uiGroupStartMenu.DOFade(1f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			PixelEditorPaintBoard.instance.Hide();
		});
	}

	public void HideStart(bool showPaintBoard = true)
	{
		uiGroupStartMenu.DOFade(0f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			uiGroupStartMenu.gameObject.SetActive(false);
			if (showPaintBoard)
			{
				PixelEditorPaintBoard.instance.Show();
			}
		});
	}

	public void CreateNewBoard()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmA);
		BloxelBoard bloxelBoard = new BloxelBoard();
		PixelEditorPaintBoard.instance.Clear();
		PixelEditorPaintBoard.instance.currentBloxelBoard = bloxelBoard;
		BloxelBoardLibrary.instance.AddBoard(bloxelBoard);
	}
}
