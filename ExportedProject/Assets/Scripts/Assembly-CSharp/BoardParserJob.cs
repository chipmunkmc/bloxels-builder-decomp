using System;
using System.Threading;

public class BoardParserJob : ThreadedJob
{
	public delegate void HandleParseComplete(BloxelBoard _board);

	public bool isRunning;

	public string responseFromServer;

	public BloxelBoard board;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		board = new BloxelBoard(responseFromServer, DataSource.JSONString);
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(board);
		}
	}
}
