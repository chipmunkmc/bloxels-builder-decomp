public enum BoardValidationStep
{
	Splash = 0,
	Instructions = 1,
	Capture = 2,
	Validation = 3
}
