using DG.Tweening;
using UnityEngine;

public class BoardVerificationCapture : BoardVerificationStep
{
	[Header("UI")]
	public Ease animateIn;

	public Ease animateOut;

	public float offsetAmount = 500f;

	private new void Awake()
	{
		base.Awake();
		if (uiButtonNext != null)
		{
			uiButtonNext.OnClick += NextPressed;
		}
		SetupItems();
	}

	private void OnDestroy()
	{
		if (uiButtonNext != null)
		{
			uiButtonNext.OnClick -= NextPressed;
		}
	}

	private new void SetupItems()
	{
		base.SetupItems();
	}

	public override void Activate()
	{
		base.Activate();
		ShowItems();
	}

	public override void DeActivate()
	{
		base.DeActivate();
		HideItems();
	}

	private Tweener ShowItems()
	{
		controller.overlay.enabled = false;
		controller.rectBackground.DOScale(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			controller.rectTitleBar.transform.DOScale(0f, UIAnimationManager.speedFast);
			controller.uiButtonDismiss.transform.DOScale(0f, UIAnimationManager.speedFast);
		}).OnComplete(delegate
		{
			CaptureManager.instance.Show();
			controller.transform.SetAsLastSibling();
		});
		return uiGroup.DOFade(1f, UIAnimationManager.speedSlow).OnStart(delegate
		{
			uiGroup.blocksRaycasts = true;
			uiGroup.interactable = true;
			controller.boardContainer.DOAnchorPos(controller.startingBoardPosition, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn).OnStart(delegate
			{
				controller.boardImage.DOFade(1f, UIAnimationManager.speedMedium);
			});
			float reScale = 0.33f;
			float paddingX = 60f;
			float num = 40f;
			controller.boardContainer.DOScale(new Vector3(reScale, reScale, 1f), UIAnimationManager.speedMedium).OnComplete(delegate
			{
				Vector2 endValue = new Vector2(0f - (controller.panelTransform.rect.width / 2f + controller.boardContainer.sizeDelta.x * reScale - paddingX + controller.boardContainer.anchoredPosition.x), -16f);
				controller.boardContainer.DOAnchorPos(endValue, UIAnimationManager.speedSlow).OnComplete(delegate
				{
					controller.boardBorder.DOFade(1f, UIAnimationManager.speedMedium);
				});
			});
		});
	}

	private Tweener HideItems()
	{
		return uiGroup.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroup.blocksRaycasts = false;
			uiGroup.interactable = false;
		});
	}

	private void NextPressed()
	{
		controller.ShowValidationPanelAtStep(BoardValidationStep.Validation);
	}
}
