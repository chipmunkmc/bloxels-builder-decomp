using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BoardVerificationInstructions : BoardVerificationStep
{
	[Header("UI")]
	public RectTransform rectTitle;

	public RectTransform rectBody;

	private Text uiTextTitle;

	private Text uiTextBody;

	private Vector2 positionTitle;

	private Vector2 positionBody;

	public Tweener itemAnimation;

	public Ease animateIn;

	public Ease animateOut;

	public float offsetAmount = 500f;

	private new void Awake()
	{
		base.Awake();
		positionTitle = rectTitle.anchoredPosition;
		positionBody = rectBody.anchoredPosition;
		uiTextTitle = rectTitle.GetComponent<Text>();
		uiTextBody = rectBody.GetComponent<Text>();
		uiButtonNext.OnClick += NextPressed;
		SetupItems();
	}

	private void OnDestroy()
	{
		uiButtonNext.OnClick -= NextPressed;
	}

	private new void SetupItems()
	{
		base.SetupItems();
		rectTitle.anchoredPosition = new Vector2(rectTitle.anchoredPosition.x + offsetAmount, rectTitle.anchoredPosition.y);
		rectBody.anchoredPosition = new Vector2(rectBody.anchoredPosition.x + offsetAmount, rectBody.anchoredPosition.y);
		rectButtonNext.anchoredPosition = new Vector2(rectButtonNext.anchoredPosition.x, rectButtonNext.anchoredPosition.y - offsetAmount);
	}

	public override void Activate()
	{
		base.Activate();
		ShowItems();
	}

	public override void DeActivate()
	{
		base.DeActivate();
		HideItems();
	}

	private Tweener ShowItems()
	{
		controller.overlay.enabled = true;
		controller.boardContainer.localScale = Vector3.one;
		controller.boardContainer.anchoredPosition = new Vector2(controller.boardContainer.anchoredPosition.x - offsetAmount, controller.boardContainer.anchoredPosition.y);
		return uiGroup.DOFade(1f, UIAnimationManager.speedSlow).OnStart(delegate
		{
			uiGroup.blocksRaycasts = true;
			uiGroup.interactable = true;
			controller.boardContainer.DOAnchorPos(controller.startingBoardPosition, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn).OnStart(delegate
			{
				SoundManager.instance.PlayDelayedSound(UIAnimationManager.speedFast, SoundManager.instance.appearance);
				controller.boardImage.DOFade(1f, UIAnimationManager.speedMedium);
			});
			rectTitle.DOAnchorPos(positionTitle, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn);
			rectBody.DOAnchorPos(positionBody, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn);
			rectButtonNext.DOAnchorPos(positionButtonNext, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn).OnComplete(delegate
			{
				rectButtonNext.DOShakeScale(UIAnimationManager.speedFast, 0.5f, 5, 45f).OnStart(delegate
				{
					SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
				});
			});
		});
	}

	private Tweener HideItems()
	{
		return uiGroup.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroup.blocksRaycasts = false;
			uiGroup.interactable = false;
			rectTitle.DOAnchorPos(new Vector2(rectTitle.anchoredPosition.x + offsetAmount, rectTitle.anchoredPosition.y), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
			rectBody.DOAnchorPos(new Vector2(rectBody.anchoredPosition.x + offsetAmount, rectBody.anchoredPosition.y), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
			rectButtonNext.DOAnchorPos(new Vector2(rectButtonNext.anchoredPosition.x, rectButtonNext.anchoredPosition.y - offsetAmount), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
		});
	}

	private void NextPressed()
	{
		controller.ShowValidationPanelAtStep(BoardValidationStep.Capture);
	}
}
