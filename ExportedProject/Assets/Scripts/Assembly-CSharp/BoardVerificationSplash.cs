using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class BoardVerificationSplash : BoardVerificationStep
{
	[Header("UI")]
	public RectTransform rectButtonNo;

	public RectTransform rectButtonInfo;

	public RectTransform rectTitle;

	public RectTransform rectBody;

	public RectTransform rectImage;

	public RectTransform rectEducators;

	public UIGemEarn uiGemEarn;

	private UIButton uiButtonNo;

	private Text uiTextTitle;

	private Text uiTextBody;

	private Image uiImage;

	private Vector2 positionButtonNo;

	private Vector2 positionButtonInfo;

	private Vector2 positionTitle;

	private Vector2 positionBody;

	private Vector2 positionImage;

	public Tweener itemAnimation;

	public Ease animateIn;

	public Ease animateOut;

	public float offsetAmount = 500f;

	private new void Awake()
	{
		base.Awake();
		positionButtonNo = rectButtonNo.anchoredPosition;
		positionButtonInfo = rectButtonInfo.anchoredPosition;
		positionTitle = rectTitle.anchoredPosition;
		positionBody = rectBody.anchoredPosition;
		positionImage = rectImage.anchoredPosition;
		uiButtonNo = rectButtonNo.GetComponent<UIButton>();
		uiTextTitle = rectTitle.GetComponent<Text>();
		uiTextBody = rectBody.GetComponent<Text>();
		uiImage = rectImage.GetComponent<Image>();
		uiButtonNo.OnClick += DismissPressed;
		uiButtonNext.OnClick += NextPressed;
		SetupItems();
	}

	private void OnDestroy()
	{
		uiButtonNo.OnClick -= DismissPressed;
		uiButtonNext.OnClick -= NextPressed;
	}

	private new void SetupItems()
	{
		base.SetupItems();
		rectTitle.anchoredPosition = new Vector2(rectTitle.anchoredPosition.x + offsetAmount, rectTitle.anchoredPosition.y);
		rectBody.anchoredPosition = new Vector2(rectBody.anchoredPosition.x + offsetAmount, rectBody.anchoredPosition.y);
		rectImage.anchoredPosition = new Vector2(rectImage.anchoredPosition.x - offsetAmount, rectImage.anchoredPosition.y);
		rectButtonNext.anchoredPosition = new Vector2(rectButtonNext.anchoredPosition.x, rectButtonNext.anchoredPosition.y - offsetAmount);
		rectButtonNo.anchoredPosition = new Vector2(rectButtonNo.anchoredPosition.x, rectButtonNo.anchoredPosition.y - offsetAmount);
		uiGemEarn.selfRect.localScale = Vector3.zero;
		rectEducators.localScale = Vector3.zero;
	}

	public override void Activate()
	{
		base.Activate();
		ShowItems();
	}

	public override void DeActivate()
	{
		base.DeActivate();
		HideItems();
	}

	private Tweener ShowItems()
	{
		controller.ResetPreviewBoard();
		controller.boardBorder.DOFade(0f, 0f);
		controller.rectBackground.localScale = Vector3.one;
		controller.uiButtonDismiss.transform.localScale = Vector3.one;
		controller.rectTitleBar.transform.localScale = Vector3.one;
		return uiGroup.DOFade(1f, UIAnimationManager.speedSlow).OnStart(delegate
		{
			uiGroup.blocksRaycasts = true;
			uiGroup.interactable = true;
			rectImage.DOAnchorPos(positionImage, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn).OnStart(delegate
			{
				SoundManager.instance.PlayDelayedSound(UIAnimationManager.speedFast, SoundManager.instance.appearance);
			});
			rectTitle.DOAnchorPos(positionTitle, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn);
			rectBody.DOAnchorPos(positionBody, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn);
			rectButtonNext.DOAnchorPos(positionButtonNext, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn).OnComplete(delegate
			{
				rectButtonNext.DOShakeScale(UIAnimationManager.speedFast, 0.5f, 5, 45f).OnStart(delegate
				{
					SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
				});
			});
			rectButtonNo.DOAnchorPos(positionButtonNo, Random.Range(UIAnimationManager.speedMedium, UIAnimationManager.speedSlow)).SetEase(animateIn).OnComplete(delegate
			{
				rectButtonNo.DOShakeScale(UIAnimationManager.speedFast, 0.5f, 5, 45f).OnStart(delegate
				{
					SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
				});
				uiGemEarn.selfRect.DOScale(0.8f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce);
				rectEducators.DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.InOutBounce);
			});
		});
	}

	private Tweener HideItems()
	{
		return uiGroup.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroup.blocksRaycasts = false;
			uiGroup.interactable = false;
			rectTitle.DOAnchorPos(new Vector2(rectTitle.anchoredPosition.x + offsetAmount, rectTitle.anchoredPosition.y), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
			rectBody.DOAnchorPos(new Vector2(rectBody.anchoredPosition.x + offsetAmount, rectBody.anchoredPosition.y), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
			rectButtonNext.DOAnchorPos(new Vector2(rectButtonNext.anchoredPosition.x, rectButtonNext.anchoredPosition.y - offsetAmount), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
			rectButtonNo.DOAnchorPos(new Vector2(rectButtonNo.anchoredPosition.x, rectButtonNo.anchoredPosition.y - offsetAmount), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
			rectImage.DOAnchorPos(new Vector2(rectImage.anchoredPosition.x - offsetAmount, rectImage.anchoredPosition.y), Random.Range(UIAnimationManager.speedFast, UIAnimationManager.speedMedium)).SetEase(animateOut);
			rectEducators.DOScale(0f, UIAnimationManager.speedFast).SetEase(animateOut);
		});
	}

	private void DismissPressed()
	{
		HideItems().OnComplete(delegate
		{
			controller.Dismiss();
		});
	}

	private void NextPressed()
	{
		controller.ShowValidationPanelAtStep(BoardValidationStep.Instructions);
	}
}
