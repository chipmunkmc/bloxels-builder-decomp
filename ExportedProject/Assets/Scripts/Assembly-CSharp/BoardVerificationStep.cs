using UnityEngine;

public class BoardVerificationStep : MonoBehaviour
{
	public BoardValidationStep step;

	public RectTransform rectButtonNext;

	[Header("Dynamically Set")]
	public UIPopupBoardActivation controller;

	public RectTransform rect;

	public CanvasGroup uiGroup;

	public UIButton uiButtonNext;

	public Vector2 positionButtonNext;

	public bool isActive;

	public void Awake()
	{
		controller = GetComponentInParent<UIPopupBoardActivation>();
		rect = GetComponent<RectTransform>();
		uiGroup = GetComponent<CanvasGroup>();
		if (rectButtonNext != null)
		{
			uiButtonNext = rectButtonNext.GetComponent<UIButton>();
			positionButtonNext = rectButtonNext.anchoredPosition;
		}
	}

	public void SetupItems()
	{
		uiGroup.alpha = 0f;
		uiGroup.blocksRaycasts = false;
		uiGroup.interactable = false;
	}

	public virtual void Activate()
	{
		if (!isActive)
		{
			isActive = true;
		}
	}

	public virtual void DeActivate()
	{
		if (isActive)
		{
			isActive = false;
		}
	}
}
