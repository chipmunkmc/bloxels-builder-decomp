using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class BoardVerificationValidation : BoardVerificationStep
{
	[Header("UI")]
	public RawImage capturedPreview;

	public RectTransform transformTextSuccess;

	public TextMeshProUGUI uiTextSuccess;

	public UIButton uiButtonTryAgain;

	public UIButton uiButtonRefresh;

	public CoverBoard capturedPreviewCover;

	public static BloxelBoard capturedPreviewBoard;

	public Ease animateIn;

	public Ease animateOut;

	public float offsetAmount = 500f;

	[Header("Particles")]
	public ParticleSystem confettiCannon;

	public ParticleSystem gemBarf;

	[Header("Gems")]
	public RectTransform rectGemBank;

	public RectTransform rectBankCount;

	public TextMeshProUGUI uiTextCount;

	public RectTransform rectIncrementCount;

	public TextMeshProUGUI uiTextIncrementAmount;

	private new void Awake()
	{
		base.Awake();
		if (uiButtonNext != null)
		{
			uiButtonNext.OnClick += NextPressed;
		}
		uiButtonTryAgain.OnClick += TryAgain;
		uiButtonRefresh.OnClick += Refresh;
		capturedPreviewBoard = new BloxelBoard();
		capturedPreviewCover = new CoverBoard(new CoverStyle(BloxelBoard.baseUIBoardColor, 4, 2, 1, true));
		capturedPreview.texture = capturedPreviewBoard.UpdateTexture2D(ref capturedPreviewCover.colors, ref capturedPreviewCover.texture, capturedPreviewCover.style);
		SetupItems();
	}

	private void OnDestroy()
	{
		if (uiButtonNext != null)
		{
			uiButtonNext.OnClick -= NextPressed;
		}
		uiButtonTryAgain.OnClick -= TryAgain;
		uiButtonRefresh.OnClick -= Refresh;
	}

	private new void SetupItems()
	{
		base.SetupItems();
		transformTextSuccess.localScale = Vector3.zero;
		uiButtonTryAgain.transform.localScale = Vector3.zero;
		rectGemBank.localScale = Vector3.zero;
		rectIncrementCount.localScale = Vector3.zero;
		uiTextIncrementAmount.SetText("+" + GemEarnable.valueLookup[GemEarnable.Type.BoardVerify]);
		uiTextCount.SetText(PlayerBankManager.instance.GetGemCount().ToString());
	}

	public override void Activate()
	{
		base.Activate();
		ShowItems();
	}

	public override void DeActivate()
	{
		base.DeActivate();
		HideItems();
	}

	private Tweener ShowItems()
	{
		controller.overlay.enabled = true;
		controller.boardBorder.DOFade(0f, UIAnimationManager.speedFast);
		controller.rectBackground.DOScale(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			controller.uiButtonDismiss.transform.DOScale(1f, UIAnimationManager.speedFast);
			controller.rectTitleBar.transform.DOScale(1f, UIAnimationManager.speedFast);
		});
		capturedPreviewBoard.blockColors = CaptureManager.latest;
		capturedPreview.texture = capturedPreviewBoard.UpdateTexture2D(ref capturedPreviewCover.colors, ref capturedPreviewCover.texture, capturedPreviewCover.style);
		return uiGroup.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroup.blocksRaycasts = true;
			uiGroup.interactable = true;
			controller.boardContainer.DOScale(0.8f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				controller.boardContainer.DOAnchorPos(new Vector2(0f, 40f), UIAnimationManager.speedSlow).SetEase(animateIn).OnComplete(delegate
				{
					if (AppStateManager.instance.userOwnsBoard)
					{
						StartCoroutine("Success");
					}
					else
					{
						Fail();
					}
				});
			});
		});
	}

	private Tweener HideItems()
	{
		return uiGroup.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroup.blocksRaycasts = false;
			uiGroup.interactable = false;
			transformTextSuccess.localScale = Vector3.zero;
			uiButtonTryAgain.transform.localScale = Vector3.zero;
			uiButtonRefresh.transform.localScale = Vector3.zero;
		});
	}

	private IEnumerator Success()
	{
		uiButtonTryAgain.transform.localScale = Vector3.zero;
		uiButtonRefresh.transform.localScale = Vector3.zero;
		int gemStart = PlayerBankManager.instance.GetGemCount();
		int loot = GemEarnable.valueLookup[GemEarnable.Type.BoardVerify];
		GemManager.instance.AddGemsForType(GemEarnable.Type.BoardVerify);
		GemManager.instance.BulkRoomUnlock();
		controller.boardContainer.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), UIAnimationManager.speedMedium).SetLoops(2, LoopType.Yoyo);
		yield return new WaitForSeconds(UIAnimationManager.speedMedium * 2f);
		confettiCannon.Play();
		SoundManager.instance.PlaySound(SoundManager.instance.verifySuccess);
		transformTextSuccess.DOScale(new Vector3(0.9f, 0.9f, 1f), UIAnimationManager.speedMedium);
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		rectGemBank.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		rectIncrementCount.DOScale(1f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			rectIncrementCount.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f);
		});
		gemBarf.Play();
		SoundManager.PlayOneShot(SoundManager.instance.coinReward);
		int chunk = 10;
		int iterator = loot / chunk;
		for (int i = 0; i < iterator; i++)
		{
			gemStart += chunk;
			string inc = gemStart.ToString();
			uiTextCount.text = inc;
			yield return new WaitForSeconds(gemBarf.duration / (float)iterator);
		}
		StartCoroutine("Transition");
	}

	private void Fail()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.popB);
		controller.boardImage.DOColor(AssetManager.instance.bloxelRed, UIAnimationManager.speedFast).SetLoops(4, LoopType.Yoyo);
		uiButtonTryAgain.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(animateIn).OnStart(delegate
		{
			uiButtonRefresh.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(animateIn);
		})
			.OnComplete(delegate
			{
				uiButtonTryAgain.transform.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 2, 20f).OnComplete(delegate
				{
					uiButtonRefresh.transform.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 2, 20f);
				});
			});
	}

	private void TryAgain()
	{
		controller.ShowValidationPanelAtStep(BoardValidationStep.Capture);
	}

	private void Refresh()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmC);
		controller.RefreshBoard();
	}

	private IEnumerator Transition()
	{
		StopCoroutine("Success");
		yield return new WaitForSeconds(controller.transitionDelay);
		controller.Dismiss(false).OnComplete(delegate
		{
			UIOverlayCanvas.instance.Popup(UIOverlayCanvas.instance.gemInfoPrefab);
			Object.Destroy(controller.gameObject);
		});
	}

	private void NextPressed()
	{
		controller.ShowValidationPanelAtStep(BoardValidationStep.Validation);
	}
}
