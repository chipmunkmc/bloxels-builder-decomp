using System.Collections;
using DG.Tweening;
using UnityEngine;

public class BoardZoomManager : MonoBehaviour
{
	public static BoardZoomManager instance;

	private PrefsManager _prefsManager;

	public GameObject toolsUnderLayer;

	public GameObject boardPreviewUnderLayer;

	public GameObject topToolBar;

	public GameObject bottomModesMenuBar;

	public GameObject navMenuButton;

	public GameObject captureButton;

	public GameObject mapButtons;

	public GameObject copyButton;

	public GameObject pasteButton;

	public GameObject eraseButton;

	public GameObject boardPreview;

	public GameObject paintBoardTools;

	public GameObject gemAmountContainer;

	public UITransparentClickableLayer eraserTransparentLayer;

	public UITransparentClickableLayer copyTransparentLayer;

	public UITransparentClickableLayer pasteTransparentLayer;

	public MapZoomToggle mapZoomToggle;

	public UIMoveable characterBuilderModesBar;

	public AnimationTimeline animationTimelineBar;

	private BloxelLibrary _library;

	private PixelToolPalette _pixelToolPalette;

	private PixelEditorPaintBoard _pixelEditorPaintBoard;

	private Vector2 _initialToolsUnderLayerPosition;

	private Vector2 _initialBoardPreviewUnderLayerPosition;

	private Vector2 _initialPaletteOpenPosition;

	private Vector2 _initialPaletteStartingPosition;

	private Vector2 _initialBoardBasePosition;

	private Vector2 _initialBoardLeftPosition;

	private Vector2 _initialCharacterBuilderModesBarPosition;

	private Vector2 _initialToolTransparentLayerDimensions;

	private Vector2 _zoomToolTransparentLayerDimensions;

	private Vector3 _initialTopToolBarPosition;

	private Vector3 _initialBottomModesMenuBarPosition;

	private Vector3 _initialAnimationTimelineBarPosition;

	private Vector3 _initialNavMenuButtonPosition;

	private Vector3 _initialCaptureButtonPosition;

	private Vector3 _initialPixelsToolsPalettePosition;

	private Vector3 _initialPixelsPaintBoardPosition;

	private Vector3 _initialBoardOutputAreaPosition;

	private Vector3 _initialMapButtonsPosition;

	private Vector3 _initialLibraryPosition;

	private Vector3 _initialCopyButtonPosition;

	private Vector3 _initialPasteButtonPosition;

	private Vector3 _initialEraseButtonPosition;

	private Vector3 _initialGemAmountContainerPosition;

	private Vector3 _initialBoardPreviewPosition;

	public bool is16by10OrWider;

	public bool isZoomed;

	public bool canZoom;

	public float zoomSpeed = UIAnimationManager.speedMedium;

	public Vector3 boardToolsSize = new Vector3(1.5f, 1.5f, 1f);

	private GestureTutorialManager _gestureTutorialManager;

	private UIOverlayCanvas _uiOverlayCanvas;

	public float TestFloat1;

	public float TestFloat2;

	public float TestFloat3;

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
		}
		else
		{
			instance = this;
		}
	}

	private void Start()
	{
		_prefsManager = PrefsManager.instance;
		_pixelToolPalette = PixelToolPalette.instance;
		_pixelEditorPaintBoard = PixelEditorPaintBoard.instance;
		_uiOverlayCanvas = UIOverlayCanvas.instance;
		_gestureTutorialManager = GestureTutorialManager.instance;
		_library = BloxelLibrary.instance;
		DetermineIfDeviceIsCandidateForZoomageDependingOnItsAspectRatio();
		EstablishNewInitialScaleAndPositionForApplicableGameObjects();
		isZoomed = false;
	}

	public void StartCanZoomBuffer()
	{
		StartCoroutine(zoomBufferCo());
	}

	private IEnumerator zoomBufferCo()
	{
		canZoom = false;
		yield return new WaitForSeconds(zoomSpeed * 1.5f);
		canZoom = true;
	}

	public void StartCanZoomBuffer(float waitTime)
	{
		StartCoroutine(zoomBufferCo(waitTime));
	}

	private IEnumerator zoomBufferCo(float waitTime)
	{
		canZoom = false;
		yield return new WaitForSeconds(waitTime);
		canZoom = true;
	}

	public void ZoomInOnGameBoardIfZoomable()
	{
		if (!isZoomed && CanZoomOnPixelBoard())
		{
			SoundManager.PlaySoundClip(SoundClip.sweepA);
			DetermineInitialPositioningOfGameObjects();
			topToolBar.transform.DOLocalMoveY(_initialTopToolBarPosition.y + 120f, zoomSpeed).SetEase(Ease.OutBack);
			navMenuButton.transform.DOLocalMoveY(_initialNavMenuButtonPosition.y + 120f, zoomSpeed).SetEase(Ease.OutBack);
			bottomModesMenuBar.transform.DOLocalMoveY(_initialBottomModesMenuBarPosition.y - 120f, zoomSpeed).SetEase(Ease.OutBack);
			animationTimelineBar.transform.DOLocalMoveY(_initialAnimationTimelineBarPosition.y - 450f, zoomSpeed).SetEase(Ease.OutBack);
			captureButton.transform.DOLocalMoveY(_initialCaptureButtonPosition.y - 360f, zoomSpeed).SetEase(Ease.OutBack);
			mapButtons.transform.DOLocalMoveX(_initialMapButtonsPosition.x - 460f, zoomSpeed).SetEase(Ease.OutBack);
			_library.transform.DOLocalMoveX(_initialLibraryPosition.x - 360f, zoomSpeed).SetEase(Ease.OutBack);
			gemAmountContainer.transform.DOLocalMoveX(_initialGemAmountContainerPosition.x - 360f, zoomSpeed).SetEase(Ease.OutBack);
			if (PixelEditorController.instance.mode.Equals(CanvasMode.CharacterBuilder))
			{
				characterBuilderModesBar.transform.DOLocalMoveX(_initialCharacterBuilderModesBarPosition.x - 360f, zoomSpeed).SetEase(Ease.OutBack);
			}
			ExpandPixelsToolsPalette();
			ExpandPixelsPaintBoard();
			isZoomed = true;
			StartCanZoomBuffer();
			if (!_prefsManager.hasSeenPinchZoomTutorial)
			{
				_prefsManager.hasSeenPinchZoomTutorial = true;
			}
		}
	}

	public void ZoomOutOnGameBoardIfZoomable()
	{
		if (isZoomed && CanZoomOnPixelBoard())
		{
			SoundManager.PlaySoundClip(SoundClip.sweepA);
			topToolBar.transform.DOLocalMoveY(_initialTopToolBarPosition.y, zoomSpeed).SetEase(Ease.OutQuad);
			navMenuButton.transform.DOLocalMoveY(_initialNavMenuButtonPosition.y, zoomSpeed).SetEase(Ease.OutQuad);
			animationTimelineBar.transform.DOLocalMoveY(_initialAnimationTimelineBarPosition.y, zoomSpeed).SetEase(Ease.OutQuad);
			bottomModesMenuBar.transform.DOLocalMoveY(_initialBottomModesMenuBarPosition.y, zoomSpeed).SetEase(Ease.OutQuad);
			captureButton.transform.DOLocalMoveY(_initialCaptureButtonPosition.y, zoomSpeed).SetEase(Ease.OutBack);
			mapButtons.transform.DOLocalMoveX(_initialMapButtonsPosition.x, zoomSpeed).SetEase(Ease.OutQuad);
			_library.transform.DOLocalMoveX(_initialLibraryPosition.x, zoomSpeed).SetEase(Ease.OutQuad);
			gemAmountContainer.transform.DOLocalMoveX(_initialGemAmountContainerPosition.x, zoomSpeed).SetEase(Ease.OutQuad);
			if (PixelEditorController.instance.mode.Equals(CanvasMode.CharacterBuilder))
			{
				characterBuilderModesBar.transform.DOLocalMoveX(_initialCharacterBuilderModesBarPosition.x, zoomSpeed).SetEase(Ease.OutBack);
			}
			ShrinkPixelsToolsPalette();
			ShrinkPixelsPaintBoard();
			isZoomed = false;
			StartCanZoomBuffer();
		}
	}

	public void ExpandPixelsToolsPalette()
	{
		ShortcutExtensions.DOLocalMove(endValue: new Vector3(_initialPixelsToolsPalettePosition.x - 100f, _initialPixelsToolsPalettePosition.y + 108f, 0f), target: _pixelToolPalette.transform, duration: zoomSpeed).SetEase(Ease.OutBack);
		_pixelToolPalette.transform.DOScale(2.1f, zoomSpeed).SetEase(Ease.OutBack);
		MoveToolsBlocksToTheLeftOfTheColorBlocks();
		ShortcutExtensions.DOLocalMove(endValue: new Vector3(_initialBoardPreviewPosition.x + 350f, _initialBoardPreviewPosition.y + 350f, 0f), target: boardPreview.transform, duration: zoomSpeed);
		boardPreviewUnderLayer.transform.DOScale(1f, zoomSpeed);
		CompensateForPalleteOffsetOnZoom();
	}

	public void ShrinkPixelsToolsPalette()
	{
		MoveToolsBlocksUnderTheColorBlocks();
		_pixelToolPalette.transform.DOLocalMove(_initialPixelsToolsPalettePosition, zoomSpeed).SetEase(Ease.OutBack);
		_pixelToolPalette.transform.DOScale(1f, zoomSpeed).SetEase(Ease.OutBack);
		boardPreview.transform.localPosition = _initialBoardPreviewPosition;
		boardPreviewUnderLayer.transform.DOScale(0f, zoomSpeed);
		CompensateForPalleteOffsetOnUnZoom();
	}

	public void MoveToolsBlocksToTheLeftOfTheColorBlocks()
	{
		ShortcutExtensions.DOLocalMove(endValue: new Vector3(_initialCopyButtonPosition.x - 75f, _initialCopyButtonPosition.y + 227f, 0f), target: copyButton.transform, duration: zoomSpeed).SetEase(Ease.OutBack);
		ShortcutExtensions.DOLocalMove(endValue: new Vector3(_initialPasteButtonPosition.x - 75f, _initialPasteButtonPosition.y + 230f, 0f), target: pasteButton.transform, duration: zoomSpeed).SetEase(Ease.OutBack);
		ShortcutExtensions.DOLocalMove(endValue: new Vector3(_initialEraseButtonPosition.x - 75f, _initialEraseButtonPosition.y + 223f, 0f), target: eraseButton.transform, duration: zoomSpeed).SetEase(Ease.OutBack);
		ShortcutExtensions.DOLocalMove(endValue: new Vector3(_initialToolsUnderLayerPosition.x - 75f, _initialToolsUnderLayerPosition.y + 170f, 0f), target: toolsUnderLayer.transform, duration: zoomSpeed).SetEase(Ease.OutBack);
		_zoomToolTransparentLayerDimensions = new Vector2(_initialToolTransparentLayerDimensions.x - 25f, _initialToolTransparentLayerDimensions.y);
		copyTransparentLayer.GetComponent<RectTransform>().sizeDelta = _zoomToolTransparentLayerDimensions;
		pasteTransparentLayer.GetComponent<RectTransform>().sizeDelta = _zoomToolTransparentLayerDimensions;
		eraserTransparentLayer.GetComponent<RectTransform>().sizeDelta = _zoomToolTransparentLayerDimensions;
	}

	public void MoveToolsBlocksUnderTheColorBlocks()
	{
		copyButton.transform.DOLocalMove(_initialCopyButtonPosition, zoomSpeed).SetEase(Ease.OutBack);
		copyTransparentLayer.GetComponent<RectTransform>().sizeDelta = _initialToolTransparentLayerDimensions;
		pasteButton.transform.DOLocalMove(_initialPasteButtonPosition, zoomSpeed).SetEase(Ease.OutBack);
		pasteTransparentLayer.GetComponent<RectTransform>().sizeDelta = _initialToolTransparentLayerDimensions;
		eraseButton.transform.DOLocalMove(_initialEraseButtonPosition, zoomSpeed).SetEase(Ease.OutBack);
		eraserTransparentLayer.GetComponent<RectTransform>().sizeDelta = _initialToolTransparentLayerDimensions;
		toolsUnderLayer.transform.DOLocalMove(_initialToolsUnderLayerPosition, zoomSpeed).SetEase(Ease.OutBack);
		toolsUnderLayer.transform.DOScaleY(1f, zoomSpeed);
	}

	public void ExpandPixelsPaintBoard()
	{
		ShortcutExtensions.DOLocalMove(endValue: new Vector3(_initialPixelsPaintBoardPosition.x, _initialPixelsPaintBoardPosition.y + 130f, 0f), target: _pixelEditorPaintBoard.transform, duration: zoomSpeed).SetEase(Ease.OutBack);
		_pixelEditorPaintBoard.transform.DOScale(1.3f, zoomSpeed).SetEase(Ease.OutBack);
	}

	public void ShrinkPixelsPaintBoard()
	{
		_pixelEditorPaintBoard.transform.DOLocalMove(_initialPixelsPaintBoardPosition, zoomSpeed).SetEase(Ease.OutBack);
		_pixelEditorPaintBoard.transform.DOScale(1f, zoomSpeed).SetEase(Ease.OutBack);
	}

	public bool CanZoomOnPixelBoard()
	{
		if (!is16by10OrWider || !canZoom || !_pixelEditorPaintBoard.isVisible || _library.isVisible || _pixelToolPalette.isOpen || _uiOverlayCanvas.transform.childCount > _uiOverlayCanvas.staticChildCount || _pixelEditorPaintBoard.rect.anchoredPosition.y != _pixelEditorPaintBoard.basePosition.y)
		{
			return false;
		}
		return true;
	}

	public void DetermineInitialPositioningOfGameObjects()
	{
		_initialTopToolBarPosition = topToolBar.transform.localPosition;
		_initialBottomModesMenuBarPosition = bottomModesMenuBar.transform.localPosition;
		_initialAnimationTimelineBarPosition = animationTimelineBar.transform.localPosition;
		_initialNavMenuButtonPosition = navMenuButton.transform.localPosition;
		_initialCaptureButtonPosition = captureButton.transform.localPosition;
		_initialMapButtonsPosition = mapButtons.transform.localPosition;
		_initialLibraryPosition = _library.transform.localPosition;
		_initialCharacterBuilderModesBarPosition = characterBuilderModesBar.transform.localPosition;
		_initialToolTransparentLayerDimensions = eraserTransparentLayer.GetComponent<RectTransform>().sizeDelta;
		_initialPixelsToolsPalettePosition = _pixelToolPalette.GetComponent<RectTransform>().localPosition;
		_initialToolsUnderLayerPosition = toolsUnderLayer.GetComponent<RectTransform>().localPosition;
		_initialCopyButtonPosition = copyButton.GetComponent<RectTransform>().localPosition;
		_initialPasteButtonPosition = pasteButton.GetComponent<RectTransform>().localPosition;
		_initialEraseButtonPosition = eraseButton.GetComponent<RectTransform>().localPosition;
		_initialPixelsPaintBoardPosition = _pixelEditorPaintBoard.GetComponent<RectTransform>().localPosition;
		_initialGemAmountContainerPosition = gemAmountContainer.transform.localPosition;
		_initialBoardPreviewPosition = boardPreview.GetComponent<RectTransform>().localPosition;
	}

	public void DetermineInitialScaleOfGameObjects()
	{
	}

	public void DetermineIfDeviceIsCandidateForZoomageDependingOnItsAspectRatio()
	{
		if ((double)Camera.main.aspect >= 1.6)
		{
			canZoom = true;
			is16by10OrWider = true;
		}
		else
		{
			canZoom = false;
			is16by10OrWider = false;
		}
	}

	public void CompensateForPalleteOffsetOnZoom()
	{
		_initialPaletteOpenPosition = _pixelToolPalette.openPosition;
		_initialPaletteStartingPosition = _pixelToolPalette.startingPosition;
		_initialBoardBasePosition = _pixelEditorPaintBoard.basePosition;
		_initialBoardLeftPosition = _pixelEditorPaintBoard.leftPosition;
		_pixelToolPalette.openPosition = new Vector2(_pixelToolPalette.openPosition.x - 1150f, _pixelToolPalette.openPosition.y + 108f);
		_pixelToolPalette.startingPosition = new Vector2(_pixelToolPalette.startingPosition.x - 100f, _pixelToolPalette.startingPosition.y + 108f);
		_pixelEditorPaintBoard.basePosition = new Vector2(_pixelEditorPaintBoard.transform.position.x, _pixelEditorPaintBoard.basePosition.y + 130f);
		_pixelEditorPaintBoard.leftPosition = _pixelEditorPaintBoard.basePosition;
		characterBuilderModesBar.transform.DOScaleX(0f, zoomSpeed).SetEase(Ease.InElastic);
	}

	public void CompensateForPalleteOffsetOnUnZoom()
	{
		_pixelToolPalette.openPosition = _initialPaletteOpenPosition;
		_pixelToolPalette.startingPosition = _initialPaletteStartingPosition;
		_pixelEditorPaintBoard.basePosition = _initialBoardBasePosition;
		_pixelEditorPaintBoard.leftPosition = _initialBoardLeftPosition;
		characterBuilderModesBar.transform.DOScale(1.35f, zoomSpeed).SetEase(Ease.OutElastic);
	}

	public void EstablishNewInitialScaleAndPositionForApplicableGameObjects()
	{
		if (is16by10OrWider)
		{
			characterBuilderModesBar.GetComponent<RectTransform>().pivot = new Vector2(0.75f, 0.5f);
			characterBuilderModesBar.transform.localScale = new Vector3(1.35f, 1.35f, 0f);
			paintBoardTools.GetComponent<RectTransform>().pivot = new Vector2(0.33f, 0.5f);
			paintBoardTools.transform.localScale = new Vector3(1.5f, 1.5f, 0f);
			Vector3 localPosition = new Vector3(mapButtons.transform.localPosition.x + 37f, mapButtons.transform.localPosition.y + 37f, 0f);
			mapButtons.transform.localPosition = localPosition;
			mapButtons.transform.localScale = new Vector3(1.5f, 1.5f, 0f);
			mapZoomToggle.transform.localScale = new Vector3(0f, 0f, 0f);
		}
	}
}
