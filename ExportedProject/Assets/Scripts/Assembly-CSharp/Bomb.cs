using System;
using UnityEngine;

public sealed class Bomb : PoolableComponent
{
	public float startTime;

	public float throwForceX;

	public float throwForceY;

	public float gScale;

	public float explodeDelay;

	public float collisionBlockRadius;

	public LayerMask collisionLayer;

	public Rigidbody2D rigidbody2dComponent;

	public Collider2D collider2dComponent;

	public GameObject meshContainer;

	public ParticleSystem particles;

	public static readonly int BombDamage = 6;

	private static Vector3 _originalScale;

	private static float _originalGravityScale;

	private static float _originalParticleSize;

	private static float _originalParticleSpeed;

	private static float _originalParticleGravity;

	private static float _originalParticleEmissionRadius;

	private float _expirationTime;

	private Action expirationAction;

	private static Collider2D[] _bombCollisionResults = new Collider2D[64];

	private int bounceCount;

	public static void InitStaticValues(Bomb bombTemplate)
	{
		_originalScale = bombTemplate.selfTransform.localScale;
		_originalGravityScale = bombTemplate.gScale;
		_originalParticleSize = bombTemplate.particles.startSize;
		_originalParticleSpeed = bombTemplate.particles.startSpeed;
		_originalParticleGravity = bombTemplate.particles.gravityModifier;
		_originalParticleEmissionRadius = bombTemplate.particles.shape.radius;
	}

	public void InitWithScale(float scaleMultiplier)
	{
		selfTransform.localScale = _originalScale * scaleMultiplier;
		rigidbody2dComponent.gravityScale = gScale * scaleMultiplier;
	}

	public override void EarlyAwake()
	{
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		selfTransform.SetParent(WorldWrapper.instance.runtimeObjectContainer);
		_expirationTime = Time.time + explodeDelay;
		expirationAction = Explode;
		rigidbody2dComponent.isKinematic = false;
		rigidbody2dComponent.gravityScale = _originalGravityScale;
		collider2dComponent.enabled = true;
		meshContainer.SetActive(true);
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		base.gameObject.SetActive(false);
	}

	public override void Despawn()
	{
		GameplayPool.DespawnBomb(this);
	}

	public void Throw(int direction)
	{
		float num = 1f;
		Vector2 velocity = Vector2.zero;
		if ((bool)PixelPlayerController.instance)
		{
			num = PixelPlayerController.instance.selfTransform.localScale.z;
			velocity = new Vector2(PixelPlayerController.instance.velocity.x, (!(PixelPlayerController.instance.velocity.y > 0f)) ? 0f : (PixelPlayerController.instance.velocity.y / 2f));
		}
		rigidbody2dComponent.velocity = velocity;
		rigidbody2dComponent.AddForce(new Vector2(throwForceX * (float)direction * num, throwForceY * num), ForceMode2D.Impulse);
	}

	private void Update()
	{
		if (Time.time > _expirationTime)
		{
			expirationAction();
		}
	}

	private void Explode()
	{
		_expirationTime = Time.time + 4f;
		expirationAction = Despawn;
		rigidbody2dComponent.isKinematic = true;
		collider2dComponent.enabled = false;
		meshContainer.SetActive(false);
		float num = selfTransform.localScale.y * 2f;
		particles.startSize = _originalParticleSize * num;
		particles.startSpeed = _originalParticleSpeed * num;
		particles.gravityModifier = _originalParticleGravity * num;
		ParticleSystem.ShapeModule shape = particles.shape;
		shape.radius = _originalParticleEmissionRadius * num;
		particles.Play();
		BloxelCamera.instance.cameraShake.PlayShake();
		ExplosionFlash explosionFlash = GameplayController.SpawnExplosionFlash();
		explosionFlash.explosionParticles.gravityModifier = ExplosionFlash.OriginalGravityModifier * num;
		explosionFlash.selfTransform.localScale = new Vector3(2f, 2f, 2f) * num;
		explosionFlash.selfTransform.localPosition = selfTransform.localPosition;
		explosionFlash.PlayAll();
		SoundManager.PlayOneShot(SoundManager.instance.bombSFX);
		int num2 = Physics2D.OverlapCircleNonAlloc(new Vector2(base.transform.position.x, base.transform.position.y), collisionBlockRadius * 13f * num, _bombCollisionResults, collisionLayer);
		for (int i = 0; i < num2; i++)
		{
			Collider2D collider2D = _bombCollisionResults[i];
			TilePhysicsEnabler component = collider2D.attachedRigidbody.GetComponent<TilePhysicsEnabler>();
			if ((bool)component)
			{
				((BloxelEnemyTile)component.tileRenderer.bloxelTile).TakeDamage(selfTransform.position, 9.25f, BombDamage);
				continue;
			}
			BloxelTile component2 = collider2D.GetComponent<BloxelTile>();
			if (component2 != null && component2.tileInfo.isLoaded)
			{
				if (component2.tileRenderer.collisionLayer == LayerMask.NameToLayer(BloxelDestructibleTile.collider2DLayer) || component2.tileRenderer.collisionLayer == LayerMask.NameToLayer(BloxelPowerUpTile.collider2DLayer))
				{
					TileExplosionManager.instance.ExplodeChunk(component2.tileRenderer.currentFrameChunk, component2.selfTransform, component2.rigidbodyComponent, base.transform.position + TileExplosionManager.instance.bombExplosionOffset, TileExplosionManager.instance.bombExplosionForce, TileExplosionManager.instance.bombExplosionRadius, TileExplosionManager.instance.bombUpwardsModifier, new Vector3(component2.selfTransform.localScale.x, component2.selfTransform.localScale.y, component2.selfTransform.localScale.y), false);
					GameplayBuilder.instance.UnloadTile(component2, true);
				}
				if (component2 is BloxelPowerUpTile)
				{
					((BloxelPowerUpTile)component2).SpawnContents();
				}
			}
		}
		if (num2 > 0)
		{
			SoundManager.PlayOneShot(SoundManager.instance.destructSFX);
		}
	}
}
