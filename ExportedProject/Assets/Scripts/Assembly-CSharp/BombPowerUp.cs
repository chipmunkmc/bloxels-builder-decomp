using UnityEngine;

public class BombPowerUp : PowerUp, InventoryItem
{
	private new void Start()
	{
		base.Start();
		powerUpType = PowerUpType.Bomb;
		Bounce();
	}

	public override void Collect()
	{
		base.Collect();
		Object.Destroy(base.gameObject);
	}

	public void AddItemToInventory()
	{
		GameplayController.instance.inventoryManager.AddToInventory(InventoryData.Type.Bomb);
	}
}
