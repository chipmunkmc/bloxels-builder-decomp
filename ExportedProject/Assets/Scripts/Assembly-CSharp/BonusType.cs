public enum BonusType : byte
{
	Coin = 0,
	Health = 1,
	Bomb = 2,
	Shrink = 3
}
