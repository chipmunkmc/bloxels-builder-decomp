using UnityEngine;

public class BrainPropertyManager : ExposableMonoBehaviour
{
	[Header("Damage")]
	[SerializeField]
	[HideInInspector]
	private int _explosiveDeathThreshold;

	[Header("Bonus")]
	[SerializeField]
	[HideInInspector]
	private int _healthBonusThreshold;

	[Header("Burgle")]
	[SerializeField]
	[HideInInspector]
	private int _healthBurgleThreshold;

	[Header("Scale")]
	[SerializeField]
	[HideInInspector]
	private int _scaleShrinkThreshold;

	[Header("Projectile")]
	[SerializeField]
	[HideInInspector]
	private int _spreadShotThreshold;

	[SerializeField]
	[HideInInspector]
	private int _laserThreshold;

	[Header("Health")]
	[SerializeField]
	[HideInInspector]
	private int _dryBonesThreshold;

	[SerializeField]
	[HideInInspector]
	private int _bulletReflectionThreshold;

	[ExposeProperty]
	public int explosiveDeathThreshold
	{
		get
		{
			return _explosiveDeathThreshold;
		}
		set
		{
			_explosiveDeathThreshold = value;
			BloxelBrain.ExplosiveDeathThreshold = _explosiveDeathThreshold;
		}
	}

	[ExposeProperty]
	public int healthBonusThreshold
	{
		get
		{
			return _healthBonusThreshold;
		}
		set
		{
			_healthBonusThreshold = value;
			BloxelBrain.HealthBonusThreshold = _healthBonusThreshold;
		}
	}

	[ExposeProperty]
	public int healthBurgleThreshold
	{
		get
		{
			return _healthBurgleThreshold;
		}
		set
		{
			_healthBurgleThreshold = value;
			BloxelBrain.HealthBurgleThreshold = _healthBurgleThreshold;
		}
	}

	[ExposeProperty]
	public int scaleShrinkThreshold
	{
		get
		{
			return _scaleShrinkThreshold;
		}
		set
		{
			_scaleShrinkThreshold = value;
			BloxelBrain.ScaleShrinkThreshold = _scaleShrinkThreshold;
		}
	}

	[ExposeProperty]
	public int spreadShotThreshold
	{
		get
		{
			return _spreadShotThreshold;
		}
		set
		{
			_spreadShotThreshold = value;
			BloxelBrain.SpreadShotThreshold = _spreadShotThreshold;
		}
	}

	[ExposeProperty]
	public int laserThreshold
	{
		get
		{
			return _laserThreshold;
		}
		set
		{
			_laserThreshold = value;
			BloxelBrain.LaserThreshold = _laserThreshold;
			BloxelBrain.LaserTrackingThreshold = _laserThreshold + 8;
		}
	}

	[ExposeProperty]
	public int dryBonesThreshold
	{
		get
		{
			return _dryBonesThreshold;
		}
		set
		{
			_dryBonesThreshold = value;
			BloxelBrain.DryBonesThreshold = _dryBonesThreshold;
		}
	}

	[ExposeProperty]
	public int bulletReflectionThreshold
	{
		get
		{
			return _bulletReflectionThreshold;
		}
		set
		{
			_bulletReflectionThreshold = value;
			BloxelBrain.BulletReflectionThreshold = _bulletReflectionThreshold;
		}
	}

	private void Awake()
	{
		InitBrainProperties();
	}

	private void InitBrainProperties()
	{
		BloxelBrain.ExplosiveDeathThreshold = _explosiveDeathThreshold;
		BloxelBrain.HealthBonusThreshold = _healthBonusThreshold;
		BloxelBrain.HealthBurgleThreshold = _healthBurgleThreshold;
		BloxelBrain.ScaleShrinkThreshold = _scaleShrinkThreshold;
		BloxelBrain.SpreadShotThreshold = _spreadShotThreshold;
		BloxelBrain.LaserThreshold = _laserThreshold;
		BloxelBrain.LaserTrackingThreshold = _laserThreshold + 8;
		BloxelBrain.DryBonesThreshold = _dryBonesThreshold;
		BloxelBrain.BulletReflectionThreshold = _bulletReflectionThreshold;
		BloxelBrain.Thresholds = new int[9][]
		{
			new int[1] { _explosiveDeathThreshold },
			new int[0],
			new int[1] { _healthBonusThreshold },
			new int[1] { _healthBurgleThreshold },
			new int[0],
			new int[3]
			{
				_spreadShotThreshold,
				_laserThreshold,
				_laserThreshold + 8
			},
			new int[1] { _bulletReflectionThreshold },
			new int[0],
			new int[0]
		};
	}
}
