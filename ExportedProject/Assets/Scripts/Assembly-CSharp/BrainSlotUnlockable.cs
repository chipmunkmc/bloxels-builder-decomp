public class BrainSlotUnlockable : GemUnlockable
{
	public int slotId;

	public BrainSlotUnlockable(int idx)
	{
		slotId = idx;
		cost = GemUnlockable.BrainSlotCost;
		type = Type.BrainBoardSlot;
		uiBuyable = UIBuyable.CreateBrainSlot();
	}

	public override bool IsLocked()
	{
		return GemManager.instance.IsBrainSlotLocked(slotId);
	}

	public override void Unlock()
	{
		GemManager.instance.UnlockBrainSlot(slotId);
		EventUnlock(this);
	}
}
