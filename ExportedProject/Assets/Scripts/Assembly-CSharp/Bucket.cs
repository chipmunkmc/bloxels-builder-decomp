public enum Bucket
{
	AccountCloud = 0,
	GameScreenshots = 1,
	CaptureResults = 2,
	News = 3,
	ProjectCovers = 4,
	RedisBackup = 5,
	ProjectCoversOld = 6
}
