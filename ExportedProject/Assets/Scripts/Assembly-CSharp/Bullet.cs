using System;
using UnityEngine;

public sealed class Bullet : PoolableComponent
{
	public float lifetime;

	private float _expirationTime;

	public Rigidbody2D rigidbodyComponent;

	public Collider2D collider2dComponent;

	public ParticleSystem particles;

	public ParticleSystem explosionParticles;

	private ParticleSystem.EmissionModule _emissionModule;

	private static Vector3 _originalScale;

	private static float _originalExplosionParticleGravity;

	private static ParticleSystem.MinMaxCurve _zeroEmissionRate;

	private static ParticleSystem.MinMaxCurve _originalEmissionRate;

	private Action expirationAction;

	public static void InitEmissionRates(Bullet originalBullet)
	{
		_originalScale = originalBullet.selfTransform.localScale;
		_originalExplosionParticleGravity = originalBullet.explosionParticles.gravityModifier;
		ParticleSystem.EmissionModule emission = originalBullet.particles.emission;
		_zeroEmissionRate = new ParticleSystem.MinMaxCurve(0f);
		_originalEmissionRate = emission.rate;
	}

	public override void EarlyAwake()
	{
		_emissionModule = particles.emission;
		particles.randomSeed = (uint)UnityEngine.Random.Range(int.MinValue, int.MaxValue);
		explosionParticles.randomSeed = (uint)UnityEngine.Random.Range(int.MinValue, int.MaxValue);
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		selfTransform.parent = WorldWrapper.instance.runtimeObjectContainer;
		rigidbodyComponent.velocity = Vector2.zero;
		rigidbodyComponent.angularVelocity = 0f;
		collider2dComponent.enabled = true;
		_expirationTime = Time.time + lifetime;
		expirationAction = KillBullet;
		float y = PixelPlayerController.instance.selfTransform.localScale.y;
		selfTransform.localScale = _originalScale * y;
		explosionParticles.gravityModifier = _originalExplosionParticleGravity * y;
		_emissionModule.rate = _originalEmissionRate;
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		base.gameObject.SetActive(false);
	}

	public override void Despawn()
	{
		GameplayPool.DespawnBullet(this);
	}

	private void Update()
	{
		if (Time.time >= _expirationTime)
		{
			expirationAction();
		}
	}

	public void Explode()
	{
		KillBullet();
		explosionParticles.Emit(30);
	}

	public void KillBullet()
	{
		_expirationTime = Time.time + 3f;
		expirationAction = Despawn;
		_emissionModule.rate = _zeroEmissionRate;
		collider2dComponent.enabled = false;
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		LayerMask layerMask = collision.gameObject.layer;
		Rigidbody2D rigidbody = collision.rigidbody;
		BloxelTile bloxelTile = null;
		switch ((long)layerMask)
		{
		case 10L:
			bloxelTile = rigidbody.gameObject.GetComponent<BloxelTile>();
			if (bloxelTile != null && bloxelTile.tileInfo.isLoaded)
			{
				TileExplosionManager.instance.ExplodeChunk(bloxelTile.tileRenderer.currentFrameChunk, bloxelTile.selfTransform, bloxelTile.rigidbodyComponent, selfTransform.position, TileExplosionManager.instance.bulletExplosionForce, TileExplosionManager.instance.bulletExplosionRadius, 0f, new Vector3(bloxelTile.selfTransform.localScale.x, bloxelTile.selfTransform.localScale.y, bloxelTile.selfTransform.localScale.y));
				GameplayBuilder.instance.UnloadTile(bloxelTile, true);
			}
			break;
		case 13L:
			bloxelTile = rigidbody.gameObject.GetComponent<BloxelTile>();
			if (bloxelTile != null && bloxelTile.tileInfo.isLoaded)
			{
				((BloxelPowerUpTile)bloxelTile).SpawnContents();
				TileExplosionManager.instance.ExplodeChunk(bloxelTile.tileRenderer.currentFrameChunk, bloxelTile.selfTransform, bloxelTile.rigidbodyComponent, selfTransform.position, TileExplosionManager.instance.bulletExplosionForce, TileExplosionManager.instance.bulletExplosionRadius, 0f, new Vector3(bloxelTile.selfTransform.localScale.x, bloxelTile.selfTransform.localScale.y, bloxelTile.selfTransform.localScale.y));
				GameplayBuilder.instance.UnloadTile(bloxelTile, true);
			}
			break;
		case 18L:
		{
			TilePhysicsEnabler component = rigidbody.GetComponent<TilePhysicsEnabler>();
			if ((bool)component)
			{
				bloxelTile = component.tileRenderer.bloxelTile;
				((BloxelEnemyTile)bloxelTile).BulletHit(this);
			}
			return;
		}
		}
		Explode();
	}
}
