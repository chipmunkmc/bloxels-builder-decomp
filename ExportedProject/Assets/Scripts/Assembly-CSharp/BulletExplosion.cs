using UnityEngine;

public class BulletExplosion : MonoBehaviour
{
	private ParticleSystem particles;

	private float recycleTime;

	private float lifeTime = 1.5f;

	private Vector3 originalScale;

	private float originalParticleSize;

	private float originalParticleSpeed;

	private float originalGravityModifier;

	private void Awake()
	{
		particles = GetComponent<ParticleSystem>();
		originalScale = base.transform.localScale;
		originalParticleSize = particles.startSize;
		originalParticleSpeed = particles.startSpeed;
		originalGravityModifier = particles.gravityModifier;
	}

	private void OnEnable()
	{
		if ((bool)PixelPlayerController.instance)
		{
			float z = PixelPlayerController.instance.selfTransform.localScale.z;
			base.transform.localScale = new Vector3(originalScale.x * z, originalScale.y * z, originalScale.z * z);
			particles.startSize = originalParticleSize * z;
			particles.startSpeed = originalParticleSpeed * z;
			particles.gravityModifier = originalGravityModifier * z;
		}
		particles.Play();
		recycleTime = Time.time + lifeTime;
	}

	private void Update()
	{
		if (Time.time >= recycleTime)
		{
			base.gameObject.Recycle();
		}
	}
}
