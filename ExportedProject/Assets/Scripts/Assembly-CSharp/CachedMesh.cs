using System.Collections.Generic;
using UnityEngine;

public class CachedMesh
{
	public float expirationTime;

	public List<Vector2[]> colliderPaths = new List<Vector2[]>(32);

	public static float lifetime = 3f;

	public static MeshCacheManager CacheManager;

	public int id { get; private set; }

	public Mesh sharedMesh { get; private set; }

	public CachedMesh(int id, Mesh sharedMesh)
	{
		this.id = id;
		expirationTime = Time.time + lifetime;
		this.sharedMesh = sharedMesh;
		CacheManager.cachedMeshes[id] = this;
	}

	public void UpdateTick()
	{
		if (Time.time > expirationTime)
		{
			CacheManager.idsToRemove.Add(id);
			Object.Destroy(sharedMesh);
		}
	}

	public void RemoveFromCacheImmediately()
	{
		CacheManager.cachedMeshes.Remove(id);
		Object.Destroy(sharedMesh);
	}
}
