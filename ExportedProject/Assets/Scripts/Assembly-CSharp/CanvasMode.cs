public enum CanvasMode
{
	PaintBoard = 0,
	Animator = 1,
	MegaBoard = 2,
	LevelEditor = 3,
	CharacterBuilder = 4
}
