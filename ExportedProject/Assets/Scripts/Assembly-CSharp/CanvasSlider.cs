using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSlider : MonoBehaviour
{
	public RectTransform theRect;

	public Text updateText;

	public float closedVal;

	public float openVal;

	public bool noisy;

	public bool open;

	private void Start()
	{
	}

	public void toggleSlider()
	{
		Vector2 anchoredPosition = theRect.anchoredPosition;
		if (noisy)
		{
		}
		string text;
		if (anchoredPosition.x < closedVal)
		{
			anchoredPosition.x = closedVal;
			text = "-";
		}
		else
		{
			anchoredPosition.x = openVal;
			text = "+";
		}
		open = !open;
		DOTween.To(() => theRect.anchoredPosition, delegate(Vector2 x)
		{
			theRect.anchoredPosition = x;
		}, anchoredPosition, 0.3f);
		if (updateText != null)
		{
			updateText.text = text;
		}
	}
}
