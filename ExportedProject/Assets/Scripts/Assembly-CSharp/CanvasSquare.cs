using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VoxelEngine;

public sealed class CanvasSquare : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleStateChange(CanvasTileInfo.ReadyState _val);

	public Transform selfTransform;

	[Header("| ========= Data ========= |")]
	public BloxelProject project;

	public WorldPos2D iWallCoordinate;

	public int currentValue;

	private List<BloxelBoard> _animationFrameBoards;

	private float _largeScale = 3f;

	private string _associatedProjectJSON;

	private float _frameRate;

	private float _timerStart;

	private int _currentFrameIndex;

	private GameParserJob _gameParseJob;

	private CharacterParserJob _characterParseJob;

	private AnimationParserJob _animationParseJob;

	private BoardParserJob _boardParseJob;

	private MegaBoardParserJob _megaBoardParseJob;

	[Header("| ========= Localization ========= |")]
	public string string_loadingText = "Loading...";

	private string _string_selectInstructions = "Select an Item From Your Library";

	private string _string_noDemoGame = "Cannot Publish Default Story Game";

	private string _string_errorUploading = "Error Uploading";

	[Header("| ========= UI ========= |")]
	public RectTransform rectTransform;

	public Text uiTextCoordinate;

	public Text uiTextInstructions;

	public CanvasGroup container;

	public Image uiIconAdd;

	public Image uiColorBar;

	public Image iconMySquare;

	public Image image;

	public Sprite sprite;

	public static Texture2D downloadTexture;

	private static Color[] _downloadColorBuffer = new Color[676];

	private static Color[] _spriteColorBuffer = new Color[169];

	[Header("| ========= State Tracking ========= |")]
	public bool isActive;

	public bool shouldAnimate;

	private static bool _IsDragging;

	private CanvasTileInfo.ReadyState _state;

	[Header("| ========= Square Info ========= |")]
	public ServerSquare serverSquare;

	public int bloxelsUsed;

	public int roomsUsed;

	private int _boardsUsed;

	public bool mine;

	public bool taken;

	public static CanvasSquare activeSquare;

	public CanvasTileInfo.ReadyState state
	{
		get
		{
			return _state;
		}
		set
		{
			_state = value;
			if (this.OnStateChange != null)
			{
				this.OnStateChange(_state);
			}
		}
	}

	public event HandleStateChange OnStateChange;

	public void Awake()
	{
		string_loadingText = LocalizationManager.getInstance().getLocalizedText("loadingscreen2", "Loading...");
		_string_selectInstructions = LocalizationManager.getInstance().getLocalizedText("iWall41", "Select an Item From Your Library");
		_string_noDemoGame = LocalizationManager.getInstance().getLocalizedText("iWall95", "Cannot Publish Default Story Game");
		_string_errorUploading = LocalizationManager.getInstance().getLocalizedText("iWall96", "Error Uploading");
		selfTransform = base.transform;
		uiIconAdd.transform.localScale = Vector3.one;
		HideInstructions();
		state = CanvasTileInfo.ReadyState.Requested;
	}

	public void OnEnable()
	{
		CanvasStreamingInterface.instance.OnRowOrColumnShift += HandleOnRowOrColumnShift;
		Reset();
		base.gameObject.SetActive(true);
	}

	public void OnDisable()
	{
		CanvasStreamingInterface.instance.OnRowOrColumnShift -= HandleOnRowOrColumnShift;
		base.gameObject.SetActive(false);
	}

	private void Update()
	{
		if (shouldAnimate && _boardsUsed > 0 && _timerStart + 1f / _frameRate < Time.time)
		{
			_currentFrameIndex = (_currentFrameIndex + 1) % _boardsUsed;
			_animationFrameBoards[_currentFrameIndex].SetPixelsForSprite13x13(sprite, BloxelBoard.baseUIBoardColor);
			_timerStart += 1f / _frameRate;
		}
	}

	public void SetNewCoordinates(WorldPos2D coordinate, bool init = false)
	{
		if (!init)
		{
			CanvasStreamingInterface.instance.canvasSquaresDict.Remove(coordinate);
		}
		iWallCoordinate = coordinate;
		CanvasStreamingInterface.instance.canvasSquaresDict[iWallCoordinate] = this;
		Reset();
		uiTextCoordinate.text = iWallCoordinate.ToString().Replace('|', ',');
	}

	private void OnDestroy()
	{
		Unload();
	}

	private void Unload()
	{
		project = null;
		_animationFrameBoards = null;
		_associatedProjectJSON = null;
		_gameParseJob = null;
		_characterParseJob = null;
		_animationParseJob = null;
		_boardParseJob = null;
		_megaBoardParseJob = null;
		string_loadingText = null;
		_string_selectInstructions = null;
		_string_noDemoGame = null;
		rectTransform = null;
		uiTextCoordinate = null;
		uiTextInstructions = null;
		uiIconAdd = null;
		uiColorBar = null;
		iconMySquare = null;
		image = null;
		sprite = null;
		serverSquare = null;
	}

	private void HandleOnRowOrColumnShift()
	{
		int num = 0;
		int num2 = 0;
		Vector2 anchoredPosition = rectTransform.anchoredPosition;
		if (anchoredPosition.x <= 0f - CanvasStreamingInterface.instance.halfSpanX)
		{
			anchoredPosition += new Vector2(CanvasStreamingInterface.instance.canvasWorldSpanX, 0f);
			num += CanvasStreamingInterface.instance.squarePositionsWidth;
		}
		else if (anchoredPosition.x >= CanvasStreamingInterface.instance.halfSpanX)
		{
			anchoredPosition -= new Vector2(CanvasStreamingInterface.instance.canvasWorldSpanX, 0f);
			num -= CanvasStreamingInterface.instance.squarePositionsWidth;
		}
		if (anchoredPosition.y <= 0f - CanvasStreamingInterface.instance.halfSpanY)
		{
			anchoredPosition += new Vector2(0f, CanvasStreamingInterface.instance.canvasWorldSpanY);
			num2 += CanvasStreamingInterface.instance.squarePositionsHeight;
		}
		else if (anchoredPosition.y >= CanvasStreamingInterface.instance.halfSpanY)
		{
			anchoredPosition -= new Vector2(0f, CanvasStreamingInterface.instance.canvasWorldSpanY);
			num2 -= CanvasStreamingInterface.instance.squarePositionsHeight;
		}
		if (num != 0 || num2 != 0)
		{
			rectTransform.anchoredPosition = anchoredPosition;
			WorldPos2D coordinate = new WorldPos2D(iWallCoordinate.x + num, iWallCoordinate.y + num2);
			SetNewCoordinates(coordinate);
		}
	}

	public void UpdateData(JSONObject barf)
	{
		serverSquare = new ServerSquare(barf.ToString());
		StartCoroutine(WWWTex(serverSquare.texture));
		InitStartupModel();
		SetTaken(true);
		if (CurrentUser.instance.account != null && serverSquare.publisher != null && serverSquare.publisher == CurrentUser.instance.account.serverID)
		{
			mine = true;
			iconMySquare.enabled = true;
			if (!CurrentUser.instance.account.publishedSquares.Contains(serverSquare._id) && !string.IsNullOrEmpty(serverSquare._id))
			{
				CurrentUser.instance.account.publishedSquares.Add(serverSquare._id);
			}
		}
	}

	public void ShowInstructions()
	{
		uiIconAdd.transform.localScale = Vector3.zero;
		uiTextInstructions.gameObject.SetActive(true);
		Pulse();
	}

	public void NoDemoGame()
	{
		Pulse();
		uiTextInstructions.DOText(_string_noDemoGame, UIAnimationManager.speedMedium);
		StartCoroutine(DelayedResetInstructions());
	}

	public void ErrorUploading()
	{
		Pulse();
		uiTextInstructions.DOText(_string_errorUploading, UIAnimationManager.speedMedium);
		StartCoroutine(DelayedResetInstructions());
	}

	private IEnumerator DelayedResetInstructions()
	{
		yield return new WaitForSeconds(2f);
		Pulse();
		uiTextInstructions.DOText(_string_selectInstructions, UIAnimationManager.speedMedium);
	}

	public void Pulse()
	{
		base.transform.SetAsLastSibling();
		rectTransform.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), UIAnimationManager.speedFast);
	}

	public void HideInstructions()
	{
		uiTextInstructions.text = _string_selectInstructions;
		uiTextInstructions.gameObject.SetActive(false);
	}

	public void Reset()
	{
		Viewer.instance.serverSquares.Remove(iWallCoordinate.ToString());
		StopAllCoroutines();
		StopParsingJobs();
		iconMySquare.enabled = false;
		shouldAnimate = false;
		ResetSpritePixels();
		image.color = Color.white;
		uiIconAdd.transform.localScale = Vector3.one;
		mine = false;
		taken = false;
		uiColorBar.color = Color.gray;
		_associatedProjectJSON = null;
		serverSquare = null;
		project = null;
	}

	private void StopParsingJobs()
	{
		if (_boardParseJob != null && _boardParseJob.isRunning)
		{
			_boardParseJob.Interrupt();
		}
		else if (_animationParseJob != null && _animationParseJob.isRunning)
		{
			_animationParseJob.Interrupt();
		}
		else if (_characterParseJob != null && _characterParseJob.isRunning)
		{
			_characterParseJob.Interrupt();
		}
		else if (_megaBoardParseJob != null && _megaBoardParseJob.isRunning)
		{
			_megaBoardParseJob.Interrupt();
		}
		else if (_gameParseJob != null && _gameParseJob.isRunning)
		{
			_gameParseJob.Interrupt();
		}
	}

	public void Toggle()
	{
		if (isActive)
		{
			DeActivate();
		}
		else
		{
			Activate();
		}
	}

	public void Activate()
	{
		if (serverSquare == null)
		{
			StartCoroutine("DelayedActivate");
			return;
		}
		SoundManager.PlaySoundClip(SoundClip.confirmA);
		if (serverSquare.type == ProjectType.Game && _associatedProjectJSON == null)
		{
			GetGame();
		}
		else if (serverSquare.type == ProjectType.MegaBoard && _associatedProjectJSON == null)
		{
			GetMegaBoard();
		}
		else if (serverSquare.type == ProjectType.Board && _associatedProjectJSON == null)
		{
			GetBoard();
		}
		else if (serverSquare.type == ProjectType.Animation && _associatedProjectJSON == null && DeviceManager.isPoopDevice)
		{
			Gandalf.instance.Request(this);
		}
		else if (serverSquare.type == ProjectType.Character && _associatedProjectJSON == null && DeviceManager.isPoopDevice)
		{
			Gandalf.instance.Request(this);
		}
		else
		{
			state = CanvasTileInfo.ReadyState.Ready;
		}
		isActive = true;
		activeSquare = this;
		CanvasStreamingInterface.instance.SquareActivate(this);
		CanvasTileInfo.instance.Populate(this);
		StartCoroutine("WaitForSquareToCenter");
		UIWarpDrive.instance.Move(Direction.Up, UIWarpDrive.instance.hiddenPosition);
	}

	public IEnumerator DelayedActivate()
	{
		while (serverSquare == null)
		{
			yield return new WaitForEndOfFrame();
		}
		Activate();
	}

	private IEnumerator WaitForSquareToCenter()
	{
		while (rectTransform.anchoredPosition.sqrMagnitude > 1f)
		{
			yield return new WaitForEndOfFrame();
		}
		CanvasStreamingInterface.instance.HideLoadingText();
		base.transform.SetAsLastSibling();
		base.transform.DOScale(_largeScale, UIAnimationManager.speedFast);
		rectTransform.DOAnchorPos(new Vector2(rectTransform.anchoredPosition.x, rectTransform.anchoredPosition.y + 70f), UIAnimationManager.speedFast);
		if (mine)
		{
			iconMySquare.enabled = false;
		}
		CanvasStreamingInterface.instance.details.InitForSquare(this);
		BloxelServerInterface.instance.Emit_ViewSquare(serverSquare._id);
	}

	public void DeActivate(bool shouldHideInfo = true)
	{
		SoundManager.PlaySoundClip(SoundClip.cancelA);
		StopCoroutine("WaitForSquareToCenter");
		StopCoroutine("DelayedActivate");
		if (shouldHideInfo)
		{
			CanvasTileInfo.instance.Hide();
		}
		isActive = false;
		activeSquare = null;
		if (mine)
		{
			iconMySquare.enabled = true;
		}
		CanvasStreamingInterface.instance.details.Dismiss(!shouldHideInfo);
	}

	private void SwitchOutline()
	{
		switch (serverSquare.type)
		{
		case ProjectType.Board:
			uiColorBar.color = AssetManager.instance.bloxelOrange;
			break;
		case ProjectType.Animation:
			uiColorBar.color = AssetManager.instance.bloxelBlue;
			break;
		case ProjectType.MegaBoard:
			uiColorBar.color = AssetManager.instance.bloxelGreen;
			break;
		case ProjectType.Character:
			uiColorBar.color = AssetManager.instance.bloxelRed;
			break;
		case ProjectType.Game:
			uiColorBar.color = AssetManager.instance.bloxelPurple;
			break;
		default:
			uiColorBar.color = Color.white;
			break;
		}
	}

	public void InitStartupModel()
	{
		switch (serverSquare.type)
		{
		case ProjectType.Animation:
			if (!DeviceManager.isPoopDevice)
			{
				Gandalf.instance.Request(this);
			}
			break;
		case ProjectType.Character:
			if (!DeviceManager.isPoopDevice)
			{
				Gandalf.instance.Request(this);
			}
			break;
		}
	}

	private void GetBoard()
	{
		state = CanvasTileInfo.ReadyState.Downloading;
		StartCoroutine(BloxelServerRequests.instance.FindByID(serverSquare.type, serverSquare.projectID, delegate(string jsonString)
		{
			if (!string.IsNullOrEmpty(jsonString))
			{
				_associatedProjectJSON = jsonString;
				_boardParseJob = new BoardParserJob();
				_boardParseJob.responseFromServer = jsonString;
				_boardParseJob.OnComplete += BoardParseComplete;
				_boardParseJob.Start();
				StartCoroutine(_boardParseJob.WaitFor());
				state = CanvasTileInfo.ReadyState.Waiting;
			}
			else
			{
				state = CanvasTileInfo.ReadyState.Failed;
			}
		}));
	}

	public void SetFailedState()
	{
		_associatedProjectJSON = null;
		state = CanvasTileInfo.ReadyState.Failed;
	}

	public void SetWaitingState()
	{
		state = CanvasTileInfo.ReadyState.Waiting;
	}

	private void GetGame()
	{
		state = CanvasTileInfo.ReadyState.Downloading;
		CanvasTileInfo.instance.ShowProgress();
		StartCoroutine(BloxelServerRequests.instance.FindByID(ProjectType.Game, serverSquare.projectID, delegate(string jsonString)
		{
			if (!string.IsNullOrEmpty(jsonString))
			{
				_associatedProjectJSON = jsonString;
				ParseGame();
				state = CanvasTileInfo.ReadyState.Waiting;
			}
			else
			{
				state = CanvasTileInfo.ReadyState.Failed;
			}
		}));
	}

	private void GetMegaBoard()
	{
		state = CanvasTileInfo.ReadyState.Downloading;
		CanvasTileInfo.instance.progressContainer.DOAnchorPos(new Vector2(0f, 100f), UIAnimationManager.speedFast).OnComplete(delegate
		{
			CanvasTileInfo.instance.progressBar.FakeIncrement();
		});
		StartCoroutine(BloxelServerRequests.instance.FindByID(ProjectType.MegaBoard, serverSquare.projectID, delegate(string jsonString)
		{
			if (!string.IsNullOrEmpty(jsonString))
			{
				_associatedProjectJSON = jsonString;
				ParseMegaBoard();
				state = CanvasTileInfo.ReadyState.Waiting;
			}
			else
			{
				state = CanvasTileInfo.ReadyState.Failed;
			}
		}));
	}

	private void ParseMegaBoard()
	{
		if (project == null && _associatedProjectJSON != null)
		{
			_megaBoardParseJob = new MegaBoardParserJob();
			_megaBoardParseJob.responseFromServer = _associatedProjectJSON;
			_megaBoardParseJob.OnComplete += MegaBoardParseComplete;
			_megaBoardParseJob.Start();
			StartCoroutine(_megaBoardParseJob.WaitFor());
		}
	}

	private void ParseGame()
	{
		if (project == null && _associatedProjectJSON != null)
		{
			_gameParseJob = new GameParserJob();
			_gameParseJob.responseFromServer = _associatedProjectJSON;
			_gameParseJob.OnComplete += GameParseComplete;
			_gameParseJob.Start();
			StartCoroutine(_gameParseJob.WaitFor());
		}
	}

	private void BoardParseComplete(BloxelBoard _board)
	{
		_boardParseJob.OnComplete -= BoardParseComplete;
		if (serverSquare != null && !(serverSquare.projectID == string.Empty))
		{
			project = _board;
			if (project != null)
			{
				_boardsUsed = 1;
				bloxelsUsed = ((BloxelBoard)project).GetBloxelCount();
				currentValue = ExchangeManager.instance.CalculateValue(bloxelsUsed);
				state = CanvasTileInfo.ReadyState.Ready;
			}
			else
			{
				state = CanvasTileInfo.ReadyState.Failed;
			}
		}
	}

	private void MegaBoardParseComplete(BloxelMegaBoard _megaBoard)
	{
		_megaBoardParseJob.OnComplete -= MegaBoardParseComplete;
		if (serverSquare != null && !(serverSquare.projectID == string.Empty))
		{
			project = _megaBoard;
			CanvasTileInfo.instance.progressContainer.DOAnchorPos(Vector2.zero, UIAnimationManager.speedFast).OnComplete(delegate
			{
				CanvasTileInfo.instance.progressBar.StopProgress();
			});
			if (project != null)
			{
				_boardsUsed = ((BloxelMegaBoard)project).UniqueBoards().Count;
				bloxelsUsed = ((BloxelMegaBoard)project).GetBloxelCount();
				currentValue = ExchangeManager.instance.CalculateValue(bloxelsUsed);
				CanvasStreamingInterface.instance.details.FadeInDectoratedBoard();
				state = CanvasTileInfo.ReadyState.Ready;
			}
			else
			{
				state = CanvasTileInfo.ReadyState.Failed;
			}
		}
	}

	public void AnimationParseComplete(BloxelAnimation _animation)
	{
		if (serverSquare == null || serverSquare.projectID == string.Empty)
		{
			return;
		}
		project = _animation;
		if (project != null)
		{
			_boardsUsed = ((BloxelAnimation)project).boards.Count;
			bloxelsUsed = ((BloxelAnimation)project).GetBloxelCount();
			currentValue = ExchangeManager.instance.CalculateValue(bloxelsUsed);
			_animationFrameBoards = ((BloxelAnimation)project).boards;
			_frameRate = ((BloxelAnimation)project).GetFrameRate();
			shouldAnimate = true;
			int num = Mathf.FloorToInt(Time.time * _frameRate);
			if (_frameRate == 0f || _boardsUsed == 0)
			{
				_timerStart = Time.time;
				_currentFrameIndex = 0;
			}
			else
			{
				_timerStart = (float)num * (1f / _frameRate);
				_currentFrameIndex = num % _boardsUsed;
			}
			state = CanvasTileInfo.ReadyState.Ready;
		}
		else
		{
			state = CanvasTileInfo.ReadyState.Failed;
		}
	}

	public void CharacterParseComplete(BloxelCharacter _character)
	{
		if (serverSquare == null || serverSquare.projectID == string.Empty)
		{
			return;
		}
		project = _character;
		if (project != null)
		{
			bloxelsUsed = ((BloxelCharacter)project).GetBloxelCount();
			currentValue = ExchangeManager.instance.CalculateValue(bloxelsUsed);
			if (((BloxelCharacter)project).animations.ContainsKey(AnimationType.Idle))
			{
				_boardsUsed = ((BloxelCharacter)project).animations[AnimationType.Idle].boards.Count;
				_animationFrameBoards = ((BloxelCharacter)project).animations[AnimationType.Idle].boards;
				_frameRate = ((BloxelCharacter)project).animations[AnimationType.Idle].GetFrameRate();
				shouldAnimate = true;
				int num = Mathf.FloorToInt(Time.time * _frameRate);
				if (_frameRate == 0f || _boardsUsed == 0)
				{
					_timerStart = Time.time;
					_currentFrameIndex = 0;
				}
				else
				{
					_timerStart = (float)num * (1f / _frameRate);
					_currentFrameIndex = num % _boardsUsed;
				}
			}
			state = CanvasTileInfo.ReadyState.Ready;
		}
		else
		{
			state = CanvasTileInfo.ReadyState.Failed;
		}
	}

	private void GameParseComplete(BloxelGame _game)
	{
		_gameParseJob.OnComplete -= GameParseComplete;
		if (serverSquare != null && !(serverSquare.projectID == string.Empty))
		{
			project = _game;
			CanvasTileInfo.instance.progressContainer.DOAnchorPos(Vector2.zero, UIAnimationManager.speedFast).OnComplete(delegate
			{
				CanvasTileInfo.instance.progressBar.StopProgress();
			});
			if (project != null)
			{
				CanvasStreamingInterface.instance.details.InitForSquare(this);
				_boardsUsed = ((BloxelGame)project).UniqueBoards().Count;
				bloxelsUsed = ((BloxelGame)project).GetBloxelCount();
				currentValue = ExchangeManager.instance.CalculateValue(bloxelsUsed);
				roomsUsed = ((BloxelGame)project).levels.Count;
				CanvasStreamingInterface.instance.details.FadeInDectoratedBoard();
				state = CanvasTileInfo.ReadyState.Ready;
			}
			else
			{
				state = CanvasTileInfo.ReadyState.Failed;
			}
		}
	}

	public void SetTaken(bool isTaken)
	{
		if (isTaken)
		{
			taken = true;
			uiIconAdd.transform.localScale = Vector3.zero;
			SwitchOutline();
		}
		else
		{
			taken = false;
			uiIconAdd.transform.localScale = Vector3.one;
		}
	}

	private IEnumerator WWWTex(string texurl)
	{
		using (WWW www = new WWW(texurl))
		{
			yield return www;
			if (serverSquare.projectID != null && serverSquare.projectID != string.Empty)
			{
				www.LoadImageIntoTexture(downloadTexture);
				downloadTexture.filterMode = FilterMode.Point;
				SetSpriteToDownloadTexture();
			}
		}
	}

	private void SetSpriteToDownloadTexture()
	{
		_downloadColorBuffer = downloadTexture.GetPixels(4, 4, 26, 26);
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				Color color = _downloadColorBuffer[i * 2 * 26 + j * 2];
				_spriteColorBuffer[i * 13 + j] = color;
			}
		}
		sprite.texture.SetPixels((int)sprite.rect.x, (int)sprite.rect.y, 13, 13, _spriteColorBuffer);
	}

	private void ResetSpritePixels()
	{
		if (!(sprite == null))
		{
			sprite.texture.SetPixels32((int)sprite.rect.x, (int)sprite.rect.y, 13, 13, CanvasStreamingInterface.instance.blankSpriteColorBuffer);
		}
	}

	private void SetSpritePixelsWithBuffer13x13(Color32[] colorBuffer)
	{
		sprite.texture.SetPixels32((int)sprite.rect.x, (int)sprite.rect.y, 13, 13, colorBuffer);
	}

	public void OnPointerClick(PointerEventData pointerData)
	{
		if (Input.touchCount > 1 || _IsDragging)
		{
			return;
		}
		if (isActive)
		{
			DeActivate();
			return;
		}
		if (PlayerHUD.instance.infinityViewOn)
		{
			StopCoroutine("WaitForTransitionBeforeClick");
			StartCoroutine("WaitForTransitionBeforeClick");
			return;
		}
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		if (activeSquare != null)
		{
			activeSquare.DeActivate();
		}
		Viewer.instance.SquareAction(this);
	}

	private IEnumerator WaitForTransitionBeforeClick()
	{
		yield return PlayerHUD.instance.ForceTurnOffInfinityView();
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		if (activeSquare != null)
		{
			activeSquare.DeActivate();
		}
		Viewer.instance.SquareAction(this);
	}

	public void OnBeginDrag(PointerEventData pointerData)
	{
		if (!isActive && !_IsDragging && Input.touchCount <= 1)
		{
			_IsDragging = true;
			UIWarpDrive.instance.Move(Direction.Up, UIWarpDrive.instance.hiddenPosition);
			ExecuteEvents.beginDragHandler(CanvasStreamingInterface.instance, pointerData);
		}
	}

	public void OnDrag(PointerEventData pointerData)
	{
		if (!isActive && Input.touchCount <= 1)
		{
			ExecuteEvents.dragHandler(CanvasStreamingInterface.instance, pointerData);
		}
	}

	public void OnEndDrag(PointerEventData pointerData)
	{
		if (!isActive)
		{
			_IsDragging = false;
			UIWarpDrive.instance.Move(Direction.Down, Vector2.zero);
			ExecuteEvents.endDragHandler(CanvasStreamingInterface.instance, pointerData);
		}
	}
}
