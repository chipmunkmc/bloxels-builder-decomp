using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CanvasSquareDetails : MonoBehaviour
{
	public RectTransform selfRectTransform;

	public CanvasSquare currentSquare;

	public RectTransform toolContainer;

	public RectTransform backgroundImageTransform;

	public UIButton uiButtonDismiss;

	public UITriggerLikeTile uiTriggerLike;

	public UITriggerMap uiTriggerMap;

	public UITriggerShareTile uiTriggerShare;

	public UITriggerDeleteTile uiTriggerDelete;

	public Vector2 toolContainerNormal;

	public Vector2 toolContainerExpanded;

	public Image decoratedBoard;

	private Sprite _detailSprite;

	public void Init()
	{
		selfRectTransform = base.transform as RectTransform;
		Texture2D texture2D = new Texture2D(169, 169, TextureFormat.ARGB32, false);
		texture2D.filterMode = FilterMode.Point;
		texture2D.wrapMode = TextureWrapMode.Clamp;
		_detailSprite = Sprite.Create(texture2D, new Rect(0f, 0f, 169f, 169f), new Vector2(0.5f, 0.5f), 100f);
		decoratedBoard.sprite = _detailSprite;
	}

	public void InitForSquare(CanvasSquare square)
	{
		currentSquare = square;
		BloxelProject project = currentSquare.project;
		selfRectTransform.SetParent(currentSquare.rectTransform);
		selfRectTransform.SetAsLastSibling();
		backgroundImageTransform.SetParent(currentSquare.rectTransform);
		backgroundImageTransform.SetAsFirstSibling();
		selfRectTransform.anchoredPosition = Vector2.zero;
		toolContainer.anchoredPosition = Vector2.zero;
		backgroundImageTransform.anchoredPosition = Vector2.zero;
		uiButtonDismiss.OnClick -= HandleDismissButton;
		uiButtonDismiss.OnClick += HandleDismissButton;
		uiButtonDismiss.gameObject.SetActive(false);
		uiTriggerLike.Init(currentSquare);
		uiTriggerShare.Init(currentSquare);
		uiTriggerMap.Init(currentSquare);
		uiTriggerDelete.Init(currentSquare);
		uiTriggerDelete.transform.localScale = Vector3.zero;
		if (project != null && project.type == ProjectType.Game)
		{
			AdjustUI_Game();
		}
		else
		{
			AdjustUI_NonGame();
		}
		base.gameObject.SetActive(true);
		toolContainer.DOAnchorPos(new Vector2(toolContainer.sizeDelta.x * -1f, 0f), UIAnimationManager.speedMedium).OnComplete(delegate
		{
			uiButtonDismiss.gameObject.SetActive(true);
		});
		if (currentSquare.mine)
		{
			uiTriggerDelete.transform.DOScale(1f, UIAnimationManager.speedMedium);
		}
		decoratedBoard.color = new Color(1f, 1f, 1f, 0f);
		if (project != null && (project.type == ProjectType.Game || project.type == ProjectType.MegaBoard))
		{
			FadeInDectoratedBoard();
		}
	}

	private void HandleDismissButton()
	{
		currentSquare.OnPointerClick(null);
	}

	private void AdjustUI_Game()
	{
		toolContainer.sizeDelta = toolContainerExpanded;
		uiTriggerMap.gameObject.SetActive(true);
	}

	private void AdjustUI_NonGame()
	{
		toolContainer.sizeDelta = toolContainerNormal;
		uiTriggerMap.gameObject.SetActive(false);
	}

	public void Dismiss(bool immediate = false)
	{
		if (currentSquare == null)
		{
			UIWarpDrive.instance.Move(Direction.Down, Vector2.zero);
			toolContainer.anchoredPosition = Vector2.zero;
			uiButtonDismiss.gameObject.SetActive(false);
			if (backgroundImageTransform != null)
			{
				backgroundImageTransform.SetParent(selfRectTransform);
				backgroundImageTransform.anchoredPosition = Vector2.zero;
				backgroundImageTransform.SetAsLastSibling();
			}
			base.gameObject.SetActive(false);
			return;
		}
		if (currentSquare.mine)
		{
			uiTriggerDelete.transform.DOScale(0f, UIAnimationManager.speedFast);
		}
		UIWarpDrive.instance.Move(Direction.Down, Vector2.zero);
		if (!immediate)
		{
			toolContainer.DOAnchorPos(Vector2.zero, UIAnimationManager.speedFast).OnStart(delegate
			{
				uiButtonDismiss.gameObject.SetActive(false);
			}).OnComplete(delegate
			{
				CanvasStreamingInterface.instance.SquareDeActivate();
				FadeOutDecoratedBoard();
				currentSquare.selfTransform.DOScale(Vector3.one, UIAnimationManager.speedFast).OnStart(delegate
				{
					currentSquare.rectTransform.DOAnchorPos(new Vector2(currentSquare.rectTransform.anchoredPosition.x, currentSquare.rectTransform.anchoredPosition.y - 70f), UIAnimationManager.speedFast);
				}).OnComplete(delegate
				{
					backgroundImageTransform.SetParent(selfRectTransform);
					backgroundImageTransform.anchoredPosition = Vector2.zero;
					backgroundImageTransform.SetAsLastSibling();
					base.gameObject.SetActive(false);
				});
			});
		}
		else
		{
			toolContainer.anchoredPosition = Vector2.zero;
			uiButtonDismiss.gameObject.SetActive(false);
			CanvasStreamingInterface.instance.SquareDeActivate();
			FadeOutDecoratedBoard();
			currentSquare.selfTransform.localScale = Vector3.one;
			currentSquare.rectTransform.anchoredPosition = new Vector2(currentSquare.rectTransform.anchoredPosition.x, currentSquare.rectTransform.anchoredPosition.y - 70f);
			backgroundImageTransform.SetParent(selfRectTransform);
			backgroundImageTransform.anchoredPosition = Vector2.zero;
			backgroundImageTransform.SetAsLastSibling();
			base.gameObject.SetActive(false);
		}
	}

	public void FadeInDectoratedBoard()
	{
		if (!(currentSquare == null) && currentSquare.project != null)
		{
			if (currentSquare.project.type == ProjectType.Game)
			{
				((BloxelGame)currentSquare.project).SetCoverTexturePixels(_detailSprite, BloxelBoard.baseUIBoardColor);
			}
			else if (currentSquare.project.type == ProjectType.MegaBoard)
			{
				((BloxelMegaBoard)currentSquare.project).SetCoverTexturePixels(_detailSprite, BloxelBoard.baseUIBoardColor);
			}
			currentSquare.image.DOKill(true);
			decoratedBoard.DOKill(true);
			currentSquare.image.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				decoratedBoard.enabled = true;
				decoratedBoard.DOFade(1f, UIAnimationManager.speedMedium);
			}).OnComplete(delegate
			{
				currentSquare.image.enabled = false;
			});
		}
	}

	public void FadeOutDecoratedBoard()
	{
		if (!(currentSquare == null))
		{
			currentSquare.image.DOKill(true);
			decoratedBoard.DOKill(true);
			currentSquare.image.enabled = true;
			currentSquare.image.DOFade(1f, UIAnimationManager.speedMedium);
			decoratedBoard.DOFade(0f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				decoratedBoard.enabled = false;
			});
		}
	}
}
