using System;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VoxelEngine;

public class CanvasStreamingInterface : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleRowOrColumnShift();

	public static CanvasStreamingInterface instance;

	private RectTransform selfRectTransform;

	public GameObject canvasSquarePrefab;

	public RectTransform squaresContainerRectTransform;

	public RectTransform uiCanvasRectTransform;

	public CanvasSquareDetails details;

	public CanvasScaler canvasScaler;

	public Camera streamingCamera;

	public RawImage overlay;

	public Text uiTextLoading;

	public float cameraTrackingSpeed;

	public int squarePositionsWidth;

	public int squarePositionsHeight;

	public CanvasSquare[,] canvasSquares;

	public WorldPos2D[] squarePositions;

	public Dictionary<WorldPos2D, CanvasSquare> canvasSquaresDict;

	public int squareUpdateBufferSize = 4;

	public float canvasSquareSize;

	public float canvasWorldSpanX;

	public float canvasWorldSpanY;

	public float halfSpanY;

	public float halfSpanX;

	public WorldPos2D screenCenter;

	private bool isDragging;

	public CanvasSquare activeSquare;

	private Vector2 movementReference = Vector2.zero;

	private float squareSizeWithPadding = 10f;

	private float timeOfLastCoordUpdate;

	private float movingCoordinateUpdateInterval = 0.1f;

	private float idleCoordinateUpdateInterval = 1.5f;

	public Texture2D squareAtlas;

	public Color32[] blankSpriteColorBuffer;

	public Texture2D levelDetailTexture;

	private Color32[] clearDetailColorBuffer;

	private Color[] boardColorBuffer = new Color[169];

	public Texture2D levelBackgroundDetailTexture;

	public Texture2D gameBackgroundDetailTexture;

	public Vector3 cameraPosOnLastShift;

	public float squarePadding;

	private Vector2 targetAnchoredPosOffset = Vector2.zero;

	public float originalSquareSize;

	private float enlargedSquareSize;

	public float originalSquareScale = 1f;

	public float enlargedSquareScale;

	public event HandleRowOrColumnShift OnRowOrColumnShift;

	private void Awake()
	{
		instance = this;
		details.Init();
		CanvasSquare.downloadTexture = new Texture2D(34, 34, TextureFormat.ARGB32, false);
		Input.simulateMouseWithTouches = true;
		selfRectTransform = GetComponent<RectTransform>();
		targetAnchoredPosOffset = Vector2.zero;
		canvasSquares = new CanvasSquare[squarePositionsWidth, squarePositionsHeight];
		canvasSquaresDict = new Dictionary<WorldPos2D, CanvasSquare>(squarePositionsWidth * squarePositionsHeight);
		squarePositions = new WorldPos2D[squarePositionsWidth * squarePositionsHeight];
		squarePositions = new WorldPos2D[squarePositionsWidth * squarePositionsHeight];
		for (int i = 0; i < squarePositionsHeight; i++)
		{
			for (int j = 0; j < squarePositionsWidth; j++)
			{
				squarePositions[i * squarePositionsHeight + j] = new WorldPos2D(j - Mathf.FloorToInt((float)squarePositionsWidth / 2f), i - Mathf.FloorToInt((float)squarePositionsHeight / 2f));
			}
		}
		Array.Sort(squarePositions, (WorldPos2D s1, WorldPos2D s2) => s1.CompareDistanceToOrigin(s2));
		blankSpriteColorBuffer = new Color32[169];
		for (int k = 0; k < 169; k++)
		{
			blankSpriteColorBuffer[k] = BloxelBoard.baseUIBoardColor32;
		}
		squareAtlas = new Texture2D(256, 256, TextureFormat.ARGB32, false);
		squareAtlas.filterMode = FilterMode.Point;
		for (int l = 0; l < 256; l++)
		{
			for (int m = 0; m < 256; m++)
			{
				squareAtlas.SetPixel(m, l, BloxelBoard.baseUIBoardColor);
			}
		}
		for (int n = 0; n < 13; n++)
		{
			for (int num = 0; num < 13; num++)
			{
				squareAtlas.SetPixels32(num * 14 + 1, n * 14 + 1, 13, 13, blankSpriteColorBuffer);
			}
		}
		squareAtlas.Apply();
		Color32 color = Color.clear;
		clearDetailColorBuffer = new Color32[169];
		for (int num2 = 0; num2 < 169; num2++)
		{
			clearDetailColorBuffer[num2] = color;
		}
		levelDetailTexture = new Texture2D(169, 169, TextureFormat.ARGB32, false);
		levelDetailTexture.filterMode = FilterMode.Point;
		levelDetailTexture.wrapMode = TextureWrapMode.Clamp;
		levelBackgroundDetailTexture = new Texture2D(169, 169, TextureFormat.ARGB32, false);
		levelBackgroundDetailTexture.filterMode = FilterMode.Point;
		levelBackgroundDetailTexture.wrapMode = TextureWrapMode.Clamp;
		gameBackgroundDetailTexture = new Texture2D(169, 169, TextureFormat.ARGB32, false);
		gameBackgroundDetailTexture.filterMode = FilterMode.Point;
		gameBackgroundDetailTexture.wrapMode = TextureWrapMode.Clamp;
		for (int num3 = 0; num3 < 13; num3++)
		{
			for (int num4 = 0; num4 < 13; num4++)
			{
				levelDetailTexture.SetPixels32(num4 * 13, num3 * 13, 13, 13, clearDetailColorBuffer);
				levelBackgroundDetailTexture.SetPixels32(num4 * 13, num3 * 13, 13, 13, clearDetailColorBuffer);
				gameBackgroundDetailTexture.SetPixels32(num4 * 13, num3 * 13, 13, 13, clearDetailColorBuffer);
			}
		}
		levelDetailTexture.Apply();
		levelBackgroundDetailTexture.Apply();
		gameBackgroundDetailTexture.Apply();
	}

	private void Start()
	{
		InitializeCanvasSquares();
		overlay.transform.SetAsLastSibling();
		overlay.enabled = false;
		uiTextLoading.enabled = false;
	}

	private void OnDestroy()
	{
		if (string.IsNullOrEmpty(PrefsManager.instance.iWallWarpLocation))
		{
			PrefsManager.instance.shouldActiveAfterWarp = false;
			PrefsManager.instance.iWallWarpLocation = screenCenter.CoordinateConvert();
		}
		Unload();
	}

	private void Unload()
	{
		instance = null;
		selfRectTransform = null;
		uiCanvasRectTransform = null;
		canvasScaler = null;
		streamingCamera = null;
		overlay = null;
		uiTextLoading = null;
		canvasSquares = null;
		squarePositions = null;
		canvasSquaresDict = null;
		activeSquare = null;
		squareAtlas = null;
		blankSpriteColorBuffer = null;
		levelDetailTexture = null;
		clearDetailColorBuffer = null;
		boardColorBuffer = null;
		levelBackgroundDetailTexture = null;
		gameBackgroundDetailTexture = null;
	}

	public void SetCanvasSquareWorldSize()
	{
		squareSizeWithPadding = canvasSquareSize + squarePadding * 2f;
		canvasWorldSpanX = squareSizeWithPadding * (float)squarePositionsWidth;
		halfSpanX = canvasWorldSpanX / 2f;
		canvasWorldSpanY = squareSizeWithPadding * (float)squarePositionsHeight;
		halfSpanY = canvasWorldSpanY / 2f;
		enlargedSquareSize = squareSizeWithPadding - 1f;
		enlargedSquareScale = enlargedSquareSize / originalSquareSize;
	}

	public void SquareActivate(CanvasSquare square)
	{
		activeSquare = square;
		targetAnchoredPosOffset = -square.rectTransform.anchoredPosition;
		overlay.transform.SetAsLastSibling();
		overlay.enabled = true;
		uiTextLoading.enabled = true;
	}

	public void HideLoadingText()
	{
		uiTextLoading.enabled = false;
	}

	public void SquareDeActivate()
	{
		overlay.enabled = false;
	}

	private void InitializeCanvasSquares()
	{
		SetCanvasSquareWorldSize();
		int length = canvasSquares.GetLength(1);
		int length2 = canvasSquares.GetLength(0);
		int num = Mathf.FloorToInt((float)length2 / 2f);
		int num2 = Mathf.FloorToInt((float)length / 2f);
		for (int i = 0; i < length; i++)
		{
			for (int j = 0; j < length2; j++)
			{
				CanvasSquare component = UnityEngine.Object.Instantiate(canvasSquarePrefab).GetComponent<CanvasSquare>();
				component.selfTransform.SetParent(squaresContainerRectTransform, true);
				component.rectTransform.anchoredPosition = new Vector2(((float)j - (float)length2 / 2f) * squareSizeWithPadding, ((float)i - (float)length / 2f) * squareSizeWithPadding);
				component.rectTransform.sizeDelta = new Vector2(canvasSquareSize, canvasSquareSize);
				component.rectTransform.localScale = Vector3.one;
				WorldPos2D coordinate = new WorldPos2D(j - num, i - num2);
				component.SetNewCoordinates(coordinate, true);
				canvasSquares[j, i] = component;
				component.sprite = Sprite.Create(instance.squareAtlas, new Rect(j * 14 + 1, i * 14 + 1, 13f, 13f), new Vector2(0.5f, 0.5f), 100f);
				component.image.sprite = component.sprite;
				canvasSquaresDict[component.iWallCoordinate] = component;
			}
		}
	}

	public void OnBeginDrag(PointerEventData pointerData)
	{
		if (Input.touchCount <= 1)
		{
			isDragging = true;
			targetAnchoredPosOffset = Vector2.zero;
		}
	}

	public void OnDrag(PointerEventData pointerData)
	{
		if (Input.touchCount <= 1)
		{
			targetAnchoredPosOffset += pointerData.delta * (canvasScaler.referenceResolution.y / (float)Screen.height);
		}
	}

	public void OnEndDrag(PointerEventData pointerData)
	{
		isDragging = false;
	}

	public void OnPointerClick(PointerEventData pointerData)
	{
		if (Input.touchCount <= 1 && !isDragging)
		{
		}
	}

	public void GoToCoordinates(int xCoordinate, int yCoordinate, bool shouldActivate = true)
	{
		int num = squarePositionsHeight;
		int num2 = squarePositionsWidth;
		int num3 = Mathf.FloorToInt((float)num2 / 2f);
		int num4 = Mathf.FloorToInt((float)num / 2f);
		CanvasSquare canvasSquare = null;
		float num5 = squareSizeWithPadding / 2f;
		canvasSquaresDict.Clear();
		for (int i = 0; i < num; i++)
		{
			for (int j = 0; j < num2; j++)
			{
				CanvasSquare canvasSquare2 = canvasSquares[j, i];
				canvasSquare2.rectTransform.anchoredPosition = new Vector2(((float)j - (float)num2 / 2f) * squareSizeWithPadding + num5, ((float)i - (float)num / 2f) * squareSizeWithPadding + num5);
				WorldPos2D coordinate = new WorldPos2D(j + xCoordinate - num3, i + yCoordinate - num4);
				canvasSquare2.SetNewCoordinates(coordinate, true);
				if (canvasSquare2.iWallCoordinate.x == xCoordinate && canvasSquare2.iWallCoordinate.y == yCoordinate)
				{
					canvasSquare = canvasSquare2;
				}
			}
		}
		if (shouldActivate)
		{
			canvasSquare.Activate();
		}
	}

	private void LateUpdate()
	{
		UpdateSquarePositions();
		CheckForRowOrColumnShifts();
		FindSquaresToUpdate();
		squareAtlas.Apply();
	}

	public void TransitionSquaresScale(float startAlpha, float endAlpha, float startScale, float endScale, float speed)
	{
		float currentScale = startScale;
		DOTween.To(() => startScale, delegate(float x)
		{
			currentScale = x;
		}, endScale, speed).OnUpdate(delegate
		{
			for (int m = 0; m < squarePositionsHeight; m++)
			{
				for (int n = 0; n < squarePositionsWidth; n++)
				{
					canvasSquares[n, m].image.rectTransform.localScale = new Vector3(currentScale, currentScale, 1f);
				}
			}
		}).OnStart(delegate
		{
			float currentAlpha = startAlpha;
			DOTween.To(() => startAlpha, delegate(float x)
			{
				currentAlpha = x;
			}, endAlpha, speed).OnUpdate(delegate
			{
				for (int k = 0; k < squarePositionsHeight; k++)
				{
					for (int l = 0; l < squarePositionsWidth; l++)
					{
						canvasSquares[l, k].container.alpha = currentAlpha;
					}
				}
			});
		})
			.OnComplete(delegate
			{
				bool raycastTarget = endScale != 1f;
				for (int i = 0; i < squarePositionsHeight; i++)
				{
					for (int j = 0; j < squarePositionsWidth; j++)
					{
						canvasSquares[j, i].image.raycastTarget = raycastTarget;
					}
				}
			});
	}

	public void UpdateSquarePositions()
	{
		if (targetAnchoredPosOffset == Vector2.zero)
		{
			return;
		}
		Vector2 vector = Vector2.Lerp(Vector2.zero, targetAnchoredPosOffset, cameraTrackingSpeed * Time.deltaTime);
		targetAnchoredPosOffset -= vector;
		movementReference += vector;
		for (int i = 0; i < squarePositionsHeight; i++)
		{
			for (int j = 0; j < squarePositionsWidth; j++)
			{
				canvasSquares[j, i].rectTransform.anchoredPosition += vector;
			}
		}
	}

	public void CheckForRowOrColumnShifts()
	{
		Vector2 vector = new Vector2((float)Mathf.FloorToInt(Mathf.Abs(movementReference.x / squareSizeWithPadding)) * squareSizeWithPadding * Mathf.Sign(movementReference.x), (float)Mathf.FloorToInt(Mathf.Abs(movementReference.y / squareSizeWithPadding)) * squareSizeWithPadding * Mathf.Sign(movementReference.y));
		if (vector != Vector2.zero)
		{
			movementReference -= vector;
			if (this.OnRowOrColumnShift != null)
			{
				this.OnRowOrColumnShift();
			}
		}
	}

	public void FindSquaresToUpdate()
	{
		CanvasSquare canvasSquare = null;
		float num = float.PositiveInfinity;
		for (int i = 0; i < squarePositionsWidth; i++)
		{
			for (int j = 0; j < squarePositionsHeight; j++)
			{
				CanvasSquare canvasSquare2 = canvasSquares[i, j];
				float sqrMagnitude = canvasSquare2.rectTransform.anchoredPosition.sqrMagnitude;
				if (sqrMagnitude < num)
				{
					canvasSquare = canvasSquare2;
					num = sqrMagnitude;
				}
			}
		}
		screenCenter = canvasSquare.iWallCoordinate;
		float sqrMagnitude2 = movementReference.sqrMagnitude;
		float num2 = movingCoordinateUpdateInterval;
		if (sqrMagnitude2 <= 1f)
		{
			num2 = idleCoordinateUpdateInterval;
		}
		if (timeOfLastCoordUpdate + num2 < Time.time)
		{
			BloxelServerInterface.instance.Emit_CoordinateUpdate(screenCenter);
			timeOfLastCoordUpdate = Time.time;
		}
		int num3 = squarePositions.Length;
		int num4 = 0;
		for (int k = 0; k < num3; k++)
		{
			if (num4 > squareUpdateBufferSize)
			{
				break;
			}
			WorldPos2D key = squarePositions[k];
			key.x += screenCenter.x;
			key.y += screenCenter.y;
			CanvasSquare value = null;
			if (!canvasSquaresDict.TryGetValue(key, out value))
			{
				continue;
			}
			JSONObject value2 = null;
			if (Viewer.instance.squarePositionToBarf.TryGetValue(key, out value2))
			{
				string str = value2["projectID"].str;
				if (value.serverSquare == null || !(value.serverSquare.projectID == str))
				{
					value.UpdateData(value2);
					num4++;
				}
			}
		}
	}

	public void SetDetailTexturesForGame(BloxelGame game, out Texture2D levelTex, out Texture2D levelBkgdTex)
	{
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = game.levels.GetEnumerator();
		BloxelLevel bloxelLevel = null;
		while (enumerator.MoveNext())
		{
			BloxelLevel value = enumerator.Current.Value;
			if (value.id.Equals(game.heroStartPosition.id))
			{
				bloxelLevel = value;
				break;
			}
		}
		SetLevelDetailTexture(bloxelLevel);
		SetLevelBackgroundDetailTexture(bloxelLevel);
		levelTex = levelDetailTexture;
		levelBkgdTex = levelBackgroundDetailTexture;
	}

	public void SetLevelDetailTexture(BloxelLevel level)
	{
		if (level == null)
		{
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					levelDetailTexture.SetPixels32(j * 13, i * 13, 13, 13, clearDetailColorBuffer);
				}
			}
			levelDetailTexture.Apply();
			return;
		}
		BlockColor[,] blockColors = level.coverBoard.blockColors;
		for (int k = 0; k < 13; k++)
		{
			for (int l = 0; l < 13; l++)
			{
				BlockColor blockColor = blockColors[l, k];
				if (blockColor == BlockColor.Blank)
				{
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, clearDetailColorBuffer);
					continue;
				}
				BloxelProject projectModelAtLocation = level.GetProjectModelAtLocation(new GridLocation(l, k));
				if (projectModelAtLocation != null)
				{
					if (projectModelAtLocation.coverBoard != null)
					{
						projectModelAtLocation.coverBoard.GetColorsNonAlloc(ref boardColorBuffer);
						levelDetailTexture.SetPixels(l * 13, k * 13, 13, 13, boardColorBuffer);
					}
					else
					{
						levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, clearDetailColorBuffer);
					}
					continue;
				}
				switch (blockColor)
				{
				case BlockColor.Red:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidRed.GetPixels32());
					break;
				case BlockColor.Orange:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidOrange.GetPixels32());
					break;
				case BlockColor.Yellow:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidYellow.GetPixels32());
					break;
				case BlockColor.Green:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidGreen.GetPixels32());
					break;
				case BlockColor.Blue:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidBlue.GetPixels32());
					break;
				case BlockColor.Purple:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidPurple.GetPixels32());
					break;
				case BlockColor.Pink:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidPink.GetPixels32());
					break;
				case BlockColor.White:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, AssetManager.instance.boardSolidWhite.GetPixels32());
					break;
				default:
					levelDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, clearDetailColorBuffer);
					break;
				}
			}
		}
		levelDetailTexture.Apply();
	}

	public void SetLevelBackgroundDetailTexture(BloxelLevel level)
	{
		if (level == null)
		{
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					levelBackgroundDetailTexture.SetPixels32(j * 13, i * 13, 13, 13, clearDetailColorBuffer);
				}
			}
			levelBackgroundDetailTexture.Apply();
			return;
		}
		for (int k = 0; k < 13; k++)
		{
			for (int l = 0; l < 13; l++)
			{
				GridLocation key = new GridLocation(l, k);
				BloxelBoard value = null;
				if (level.backgroundTiles.TryGetValue(key, out value))
				{
					value.GetColorsNonAlloc(ref boardColorBuffer);
					levelBackgroundDetailTexture.SetPixels(l * 13, k * 13, 13, 13, boardColorBuffer);
				}
				else
				{
					levelBackgroundDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, clearDetailColorBuffer);
				}
			}
		}
		levelBackgroundDetailTexture.Apply();
	}

	public void SetGameBackgroundDetailTexture(BloxelMegaBoard gameBackground, out Texture2D megaTex)
	{
		if (gameBackground == null)
		{
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					gameBackgroundDetailTexture.SetPixels32(j * 13, i * 13, 13, 13, clearDetailColorBuffer);
				}
			}
			gameBackgroundDetailTexture.Apply();
			megaTex = gameBackgroundDetailTexture;
			return;
		}
		for (int k = 0; k < 13; k++)
		{
			for (int l = 0; l < 13; l++)
			{
				GridLocation key = new GridLocation(l, k);
				BloxelBoard value = null;
				if (gameBackground.boards.TryGetValue(key, out value))
				{
					value.GetColorsNonAlloc(ref boardColorBuffer);
					gameBackgroundDetailTexture.SetPixels(l * 13, k * 13, 13, 13, boardColorBuffer);
				}
				else
				{
					gameBackgroundDetailTexture.SetPixels32(l * 13, k * 13, 13, 13, clearDetailColorBuffer);
				}
			}
		}
		gameBackgroundDetailTexture.Apply();
		megaTex = gameBackgroundDetailTexture;
	}
}
