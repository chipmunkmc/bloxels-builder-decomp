using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CanvasTileInfo : MonoBehaviour
{
	public enum ReadyState
	{
		Requested = 0,
		Waiting = 1,
		Ready = 2,
		Failed = 3,
		Downloading = 4
	}

	public static CanvasTileInfo instance;

	[Header("| ========= Movable ========= |")]
	public bool isVisible;

	public RectTransform rect;

	public Vector2 hiddenPosition;

	public Vector2 visiblePosition;

	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	public bool isGame;

	private bool isReady;

	[Header("| ========= UI ========= |")]
	public Text uiTextType;

	public Text uiTextPlayCount;

	public UGCAuth ugcTextTitle;

	public string string_playCountPrefix = "(";

	public string string_playCountSuffix = " Plays)";

	public string justABoard = "Just a Board";

	public string noTitle = "No Title";

	public string loadingString = "Downloading...";

	public string parsingString = "Un-Packing...";

	public string requestedString = "Requested...";

	public RectTransform progressContainer;

	public UIProgressBar progressBar;

	public Text uiTextProgressInfo;

	public UITriggerBuyAsset uiTriggerBuy;

	public UITriggerViewGameAssets uiTriggerViewAssets;

	public UITriggerPlayGame uiTriggerPlay;

	public UITriggerUserProfile uiTriggerUser;

	public UITriggerReportTile uiTriggerReport;

	public UICanvasHeroAnimation uiAnimatedHero;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		ResetInfo();
		string_playCountSuffix = " " + LocalizationManager.getInstance().getLocalizedText("iWall20", "Plays") + ")";
		justABoard = LocalizationManager.getInstance().getLocalizedText("iWall105", "Just a Board");
		noTitle = LocalizationManager.getInstance().getLocalizedText("iWall23", "No Title");
		loadingString = LocalizationManager.getInstance().getLocalizedText("iWall106", "Downloading...");
		parsingString = LocalizationManager.getInstance().getLocalizedText("iWall50", "Un-Packing...");
		requestedString = LocalizationManager.getInstance().getLocalizedText("iWall49", "Requested...");
	}

	private void ResetInfo()
	{
		isReady = false;
		ugcTextTitle.uiText.text = "---";
		uiTextType.text = "---";
		uiTextPlayCount.text = "---";
		uiTriggerViewAssets.transform.localScale = Vector3.zero;
	}

	public void Populate(CanvasSquare _square)
	{
		square = _square;
		square.OnStateChange += HandleOnStateChange;
		SetupType();
		CheckGame();
		if (square.project != null)
		{
			isReady = true;
			SetTitle();
			SetReady(square.state);
			uiTriggerBuy.Init(square);
			uiTriggerPlay.Init(square);
			if (isGame)
			{
				uiAnimatedHero.Init(square);
			}
		}
		uiTriggerUser.Init(square.serverSquare.owner);
		uiTriggerReport.Init(square);
		Show();
	}

	private void HandleOnStateChange(ReadyState _val)
	{
		if (isVisible)
		{
			SetReady(square.state);
		}
	}

	public void ShakePlay()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.popC);
		uiTriggerPlay.transform.DOShakePosition(UIAnimationManager.speedMedium, 10f, 20, 40f);
	}

	private void SetTitle()
	{
		if (string.IsNullOrEmpty(square.project.title))
		{
			ugcTextTitle.uiText.text = noTitle;
		}
		else
		{
			ugcTextTitle.Init(square.project);
		}
		if (square.project.type == ProjectType.Board)
		{
			ugcTextTitle.uiText.text = justABoard;
		}
	}

	private void CheckGame()
	{
		if (square.serverSquare.type != ProjectType.Game)
		{
			isGame = false;
		}
		else
		{
			isGame = true;
		}
		uiTriggerBuy.gameObject.SetActive(!isGame);
		uiTriggerViewAssets.gameObject.SetActive(isGame);
		uiTriggerPlay.uiButton.interactable = !isGame;
		uiTextPlayCount.gameObject.SetActive(isGame);
		uiTriggerPlay.gameObject.SetActive(isGame);
	}

	public void SetupPlays()
	{
		uiTextPlayCount.gameObject.SetActive(true);
		StartCoroutine(BloxelServerRequests.instance.GetPlays(square.serverSquare.projectID, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				uiTextPlayCount.text = string_playCountPrefix + response + string_playCountSuffix;
			}
			else
			{
				uiTextPlayCount.text = string.Empty;
			}
		}));
	}

	private void SetupType()
	{
		switch (square.serverSquare.type)
		{
		case ProjectType.Board:
			uiTextType.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange);
			break;
		case ProjectType.Animation:
			uiTextType.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue);
			break;
		case ProjectType.MegaBoard:
			uiTextType.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green);
			break;
		case ProjectType.Character:
			uiTextType.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
			break;
		case ProjectType.Game:
			uiTextType.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Purple);
			break;
		}
		ProjectType type = square.serverSquare.type;
		string unityKey = BloxelProject.projectTypeDisplayKeys[type];
		string localizedText = LocalizationManager.getInstance().getLocalizedText(unityKey, type.ToString());
		uiTextType.text = localizedText;
	}

	public void SetReady(ReadyState state)
	{
		switch (state)
		{
		case ReadyState.Requested:
			uiTextProgressInfo.text = requestedString;
			break;
		case ReadyState.Downloading:
			uiTextProgressInfo.text = loadingString;
			break;
		case ReadyState.Waiting:
			uiTextProgressInfo.text = parsingString;
			break;
		case ReadyState.Ready:
			if (square.project == null)
			{
				break;
			}
			uiTextProgressInfo.text = requestedString;
			if (!isReady)
			{
				SetTitle();
				uiTriggerBuy.Init(square);
				uiTriggerViewAssets.Init(square);
				uiTriggerPlay.Init(square);
				if (isGame)
				{
					uiTriggerViewAssets.transform.localScale = Vector3.one;
					uiAnimatedHero.Init(square);
				}
				isReady = true;
			}
			if (isGame)
			{
				SetupPlays();
			}
			break;
		case ReadyState.Failed:
			break;
		}
	}

	public void HideProgress()
	{
		progressContainer.DOAnchorPos(Vector2.zero, UIAnimationManager.speedFast).OnStart(delegate
		{
			progressBar.Reset();
		});
	}

	public void ShowProgress()
	{
		progressContainer.DOAnchorPos(new Vector2(0f, 100f), UIAnimationManager.speedFast).OnComplete(delegate
		{
			progressBar.FakeIncrement();
		});
	}

	public void Show()
	{
		rect.DOAnchorPos(visiblePosition, UIAnimationManager.speedFast).OnComplete(delegate
		{
			isVisible = true;
		});
	}

	public void Hide(bool hideSquare = false)
	{
		if (!(square == null))
		{
			if (hideSquare)
			{
				square.DeActivate(!hideSquare);
			}
			HideProgress();
			uiAnimatedHero.Reset();
			rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedFast).OnComplete(delegate
			{
				isVisible = false;
				square.OnStateChange -= HandleOnStateChange;
				square = null;
				ResetInfo();
			});
		}
	}
}
