using System;
using System.Collections;
using System.Threading;
using DiscoCapture;
using OpenCVForUnity;
using UnityEngine;

[RequireComponent(typeof(CaptureController))]
public class CaptureManager : MonoBehaviour
{
	public delegate void HandleCaptureConfirm();

	public delegate void HandleDismiss();

	public delegate void HandleRepeatCapture();

	public static CaptureManager instance;

	public CaptureController captureController;

	public UIPopupCapture uiPopupCapture;

	public static BlockColor[,] latest;

	public UnityEngine.Object captureWindowPrefab;

	private UnityEngine.Object _cameraSettingsPrefab;

	public bool gameboardActivationInProgress;

	public static bool isReady;

	private static bool initComplete;

	public bool shouldProcess;

	public Texture2D displayTexture;

	public Color32[] displayColors;

	private int _currentFrameIndex;

	public static string DataVersion;

	public event HandleCaptureConfirm OnCaptureConfirm;

	public event HandleDismiss OnCaptureDismiss;

	public event HandleRepeatCapture OnCaptureRepeat;

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else if (!initComplete)
		{
			instance = this;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
			_cameraSettingsPrefab = Resources.Load("Prefabs/UIPopupCameraSettings");
			if (captureController == null)
			{
				captureController = GetComponent<CaptureController>();
			}
			DataVersion = PrefsManager.instance.captureVersion;
		}
	}

	private void Start()
	{
		if (!initComplete)
		{
			captureController.deviceCamera.OnCachedCameraProperties += handleCameraInitComplete;
			StartCoroutine(managerInit());
		}
	}

	private void OnDestroy()
	{
		captureController.deviceCamera.OnCachedCameraProperties -= handleCameraInitComplete;
	}

	private void Update()
	{
		if (shouldProcess && DeviceCameraController.FilledCameraMat)
		{
			Process();
		}
	}

	public UIPopupCapture Show()
	{
		TryHideNav();
		uiPopupCapture = UIOverlayCanvas.instance.Popup(captureWindowPrefab).GetComponent<UIPopupCapture>();
		SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
		uiPopupCapture.captureController = captureController;
		uiPopupCapture.OnInit += HandlePopupInit;
		return uiPopupCapture;
	}

	public UIPopupCapture CaptureBrain(BloxelEnemyTile tile)
	{
		TryHideNav();
		uiPopupCapture = UIOverlayCanvas.instance.Popup(captureWindowPrefab).GetComponent<UIPopupCapture>();
		SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
		uiPopupCapture.captureController = captureController;
		uiPopupCapture.OnInit += HandlePopupInit;
		uiPopupCapture.isBrainCapture = true;
		uiPopupCapture.enemyTile = tile;
		return uiPopupCapture;
	}

	public void CaptureConfirm()
	{
		latest = ColorClassifier.BlockColors.Clone() as BlockColor[,];
		Reset();
		if (this.OnCaptureConfirm != null)
		{
			this.OnCaptureConfirm();
		}
		TryShowNav();
	}

	public void CaptureRepeat()
	{
		_currentFrameIndex = 0;
		captureController.deviceCamera.webcamTexture.Play();
		shouldProcess = true;
		if (this.OnCaptureRepeat != null)
		{
			this.OnCaptureRepeat();
		}
	}

	public void TryShowNav()
	{
		if (!(HomeController.instance == null))
		{
			HomeController.instance.menu.transform.localScale = HomeController.instance.initialMenuScale;
			HomeController.instance.TryShowCaptureIndicators();
		}
	}

	public void TryHideNav()
	{
		if (!(HomeController.instance == null))
		{
			HomeController.instance.menu.transform.localScale = Vector3.zero;
			HomeController.instance.TryHideCaptureIndicators();
		}
	}

	public void DismissPopup()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.cancelA);
		Reset();
		if (this.OnCaptureDismiss != null)
		{
			this.OnCaptureDismiss();
		}
		TryShowNav();
	}

	public void ResetForNewTemplate()
	{
		_currentFrameIndex = 0;
		CaptureController.Instance.ClearImageCache();
	}

	private void Reset()
	{
		if (!(uiPopupCapture == null))
		{
			shouldProcess = false;
			_currentFrameIndex = 0;
			uiPopupCapture.OnInit -= HandlePopupInit;
			captureController.deviceCamera.webcamTexture.Pause();
			uiPopupCapture.Dismiss();
		}
	}

	public void ResetOnProcessComplete()
	{
		shouldProcess = false;
		_currentFrameIndex = 0;
		captureController.deviceCamera.webcamTexture.Pause();
		DeviceCameraController.frameCount = 0;
	}

	public void ForceClose()
	{
		if (!(uiPopupCapture == null))
		{
			uiPopupCapture.OnInit -= HandlePopupInit;
			shouldProcess = false;
			captureController.deviceCamera.webcamTexture.Pause();
			TryShowNav();
			UnityEngine.Object.DestroyImmediate(uiPopupCapture.gameObject);
		}
	}

	public void ShowCameraMessage()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(_cameraSettingsPrefab);
	}

	private void HandlePopupInit()
	{
		ToggleCaptureFlip(PrefsManager.instance.shouldFlipCamera);
		captureController.deviceCamera.webcamTexture.Play();
		StartCoroutine("initPopupFeed");
	}

	private void HandleCaptureButtonPress()
	{
		Show();
	}

	private void handleCameraInitComplete()
	{
		displayTexture = new Texture2D(CaptureController.PreviewWidth, CaptureController.PreviewHeight, TextureFormat.RGB24, false);
		displayTexture.filterMode = FilterMode.Point;
		displayColors = new Color32[displayTexture.width * displayTexture.height];
		captureController.deviceCamera.webcamTexture.Pause();
	}

	private IEnumerator initPopupFeed()
	{
		while (!captureController.deviceCamera.webcamTexture.didUpdateThisFrame)
		{
			yield return new WaitForEndOfFrame();
		}
		uiPopupCapture.menuCaptureViewport.SetRenderer();
		shouldProcess = true;
	}

	private IEnumerator managerInit()
	{
		captureController.InitializeCamera();
		while (!captureController.deviceCamera.webcamTexture.didUpdateThisFrame)
		{
			yield return new WaitForEndOfFrame();
		}
		if (DeviceManager.isPoopDevice)
		{
			yield return new WaitForSeconds(2f);
		}
		else
		{
			yield return new WaitForSeconds(1f);
		}
		isReady = true;
		initComplete = true;
	}

	public void ToggleCaptureFlip(bool flip)
	{
		ProcessorEnvironment.FlipCamera = flip;
		uiPopupCapture.menuCaptureViewport.uiRawImage.transform.localScale = ((!flip) ? Vector3.one : (Vector3.one * -1f));
	}

	public void ToggleBoardFindingMode(bool isGrid)
	{
		ProcessorEnvironment.DetectionType = ((!isGrid) ? ProcessorEnvironment.Type.Edge : ProcessorEnvironment.Type.Grid);
	}

	public void Process()
	{
		bool flag = false;
		if (ProcessorEnvironment.DetectionType == ProcessorEnvironment.Type.Grid)
		{
			Mat matToProcess = CaptureController.Instance.gridPreProcessor.Process();
			Mat matToProcess2 = CaptureController.Instance.centerMassProcessor.Process(matToProcess);
			MatOfPoint2f finalPolyPoints = CaptureController.Instance.polygonHullProcessor.Process(matToProcess2);
			flag = finalPolyPoints != null && BoardUtils.VerifyQuadAndSortCorners(ref finalPolyPoints, ref ProcessorEnvironment.GridCorners, CaptureController.PreviewSize);
		}
		else
		{
			flag = CaptureController.Instance.edgeProcessor.Process();
		}
		if (!flag)
		{
			Utils.matToTexture2D(CaptureController.CameraMat, displayTexture, displayColors);
			return;
		}
		if (S3Interface.instance.CanUploadCapture())
		{
			CaptureController.Instance.TryAddToCache();
		}
		CaptureController.Instance.cornerProcessor.Process();
		bool flag2 = CaptureController.Instance.gridVerification.Process();
		DiscoUI.DrawGridCorners(ref CaptureController.CameraMat, ref ProcessorEnvironment.GridCorners, new Scalar(60.0, 194.0, 248.0), 0.075f, 2);
		if (flag2)
		{
			double num = CaptureController.Instance.cctProcessor.Process(GridVerification.WarpedMat, BrightnessDetector.Mask);
			DiscoUI.DrawBoardOutline(ref CaptureController.CameraMat, ref ProcessorEnvironment.BoardCorners, new Scalar(0.0, 255.0, 0.0));
			ColorClassifier.CopyFrameToBuffer(_currentFrameIndex, (float)num);
			_currentFrameIndex = (_currentFrameIndex + 1) % CaptureController.FrameBufferLength;
			Utils.matToTexture2D(CaptureController.CameraMat, displayTexture, displayColors);
		}
		else
		{
			Utils.matToTexture2D(CaptureController.CameraMat, displayTexture, displayColors);
		}
	}
}
