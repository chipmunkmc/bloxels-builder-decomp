using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CapturePreviewBlock : MonoBehaviour
{
	private RectTransform rectTransform;

	public int col;

	public int row;

	[SerializeField]
	private BlockColor blockColor;

	private Image imageDisplay;

	private Tweener blockChangedTweener;

	private void Awake()
	{
		rectTransform = GetComponent<RectTransform>();
		imageDisplay = GetComponent<Image>();
	}

	public void MarkTransparent()
	{
		imageDisplay.color = new Color(imageDisplay.color.r, imageDisplay.color.g, imageDisplay.color.b, 0.3f);
	}

	public void Initialize(int col, int row, float startOffset, float spacing, float size, Sprite sprite)
	{
		this.col = col;
		this.row = row;
		imageDisplay.sprite = sprite;
		SetBlockColor(BlockColor.Blank);
		rectTransform.sizeDelta = new Vector2(size, size);
		rectTransform.anchoredPosition = new Vector2((float)col * spacing + startOffset, (float)row * spacing + startOffset);
		blockChangedTweener = imageDisplay.DOColor(Color.clear, UIAnimationManager.speedSlowAsCrap).SetAutoKill(false);
		blockChangedTweener.OnPlay(delegate
		{
			rectTransform.DOPunchScale(new Vector3(1.2f, 1.2f, 1f), UIAnimationManager.speedSlow);
		});
		blockChangedTweener.Pause();
	}

	public void SetBlockColor(BlockColor newBlockColor, bool animate = false)
	{
		blockColor = newBlockColor;
		Color color = Color.black;
		if (newBlockColor != BlockColor.Blank)
		{
			color = ColorUtilities.blockRGB[newBlockColor];
		}
		if (!animate)
		{
			imageDisplay.color = color;
			return;
		}
		blockChangedTweener.ChangeEndValue(color);
		if (!blockChangedTweener.IsPlaying())
		{
			blockChangedTweener.Play();
		}
	}
}
