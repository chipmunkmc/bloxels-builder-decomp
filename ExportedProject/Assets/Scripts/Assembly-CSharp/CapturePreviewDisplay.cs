using UnityEngine;
using UnityEngine.UI;

public class CapturePreviewDisplay : MonoBehaviour
{
	public RectTransform rect;

	public GameObject displayBlockPrefab;

	public CapturePreviewBlock[] displayBlocks;

	private int numberOfBlocks = 169;

	public Texture2D displayTexture;

	private Sprite displaySprite;

	public Image boardDisplayImage;

	private float boardUnits = 259f;

	private float cubeUnits = 10.83f;

	private float cubeGutter = 4.7f;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		float num = cubeUnits * 12f + cubeGutter * 12f;
		float x = rect.sizeDelta.x;
		float num2 = x * num / boardUnits;
		float spacing = num2 / 12f;
		float size = cubeUnits / boardUnits * x;
		float startOffset = 0f - x / 2f + (x - num2) / 2f;
		displayBlocks = new CapturePreviewBlock[numberOfBlocks];
		displaySprite = Sprite.Create(displayTexture, new Rect(0f, 0f, displayTexture.width, displayTexture.height), new Vector2(0.5f, 0.5f));
		boardDisplayImage = GetComponent<Image>();
		boardDisplayImage.sprite = displaySprite;
		boardDisplayImage.color = BloxelBoard.baseUIBoardColor;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				GameObject gameObject = Object.Instantiate(displayBlockPrefab);
				CapturePreviewBlock component = gameObject.GetComponent<CapturePreviewBlock>();
				component.transform.SetParent(base.transform, true);
				component.transform.localPosition = Vector3.zero;
				component.Initialize(j, i, startOffset, spacing, size, displaySprite);
				component.transform.localScale = Vector3.one;
				displayBlocks[i * 13 + j] = component;
			}
		}
	}

	private void OnDestroy()
	{
		Unload();
	}

	private void Unload()
	{
		displayBlockPrefab = null;
		displayBlocks = null;
		displayTexture = null;
		displaySprite = null;
		boardDisplayImage = null;
	}

	private void SetBlockColors(BlockColor[,] blockColors, bool animate = false)
	{
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor newBlockColor = blockColors[j, i];
				displayBlocks[i * 13 + j].SetBlockColor(newBlockColor, animate);
			}
		}
	}

	public void MarkAllUnclassified()
	{
		for (int i = 0; i < 169; i++)
		{
			displayBlocks[i].MarkTransparent();
		}
	}

	public void UpdateBlock(int col, int row, BlockColor blockColor, bool animate = false)
	{
		displayBlocks[row * 13 + col].SetBlockColor(blockColor, animate);
	}
}
