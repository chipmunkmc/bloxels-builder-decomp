using System;
using System.Collections;
using System.IO;
using DiscoData;
using UnityEngine;

[Serializable]
public class CapturedFrameMetaData
{
	private static string _Path = string.Empty;

	public string deviceName = "None";

	public string imageName = string.Empty;

	public float[] cornerData = new float[8] { 320f, 240f, 320f, 240f, 320f, 240f, 320f, 240f };

	public BlockColor[] blockColors = new BlockColor[169];

	public bool hasBlockColorData;

	private static int[] _RandomCornerNoise;

	private bool _shouldSave = true;

	public void Clear()
	{
		deviceName = "None";
		imageName = string.Empty;
		cornerData = new float[8] { 320f, 240f, 320f, 240f, 320f, 240f, 320f, 240f };
		Array.Clear(blockColors, 0, blockColors.Length);
		hasBlockColorData = false;
	}

	public bool CompareBlockColors(BlockColor[,] capturedColors, out CaptureData captureData)
	{
		captureData = new CaptureData();
		int num = 0;
		if (!hasBlockColorData)
		{
			return false;
		}
		int length = capturedColors.GetLength(0);
		int length2 = capturedColors.GetLength(1);
		int num2 = length * length2;
		if (num2 != blockColors.Length)
		{
			return false;
		}
		int i = 0;
		int num3 = 0;
		for (; i < length; i++)
		{
			int num4 = 0;
			while (num4 < length2)
			{
				BlockColor blockColor = capturedColors[num4, i];
				BlockColor blockColor2 = blockColors[num3];
				if (blockColor2 != blockColor)
				{
					num++;
					if (blockColor2 == BlockColor.Blank && blockColor != BlockColor.Blank)
					{
						captureData.numBlankAsBlocks++;
					}
					else if (blockColor2 != BlockColor.Blank && blockColor == BlockColor.Blank)
					{
						captureData.numBlocksAsBlank++;
					}
					else if (blockColor2 == BlockColor.White && blockColor != BlockColor.White)
					{
						captureData.numWhiteAsColor++;
					}
					else if (blockColor2 != BlockColor.White && blockColor == BlockColor.White)
					{
						captureData.numColorAsWhite++;
					}
					else
					{
						captureData.numColorsMisclassified++;
					}
				}
				num4++;
				num3++;
			}
		}
		captureData.percentError = (double)num / (double)num2 * 100.0;
		return true;
	}

	public void SetBlockColors(BlockColor[,] blockColors2D)
	{
		int i = 0;
		int num = 0;
		for (; i < 13; i++)
		{
			int num2 = 0;
			while (num2 < 13)
			{
				blockColors[num] = blockColors2D[num2, i];
				num2++;
				num++;
			}
		}
		hasBlockColorData = true;
	}

	public void SaveToS3(string imageURL)
	{
		if (_shouldSave)
		{
			string key = imageURL.Replace(".jpg", ".json");
			string jsonString = JsonUtility.ToJson(this);
			S3Interface.instance.UploadCaptureFrameMetaDataStub(jsonString, key);
		}
	}

	public IEnumerator LoadFromS3(string imageURL)
	{
		string url = imageURL.Replace(".jpg", ".json");
		using (WWW www = new WWW(url))
		{
			yield return www;
			if (!string.IsNullOrEmpty(www.text) && www.text.Contains("imageName"))
			{
				string text = www.text;
				JsonUtility.FromJsonOverwrite(text, this);
			}
			else
			{
				Clear();
			}
		}
	}

	public void SaveToDisk()
	{
		if (_shouldSave && !string.IsNullOrEmpty(_Path) && !string.IsNullOrEmpty(imageName))
		{
			if (!Directory.Exists(_Path))
			{
				Directory.CreateDirectory(_Path);
			}
			string contents = JsonUtility.ToJson(this);
			string path = _Path + "/" + imageName;
			File.WriteAllText(path, contents);
		}
	}

	public void LoadFromDisk(string filePath)
	{
		if (File.Exists(filePath))
		{
			string json = File.ReadAllText(filePath);
			JsonUtility.FromJsonOverwrite(json, this);
		}
	}

	public static void InitRandomCornerNoise(int seed)
	{
		_RandomCornerNoise = new int[4];
		System.Random random = new System.Random(seed);
		for (int i = 0; i < _RandomCornerNoise.Length; i++)
		{
			_RandomCornerNoise[i] = random.Next();
		}
	}

	public void AddNoiseToCornerData()
	{
		if (_RandomCornerNoise != null)
		{
			for (int i = 0; i < cornerData.Length; i++)
			{
				cornerData[i] += _RandomCornerNoise[i];
			}
			_shouldSave = false;
		}
	}
}
