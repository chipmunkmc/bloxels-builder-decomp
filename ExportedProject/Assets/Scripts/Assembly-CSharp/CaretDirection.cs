public enum CaretDirection
{
	UpLeft = 0,
	Up = 1,
	UpRight = 2,
	RightUp = 3,
	Right = 4,
	RightDown = 5,
	DownRight = 6,
	Down = 7,
	DownLeft = 8,
	LeftDown = 9,
	Left = 10,
	LeftUp = 11,
	None = 12
}
