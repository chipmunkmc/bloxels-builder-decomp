using System;
using System.Collections.Generic;
using UnityEngine;

public class ChallengeManager : MonoBehaviour, Localizable
{
	public static ChallengeManager instance;

	[Header("Data")]
	public List<Challenge> completedChallenges;

	public bool hasActiveChallenge;

	public Challenge latestActiveChallenge;

	private UnityEngine.Object _uiPopupChallengePrefab;

	private UnityEngine.Object _uiChallengeHeaderPrefab;

	public UIChallengeHeader uiChallengeHeader;

	private Dictionary<Challenge, ChallengeMode> modeLookup;

	public Dictionary<Challenge, int> rewardForChallenge;

	public Dictionary<Challenge, string> descriptionForChallenge;

	public Dictionary<Challenge, Sprite> badgeForChallenge;

	public string acceptanceText;

	private Dictionary<Challenge, string> challengeShortNames;

	private Dictionary<Challenge, string> challengeLongNames;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		_uiPopupChallengePrefab = Resources.Load("Prefabs/UIPopupChallenges");
		_uiChallengeHeaderPrefab = Resources.Load("Prefabs/UIChallengeHeader");
		completedChallenges = new List<Challenge>();
		LocalizationManager.getInstance().addSubscriber(this);
		localizeText();
	}

	private void Destroy()
	{
		LocalizationManager.getInstance().addSubscriber(this);
	}

	public void localizeText()
	{
		descriptionForChallenge = new Dictionary<Challenge, string>
		{
			{
				Challenge.GameBuilder_Easy,
				LocalizationManager.getInstance().getLocalizedText("challenges83", "Game Builder Bronze")
			},
			{
				Challenge.CharacterBuilder_Easy,
				LocalizationManager.getInstance().getLocalizedText("challenges84", "Hero Builder Bronze")
			}
		};
		challengeShortNames = new Dictionary<Challenge, string>
		{
			{
				Challenge.GameBuilder_Easy,
				LocalizationManager.getInstance().getLocalizedText("nav2", "Game Builder")
			},
			{
				Challenge.CharacterBuilder_Easy,
				LocalizationManager.getInstance().getLocalizedText("challenges7", "Hero Builder")
			}
		};
		challengeLongNames = new Dictionary<Challenge, string>
		{
			{
				Challenge.GameBuilder_Easy,
				LocalizationManager.getInstance().getLocalizedText("challenges9", "Challenge Mode - Game Builder")
			},
			{
				Challenge.CharacterBuilder_Easy,
				LocalizationManager.getInstance().getLocalizedText("challenges48", "Challenge Mode - Hero Builder")
			}
		};
		acceptanceText = LocalizationManager.getInstance().getLocalizedText("challenges47", "Awesome!");
	}

	private void Start()
	{
		foreach (Challenge value in Enum.GetValues(typeof(Challenge)))
		{
			if (CheckChallenge(value) && !completedChallenges.Contains(value))
			{
				completedChallenges.Add(value);
			}
		}
		rewardForChallenge = new Dictionary<Challenge, int>
		{
			{
				Challenge.GameBuilder_Easy,
				GemEarnable.valueLookup[GemEarnable.Type.GameBuilder_Easy]
			},
			{
				Challenge.CharacterBuilder_Easy,
				GemEarnable.valueLookup[GemEarnable.Type.CharacterBuilder_Easy]
			}
		};
		badgeForChallenge = new Dictionary<Challenge, Sprite>
		{
			{
				Challenge.GameBuilder_Easy,
				AssetManager.instance.gameBuilderBadgeEasy
			},
			{
				Challenge.CharacterBuilder_Easy,
				AssetManager.instance.heroBuilderBadgeEasy
			}
		};
	}

	public void InitForEditor()
	{
		modeLookup = new Dictionary<Challenge, ChallengeMode>
		{
			{
				Challenge.GameBuilder_Easy,
				Challenge_GameBuilder_Easy.instance
			},
			{
				Challenge.CharacterBuilder_Easy,
				Challenge_CharacterBuilder_Easy.instance
			}
		};
	}

	public bool CheckChallenge(Challenge challenge)
	{
		return PrefsManager.instance.CheckChallenge(challenge);
	}

	public void CompleteChallenge(Challenge challenge)
	{
		PrefsManager.instance.ChallengeCompleted(challenge);
		if (!completedChallenges.Contains(challenge))
		{
			completedChallenges.Add(challenge);
		}
	}

	public void CompleteCurrentChallenge()
	{
		CompleteChallenge(latestActiveChallenge);
	}

	public void ShowChallengeMenu(bool shouldUnlock = false)
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(_uiPopupChallengePrefab);
		UIPopupChallenges component = gameObject.GetComponent<UIPopupChallenges>();
		if (shouldUnlock)
		{
			component.Unlock();
		}
		else
		{
			component.Init();
		}
	}

	public UIChallengeHeader ShowHeader()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(_uiChallengeHeaderPrefab);
		uiChallengeHeader = gameObject.GetComponent<UIChallengeHeader>();
		return uiChallengeHeader;
	}

	public string GetNameFromType(Challenge challenge)
	{
		return challengeShortNames[challenge];
	}

	public string GetHeaderForLatest()
	{
		return challengeLongNames[latestActiveChallenge];
	}

	public void ActivateChallenge(Challenge challenge)
	{
		hasActiveChallenge = true;
		latestActiveChallenge = challenge;
		modeLookup[challenge].Init();
	}

	public void QuitLatest()
	{
		hasActiveChallenge = false;
		modeLookup[latestActiveChallenge].Quit();
		if (uiChallengeHeader != null && uiChallengeHeader.gameObject.activeInHierarchy)
		{
			uiChallengeHeader.Dismiss();
		}
	}
}
