using System.Collections;
using UnityEngine;

public class ChallengeMode : MonoBehaviour
{
	public Challenge type;

	public int totalSteps;

	public static string noMatchTitleKey = "challenges18";

	public static string noMatchTitle = "Uh Oh!";

	public static string noMatchDescriptionKey = "challenges19";

	public static string noMatchDescription = "Doesn't look like you matched the layout in the challenge, please make sure that the layout matches the challenge poster or the instructions in the popup.";

	public static string noMatchButtonTextKey = "challenges20";

	public static string noMatchButtonText = "Try Again";

	public static string noTouchTitleKey = "challenges21";

	public static string noTouchTitle = "Whoa There";

	public static string noTouchDescriptionKey = "challenges22";

	public static string noTouchDescription = "Areas outside the flow of the Challenge Mode are not available.";

	public static string noTouchButtonTextKey = "account80";

	public static string noTouchButtonText = "Got it!";

	[Header("| ========= Objects ========= |")]
	public Object uiTipPrefab;

	public GameObject currentTipObject;

	public UITip currentTip;

	public Object boardLayoutPrefab;

	public GameObject currentBuildLayoutObject;

	public UIPopupBuildLayout currentLayoutPopup;

	public Object infoPopupPrefab;

	public Object challengeCompletePrefab;

	public static BloxelBoard boardConstant;

	public UIChallengeHeader uiChallengeHeader;

	public UIRectangle uiRectangleBottom;

	public UIRectangle uiRectangleLeft;

	public UIRectangle uiRectangleRight;

	public UIRectangle uiRectangleCenter;

	public void Awake()
	{
		infoPopupPrefab = Resources.Load("Prefabs/UIPopupChallengeInfo");
		challengeCompletePrefab = Resources.Load("Prefabs/UIPopupChallengeComplete");
		boardConstant = new BloxelBoard();
	}

	public virtual void Init()
	{
	}

	public virtual void Quit()
	{
	}

	public void GeneratePopup_BuildLayout(string assetID, string title, string body, string step, string helper)
	{
		BlockColorArray.CopyBlockColorsFromSavedArray(assetID, boardConstant.blockColors);
		currentBuildLayoutObject = UIOverlayCanvas.instance.Popup(boardLayoutPrefab);
		currentLayoutPopup = currentBuildLayoutObject.GetComponent<UIPopupBuildLayout>();
		currentLayoutPopup.Init(boardConstant, title, body, step, helper);
		currentLayoutPopup.OnComplete += CurrentLayoutComplete;
	}

	public void BlockingRectTapped()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(infoPopupPrefab);
		gameObject.transform.SetAsLastSibling();
		UIPopupChallengeInfo menu = gameObject.GetComponent<UIPopupChallengeInfo>();
		string localizedText = LocalizationManager.getInstance().getLocalizedText(noTouchTitleKey, noTouchTitle);
		string localizedText2 = LocalizationManager.getInstance().getLocalizedText(noTouchDescriptionKey, noTouchDescription);
		string localizedText3 = LocalizationManager.getInstance().getLocalizedText(noTouchButtonTextKey, noTouchButtonText);
		menu.Init(localizedText, localizedText2, localizedText3);
		menu.OnComplete += delegate
		{
			menu = null;
		};
	}

	public virtual void CurrentLayoutComplete()
	{
		currentLayoutPopup = null;
		currentBuildLayoutObject = null;
	}

	public bool CheckMatch(string assetID, BlockColor[,] blockColors)
	{
		BlockColor[,] array = new BlockColor[13, 13];
		BlockColorArray.CopyBlockColorsFromSavedArray(assetID, array);
		if (!ColorUtilities.BlockColorsMatch(array, blockColors))
		{
			StartCoroutine(delayedChallengeInfoPopup());
			return false;
		}
		return true;
	}

	private IEnumerator delayedChallengeInfoPopup()
	{
		yield return new WaitForSeconds(0.5f);
		GameObject obj = UIOverlayCanvas.instance.Popup(infoPopupPrefab);
		obj.transform.SetAsLastSibling();
		UIPopupChallengeInfo menu = obj.GetComponent<UIPopupChallengeInfo>();
		string title = LocalizationManager.getInstance().getLocalizedText(noMatchTitleKey, noMatchTitle);
		string description = LocalizationManager.getInstance().getLocalizedText(noMatchDescriptionKey, noMatchDescription);
		string buttonText = LocalizationManager.getInstance().getLocalizedText(noMatchButtonTextKey, noMatchButtonText);
		menu.Init(title, description, buttonText);
		menu.OnComplete += delegate
		{
			menu = null;
		};
	}

	public void CreateTip(UITipVisual tipVisual, StepText stepText)
	{
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		currentTipObject = UIOverlayCanvas.instance.Popup(uiTipPrefab);
		currentTip = currentTipObject.GetComponent<UITip>();
		currentTip.uiToggleStop.gameObject.SetActive(false);
		currentTip.uiImageOverlay.enabled = false;
		currentTip.uiButtonOk.gameObject.SetActive(false);
		currentTip.uiTextStep.gameObject.SetActive(false);
		currentTip.uiTextBody.GetComponent<RectTransform>().anchoredPosition = new Vector2(30f, 0f);
		currentTip.Init(tipVisual, stepText);
	}
}
