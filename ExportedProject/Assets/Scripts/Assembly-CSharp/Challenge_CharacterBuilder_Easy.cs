using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge_CharacterBuilder_Easy : ChallengeMode
{
	public enum Step
	{
		Welcome = 0,
		IdleTip = 1,
		BuildLayout1 = 2,
		CaptureTip1 = 3,
		CaptureLayout1 = 4,
		DuplicateFrame1 = 5,
		BuildLayout2 = 6,
		CaptureTip2 = 7,
		CaptureLayout2 = 8,
		GoodJob1 = 9,
		WalkTip = 10,
		BuildLayout3 = 11,
		CaptureTip3 = 12,
		CaptureLayout3 = 13,
		DuplicateFrame2 = 14,
		BuildLayout4 = 15,
		CaptureTip4 = 16,
		CaptureLayout4 = 17,
		JumpTip = 18,
		BuildLayout5 = 19,
		CaptureTip5 = 20,
		CaptureLayout5 = 21,
		DuplicateFrame3 = 22,
		BuildLayout6 = 23,
		CaptureTip6 = 24,
		CaptureLayout6 = 25,
		TestTip1 = 26,
		Test1 = 27,
		FrameRateTip = 28,
		TestTip2 = 29,
		Test2 = 30,
		Complete = 31
	}

	public static Challenge_CharacterBuilder_Easy instance;

	public Dictionary<Step, UITipVisual> uiTipForStep;

	public Dictionary<Step, StepText> textForStep;

	public Dictionary<Step, string> boardAssetForStep;

	public Step currentStep;

	public UIRectangle uiRectangleTools;

	public int idleAttempts;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		idleAttempts = 0;
		totalSteps = Enum.GetValues(typeof(Step)).Length;
		CreateTipUILookup();
		CreateTipTextLookup();
		CreateAssetLookup();
	}

	private void CreateTipUILookup()
	{
		uiTipForStep = new Dictionary<Step, UITipVisual>
		{
			{
				Step.IdleTip,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(168f, 100f)), CaretDirection.Right, new Vector2(500f, 300f))
			},
			{
				Step.WalkTip,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(168f, 30f)), CaretDirection.Right, new Vector2(500f, 300f))
			},
			{
				Step.JumpTip,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(168f, -40f)), CaretDirection.Right, new Vector2(500f, 300f))
			},
			{
				Step.DuplicateFrame1,
				new UITipVisual(new RectPosition(Direction.DownLeft, new Vector2(188f, 110f)), CaretDirection.Down, new Vector2(500f, 300f))
			},
			{
				Step.DuplicateFrame2,
				new UITipVisual(new RectPosition(Direction.DownLeft, new Vector2(188f, 110f)), CaretDirection.Down, new Vector2(500f, 300f))
			},
			{
				Step.DuplicateFrame3,
				new UITipVisual(new RectPosition(Direction.DownLeft, new Vector2(188f, 110f)), CaretDirection.Down, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip1,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-280f, 180f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip2,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-280f, 180f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip3,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-280f, 180f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip4,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-280f, 180f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip5,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-280f, 180f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip6,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-280f, 180f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.FrameRateTip,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-56f, 90f)), CaretDirection.DownRight, new Vector2(500f, 300f))
			},
			{
				Step.TestTip1,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(164f, -52f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.TestTip2,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(164f, -52f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			}
		};
	}

	private void CreateTipTextLookup()
	{
		textForStep = new Dictionary<Step, StepText>
		{
			{
				Step.IdleTip,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges49", "Idle State"), LocalizationManager.getInstance().getLocalizedText("challenges50", "Characters are made up of multiple animations, or States. We'll begin with the Idle State."))
			},
			{
				Step.WalkTip,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges62", "Walk State"), LocalizationManager.getInstance().getLocalizedText("challenges63", "Now let's capture some frames to make our Hero walk!"))
			},
			{
				Step.JumpTip,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges68", "Jump State"), LocalizationManager.getInstance().getLocalizedText("challenges69", "Select the last State, Jump, to capture what our Hero will look like while they're in the air!"))
			},
			{
				Step.DuplicateFrame1,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges55", "Duplicate Frame"), LocalizationManager.getInstance().getLocalizedText("challenges56", "We need to add a new frame to capture, let's duplicate the last frame and capture over it."))
			},
			{
				Step.DuplicateFrame2,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges55", "Duplicate Frame"), LocalizationManager.getInstance().getLocalizedText("challenges66", "Time to duplicate the last frame in order to capture over it."))
			},
			{
				Step.DuplicateFrame3,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges55", "Duplicate Frame"), LocalizationManager.getInstance().getLocalizedText("challenges71", "Let's duplicate this frame to capture our final board."))
			},
			{
				Step.CaptureTip1,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges53", "Capture"), LocalizationManager.getInstance().getLocalizedText("challenges54", "Tap here to bring your board into the app using the Capture tool. We'll walk you through it."))
			},
			{
				Step.CaptureTip2,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges53", "Capture"), LocalizationManager.getInstance().getLocalizedText("challenges59", "Time to capture again, tap here to get started."))
			},
			{
				Step.CaptureTip3,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges53", "Capture"), LocalizationManager.getInstance().getLocalizedText("challenges65", "One last capture and you'll have a fully functional character built!"))
			},
			{
				Step.CaptureTip4,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges53", "Capture"), LocalizationManager.getInstance().getLocalizedText("challenges59", "Time to capture again, tap here to get started."))
			},
			{
				Step.CaptureTip5,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges53", "Capture"), LocalizationManager.getInstance().getLocalizedText("challenges59", "Time to capture again, tap here to get started."))
			},
			{
				Step.CaptureTip6,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges53", "Capture"), LocalizationManager.getInstance().getLocalizedText("challenges65", "One last capture and you'll have a fully functional character built!"))
			},
			{
				Step.FrameRateTip,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges78", "Frame Rate"), LocalizationManager.getInstance().getLocalizedText("challenges79", "This is how fast your Character State will animate, try setting this to 1 or 2 for the Jump animation"))
			},
			{
				Step.TestTip1,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges73", "Preview"), LocalizationManager.getInstance().getLocalizedText("challenges74", "Let's preview your Hero in a game environment. Tap here to test your character."))
			},
			{
				Step.TestTip2,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges73", "Preview"), LocalizationManager.getInstance().getLocalizedText("challenges80", "Let's test one more time!"))
			}
		};
	}

	private void CreateAssetLookup()
	{
		boardAssetForStep = new Dictionary<Step, string>
		{
			{
				Step.BuildLayout1,
				AssetManager.challengeLayout_2_1_1
			},
			{
				Step.BuildLayout2,
				AssetManager.challengeLayout_2_1_2
			},
			{
				Step.BuildLayout3,
				AssetManager.challengeLayout_2_2_1
			},
			{
				Step.BuildLayout4,
				AssetManager.challengeLayout_2_2_2
			},
			{
				Step.BuildLayout5,
				AssetManager.challengeLayout_2_3_1
			},
			{
				Step.BuildLayout6,
				AssetManager.challengeLayout_2_3_2
			},
			{
				Step.CaptureLayout1,
				AssetManager.challengeLayout_2_1_1
			},
			{
				Step.CaptureLayout2,
				AssetManager.challengeLayout_2_1_2
			},
			{
				Step.CaptureLayout3,
				AssetManager.challengeLayout_2_2_1
			},
			{
				Step.CaptureLayout4,
				AssetManager.challengeLayout_2_2_2
			},
			{
				Step.CaptureLayout5,
				AssetManager.challengeLayout_2_3_1
			},
			{
				Step.CaptureLayout6,
				AssetManager.challengeLayout_2_3_2
			}
		};
	}

	public override void Init()
	{
		idleAttempts = 0;
		currentStep = Step.Welcome;
		BloxelLibrary.instance.libraryToggle.gameObject.SetActive(false);
		PixelEditorController.instance.SwitchCanvasMode(CanvasMode.CharacterBuilder);
		uiChallengeHeader = ChallengeManager.instance.ShowHeader();
		uiChallengeHeader.OnInfo += InfoButtonPressed;
		CharacterBuilderCanvas.instance.AdjustStartForChallenge();
		SetupBlockingRects();
		SetupEvents();
	}

	private void SetupEvents()
	{
		CharacterBuilderCanvas.instance.OnNewCharacter += HandleCharacterInit;
		CharacterBuilder.instance.OnState += HandleStateSelect;
		CharacterBuilder.instance.OnPreview += HandleOnPreview;
		UICaptureToggleTool.instance.OnClick += HandleCaptureOnClick;
		CaptureManager.instance.OnCaptureDismiss += HandleCaptureDismiss;
		CaptureManager.instance.OnCaptureConfirm += HandleCaptureConfirm;
		AnimationTimeline.instance.OnAddFrameAfter += HandleDuplicateFrame;
		AnimationTimeline.instance.uiFrameRate.OnChange += HandleFrameRate;
	}

	private void RemoveEvents()
	{
		if (CharacterBuilderCanvas.instance != null)
		{
			CharacterBuilderCanvas.instance.OnNewCharacter -= HandleCharacterInit;
		}
		if (CharacterBuilder.instance != null)
		{
			CharacterBuilder.instance.OnState -= HandleStateSelect;
			CharacterBuilder.instance.OnPreview -= HandleOnPreview;
		}
		if (UICaptureToggleTool.instance != null)
		{
			UICaptureToggleTool.instance.OnClick -= HandleCaptureOnClick;
		}
		if (CaptureManager.instance != null)
		{
			CaptureManager.instance.OnCaptureDismiss -= HandleCaptureDismiss;
			CaptureManager.instance.OnCaptureConfirm -= HandleCaptureConfirm;
		}
		if (AnimationTimeline.instance != null)
		{
			AnimationTimeline.instance.OnAddFrameAfter -= HandleDuplicateFrame;
			AnimationTimeline.instance.uiFrameRate.OnChange -= HandleFrameRate;
		}
	}

	private void SetupBlockingRects()
	{
		Color clear = Color.clear;
		RectPosition rectPos = new RectPosition(Direction.DownLeft, Vector2.zero);
		Vector2 size = new Vector2(UIOverlayCanvas.instance.rect.sizeDelta.x, 140f);
		RectPosition rectPos2 = new RectPosition(Direction.All, new Vector2(-480f, 0f));
		Vector2 size2 = new Vector2(100f, 500f);
		RectPosition rectPos3 = new RectPosition(Direction.Right, new Vector2(0f, 70f));
		Vector2 size3 = new Vector2(140f, 750f);
		RectPosition rectPos4 = new RectPosition(Direction.All, new Vector2(0f, 0f));
		Vector2 size4 = new Vector2(862f, 862f);
		RectPosition rectPos5 = new RectPosition(Direction.All, new Vector2(490f, 0f));
		Vector2 size5 = new Vector2(120f, 400f);
		uiRectangleBottom = UIOverlayCanvas.instance.GenerateRectangle(rectPos, size, clear);
		uiRectangleBottom.OnClick += base.BlockingRectTapped;
		uiRectangleLeft = UIOverlayCanvas.instance.GenerateRectangle(rectPos2, size2, clear);
		uiRectangleLeft.OnClick += base.BlockingRectTapped;
		uiRectangleRight = UIOverlayCanvas.instance.GenerateRectangle(rectPos3, size3, clear);
		uiRectangleRight.OnClick += base.BlockingRectTapped;
		uiRectangleCenter = UIOverlayCanvas.instance.GenerateRectangle(rectPos4, size4, clear);
		uiRectangleCenter.OnClick += base.BlockingRectTapped;
		uiRectangleTools = UIOverlayCanvas.instance.GenerateRectangle(rectPos5, size5, clear);
		uiRectangleTools.OnClick += base.BlockingRectTapped;
		uiRectangleCenter.SetInputBlocking(false);
		uiRectangleBottom.SetInputBlocking(false);
		uiRectangleLeft.SetInputBlocking(false);
		uiRectangleRight.SetInputBlocking(false);
		uiRectangleTools.SetInputBlocking(false);
	}

	private void HandleCharacterInit()
	{
		StartCoroutine(StartupDelay());
	}

	private IEnumerator StartupDelay()
	{
		yield return new WaitForSeconds(UIAnimationManager.speedSlow);
		StateTip(Step.IdleTip);
	}

	private void HandleStateSelect(AnimationType _state)
	{
		switch (_state)
		{
		case AnimationType.Idle:
			if (idleAttempts >= 2)
			{
				BuildLayout(Step.BuildLayout1);
			}
			idleAttempts++;
			break;
		case AnimationType.Walk:
			BuildLayout(Step.BuildLayout3);
			break;
		case AnimationType.Jump:
			BuildLayout(Step.BuildLayout5);
			break;
		}
	}

	private void HandleCaptureOnClick()
	{
		uiRectangleCenter.SetInputBlocking(false);
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		switch (currentStep)
		{
		case Step.CaptureTip1:
			CaptureLayout(Step.CaptureLayout1);
			break;
		case Step.CaptureTip2:
			CaptureLayout(Step.CaptureLayout2);
			break;
		case Step.CaptureTip3:
			CaptureLayout(Step.CaptureLayout3);
			break;
		case Step.CaptureTip4:
			CaptureLayout(Step.CaptureLayout4);
			break;
		case Step.CaptureTip5:
			CaptureLayout(Step.CaptureLayout5);
			break;
		case Step.CaptureTip6:
			CaptureLayout(Step.CaptureLayout6);
			break;
		}
	}

	private void HandleCaptureDismiss()
	{
		switch (currentStep)
		{
		case Step.CaptureLayout1:
			CaptureTip(Step.CaptureTip1);
			break;
		case Step.CaptureLayout2:
			CaptureTip(Step.CaptureTip2);
			break;
		case Step.CaptureLayout3:
			CaptureTip(Step.CaptureTip3);
			break;
		case Step.CaptureLayout4:
			CaptureTip(Step.CaptureTip4);
			break;
		case Step.CaptureLayout5:
			CaptureTip(Step.CaptureTip5);
			break;
		case Step.CaptureLayout6:
			CaptureTip(Step.CaptureTip6);
			break;
		}
	}

	private void HandleCaptureConfirm()
	{
		if (1 == 0)
		{
			Step step = currentStep;
			if (step != Step.CaptureLayout1)
			{
				if (step != Step.CaptureLayout2)
				{
					if (step != Step.CaptureLayout3)
					{
						if (step != Step.CaptureLayout4)
						{
							if (step != Step.CaptureLayout5)
							{
								if (step == Step.CaptureLayout6)
								{
									CaptureTip(Step.CaptureTip6);
								}
							}
							else
							{
								CaptureTip(Step.CaptureTip5);
							}
						}
						else
						{
							CaptureTip(Step.CaptureTip4);
						}
					}
					else
					{
						CaptureTip(Step.CaptureTip3);
					}
				}
				else
				{
					CaptureTip(Step.CaptureTip2);
				}
			}
			else
			{
				CaptureTip(Step.CaptureTip1);
			}
			return;
		}
		switch (currentStep)
		{
		case Step.CaptureLayout1:
			DuplicateFrameTip(Step.DuplicateFrame1);
			break;
		case Step.CaptureLayout2:
			StartCoroutine(GoodJob(Step.GoodJob1));
			break;
		case Step.CaptureLayout3:
			DuplicateFrameTip(Step.DuplicateFrame2);
			break;
		case Step.CaptureLayout4:
			StateTip(Step.JumpTip);
			break;
		case Step.CaptureLayout5:
			DuplicateFrameTip(Step.DuplicateFrame3);
			break;
		case Step.CaptureLayout6:
			TestTip(Step.TestTip1);
			break;
		}
	}

	public override void CurrentLayoutComplete()
	{
		base.CurrentLayoutComplete();
		switch (currentStep)
		{
		case Step.BuildLayout1:
			CaptureTip(Step.CaptureTip1);
			break;
		case Step.BuildLayout2:
			CaptureTip(Step.CaptureTip2);
			break;
		case Step.BuildLayout3:
			CaptureTip(Step.CaptureTip3);
			break;
		case Step.BuildLayout4:
			CaptureTip(Step.CaptureTip4);
			break;
		case Step.BuildLayout5:
			CaptureTip(Step.CaptureTip5);
			break;
		case Step.BuildLayout6:
			CaptureTip(Step.CaptureTip6);
			break;
		}
	}

	private void HandleDuplicateFrame()
	{
		switch (currentStep)
		{
		case Step.DuplicateFrame1:
			BuildLayout(Step.BuildLayout2);
			break;
		case Step.DuplicateFrame2:
			BuildLayout(Step.BuildLayout4);
			break;
		case Step.DuplicateFrame3:
			BuildLayout(Step.BuildLayout6);
			break;
		}
	}

	private void HandleOnPreview()
	{
		if (currentStep == Step.TestTip1)
		{
			Test(Step.Test1);
		}
		else if (currentStep == Step.TestTip2)
		{
			Test(Step.Test2);
		}
	}

	private void HandleFrameRate(float rate)
	{
		if (currentStep == Step.FrameRateTip && (rate == 1f || rate == 2f))
		{
			TestTip(Step.TestTip2);
		}
	}

	private void StateTip(Step stateStep)
	{
		currentStep = stateStep;
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		BlockAllRects(true);
		uiRectangleTools.SetInputBlocking(false);
	}

	private void TestTip(Step testStep)
	{
		currentStep = testStep;
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		BlockAllRects(true);
		uiRectangleTools.SetInputBlocking(false);
		if (currentStep == Step.TestTip2)
		{
			uiRectangleBottom.SetInputBlocking(false);
		}
	}

	private IEnumerator GoodJob(Step goodJobStep)
	{
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		currentStep = goodJobStep;
		GameObject obj = UIOverlayCanvas.instance.Popup(infoPopupPrefab);
		UIPopupChallengeInfo menu = obj.GetComponent<UIPopupChallengeInfo>();
		menu.Init(LocalizationManager.getInstance().getLocalizedText("challenges60", "Good Job!"), LocalizationManager.getInstance().getLocalizedText("challenges61", "Hey you're on a roll, your hero has an Idle animation. Time to make them walk and jump!"), LocalizationManager.getInstance().getLocalizedText("challenges37", "Let's do it!"));
		menu.OnComplete += delegate
		{
			menu = null;
			StateTip(Step.WalkTip);
		};
		BlockAllRects(false);
	}

	private void BuildLayout(Step layoutStep)
	{
		currentStep = layoutStep;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		string step = string.Empty;
		string text = string.Empty;
		string body = string.Empty;
		switch (currentStep)
		{
		case Step.BuildLayout1:
			step = "2";
			text = LocalizationManager.getInstance().getLocalizedText("challenges51", "Build Frame 1");
			body = LocalizationManager.getInstance().getLocalizedText("challenges52", "Build Frame 1 on your Gameboard and tap Finished when you are ready!");
			break;
		case Step.BuildLayout2:
			step = "2";
			text = LocalizationManager.getInstance().getLocalizedText("challenges57", "Build Frame 2");
			body = LocalizationManager.getInstance().getLocalizedText("challenges58", "Not many changes here, just make the eyes blue so it looks like Ugly Sweater Kitty is blinking!");
			break;
		case Step.BuildLayout3:
			step = "3";
			text = LocalizationManager.getInstance().getLocalizedText("challenges51", "Build Frame 1");
			body = LocalizationManager.getInstance().getLocalizedText("challenges64", "Build this frame on your Gameboard, it's the same as the first Frame from Step 2.");
			break;
		case Step.BuildLayout4:
			step = "3";
			text = LocalizationManager.getInstance().getLocalizedText("challenges57", "Build Frame 2");
			body = LocalizationManager.getInstance().getLocalizedText("challenges67", "Point the tail up and feet to the side to make it look like Ugly Sweater Kitty is walking.");
			break;
		case Step.BuildLayout5:
			step = "4";
			text = LocalizationManager.getInstance().getLocalizedText("challenges51", "Build Frame 1");
			body = LocalizationManager.getInstance().getLocalizedText("challenges70", "Build this frame on your Gameboard, it's the same as the first step in the last two states.");
			break;
		case Step.BuildLayout6:
			step = "4";
			text = LocalizationManager.getInstance().getLocalizedText("challenges57", "Build Frame 2");
			body = LocalizationManager.getInstance().getLocalizedText("challenges72", "Slight changes, point the tail down move the left foot in and move the right foot out.");
			break;
		}
		GeneratePopup_BuildLayout(boardAssetForStep[currentStep], text, body, step, text);
		BlockAllRects(false);
	}

	private void DuplicateFrameTip(Step frameStep)
	{
		currentStep = frameStep;
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		BlockAllRects(true);
		uiRectangleBottom.SetInputBlocking(false);
	}

	private void CaptureTip(Step tipStep)
	{
		currentStep = tipStep;
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		BlockAllRects(true);
	}

	private void CaptureLayout(Step layoutStep)
	{
		currentStep = layoutStep;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		BlockAllRects(false);
	}

	private void FrameRateTip()
	{
		currentStep = Step.FrameRateTip;
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		BlockAllRects(true);
		uiRectangleBottom.SetInputBlocking(false);
	}

	private void Test(Step testStep)
	{
		currentStep = testStep;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		if (currentStep == Step.Test1)
		{
			StartCoroutine(DelayedInfoRoutine());
		}
		else if (currentStep == Step.Test2)
		{
			StartCoroutine(DelayedCompleteRoutine());
		}
		BlockAllRects(false);
	}

	private IEnumerator DelayedInfoRoutine()
	{
		yield return new WaitForSeconds(10f);
		uiChallengeHeader.ShowInfoButton();
	}

	private void InfoButtonPressed()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(infoPopupPrefab);
		UIPopupChallengeInfo menu = gameObject.GetComponent<UIPopupChallengeInfo>();
		menu.Init(LocalizationManager.getInstance().getLocalizedText("challenges76", "Hmmmm"), LocalizationManager.getInstance().getLocalizedText("challenges77", "The Hero's jump animation looks a little off, let's go and fix that now."), LocalizationManager.getInstance().getLocalizedText("challenges37", "Let's do it!"));
		menu.OnComplete += delegate
		{
			menu = null;
			uiChallengeHeader.HideInfoButton();
			CharacterBuilder.instance.ComeBackFromPreview();
			FrameRateTip();
		};
	}

	private IEnumerator DelayedCompleteRoutine()
	{
		yield return new WaitForSeconds(5f);
		Complete();
	}

	public void Complete()
	{
		currentStep = Step.Complete;
		GameObject gameObject = UIOverlayCanvas.instance.Popup(challengeCompletePrefab);
		UIPopupChallengeComplete menu = gameObject.GetComponent<UIPopupChallengeComplete>();
		menu.Init(type);
		GemManager.instance.AddGemsForType(GemEarnable.Type.CharacterBuilder_Easy);
		menu.OnComplete += delegate
		{
			menu = null;
			BadgeManager.instance.BadgeEarned(BadgeManager.instance.badgeForChallenge[type]);
			ChallengeManager.instance.CompleteCurrentChallenge();
			ChallengeManager.instance.QuitLatest();
			CharacterBuilder.instance.ComeBackFromPreview();
			StartCoroutine(ShowChallengeMenu());
		};
	}

	private IEnumerator ShowChallengeMenu()
	{
		yield return new WaitForSeconds(1f);
		ChallengeManager.instance.ShowChallengeMenu();
	}

	public override void Quit()
	{
		StopAllCoroutines();
		BloxelLibrary.instance.libraryToggle.gameObject.SetActive(true);
		CharacterBuilderCanvas.instance.ResetStart();
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		if (currentLayoutPopup != null)
		{
			currentLayoutPopup.Dismiss();
		}
		if (uiRectangleBottom != null)
		{
			uiRectangleBottom.DestroyMe();
		}
		if (uiRectangleCenter != null)
		{
			uiRectangleCenter.DestroyMe();
		}
		if (uiRectangleLeft != null)
		{
			uiRectangleLeft.DestroyMe();
		}
		if (uiRectangleRight != null)
		{
			uiRectangleRight.DestroyMe();
		}
		if (uiRectangleTools != null)
		{
			uiRectangleTools.DestroyMe();
		}
		RemoveEvents();
		uiChallengeHeader = null;
	}

	private void BlockAllRects(bool isOn)
	{
		uiRectangleBottom.SetInputBlocking(isOn);
		uiRectangleTools.SetInputBlocking(isOn);
		uiRectangleCenter.SetInputBlocking(isOn);
		uiRectangleRight.SetInputBlocking(isOn);
		uiRectangleLeft.SetInputBlocking(isOn);
	}
}
