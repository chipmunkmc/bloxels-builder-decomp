using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Challenge_GameBuilder_Easy : ChallengeMode
{
	public enum Step
	{
		Welcome = 0,
		RoomSelect1 = 1,
		BuildLayout1 = 2,
		CaptureTip1 = 3,
		CaptureLayout1 = 4,
		MapToggle = 5,
		RoomSelect2 = 6,
		BuildLayout2 = 7,
		CaptureTip2 = 8,
		CaptureLayout2 = 9,
		Configure = 10,
		ConfigStoryBlock = 11,
		TestTip1 = 12,
		Test1 = 13,
		ConfigPowerUp = 14,
		TestTip2 = 15,
		Test2 = 16,
		Complete = 17
	}

	public static Challenge_GameBuilder_Easy instance;

	public Dictionary<Step, UITipVisual> uiTipForStep;

	public Dictionary<Step, StepText> textForStep;

	public Dictionary<Step, string> boardAssetForStep;

	public Step currentStep;

	private UIPopupNPCConfig _npcConfigMenu;

	private UIPopupPowerupConfig _powerupConfigMenu;

	private new void Awake()
	{
		base.Awake();
		if (instance == null)
		{
			instance = this;
		}
		totalSteps = Enum.GetValues(typeof(Step)).Length;
		CreateTipUILookup();
		CreateTipTextLookup();
		CreateAssetLookup();
	}

	private void Start()
	{
		GameBuilderCanvas.instance.OnNewGameInit += HandleGameInit;
		MegaBoardCanvas.instance.OnTileSelect += HandleTileSelect;
		UICaptureToggleTool.instance.OnClick += HandleCaptureOnClick;
		CaptureManager.instance.OnCaptureDismiss += HandleCaptureDismiss;
		CaptureManager.instance.OnCaptureConfirm += HandleCaptureConfirm;
		GameBuilderCanvas.instance.OnGameBuilderModeChange += HandleGameBuilderModeChange;
		GameBuilderCanvas.instance.OnConfigSelect += HandleConfigItem;
	}

	private void OnDestroy()
	{
		if (GameBuilderCanvas.instance != null)
		{
			GameBuilderCanvas.instance.OnNewGameInit -= HandleGameInit;
		}
		if (MegaBoardCanvas.instance != null)
		{
			MegaBoardCanvas.instance.OnTileSelect -= HandleTileSelect;
		}
		if (UICaptureToggleTool.instance != null)
		{
			UICaptureToggleTool.instance.OnClick -= HandleCaptureOnClick;
		}
		if (CaptureManager.instance != null)
		{
			CaptureManager.instance.OnCaptureDismiss -= HandleCaptureDismiss;
			CaptureManager.instance.OnCaptureConfirm -= HandleCaptureConfirm;
		}
		if (GameBuilderCanvas.instance != null)
		{
			GameBuilderCanvas.instance.OnGameBuilderModeChange -= HandleGameBuilderModeChange;
			GameBuilderCanvas.instance.OnConfigSelect -= HandleConfigItem;
		}
	}

	private void CreateTipUILookup()
	{
		uiTipForStep = new Dictionary<Step, UITipVisual>
		{
			{
				Step.RoomSelect1,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(278f, -10f)), CaretDirection.Left, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip1,
				new UITipVisual(new RectPosition(Direction.Right, new Vector2(-278f, -210f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.MapToggle,
				new UITipVisual(new RectPosition(Direction.Left, new Vector2(190f, -216f)), CaretDirection.LeftDown, new Vector2(500f, 300f))
			},
			{
				Step.RoomSelect2,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(354f, -10f)), CaretDirection.Left, new Vector2(500f, 300f))
			},
			{
				Step.CaptureTip2,
				new UITipVisual(new RectPosition(Direction.Right, new Vector2(-278f, -210f)), CaretDirection.RightDown, new Vector2(500f, 300f))
			},
			{
				Step.Configure,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-160f, 110f)), CaretDirection.Down, new Vector2(500f, 300f))
			},
			{
				Step.ConfigStoryBlock,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(140f, 254f)), CaretDirection.Right, new Vector2(500f, 300f))
			},
			{
				Step.TestTip1,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-16f, 90f)), CaretDirection.DownRight, new Vector2(500f, 300f))
			},
			{
				Step.ConfigPowerUp,
				new UITipVisual(new RectPosition(Direction.All, new Vector2(80f, 60f)), CaretDirection.Right, new Vector2(500f, 300f))
			},
			{
				Step.TestTip2,
				new UITipVisual(new RectPosition(Direction.DownRight, new Vector2(-16f, 90f)), CaretDirection.DownRight, new Vector2(500f, 300f))
			}
		};
	}

	private void CreateTipTextLookup()
	{
		textForStep = new Dictionary<Step, StepText>
		{
			{
				Step.RoomSelect1,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges11", "Select Room"), LocalizationManager.getInstance().getLocalizedText("challenges12", "Select this room to get started with your Layout!"))
			},
			{
				Step.CaptureTip1,
				new StepText(LocalizationManager.getInstance().getLocalizedText("gamebuilder26", "Capture Layout") + " 1", LocalizationManager.getInstance().getLocalizedText("challenges17", "Tap the capture button to bring your Gameboard layout into the app and begin to build your game!"))
			},
			{
				Step.MapToggle,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges24", "Map View"), LocalizationManager.getInstance().getLocalizedText("challenges25", "Great job! Now let's go to Map view to expand your game by adding another room."))
			},
			{
				Step.RoomSelect2,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges11", "Select Room"), LocalizationManager.getInstance().getLocalizedText("challenges12", "Select this room to get started with your Layout!"))
			},
			{
				Step.CaptureTip2,
				new StepText(LocalizationManager.getInstance().getLocalizedText("gamebuilder26", "Capture Layout") + " 2", LocalizationManager.getInstance().getLocalizedText("challenges29", "Tap the capture button to bring your second Gameboard into the app, soon you'll be a pro!"))
			},
			{
				Step.Configure,
				new StepText(LocalizationManager.getInstance().getLocalizedText("gamebuilder34", "Configure"), LocalizationManager.getInstance().getLocalizedText("challenges31", "We're going to need to go into configure mode now, tap here to get started."))
			},
			{
				Step.ConfigStoryBlock,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges32", "Configure Story Block"), LocalizationManager.getInstance().getLocalizedText("challenges33", "Tap this story block and select the Game End Flag checkbox and hit Save!"))
			},
			{
				Step.TestTip1,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges34", "Play Test"), LocalizationManager.getInstance().getLocalizedText("challenges35", "Time to test, go ahead and play your game and see if you can get to the end flag!"))
			},
			{
				Step.ConfigPowerUp,
				new StepText(LocalizationManager.getInstance().getLocalizedText(string.Empty, "Configure Power Up"), LocalizationManager.getInstance().getLocalizedText("challenges39", "This power-up isn't right. Tap on it to configure it to a Jetpack so you can fly to the end flag!"))
			},
			{
				Step.TestTip2,
				new StepText(LocalizationManager.getInstance().getLocalizedText("challenges34", "Play Test") + " 2", LocalizationManager.getInstance().getLocalizedText("challenges40", "Ok let's try testing again, get that jetpack and fly to the end flag!"))
			}
		};
	}

	private void CreateAssetLookup()
	{
		boardAssetForStep = new Dictionary<Step, string>
		{
			{
				Step.CaptureLayout1,
				AssetManager.challengeLayout_1_1
			},
			{
				Step.CaptureLayout2,
				AssetManager.challengeLayout_1_2
			}
		};
	}

	private void HandleConfigItem(UIConfigItem menu, BloxelTile tile)
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == type)
		{
			if (currentTip != null)
			{
				currentTip.Dismiss();
			}
			if (tile.tileInfo.wireframeColor == BlockColor.White)
			{
				_npcConfigMenu = (UIPopupNPCConfig)menu;
				_npcConfigMenu.OnFlagChange += HandleFlagSet;
			}
			else if (tile.tileInfo.wireframeColor == BlockColor.Pink)
			{
				_powerupConfigMenu = (UIPopupPowerupConfig)menu;
				_powerupConfigMenu.OnConfigure += HandlePowerUpConfig;
			}
		}
	}

	private void HandleTileSelect(MegaBoardTile tile)
	{
		if (!ChallengeManager.instance.hasActiveChallenge || ChallengeManager.instance.latestActiveChallenge != type)
		{
			return;
		}
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		if (currentStep == Step.RoomSelect1)
		{
			if (tile.loc == BloxelLevel.defaultLocation)
			{
				BuildFirstLayout(tile);
				return;
			}
		}
		else if (currentStep == Step.RoomSelect2 && tile.loc == new GridLocation(7, 6))
		{
			BuildSecondLayout(tile);
			return;
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
	}

	private void HandleCaptureDismiss()
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == type)
		{
			if (currentStep == Step.BuildLayout1)
			{
				CaptureTip1();
			}
			else if (currentStep == Step.BuildLayout2)
			{
				CaptureTip2();
			}
		}
	}

	public override void Init()
	{
		currentStep = Step.Welcome;
		BloxelLibrary.instance.libraryToggle.gameObject.SetActive(false);
		GameBuilderCanvas.instance.currentMode = GameBuilderMode.None;
		PixelEditorController.instance.SwitchCanvasMode(CanvasMode.LevelEditor);
		uiChallengeHeader = ChallengeManager.instance.ShowHeader();
		uiChallengeHeader.OnInfo += InfoButtonPressed;
		GameBuilderCanvas.instance.AdjustStartForChallenge();
		SetupBlockingRects();
	}

	private void SetupBlockingRects()
	{
		Color clear = Color.clear;
		RectPosition rectPos = new RectPosition(Direction.DownLeft, Vector2.zero);
		Vector2 size = new Vector2(UIOverlayCanvas.instance.rect.sizeDelta.x, 80f);
		RectPosition rectPos2 = new RectPosition(Direction.Left, Vector2.zero);
		Vector2 size2 = new Vector2(250f, 800f);
		RectPosition rectPos3 = new RectPosition(Direction.Right, new Vector2(0f, 70f));
		Vector2 size3 = new Vector2(100f, 750f);
		RectPosition rectPos4 = new RectPosition(Direction.All, new Vector2(0f, 0f));
		Vector2 size4 = new Vector2(862f, 862f);
		uiRectangleBottom = UIOverlayCanvas.instance.GenerateRectangle(rectPos, size, clear);
		uiRectangleBottom.OnClick += base.BlockingRectTapped;
		uiRectangleLeft = UIOverlayCanvas.instance.GenerateRectangle(rectPos2, size2, clear);
		uiRectangleLeft.OnClick += base.BlockingRectTapped;
		uiRectangleRight = UIOverlayCanvas.instance.GenerateRectangle(rectPos3, size3, clear);
		uiRectangleRight.OnClick += base.BlockingRectTapped;
		uiRectangleCenter = UIOverlayCanvas.instance.GenerateRectangle(rectPos4, size4, clear);
		uiRectangleCenter.OnClick += base.BlockingRectTapped;
		uiRectangleCenter.SetInputBlocking(false);
	}

	private void HandleGameInit()
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == type)
		{
			currentStep = Step.RoomSelect1;
			CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		}
	}

	private void BuildFirstLayout(MegaBoardTile tile)
	{
		currentStep = Step.BuildLayout1;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		GameBuilderCanvas.instance.MegaTileClicked(tile);
		GeneratePopup_BuildLayout(AssetManager.challengeLayout_1_1, LocalizationManager.getInstance().getLocalizedText("challenges82", "Build Layout") + " 1", LocalizationManager.getInstance().getLocalizedText("challenges15", "Use your Bloxels Gameboard to build this layout."), "2", LocalizationManager.getInstance().getLocalizedText("challenges14", "Layout") + " 1");
	}

	public override void CurrentLayoutComplete()
	{
		base.CurrentLayoutComplete();
		if (currentStep == Step.BuildLayout1)
		{
			CaptureTip1();
		}
		else if (currentStep == Step.BuildLayout2)
		{
			CaptureTip2();
		}
	}

	private void CaptureTip1()
	{
		currentStep = Step.CaptureTip1;
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleCenter.SetInputBlocking(true);
	}

	private void HandleCaptureOnClick()
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == type)
		{
			uiRectangleCenter.SetInputBlocking(false);
			if (currentTip != null)
			{
				currentTip.Dismiss();
			}
			if (currentStep == Step.CaptureTip1)
			{
				CaptureFirstLayout();
			}
			else
			{
				CaptureSecondLayout();
			}
		}
	}

	private void CaptureFirstLayout()
	{
		uiRectangleLeft.SetInputBlocking(false);
		currentStep = Step.CaptureLayout1;
	}

	private void HandleCaptureConfirm()
	{
		if (!ChallengeManager.instance.hasActiveChallenge || ChallengeManager.instance.latestActiveChallenge != type)
		{
			return;
		}
		CaptureManager.instance.ForceClose();
		if (!CheckMatch(boardAssetForStep[currentStep], CaptureManager.latest))
		{
			if (currentStep == Step.CaptureLayout1)
			{
				CaptureTip1();
			}
			else if (currentStep == Step.CaptureLayout2)
			{
				CaptureTip2();
			}
		}
		else if (currentStep == Step.CaptureLayout1)
		{
			MapToggle();
		}
		else if (currentStep == Step.CaptureLayout2)
		{
			Configure();
		}
	}

	private void MapToggle()
	{
		currentStep = Step.MapToggle;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleLeft.SetInputBlocking(false);
		uiRectangleCenter.SetInputBlocking(true);
	}

	private void HandleGameBuilderModeChange(GameBuilderMode _mode)
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == type)
		{
			if (_mode == GameBuilderMode.Map && currentStep == Step.MapToggle)
			{
				SecondRoomSelect();
			}
			else if (_mode == GameBuilderMode.ConfigBlocks && currentStep == Step.Configure)
			{
				ConfigStoryBlock();
			}
			else if (_mode == GameBuilderMode.Test && currentStep == Step.TestTip1)
			{
				FirstTest();
			}
			else if (_mode == GameBuilderMode.Test && currentStep == Step.TestTip2)
			{
				SecondTest();
			}
		}
	}

	private void SecondRoomSelect()
	{
		currentStep = Step.RoomSelect2;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleCenter.SetInputBlocking(false);
		uiRectangleLeft.SetInputBlocking(true);
	}

	private void BuildSecondLayout(MegaBoardTile tile)
	{
		currentStep = Step.BuildLayout2;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		GameBuilderCanvas.instance.MegaTileClicked(tile);
		GeneratePopup_BuildLayout(AssetManager.challengeLayout_1_2, LocalizationManager.getInstance().getLocalizedText("challenges82", "Build Layout") + " 2", LocalizationManager.getInstance().getLocalizedText("challenges28", "Use your Bloxels Gameboard to build this second layout."), "5", LocalizationManager.getInstance().getLocalizedText("challenges14", "Layout") + " 2");
	}

	private void CaptureTip2()
	{
		currentStep = Step.CaptureTip2;
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleLeft.SetInputBlocking(true);
		uiRectangleCenter.SetInputBlocking(true);
	}

	private void CaptureSecondLayout()
	{
		currentStep = Step.CaptureLayout2;
		uiRectangleLeft.SetInputBlocking(false);
		uiRectangleCenter.SetInputBlocking(false);
	}

	private void Configure()
	{
		currentStep = Step.Configure;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleCenter.SetInputBlocking(true);
		uiRectangleLeft.SetInputBlocking(true);
		uiRectangleBottom.SetInputBlocking(false);
	}

	private void ConfigStoryBlock()
	{
		currentStep = Step.ConfigStoryBlock;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleCenter.SetInputBlocking(false);
		uiRectangleBottom.SetInputBlocking(true);
	}

	private void HandleFlagSet(bool isOn)
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == type)
		{
			if (isOn)
			{
				TestTip1();
				_npcConfigMenu = null;
			}
			else
			{
				CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
			}
		}
	}

	private void TestTip1()
	{
		currentStep = Step.TestTip1;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleBottom.SetInputBlocking(false);
		uiRectangleBottom.ReSize(new Vector2(uiRectangleBottom.startSize.x - GameBuilderCanvas.instance.primaryModes[0].rect.sizeDelta.x, uiRectangleBottom.startSize.y));
	}

	private void FirstTest()
	{
		currentStep = Step.Test1;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		uiRectangleLeft.SetInputBlocking(false);
		uiRectangleRight.SetInputBlocking(false);
		uiRectangleBottom.ReSize(uiRectangleBottom.startSize);
		StartCoroutine("DelayedInfoRoutine");
	}

	private IEnumerator DelayedInfoRoutine()
	{
		yield return new WaitForSeconds(10f);
		uiChallengeHeader.ShowInfoButton();
	}

	private void InfoButtonPressed()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(infoPopupPrefab);
		UIPopupChallengeInfo menu = gameObject.GetComponent<UIPopupChallengeInfo>();
		menu.Init(LocalizationManager.getInstance().getLocalizedText("challenges18", "Uh oh!"), LocalizationManager.getInstance().getLocalizedText("challenges36", "Doesn't look like you're able to get to the end flag. That's because we forgot to configure the powerup in the first room. We should probably do that now."), LocalizationManager.getInstance().getLocalizedText("challenges37", "Let's do it!"));
		menu.OnComplete += delegate
		{
			menu = null;
			uiChallengeHeader.HideInfoButton();
			GameBuilderCanvas.instance.ComeBackFromTest();
			ConfigPowerUp();
		};
	}

	private void ConfigPowerUp()
	{
		currentStep = Step.ConfigPowerUp;
		uiRectangleLeft.SetInputBlocking(true);
		uiRectangleRight.SetInputBlocking(true);
		uiRectangleBottom.SetInputBlocking(true);
		GameBuilderCanvas.instance.mapShiftButtons[0].Pressed();
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
	}

	private void HandlePowerUpConfig(PowerUpType _type)
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == type)
		{
			if (_type != PowerUpType.Jetpack)
			{
				CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
			}
			else
			{
				TestTip2();
			}
		}
	}

	private void TestTip2()
	{
		currentStep = Step.TestTip2;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		CreateTip(uiTipForStep[currentStep], textForStep[currentStep]);
		uiRectangleBottom.SetInputBlocking(false);
		uiRectangleBottom.ReSize(new Vector2(uiRectangleBottom.startSize.x - GameBuilderCanvas.instance.primaryModes[0].rect.sizeDelta.x, uiRectangleBottom.startSize.y));
	}

	private void SecondTest()
	{
		currentStep = Step.Test2;
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		uiRectangleLeft.SetInputBlocking(false);
		uiRectangleRight.SetInputBlocking(false);
		uiRectangleBottom.ReSize(uiRectangleBottom.startSize);
	}

	public void Complete()
	{
		currentStep = Step.Complete;
		GameObject gameObject = UIOverlayCanvas.instance.Popup(challengeCompletePrefab);
		UIPopupChallengeComplete menu = gameObject.GetComponent<UIPopupChallengeComplete>();
		menu.Init(type);
		GemManager.instance.AddGemsForType(GemEarnable.Type.GameBuilder_Easy);
		menu.OnComplete += delegate
		{
			menu = null;
			BadgeManager.instance.BadgeEarned(BadgeManager.instance.badgeForChallenge[type]);
			ChallengeManager.instance.CompleteCurrentChallenge();
			ChallengeManager.instance.QuitLatest();
			GameBuilderCanvas.instance.ComeBackFromTest();
			StartCoroutine(ShowChallengeMenu());
		};
	}

	private IEnumerator ShowChallengeMenu()
	{
		yield return new WaitForSeconds(1f);
		ChallengeManager.instance.ShowChallengeMenu();
	}

	public override void Quit()
	{
		StopAllCoroutines();
		BloxelLibrary.instance.libraryToggle.gameObject.SetActive(true);
		GameBuilderCanvas.instance.ResetStart();
		if (currentTip != null)
		{
			currentTip.Dismiss();
		}
		if (currentLayoutPopup != null)
		{
			currentLayoutPopup.Dismiss();
		}
		if (uiRectangleBottom != null)
		{
			uiRectangleBottom.DestroyMe();
		}
		if (uiRectangleCenter != null)
		{
			uiRectangleCenter.DestroyMe();
		}
		if (uiRectangleLeft != null)
		{
			uiRectangleLeft.DestroyMe();
		}
		if (uiRectangleRight != null)
		{
			uiRectangleRight.DestroyMe();
		}
		uiChallengeHeader = null;
	}
}
