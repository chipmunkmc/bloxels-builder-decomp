using System.Collections.Generic;
using UnityEngine;

public class CharacterAnimationController : MonoBehaviour
{
	public Transform containerTransform;

	public AnimationType currentState;

	public BloxelCharacter characterProject;

	public CharacterAnimationRenderController idleRenderer;

	public CharacterAnimationRenderController walkRenderer;

	public CharacterAnimationRenderController jumpRenderer;

	private int numberOfStates;

	public CharacterAnimationRenderController currentRenderer { get; private set; }

	private void Awake()
	{
		containerTransform = base.transform.parent;
	}

	private void OnDisable()
	{
		numberOfStates = 0;
	}

	public void InitializeAnimations(BloxelCharacter characterProject)
	{
		this.characterProject = characterProject;
		ReleaseAllFrameChunks();
		idleRenderer.animationProject = null;
		walkRenderer.animationProject = null;
		jumpRenderer.animationProject = null;
		Dictionary<AnimationType, BloxelAnimation>.Enumerator enumerator = characterProject.animations.GetEnumerator();
		while (enumerator.MoveNext())
		{
			AnimationType key = enumerator.Current.Key;
			BloxelAnimation value = enumerator.Current.Value;
			switch (key)
			{
			case AnimationType.Idle:
				idleRenderer.Initilaize(value);
				break;
			case AnimationType.Walk:
				walkRenderer.Initilaize(value);
				break;
			case AnimationType.Jump:
				jumpRenderer.Initilaize(value);
				break;
			}
		}
		if (idleRenderer.animationProject == null)
		{
			idleRenderer.Initilaize(AssetManager.instance.defaultCharacterAnimations[AnimationType.Idle]);
		}
		if (walkRenderer.animationProject == null)
		{
			walkRenderer.Initilaize(AssetManager.instance.defaultCharacterAnimations[AnimationType.Walk]);
		}
		if (jumpRenderer.animationProject == null)
		{
			jumpRenderer.Initilaize(AssetManager.instance.defaultCharacterAnimations[AnimationType.Jump]);
		}
		SetToIdle();
	}

	public void ReInitializeAnimationForState(AnimationType type)
	{
		switch (type)
		{
		case AnimationType.Idle:
			idleRenderer.animationProject = null;
			idleRenderer.Initilaize(characterProject.animations[AnimationType.Idle]);
			SetToIdle();
			break;
		case AnimationType.Walk:
			walkRenderer.animationProject = null;
			walkRenderer.Initilaize(characterProject.animations[AnimationType.Walk]);
			SetToWalk();
			break;
		case AnimationType.Jump:
			jumpRenderer.animationProject = null;
			jumpRenderer.Initilaize(characterProject.animations[AnimationType.Jump]);
			SetToJump();
			break;
		}
	}

	public void SetCharacterBoxColliderProperties(ref BoxCollider2D characterCollider)
	{
		byte b = 14;
		byte b2 = 14;
		byte b3 = 0;
		byte b4 = 0;
		bool flag = false;
		bool flag2 = false;
		bool flag3 = false;
		if (idleRenderer != null && idleRenderer.animationProject != null)
		{
			BlockBounds blockBounds = idleRenderer.animationProject.blockBounds;
			if (!blockBounds.isEmpty)
			{
				if (blockBounds.xMin < b)
				{
					b = blockBounds.xMin;
				}
				if (blockBounds.yMin < b2)
				{
					b2 = blockBounds.yMin;
				}
				if (blockBounds.xMax > b3)
				{
					b3 = blockBounds.xMax;
				}
				if (blockBounds.yMax > b4)
				{
					b4 = blockBounds.yMax;
				}
				flag = true;
			}
		}
		if (walkRenderer != null && walkRenderer.animationProject != null)
		{
			BlockBounds blockBounds2 = walkRenderer.animationProject.blockBounds;
			if (!blockBounds2.isEmpty)
			{
				if (blockBounds2.xMin < b)
				{
					b = blockBounds2.xMin;
				}
				if (blockBounds2.yMin < b2)
				{
					b2 = blockBounds2.yMin;
				}
				if (blockBounds2.xMax > b3)
				{
					b3 = blockBounds2.xMax;
				}
				if (blockBounds2.yMax > b4)
				{
					b4 = blockBounds2.yMax;
				}
				flag2 = true;
			}
		}
		if (jumpRenderer != null && jumpRenderer.animationProject != null)
		{
			BlockBounds blockBounds3 = jumpRenderer.animationProject.blockBounds;
			if (!blockBounds3.isEmpty)
			{
				if (blockBounds3.xMin < b)
				{
					b = blockBounds3.xMin;
				}
				if (blockBounds3.yMin < b2)
				{
					b2 = blockBounds3.yMin;
				}
				if (blockBounds3.xMax > b3)
				{
					b3 = blockBounds3.xMax;
				}
				if (blockBounds3.yMax > b4)
				{
					b4 = blockBounds3.yMax;
				}
				flag3 = true;
			}
		}
		if (!flag && !flag2 && !flag3)
		{
			characterCollider.size = new Vector2(8f, 10f);
			characterCollider.offset = new Vector2(0f, 5f);
			PixelPlayerController.instance.totalHorizontalRays = 11;
			PixelPlayerController.instance.totalVerticalRays = 9;
			return;
		}
		Vector2 vector = new Vector2(b3 - b, b4 - b2);
		Vector2 size = new Vector2(vector.x * 0.75f, vector.y * 0.875f);
		base.transform.localPosition = new Vector3((float)(-(b3 + b)) / 2f + 0.5f, (float)(-b2) + 0.5f, 0f);
		Vector2 offset = new Vector2(0f, size.y / 2f);
		characterCollider.size = size;
		characterCollider.offset = offset;
		PixelPlayerController.instance.totalHorizontalRays = Mathf.Clamp(Mathf.CeilToInt(size.y), 1, 13) + 1;
		PixelPlayerController.instance.totalVerticalRays = Mathf.Clamp(Mathf.CeilToInt(size.x), 1, 13) + 1;
	}

	private void Update()
	{
		if (currentRenderer != null)
		{
			currentRenderer.CheckForFrameUpdate();
		}
	}

	public void QuitPlayback()
	{
		ReleaseAllFrameChunks();
		currentRenderer = null;
	}

	public void SetAnimationState(AnimationType animationState)
	{
		if (currentState != animationState)
		{
			currentState = animationState;
			switch (currentState)
			{
			case AnimationType.Idle:
				SetToIdle();
				break;
			case AnimationType.Walk:
				SetToWalk();
				break;
			case AnimationType.Jump:
				SetToJump();
				break;
			}
		}
	}

	private void SetToIdle()
	{
		currentRenderer = idleRenderer;
		idleRenderer.Play();
		walkRenderer.Stop();
		jumpRenderer.Stop();
	}

	private void SetToWalk()
	{
		currentRenderer = walkRenderer;
		idleRenderer.Stop();
		walkRenderer.Play();
		jumpRenderer.Stop();
	}

	private void SetToJump()
	{
		currentRenderer = jumpRenderer;
		idleRenderer.Stop();
		walkRenderer.Stop();
		jumpRenderer.Play();
	}

	public void ReleaseAllFrameChunks()
	{
		if (idleRenderer != null)
		{
			idleRenderer.ReleaseCurrentChunks();
		}
		if (walkRenderer != null)
		{
			walkRenderer.ReleaseCurrentChunks();
		}
		if (jumpRenderer != null)
		{
			jumpRenderer.ReleaseCurrentChunks();
		}
	}

	public void HandlePlayerDeath(bool shouldRelease = true)
	{
		currentRenderer.Stop();
		float num = PixelPlayerController.instance.selfTransform.localScale.y * 0.5f;
		TileExplosionManager.instance.ExplodeChunk(currentRenderer.currentFrameChunk, base.transform, PixelPlayerController.instance.rigidBody2D, PixelPlayerController.instance.lastHitPosition + TileExplosionManager.instance.bombExplosionOffset * num * 0.5f, TileExplosionManager.instance.bombExplosionForce * num, TileExplosionManager.instance.bombExplosionRadius * num, TileExplosionManager.instance.bombUpwardsModifier * num, PixelPlayerController.instance.selfTransform.localScale);
		if (shouldRelease)
		{
			ReleaseAllFrameChunks();
		}
	}
}
