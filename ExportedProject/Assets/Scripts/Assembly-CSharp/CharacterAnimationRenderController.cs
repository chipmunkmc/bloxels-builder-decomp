using UnityEngine;
using VoxelEngine;

public class CharacterAnimationRenderController : MonoBehaviour
{
	private Transform selfTransform;

	public MovableChunk2D currentFrameChunk;

	private MovableChunk2D[] frameChunks;

	public static Vector3 builderPivotOffset = new Vector3(-6f, -6f, 0f);

	public static Vector3 gameplayPivotOffset = new Vector3(-6f, 0f, 0f);

	private Vector3 pivotOffset;

	public BloxelAnimation animationProject;

	private int numberOfFrames;

	private int currentFrameIndex;

	private float timeOfLastFrameChange;

	private float _framesPerSecond;

	private float frameDuration;

	public MeshFilter localMeshFilter;

	public MeshRenderer localMeshRenderer;

	public PolygonCollider2D localCollider;

	public float framesPerSecond
	{
		get
		{
			return _framesPerSecond;
		}
		set
		{
			_framesPerSecond = value;
			frameDuration = 1f / _framesPerSecond;
		}
	}

	private void Awake()
	{
		selfTransform = base.transform;
		localCollider.pathCount = 0;
		localMeshFilter.sharedMesh = null;
	}

	public void CheckForFrameUpdate()
	{
		if (timeOfLastFrameChange + frameDuration < Time.time && numberOfFrames > 0)
		{
			currentFrameIndex = (currentFrameIndex + 1) % numberOfFrames;
			SetCurrentFrame(currentFrameIndex);
		}
	}

	public void UpdateFrameRate(float val)
	{
		framesPerSecond = val;
	}

	public void Initilaize(BloxelAnimation _animationProject)
	{
		animationProject = _animationProject;
		ReleaseCurrentChunks();
		numberOfFrames = _animationProject.boards.Count;
		if (numberOfFrames != 0)
		{
			frameChunks = new MovableChunk2D[numberOfFrames];
			if (PixelEditorController.instance != null && PixelEditorController.instance.mode == CanvasMode.CharacterBuilder)
			{
				pivotOffset = builderPivotOffset;
			}
			else
			{
				pivotOffset = gameplayPivotOffset;
			}
			selfTransform.localPosition = pivotOffset;
			for (int i = 0; i < numberOfFrames; i++)
			{
				MovableChunk2D movableChunk2D = MovableChunk2DPool.instance.Spawn();
				frameChunks[i] = movableChunk2D;
			}
			framesPerSecond = animationProject.GetFrameRate();
			currentFrameChunk = frameChunks[0];
			SetCurrentFrame(0);
		}
	}

	private void SetUpFrameProperties(MovableChunk2D chunk)
	{
	}

	private void SetCurrentFrame(int frameIndex)
	{
		if (frameChunks != null)
		{
			currentFrameChunk = frameChunks[frameIndex];
			if (!currentFrameChunk.initialized)
			{
				currentFrameChunk.InitWithBoard(animationProject.boards[frameIndex], false);
			}
			currentFrameChunk.UpdateChunk(animationProject.boards[frameIndex].localID, ref localMeshFilter, ref localCollider, false);
			timeOfLastFrameChange = Time.time;
		}
	}

	public void Play(int startingFrameIndex = 0)
	{
		SetCurrentFrame(startingFrameIndex);
		currentFrameIndex = startingFrameIndex;
	}

	public void Stop()
	{
	}

	private void OnDestroy()
	{
		ReleaseCurrentChunks();
	}

	public void ReleaseCurrentChunks()
	{
		if (currentFrameChunk != null && currentFrameChunk.spawned)
		{
			currentFrameChunk.Despawn();
			currentFrameChunk = null;
		}
		if (frameChunks != null && frameChunks != null)
		{
			for (int i = 0; i < frameChunks.Length; i++)
			{
				MovableChunk2D movableChunk2D = frameChunks[i];
				if (movableChunk2D.spawned)
				{
					movableChunk2D.Despawn();
				}
			}
			frameChunks = null;
		}
		if (localCollider != null && localMeshFilter != null)
		{
			localCollider.pathCount = 0;
			localMeshFilter.sharedMesh = null;
		}
	}
}
