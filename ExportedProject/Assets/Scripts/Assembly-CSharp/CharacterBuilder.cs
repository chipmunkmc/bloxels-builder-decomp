using System;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using DiscoCapture;
using UnityEngine;
using UnityEngine.UI;

public class CharacterBuilder : MonoBehaviour
{
	public delegate void HandleStateUpdate(AnimationType _state);

	public delegate void HandleOnPreview();

	public static CharacterBuilder instance;

	[Header("| ========= Data ========= |")]
	public BloxelCharacter currentCharacter;

	public UICharacterStateSelect[] animationStates;

	public AnimationType currentAnimationState;

	[Header("| ========= UI ========= |")]
	public Button uiButtonPreview;

	public UnityEngine.Object confirmationMessagePrefab;

	public UIPopupStateChange stateChangeMessage;

	public CharacterPlacementGrid gameGrid;

	public event HandleStateUpdate OnState;

	public event HandleOnPreview OnPreview;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (confirmationMessagePrefab == null)
		{
			confirmationMessagePrefab = Resources.Load("Prefabs/UIPopupStateChange");
		}
	}

	private void Start()
	{
		BloxelCharacterLibrary.instance.OnProjectSelect += LibraryItemChanged;
		BloxelCharacterLibrary.instance.OnProjectDelete += LibraryItemDeleted;
		BloxelBoardLibrary.instance.OnProjectSelect += LibraryItemSelected;
		BloxelAnimationLibrary.instance.OnProjectSelect += LibraryItemSelected;
		BloxelCamera.instance.OnModeChange += HandleCameraModeChange;
		uiButtonPreview.onClick.AddListener(Preview);
		for (int i = 0; i < animationStates.Length; i++)
		{
			animationStates[i].OnSelect += UpdateAnimationState;
		}
	}

	private void OnEnable()
	{
		CaptureManager.instance.OnCaptureConfirm += HandleCaptureConfirm;
	}

	private void OnDisable()
	{
		CaptureManager.instance.OnCaptureConfirm -= HandleCaptureConfirm;
	}

	private void HandleCaptureConfirm()
	{
		bool flag = true;
		foreach (KeyValuePair<AnimationType, BloxelAnimation> animation in currentCharacter.animations)
		{
			if (animation.Key == currentAnimationState)
			{
				continue;
			}
			if (animation.Value.boards.Count != 1)
			{
				flag = false;
				break;
			}
			BloxelBoard bloxelBoard = animation.Value.boards[0];
			bool flag2 = true;
			for (int i = 0; i < 13; i++)
			{
				if (!flag2)
				{
					break;
				}
				for (int j = 0; j < 13; j++)
				{
					if (!flag2)
					{
						break;
					}
					flag2 = bloxelBoard.blockColors[j, i] == BlockColor.Blank;
				}
			}
			if (flag2)
			{
				continue;
			}
			flag = false;
			break;
		}
		if (flag)
		{
			CaptureColorsToAllStates();
		}
	}

	private void CaptureColorsToAllStates()
	{
		foreach (KeyValuePair<AnimationType, BloxelAnimation> animation in currentCharacter.animations)
		{
			BloxelBoard bloxelBoard = animation.Value.boards[0];
			bloxelBoard.blockColors = ColorClassifier.BlockColors.Clone() as BlockColor[,];
		}
	}

	private void OnDestroy()
	{
		BloxelCharacterLibrary.instance.OnProjectSelect -= LibraryItemChanged;
		BloxelCharacterLibrary.instance.OnProjectDelete -= LibraryItemDeleted;
		BloxelBoardLibrary.instance.OnProjectSelect -= LibraryItemSelected;
		BloxelAnimationLibrary.instance.OnProjectSelect -= LibraryItemSelected;
		BloxelCamera.instance.OnModeChange -= HandleCameraModeChange;
	}

	private void HandleCameraModeChange()
	{
		if (PixelEditorController.instance.mode != CanvasMode.CharacterBuilder)
		{
			return;
		}
		if (BloxelCamera.instance.mode == BloxelCamera.CameraMode.Gameplay || BloxelCamera.instance.mode == BloxelCamera.CameraMode.Gameplay2D)
		{
			PixelEditorController.instance.primaryCanvasGroup.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				PixelEditorController.instance.primaryCanvasGroup.blocksRaycasts = false;
			}).OnComplete(delegate
			{
				GameplayController.instance.uiCanvasGroupHUD.gameObject.SetActive(true);
				GameplayController.instance.uiCanvasGroupHUD.DOFade(1f, UIAnimationManager.speedMedium);
				GameplayController.instance.ShowTouchControls();
			});
		}
		else
		{
			GameplayController.instance.uiCanvasGroupHUD.DOFade(0f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				GameplayController.instance.uiCanvasGroupHUD.gameObject.SetActive(false);
			});
			PixelEditorController.instance.primaryCanvasGroup.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				GameplayController.instance.HideTouchControls();
				PixelEditorController.instance.primaryCanvasGroup.blocksRaycasts = true;
			});
		}
	}

	private void LibraryItemSelected(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.CharacterBuilder)
		{
			ShowConfirmationMessage(item.dataModel);
		}
	}

	public void ChangeCurrentStateAnimation(BloxelProject project)
	{
		stateChangeMessage.OnChoice -= ChangeCurrentStateAnimation;
		stateChangeMessage = null;
		BloxelAnimation anim = null;
		switch (project.type)
		{
		case ProjectType.Board:
		{
			BloxelBoard board = ((BloxelBoard)project).Clone();
			anim = new BloxelAnimation(board);
			break;
		}
		case ProjectType.Animation:
			anim = ((BloxelAnimation)project).Clone();
			break;
		}
		currentCharacter.SetAnimationForState(currentAnimationState, anim, false);
		currentCharacter.Save();
		UpdateAnimationState(currentAnimationState);
	}

	public void SetCurrentStateAnimation(BloxelAnimation animation)
	{
		BloxelAnimation anim = animation.Clone();
		currentCharacter.SetAnimationForState(currentAnimationState, anim, false);
		currentCharacter.Save();
		UpdateAnimationState(currentAnimationState);
	}

	private void ShowConfirmationMessage(BloxelProject project)
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(confirmationMessagePrefab);
		stateChangeMessage = gameObject.GetComponent<UIPopupStateChange>();
		stateChangeMessage.Init(project, currentAnimationState);
		stateChangeMessage.OnChoice += ChangeCurrentStateAnimation;
	}

	private void LibraryItemDeleted(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.CharacterBuilder && item.isActive)
		{
			SetCharacter(null);
		}
	}

	private void LibraryItemChanged(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.CharacterBuilder)
		{
			SetCharacter(item.dataModel as BloxelCharacter);
		}
	}

	public void ReInit()
	{
		AnimationTimeline.instance.InitFromBloxelAnimation(currentCharacter.animations[currentAnimationState]);
	}

	private void UpdateAnimationState(AnimationType _state)
	{
		currentAnimationState = _state;
		for (int i = 0; i < animationStates.Length; i++)
		{
			if (animationStates[i].animationState != currentAnimationState)
			{
				animationStates[i].uiButton.image.DOColor(animationStates[i].inactiveColor, UIAnimationManager.speedMedium);
			}
			else
			{
				animationStates[i].uiButton.image.DOColor(animationStates[i].activeColor, UIAnimationManager.speedMedium);
			}
		}
		if (currentCharacter != null)
		{
			AnimationTimeline.instance.InitFromBloxelAnimation(currentCharacter.animations[currentAnimationState]);
		}
		if (this.OnState != null)
		{
			this.OnState(_state);
		}
	}

	public void SetCharacter(BloxelCharacter _character)
	{
		currentCharacter = _character;
		UpdateAnimationState(AnimationType.Idle);
	}

	public void Create()
	{
		BloxelCharacter bloxelCharacter = new BloxelCharacter();
		bool flag = BloxelCharacterLibrary.instance.AddProject(bloxelCharacter);
		SetCharacter(bloxelCharacter);
	}

	public void ComeBackFromPreview()
	{
		gameGrid.transform.localPosition = new Vector3(gameGrid.transform.localPosition.x, gameGrid.transform.localPosition.y, -6.5f);
		gameGrid.gameObject.SetActive(false);
		if (GameplayController.instance.heroObject != null)
		{
			UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
		}
		if (SoundManager.instance.loopingSFXPlayer.isPlaying)
		{
			SoundManager.instance.StopLoopingSound();
		}
		WorldWrapper.instance.Hide();
		PixelEditorController.instance.primaryCanvas.gameObject.SetActive(true);
		BloxelLibrary.instance.libraryCanvas.gameObject.SetActive(true);
		BloxelLibrary.instance.Show();
		GameplayController.instance.CancelReSpawnPlayerDelayInTest();
		GameplayController.instance.CancelDroppingCharacter();
		WorldWrapper.instance.ClearRuntimeContainer();
		SoundManager.instance.PlayMusic(SoundManager.instance.editorMusic);
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
		GameplayController.instance.PlayerExitedWater();
	}

	public void Preview()
	{
		SoundManager.PlayOneShot(SoundManager.instance.editorPlay);
		if (GameplayBuilder.instance.isStreaming)
		{
			GameplayBuilder.instance.UnloadGameplay();
			GameplayBuilder.instance.isStreaming = false;
		}
		GameplayBuilder.instance.StartStreaming(AssetManager.instance.characterPreviewGame);
		GameplayBuilder.instance.InitWorldTileData();
		SetupHero();
		BloxelCamera.instance.target = GameplayController.instance.heroObject;
		BloxelCamera.instance.selfTransform.position = new Vector3(GameplayController.instance.heroObject.transform.position.x, GameplayController.instance.heroObject.transform.position.y, GameplayController.instance.heroObject.transform.position.z - 15f);
		BloxelLibrary.instance.libraryToggle.Hide();
		BloxelLibrary.instance.Hide();
		PixelEditorController.instance.primaryCanvas.gameObject.SetActive(false);
		BloxelLibrary.instance.libraryCanvas.gameObject.SetActive(false);
		GameplayController.instance.currentMode = GameplayController.Mode.Test;
		GameplayController.instance.gameplayActive = true;
		GameplayController.instance.currentGame = AssetManager.instance.characterPreviewGame;
		if (!WorldWrapper.instance.gameObject.activeInHierarchy)
		{
			WorldWrapper.instance.Show();
		}
		WorldWrapper.instance.currentLevelBounds.gameObject.SetActive(false);
		GameplayController.instance.startingPosition = new Vector3(GameplayController.instance.heroObject.transform.localPosition.x, GameplayController.instance.heroObject.transform.localPosition.y, 0f);
		GameplayController.instance.lastCheckpoint = GameplayController.instance.startingPosition;
		LevelBuilder.instance.SetupGameBackground(AssetManager.instance.characterPreviewGame);
		BloxelCamera.instance.target = GameplayController.instance.heroObject;
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		gameGrid.gameObject.SetActive(true);
		gameGrid.transform.localPosition = new Vector3(gameGrid.transform.localPosition.x, gameGrid.transform.localPosition.y, 6f);
		SoundManager.instance.PlayMusic(SoundManager.instance.musicChoiceLookup[AssetManager.instance.characterPreviewGame.musicChoice]);
		if (this.OnPreview != null)
		{
			this.OnPreview();
		}
	}

	private void SetupHero()
	{
		if (GameplayController.instance.heroObject != null)
		{
			UnityEngine.Object.DestroyImmediate(GameplayController.instance.heroObject);
		}
		Vector3 vector = new Vector3(1014f, 1014f, 0f);
		Vector3 localPosition = new Vector3((float)AssetManager.instance.characterPreviewGame.heroStartPosition.locationInBoard.c * 13f + 6f, (float)AssetManager.instance.characterPreviewGame.heroStartPosition.locationInBoard.r * 13f + 6f, 0f) + vector;
		GameplayController.instance.heroObject = UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Player/Player")) as GameObject;
		GameplayController.instance.heroObject.transform.SetParent(WorldWrapper.instance.selfTransform);
		GameplayController.instance.heroObject.transform.localPosition = localPosition;
		PixelPlayerController.instance.DisableForEditor();
		GameplayController.instance.lastCheckpoint = GameplayController.instance.heroObject.transform.localPosition + new Vector3(0f, 0f, 7f);
		currentCharacter.Save();
		PixelPlayerController.instance.animationController.InitializeAnimations(currentCharacter);
		GameplayController.instance.StartDroppingCharacter();
	}
}
