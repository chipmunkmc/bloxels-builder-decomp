using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;

public class CharacterBuilderCanvas : UIMoveable
{
	public delegate void HandleCharacterInit();

	public static CharacterBuilderCanvas instance;

	[Header("| ========= Data ========= |")]
	public CanvasMode mode = CanvasMode.CharacterBuilder;

	[Header("| ========= UI ========= |")]
	public CanvasGroup uiGroupStartMenu;

	public UIButton uiButtonStart;

	public UIButton uiButtonEdit;

	public GameObject startScreenHR;

	[Header("| ========= Tracking ========= |")]
	private bool initComplete;

	public event HandleCharacterInit OnNewCharacter;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		base.Awake();
	}

	private new void Start()
	{
		base.Start();
		BloxelCharacterLibrary.instance.OnProjectSelect += LibraryItemChanged;
		BloxelCharacterLibrary.instance.OnProjectDelete += LibraryItemDeleted;
		PixelToolPalette.instance.OnChange += HandleColorPaletteVisibility;
		uiButtonStart.OnClick += StartButtonPressed;
		uiButtonEdit.OnClick += ShowLibrary;
		initComplete = true;
	}

	private void OnDestroy()
	{
		uiButtonStart.OnClick -= StartButtonPressed;
		uiButtonEdit.OnClick -= ShowLibrary;
		BloxelCharacterLibrary.instance.OnProjectSelect -= LibraryItemChanged;
		BloxelCharacterLibrary.instance.OnProjectDelete -= LibraryItemDeleted;
		PixelToolPalette.instance.OnChange -= HandleColorPaletteVisibility;
	}

	private void ShowLibrary()
	{
		BloxelLibrary.instance.libraryToggle.Show();
	}

	private void LibraryItemDeleted(SavedProject item)
	{
		if (PixelEditorController.instance.mode == mode && item.isActive)
		{
			ShowStart();
		}
	}

	public void LibraryItemChanged(SavedProject item)
	{
		if (PixelEditorController.instance.mode == mode)
		{
			if (CharacterBuilder.instance.currentCharacter != null)
			{
				CharacterBuilder.instance.currentCharacter.Save();
			}
			HideStart();
		}
	}

	private void HandleColorPaletteVisibility(bool isOpen)
	{
		if (PixelEditorController.instance.mode == CanvasMode.CharacterBuilder)
		{
			if (isOpen)
			{
				PixelEditorPaintBoard.instance.characterBuilderTools.Hide();
			}
			else
			{
				PixelEditorPaintBoard.instance.characterBuilderTools.Show();
			}
		}
	}

	public new Tweener Show()
	{
		if (ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == Challenge.CharacterBuilder_Easy)
		{
			ShowStart();
			return base.Show();
		}
		if (CharacterBuilder.instance.currentCharacter != null)
		{
			HideStart();
			CharacterBuilder.instance.ReInit();
		}
		else
		{
			ShowStart();
		}
		return base.Show();
	}

	public new Tweener Hide()
	{
		if (CharacterBuilder.instance.currentCharacter != null)
		{
			CharacterBuilder.instance.currentCharacter.Save();
		}
		PixelEditorPaintBoard.instance.characterBuilderTools.Hide();
		AnimationTimeline.instance.Hide();
		return base.Hide();
	}

	public void AdjustStartForChallenge()
	{
		RectTransform startBtnRect = uiButtonStart.GetComponent<RectTransform>();
		uiButtonEdit.transform.localScale = Vector3.zero;
		startScreenHR.transform.localScale = Vector3.zero;
		startBtnRect.DOAnchorPos(Vector2.zero, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			startBtnRect.DOShakeScale(UIAnimationManager.speedSlow).OnStart(delegate
			{
				SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
			});
		});
	}

	public void ResetStart()
	{
		RectTransform component = uiButtonStart.GetComponent<RectTransform>();
		uiButtonEdit.transform.localScale = Vector3.one;
		startScreenHR.transform.localScale = Vector3.one;
		component.anchoredPosition = new Vector2(0f, 125f);
	}

	public void ShowStart()
	{
		uiGroupStartMenu.gameObject.SetActive(true);
		uiGroupStartMenu.DOFade(1f, UIAnimationManager.speedFast).OnStart(delegate
		{
			uiGroupStartMenu.blocksRaycasts = true;
			uiGroupStartMenu.interactable = true;
		}).OnComplete(delegate
		{
			AnimationTimeline.instance.DumpTimeline();
			AnimatedFrame.instance.Clear();
			AnimationTimeline.instance.Hide();
			PixelEditorPaintBoard.instance.Hide();
			PixelEditorPaintBoard.instance.characterBuilderTools.Hide();
		});
	}

	public void HideStart()
	{
		uiGroupStartMenu.DOFade(0f, UIAnimationManager.speedFast).OnStart(delegate
		{
			uiGroupStartMenu.blocksRaycasts = false;
			uiGroupStartMenu.interactable = false;
		}).OnComplete(delegate
		{
			uiGroupStartMenu.gameObject.SetActive(false);
			AnimationTimeline.instance.Show();
			PixelEditorPaintBoard.instance.Show();
			PixelEditorPaintBoard.instance.characterBuilderTools.Show();
		});
	}

	public void StartButtonPressed()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmA);
		CharacterBuilder.instance.Create();
		if (this.OnNewCharacter != null)
		{
			this.OnNewCharacter();
		}
	}
}
