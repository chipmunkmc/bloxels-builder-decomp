using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CharacterConfig : UIMoveable
{
	public static CharacterConfig instance;

	private BloxelCharacter _character;

	public Button uiButtonPullTab;

	public Button uiButtonReset;

	public Image uiImageCaret;

	public Text uiTextJumpHeightVal;

	public Text uiTextInertiaVal;

	public Text uiTextRunSpeedVal;

	public Text uiTextWeightVal;

	public Slider uiSliderJumpHeight;

	public Slider uiSliderInertia;

	public Slider uiSliderRunSpeed;

	public Slider uiSliderWeight;

	public Slider uiSliderDoubleJump;

	private float _jumpHeight;

	private float _inertia;

	private float _runSpeed;

	private float _characterGravity;

	private bool _doubleJump;

	public bool isOpen;

	private new void Awake()
	{
		base.Awake();
		if (instance == null)
		{
			instance = this;
		}
	}

	private new void Start()
	{
		uiButtonPullTab.onClick.AddListener(Toggle);
		uiButtonReset.onClick.AddListener(ResetDefaults);
		uiSliderJumpHeight.onValueChanged.AddListener(AdjustJumpHeight);
		uiSliderInertia.onValueChanged.AddListener(AdjustInertia);
		uiSliderRunSpeed.onValueChanged.AddListener(AdjustRunSpeed);
		uiSliderWeight.onValueChanged.AddListener(AdjustCharacterGravity);
		uiSliderDoubleJump.onValueChanged.AddListener(AdjustDoubleJump);
	}

	private void Toggle()
	{
		if (isOpen)
		{
			DeActivate();
		}
		else
		{
			Activate();
		}
	}

	private void Activate()
	{
		SoundManager.PlayEventSound(SoundEvent.DrawerSlide);
		rect.DOAnchorPos(basePosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isOpen = true;
		}).OnComplete(delegate
		{
			uiImageCaret.transform.DOScaleX(-1f, UIAnimationManager.speedFast);
		});
	}

	private void DeActivate()
	{
		SoundManager.PlayEventSound(SoundEvent.DrawerSlide);
		rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isOpen = false;
		}).OnComplete(delegate
		{
			uiImageCaret.transform.DOScaleX(1f, UIAnimationManager.speedFast);
		});
	}

	public void Init()
	{
		uiSliderJumpHeight.value = _character.characterJumpHeight;
		uiSliderInertia.value = _character.characterInertia;
		uiSliderRunSpeed.value = _character.characterRunSpeed;
		uiSliderWeight.value = Mathf.Abs(_character.characterGravity);
		if (_character.doubleJump)
		{
			uiSliderDoubleJump.value = 1f;
			AdjustDoubleJump(1f);
		}
		else
		{
			uiSliderDoubleJump.value = 0f;
			AdjustDoubleJump(0f);
		}
		AdjustJumpHeight(_character.characterJumpHeight);
		AdjustInertia(_character.characterInertia);
		AdjustRunSpeed(_character.characterRunSpeed);
		AdjustCharacterGravity(Mathf.Abs(_character.characterGravity));
	}

	public void SetCharacter(ref BloxelCharacter character)
	{
		_character = character;
	}

	private void AdjustJumpHeight(float val)
	{
		_jumpHeight = val;
		uiTextJumpHeightVal.text = val.ToString();
		_character.SetCharacterJumpHeight(_jumpHeight);
	}

	private void AdjustInertia(float val)
	{
		_inertia = val;
		uiTextInertiaVal.text = val.ToString();
		_character.SetCharacterInertia(_inertia);
	}

	private void AdjustRunSpeed(float val)
	{
		_runSpeed = val;
		uiTextRunSpeedVal.text = val.ToString();
		_character.SetCharacterRunSpeed(_runSpeed);
	}

	private void AdjustCharacterGravity(float val)
	{
		_characterGravity = val;
		uiTextWeightVal.text = val.ToString();
		_character.SetCharacterGravity(_characterGravity);
	}

	private void AdjustDoubleJump(float val)
	{
		if (val == 0f)
		{
			_doubleJump = false;
		}
		else if (val == 1f)
		{
			_doubleJump = true;
		}
		_character.SetCharacterDoubleJump(_doubleJump);
	}

	private void Save()
	{
		_character.SetCharacterJumpHeight(_jumpHeight, false);
		_character.SetCharacterInertia(_inertia, false);
		_character.SetCharacterRunSpeed(_runSpeed, false);
		_character.SetCharacterGravity(_characterGravity, false);
		_character.SetCharacterDoubleJump(_doubleJump, false);
		_character.Save();
	}

	private void ResetDefaults()
	{
		uiSliderJumpHeight.DOValue(PixelPlayerController.defaultJumpHeight, UIAnimationManager.speedMedium);
		uiSliderInertia.DOValue(PixelPlayerController.defaultInertia, UIAnimationManager.speedMedium);
		uiSliderRunSpeed.DOValue(PixelPlayerController.defaultRunSpeed, UIAnimationManager.speedMedium);
		uiSliderWeight.DOValue(Mathf.Abs(PixelPlayerController.defaultPlayerGravity), UIAnimationManager.speedMedium);
		uiSliderDoubleJump.DOValue(1f, UIAnimationManager.speedMedium);
		AdjustJumpHeight(PixelPlayerController.defaultJumpHeight);
		AdjustInertia(PixelPlayerController.defaultInertia);
		AdjustRunSpeed(PixelPlayerController.defaultRunSpeed);
		AdjustCharacterGravity(Mathf.Abs(PixelPlayerController.defaultPlayerGravity));
		AdjustDoubleJump(1f);
	}
}
