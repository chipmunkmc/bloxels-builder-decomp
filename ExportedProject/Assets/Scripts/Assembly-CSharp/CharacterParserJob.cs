using System;
using System.Threading;

public class CharacterParserJob : ThreadedJob
{
	public delegate void HandleParseComplete(BloxelCharacter _character);

	public bool isRunning;

	public string responseFromServer;

	public BloxelCharacter character;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		character = new BloxelCharacter(responseFromServer, DataSource.JSONString);
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(character);
		}
	}
}
