using UnityEngine;

public class CharacterPlacementGrid : MonoBehaviour
{
	public SpriteRenderer spriteRenderer;

	public Color lineColor;

	public Color cellColor;

	public int stepSize = 13;

	private int pixelCount = 183;

	private Color[] colors;

	private Texture2D texture;

	private void Start()
	{
		colors = new Color[pixelCount * pixelCount];
		for (int i = 0; i < colors.Length; i++)
		{
			colors[i] = lineColor;
		}
		texture = new Texture2D(pixelCount, pixelCount, TextureFormat.ARGB32, false);
		texture.filterMode = FilterMode.Point;
		texture.SetPixels(colors);
		Color[] array = new Color[stepSize * stepSize];
		for (int j = 0; j < array.Length; j++)
		{
			array[j] = cellColor;
		}
		for (int k = 0; k < stepSize; k++)
		{
			for (int l = 0; l < stepSize; l++)
			{
				texture.SetPixels(l * (stepSize + 1) + 1, k * (stepSize + 1) + 1, stepSize, stepSize, array);
			}
		}
		texture.Apply();
		float pixelsPerUnit = (float)pixelCount / 171f;
		spriteRenderer.sprite = Sprite.Create(texture, new Rect(0f, 0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), pixelsPerUnit);
	}
}
