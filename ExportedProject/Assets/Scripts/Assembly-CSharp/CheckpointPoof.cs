using System.Collections;
using DG.Tweening;
using UnityEngine;

public class CheckpointPoof : MonoBehaviour
{
	public CanvasGroup uiCanvasGroup;

	public RectTransform rect;

	private void Start()
	{
		rect.anchoredPosition = new Vector2(0f, -400f);
		Poof();
	}

	private void OnDestroy()
	{
		uiCanvasGroup.DOKill();
		rect.DOKill();
	}

	private void Poof()
	{
		StartCoroutine(DelayedRemove());
	}

	private IEnumerator DelayedRemove()
	{
		base.transform.localScale = Vector3.zero;
		base.transform.DOScale(1f, 0.15f).SetEase(Ease.OutBack);
		yield return new WaitForSeconds(1.5f);
		rect.DOAnchorPos(new Vector2(rect.anchoredPosition.x, rect.anchoredPosition.y + 300f), UIAnimationManager.speedSlowAsCrap).OnStart(delegate
		{
			uiCanvasGroup.DOFade(0f, UIAnimationManager.speedSlow);
		}).OnComplete(delegate
		{
			Object.Destroy(base.gameObject);
		});
	}
}
