using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class CloudConnector : MonoBehaviour
{
	private string webServiceUrl = "https://script.google.com/macros/s/AKfycby6mQUSXcjUdD8r-o6TrePxc2wREOEo0qFqjT3cOfMH5bTuOZQ/exec";

	private string spreadsheetId = "1AKaUKhRWlf4L4yUPmzTT7oQu9xEetqSiWUr-YTWdETE";

	private string servicePassword = "sushiAight";

	private float timeOutLimit = 30f;

	public bool usePOST;

	private static CloudConnector _Instance;

	private UnityWebRequest www;

	public static CloudConnector Instance
	{
		get
		{
			return _Instance ?? (_Instance = new GameObject("CloudConnector").AddComponent<CloudConnector>());
		}
	}

	public void CreateRequest(Dictionary<string, string> form)
	{
		form.Add("ssid", spreadsheetId);
		form.Add("pass", servicePassword);
		if (usePOST)
		{
			CloudConnectorCore.UpdateStatus("Establishing Connection at URL " + webServiceUrl);
			www = UnityWebRequest.Post(webServiceUrl, form);
		}
		else
		{
			string text = "?";
			foreach (KeyValuePair<string, string> item in form)
			{
				string text2 = text;
				text = text2 + item.Key + "=" + item.Value + "&";
			}
			CloudConnectorCore.UpdateStatus("Establishing Connection at URL " + webServiceUrl + text);
			www = UnityWebRequest.Get(webServiceUrl + text);
		}
		StartCoroutine(ExecuteRequest(form));
	}

	private IEnumerator ExecuteRequest(Dictionary<string, string> postData)
	{
		www.Send();
		float elapsedTime = 0f;
		while (!www.isDone)
		{
			elapsedTime += Time.deltaTime;
			if (elapsedTime >= timeOutLimit)
			{
				CloudConnectorCore.ProcessResponse("TIME_OUT", elapsedTime);
				break;
			}
			yield return null;
		}
		if (www.isNetworkError)
		{
			CloudConnectorCore.ProcessResponse("CONN_ERRORConnection error after " + elapsedTime + " seconds: " + www.error, elapsedTime);
		}
		else
		{
			CloudConnectorCore.ProcessResponse(www.downloadHandler.text, elapsedTime);
		}
	}
}
