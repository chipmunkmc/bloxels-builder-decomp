using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine.Events;

public static class CloudConnectorCore
{
	public enum QueryType
	{
		createObject = 0,
		createTable = 1,
		getObjects = 2,
		getTable = 3,
		getAllTables = 4,
		updateObjects = 5,
		deleteObjects = 6
	}

	public class CallBackEventRaw : UnityEvent<string>
	{
	}

	public class CallBackEventProcessed : UnityEvent<QueryType, List<string>, List<string>>
	{
	}

	private static bool debugMode = true;

	public const string MSG_OBJ_CREATED_OK = "OBJ_CREATED_OK";

	public const string MSG_TBL_CREATED_OK = "TBL_CREATED_OK";

	public const string MSG_OBJ_DATA = "OBJ_DATA";

	public const string MSG_TBL_DATA = "TBL_DATA";

	public const string MSG_TBLS_DATA = "TBLS_DATA";

	public const string MSG_OBJ_UPDT = "OBJ_UPT_OK";

	public const string MSG_OBJ_DEL = "OBJ_DEL_OK";

	public const string MSG_MISS_PARAM = "MISSING_PARAM";

	public const string MSG_CONN_ERR = "CONN_ERROR";

	public const string MSG_TIME_OUT = "TIME_OUT";

	public const string TYPE_END = "_ENDTYPE\n";

	public const string TYPE_STRT = "TYPE_";

	public const string MSG_BAD_PASS = "PASS_ERROR";

	private static string currentStatus = string.Empty;

	public static CallBackEventRaw rawResponseCallback = new CallBackEventRaw();

	public static CallBackEventProcessed processedResponseCallback = new CallBackEventProcessed();

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map1;

	public static void CreateObject(Dictionary<string, string> fields, string objTypeName, bool runtime = true)
	{
		fields.Add("action", QueryType.createObject.ToString());
		fields.Add("isJson", "false");
		fields.Add("type", objTypeName);
		SendRequest(fields, runtime);
	}

	public static void CreateObject(string jsonObject, string objTypeName, bool runtime = true)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("action", QueryType.createObject.ToString());
		dictionary.Add("isJson", "true");
		dictionary.Add("type", objTypeName);
		dictionary.Add("jsonData", jsonObject);
		SendRequest(dictionary, runtime);
	}

	public static void CreateTable(string[] headers, string tableTypeName, bool runtime = true)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("action", QueryType.createTable.ToString());
		dictionary.Add("type", tableTypeName);
		dictionary.Add("num", headers.Length.ToString());
		for (int i = 0; i < headers.Length; i++)
		{
			dictionary.Add("field" + i, headers[i]);
		}
		SendRequest(dictionary, runtime);
	}

	public static void GetObjectsByField(string objTypeName, string searchFieldName, string searchValue, bool runtime = true)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("action", QueryType.getObjects.ToString());
		dictionary.Add("type", objTypeName);
		dictionary.Add(searchFieldName, searchValue);
		dictionary.Add("search", searchFieldName);
		SendRequest(dictionary, runtime);
	}

	public static void GetTable(string tableTypeName, bool runtime = true)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("action", QueryType.getTable.ToString());
		dictionary.Add("type", tableTypeName);
		SendRequest(dictionary, runtime);
	}

	public static void GetAllTables(bool runtime = true)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("action", QueryType.getAllTables.ToString());
		SendRequest(dictionary, runtime);
	}

	public static void UpdateObjects(string objTypeName, string searchFieldName, string searchValue, string fieldNameToUpdate, string updateValue, bool runtime = true)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("action", QueryType.updateObjects.ToString());
		dictionary.Add("type", objTypeName);
		dictionary.Add("searchField", searchFieldName);
		dictionary.Add("searchValue", searchValue);
		dictionary.Add("updtField", fieldNameToUpdate);
		dictionary.Add("updtValue", updateValue);
		SendRequest(dictionary, runtime);
	}

	public static void DeleteObjects(string objTypeName, string searchFieldName, string searchValue, bool runtime = true)
	{
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("action", QueryType.deleteObjects.ToString());
		dictionary.Add("type", objTypeName);
		dictionary.Add(searchFieldName, searchValue);
		dictionary.Add("search", searchFieldName);
		SendRequest(dictionary, runtime);
	}

	public static void UnpackJson(string response)
	{
		List<string> list = new List<string>();
		List<string> list2 = new List<string>();
		string empty = string.Empty;
		QueryType arg = QueryType.getObjects;
		if (response.StartsWith("OBJ_DATA"))
		{
			empty = response.Substring("OBJ_DATA".Length + 1);
			list.Add(empty.Substring(0, empty.IndexOf("_ENDTYPE\n")));
			list2.Add(empty.Substring(empty.IndexOf("_ENDTYPE\n") + "_ENDTYPE\n".Length));
			arg = QueryType.getObjects;
		}
		if (response.StartsWith("TBL_DATA"))
		{
			empty = response.Substring("TBL_DATA".Length + 1);
			list.Add(empty.Substring(0, empty.IndexOf("_ENDTYPE\n")));
			list2.Add(empty.Substring(empty.IndexOf("_ENDTYPE\n") + "_ENDTYPE\n".Length));
			arg = QueryType.getTable;
		}
		if (response.StartsWith("TBLS_DATA"))
		{
			empty = response.Substring("TBLS_DATA".Length + 1);
			string[] separator = new string[1] { "TYPE_" };
			string[] array = empty.Split(separator, StringSplitOptions.None);
			separator = new string[1] { "_ENDTYPE\n" };
			for (int i = 0; i < array.Length; i++)
			{
				if (!(array[i] == string.Empty))
				{
					string[] array2 = array[i].Split(separator, StringSplitOptions.None);
					list.Add(array2[0]);
					list2.Add(array2[1]);
				}
			}
			arg = QueryType.getAllTables;
		}
		processedResponseCallback.Invoke(arg, list, list2);
	}

	private static void SendRequest(Dictionary<string, string> form, bool runtime = true)
	{
		if (runtime)
		{
			CloudConnector.Instance.CreateRequest(form);
		}
	}

	public static void ProcessResponse(string response, float time)
	{
		bool flag = false;
		if (response.StartsWith("OBJ_DATA"))
		{
			UnpackJson(response);
			response = "OBJ_DATA";
			flag = true;
		}
		if (response.StartsWith("TBL_DATA"))
		{
			UnpackJson(response);
			response = "TBL_DATA";
			flag = true;
		}
		if (response.StartsWith("TBLS_DATA"))
		{
			UnpackJson(response);
			response = "TBLS_DATA";
			flag = true;
		}
		if (response.StartsWith("PASS_ERROR"))
		{
			response = "PASS_ERROR";
		}
		string text = "Undefined connection error.";
		if (response.StartsWith("CONN_ERROR"))
		{
			text = response.Substring("CONN_ERROR".Length);
			response = "CONN_ERROR";
		}
		string text2 = " Time: " + time;
		string empty = string.Empty;
		if (response != null)
		{
			if (_003C_003Ef__switch_0024map1 == null)
			{
				Dictionary<string, int> dictionary = new Dictionary<string, int>(11);
				dictionary.Add("OBJ_CREATED_OK", 0);
				dictionary.Add("TBL_CREATED_OK", 1);
				dictionary.Add("OBJ_DATA", 2);
				dictionary.Add("TBL_DATA", 3);
				dictionary.Add("TBLS_DATA", 4);
				dictionary.Add("OBJ_UPT_OK", 5);
				dictionary.Add("OBJ_DEL_OK", 6);
				dictionary.Add("MISSING_PARAM", 7);
				dictionary.Add("TIME_OUT", 8);
				dictionary.Add("CONN_ERROR", 9);
				dictionary.Add("PASS_ERROR", 10);
				_003C_003Ef__switch_0024map1 = dictionary;
			}
			int value;
			if (_003C_003Ef__switch_0024map1.TryGetValue(response, out value))
			{
				switch (value)
				{
				case 0:
					break;
				case 1:
					goto IL_01cc;
				case 2:
					goto IL_01d7;
				case 3:
					goto IL_01e2;
				case 4:
					goto IL_01ed;
				case 5:
					goto IL_01f8;
				case 6:
					goto IL_0203;
				case 7:
					goto IL_020e;
				case 8:
					goto IL_0219;
				case 9:
					goto IL_0224;
				case 10:
					goto IL_022b;
				default:
					goto IL_0236;
				}
				empty = "Object saved correctly.";
				goto IL_0247;
			}
		}
		goto IL_0236;
		IL_0236:
		empty = "Undefined server response: \n" + response;
		goto IL_0247;
		IL_01cc:
		empty = "Worksheet table created correctly.";
		goto IL_0247;
		IL_0224:
		empty = text;
		goto IL_0247;
		IL_022b:
		empty = "Error: password incorrect.";
		goto IL_0247;
		IL_020e:
		empty = "Parsing Error: Missing parameters.";
		goto IL_0247;
		IL_0219:
		empty = "Operation timed out, connection aborted. Check your internet connection and try again.";
		goto IL_0247;
		IL_01f8:
		empty = "Object updated correctly.";
		goto IL_0247;
		IL_0203:
		empty = "Object deleted from DB.";
		goto IL_0247;
		IL_01e2:
		empty = "Table data received correctly.";
		goto IL_0247;
		IL_01ed:
		empty = "All DB data received correctly.";
		goto IL_0247;
		IL_01d7:
		empty = "Object data received correctly.";
		goto IL_0247;
		IL_0247:
		UpdateStatus(empty + text2);
		if (!flag)
		{
			rawResponseCallback.Invoke(response);
		}
	}

	public static void UpdateStatus(string status)
	{
		currentStatus = status;
		if (!debugMode)
		{
		}
	}
}
