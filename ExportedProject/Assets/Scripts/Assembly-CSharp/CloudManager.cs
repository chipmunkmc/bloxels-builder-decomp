using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class CloudManager : MonoBehaviour
{
	public static CloudManager Instance;

	public static readonly string SyncFilename = "sync";

	public static string SyncFile = string.Empty;

	public static bool SyncPathExists = false;

	public static string VersionString = "v1";

	private float _timeBetweenUpdates = 5f;

	private float _timeOfLastUpdate;

	private float _timeoutInterval = 120f;

	public HashSet<ProjectSyncInfo> queue = new HashSet<ProjectSyncInfo>();

	private static ProjectSyncInfo[] SyncInfoBuffer = new ProjectSyncInfo[128];

	private string[] idsToAdd = new string[16];

	private static bool _initComplete = false;

	public bool shouldUseCloudSync = true;

	private bool _accountDownloadComplete;

	public int savesPerFrame = 1;

	public bool addToQueueOverride;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	private IEnumerator Start()
	{
		while (!SyncPathExists || !AssetManager.initComplete)
		{
			yield return new WaitForEndOfFrame();
		}
		addToQueueOverride = true;
		string[] diskIds = File.ReadAllLines(SyncFile);
		int notBlankOrNull = 0;
		int goodIds = 0;
		for (int i = 0; i < diskIds.Length; i++)
		{
			if (!string.IsNullOrEmpty(diskIds[i]))
			{
				notBlankOrNull++;
				ProjectSyncInfo syncInfoById = GetSyncInfoById(diskIds[i]);
				if (!syncInfoById.Equals(ProjectSyncInfo.Invalid))
				{
					goodIds++;
					EnqueueFileForSync(syncInfoById, false);
				}
			}
		}
		addToQueueOverride = false;
		_initComplete = true;
	}

	public static ProjectSyncInfo GetSyncInfoById(string id)
	{
		BloxelBoard value = null;
		if (AssetManager.instance.boardPool.TryGetValue(id, out value))
		{
			return value.syncInfo;
		}
		BloxelAnimation value2 = null;
		if (AssetManager.instance.animationPool.TryGetValue(id, out value2))
		{
			return value2.syncInfo;
		}
		BloxelCharacter value3 = null;
		if (AssetManager.instance.characterPool.TryGetValue(id, out value3))
		{
			return value3.syncInfo;
		}
		BloxelBrain value4 = null;
		if (AssetManager.instance.brainPool.TryGetValue(id, out value4))
		{
			return value4.syncInfo;
		}
		BloxelMegaBoard value5 = null;
		if (AssetManager.instance.megaBoardPool.TryGetValue(id, out value5))
		{
			return value5.syncInfo;
		}
		BloxelLevel value6 = null;
		if (AssetManager.instance.levelPool.TryGetValue(id, out value6))
		{
			return value6.syncInfo;
		}
		BloxelGame value7 = null;
		if (AssetManager.instance.gamePool.TryGetValue(id, out value7))
		{
			return value7.syncInfo;
		}
		return ProjectSyncInfo.Invalid;
	}

	public void SetupSyncFile()
	{
		SyncFile = AssetManager.baseStoragePath + SyncFilename;
		if (!Directory.Exists(AssetManager.baseStoragePath))
		{
			Directory.CreateDirectory(AssetManager.baseStoragePath);
		}
		if (!File.Exists(SyncFile))
		{
			File.WriteAllText(SyncFile, string.Empty);
		}
		SyncPathExists = true;
	}

	public void SwitchSyncFiles()
	{
		SyncFile = AssetManager.instance.SetBaseStoragePath() + SyncFilename;
		PrefsManager.instance.hasSyncedGuest = (SyncPathExists = File.Exists(SyncFile));
		if (!SyncPathExists)
		{
			if (!Directory.Exists(AssetManager.baseStoragePath))
			{
				Directory.CreateDirectory(AssetManager.baseStoragePath);
			}
			File.WriteAllText(SyncFile, string.Empty);
			SyncPathExists = true;
		}
	}

	public void WriteSyncFile(ProjectSyncInfo[] syncInfo)
	{
		if (SyncPathExists)
		{
			string[] array = new string[syncInfo.Length];
			for (int i = 0; i < syncInfo.Length; i++)
			{
				array[i] = syncInfo[i].guid.ToString();
			}
			File.WriteAllLines(SyncFile, array);
		}
	}

	private bool canSync(ProjectSyncInfo syncInfo)
	{
		return CurrentUser.instance.active && syncInfo.disk != ProjectSyncInfo.Invalid.disk && syncInfo.s3 != ProjectSyncInfo.Invalid.s3 && syncInfo.disk != null && syncInfo.s3 != null && (addToQueueOverride || AppStateManager.instance.currentScene == SceneName.PixelEditor_iPad);
	}

	public void EnqueueFileForSync(ProjectSyncInfo syncInfo, bool saveToDisk = true)
	{
		if (!canSync(syncInfo) || !queue.Add(syncInfo) || !SyncPathExists || !saveToDisk)
		{
			return;
		}
		using (StreamWriter streamWriter = File.AppendText(SyncFile))
		{
			streamWriter.WriteLine(syncInfo.guid.ToString());
		}
	}

	public void RemoveFileFromQueue(ProjectSyncInfo syncInfo)
	{
		queue.Remove(syncInfo);
	}

	private void resetQueueOnDisk()
	{
		if (SyncPathExists)
		{
			Array.Resize(ref idsToAdd, queue.Count);
			HashSet<ProjectSyncInfo>.Enumerator enumerator = queue.GetEnumerator();
			int num = 0;
			while (enumerator.MoveNext())
			{
				idsToAdd[num] = enumerator.Current.guid.ToString();
				num++;
			}
			File.WriteAllLines(SyncFile, idsToAdd);
		}
	}

	private bool netConnected()
	{
		return Application.internetReachability == NetworkReachability.ReachableViaLocalAreaNetwork || (Application.internetReachability == NetworkReachability.ReachableViaCarrierDataNetwork && PrefsManager.instance.useCellualarData);
	}

	public IEnumerator Sync()
	{
		SaveManager.Instance.isProcessing = true;
		if (CurrentUser.instance == null || !CurrentUser.instance.active)
		{
			SaveManager.Instance.currentMethod = SaveManager.Method.Save;
			SaveManager.Instance.isProcessing = false;
			UIOverlayCanvas.instance.SetSyncState(UIOverlayCanvas.SyncState.Borked);
			yield break;
		}
		if (!shouldUseCloudSync)
		{
			SaveManager.Instance.currentMethod = SaveManager.Method.Save;
			SaveManager.Instance.isProcessing = false;
			UIOverlayCanvas.instance.SetSyncState(UIOverlayCanvas.SyncState.Borked);
			yield break;
		}
		if (!netConnected())
		{
			SaveManager.Instance.currentMethod = SaveManager.Method.Save;
			SaveManager.Instance.isProcessing = false;
			UIOverlayCanvas.instance.SetSyncState(UIOverlayCanvas.SyncState.Borked);
			yield break;
		}
		if (queue.Count == 0)
		{
			SaveManager.Instance.currentMethod = SaveManager.Method.Save;
			SaveManager.Instance.isProcessing = false;
			UIOverlayCanvas.instance.SetSyncState(UIOverlayCanvas.SyncState.Waiting);
			yield break;
		}
		UIOverlayCanvas.instance.SetSyncState(UIOverlayCanvas.SyncState.Syncing);
		int count = queue.Count;
		int numSyncAttemptsComplete = 0;
		float breakoutTime = Time.time + _timeoutInterval;
		if (SyncInfoBuffer.Length < queue.Count)
		{
			Array.Resize(ref SyncInfoBuffer, queue.Count);
		}
		queue.CopyTo(SyncInfoBuffer);
		for (int i = 0; i < count; i++)
		{
			ProjectSyncInfo info = SyncInfoBuffer[i];
			yield return StartCoroutine(S3Interface.instance.Upload(info, delegate(bool didSave, ProjectSyncInfo infoReturn)
			{
				if (didSave)
				{
					RemoveFileFromQueue(infoReturn);
				}
				numSyncAttemptsComplete++;
			}));
			if (i % savesPerFrame == 0)
			{
				yield return new WaitForEndOfFrame();
			}
		}
		while (numSyncAttemptsComplete < count && Time.time < breakoutTime)
		{
			yield return new WaitForEndOfFrame();
		}
		resetQueueOnDisk();
		if (Time.time >= breakoutTime)
		{
			UnityEngine.Debug.LogError("Timed out on cloud sync. Switching back to local save mode.");
		}
		SaveManager.Instance.isProcessing = false;
		SaveManager.Instance.currentMethod = SaveManager.Method.Save;
		SaveManager.Instance.syncRoutine = null;
		UIOverlayCanvas.instance.SetSyncState(UIOverlayCanvas.SyncState.Waiting);
	}

	public IEnumerator GetCloudZip(Action<byte[]> callback)
	{
		string url = S3Interface.S3UtilityEndpoint + "users/" + CurrentUser.instance.account.serverID;
		using (WWW www = new WWW(url))
		{
			yield return www;
			if (www.error == null)
			{
				callback(www.bytes);
			}
			else
			{
				callback(null);
			}
		}
	}

	public bool CanProgress()
	{
		return _accountDownloadComplete;
	}
}
