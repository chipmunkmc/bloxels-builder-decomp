using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class CoinPoofToTarget : MonoBehaviour
{
	public Image coin;

	public Text uiText;

	public Outline uiOutline;

	public int value;

	public Vector2 targetLocation;

	public RectTransform rect;

	private void Awake()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
		uiOutline = uiText.GetComponent<Outline>();
	}

	private void Start()
	{
		uiText.text = value.ToString("N0");
		rect.DOAnchorPos(targetLocation, UIAnimationManager.speedMedium).OnStart(delegate
		{
		}).OnComplete(delegate
		{
			coin.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				uiText.DOFade(0f, UIAnimationManager.speedMedium);
				uiOutline.DOFade(0f, UIAnimationManager.speedMedium);
			}).OnComplete(delegate
			{
				Object.Destroy(base.gameObject);
			});
		});
	}
}
