using System;
using OpenCVForUnity;

public class ColorCorrection
{
	private static double[] pixels;

	private static Random randomNumberGenerator = new Random();

	private static double[] currentPixel = new double[3];

	private static double[] max = new double[3];

	private static double p1 = 0.0;

	private static double p2 = 0.0;

	private static Mat source;

	private static Scalar EstimatedIlluminantColor = new Scalar(0.0, 0.0, 0.0);

	public static int IlluminantTemperature;

	private static Mat converted;

	private static Scalar PerformIlluminationEstimation(Mat img, int n, int m)
	{
		if (source == null || source.total() != img.total())
		{
			source = new Mat(img.size(), CvType.CV_64FC3);
		}
		img.convertTo(source, CvType.CV_64FC3);
		if (pixels == null || pixels.Length != img.total() * img.channels())
		{
			pixels = new double[img.total() * img.channels()];
		}
		Utils.copyFromMat(source, pixels);
		int num = source.rows();
		int num2 = source.cols();
		Array.Clear(EstimatedIlluminantColor.val, 0, EstimatedIlluminantColor.val.Length);
		p1 = 0.0;
		p2 = 0.0;
		for (int i = 0; i < m; i++)
		{
			Array.Clear(max, 0, 3);
			for (int j = 0; j < n; j++)
			{
				p1 = randomNumberGenerator.NextDouble();
				p2 = randomNumberGenerator.NextDouble();
				int num3 = (int)((double)(num - 1) * p1);
				int num4 = (int)((double)(num2 - 1) * p2);
				int num5 = num3 * num2 + num4;
				num5 *= 3;
				currentPixel[0] = pixels[num5];
				currentPixel[1] = pixels[num5 + 1];
				currentPixel[2] = pixels[num5 + 2];
				for (int k = 0; k < 3; k++)
				{
					if (max[k] < currentPixel[k])
					{
						max[k] = currentPixel[k];
					}
				}
			}
			for (int l = 0; l < 3; l++)
			{
				EstimatedIlluminantColor.val[l] += max[l];
			}
		}
		double d = EstimatedIlluminantColor.val[0] * EstimatedIlluminantColor.val[0] + EstimatedIlluminantColor.val[1] * EstimatedIlluminantColor.val[1] + EstimatedIlluminantColor.val[2] * EstimatedIlluminantColor.val[2] / 3.0;
		d = Math.Sqrt(d) / 2.0;
		for (int num6 = 0; num6 < 3; num6++)
		{
			EstimatedIlluminantColor.val[num6] /= d;
		}
		return EstimatedIlluminantColor;
	}

	private static void RemoveColorCast(Mat input, Mat destination, Scalar illuminationEstimation)
	{
		int rows = input.rows();
		int cols = input.cols();
		if (converted == null || converted.total() != input.total())
		{
			converted = new Mat(rows, cols, CvType.CV_64FC3);
		}
		input.convertTo(converted, CvType.CV_64FC3);
		Core.divide(input, illuminationEstimation, destination);
		destination.convertTo(destination, CvType.CV_8UC3);
	}

	public static Mat GetColorCorrectedMat(Mat input, int randomSampleSize, int numberOfRandomSamples)
	{
		Scalar illuminationEstimation = PerformIlluminationEstimation(input, randomSampleSize, numberOfRandomSamples);
		Mat mat = new Mat(input.size(), CvType.CV_64FC3);
		RemoveColorCast(input, mat, illuminationEstimation);
		return mat;
	}
}
