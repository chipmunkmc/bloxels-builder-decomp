using System;
using System.Collections.Generic;
using UnityEngine;

public class ColorPalette : MonoBehaviour
{
	public static ColorPalette instance;

	public Dictionary<BlockColor, Color> paletteChoicesForBlockColors;

	public Dictionary<Color, BlockColor> blockColorsForPaletteChoices;

	public Dictionary<Color, GridLocation> paletteTextureCoordinates;

	public GridLocation[] textureCoordinatesForBlockColors;

	public int tilePixelSize;

	public static float tileSize;

	public static int numberOfBlockColors;

	public Texture2D highResPaletteTexture;

	public Texture2D paletteColorReference;

	public Texture2D complimentaryColorReference;

	public Material defaultTileMaterial;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			if (highResPaletteTexture.width != highResPaletteTexture.height)
			{
				return;
			}
			tileSize = (float)tilePixelSize / (float)highResPaletteTexture.width;
			numberOfBlockColors = Enum.GetValues(typeof(BlockColor)).Length;
			paletteChoicesForBlockColors = new Dictionary<BlockColor, Color>(numberOfBlockColors);
			blockColorsForPaletteChoices = new Dictionary<Color, BlockColor>(numberOfBlockColors);
			paletteTextureCoordinates = new Dictionary<Color, GridLocation>(highResPaletteTexture.width * highResPaletteTexture.height);
			textureCoordinatesForBlockColors = new GridLocation[numberOfBlockColors];
			for (int i = 0; i < 8; i++)
			{
				for (int j = 0; j < 8; j++)
				{
					paletteTextureCoordinates[paletteColorReference.GetPixel(j, i)] = new GridLocation(j, i);
				}
			}
			int num = 0;
			{
				foreach (BlockColor value in Enum.GetValues(typeof(BlockColor)))
				{
					if (value != BlockColor.Blank)
					{
						paletteChoicesForBlockColors[value] = highResPaletteTexture.GetPixel((int)value, num);
						blockColorsForPaletteChoices[highResPaletteTexture.GetPixel((int)value, num)] = value;
						textureCoordinatesForBlockColors[(uint)value] = paletteTextureCoordinates[highResPaletteTexture.GetPixel((int)value, num)];
						num++;
					}
				}
				return;
			}
		}
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void SetPaletteTexCoordsForBoard(BloxelBoard board)
	{
		for (int i = 0; i < numberOfBlockColors; i++)
		{
			BlockColor blockColor = BlockColor.Blank;
			switch (i)
			{
			case 0:
				blockColor = BlockColor.Red;
				break;
			case 1:
				blockColor = BlockColor.Blue;
				break;
			case 2:
				blockColor = BlockColor.Green;
				break;
			case 3:
				blockColor = BlockColor.Yellow;
				break;
			case 4:
				blockColor = BlockColor.Orange;
				break;
			case 5:
				blockColor = BlockColor.Pink;
				break;
			case 6:
				blockColor = BlockColor.Purple;
				break;
			case 7:
				blockColor = BlockColor.White;
				break;
			default:
				blockColor = BlockColor.Blank;
				break;
			}
			if (blockColor == BlockColor.Blank)
			{
				continue;
			}
			Color key = board.toolPaletteChoices[i];
			if (!paletteTextureCoordinates.ContainsKey(key))
			{
				Color key2 = new Color(0f, 0f, 0f, 0f);
				float num = 0f;
				float num2 = float.PositiveInfinity;
				foreach (Color key3 in instance.paletteTextureCoordinates.Keys)
				{
					num = Mathf.Abs(key3.r - key.r) + Mathf.Abs(key3.g - key.g) + Mathf.Abs(key3.b - key.b);
					if (num < num2)
					{
						key2 = key3;
						num2 = num;
					}
				}
				board.paletteChoiceTexCoordArray[(uint)blockColor] = paletteTextureCoordinates[key2];
			}
			else
			{
				board.paletteChoiceTexCoordArray[(uint)blockColor] = paletteTextureCoordinates[key];
			}
		}
	}
}
