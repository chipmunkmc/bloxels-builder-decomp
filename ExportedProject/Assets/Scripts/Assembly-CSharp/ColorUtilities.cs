using System.Collections.Generic;
using UnityEngine;

public class ColorUtilities
{
	public static Color blankUnityColor = new Color(1f, 1f, 1f, 0f);

	public static Dictionary<BlockColor, Color> blockRGB = new Dictionary<BlockColor, Color>(new Dictionary<BlockColor, Color>
	{
		{
			BlockColor.Red,
			PaletteColorToUnityColor(PaletteColor.RGBA_248_79_94_255)
		},
		{
			BlockColor.Blue,
			PaletteColorToUnityColor(PaletteColor.RGBA_98_177_231_255)
		},
		{
			BlockColor.Purple,
			PaletteColorToUnityColor(PaletteColor.RGBA_120_88_176_255)
		},
		{
			BlockColor.Yellow,
			PaletteColorToUnityColor(PaletteColor.RGBA_252_236_39_255)
		},
		{
			BlockColor.Orange,
			PaletteColorToUnityColor(PaletteColor.RGBA_249_137_58_255)
		},
		{
			BlockColor.Green,
			PaletteColorToUnityColor(PaletteColor.RGBA_45_208_59_255)
		},
		{
			BlockColor.Pink,
			PaletteColorToUnityColor(PaletteColor.RGBA_237_57_189_255)
		},
		{
			BlockColor.White,
			PaletteColorToUnityColor(PaletteColor.RGBA_255_255_255_255)
		},
		{
			BlockColor.Blank,
			blankUnityColor
		}
	}, default(BlockColorComparer));

	public static Dictionary<BlockColor, Color32> blockRGB32 = new Dictionary<BlockColor, Color32>(new Dictionary<BlockColor, Color32>
	{
		{
			BlockColor.Red,
			PaletteColorToUnityColor(PaletteColor.RGBA_248_79_94_255)
		},
		{
			BlockColor.Blue,
			PaletteColorToUnityColor(PaletteColor.RGBA_98_177_231_255)
		},
		{
			BlockColor.Purple,
			PaletteColorToUnityColor(PaletteColor.RGBA_120_88_176_255)
		},
		{
			BlockColor.Yellow,
			PaletteColorToUnityColor(PaletteColor.RGBA_252_236_39_255)
		},
		{
			BlockColor.Orange,
			PaletteColorToUnityColor(PaletteColor.RGBA_249_137_58_255)
		},
		{
			BlockColor.Green,
			PaletteColorToUnityColor(PaletteColor.RGBA_45_208_59_255)
		},
		{
			BlockColor.Pink,
			PaletteColorToUnityColor(PaletteColor.RGBA_237_57_189_255)
		},
		{
			BlockColor.White,
			PaletteColorToUnityColor(PaletteColor.RGBA_255_255_255_255)
		},
		{
			BlockColor.Blank,
			blankUnityColor
		}
	}, default(BlockColorComparer));

	public static Color[] blockColorArrayRGB = new Color[9]
	{
		new Color(0.972549f, 0.30980393f, 0.36862746f, 1f),
		new Color(83f / 85f, 0.5372549f, 0.22745098f, 1f),
		new Color(84f / 85f, 0.9254902f, 13f / 85f, 1f),
		new Color(0.1764706f, 0.8156863f, 0.23137255f, 1f),
		new Color(0.38431373f, 59f / 85f, 77f / 85f, 1f),
		new Color(0.47058824f, 0.34509805f, 0.6901961f, 1f),
		new Color(79f / 85f, 19f / 85f, 63f / 85f, 1f),
		new Color(1f, 1f, 1f, 1f),
		new Color(1f, 1f, 1f, 0f)
	};

	public static Dictionary<float, BlockColor> hueToBlockColor = new Dictionary<float, BlockColor>
	{
		{
			175f,
			BlockColor.Red
		},
		{
			8f,
			BlockColor.Orange
		},
		{
			25.2f,
			BlockColor.Yellow
		},
		{
			66.3f,
			BlockColor.Green
		},
		{
			101.7f,
			BlockColor.Blue
		},
		{
			128.8f,
			BlockColor.Purple
		},
		{
			151.3f,
			BlockColor.Pink
		}
	};

	public static Dictionary<float, BlockColor> hueToBlockColorEdge = new Dictionary<float, BlockColor>
	{
		{
			180f,
			BlockColor.Red
		},
		{
			6f,
			BlockColor.Orange
		},
		{
			20f,
			BlockColor.Yellow
		},
		{
			52f,
			BlockColor.Green
		},
		{
			98f,
			BlockColor.Blue
		},
		{
			130f,
			BlockColor.Purple
		},
		{
			165f,
			BlockColor.Pink
		}
	};

	public static Dictionary<int, Color> colors = new Dictionary<int, Color>
	{
		{
			0,
			new Color(1f, 0f, 0f)
		},
		{
			1,
			new Color(1f, 0.61f, 0f)
		},
		{
			2,
			new Color(1f, 1f, 0f)
		},
		{
			3,
			new Color(0f, 1f, 0f)
		},
		{
			4,
			new Color(0f, 0f, 1f)
		},
		{
			5,
			new Color(0.59f, 0f, 1f)
		},
		{
			6,
			new Color(1f, 0f, 0.78f)
		}
	};

	public static Dictionary<BlockColor, float> blockHue = new Dictionary<BlockColor, float>
	{
		{
			BlockColor.Red,
			175f
		},
		{
			BlockColor.Orange,
			8f
		},
		{
			BlockColor.Yellow,
			25.2f
		},
		{
			BlockColor.Green,
			66.3f
		},
		{
			BlockColor.Blue,
			101.7f
		},
		{
			BlockColor.Purple,
			128.8f
		},
		{
			BlockColor.Pink,
			151.3f
		}
	};

	public static Dictionary<BlockColor, float> blockHueEdge = new Dictionary<BlockColor, float>
	{
		{
			BlockColor.Red,
			180f
		},
		{
			BlockColor.Orange,
			6f
		},
		{
			BlockColor.Yellow,
			20f
		},
		{
			BlockColor.Green,
			52f
		},
		{
			BlockColor.Blue,
			98f
		},
		{
			BlockColor.Purple,
			130f
		},
		{
			BlockColor.Pink,
			165f
		}
	};

	public static Dictionary<BlockColor, float> blockSaturation = new Dictionary<BlockColor, float>
	{
		{
			BlockColor.Red,
			191f
		},
		{
			BlockColor.Orange,
			199f
		},
		{
			BlockColor.Yellow,
			206.5f
		},
		{
			BlockColor.Green,
			160.6f
		},
		{
			BlockColor.Blue,
			147.9f
		},
		{
			BlockColor.Purple,
			114.7f
		},
		{
			BlockColor.Pink,
			155.5f
		}
	};

	public static float[] H = new float[7] { 175f, 101.7f, 66.3f, 128.8f, 8f, 25.2f, 151.3f };

	public static float[] edgeH = new float[7] { 180f, 98f, 52f, 130f, 6f, 20f, 165f };

	public static Dictionary<BlockColor, PaletteColor> blockColorToPaletteColor = new Dictionary<BlockColor, PaletteColor>
	{
		{
			BlockColor.Red,
			PaletteColor.RGBA_248_79_94_255
		},
		{
			BlockColor.Blue,
			PaletteColor.RGBA_98_177_231_255
		},
		{
			BlockColor.Purple,
			PaletteColor.RGBA_120_88_176_255
		},
		{
			BlockColor.Yellow,
			PaletteColor.RGBA_252_236_39_255
		},
		{
			BlockColor.Orange,
			PaletteColor.RGBA_249_137_58_255
		},
		{
			BlockColor.Green,
			PaletteColor.RGBA_45_208_59_255
		},
		{
			BlockColor.Pink,
			PaletteColor.RGBA_237_57_189_255
		},
		{
			BlockColor.White,
			PaletteColor.RGBA_255_255_255_255
		}
	};

	public static Color PaletteColorToUnityColor(PaletteColor c)
	{
		string[] array = c.ToString().Split('_');
		float r = float.Parse(array[1]) / 255f;
		float g = float.Parse(array[2]) / 255f;
		float b = float.Parse(array[3]) / 255f;
		float a = float.Parse(array[4]) / 255f;
		return new Color(r, g, b, a);
	}

	public static Color GetUnityColorFromBlockColor(BlockColor color)
	{
		Color value = default(Color);
		blockRGB.TryGetValue(color, out value);
		return value;
	}

	public static bool BlockColorsMatch(BlockColor[,] array1, BlockColor[,] array2)
	{
		if (array1.Length != array2.Length)
		{
			return false;
		}
		for (int i = 0; i < array1.GetLength(0); i++)
		{
			for (int j = 0; j < array1.GetLength(1); j++)
			{
				if (array1[i, j] != array2[i, j])
				{
					return false;
				}
			}
		}
		return true;
	}
}
