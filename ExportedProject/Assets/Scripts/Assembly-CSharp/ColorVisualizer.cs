using DiscoCapture;
using UnityEngine;
using UnityEngine.UI;

public class ColorVisualizer : MonoBehaviour
{
	public RawImage uiRawImage;

	public Texture2D texture;

	public Color[] colors;

	public static Color[] blockColorArrayRGB = new Color[9]
	{
		new Color(0.972549f, 0.30980393f, 0.36862746f, 1f),
		new Color(83f / 85f, 0.5372549f, 0.22745098f, 1f),
		new Color(84f / 85f, 0.9254902f, 13f / 85f, 1f),
		new Color(0.1764706f, 0.8156863f, 0.23137255f, 1f),
		new Color(0.38431373f, 59f / 85f, 77f / 85f, 1f),
		new Color(0.47058824f, 0.34509805f, 0.6901961f, 1f),
		new Color(79f / 85f, 19f / 85f, 63f / 85f, 1f),
		Color.white,
		Color.black
	};

	private void Awake()
	{
		texture = new Texture2D(46, 46, TextureFormat.ARGB32, false);
		texture.filterMode = FilterMode.Point;
		colors = new Color[texture.width * texture.height];
		for (int i = 0; i < colors.Length; i++)
		{
			colors[i] = new Color(0.20784314f, 0.20784314f, 0.20784314f, 1f);
		}
		texture.SetPixels(colors);
		texture.Apply();
		uiRawImage.texture = texture;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.R))
		{
			ColorClassifier.ClassifyAllBlocks(true);
			UpdateTexture2D();
		}
	}

	public void UpdateTexture2D(bool darken = false)
	{
		Color color = default(Color);
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < ColorClassifier.BlockColors.GetLength(0); i++)
		{
			for (int j = 0; j < ColorClassifier.BlockColors.GetLength(1); j++)
			{
				color = blockColorArrayRGB[(uint)ColorClassifier.BlockColors[i, j]];
				if (darken)
				{
					color = Color.Lerp(color, Color.clear, 0.25f);
				}
				num = 4 + i * 3;
				num2 = 4 + j * 3;
				texture.SetPixel(num, num2, color);
				texture.SetPixel(num + 1, num2 + 1, color);
				texture.SetPixel(num, num2 + 1, color);
				texture.SetPixel(num + 1, num2, color);
			}
		}
		texture.Apply();
	}

	public void UpdateBlockInTexture2D(int c, int r)
	{
		Color color = default(Color);
		int num = 0;
		int num2 = 0;
		color = blockColorArrayRGB[(uint)ColorClassifier.BlockColors[c, r]];
		num = 4 + c * 3;
		num2 = 4 + r * 3;
		texture.SetPixel(num, num2, color);
		texture.SetPixel(num + 1, num2 + 1, color);
		texture.SetPixel(num, num2 + 1, color);
		texture.SetPixel(num + 1, num2, color);
		texture.Apply();
	}
}
