using DG.Tweening;
using UnityEngine;

public sealed class ConfigIndicator : PoolableComponent
{
	public float bounceAdjustment = 3f;

	private static Vector3 positionOffset = new Vector3(6f, 10f, -10f);

	public GridLocation locationInLevel;

	private Tweener bounceTween;

	public override void EarlyAwake()
	{
		selfTransform = base.transform;
	}

	public override void PrepareSpawn()
	{
		base.gameObject.SetActive(true);
		spawned = true;
	}

	public override void PrepareDespawn()
	{
		if (bounceTween != null)
		{
			bounceTween.Kill();
			bounceTween = null;
		}
		base.gameObject.SetActive(false);
		selfTransform.SetParent(ConfigIndicatorPool.instance.selfTransform);
		spawned = false;
	}

	public override void Despawn()
	{
		PrepareDespawn();
		ConfigIndicatorPool.instance.pool.Push(this);
	}

	public void Init(GridLocation locationInLevel, Vector3 tilePositionInWorld)
	{
		this.locationInLevel = locationInLevel;
		selfTransform.localPosition = tilePositionInWorld + positionOffset;
	}

	public void MoveToLocation(GridLocation newLocationInLevel)
	{
		ConfigIndicatorContainer.ActiveIndicators[locationInLevel.c, locationInLevel.r] = null;
		selfTransform.localPosition = new Vector3(((float)GameBuilderCanvas.instance.currentBloxelLevel.location.c * 13f + (float)newLocationInLevel.c) * 13f, ((float)GameBuilderCanvas.instance.currentBloxelLevel.location.r * 13f + (float)newLocationInLevel.r) * 13f, 0f);
		selfTransform.localPosition += positionOffset;
		locationInLevel = newLocationInLevel;
		ConfigIndicatorContainer.ActiveIndicators[newLocationInLevel.c, newLocationInLevel.r] = this;
		Bounce();
	}

	public void Bounce()
	{
		if (bounceTween != null)
		{
			bounceTween.Kill();
		}
		bounceTween = selfTransform.DOLocalMoveY(selfTransform.localPosition.y + bounceAdjustment, UIAnimationManager.speedMedium).SetLoops(-1, LoopType.Yoyo);
	}
}
