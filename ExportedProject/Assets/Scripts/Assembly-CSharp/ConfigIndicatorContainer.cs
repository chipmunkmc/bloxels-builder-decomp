using UnityEngine;

public class ConfigIndicatorContainer : MonoBehaviour
{
	public Transform selfTransform;

	public static ConfigIndicator[,] ActiveIndicators = new ConfigIndicator[13, 13];

	private void Awake()
	{
		selfTransform = base.transform;
		GameBuilderCanvas.instance.OnGameBuilderModeChange += HandleGameBuilderModeChanged;
		PixelEditorController.instance.OnEditorModeChange += HandleEditorModeChanged;
	}

	private void OnDestroy()
	{
		if (GameBuilderCanvas.instance != null)
		{
			GameBuilderCanvas.instance.OnGameBuilderModeChange -= HandleGameBuilderModeChanged;
		}
		if (PixelEditorController.instance != null)
		{
			PixelEditorController.instance.OnEditorModeChange -= HandleEditorModeChanged;
		}
	}

	public void HandleEditorModeChanged(CanvasMode mode)
	{
		ResetConfigIndicators();
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor && GameBuilderCanvas.instance.currentMode == GameBuilderMode.ConfigBlocks)
		{
			GameBuilderCanvas.instance.SetUpConfigIndicators(GameBuilderCanvas.instance.currentBloxelLevel.location);
		}
	}

	public void HandleGameBuilderModeChanged(GameBuilderMode mode)
	{
		ResetConfigIndicators();
		if (mode == GameBuilderMode.ConfigBlocks)
		{
			GameBuilderCanvas.instance.SetUpConfigIndicators(GameBuilderCanvas.instance.currentBloxelLevel.location);
		}
	}

	public void MoveIndicatorsForNewLevel(BloxelLevel level)
	{
		ResetConfigIndicators();
		GameBuilderCanvas.instance.SetUpConfigIndicators(level.location);
	}

	public void ResetConfigIndicators()
	{
		if (selfTransform.childCount == 0)
		{
			return;
		}
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				ConfigIndicator configIndicator = ActiveIndicators[j, i];
				if (configIndicator != null)
				{
					configIndicator.Despawn();
				}
				ActiveIndicators[j, i] = null;
			}
		}
	}
}
