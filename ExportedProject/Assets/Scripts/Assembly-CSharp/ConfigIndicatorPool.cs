using System.Collections.Generic;
using UnityEngine;

public class ConfigIndicatorPool : MonoBehaviour
{
	public static ConfigIndicatorPool instance;

	public Transform selfTransform;

	public int poolSize;

	public GameObject prefab;

	private static bool _InitComplete;

	public Stack<ConfigIndicator> pool { get; private set; }

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			instance = this;
			_InitComplete = true;
			Object.DontDestroyOnLoad(base.gameObject);
			selfTransform = base.transform;
			Initialize();
		}
	}

	private void Initialize()
	{
		pool = new Stack<ConfigIndicator>(poolSize);
		AllocatePoolObjects();
	}

	private void AllocatePoolObjects()
	{
		for (int i = 0; i < poolSize; i++)
		{
			GameObject gameObject = Object.Instantiate(prefab);
			gameObject.name = prefab.name;
			ConfigIndicator component = gameObject.GetComponent<ConfigIndicator>();
			component.EarlyAwake();
			component.selfTransform.SetParent(selfTransform);
			gameObject.SetActive(false);
			pool.Push(component);
		}
	}

	public ConfigIndicator Spawn()
	{
		ConfigIndicator configIndicator = pool.Pop();
		configIndicator.PrepareSpawn();
		return configIndicator;
	}

	public void PrepareForSceneChange()
	{
		Stack<ConfigIndicator>.Enumerator enumerator = pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.selfTransform.SetParent(selfTransform);
			Object.DontDestroyOnLoad(enumerator.Current.gameObject);
		}
	}
}
