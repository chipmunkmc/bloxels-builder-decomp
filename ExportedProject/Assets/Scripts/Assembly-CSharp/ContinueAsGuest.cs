using DG.Tweening;
using UnityEngine;

public class ContinueAsGuest : SequentialMenu
{
	public UIPopupAccount controller;

	public UIMenuPanel uiMenuPanel;

	private new void Awake()
	{
		base.Awake();
		uiMenuPanel = GetComponent<UIMenuPanel>();
	}

	private void Start()
	{
		uiMenuPanel.OverrideDismiss();
		uiButtonNext.OnClick += Next;
		uiMenuPanel.uiButtonDismiss.OnClick += BackToWelcome;
		if (controller == null)
		{
			controller = GetComponentInParent<UIPopupAccount>();
		}
	}

	private new void OnDestroy()
	{
		uiButtonNext.OnClick -= Next;
		uiMenuPanel.uiButtonDismiss.OnClick -= BackToWelcome;
	}

	public new void Init()
	{
		base.gameObject.SetActive(true);
		base.transform.localScale = Vector3.zero;
	}

	public void BackToWelcome()
	{
		Hide().OnComplete(delegate
		{
			controller.registerController.SwitchStep(RegistrationController.Step.Welcome);
		});
	}

	public override void Next()
	{
		controller.GuestLogin();
	}
}
