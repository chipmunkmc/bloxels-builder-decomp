using UnityEngine;

public class CoverBoard
{
	public CoverStyle style;

	public Texture2D texture;

	public Color[] colors;

	public CoverBoard(Color emptyColor)
	{
		style = new CoverStyle(emptyColor);
		colors = new Color[style.size * style.size];
		for (int i = 0; i < colors.Length; i++)
		{
			colors[i] = style.emptyColor;
		}
		texture = new Texture2D(style.size, style.size, TextureFormat.ARGB32, false);
		texture.filterMode = FilterMode.Point;
		texture.wrapMode = TextureWrapMode.Clamp;
		texture.SetPixels(colors);
	}

	public CoverBoard(CoverStyle coverStyle)
	{
		style = coverStyle;
		colors = new Color[style.size * style.size];
		for (int i = 0; i < colors.Length; i++)
		{
			colors[i] = style.emptyColor;
		}
		texture = new Texture2D(style.size, style.size, TextureFormat.ARGB32, false);
		texture.filterMode = FilterMode.Point;
		texture.wrapMode = TextureWrapMode.Clamp;
		texture.SetPixels(colors);
	}
}
