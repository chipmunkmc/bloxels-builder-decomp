using UnityEngine;

public class CoverSprite
{
	public bool inUse;

	public int spriteIndex;

	public Sprite sprite;

	public CoverSprite(int spriteIndex, Sprite sprite)
	{
		inUse = false;
		this.spriteIndex = spriteIndex;
		this.sprite = sprite;
	}
}
