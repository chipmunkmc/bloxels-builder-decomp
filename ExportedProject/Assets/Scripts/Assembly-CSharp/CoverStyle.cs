using UnityEngine;

public struct CoverStyle
{
	public Color emptyColor;

	public int margin;

	public int blockSize;

	public int blockGutter;

	public bool blankIsBlack;

	public int size;

	public int step;

	public CoverStyle(Color _emptyColor, int _boardMarginSize = 4, int _blockSize = 2, int _blockGutter = 0, bool _blankIsBlack = false)
	{
		emptyColor = _emptyColor;
		margin = _boardMarginSize;
		blockSize = _blockSize;
		blockGutter = _blockGutter;
		blankIsBlack = _blankIsBlack;
		size = margin * 2 + blockSize * 13 + blockGutter * 12;
		step = blockSize + blockGutter;
	}

	public override string ToString()
	{
		return string.Concat("Color: ", emptyColor, ", Margin: ", margin, ", BlockSize: ", blockSize, ", Gutter: ", blockGutter, ", BlankBlack? ", blankIsBlack);
	}
}
