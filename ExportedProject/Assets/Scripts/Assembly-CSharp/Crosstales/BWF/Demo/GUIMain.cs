using System;
using System.Collections.Generic;
using Crosstales.BWF.Filter;
using Crosstales.BWF.Model;
using UnityEngine;
using UnityEngine.UI;

namespace Crosstales.BWF.Demo
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_demo_1_1_g_u_i_main.html")]
	public class GUIMain : MonoBehaviour
	{
		public bool AutoTest = true;

		public bool AutoReplace;

		public bool Fuzzy;

		public float IntervalCheck = 0.5f;

		public float IntervalReplace = 0.5f;

		public InputField Text;

		public Text OutputText;

		public Text BadWordList;

		public Text BadWordCounter;

		public Text Version;

		public Toggle TestEnabled;

		public Toggle ReplaceEnabled;

		public Toggle Badword;

		public Toggle Domain;

		public Toggle Capitalization;

		public Toggle Punctuation;

		public InputField BadwordReplaceChars;

		public InputField DomainReplaceChars;

		public InputField CapsTrigger;

		public InputField PuncTrigger;

		public Toggle FuzzyEnabled;

		public Image BadWordListImage;

		public Color32 GoodColor = new Color32(0, byte.MaxValue, 0, 192);

		public Color32 BadColor = new Color32(byte.MaxValue, 0, 0, 192);

		public ManagerMask BadwordManager = ManagerMask.BadWord;

		public ManagerMask DomainManager = ManagerMask.Domain;

		public ManagerMask CapsManager = ManagerMask.Capitalization;

		public ManagerMask PuncManager = ManagerMask.Punctuation;

		public List<string> Sources = new List<string>(30);

		private List<string> badWords = new List<string>();

		private float elapsedTimeCheck;

		private float elapsedTimeReplace;

		private BadWordFilter bwf;

		private DomainFilter df;

		private CapitalizationFilter cf;

		private PunctuationFilter pf;

		private bool tested;

		public void Start()
		{
			bwf = (BadWordFilter)BWFManager.Filter();
			df = (DomainFilter)BWFManager.Filter(ManagerMask.Domain);
			cf = (CapitalizationFilter)BWFManager.Filter(ManagerMask.Capitalization);
			pf = (PunctuationFilter)BWFManager.Filter(ManagerMask.Punctuation);
			Version.text = "BWF PRO - 2.8.4";
			if (!AutoTest)
			{
				TestEnabled.isOn = false;
			}
			if (!AutoReplace)
			{
				ReplaceEnabled.isOn = false;
			}
			if (BadwordManager != ManagerMask.BadWord)
			{
				Badword.isOn = false;
			}
			if (DomainManager != ManagerMask.Domain)
			{
				Domain.isOn = false;
			}
			if (CapsManager != ManagerMask.Capitalization)
			{
				Capitalization.isOn = false;
			}
			if (PuncManager != ManagerMask.Punctuation)
			{
				Punctuation.isOn = false;
			}
			bwf.isFuzzy = Fuzzy;
			if (!Fuzzy)
			{
				FuzzyEnabled.isOn = false;
			}
			BadwordReplaceChars.text = bwf.ReplaceCharacters;
			DomainReplaceChars.text = df.ReplaceCharacters;
			CapsTrigger.text = cf.CharacterNumber.ToString();
			PuncTrigger.text = pf.CharacterNumber.ToString();
			BadWordList.text = ((badWords.Count <= 0) ? "Not tested" : string.Empty);
			Text.text = string.Empty;
		}

		public void Update()
		{
			elapsedTimeCheck += Time.deltaTime;
			elapsedTimeReplace += Time.deltaTime;
			if (AutoTest && !AutoReplace && elapsedTimeCheck > IntervalCheck)
			{
				Test();
				elapsedTimeCheck = 0f;
			}
			if (AutoReplace && elapsedTimeReplace > IntervalReplace)
			{
				Replace();
				elapsedTimeReplace = 0f;
			}
			bwf.ReplaceCharacters = BadwordReplaceChars.text;
			df.ReplaceCharacters = DomainReplaceChars.text;
			int result;
			bool flag = int.TryParse(CapsTrigger.text, out result);
			cf.CharacterNumber = ((!flag) ? 2 : ((result <= 2) ? 2 : result));
			CapsTrigger.text = cf.CharacterNumber.ToString();
			flag = int.TryParse(PuncTrigger.text, out result);
			pf.CharacterNumber = ((!flag) ? 2 : ((result <= 2) ? 2 : result));
			PuncTrigger.text = pf.CharacterNumber.ToString();
			if (tested)
			{
				if (badWords.Count > 0)
				{
					BadWordList.text = string.Join(Environment.NewLine, badWords.ToArray());
					BadWordListImage.color = BadColor;
				}
				else
				{
					BadWordList.text = "No bad words found";
					BadWordListImage.color = GoodColor;
				}
			}
			BadWordCounter.text = badWords.Count.ToString();
			OutputText.text = BWFManager.Mark(Text.text, badWords);
		}

		public void TestChanged(bool val)
		{
			AutoTest = val;
		}

		public void ReplaceChanged(bool val)
		{
			AutoReplace = val;
		}

		public void BadwordChanged(bool val)
		{
			BadwordManager = (val ? ManagerMask.BadWord : ManagerMask.None);
		}

		public void DomainChanged(bool val)
		{
			DomainManager = (val ? ManagerMask.Domain : ManagerMask.None);
		}

		public void CapitalizationChanged(bool val)
		{
			CapsManager = (val ? ManagerMask.Capitalization : ManagerMask.None);
		}

		public void PunctuationChanged(bool val)
		{
			PuncManager = (val ? ManagerMask.Punctuation : ManagerMask.None);
		}

		public void FuzzyChanged(bool val)
		{
			bwf.isFuzzy = val;
		}

		public void FullscreenChanged(bool val)
		{
			Screen.fullScreen = val;
		}

		public void Test()
		{
			tested = true;
			if (!string.IsNullOrEmpty(Text.text))
			{
				badWords = BWFManager.GetAll(Text.text, BadwordManager | DomainManager | CapsManager | PuncManager, Sources.ToArray());
			}
		}

		public void Replace()
		{
			tested = true;
			if (!string.IsNullOrEmpty(Text.text))
			{
				Text.text = BWFManager.ReplaceAll(Text.text, BadwordManager | DomainManager | CapsManager | PuncManager, Sources.ToArray());
				badWords.Clear();
			}
		}

		public void OpenAssetURL()
		{
			Application.OpenURL("https://www.assetstore.unity3d.com/#!/list/42213-crosstales?aid=1011lNGT&pubref=BWF PRO");
		}

		public void OpenCTURL()
		{
			Application.OpenURL("https://www.crosstales.com");
		}

		public void Quit()
		{
			if (!Application.isEditor)
			{
				Application.Quit();
			}
		}
	}
}
