using System;
using System.Collections;
using System.Diagnostics;
using Crosstales.BWF.Filter;
using Crosstales.BWF.Model;
using UnityEngine;

namespace Crosstales.BWF.Test
{
	public abstract class BaseTest : MonoBehaviour
	{
		public int Iterations = 50;

		public int TextStartLength = 100;

		public int TextGrowPerIteration;

		public ManagerMask[] Managers;

		public string[] TestSources;

		public string RandomChars = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ.?!-*";

		public char ReplaceChar = '*';

		protected System.Random rd = new System.Random();

		protected Stopwatch stopWatch = new Stopwatch();

		protected int failCounter;

		protected static readonly string badword = "Fuuuccckkk";

		protected static readonly string noBadword = "assume";

		protected static readonly string domain = "goOgle.cOm";

		protected static readonly string email = "stEve76@goOgle.cOm";

		protected static readonly string noDomain = "my.cOmMand";

		protected static readonly string scunthorpe = "scuntHorPe";

		protected static readonly string arabicBadword = "آنتاكلب";

		protected static readonly string globalBadword = "h!+leR";

		protected static readonly string nameBadword = "bAmbi";

		protected static readonly string emoji = "卍";

		protected BadWordFilter bwf;

		protected DomainFilter df;

		protected CapitalizationFilter cf;

		protected PunctuationFilter pf;

		private bool isFirsttime = true;

		public virtual void Update()
		{
			if (isFirsttime)
			{
				StartCoroutine(runTest());
				isFirsttime = false;
			}
		}

		protected virtual IEnumerator runTest()
		{
			while (!BWFManager.isReady)
			{
				yield return null;
			}
			bwf = (BadWordFilter)BWFManager.Filter();
			df = (DomainFilter)BWFManager.Filter(ManagerMask.Domain);
			cf = (CapitalizationFilter)BWFManager.Filter(ManagerMask.Capitalization);
			pf = (PunctuationFilter)BWFManager.Filter(ManagerMask.Punctuation);
			bwf.ReplaceCharacters = new string(ReplaceChar, 1);
			df.ReplaceCharacters = new string(ReplaceChar, 1);
			ManagerMask[] managers = Managers;
			foreach (ManagerMask mask in managers)
			{
				speedTest(mask);
				yield return null;
				sanityTest(mask);
				yield return null;
			}
			if (failCounter <= 0)
			{
			}
		}

		protected virtual string createRandomString(int stringLength)
		{
			char[] array = new char[stringLength];
			for (int i = 0; i < stringLength; i++)
			{
				array[i] = RandomChars[rd.Next(0, RandomChars.Length)];
			}
			return new string(array);
		}

		protected abstract void speedTest(ManagerMask mask);

		protected abstract void sanityTest(ManagerMask mask);
	}
}
