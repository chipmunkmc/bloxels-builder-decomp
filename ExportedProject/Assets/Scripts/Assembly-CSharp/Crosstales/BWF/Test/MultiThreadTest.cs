using System.Collections;
using System.Collections.Generic;
using System.Threading;
using Crosstales.BWF.Manager;
using Crosstales.BWF.Model;
using UnityEngine;

namespace Crosstales.BWF.Test
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_test_1_1_multi_thread_test.html")]
	public class MultiThreadTest : MonoBehaviour
	{
		public string DirtyText;

		private List<string> unwantedWords = new List<string>();

		private void Start()
		{
			StartCoroutine(multiTreaded());
		}

		private IEnumerator multiTreaded()
		{
			while (!BadWordManager.isReady)
			{
				yield return null;
			}
			Thread worker = new Thread((ThreadStart)delegate
			{
				BWFManager.GetAllMT(out unwantedWords, DirtyText, ManagerMask.All);
			});
			worker.Start();
			do
			{
				yield return null;
			}
			while (worker.IsAlive);
		}
	}
}
