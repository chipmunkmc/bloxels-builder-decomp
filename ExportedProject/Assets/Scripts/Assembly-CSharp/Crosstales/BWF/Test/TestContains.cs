using Crosstales.BWF.Model;
using Crosstales.BWF.Util;
using UnityEngine;

namespace Crosstales.BWF.Test
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_test_1_1_test_contains.html")]
	public class TestContains : BaseTest
	{
		protected override void speedTest(ManagerMask mask)
		{
			stopWatch.Reset();
			stopWatch.Start();
			for (int i = 0; i < Iterations; i++)
			{
				BWFManager.Contains(createRandomString(TextStartLength + TextGrowPerIteration * i), mask, TestSources);
			}
			stopWatch.Stop();
		}

		protected override void sanityTest(ManagerMask mask)
		{
			if (!BWFManager.Contains(null, mask))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (!BWFManager.Contains(string.Empty, ManagerMask.All))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (!BWFManager.Contains(BaseTest.scunthorpe, mask, "test"))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (!BWFManager.Contains(BaseTest.scunthorpe, mask, (string[])null))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (!BWFManager.Contains(BaseTest.scunthorpe, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.Contains(BaseTest.badword, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (!BWFManager.Contains(BaseTest.scunthorpe, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.Contains(BaseTest.badword, mask, "english"))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (!BWFManager.Contains(BaseTest.noBadword, mask, "english"))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.Contains(BaseTest.arabicBadword, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.Contains(BaseTest.globalBadword, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.Contains(BaseTest.nameBadword, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.Contains(BaseTest.emoji, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.Contains(BaseTest.domain, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.Contains(BaseTest.email, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (!BWFManager.Contains(BaseTest.noDomain, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization)
			{
				string testString = new string('A', cf.CharacterNumber);
				string testString2 = new string('A', cf.CharacterNumber - 1);
				if (BWFManager.Contains(testString, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (!BWFManager.Contains(testString2, mask))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Punctuation) != ManagerMask.Punctuation)
			{
				return;
			}
			string testString3 = new string('!', pf.CharacterNumber);
			string testString4 = new string('!', pf.CharacterNumber - 1);
			if (BWFManager.Contains(testString3, ManagerMask.All))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (!BWFManager.Contains(testString4, ManagerMask.All))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
		}
	}
}
