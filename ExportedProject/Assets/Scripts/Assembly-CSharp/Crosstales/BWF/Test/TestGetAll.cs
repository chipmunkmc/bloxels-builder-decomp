using Crosstales.BWF.Model;
using Crosstales.BWF.Util;
using UnityEngine;

namespace Crosstales.BWF.Test
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_test_1_1_test_get_all.html")]
	public class TestGetAll : BaseTest
	{
		protected override void speedTest(ManagerMask mask)
		{
			stopWatch.Reset();
			stopWatch.Start();
			for (int i = 0; i < Iterations; i++)
			{
				BWFManager.GetAll(createRandomString(TextStartLength + TextGrowPerIteration * i), mask, TestSources);
			}
			stopWatch.Stop();
		}

		protected override void sanityTest(ManagerMask mask)
		{
			if (BWFManager.GetAll(null, mask).Count == 0)
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (BWFManager.GetAll(string.Empty, mask).Count == 0)
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.GetAll(BaseTest.scunthorpe, mask, "test").Count == 0)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.scunthorpe, mask, (string[])null).Count == 0)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.scunthorpe, mask).Count == 0)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.GetAll(BaseTest.badword, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.scunthorpe, mask).Count == 0)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.badword, mask, "english").Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.noBadword, mask, "english").Count == 0)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.arabicBadword, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.globalBadword, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.nameBadword, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.emoji, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.GetAll(BaseTest.domain, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.email, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(BaseTest.noDomain, mask).Count == 0)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization)
			{
				string testString = new string('A', cf.CharacterNumber);
				string testString2 = new string('A', cf.CharacterNumber - 1);
				if (BWFManager.GetAll(testString, mask).Count == 1)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.GetAll(testString2, mask).Count == 0)
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Punctuation) != ManagerMask.Punctuation)
			{
				return;
			}
			string testString3 = new string('!', pf.CharacterNumber);
			string testString4 = new string('!', pf.CharacterNumber - 1);
			if (BWFManager.GetAll(testString3, mask).Count == 1)
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (BWFManager.GetAll(testString4, mask).Count == 0)
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
		}
	}
}
