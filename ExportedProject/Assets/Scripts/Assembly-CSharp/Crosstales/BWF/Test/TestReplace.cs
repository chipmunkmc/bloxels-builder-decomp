using System.Collections.Generic;
using Crosstales.BWF.Model;
using Crosstales.BWF.Util;
using UnityEngine;

namespace Crosstales.BWF.Test
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_test_1_1_test_replace.html")]
	public class TestReplace : BaseTest
	{
		protected override void speedTest(ManagerMask mask)
		{
			List<string> list = new List<string>();
			list.Add(BaseTest.badword);
			stopWatch.Reset();
			stopWatch.Start();
			for (int i = 0; i < Iterations; i++)
			{
				BWFManager.Replace(createRandomString(TextStartLength + TextGrowPerIteration * i), list, mask);
			}
			stopWatch.Stop();
		}

		protected override void sanityTest(ManagerMask mask)
		{
			List<string> unwantedWords = new List<string>();
			if (BWFManager.Replace(null, unwantedWords, mask).Equals(string.Empty))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (BWFManager.Replace(BaseTest.scunthorpe, null, mask).Equals(BaseTest.scunthorpe))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (BWFManager.Replace(string.Empty, unwantedWords, mask).Equals(string.Empty))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All)
			{
				List<string> list = new List<string>();
				list.Add(BaseTest.badword);
				if (BWFManager.Replace(BaseTest.badword, list, mask).Equals(new string(ReplaceChar, BaseTest.badword.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All)
			{
				List<string> list2 = new List<string>();
				list2.Add(BaseTest.domain);
				if (BWFManager.Replace(BaseTest.domain, list2, mask).Equals(new string(ReplaceChar, BaseTest.domain.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization)
			{
				string text = new string('A', cf.CharacterNumber);
				List<string> list3 = new List<string>();
				list3.Add(text);
				if (BWFManager.Replace(text, list3, mask).Equals(text.ToLowerInvariant()))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Punctuation) != ManagerMask.Punctuation)
			{
				return;
			}
			string text2 = new string('!', pf.CharacterNumber);
			List<string> list4 = new List<string>();
			list4.Add(text2);
			if (BWFManager.Replace(text2, list4, mask).Equals(new string('!', pf.CharacterNumber - 1)))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
		}
	}
}
