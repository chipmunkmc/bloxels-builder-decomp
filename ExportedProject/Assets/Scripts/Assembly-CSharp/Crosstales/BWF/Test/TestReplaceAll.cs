using Crosstales.BWF.Model;
using Crosstales.BWF.Util;
using UnityEngine;

namespace Crosstales.BWF.Test
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_test_1_1_test_replace_all.html")]
	public class TestReplaceAll : BaseTest
	{
		protected override void speedTest(ManagerMask mask)
		{
			stopWatch.Reset();
			stopWatch.Start();
			for (int i = 0; i < Iterations; i++)
			{
				BWFManager.ReplaceAll(createRandomString(TextStartLength + TextGrowPerIteration * i), mask, TestSources);
			}
			stopWatch.Stop();
		}

		protected override void sanityTest(ManagerMask mask)
		{
			if (BWFManager.ReplaceAll(null, mask).Equals(string.Empty))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (BWFManager.ReplaceAll(string.Empty, mask).Equals(string.Empty))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.ReplaceAll(BaseTest.scunthorpe, mask, "test").Equals(BaseTest.scunthorpe))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.scunthorpe, mask, (string[])null).Equals(BaseTest.scunthorpe))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.scunthorpe, mask).Equals(BaseTest.scunthorpe))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.BadWord) == ManagerMask.BadWord || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.ReplaceAll(BaseTest.badword, mask).Equals(new string(ReplaceChar, BaseTest.badword.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.scunthorpe, mask).Equals(BaseTest.scunthorpe))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.badword, mask, "english").Equals(new string(ReplaceChar, BaseTest.badword.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.noBadword, mask, "english").Equals(BaseTest.noBadword))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.arabicBadword, mask).Equals(new string(ReplaceChar, BaseTest.arabicBadword.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.globalBadword, mask).Equals(new string(ReplaceChar, BaseTest.globalBadword.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.nameBadword, mask).Equals(new string(ReplaceChar, BaseTest.nameBadword.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.emoji, mask).Equals(new string(ReplaceChar, BaseTest.emoji.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Domain) == ManagerMask.Domain || (mask & ManagerMask.All) == ManagerMask.All)
			{
				if (BWFManager.ReplaceAll(BaseTest.domain, mask).Equals(new string(ReplaceChar, BaseTest.domain.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.email, mask).Equals(new string(ReplaceChar, BaseTest.email.Length)))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(BaseTest.noDomain, mask).Equals(BaseTest.noDomain))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Capitalization) == ManagerMask.Capitalization)
			{
				string text = new string('A', cf.CharacterNumber);
				string text2 = new string('A', cf.CharacterNumber - 1);
				if (BWFManager.ReplaceAll(text, mask).Equals(text.ToLowerInvariant()))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
				if (BWFManager.ReplaceAll(text2, mask).Equals(text2))
				{
					if (!Config.DEBUG)
					{
					}
				}
				else
				{
					failCounter++;
				}
			}
			if ((mask & ManagerMask.Punctuation) != ManagerMask.Punctuation)
			{
				return;
			}
			string testString = new string('!', pf.CharacterNumber);
			string text3 = new string('!', pf.CharacterNumber - 1);
			if (BWFManager.ReplaceAll(testString, mask).Equals(text3))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
			if (BWFManager.ReplaceAll(text3, mask).Equals(text3))
			{
				if (!Config.DEBUG)
				{
				}
			}
			else
			{
				failCounter++;
			}
		}
	}
}
