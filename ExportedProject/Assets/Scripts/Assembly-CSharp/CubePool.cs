using System.Collections.Generic;
using UnityEngine;
using VoxelEngine;

public class CubePool : MonoBehaviour
{
	public static CubePool instance;

	private Transform selfTransform;

	public int poolSize;

	public GameObject prefab;

	private static bool _InitComplete;

	private Mesh[] cubeSharedMeshes = new Mesh[64];

	private float uvOffsetScale = 0.75f;

	private int[] uvIndices = new int[24]
	{
		2, 3, 0, 1, 6, 7, 10, 11, 19, 16,
		18, 17, 23, 20, 22, 21, 4, 5, 8, 9,
		15, 14, 12, 13
	};

	public Stack<ExplosionCube> pool { get; private set; }

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			instance = this;
			_InitComplete = true;
			Object.DontDestroyOnLoad(base.gameObject);
			selfTransform = base.transform;
			Initialize();
		}
	}

	private void Initialize()
	{
		if (SystemInfo.systemMemorySize < 750)
		{
			poolSize = 1750;
		}
		pool = new Stack<ExplosionCube>(poolSize);
		AllocatePoolObjects();
		InitializeCubeMeshes();
	}

	private void AllocatePoolObjects()
	{
		for (int i = 0; i < poolSize; i++)
		{
			GameObject gameObject = Object.Instantiate(prefab);
			gameObject.name = prefab.name;
			ExplosionCube component = gameObject.GetComponent<ExplosionCube>();
			component.EarlyAwake();
			component.selfTransform.SetParent(selfTransform);
			component.hiddenPosition = new Vector3(0f, 0f, -1000f - (float)(2 * i));
			component.selfTransform.localPosition = component.hiddenPosition;
			pool.Push(component);
		}
	}

	private void InitializeCubeMeshes()
	{
		GameObject gameObject = GameObject.CreatePrimitive(PrimitiveType.Cube);
		MeshFilter component = gameObject.GetComponent<MeshFilter>();
		Mesh mesh = component.mesh;
		Vector2[] UVs = new Vector2[24];
		Vector2[] array = new Vector2[4];
		Vector3[] vertices = mesh.vertices;
		int[] triangles = mesh.triangles;
		for (int i = 0; i < 8; i++)
		{
			for (int j = 0; j < 8; j++)
			{
				int num = 0;
				int num2 = 0;
				while (num < 6)
				{
					SetFaceUVs(i, j, ref UVs, num2);
					num++;
					num2 += 4;
				}
				Mesh mesh2 = new Mesh();
				mesh2.vertices = vertices;
				mesh2.triangles = triangles;
				mesh2.uv = UVs;
				mesh2.RecalculateNormals();
				cubeSharedMeshes[j * 8 + i] = mesh2;
			}
		}
		Object.Destroy(gameObject);
	}

	private void SetFaceUVs(int col, int row, ref Vector2[] UVs, int startingIndex)
	{
		UVs[uvIndices[startingIndex++]] = new Vector2(ColorPalette.tileSize * (float)col + BlockSolidColor2D.UVEdgeOffset * uvOffsetScale, ColorPalette.tileSize * (float)(row + 1) - BlockSolidColor2D.UVEdgeOffset * uvOffsetScale);
		UVs[uvIndices[startingIndex++]] = new Vector2(ColorPalette.tileSize * (float)(col + 1) - BlockSolidColor2D.UVEdgeOffset * uvOffsetScale, ColorPalette.tileSize * (float)(row + 1) - BlockSolidColor2D.UVEdgeOffset * uvOffsetScale);
		UVs[uvIndices[startingIndex++]] = new Vector2(ColorPalette.tileSize * (float)col + BlockSolidColor2D.UVEdgeOffset * uvOffsetScale, ColorPalette.tileSize * (float)row + BlockSolidColor2D.UVEdgeOffset * uvOffsetScale);
		UVs[uvIndices[startingIndex++]] = new Vector2(ColorPalette.tileSize * (float)(col + 1) - BlockSolidColor2D.UVEdgeOffset * uvOffsetScale, ColorPalette.tileSize * (float)row + BlockSolidColor2D.UVEdgeOffset * uvOffsetScale);
	}

	public ExplosionCube Spawn(GridLocation paletteCoords)
	{
		ExplosionCube explosionCube = pool.Pop();
		explosionCube.PrepareSpawn();
		explosionCube.meshFilter.sharedMesh = cubeSharedMeshes[paletteCoords.r * 8 + paletteCoords.c];
		return explosionCube;
	}
}
