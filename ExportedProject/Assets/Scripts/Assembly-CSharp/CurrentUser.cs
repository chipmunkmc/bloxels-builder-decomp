using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using BestHTTP;
using UnityEngine;

public class CurrentUser : MonoBehaviour
{
	public delegate void HandleStatus(UserAccount acct);

	public delegate void HandleLoginSuccess();

	public static CurrentUser instance;

	private UserAccount _account;

	public string socketID;

	public string loginToken;

	public string ErrorString = "ERROR";

	public CoverBoard cover;

	public static bool initialServerSynced;

	public static bool syncWithGuest;

	public UserAccount account
	{
		get
		{
			return _account;
		}
		set
		{
			_account = value;
			if (this.OnUpdate != null)
			{
				this.OnUpdate(_account);
			}
		}
	}

	public bool active
	{
		get
		{
			return account != null && account.status == UserAccount.Status.active && account.completed;
		}
	}

	public string directoryPath
	{
		get
		{
			return (!active) ? "/" : ("/" + account.serverID + "/");
		}
	}

	public event HandleStatus OnUpdate;

	public event HandleLoginSuccess OnLoginSuccess;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (PrefsManager.instance.hasUserAccount)
		{
			account = PrefsManager.instance.userAccount;
			loginToken = PrefsManager.instance.userToken;
		}
		else
		{
			Logout();
		}
		cover = new CoverBoard(Color.clear);
	}

	private void Start()
	{
		if (account != null && InternetReachabilityVerifier.Instance.status == InternetReachabilityVerifier.Status.NetVerified)
		{
			StartCoroutine("DelayedUpdateFromServer");
		}
		AppStateManager.instance.OnOwnsBoard += HandleBoardStatus;
		InternetReachabilityVerifier.Instance.statusChangedDelegate += HandleInternetConnection;
		BloxelServerInterface.instance.OnApproved += HandleOnApproved;
	}

	private void OnDestroy()
	{
		AppStateManager.instance.OnOwnsBoard -= HandleBoardStatus;
		InternetReachabilityVerifier.Instance.statusChangedDelegate -= HandleInternetConnection;
		BloxelServerInterface.instance.OnApproved -= HandleOnApproved;
	}

	private void HandleOnApproved()
	{
		account.status = UserAccount.Status.active;
		Save();
	}

	private void HandleBoardStatus()
	{
		if (active)
		{
			account.ownsBoard = true;
			if (initialServerSynced)
			{
				StartCoroutine(UpdateServer_BoardOwner(delegate
				{
				}));
			}
		}
	}

	private void HandleInternetConnection(InternetReachabilityVerifier.Status newStatus)
	{
		if (newStatus == InternetReachabilityVerifier.Status.NetVerified)
		{
			StopCoroutine("DelayedUpdateFromServer");
			if (account != null && newStatus == InternetReachabilityVerifier.Status.NetVerified)
			{
				StartCoroutine("DelayedUpdateFromServer");
			}
		}
	}

	private IEnumerator DelayedUpdateFromServer()
	{
		yield return new WaitForSeconds(2f);
		UpdateFromServer();
	}

	public void UpdateFromServer()
	{
		StartCoroutine(BloxelServerRequests.instance.FindUserByToken(delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "Not Found" && response != "ERROR")
			{
				initialServerSynced = true;
				account = new UserAccount(response);
				loginToken = account.userToken;
				if (account.userToken != null)
				{
					PrefsManager.instance.userToken = account.userToken;
				}
				PlayerBankManager.instance.SyncServerAccount();
				Save();
				StartCoroutine(UpdateServer_BoardOwner(delegate
				{
				}));
			}
			else if (response == "Not Found" || response == "ERROR")
			{
				Logout();
			}
		}));
	}

	public bool IsEduUser()
	{
		return active && account.eduAccount > UserAccount.EduStatus.None;
	}

	public bool ToggleSquareLike(string squareID)
	{
		bool result = true;
		if (!account.likes.Contains(squareID))
		{
			account.likes.Add(squareID);
			result = false;
		}
		else
		{
			account.likes.Remove(squareID);
		}
		Dictionary<string, string> dictionary = new Dictionary<string, string>();
		dictionary.Add("squareID", squareID);
		dictionary.Add("userID", account.serverID);
		Dictionary<string, string> dic = dictionary;
		JSONObject json = new JSONObject(dic);
		BloxelServerInterface.instance.Emit_ToggleLike(json);
		return result;
	}

	public bool HasLikedSquare(string squareID)
	{
		return account.likes.Contains(squareID);
	}

	public void Save()
	{
		if (account != null)
		{
			PrefsManager.instance.userAccount = account;
			if (this.OnUpdate != null)
			{
				this.OnUpdate(account);
			}
		}
	}

	public void Login(UserAccount userAccount)
	{
		account = userAccount;
		loginToken = account.userToken;
		PrefsManager.instance.userToken = account.userToken;
		initialServerSynced = true;
		PlayerBankManager.instance.SyncServerAccount();
		Save();
	}

	public void SwitchAccounts()
	{
		CloudManager.Instance.SwitchSyncFiles();
		if (active && !PrefsManager.instance.hasSyncedGuest)
		{
			AppStateManager.instance.acctTransitionState = AppStateManager.AccountTransitionState.Login;
			UIPopupSyncGuest uIPopupSyncGuest = UIOverlayCanvas.instance.SyncGuestPopup();
			uIPopupSyncGuest.OnDismiss += delegate
			{
				BloxelsSceneManager.instance.GoTo(SceneName.AccountTransition);
			};
			PrefsManager.instance.hasSyncedGuest = true;
		}
		else
		{
			if (active)
			{
				AppStateManager.instance.acctTransitionState = AppStateManager.AccountTransitionState.Login;
			}
			else
			{
				AppStateManager.instance.acctTransitionState = AppStateManager.AccountTransitionState.Logout;
			}
			BloxelsSceneManager.instance.GoTo(SceneName.AccountTransition);
		}
	}

	public IEnumerator UpdateServer_Gems(Action<string> callback)
	{
		string token = PrefsManager.instance.userToken;
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + account.serverID;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("gemBool", PrefsManager.instance.gemBooleanString);
		payloadObject.AddField("spendBoolv2", PrefsManager.instance.spendBooleanStringv2);
		payloadObject.AddField("featuredBool", PrefsManager.instance.featuredBooleanString);
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", token);
		request.SetHeader("Auth-UserID", instance.account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateServer_BoardOwner(Action<string> callback)
	{
		string token = PrefsManager.instance.userToken;
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + account.serverID;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("ownsBoard", AppStateManager.instance.userOwnsBoard);
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", token);
		request.SetHeader("Auth-UserID", instance.account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateServer_Avatar(Action<string> callback)
	{
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + account.serverID;
		string payload = string.Empty;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userAvatar", account.avatar._serverID);
		payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", loginToken);
		request.SetHeader("Auth-UserID", instance.account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateServer_UserName(string _uName, Action<string> callback)
	{
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + account.serverID;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("userName", _uName);
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", loginToken);
		request.SetHeader("Auth-UserID", account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateServer_UnFollowUser(string userID, Action<string> callback)
	{
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + account.serverID;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("unFollowUser", userID);
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", loginToken);
		request.SetHeader("Auth-UserID", account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator UpdateServer_FollowUser(string userID, Action<string> callback)
	{
		string path = "accounts";
		string url = BloxelServerInterface.instance.apiURL + "/" + path + "/" + account.serverID;
		JSONObject payloadObject = new JSONObject();
		payloadObject.AddField("followUser", userID);
		string payload = payloadObject.ToString();
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode == 200 || resp.StatusCode == 201)
			{
				callback(resp.DataAsText);
			}
			else
			{
				callback(ErrorString);
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		request.SetHeader("Auth-Token", loginToken);
		request.SetHeader("Auth-UserID", account.serverID);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(payload);
		request.Send();
		yield return StartCoroutine(request);
	}

	public void Logout()
	{
		StopCoroutine("DelayedUpdateFromServer");
		PlayerBankManager.instance.CancelUpdate();
		PlayerBankManager.instance.SetCoinCount(0);
		PlayerBankManager.instance.SetGemCount(0);
		account = null;
		if (PrefsManager.instance.hasUserAccount)
		{
			PrefsManager.instance.RemoveLoggedInUser();
		}
	}
}
