using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class DecorateTutorial : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public Button uiButtonComplete;

	public RectTransform indicatorLibrary;

	public RectTransform indicatorDraw;

	public RectTransform indicatorFinished;

	public float drawScrubDistance;

	private void Start()
	{
		StartCoroutine("DrawAnimation");
		BoardsBounce();
		FinishedBounce();
		uiButtonComplete.onClick.AddListener(Complete);
	}

	private IEnumerator DrawAnimation()
	{
		yield return new WaitForSeconds(2f);
		indicatorDraw.DOAnchorPos(new Vector2(indicatorDraw.anchoredPosition.x + drawScrubDistance, indicatorDraw.anchoredPosition.y), UIAnimationManager.speedSlow);
		yield return new WaitForSeconds(2f);
		indicatorDraw.DOAnchorPos(new Vector2(indicatorDraw.anchoredPosition.x - drawScrubDistance, indicatorDraw.anchoredPosition.y), UIAnimationManager.speedSlow);
		StartCoroutine("DrawAnimation");
	}

	private void BoardsBounce()
	{
		indicatorLibrary.DOAnchorPos(new Vector2(indicatorLibrary.anchoredPosition.x + 5f, indicatorLibrary.anchoredPosition.y), UIAnimationManager.speedMedium).OnComplete(delegate
		{
			indicatorLibrary.DOAnchorPos(new Vector2(indicatorLibrary.anchoredPosition.x - 5f, indicatorLibrary.anchoredPosition.y), UIAnimationManager.speedMedium).OnComplete(delegate
			{
				BoardsBounce();
			});
		});
	}

	private void FinishedBounce()
	{
		indicatorFinished.DOAnchorPos(new Vector2(indicatorFinished.anchoredPosition.x + 5f, indicatorFinished.anchoredPosition.y), UIAnimationManager.speedMedium).OnComplete(delegate
		{
			indicatorFinished.DOAnchorPos(new Vector2(indicatorFinished.anchoredPosition.x - 5f, indicatorFinished.anchoredPosition.y), UIAnimationManager.speedMedium).OnComplete(delegate
			{
				FinishedBounce();
			});
		});
	}

	private void Complete()
	{
		StopCoroutine("DrawAnimation");
		indicatorDraw.DOKill();
		indicatorDraw.DOKill();
		indicatorDraw.DOKill();
		Object.Destroy(base.gameObject, 0.2f);
	}

	public void OnPointerClick(PointerEventData pData)
	{
		Complete();
	}
}
