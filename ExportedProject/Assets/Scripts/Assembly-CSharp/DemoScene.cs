using Prime31;
using UnityEngine;

public class DemoScene : MonoBehaviour
{
	public float gravity = -25f;

	public float runSpeed = 8f;

	public float groundDamping = 20f;

	public float inAirDamping = 5f;

	public float jumpHeight = 3f;

	[HideInInspector]
	private float normalizedHorizontalSpeed;

	private CharacterController2D _controller;

	private Animator _animator;

	private RaycastHit2D _lastControllerColliderHit;

	private Vector3 _velocity;

	private void Awake()
	{
		_animator = GetComponent<Animator>();
		_controller = GetComponent<CharacterController2D>();
		_controller.onControllerCollidedEvent += onControllerCollider;
		_controller.onTriggerEnterEvent += onTriggerEnterEvent;
		_controller.onTriggerExitEvent += onTriggerExitEvent;
	}

	private void onControllerCollider(RaycastHit2D hit)
	{
		if (hit.normal.y != 1f)
		{
		}
	}

	private void onTriggerEnterEvent(Collider2D col)
	{
	}

	private void onTriggerExitEvent(Collider2D col)
	{
	}

	private void Update()
	{
		if (_controller.isGrounded)
		{
			_velocity.y = 0f;
		}
		if (Input.GetKey(KeyCode.RightArrow))
		{
			normalizedHorizontalSpeed = 1f;
			if (base.transform.localScale.x < 0f)
			{
				base.transform.localScale = new Vector3(0f - base.transform.localScale.x, base.transform.localScale.y, base.transform.localScale.z);
			}
			if (_controller.isGrounded)
			{
				_animator.Play(Animator.StringToHash("Run"));
			}
		}
		else if (Input.GetKey(KeyCode.LeftArrow))
		{
			normalizedHorizontalSpeed = -1f;
			if (base.transform.localScale.x > 0f)
			{
				base.transform.localScale = new Vector3(0f - base.transform.localScale.x, base.transform.localScale.y, base.transform.localScale.z);
			}
			if (_controller.isGrounded)
			{
				_animator.Play(Animator.StringToHash("Run"));
			}
		}
		else
		{
			normalizedHorizontalSpeed = 0f;
			if (_controller.isGrounded)
			{
				_animator.Play(Animator.StringToHash("Idle"));
			}
		}
		if (_controller.isGrounded && Input.GetKeyDown(KeyCode.UpArrow))
		{
			_velocity.y = Mathf.Sqrt(2f * jumpHeight * (0f - gravity));
			_animator.Play(Animator.StringToHash("Jump"));
		}
		float num = ((!_controller.isGrounded) ? inAirDamping : groundDamping);
		_velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * runSpeed, Time.deltaTime * num);
		_velocity.y += gravity * Time.deltaTime;
		if (_controller.isGrounded && Input.GetKey(KeyCode.DownArrow))
		{
			_velocity.y *= 3f;
			_controller.ignoreOneWayPlatformsThisFrame = true;
			_animator.Play(Animator.StringToHash("Jump"));
		}
		_controller.move(_velocity * Time.deltaTime);
		_velocity = _controller.velocity;
	}
}
