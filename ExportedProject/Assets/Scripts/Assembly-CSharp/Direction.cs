public enum Direction
{
	None = 0,
	All = 1,
	Up = 2,
	Right = 3,
	Down = 4,
	Left = 5,
	UpLeft = 6,
	UpRight = 7,
	DownLeft = 8,
	DownRight = 9
}
