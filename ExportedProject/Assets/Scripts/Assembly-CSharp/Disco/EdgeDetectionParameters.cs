using UnityEngine;

namespace Disco
{
	public class EdgeDetectionParameters : ExposableMonoBehaviour
	{
		[HideInInspector]
		[SerializeField]
		private int pre_medianBlur_kSize;

		[HideInInspector]
		[SerializeField]
		private int pre_adaptiveThresh_blockSize;

		[HideInInspector]
		[SerializeField]
		private int pre_adaptiveThresh_offset;

		[HideInInspector]
		[SerializeField]
		private double minPolyArea;

		[HideInInspector]
		[SerializeField]
		private double maxPolyArea;

		[HideInInspector]
		[SerializeField]
		private double polyApproxTolerance;

		[HideInInspector]
		[SerializeField]
		private int morphOps_erodeSize;

		[HideInInspector]
		[SerializeField]
		private int morphOps_dilateSize;

		[ExposeProperty]
		public int Pre_medianBlur_kSize
		{
			get
			{
				return pre_medianBlur_kSize;
			}
			set
			{
				if (value % 2 == 0)
				{
					pre_medianBlur_kSize = ((value <= 1) ? 1 : (value - 1));
				}
				else
				{
					pre_medianBlur_kSize = value;
				}
			}
		}

		[ExposeProperty]
		public int Pre_adaptiveThresh_blockSize
		{
			get
			{
				return pre_adaptiveThresh_blockSize;
			}
			set
			{
				if (value % 2 == 0)
				{
					pre_adaptiveThresh_blockSize = ((value <= 1) ? 1 : (value - 1));
				}
				else
				{
					pre_adaptiveThresh_blockSize = value;
				}
			}
		}

		[ExposeProperty]
		public int Pre_adaptiveThresh_offset { get; set; }

		public double MinPolyArea { get; set; }

		public double MaxPolyArea { get; set; }

		public double PolyApproxTolerance
		{
			get
			{
				return polyApproxTolerance;
			}
			set
			{
				polyApproxTolerance = ((!(value >= 0.0)) ? 0.0 : value);
			}
		}

		[ExposeProperty]
		public int MorphOps_erodeSize
		{
			get
			{
				return morphOps_erodeSize;
			}
			set
			{
				morphOps_erodeSize = ((value < 1) ? 1 : value);
			}
		}

		[ExposeProperty]
		public int MorphOps_dilateSize
		{
			get
			{
				return morphOps_dilateSize;
			}
			set
			{
				morphOps_dilateSize = ((value < 1) ? 1 : value);
			}
		}

		public EdgeDetectionParameters()
		{
			Pre_medianBlur_kSize = 3;
			Pre_adaptiveThresh_blockSize = 11;
			Pre_adaptiveThresh_offset = 3;
			MinPolyArea = 8000.0;
			MaxPolyArea = 900000.0;
			PolyApproxTolerance = 15.0;
			MorphOps_erodeSize = 1;
			MorphOps_dilateSize = 1;
		}
	}
}
