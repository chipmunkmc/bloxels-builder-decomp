using UnityEngine;

namespace Disco
{
	public class GridDetectionParameters : ExposableMonoBehaviour
	{
		[HideInInspector]
		[SerializeField]
		private int pre_medianBlur_kSize;

		[HideInInspector]
		[SerializeField]
		private int pre_adaptiveThresh_blockSize;

		[HideInInspector]
		[SerializeField]
		private int pre_adaptiveThresh_offset;

		[HideInInspector]
		[SerializeField]
		private double minPolyArea;

		[HideInInspector]
		[SerializeField]
		private double maxPolyArea;

		[HideInInspector]
		[SerializeField]
		private double polyApproxTolerance;

		[HideInInspector]
		[SerializeField]
		private int morphOps_erodeSize;

		[HideInInspector]
		[SerializeField]
		private int morphOps_dilateSize;

		[HideInInspector]
		[SerializeField]
		private int cannyLow_board;

		[HideInInspector]
		[SerializeField]
		private int cannyHigh_board;

		public float contrastValue = 1f;

		[ExposeProperty]
		public int Pre_medianBlur_kSize
		{
			get
			{
				return pre_medianBlur_kSize;
			}
			set
			{
				if (value % 2 == 0)
				{
					pre_medianBlur_kSize = ((value <= 1) ? 1 : (value - 1));
				}
				else
				{
					pre_medianBlur_kSize = value;
				}
			}
		}

		[ExposeProperty]
		public int Pre_adaptiveThresh_blockSize
		{
			get
			{
				return pre_adaptiveThresh_blockSize;
			}
			set
			{
				if (value % 2 == 0)
				{
					pre_adaptiveThresh_blockSize = ((value <= 1) ? 1 : (value - 1));
				}
				else
				{
					pre_adaptiveThresh_blockSize = value;
				}
			}
		}

		[ExposeProperty]
		public int Pre_adaptiveThresh_offset { get; set; }

		public double MinPolyArea { get; set; }

		public double MaxPolyArea { get; set; }

		public double PolyApproxTolerance
		{
			get
			{
				return polyApproxTolerance;
			}
			set
			{
				polyApproxTolerance = ((!(value >= 0.0)) ? 0.0 : value);
			}
		}

		[ExposeProperty]
		public int MorphOps_erodeSize
		{
			get
			{
				return morphOps_erodeSize;
			}
			set
			{
				morphOps_erodeSize = ((value < 1) ? 1 : value);
			}
		}

		[ExposeProperty]
		public int MorphOps_dilateSize
		{
			get
			{
				return morphOps_dilateSize;
			}
			set
			{
				morphOps_dilateSize = ((value < 1) ? 1 : value);
			}
		}

		[ExposeProperty]
		public int CannyLow_Board
		{
			get
			{
				return cannyLow_board;
			}
			set
			{
				cannyLow_board = value;
			}
		}

		[ExposeProperty]
		public int CannyHigh_Board
		{
			get
			{
				return cannyHigh_board;
			}
			set
			{
				cannyHigh_board = value;
			}
		}

		public GridDetectionParameters()
		{
			Pre_medianBlur_kSize = 7;
			Pre_adaptiveThresh_blockSize = 5;
			Pre_adaptiveThresh_offset = 4;
			MinPolyArea = 8000.0;
			MaxPolyArea = 900000.0;
			PolyApproxTolerance = 15.0;
			MorphOps_erodeSize = 12;
			MorphOps_dilateSize = 10;
			CannyLow_Board = 10;
			CannyHigh_Board = 60;
		}
	}
}
