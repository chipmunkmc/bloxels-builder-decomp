using UnityEngine;

namespace Disco
{
	public class UIColorOutputViewer : MonoBehaviour
	{
		public static UIColorOutputViewer instance;

		public GameObject output;

		public Transform[] blockTransforms;

		private void Awake()
		{
			if (instance == null)
			{
				instance = this;
			}
		}

		private void Start()
		{
			constructTransforms();
		}

		private void constructTransforms()
		{
			blockTransforms = new Transform[output.transform.childCount];
			int num = 0;
			foreach (Transform item in output.transform)
			{
				blockTransforms[num] = item;
				num++;
			}
		}
	}
}
