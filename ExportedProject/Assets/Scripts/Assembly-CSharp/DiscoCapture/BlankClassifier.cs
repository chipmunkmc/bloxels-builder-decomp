using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class BlankClassifier
	{
		private static Mat _RoiMask;

		private static Mat[,] _InnerRoiMasks;

		private static Mat[,] _OuterRoiMasks;

		private static Mat[,][] _BufferedRois;

		private static double[] meanBuffer = new double[3];

		public static void InitSubmatBuffers(Mat[] bufferedMats, Point startingPoint, float roiSize)
		{
			_RoiMask = new Mat(CaptureController.PreviewHeight, CaptureController.PreviewHeight, 0);
			_RoiMask.setTo(new Scalar(255.0));
			_OuterRoiMasks = new Mat[13, 13];
			_InnerRoiMasks = new Mat[13, 13];
			_BufferedRois = new Mat[13, 13][];
			int num = Mathf.FloorToInt(roiSize / 4f);
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					Mat[] array = new Mat[bufferedMats.Length];
					OpenCVForUnity.Rect roi = new OpenCVForUnity.Rect(Mathf.FloorToInt((float)startingPoint.x + roiSize * (float)i), Mathf.FloorToInt((float)startingPoint.y + roiSize * (float)j), Mathf.FloorToInt(roiSize), Mathf.FloorToInt(roiSize));
					for (int k = 0; k < bufferedMats.Length; k++)
					{
						array[k] = bufferedMats[k].submat(roi);
					}
					_BufferedRois[i, 12 - j] = array;
					OpenCVForUnity.Rect roi2 = new OpenCVForUnity.Rect(Mathf.FloorToInt((float)startingPoint.x + roiSize * (float)i + (float)num), Mathf.FloorToInt((float)startingPoint.y + roiSize * (float)j + (float)num), Mathf.FloorToInt(roiSize - (float)(2 * num)), Mathf.FloorToInt(roiSize - (float)(2 * num)));
					Mat mat = _RoiMask.submat(roi2);
					mat.setTo(new Scalar(0.0));
					Mat mat2 = _RoiMask.submat(roi);
					_OuterRoiMasks[i, 12 - j] = _RoiMask.submat(roi);
					Mat mat3 = new Mat(mat2.size(), mat2.type());
					Core.bitwise_not(mat2, mat3);
					_InnerRoiMasks[i, 12 - j] = mat3;
				}
			}
		}

		public static bool ClassifyBlankAtLocation(int col, int row, int frameIndex)
		{
			Mat src = _BufferedRois[col, row][frameIndex];
			Mat mask = _OuterRoiMasks[col, row];
			Core.meanNonAlloc(src, mask, meanBuffer);
			byte b = (byte)meanBuffer[2];
			Mat mask2 = _InnerRoiMasks[col, row];
			Core.meanNonAlloc(src, mask2, meanBuffer);
			byte b2 = (byte)meanBuffer[2];
			return b2 < b;
		}
	}
}
