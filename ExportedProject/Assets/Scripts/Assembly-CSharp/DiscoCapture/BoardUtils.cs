using System.Collections.Generic;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class BoardUtils
	{
		private const int CORNER_ROTATIONS_FOR_FLIP = 2;

		private static int rotateFlippedCornersBy = 2;

		private static float[] _CurrentPoint = new float[2];

		private static Point[] _CornerBuffer = new Point[4]
		{
			new Point(0.0, 0.0),
			new Point(0.0, 0.0),
			new Point(0.0, 0.0),
			new Point(0.0, 0.0)
		};

		private static double[] _A = new double[4];

		private static double[] _B = new double[4];

		private static Point _Center = new Point(0.0, 0.0);

		private static double[] _CornersPerspective = new double[8];

		private static double[] _CornersSquare = new double[8];

		private static Mat _SquareMat = new Mat(4, 1, CvType.CV_32FC2);

		private static Mat _CornerMatPerspective = new Mat(4, 1, CvType.CV_32FC2);

		public static bool VerifyQuad(ref List<Point> cornersToSort, Size previewFrame)
		{
			if (cornersToSort[0].x < 3.0 || cornersToSort[1].x > previewFrame.width - 3.0 || cornersToSort[2].x > previewFrame.width - 3.0 || cornersToSort[3].x < 3.0 || cornersToSort[0].y < 3.0 || cornersToSort[1].y < 3.0 || cornersToSort[2].y > previewFrame.height - 3.0 || cornersToSort[3].y > previewFrame.height - 3.0)
			{
				return false;
			}
			return true;
		}

		public static bool VerifyQuadAndSortCorners(ref MatOfPoint2f finalPolyPoints, ref List<Point> cornersToSort, Size previewFrame)
		{
			if (finalPolyPoints.rows() != 4)
			{
				return false;
			}
			SortCorners(ref cornersToSort, finalPolyPoints);
			if (cornersToSort[rotateFlippedCornersBy % 4].x < 3.0 || cornersToSort[(1 + rotateFlippedCornersBy) % 4].x > previewFrame.width - 3.0 || cornersToSort[(2 + rotateFlippedCornersBy) % 4].x > previewFrame.width - 3.0 || cornersToSort[(3 + rotateFlippedCornersBy) % 4].x < 3.0 || cornersToSort[rotateFlippedCornersBy % 4].y < 3.0 || cornersToSort[1].y < 3.0 || cornersToSort[(2 + rotateFlippedCornersBy) % 4].y > previewFrame.height - 3.0 || cornersToSort[(3 + rotateFlippedCornersBy) % 4].y > previewFrame.height - 3.0)
			{
				return false;
			}
			return true;
		}

		public static void SortCorners(ref Point[] cornersToReturn, MatOfPoint2f cornersMat)
		{
			for (int i = 0; i < 4; i++)
			{
				cornersMat.get(i, 0, _CurrentPoint);
				_CornerBuffer[i].x = _CurrentPoint[0];
				_CornerBuffer[i].y = _CurrentPoint[1];
			}
			Point point = FindCenterOfPoints(_CornerBuffer);
			for (int j = 0; j < 4; j++)
			{
				if (_CornerBuffer[j].x < point.x)
				{
					if (_CornerBuffer[j].y < point.y)
					{
						cornersToReturn[0].x = _CornerBuffer[j].x;
						cornersToReturn[0].y = _CornerBuffer[j].y;
					}
					else
					{
						cornersToReturn[3].x = _CornerBuffer[j].x;
						cornersToReturn[3].y = _CornerBuffer[j].y;
					}
				}
				else if (_CornerBuffer[j].y < point.y)
				{
					cornersToReturn[1].x = _CornerBuffer[j].x;
					cornersToReturn[1].y = _CornerBuffer[j].y;
				}
				else
				{
					cornersToReturn[2].x = _CornerBuffer[j].x;
					cornersToReturn[2].y = _CornerBuffer[j].y;
				}
			}
			rotateFlippedCornersBy = 0;
			cornersToReturn.CopyTo(_CornerBuffer, 0);
			if (ProcessorEnvironment.FlipCamera)
			{
				rotateFlippedCornersBy = 2;
				cornersToReturn.CopyTo(_CornerBuffer, 0);
				for (int k = 0; k < 4; k++)
				{
					cornersToReturn[k] = _CornerBuffer[(k + rotateFlippedCornersBy) % 4];
				}
			}
		}

		public static void SortCorners(ref List<Point> cornersToReturn, MatOfPoint2f cornersMat)
		{
			for (int i = 0; i < 4; i++)
			{
				cornersMat.get(i, 0, _CurrentPoint);
				_CornerBuffer[i].x = _CurrentPoint[0];
				_CornerBuffer[i].y = _CurrentPoint[1];
			}
			Point point = FindCenterOfPoints(_CornerBuffer);
			for (int j = 0; j < 4; j++)
			{
				if (_CornerBuffer[j].x < point.x)
				{
					if (_CornerBuffer[j].y < point.y)
					{
						cornersToReturn[0].x = _CornerBuffer[j].x;
						cornersToReturn[0].y = _CornerBuffer[j].y;
					}
					else
					{
						cornersToReturn[3].x = _CornerBuffer[j].x;
						cornersToReturn[3].y = _CornerBuffer[j].y;
					}
				}
				else if (_CornerBuffer[j].y < point.y)
				{
					cornersToReturn[1].x = _CornerBuffer[j].x;
					cornersToReturn[1].y = _CornerBuffer[j].y;
				}
				else
				{
					cornersToReturn[2].x = _CornerBuffer[j].x;
					cornersToReturn[2].y = _CornerBuffer[j].y;
				}
			}
			rotateFlippedCornersBy = 0;
			cornersToReturn.CopyTo(_CornerBuffer, 0);
			if (ProcessorEnvironment.FlipCamera)
			{
				rotateFlippedCornersBy = 2;
				cornersToReturn.CopyTo(_CornerBuffer, 0);
				for (int k = 0; k < 4; k++)
				{
					cornersToReturn[k] = _CornerBuffer[(k + rotateFlippedCornersBy) % 4];
				}
			}
		}

		public static Point FindCenterOfPoints(List<Point> corners)
		{
			Point point = corners[0];
			Point point2 = corners[1];
			Point point3 = corners[2];
			Point point4 = corners[3];
			double x = (point.x + point2.x + point3.x + point4.x) / 4.0;
			double y = (point.y + point2.y + point3.y + point4.y) / 4.0;
			return new Point(x, y);
		}

		public static Point FindCenterOfPoints(Point[] corners)
		{
			Point point = corners[0];
			Point point2 = corners[1];
			Point point3 = corners[2];
			Point point4 = corners[3];
			double x = (point.x + point2.x + point3.x + point4.x) / 4.0;
			double y = (point.y + point2.y + point3.y + point4.y) / 4.0;
			return new Point(x, y);
		}

		public static float ComputeROIOffset(ref Point[] corners, float blockHeight)
		{
			Point point = corners[0];
			Point point2 = corners[1];
			Point point3 = corners[2];
			Point point4 = corners[3];
			float num = Mathf.Sqrt(Mathf.Pow((float)(point.x - point2.x), 2f) + Mathf.Pow((float)(point.y - point2.y), 2f));
			float num2 = Mathf.Sqrt(Mathf.Pow((float)(point4.x - point3.x), 2f) + Mathf.Pow((float)(point4.y - point3.y), 2f));
			return (1f - num / num2) * blockHeight;
		}

		public static List<Point> GetInversePerspectiveCorners(List<Point> sortedCorners, Mat inversePerspectiveTransform)
		{
			double[] data = new double[8]
			{
				sortedCorners[0].x,
				sortedCorners[0].y,
				sortedCorners[1].x,
				sortedCorners[1].y,
				sortedCorners[2].x,
				sortedCorners[2].y,
				sortedCorners[3].x,
				sortedCorners[3].y
			};
			Mat mat = new Mat(4, 1, CvType.CV_32FC2);
			mat.put(0, 0, data);
			List<Point> list = new List<Point>(4);
			Mat mat2 = new Mat(4, 1, CvType.CV_32FC2);
			Core.perspectiveTransform(mat, mat2, inversePerspectiveTransform);
			Converters.Mat_to_vector_Point(mat2, list);
			return list;
		}

		public static List<Point> ExtendCorners(List<Point> sortedCorners, float boardCenterToExtendedCorner, float boardCenterToGridCorner)
		{
			_A[0] = sortedCorners[0].x;
			_A[1] = sortedCorners[0].y;
			_A[2] = sortedCorners[2].x;
			_A[3] = sortedCorners[2].y;
			_B[0] = sortedCorners[1].x;
			_B[1] = sortedCorners[1].y;
			_B[2] = sortedCorners[3].x;
			_B[3] = sortedCorners[3].y;
			ComputeIntersectNonAlloc(_A, _B, _Center);
			List<Point> list = new List<Point>(4);
			int num = 0;
			int num2 = 0;
			while (num < 4)
			{
				_CornersPerspective[num2] = sortedCorners[num].x;
				_CornersPerspective[num2 + 1] = sortedCorners[num].y;
				num++;
				num2 += 2;
			}
			_CornersSquare[0] = _Center.x - 1.0;
			_CornersSquare[1] = _Center.y - 1.0;
			_CornersSquare[2] = _Center.x + 1.0;
			_CornersSquare[3] = _Center.y - 1.0;
			_CornersSquare[4] = _Center.x + 1.0;
			_CornersSquare[5] = _Center.y + 1.0;
			_CornersSquare[6] = _Center.x - 1.0;
			_CornersSquare[7] = _Center.y + 1.0;
			_SquareMat.put(0, 0, _CornersSquare);
			_CornerMatPerspective.put(0, 0, _CornersPerspective);
			Mat perspectiveTransform = Imgproc.getPerspectiveTransform(_SquareMat, _CornerMatPerspective);
			float num3 = boardCenterToExtendedCorner / boardCenterToGridCorner;
			_CornersSquare[0] = _Center.x - (double)num3;
			_CornersSquare[1] = _Center.y - (double)num3;
			_CornersSquare[2] = _Center.x + (double)num3;
			_CornersSquare[3] = _Center.y - (double)num3;
			_CornersSquare[4] = _Center.x + (double)num3;
			_CornersSquare[5] = _Center.y + (double)num3;
			_CornersSquare[6] = _Center.x - (double)num3;
			_CornersSquare[7] = _Center.y + (double)num3;
			_SquareMat.put(0, 0, _CornersSquare);
			Core.perspectiveTransform(_SquareMat, _CornerMatPerspective, perspectiveTransform);
			Converters.Mat_to_vector_Point(_CornerMatPerspective, list);
			return list;
		}

		public static void ComputeIntersectNonAlloc(double[] a, double[] b, Point result)
		{
			double num = a[0];
			double num2 = a[1];
			double num3 = a[2];
			double num4 = a[3];
			double num5 = b[0];
			double num6 = b[1];
			double num7 = b[2];
			double num8 = b[3];
			float num9 = (float)(num - num3) * (float)(num6 - num8) - (float)(num2 - num4) * (float)(num5 - num7);
			result.x = ((num * num4 - num2 * num3) * (num5 - num7) - (num - num3) * (num5 * num8 - num6 * num7)) / (double)num9;
			result.y = ((num * num4 - num2 * num3) * (num6 - num8) - (num2 - num4) * (num5 * num8 - num6 * num7)) / (double)num9;
		}
	}
}
