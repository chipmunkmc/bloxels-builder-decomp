using System.Collections.Generic;
using OpenCVForUnity;

namespace DiscoCapture
{
	public class BrightnessDetector
	{
		private Mat _firstPass;

		public static Mat Mask;

		public static Mat[] _EdgeMats = new Mat[4];

		private static int _Inset = 10;

		private static int _ShortSide;

		private static int _LongSide;

		private double[] _avgHSV = new double[4];

		private double _avgH;

		private double _avgS;

		private double _avgV;

		public static double CurrentBrightness;

		public static List<double> Buffer = new List<double>(128);

		public static float AdjustmentInterval = 1f;

		private static double _IdealBrightness = 90.0;

		private static double _IdealDeviation = 10.0;

		public BrightnessDetector()
		{
			_firstPass = new Mat(CaptureController.PreviewSize, CvType.CV_8UC3);
			Mask = Mat.zeros(new Size(CaptureController.PreviewHeight - 1, CaptureController.PreviewHeight - 1), 0);
			int num = 5;
			int num2 = Mask.width();
			int num3 = Mask.height();
			Mat mat = Mask.submat(num, num3 - num, num, num2 - num);
			mat.setTo(new Scalar(255.0));
			int num4 = 40 + num;
			Mat mat2 = Mask.submat(num4, num3 - num4, num4, num2 - num4);
			mat2.setTo(new Scalar(0.0));
		}

		private double doFirstPass()
		{
			CaptureController.CameraMat.copyTo(_firstPass);
			Imgproc.cvtColor(_firstPass, _firstPass, 41);
			Core.meanNonAlloc(_firstPass, _avgHSV);
			_avgH = _avgHSV[0];
			_avgS = _avgHSV[1];
			_avgV = _avgHSV[2];
			Buffer.Add(_avgV);
			return _avgV;
		}

		public double Process()
		{
			if (ProcessorEnvironment.HsvMat == null)
			{
				return 0.0;
			}
			if (Mask == null)
			{
				return 0.0;
			}
			if (ProcessorEnvironment.HsvMat.rows() != Mask.rows() || ProcessorEnvironment.HsvMat.cols() != Mask.cols())
			{
				return 0.0;
			}
			Core.meanNonAlloc(ProcessorEnvironment.HsvMat, Mask, _avgHSV);
			_avgH = _avgHSV[0];
			_avgS = _avgHSV[1];
			_avgV = _avgHSV[2];
			Buffer.Add(_avgV);
			return _avgV;
		}
	}
}
