using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class CLAHEProcessor
	{
		private static CLAHE _fullWindowClahe;

		private static CLAHE _cornerClahe;

		private static CLAHE _warpedClahe;

		public int claheAdjustDirection;

		private int _windowTileSize = 14;

		private float _windowClipLimit = 10f;

		private float _cornerClashClip = 0.6f;

		private int _cornerClashTileSize = 1;

		private int _warpedClaheTileSize = 24;

		private float _warpedClaheClip = 6f;

		public int windowTileSize
		{
			get
			{
				return _windowTileSize;
			}
			set
			{
				if (value <= 0)
				{
					value = 1;
				}
				_windowTileSize = value;
				if (_fullWindowClahe != null)
				{
					_fullWindowClahe.setTilesGridSize(new Size(_windowTileSize, (float)_windowTileSize * 1.333f));
				}
			}
		}

		public float windowClipLimit
		{
			get
			{
				return _windowClipLimit;
			}
			set
			{
				value = Mathf.Clamp(value, 0.2f, 10f);
				_windowClipLimit = value;
				if (_fullWindowClahe != null)
				{
					_fullWindowClahe.setClipLimit(_windowClipLimit);
				}
			}
		}

		public float cornerClashClip
		{
			get
			{
				return _cornerClashClip;
			}
			set
			{
				value = Mathf.Clamp(value, 0.2f, 10f);
				_cornerClashClip = value;
				if (_cornerClahe != null)
				{
					_cornerClahe.setClipLimit(_cornerClashClip);
				}
			}
		}

		public int cornerClashTileSize
		{
			get
			{
				return _cornerClashTileSize;
			}
			set
			{
				if (value <= 0)
				{
					value = 1;
				}
				_cornerClashTileSize = value;
				if (_cornerClahe != null)
				{
					_cornerClahe.setTilesGridSize(new Size(_cornerClashTileSize, _cornerClashTileSize));
				}
			}
		}

		public int warpedClaheTileSize
		{
			get
			{
				return _warpedClaheTileSize;
			}
			set
			{
				if (value <= 0)
				{
					value = 1;
				}
				_warpedClaheTileSize = value;
				if (_warpedClahe != null)
				{
					_warpedClahe.setTilesGridSize(new Size(_warpedClaheTileSize, _warpedClaheTileSize));
				}
			}
		}

		public float warpedClaheClip
		{
			get
			{
				return _warpedClaheClip;
			}
			set
			{
				value = Mathf.Clamp(value, 0.2f, 20f);
				_warpedClaheClip = value;
				if (_warpedClahe != null)
				{
					_warpedClahe.setClipLimit(_warpedClaheClip);
				}
			}
		}

		public CLAHEProcessor()
		{
			_fullWindowClahe = Imgproc.createCLAHE(_windowClipLimit, new Size(_windowTileSize, (float)_windowTileSize * 1.333f));
			_cornerClahe = Imgproc.createCLAHE(cornerClashClip, new Size(cornerClashTileSize, cornerClashTileSize));
			_warpedClahe = Imgproc.createCLAHE(warpedClaheClip, new Size(warpedClaheTileSize, warpedClaheTileSize));
		}

		public void IncrementWindow()
		{
			CaptureController.Instance.claheProcessor.windowClipLimit += 0.2f;
			CaptureController.Instance.claheProcessor.claheAdjustDirection = 1;
		}

		public void DecrementWindow()
		{
			CaptureController.Instance.claheProcessor.windowClipLimit -= 0.2f;
			CaptureController.Instance.claheProcessor.claheAdjustDirection = -1;
		}

		public void AdjustDependentClipLimits()
		{
			cornerClashClip = windowClipLimit * 0.33f;
		}

		public static void ApplyWindowClahe(Mat src, Mat dst)
		{
			_fullWindowClahe.apply(src, dst);
		}

		public static void ApplyCornerClahe(Mat src, Mat dst)
		{
			_cornerClahe.apply(src, dst);
		}

		public static void ApplyWarpedClahe(Mat src, Mat dst)
		{
			_warpedClahe.apply(src, dst);
		}
	}
}
