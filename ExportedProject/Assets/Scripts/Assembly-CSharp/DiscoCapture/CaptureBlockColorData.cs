using System;

namespace DiscoCapture
{
	[Serializable]
	public class CaptureBlockColorData
	{
		public double[] cctVals;

		public double[] channelVals;
	}
}
