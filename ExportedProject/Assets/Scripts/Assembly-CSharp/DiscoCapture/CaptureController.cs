using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	[RequireComponent(typeof(DeviceCameraController))]
	public class CaptureController : MonoBehaviour
	{
		public delegate void HandleOnEnable();

		public delegate void HandleOnDisable();

		public delegate void HandleBlockClassified(int col, int row, BlockColor color);

		public delegate void HandleProcessDelegate();

		public static CaptureController Instance;

		private static bool _InitComplete;

		public DeviceCameraController deviceCamera;

		public static Size PreviewSize;

		public static int PreviewHeight;

		public static int PreviewWidth;

		public static Mat CameraMat;

		public static Scalar ZeroScalar = new Scalar(0.0);

		public static Scalar White = new Scalar(255.0, 255.0, 255.0);

		public static Scalar Black = new Scalar(0.0, 0.0, 0.0);

		public static int FrameBufferLength = 4;

		public CLAHEProcessor claheProcessor;

		public GridPreProcessor gridPreProcessor;

		public GridCenterMassProcessor centerMassProcessor;

		public PolygonHullProcessor polygonHullProcessor;

		public CornerProcessor cornerProcessor;

		public GridVerification gridVerification;

		public CorrelatedColorTemperature cctProcessor;

		public BrightnessDetector brightnessDetector;

		public EdgeDetectionParams edgeParams;

		public EdgeProcessor edgeProcessor;

		public Dictionary<int, byte[]> imageCache = new Dictionary<int, byte[]>();

		private Texture2D _captureResultTex;

		private HandleProcessDelegate[] _captureEvents;

		public int cctNotchOffset;

		public bool cctIncreaseLimitReached;

		public bool cctDecreaseLimitReached;

		public byte currentWhiteThresh = 42;

		private int[] blockIndices = new int[169];

		private System.Random rnd = new System.Random();

		private static Coroutine _ClassifierCoroutine;

		private bool restartedClassification;

		private static int numClassified;

		public int numBlocksToClassifyPerFrame = 4;

		public event HandleOnEnable onEnable;

		public event HandleOnDisable onDisable;

		public event HandleBlockClassified OnBlockClassified;

		private event HandleProcessDelegate onProcessPass;

		private event HandleProcessDelegate onProcessComplete;

		private event HandleProcessDelegate onClassificationStarted;

		private event HandleProcessDelegate onClassificationComplete;

		public void Awake()
		{
			if (Instance != null && Instance != this)
			{
				UnityEngine.Object.Destroy(base.gameObject);
			}
			else if (!_InitComplete)
			{
				Initialize();
				Instance = this;
				_InitComplete = true;
				if (deviceCamera == null)
				{
					deviceCamera = GetComponent<DeviceCameraController>();
				}
			}
		}

		private void Start()
		{
			if (deviceCamera != null)
			{
				deviceCamera.OnCachedCameraProperties += InitProcessors;
			}
		}

		private void OnEnable()
		{
			if (this.onEnable != null)
			{
				this.onEnable();
			}
		}

		private void OnDisable()
		{
			if (this.onDisable != null)
			{
				this.onDisable();
			}
		}

		private void OnDestroy()
		{
			if (Instance != null && Instance == this)
			{
				Instance = null;
				_InitComplete = false;
				if (deviceCamera != null)
				{
					deviceCamera.OnCachedCameraProperties -= InitProcessors;
				}
			}
		}

		private void Initialize()
		{
			InitIndiciesForDistributedClassification();
			InitializeEventArray();
		}

		private void InitIndiciesForDistributedClassification()
		{
			for (int i = 0; i < blockIndices.Length; i++)
			{
				blockIndices[i] = i;
			}
		}

		private void InitializeEventArray()
		{
			_captureEvents = new HandleProcessDelegate[4] { this.onProcessPass, this.onProcessComplete, this.onClassificationStarted, this.onClassificationStarted };
		}

		public void InitializeCamera()
		{
			deviceCamera.Initialize();
		}

		public void InitProcessors()
		{
			ProcessorEnvironment.GameBoard = new DiscoGameBoard(GameBoardSize.SquareMoldFinalv1_13x13, GameBoardType.physicalBlack);
			ProcessorEnvironment.HsvMat = new Mat(new Size(PreviewHeight, PreviewHeight), CvType.CV_8UC3);
			ProcessorEnvironment.DetectionType = ProcessorEnvironment.Type.Grid;
			brightnessDetector = new BrightnessDetector();
			cctProcessor = new CorrelatedColorTemperature();
			claheProcessor = new CLAHEProcessor();
			gridPreProcessor = new GridPreProcessor();
			centerMassProcessor = new GridCenterMassProcessor();
			polygonHullProcessor = new PolygonHullProcessor();
			cornerProcessor = new CornerProcessor();
			gridVerification = new GridVerification();
			edgeParams = new EdgeDetectionParams();
			edgeProcessor = new EdgeProcessor(edgeParams);
		}

		public void SetNotchRange(out float min, out float max)
		{
			min = -ColorClassifierData.CurrentMaxCCTNotchIndex;
			max = ColorClassifierData.NotchedCCTs.Length - 1 - ColorClassifierData.CurrentMinCCTNotchIndex;
		}

		public void IncrementTempOffset()
		{
			cctDecreaseLimitReached = false;
			if (!cctIncreaseLimitReached)
			{
				cctNotchOffset++;
				StartTempAdjustedClassification(out cctIncreaseLimitReached);
			}
		}

		public void DecrementTempOffset()
		{
			cctIncreaseLimitReached = false;
			if (!cctDecreaseLimitReached)
			{
				cctNotchOffset--;
				StartTempAdjustedClassification(out cctDecreaseLimitReached);
			}
		}

		public bool IncrementWhiteSatThresh()
		{
			bool result = false;
			while (ColorClassifierData.Thresholds.AdjustWhiteSatThresholds(1))
			{
				if (ColorClassifier.ClassifyAllBlocks(false))
				{
					result = true;
					break;
				}
			}
			return result;
		}

		public bool DecrementWhiteSatThresh()
		{
			bool result = false;
			while (ColorClassifierData.Thresholds.AdjustWhiteSatThresholds(-1))
			{
				if (ColorClassifier.ClassifyAllBlocks(false))
				{
					result = true;
					break;
				}
			}
			return result;
		}

		public void StartClassification()
		{
			DispatchEvent(CaptureEvent.ClassificationStart);
			cctNotchOffset = 0;
			ColorClassifier.SetCCTNotchOffset(cctNotchOffset);
			restartedClassification = true;
			if (_ClassifierCoroutine == null)
			{
				_ClassifierCoroutine = StartCoroutine(DistrubutedClassificationRandom(numBlocksToClassifyPerFrame));
			}
		}

		private void StartTempAdjustedClassification(out bool adjustmentLimitReached)
		{
			adjustmentLimitReached = ColorClassifier.SetCCTNotchOffset(cctNotchOffset);
			restartedClassification = true;
			if (_ClassifierCoroutine == null)
			{
				_ClassifierCoroutine = StartCoroutine(DistrubutedClassificationRandom(numBlocksToClassifyPerFrame));
			}
		}

		public void StopClassification()
		{
			if (_ClassifierCoroutine != null)
			{
				StopCoroutine(_ClassifierCoroutine);
				_ClassifierCoroutine = null;
			}
		}

		private IEnumerator DistrubutedClassificationRandom(int blocksPerFrame)
		{
			numClassified = 0;
			while (numClassified < 169)
			{
				if (restartedClassification)
				{
					restartedClassification = false;
					numClassified = 0;
					Shuffle(ref blockIndices);
				}
				for (int i = 0; i < 169; i++)
				{
					if (restartedClassification)
					{
						break;
					}
					int c = Mathf.FloorToInt((float)blockIndices[i] / 13f);
					int r = blockIndices[i] % 13;
					ColorClassifier.ClassifyBlockAtLocation(c, r, true);
					numClassified++;
					if (this.OnBlockClassified != null)
					{
						this.OnBlockClassified(c, r, ColorClassifier.BlockColors[c, r]);
					}
					if (i % blocksPerFrame == 0)
					{
						yield return new WaitForEndOfFrame();
					}
				}
			}
			_ClassifierCoroutine = null;
			DispatchEvent(CaptureEvent.ClassificationComplete);
		}

		private IEnumerator DistributedClassificationSpiral()
		{
			int x = 0;
			int y = 0;
			int d = 1;
			int i = 1;
			numClassified = 0;
			while (numClassified < 169)
			{
				if (restartedClassification)
				{
					restartedClassification = false;
					numClassified = 0;
					x = 0;
					y = 0;
					d = 1;
					i = 1;
				}
				while (2 * x * d < i && !restartedClassification && numClassified < 169)
				{
					ColorClassifier.ClassifyBlockAtLocation(x + 6, y + 6, true);
					if (this.OnBlockClassified != null)
					{
						this.OnBlockClassified(x + 6, y + 6, ColorClassifier.BlockColors[x + 6, y + 6]);
					}
					x += d;
					numClassified++;
					yield return new WaitForEndOfFrame();
				}
				while (2 * y * d < i && !restartedClassification && numClassified < 169)
				{
					ColorClassifier.ClassifyBlockAtLocation(x + 6, y + 6, true);
					if (this.OnBlockClassified != null)
					{
						this.OnBlockClassified(x + 6, y + 6, ColorClassifier.BlockColors[x + 6, y + 6]);
					}
					y += d;
					numClassified++;
					yield return new WaitForEndOfFrame();
				}
				if (!restartedClassification)
				{
					d = -1 * d;
					i++;
				}
			}
			_ClassifierCoroutine = null;
		}

		public void TryUpdateBlock(int col, int row, BlockColor color)
		{
			if (this.OnBlockClassified != null)
			{
				this.OnBlockClassified(col, row, color);
			}
		}

		private void Shuffle(ref int[] array)
		{
			int num = array.Length;
			while (num > 1)
			{
				int num2 = rnd.Next(num--);
				int num3 = array[num];
				array[num] = array[num2];
				array[num2] = num3;
			}
		}

		public void FillCameraMatrix()
		{
			DeviceCameraController.PreviewMatrix.copyTo(CameraMat);
		}

		public void AddListener(CaptureEvent type, HandleProcessDelegate action)
		{
			HandleProcessDelegate[] captureEvents = _captureEvents;
			int num = (int)type;
			captureEvents[num] = (HandleProcessDelegate)Delegate.Combine(captureEvents[num], action);
		}

		public void RemoveListener(CaptureEvent type, HandleProcessDelegate action)
		{
			if (_captureEvents[(int)type] != null)
			{
				HandleProcessDelegate[] captureEvents = _captureEvents;
				int num = (int)type;
				captureEvents[num] = (HandleProcessDelegate)Delegate.Combine(captureEvents[num], action);
			}
		}

		public void DispatchEvent(CaptureEvent type)
		{
			HandleProcessDelegate handleProcessDelegate = _captureEvents[(int)type];
			if (handleProcessDelegate != null)
			{
				handleProcessDelegate();
			}
		}

		public void TryAddToCache()
		{
			if (_captureResultTex == null)
			{
				_captureResultTex = new Texture2D(PreviewWidth, PreviewHeight, TextureFormat.RGB24, false);
				_captureResultTex.filterMode = FilterMode.Point;
				_captureResultTex.wrapMode = TextureWrapMode.Clamp;
			}
			int key = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
			if (!imageCache.ContainsKey(key))
			{
				Utils.fastMatToTexture2D(CameraMat, _captureResultTex);
				byte[] value = _captureResultTex.EncodeToJPG(100);
				imageCache.Add(key, value);
			}
		}

		public void ClearImageCache()
		{
			imageCache.Clear();
		}
	}
}
