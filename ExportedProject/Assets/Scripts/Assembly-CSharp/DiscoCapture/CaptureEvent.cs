namespace DiscoCapture
{
	public enum CaptureEvent
	{
		ProcessPass = 0,
		ProcessComplete = 1,
		ClassificationStart = 2,
		ClassificationComplete = 3
	}
}
