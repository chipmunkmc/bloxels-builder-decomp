using System;
using System.Collections;
using System.IO;
using System.Threading;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class CaptureImageLoader : MonoBehaviour
	{
		public delegate void HandleDownloadComplete();

		private Texture2D _currentTexture;

		private Mat _currentImageData;

		private const int _Width = 640;

		private const int _Height = 480;

		private static readonly Size _Size = new Size(640.0, 480.0);

		private Coroutine _downloadAllCoroutine;

		public static readonly string SavePath = "/S3Data/";

		private int _saveIndex;

		public event HandleDownloadComplete OnDownloadComplete;

		private void Start()
		{
			_currentTexture = new Texture2D(640, 480, TextureFormat.RGB24, false);
			_currentTexture.filterMode = FilterMode.Point;
			_currentTexture.wrapMode = TextureWrapMode.Clamp;
			_currentTexture.Apply();
			_currentImageData = new Mat(_Size, CvType.CV_8UC3);
		}

		public bool CopyImageDataToMatrix(Mat dst)
		{
			if (dst.width() != 640 || dst.height() != 480)
			{
				return false;
			}
			_currentImageData.copyTo(dst);
			return true;
		}

		public bool LoadImageFromDisk(string path)
		{
			if (!File.Exists(path))
			{
				return false;
			}
			byte[] array = File.ReadAllBytes(path);
			if (array == null)
			{
				return false;
			}
			_currentTexture.LoadImage(array);
			_currentTexture.filterMode = FilterMode.Point;
			_currentTexture.wrapMode = TextureWrapMode.Clamp;
			if (_currentImageData.width() == _currentTexture.width && _currentImageData.height() == _currentTexture.height)
			{
				Utils.fastTexture2DToMat(_currentTexture, _currentImageData);
				return true;
			}
			int width = _currentTexture.width;
			int height = _currentTexture.height;
			if (width <= 640 || height <= 640)
			{
				_currentImageData.setTo(new Scalar(0.0, 0.0, 0.0));
				return false;
			}
			using (Mat mat = new Mat(new Size(width, height), CvType.CV_8UC3))
			{
				Utils.fastTexture2DToMat(_currentTexture, mat);
				int num = (int)(((float)width - (float)height * 1.3333334f) / 2f);
				Mat src = mat.submat(0, height, num, num + (int)((float)height * 1.3333334f));
				Imgproc.resize(src, _currentImageData, _Size);
			}
			return true;
		}

		public bool LoadImageFromDiskIntoMat(string filePath, Mat dst)
		{
			if (!LoadImageFromDisk(filePath))
			{
				return false;
			}
			return CopyImageDataToMatrix(dst);
		}

		public void DownloadAllImages(string[] urls)
		{
			_downloadAllCoroutine = StartCoroutine(DownloadAndSaveAll(urls));
		}

		private IEnumerator DownloadAndSaveAll(string[] urls)
		{
			_saveIndex = 0;
			for (int i = 0; i < urls.Length; i++)
			{
				yield return StartCoroutine(DownloadAndSaveToDisk(urls[i], _saveIndex++.ToString()));
			}
			_downloadAllCoroutine = null;
			if (this.OnDownloadComplete != null)
			{
				this.OnDownloadComplete();
			}
		}

		private IEnumerator DownloadAndSaveToDisk(string jpgURL, string fileName)
		{
			using (WWW jpgWWW = new WWW(jpgURL))
			{
				yield return jpgWWW;
				if (jpgWWW.error != null)
				{
					yield break;
				}
				SaveJpgToDisk(jpgWWW.bytes, fileName + ".jpg");
			}
			string jsonURL = jpgURL.Replace(".jpg.complete", ".json");
			using (WWW jsonWWW = new WWW(jsonURL))
			{
				yield return jsonWWW;
				if (jsonWWW.error == null)
				{
					SaveTextToDisk(jsonWWW.text, fileName + ".json");
				}
			}
		}

		private void SaveJpgToDisk(byte[] jpgData, string fileName)
		{
			File.WriteAllBytes(Application.dataPath + SavePath + fileName, jpgData);
		}

		private void SaveTextToDisk(string text, string fileName)
		{
			File.WriteAllText(Application.dataPath + SavePath + fileName, text);
		}
	}
}
