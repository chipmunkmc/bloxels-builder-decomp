using System;

namespace DiscoCapture
{
	[Serializable]
	public class CaptureServerData
	{
		public double minCCT;

		public double maxCCT;

		public CaptureBlockColorData red;

		public CaptureBlockColorData orange;

		public CaptureBlockColorData yellow;

		public CaptureBlockColorData green;

		public CaptureBlockColorData blue;

		public CaptureBlockColorData purple;

		public CaptureBlockColorData pink;

		public CaptureBlockColorData white;

		public ThresholdLUT thresholds;

		public RoiOffsets offsets;

		public void CorrectHueAngleWraparound()
		{
			FixTheData(red.channelVals);
			FixTheData(orange.channelVals);
			FixTheData(yellow.channelVals);
			FixTheData(green.channelVals);
			FixTheData(blue.channelVals);
			FixTheData(purple.channelVals);
			FixTheData(pink.channelVals);
		}

		private void FixTheData(double[] hueVals)
		{
			int num = hueVals.Length;
			for (int i = 0; i < num; i++)
			{
				int num2 = (i + 1) % num;
				double num3 = hueVals[i];
				double num4 = hueVals[num2];
				if (num4 - num3 >= 180.0)
				{
					hueVals[i] += 360.0;
				}
				else if (num4 - num3 <= -180.0)
				{
					hueVals[i] -= 360.0;
				}
			}
		}
	}
}
