using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

namespace DiscoCapture
{
	public class ClassifierDebugManager : MonoBehaviour
	{
		public static ClassifierDebugManager Instance;

		private static bool _InitComplete;

		[SerializeField]
		private AnimationCurve[] _colorCurves;

		[SerializeField]
		private AnimationCurve _segments;

		private void Awake()
		{
			if (Instance != null && Instance != this)
			{
				Object.Destroy(base.gameObject);
			}
			else if (!_InitComplete)
			{
				Instance = this;
				_InitComplete = true;
			}
		}

		[Conditional("__NEVER_DEFINED__")]
		public void SetColorCurves(AnimationCurve[] curves)
		{
			_colorCurves = curves;
		}

		[Conditional("__NEVER_DEFINED__")]
		public void SetSegmentCurve(List<ColorClassifierData.CurveSegment> curveSegments)
		{
			int count = curveSegments.Count;
			Keyframe[] array = new Keyframe[count + 1];
			for (int i = 0; i < count; i++)
			{
				array[i] = new Keyframe(curveSegments[i].minCCT, 0f);
				if (i == count - 1)
				{
					array[count] = new Keyframe(curveSegments[i].maxCCT, 0f);
				}
			}
			_segments = new AnimationCurve(array);
		}
	}
}
