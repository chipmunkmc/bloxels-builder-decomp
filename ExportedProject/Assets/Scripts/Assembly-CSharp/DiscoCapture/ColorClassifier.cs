using System;
using System.Collections.Generic;
using DiscoData;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class ColorClassifier
	{
		public class RoiData
		{
			public int col;

			public int row;

			public int frameIndex;

			public Mat roi;

			public byte blankThreshold;

			public int finalHueIndex;

			public byte avgHue;

			public byte avgSat;

			public byte avgVal;

			public BlockColor blockColor;

			public RoiData(int col, int row, int frameIndex, Mat roi)
			{
				this.col = col;
				this.row = row;
				this.frameIndex = frameIndex;
				this.roi = roi;
				blankThreshold = 0;
				finalHueIndex = 0;
				avgHue = 0;
				blockColor = BlockColor.Blank;
			}

			public void SetFinalHue(int hueIndex, byte avgHue)
			{
				finalHueIndex = hueIndex;
				this.avgHue = avgHue;
			}

			public void CalculateBlockColor()
			{
				if (OriginalBlankClassifier.ClassifyBlankAtLocation(col, row, frameIndex, avgVal))
				{
					blockColor = BlockColor.Blank;
				}
				else if (avgSat < ColorClassifierData.Thresholds.GetWhiteSatThreshold(frameIndex, col, row))
				{
					blockColor = BlockColor.White;
				}
				else
				{
					blockColor = (BlockColor)finalHueIndex;
				}
			}
		}

		public static BlockColor[,] BlockColors = new BlockColor[13, 13];

		private static int _FrameBufferLength;

		private static int _RoiPixelCount;

		private static int _RoiChannelCount = 3;

		private static int _RoiTotalDataLength;

		public const float REQ_PROCESS_TIME = 0f;

		private static float _startProcessTime = 0f;

		private static bool _onePassComplete = false;

		private static Mat _GridMatrix;

		private static Mat _BlankThresholdMask;

		private static Mat[,] _GridRoiSubmats;

		private static Mat[,] _GridMaskSubmats;

		private static RoiData[,][] _RoiBuffer;

		private static byte[] _CurrentHueData;

		private static byte[] _CurrentSatData;

		private static byte[] _CurrentValData;

		private static int[] _HueModeCounts = new int[7];

		private static int[] _BlockColorModeCounts = new int[9];

		private static int[] _HueModeTotals = new int[7];

		private static int[] _HueDistanceTotals = new int[7];

		private static byte _HueBrightnessThreshLow = 1;

		private static byte _HueBrightnessThreshHigh = 254;

		private static float[] _CCTs;

		private static byte[][] _MeasuredHues;

		private static float _CubePixels;

		private static float _GutterPixels;

		private static float _Step;

		private static int _RoiInset = 0;

		private static int _RoiSize;

		private static int _BlankFirstIndex;

		private static int _BlankLastIndex;

		private static int _WhiteFirstIndex;

		private static int _WhiteLastIndex;

		private static int _BlankColLow;

		private static int _BlankColHigh;

		private static int _WhiteColLow;

		private static int _WhiteColHigh;

		public static bool Initialized = false;

		private static Mat[] _BufferedMats;

		private static Mat[,][] _BufferedRois;

		private static byte[] _CurrentPixelData;

		public static void Initialize(Mat warpedMat, DiscoGameBoard gameBoard, int frameBufferLength)
		{
			_GridMatrix = warpedMat;
			_FrameBufferLength = frameBufferLength;
			_CubePixels = gameBoard.cubeUnits * (float)_GridMatrix.width() / (gameBoard.gridWidth() + gameBoard.cubeGutter * 2f);
			_GutterPixels = gameBoard.cubeGutter * (float)_GridMatrix.width() / (gameBoard.gridWidth() + gameBoard.cubeGutter * 2f);
			_Step = _CubePixels + _GutterPixels;
			_RoiSize = Mathf.FloorToInt(_CubePixels - (float)_RoiInset);
			_RoiPixelCount = _RoiSize * _RoiSize;
			_RoiTotalDataLength = _RoiPixelCount * _RoiChannelCount;
			InitMeasuredHues();
			InitBuffers();
			InitRoiDataBuffer();
			ResetBlockColors();
			InitOffsetsFromClassiferData();
			Initialized = true;
		}

		private static void InitMeasuredHues()
		{
			_MeasuredHues = new byte[_FrameBufferLength][];
			for (int i = 0; i < _FrameBufferLength; i++)
			{
				_MeasuredHues[i] = new byte[8];
			}
			_CCTs = new float[_FrameBufferLength];
		}

		private static void InitBuffers()
		{
			_BufferedRois = new Mat[13, 13][];
			_BufferedMats = new Mat[_FrameBufferLength];
			for (int i = 0; i < _FrameBufferLength; i++)
			{
				Mat mat = new Mat(_GridMatrix.size(), _GridMatrix.type());
				_GridMatrix.copyTo(mat);
				_BufferedMats[i] = mat;
			}
			Point point = new Point(_GutterPixels, _GutterPixels);
			for (int j = 0; j < 13; j++)
			{
				for (int k = 0; k < 13; k++)
				{
					OpenCVForUnity.Rect roi = new OpenCVForUnity.Rect(new Point(point.x + (double)Mathf.FloorToInt(_Step * (float)j) + (double)_RoiInset, point.y + (double)Mathf.FloorToInt(_Step * (float)k) + (double)_RoiInset), new Size(_RoiSize - 2 * _RoiInset, _RoiSize - 2 * _RoiInset));
					_BufferedRois[j, 12 - k] = new Mat[_FrameBufferLength];
					for (int l = 0; l < _FrameBufferLength; l++)
					{
						_BufferedRois[j, 12 - k][l] = _BufferedMats[l].submat(roi);
					}
				}
			}
			OriginalBlankClassifier.InitSubmatBuffers(_BufferedMats, new Point(_GutterPixels * 0.5f, _GutterPixels * 0.5f), _Step, _CubePixels, _GutterPixels);
		}

		private static void InitRoiDataBuffer()
		{
			_RoiBuffer = new RoiData[13, 13][];
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					_RoiBuffer[j, i] = new RoiData[_FrameBufferLength];
					for (int k = 0; k < _FrameBufferLength; k++)
					{
						Mat roi = _BufferedRois[j, i][k];
						_RoiBuffer[j, i][k] = new RoiData(j, i, k, roi);
					}
				}
			}
			_CurrentPixelData = new byte[_RoiTotalDataLength];
			_CurrentHueData = new byte[_RoiPixelCount];
			_CurrentSatData = new byte[_RoiPixelCount];
			_CurrentValData = new byte[_RoiPixelCount];
		}

		public static void ResetBlockColors()
		{
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					BlockColors[j, i] = BlockColor.Blank;
				}
			}
		}

		private static void InitOffsetsFromClassiferData()
		{
			RoiOffsets offsets = ColorClassifierData.Offsets;
			_BlankFirstIndex = Mathf.FloorToInt((float)_RoiPixelCount * offsets.blankFirstIndexFactor);
			_BlankLastIndex = Mathf.FloorToInt((float)_RoiPixelCount * offsets.blankLastIndexFactor);
			_WhiteFirstIndex = Mathf.FloorToInt((float)_RoiPixelCount * offsets.whiteFirstIndexFactor);
			_WhiteLastIndex = Mathf.FloorToInt((float)_RoiPixelCount * offsets.whiteLastIndexFactor);
			_BlankColLow = Mathf.FloorToInt((float)_RoiSize * offsets.blankColLowFactor);
			_BlankColHigh = Mathf.FloorToInt((float)_RoiSize * offsets.blankColHighFactor);
			_WhiteColLow = Mathf.FloorToInt((float)_RoiSize * offsets.whiteColLowFactor);
			_WhiteColHigh = Mathf.FloorToInt((float)_RoiSize * offsets.whiteColHighFactor);
		}

		public static void CopyFrameToBuffer(int frameIndex, float cct)
		{
			Mat m = _BufferedMats[frameIndex];
			_GridMatrix.copyTo(m);
			OriginalBlankClassifier.CopyGridBrightnessMapToBuffer(frameIndex);
			_CCTs[frameIndex] = cct - (float)ColorClassifierData.MinCCTFromData;
			CaptureController.Instance.DispatchEvent(CaptureEvent.ProcessPass);
			if (isClassificationComplete(frameIndex))
			{
				CaptureController.Instance.DispatchEvent(CaptureEvent.ProcessComplete);
			}
		}

		private static void ProcessBufferedROI(RoiData roiData, int frameIndex)
		{
			Mat roi = roiData.roi;
			Utils.copyFromMat(roi, _CurrentPixelData);
			int i = 0;
			int num = 0;
			for (; i < _RoiPixelCount; i++)
			{
				_CurrentHueData[i] = _CurrentPixelData[num++];
				_CurrentSatData[i] = _CurrentPixelData[num++];
				_CurrentValData[i] = _CurrentPixelData[num++];
			}
			Array.Clear(_HueModeCounts, 0, _HueModeCounts.Length);
			Array.Clear(_HueModeTotals, 0, _HueModeTotals.Length);
			Array.Clear(_HueDistanceTotals, 0, _HueDistanceTotals.Length);
			for (int j = 0; j < _RoiPixelCount; j++)
			{
				byte hue = _CurrentHueData[j];
				byte b = _CurrentValData[j];
				if (b >= _HueBrightnessThreshLow && b <= _HueBrightnessThreshHigh)
				{
					int closestHueIndex = GetClosestHueIndex(hue, frameIndex);
					_HueModeCounts[closestHueIndex]++;
				}
			}
			int finalRoiHueIndex = GetFinalRoiHueIndex();
			byte avgHue = (byte)((float)_HueModeTotals[finalRoiHueIndex] / (float)_HueModeCounts[finalRoiHueIndex]);
			roiData.SetFinalHue(finalRoiHueIndex, avgHue);
			float num2 = 0f;
			int num3 = 0;
			int k = _BlankFirstIndex;
			int num4 = 0;
			for (; k < _BlankLastIndex; k++)
			{
				num4 = k % _RoiSize;
				if (num4 > _BlankColLow && num4 < _BlankColHigh)
				{
					num2 += (float)(int)_CurrentValData[k];
					num3++;
				}
			}
			num2 /= (float)num3;
			roiData.avgVal = (byte)num2;
			float num5 = 0f;
			num3 = 0;
			int l = _WhiteFirstIndex;
			int num6 = 0;
			for (; l < _WhiteLastIndex; l++)
			{
				num6 = l % _RoiSize;
				if (num6 > _WhiteColLow && num6 < _WhiteColHigh)
				{
					num5 += (float)(int)_CurrentSatData[l];
					num3++;
				}
			}
			num5 /= (float)num3;
			roiData.avgSat = (byte)num5;
			roiData.CalculateBlockColor();
		}

		private static int GetFinalRoiHueIndex()
		{
			int result = 0;
			int num = int.MaxValue;
			int i = 0;
			int num2 = 0;
			for (; i < _HueModeCounts.Length; i++)
			{
				int num3 = _HueModeCounts[i];
				if (num3 > num2)
				{
					num2 = num3;
					num = _HueDistanceTotals[i];
					result = i;
				}
				else if (num3 == num2)
				{
					int num4 = _HueDistanceTotals[i];
					if (num4 <= num)
					{
						num2 = num3;
						num = num4;
						result = i;
					}
				}
			}
			return result;
		}

		public static int GetClosestHueIndex(byte hue, int frameIndex)
		{
			int num = int.MaxValue;
			int num2 = 0;
			int num3 = 0;
			byte[] array = _MeasuredHues[frameIndex];
			for (int i = 0; i < array.Length - 1; i++)
			{
				byte b = array[i];
				num2 = Mathf.Abs(hue - b);
				if (num2 > 90)
				{
					num2 = 180 - num2;
				}
				if (num2 < num)
				{
					num = num2;
					num3 = i;
				}
			}
			_HueDistanceTotals[num3] += num;
			_HueModeTotals[num3] += hue;
			return num3;
		}

		public static bool ClassifyAllBlocks(bool processROI, int bufferStartIndex = 0)
		{
			bool result = false;
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					if (ClassifyBlockAtLocation(j, i, processROI, bufferStartIndex))
					{
						CaptureController.Instance.TryUpdateBlock(j, i, BlockColors[j, i]);
						result = true;
					}
				}
			}
			return result;
		}

		public static void RecordAllRoiData(CapturedFrameMetaData metaData, List<FinalPixelData> pixelDataList, double cct, int frameIndex)
		{
			int i = 0;
			int num = 0;
			for (; i < 13; i++)
			{
				int num2 = 0;
				while (num2 < 13)
				{
					Mat mat = _BufferedRois[num2, i][frameIndex];
					Utils.copyFromMat(mat, _CurrentPixelData);
					byte[] array = new byte[_RoiPixelCount];
					byte[] array2 = new byte[_RoiPixelCount];
					byte[] array3 = new byte[_RoiPixelCount];
					BlockColor blockColor = BlockColors[num2, i];
					int num3 = 0;
					for (int j = 0; j < _CurrentPixelData.Length; j += 3)
					{
						if (blockColor != BlockColor.Blank && blockColor != BlockColor.White)
						{
							byte b = _CurrentPixelData[j + 2];
							if (b < _HueBrightnessThreshLow || b > _HueBrightnessThreshHigh)
							{
								continue;
							}
						}
						array[num3] = _CurrentPixelData[j];
						array2[num3] = _CurrentPixelData[j + 1];
						array3[num3] = _CurrentPixelData[j + 2];
						num3++;
					}
					Array.Resize(ref array, num3);
					Array.Resize(ref array2, num3);
					Array.Resize(ref array3, num3);
					BlockColor blockColor2 = metaData.blockColors[num];
					FinalPixelData item = new FinalPixelData(cct, blockColor2, array, array2, array3);
					pixelDataList.Add(item);
					num2++;
					num++;
				}
			}
		}

		public static bool ClassifyBlockAtLocation(int col, int row, bool processROI, int bufferStartIndex = 0)
		{
			BlockColor blockColor = BlockColors[col, row];
			BlockColor blockColor2 = blockColor;
			Array.Clear(_BlockColorModeCounts, 0, _BlockColorModeCounts.Length);
			RoiData[] array = _RoiBuffer[col, row];
			int num = 0;
			int i = bufferStartIndex;
			int num2 = 0;
			for (; i < _FrameBufferLength; i++)
			{
				RoiData roiData = array[i];
				if (processROI)
				{
					ProcessBufferedROI(roiData, i);
				}
				else
				{
					roiData.CalculateBlockColor();
				}
				BlockColor blockColor3 = roiData.blockColor;
				num2 = ++_BlockColorModeCounts[(uint)blockColor3];
				if (num2 >= num)
				{
					num = num2;
					blockColor2 = blockColor3;
				}
			}
			BlockColors[col, row] = blockColor2;
			return blockColor != blockColor2;
		}

		public static bool SetCCTNotchOffset(int notchIndexOffset)
		{
			return ColorClassifierData.CalculateHuesFromTemp(_MeasuredHues, _CCTs, notchIndexOffset);
		}

		private static bool isClassificationComplete(int frameIndex)
		{
			bool flag = frameIndex == CaptureController.FrameBufferLength - 1;
			_onePassComplete |= flag;
			if (!_onePassComplete && frameIndex == 0)
			{
				_startProcessTime = Time.time;
			}
			float num = Time.time - _startProcessTime;
			bool flag2 = _onePassComplete && num > 0f;
			if (flag2)
			{
				_onePassComplete = false;
			}
			return flag2;
		}
	}
}
