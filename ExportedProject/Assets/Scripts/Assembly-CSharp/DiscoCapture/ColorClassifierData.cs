using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace DiscoCapture
{
	public static class ColorClassifierData
	{
		public struct ChannelData
		{
			public double[] xVals;

			public double[] yVals;

			public ChannelData(double[] rVals, double[] tVals)
			{
				if (rVals.Length != tVals.Length)
				{
					xVals = null;
					yVals = null;
					return;
				}
				if (!_ConvertFromPolarToRectangular)
				{
					xVals = rVals;
					yVals = tVals;
					return;
				}
				int num = rVals.Length;
				xVals = new double[num];
				yVals = new double[num];
				double num2 = Math.PI / 180.0;
				for (int i = 0; i < num; i++)
				{
					double num3 = rVals[i];
					double num4 = tVals[i];
					num4 *= num2;
					xVals[i] = num3 * Math.Cos(num4);
					yVals[i] = num3 * Math.Sin(num4);
				}
			}
		}

		public class CurveSegment : IEquatable<CurveSegment>
		{
			public float minCCT;

			public float maxCCT;

			public float totalHueDistanceScaled;

			public static readonly float MaxHueDistance = 1.25f;

			public static float MaxHueDistanceScaled;

			public CurveSegment(float minCCT, float maxCCT, float hueDistance)
			{
				this.minCCT = minCCT;
				this.maxCCT = maxCCT;
				totalHueDistanceScaled = hueDistance;
				if (!(totalHueDistanceScaled > MaxHueDistanceScaled))
				{
				}
			}

			public static bool IsMergable(CurveSegment segment1, CurveSegment segment2)
			{
				return segment1.totalHueDistanceScaled + segment2.totalHueDistanceScaled < MaxHueDistanceScaled;
			}

			public static CurveSegment Combine(CurveSegment segment1, CurveSegment segment2)
			{
				if (segment1 == segment2)
				{
					return null;
				}
				if (segment1.totalHueDistanceScaled + segment2.totalHueDistanceScaled > MaxHueDistanceScaled)
				{
					return null;
				}
				if (!Mathf.Approximately(segment1.maxCCT, segment2.minCCT) && !Mathf.Approximately(segment1.minCCT, segment2.maxCCT))
				{
					return null;
				}
				return new CurveSegment(Mathf.Min(segment1.minCCT, segment2.minCCT), Mathf.Max(segment1.maxCCT, segment2.maxCCT), segment1.totalHueDistanceScaled + segment2.totalHueDistanceScaled);
			}

			public override bool Equals(object obj)
			{
				if (obj == null || !(obj is CurveSegment))
				{
					return false;
				}
				CurveSegment other = obj as CurveSegment;
				return Equals(other);
			}

			public bool Equals(CurveSegment other)
			{
				return Mathf.Approximately(minCCT, other.minCCT);
			}

			public override string ToString()
			{
				return string.Format("minCCT: {0:0.000000}, maxCCT: {1:0.000000}, totalHueDistanceScaled: {2:0.000000}", minCCT, maxCCT, totalHueDistanceScaled);
			}
		}

		private static bool _InitComplete;

		private static string _StreamingAssetsRelativePath = "ColorData";

		public static string LatestServerDataFileName = "LatestColorData.json";

		private static string _StreamingAssetsFileName = "LatestColorData.json";

		private static bool _ConvertFromPolarToRectangular;

		public static int CurrentMinCCTNotchIndex;

		public static int CurrentMaxCCTNotchIndex;

		private static AnimationCurve[] _ColorCurves;

		private static CaptureServerData _ServerData;

		public static ThresholdLUT Thresholds;

		public static RoiOffsets Offsets;

		private static ChannelData[] _AllColorsChannelData;

		private static double _CCTSpan = 20.0;

		private static double _CCTScale;

		private static double _HueSpan = 20.0;

		private static double _HueScale;

		public static double OriginalCCTSpan;

		public static double MinCCTFromData;

		public static double MaxCCTFromData;

		public static float[] NotchedCCTs;

		public static float[] FinalCCTs;

		public static void Initialize()
		{
			if (!_InitComplete)
			{
				string datasetJsonFromDisk = GetDatasetJsonFromDisk();
				SetServerDataFromJSON(datasetJsonFromDisk);
				if (_ServerData.thresholds == null || !_ServerData.thresholds.initializedFromServer)
				{
					Thresholds = new ThresholdLUT();
					Thresholds.InitDefaultThresholds();
				}
				else
				{
					Thresholds = _ServerData.thresholds;
					Thresholds.InitServerThresholds();
				}
				if (_ServerData.offsets == null || !_ServerData.offsets.initializedFromServer)
				{
					_ServerData.offsets = new RoiOffsets();
					Offsets = _ServerData.offsets;
				}
				else
				{
					Offsets = _ServerData.offsets;
				}
				_InitComplete = true;
			}
		}

		private static void SetUpColorCurves()
		{
			_ColorCurves = new AnimationCurve[8];
			for (int i = 0; i < _ColorCurves.Length; i++)
			{
				SetUpCurveForBlockColor((BlockColor)i);
			}
		}

		private static void SetUpCurveForBlockColor(BlockColor blockColor)
		{
			ChannelData channelData = _AllColorsChannelData[(uint)blockColor];
			int num = channelData.xVals.Length;
			Keyframe[] array = new Keyframe[num];
			for (int i = 0; i < num; i++)
			{
				double num2 = channelData.xVals[i] * _CCTScale;
				double num3 = channelData.yVals[i] * _HueScale;
				array[i] = new Keyframe((float)num2, (float)num3);
			}
			_ColorCurves[(uint)blockColor] = new AnimationCurve(array);
		}

		public static string GetDatasetJsonFromDisk()
		{
			string json = string.Empty;
			if (!TryGettingLatestColorDataFromDisk(out json))
			{
				TryGettingColorDataFromStreamingAssets(out json);
			}
			return json;
		}

		private static bool TryGettingLatestColorDataFromDisk(out string json)
		{
			string path = Application.persistentDataPath + "/" + LatestServerDataFileName;
			if (!File.Exists(path))
			{
				json = string.Empty;
				return false;
			}
			json = File.ReadAllText(path);
			return true;
		}

		private static bool TryGettingColorDataFromStreamingAssets(out string json)
		{
			string text = Application.streamingAssetsPath + "/" + _StreamingAssetsRelativePath + "/" + _StreamingAssetsFileName;
			if (Application.platform == RuntimePlatform.Android)
			{
				WWW wWW = new WWW(text);
				while (!wWW.isDone)
				{
				}
				if (wWW.error != null)
				{
					json = null;
					return false;
				}
				json = wWW.text;
				return true;
			}
			if (!File.Exists(text))
			{
				json = string.Empty;
				return false;
			}
			json = File.ReadAllText(text);
			return true;
		}

		public static void SetServerDataFromJSON(string json)
		{
			_ServerData = JsonUtility.FromJson<CaptureServerData>(json);
			ProcessServerData();
		}

		private static void ProcessServerData()
		{
			_ServerData.CorrectHueAngleWraparound();
			ConvertDataToRectangularCoordinates();
			SetUpGraphScaling();
			SetUpColorCurves();
			InitializeNotchesForCCT();
		}

		private static void ConvertDataToRectangularCoordinates()
		{
			_AllColorsChannelData = new ChannelData[8];
			_AllColorsChannelData[0] = new ChannelData(_ServerData.red.cctVals, _ServerData.red.channelVals);
			_AllColorsChannelData[1] = new ChannelData(_ServerData.orange.cctVals, _ServerData.orange.channelVals);
			_AllColorsChannelData[2] = new ChannelData(_ServerData.yellow.cctVals, _ServerData.yellow.channelVals);
			_AllColorsChannelData[3] = new ChannelData(_ServerData.green.cctVals, _ServerData.green.channelVals);
			_AllColorsChannelData[4] = new ChannelData(_ServerData.blue.cctVals, _ServerData.blue.channelVals);
			_AllColorsChannelData[5] = new ChannelData(_ServerData.purple.cctVals, _ServerData.purple.channelVals);
			_AllColorsChannelData[6] = new ChannelData(_ServerData.pink.cctVals, _ServerData.pink.channelVals);
			_AllColorsChannelData[7] = new ChannelData(_ServerData.white.cctVals, _ServerData.white.channelVals);
		}

		private static void SetUpGraphScaling()
		{
			double num = 1000000.0;
			double num2 = -1000000.0;
			for (int i = 0; i < _AllColorsChannelData.Length; i++)
			{
				double[] yVals = _AllColorsChannelData[i].yVals;
				for (int j = 0; j < yVals.Length; j++)
				{
					if (yVals[j] < num)
					{
						num = yVals[j];
					}
					else if (yVals[j] > num2)
					{
						num2 = yVals[j];
					}
				}
			}
			double num3 = num2 - num;
			_HueScale = _HueSpan / num3;
			OriginalCCTSpan = _ServerData.maxCCT - _ServerData.minCCT;
			_CCTScale = _CCTSpan / OriginalCCTSpan;
			MinCCTFromData = _ServerData.minCCT;
			MaxCCTFromData = _ServerData.maxCCT;
		}

		private static void InitializeNotchesForCCT()
		{
			CurveSegment.MaxHueDistanceScaled = (float)((double)CurveSegment.MaxHueDistance * _HueScale);
			CurveSegment[] originalSegments = CreateSlopeSegments(1000);
			InitializeSegmentIntervals(originalSegments);
		}

		private static CurveSegment[] CreateSlopeSegments(int numSegments)
		{
			CurveSegment[] array = new CurveSegment[numSegments];
			float num = (float)(_CCTSpan / (double)numSegments);
			for (int i = 0; i < numSegments; i++)
			{
				float num2 = (float)i * num;
				float num3 = num2 + num;
				float num4 = 0f;
				for (int j = 0; j < _ColorCurves.Length; j++)
				{
					AnimationCurve animationCurve = _ColorCurves[j];
					float num5 = animationCurve.Evaluate(num2);
					float num6 = animationCurve.Evaluate(num3);
					float num7 = Mathf.Abs(num6 - num5);
					if (num7 > num4)
					{
						num4 = num7;
					}
				}
				num4 = (float)((double)num4 * _HueScale);
				array[i] = new CurveSegment(num2, num3, num4);
			}
			return array;
		}

		private static void InitializeSegmentIntervals(CurveSegment[] originalSegments)
		{
			List<CurveSegment> list = new List<CurveSegment>(originalSegments);
			List<CurveSegment> list2 = new List<CurveSegment>(list);
			int num = 0;
			bool flag = false;
			do
			{
				num = list2.Count;
				list.Sort(delegate(CurveSegment x, CurveSegment y)
				{
					float num4 = x.totalHueDistanceScaled - y.totalHueDistanceScaled;
					return (num4 != 0f) ? ((int)Mathf.Sign(num4)) : 0;
				});
				int num2 = 0;
				flag = false;
				for (int i = 0; i < num - 1; i++)
				{
					if (flag)
					{
						break;
					}
					CurveSegment curveSegment = list[i];
					int num3 = list2.IndexOf(curveSegment);
					int neighborIndex = -1;
					CurveSegment mergableNeighbor = GetMergableNeighbor(list2, num3, out neighborIndex);
					if (mergableNeighbor != null)
					{
						CurveSegment curveSegment2 = CurveSegment.Combine(curveSegment, mergableNeighbor);
						int index = Mathf.Min(num3, neighborIndex);
						int index2 = Mathf.Max(num3, neighborIndex);
						list2[index] = curveSegment2;
						list2.RemoveAt(index2);
						list.Remove(curveSegment);
						list.Remove(mergableNeighbor);
						list.Add(curveSegment2);
						flag = true;
					}
				}
			}
			while (flag);
			for (int j = 0; j < list2.Count; j++)
			{
			}
			InitNotchedCCTs(list2);
		}

		private static void InitNotchedCCTs(List<CurveSegment> segments)
		{
			int count = segments.Count;
			NotchedCCTs = new float[count];
			FinalCCTs = new float[CaptureController.FrameBufferLength];
			for (int i = 0; i < count; i++)
			{
				CurveSegment curveSegment = segments[i];
				NotchedCCTs[i] = (curveSegment.minCCT + curveSegment.maxCCT) * 0.5f;
			}
		}

		private static CurveSegment GetMergableNeighbor(List<CurveSegment> neighbors, int startingIndex, out int neighborIndex)
		{
			neighborIndex = -1;
			int count = neighbors.Count;
			CurveSegment segment = neighbors[startingIndex];
			CurveSegment curveSegment = null;
			bool flag = false;
			if (startingIndex < count - 1)
			{
				neighborIndex = startingIndex + 1;
				curveSegment = neighbors[neighborIndex];
				flag = CurveSegment.IsMergable(segment, curveSegment);
			}
			if (flag)
			{
				return curveSegment;
			}
			if (startingIndex > 0)
			{
				neighborIndex = startingIndex - 1;
				curveSegment = neighbors[neighborIndex];
				flag = CurveSegment.IsMergable(segment, curveSegment);
			}
			if (flag)
			{
				return curveSegment;
			}
			return null;
		}

		public static bool CalculateHuesFromTemp(byte[][] hues, float[] CCTs, int notchIndexOffset)
		{
			CurrentMaxCCTNotchIndex = -100000;
			CurrentMinCCTNotchIndex = 100000;
			bool flag = true;
			for (int i = 0; i < CaptureController.FrameBufferLength; i++)
			{
				float num = CCTs[i];
				float num2 = (float)((double)num * _CCTScale);
				float num3 = (float)_CCTSpan;
				int num4 = 0;
				for (int j = 0; j < NotchedCCTs.Length; j++)
				{
					float num5 = NotchedCCTs[j];
					float num6 = Mathf.Abs(num5 - num2);
					if (num6 < num3)
					{
						num3 = num6;
						num4 = j;
					}
				}
				if (num4 > CurrentMaxCCTNotchIndex)
				{
					CurrentMaxCCTNotchIndex = num4;
				}
				else if (num4 < CurrentMinCCTNotchIndex)
				{
					CurrentMinCCTNotchIndex = num4;
				}
				num4 = Mathf.Clamp(num4 + notchIndexOffset, 0, NotchedCCTs.Length - 1);
				flag &= num4 == 0 || num4 == NotchedCCTs.Length - 1;
				num2 = NotchedCCTs[num4];
				FinalCCTs[i] = (float)((double)num2 / _CCTScale + MinCCTFromData);
				byte[] array = hues[i];
				for (int k = 0; k < 8; k++)
				{
					float num7 = (float)((double)_ColorCurves[k].Evaluate(num2) / _HueScale);
					if (num7 < 0f)
					{
						num7 += 360f;
					}
					else if (num7 > 360f)
					{
						num7 -= 360f;
					}
					num7 *= 0.5f;
					array[k] = (byte)Mathf.RoundToInt(num7);
				}
			}
			return flag;
		}
	}
}
