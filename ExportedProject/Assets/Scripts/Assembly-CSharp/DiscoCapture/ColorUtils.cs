using System;
using UnityEngine;

namespace DiscoCapture
{
	public static class ColorUtils
	{
		public static readonly int Error = -999;

		private static int _RoiPixelCount;

		private static int[] _SortedHuesX = new int[324];

		private static int[] _SortedHuesY = new int[324];

		private static void ResizeUtilBuffers(int roiPixelCount)
		{
			_RoiPixelCount = roiPixelCount;
			Array.Resize(ref _SortedHuesX, _RoiPixelCount);
			Array.Resize(ref _SortedHuesY, _RoiPixelCount);
		}

		public static float GetMedianHueAngle(byte[] hues, byte[] vals, int lowValThresh, int highValThresh, int reductionFactor = 1)
		{
			int num = 0;
			float num2 = 0f;
			int num3 = hues.Length;
			int num4 = 0;
			if (reductionFactor == 1)
			{
				for (int i = 0; i < num3; i++)
				{
					num = vals[i];
					if (num >= lowValThresh && num <= highValThresh)
					{
						num2 = (float)(hues[i] * 2) * ((float)Math.PI / 180f);
						_SortedHuesX[num4] = (int)(Mathf.Cos(num2) * 10000f);
						_SortedHuesY[num4] = (int)(Mathf.Sin(num2) * 10000f);
						num4++;
					}
				}
			}
			else
			{
				for (int j = 0; j < num3; j++)
				{
					if (j % reductionFactor == 0)
					{
						num = vals[j];
						if (num >= lowValThresh && num <= highValThresh)
						{
							num2 = (float)(hues[j] * 2) * ((float)Math.PI / 180f);
							_SortedHuesX[num4] = (int)(Mathf.Cos(num2) * 10000f);
							_SortedHuesY[num4] = (int)(Mathf.Sin(num2) * 10000f);
							num4++;
						}
					}
				}
			}
			if (num4 < 2)
			{
				return Error;
			}
			Array.Sort(_SortedHuesX, 0, num4);
			Array.Sort(_SortedHuesY, 0, num4);
			int num5 = Mathf.FloorToInt((float)num4 * 0.5f);
			float num6 = 0f;
			float num7 = 0f;
			if (num4 % 2 != 0)
			{
				num6 = _SortedHuesX[num5];
				num7 = _SortedHuesY[num5];
			}
			else
			{
				num6 = (float)(_SortedHuesX[num5] + _SortedHuesX[num5 - 1]) * 0.5f;
				num7 = (float)(_SortedHuesY[num5] + _SortedHuesY[num5 - 1]) * 0.5f;
			}
			num6 *= 0.0001f;
			num7 *= 0.0001f;
			float num8 = Mathf.Atan2(num7, num6) * 57.29578f * 0.5f;
			if (num8 < 0f)
			{
				num8 += 180f;
			}
			return num8;
		}
	}
}
