using System;
using OpenCVForUnity;
using OpenCVForUnityExtended;
using UnityEngine;

namespace DiscoCapture
{
	public class CornerCorrector : MonoBehaviour
	{
		public static CornerCorrector Instance;

		private static bool _AwakeComplete;

		public bool shouldApplyClahe;

		public CLAHEModule edgeClaheHorizontal;

		public CLAHEModule edgeClaheVertical;

		public SobelModule edgeSobelModule;

		public ThresholdModule thresholdModule;

		public MorphModule edgeMorphModule;

		public MorphModule verticalMorphModule;

		public MorphModule horizontalMorphModule;

		public MorphModule floodMorphModule;

		public AdaptiveThresholdModule adaptThreshComponent;

		public CannyModule cannyComponent;

		private static int _ShortSide;

		private static int _LongSide;

		private static int _Inset;

		public static Mat[] _EdgeMats = new Mat[4];

		public static Mat[] _EdgeMatsGray = new Mat[4];

		public static Mat[] _SobelMatsX = new Mat[4];

		public static Mat[] _SobelMatsY = new Mat[4];

		public static Mat[] _SobelMatsXY = new Mat[4];

		public static Mat[] _ThresholdedMats = new Mat[4];

		public static Mat[] _MorphedMats = new Mat[4];

		public static Mat[] _FloodResltEdges = new Mat[4];

		private static byte[] _EdgePixelData;

		private static byte[] _PixelData;

		private static Mat _EdgeDisplay;

		private static Mat _CombinedMat;

		private static Mat _FloodResult;

		private static Mat _CombinedMatCenter;

		private static Mat[] _CombinedSubmats = new Mat[4];

		public static Mat _CombinedThreshold;

		private static Mat[] _CombinedThresholdSubmats = new Mat[4];

		public static bool Initialized = false;

		private static double[] xVals;

		private static double[] yVals;

		private static Point[] regressionOffsets = new Point[4];

		private static Point[] regressionLines = new Point[8]
		{
			new Point(),
			new Point(),
			new Point(),
			new Point(),
			new Point(),
			new Point(),
			new Point(),
			new Point()
		};

		private static Point[] regressionCorners = new Point[4]
		{
			new Point(),
			new Point(),
			new Point(),
			new Point()
		};

		private static int[] medianBuffer = new int[7];

		private void Awake()
		{
			if (Instance != null && Instance != this)
			{
				UnityEngine.Object.Destroy(base.gameObject);
			}
			else if (!_AwakeComplete)
			{
				Instance = this;
				_AwakeComplete = true;
			}
		}

		public static void Init(Mat source, DiscoGameBoard gameBoard)
		{
			_LongSide = source.width();
			_ShortSide = Mathf.FloorToInt((float)_LongSide * ((gameBoard.boardUnits - gameBoard.gridWidth()) / gameBoard.boardUnits));
			_Inset = Mathf.FloorToInt((float)_ShortSide / 6f);
			_EdgePixelData = new byte[(_LongSide - 2 * _Inset) * (_ShortSide - 2 * _Inset)];
			_PixelData = new byte[(_LongSide - 2 * _Inset) * (_ShortSide - 2 * _Inset)];
			xVals = new double[_LongSide - 2 * _Inset];
			yVals = new double[_LongSide - 2 * _Inset];
			int num = 2;
			_CombinedMat = new Mat(source.size(), 0);
			_CombinedThreshold = new Mat(source.size(), 0);
			_FloodResult = new Mat(source.width() + 2, source.height() + 2, 0);
			_EdgeDisplay = new Mat(_FloodResult.size(), CvType.CV_8UC4);
			_CombinedMatCenter = _CombinedMat.submat(_ShortSide - _Inset - num, _LongSide - _ShortSide + _Inset + num, _ShortSide - _Inset - num, _LongSide - _ShortSide + _Inset + num);
			SetEdgeSubmats(source, _EdgeMats);
			SetEdgeSubmats(_CombinedMat, _CombinedSubmats);
			SetEdgeSubmats(_CombinedThreshold, _CombinedThresholdSubmats);
			SetEdgeSubmats(_FloodResult, _FloodResltEdges, 1);
			regressionOffsets[0] = new Point(_Inset, _Inset);
			regressionOffsets[1] = new Point(_Inset, _Inset);
			regressionOffsets[2] = new Point(_LongSide - _ShortSide + _Inset, _Inset);
			regressionOffsets[3] = new Point(_Inset, _LongSide - _ShortSide + _Inset);
			for (int i = 0; i < _EdgeMats.Length; i++)
			{
				_EdgeMatsGray[i] = new Mat(_EdgeMats[i].width(), _EdgeMats[i].height(), CvType.CV_8UC1);
				_SobelMatsX[i] = new Mat(_EdgeMats[i].width(), _EdgeMats[i].height(), CvType.CV_8UC1);
				_SobelMatsY[i] = new Mat(_EdgeMats[i].width(), _EdgeMats[i].height(), CvType.CV_8UC1);
				_SobelMatsXY[i] = new Mat(_EdgeMats[i].width(), _EdgeMats[i].height(), CvType.CV_8UC1);
				_ThresholdedMats[i] = new Mat(_EdgeMats[i].width(), _EdgeMats[i].height(), 0);
				_MorphedMats[i] = new Mat(_EdgeMats[i].width(), _EdgeMats[i].height(), 0);
			}
			Initialized = true;
		}

		private static void SetEdgeSubmats(Mat source, Mat[] edges, int offset = 0)
		{
			edges[0] = source.submat(offset + _Inset, offset + _LongSide - _Inset, offset + _Inset, offset + _ShortSide - _Inset);
			edges[1] = source.submat(offset + _Inset, offset + _ShortSide - _Inset, offset + _Inset, offset + _LongSide - _Inset);
			edges[2] = source.submat(offset + _Inset, offset + _LongSide - _Inset, offset + _Inset + (_LongSide - _ShortSide), offset + _LongSide - _Inset);
			edges[3] = source.submat(offset + _Inset + (_LongSide - _ShortSide), offset + _LongSide - _Inset, offset + _Inset, offset + _LongSide - _Inset);
		}

		public void ProcessEdge(int index)
		{
			int num = 0;
			int num2 = 0;
			MorphModule morphModule = null;
			bool flag = false;
			if (index % 2 == 0)
			{
				num = 1;
				num2 = 0;
				morphModule = verticalMorphModule;
				flag = true;
			}
			else
			{
				num = 0;
				num2 = 1;
				morphModule = horizontalMorphModule;
			}
			Mat src = _EdgeMats[index];
			Mat mat = _EdgeMatsGray[index];
			Imgproc.cvtColor(src, mat, 7);
			if (shouldApplyClahe)
			{
				if (flag)
				{
					edgeClaheVertical.Process(mat, mat);
				}
				else
				{
					edgeClaheHorizontal.Process(mat, mat);
				}
			}
			edgeSobelModule.Process(mat, _SobelMatsX[index], num, num2);
			edgeSobelModule.Process(mat, _SobelMatsY[index], num2, num);
			Core.addWeighted(_SobelMatsX[index], 0.5, _SobelMatsY[index], 0.5, 0.0, _SobelMatsXY[index]);
			Core.convertScaleAbs(_SobelMatsXY[index], _SobelMatsXY[index]);
			thresholdModule.Process(_SobelMatsXY[index], _ThresholdedMats[index]);
			morphModule.Process(_ThresholdedMats[index], _MorphedMats[index]);
		}

		public void CombineThresholdMats()
		{
			_CombinedThreshold.setTo(new Scalar(0.0));
			for (int i = 0; i < _ThresholdedMats.Length; i++)
			{
				Core.bitwise_or(_ThresholdedMats[i], _CombinedThresholdSubmats[i], _CombinedThresholdSubmats[i]);
			}
		}

		public void CombineEdgeMats()
		{
			_CombinedMat.setTo(new Scalar(0.0));
			_CombinedMatCenter.setTo(new Scalar(255.0));
			for (int i = 0; i < _MorphedMats.Length; i++)
			{
				Core.bitwise_or(_MorphedMats[i], _CombinedSubmats[i], _CombinedSubmats[i]);
			}
		}

		public void Flood()
		{
			_FloodResult.setTo(CaptureController.ZeroScalar);
			Imgproc.floodFill(_CombinedMat, _FloodResult, new Point((float)_CombinedMat.width() / 2f, (float)_CombinedMat.height() / 2f), CaptureController.White, new OpenCVForUnity.Rect(), Scalar.all(10.0), Scalar.all(150.0), 130820);
			floodMorphModule.Process(_FloodResult, _FloodResult);
		}

		public void FinalizeEdge(int index)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = _ShortSide - 2 * _Inset;
			int num6 = _LongSide - 2 * _Inset;
			Utils.copyFromMat(_FloodResltEdges[index], _EdgePixelData);
			Array.Clear(_PixelData, 0, _PixelData.Length);
			Array.Clear(xVals, 0, xVals.Length);
			Array.Clear(yVals, 0, yVals.Length);
			switch (index)
			{
			case 0:
			{
				num3 = 0;
				num4 = num5;
				num = 0;
				num2 = num6;
				for (int m = num; m < num2; m++)
				{
					bool flag4 = false;
					for (int n = num3; n < num4; n++)
					{
						if (flag4)
						{
							break;
						}
						int num12 = m * num5 + n;
						byte b4 = _EdgePixelData[num12];
						if (flag4 = b4 > 0)
						{
							_PixelData[num12] = byte.MaxValue;
							yVals[m] = n;
							xVals[m] = m;
						}
					}
					if (!flag4)
					{
						xVals[m] = -1.0;
						yVals[m] = -1.0;
					}
				}
				break;
			}
			case 1:
			{
				num3 = 0;
				num4 = num6;
				num = 0;
				num2 = num5;
				for (int j = num3; j < num4; j++)
				{
					bool flag2 = false;
					for (int k = num; k < num2; k++)
					{
						if (flag2)
						{
							break;
						}
						int num9 = k * num6 + j;
						byte b2 = _EdgePixelData[num9];
						if (flag2 = b2 > 0)
						{
							_PixelData[num9] = byte.MaxValue;
							xVals[j] = j;
							yVals[j] = k;
						}
					}
					if (!flag2)
					{
						xVals[j] = -1.0;
						yVals[j] = -1.0;
					}
				}
				break;
			}
			case 2:
			{
				num3 = num5;
				num4 = 0;
				num = 0;
				num2 = num6;
				for (int l = num; l < num2; l++)
				{
					bool flag3 = false;
					int num10 = num3 - 1;
					while (num10 >= num4 && !flag3)
					{
						int num11 = l * num5 + num10;
						byte b3 = _EdgePixelData[num11];
						if (flag3 = b3 > 0)
						{
							_PixelData[num11] = byte.MaxValue;
							yVals[l] = num10;
							xVals[l] = l;
						}
						num10--;
					}
					if (!flag3)
					{
						xVals[l] = -1.0;
						yVals[l] = -1.0;
					}
				}
				break;
			}
			case 3:
			{
				num3 = 0;
				num4 = num6;
				num = num5;
				num2 = 0;
				for (int i = num3; i < num4; i++)
				{
					bool flag = false;
					int num7 = num - 1;
					while (num7 >= num2 && !flag)
					{
						int num8 = num7 * num6 + i;
						byte b = _EdgePixelData[num8];
						if (flag = b > 0)
						{
							_PixelData[num8] = byte.MaxValue;
							xVals[i] = i;
							yVals[i] = num7;
						}
						num7--;
					}
					if (!flag)
					{
						xVals[i] = -1.0;
						yVals[i] = -1.0;
					}
				}
				break;
			}
			}
			Utils.copyToMat(_PixelData, _FloodResltEdges[index]);
			double rsquared = 0.0;
			double yintercept = 0.0;
			double slope = 0.0;
			LinearRegression(xVals, yVals, num5, xVals.Length - num5, out rsquared, out yintercept, out slope);
			Point point = null;
			Point point2 = null;
			if (index % 2 == 0)
			{
				point = new Point(yintercept, 0.0);
				point2 = new Point(slope * (double)(num6 - 1) + yintercept, num6 - 1);
			}
			else
			{
				point = new Point(0.0, yintercept);
				point2 = new Point(num6 - 1, slope * (double)(num6 - 1) + yintercept);
			}
			Imgproc.line(_FloodResltEdges[index], point, point2, new Scalar(255.0), 3);
			regressionLines[2 * index].x = point.x + regressionOffsets[index].x;
			regressionLines[2 * index].y = point.y + regressionOffsets[index].y;
			regressionLines[2 * index + 1].x = point2.x + regressionOffsets[index].x;
			regressionLines[2 * index + 1].y = point2.y + regressionOffsets[index].y;
		}

		public void DrawRegressionCorners()
		{
			OpenCVForUnity.Rect rect = new OpenCVForUnity.Rect(0, 0, _CombinedThreshold.width(), _CombinedThreshold.height());
			for (int i = 0; i < 4; i++)
			{
				computeIntersect(regressionLines[2 * i], regressionLines[2 * i + 1], regressionLines[2 * (i + 1) % 8], regressionLines[(2 * (i + 1) + 1) % 8], regressionCorners[i]);
				if (rect.contains(regressionCorners[i]))
				{
					Imgproc.circle(_CombinedThreshold, regressionCorners[i], 5, new Scalar(255.0), 3);
				}
			}
		}

		public static void computeIntersect(Point a1, Point a2, Point b1, Point b2, Point result)
		{
			double x = a1.x;
			double y = a1.y;
			double x2 = a2.x;
			double y2 = a2.y;
			double x3 = b1.x;
			double y3 = b1.y;
			double x4 = b2.x;
			double y4 = b2.y;
			float num = (float)(x - x2) * (float)(y3 - y4) - (float)(y - y2) * (float)(x3 - x4);
			result.x = ((x * y2 - y * x2) * (x3 - x4) - (x - x2) * (x3 * y4 - y3 * x4)) / (double)num;
			result.y = ((x * y2 - y * x2) * (y3 - y4) - (y - y2) * (x3 * y4 - y3 * x4)) / (double)num;
		}

		public static void LinearRegression(double[] xVals, double[] yVals, int inclusiveStart, int exclusiveEnd, out double rsquared, out double yintercept, out double slope)
		{
			double num = 0.0;
			double num2 = 0.0;
			double num3 = 0.0;
			double num4 = 0.0;
			double num5 = 0.0;
			double num6 = 0.0;
			double num7 = 0.0;
			double num8 = 0.0;
			double num9 = exclusiveEnd - inclusiveStart;
			for (int i = inclusiveStart; i < exclusiveEnd; i++)
			{
				double num10 = xVals[i];
				double num11 = yVals[i];
				if (num10 < 0.0 || num11 < 0.0)
				{
					num9 -= 1.0;
					continue;
				}
				num7 += num10 * num11;
				num += num10;
				num2 += num11;
				num3 += num10 * num10;
				num4 += num11 * num11;
			}
			num5 = num3 - num * num / num9;
			num6 = num4 - num2 * num2 / num9;
			double num12 = num9 * num7 - num * num2;
			double d = (num9 * num3 - num * num) * (num9 * num4 - num2 * num2);
			num8 = num7 - num * num2 / num9;
			double num13 = num / num9;
			double num14 = num2 / num9;
			double num15 = num12 / Math.Sqrt(d);
			rsquared = num15 * num15;
			yintercept = num14 - num8 / num5 * num13;
			slope = num8 / num5;
		}

		public void GetApproximateEdgeLine(int index)
		{
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 2;
			int num6 = 1;
			switch (index)
			{
			case 0:
				num3 = num5;
				num4 = _ShortSide - num5;
				num = _ShortSide + num5;
				num2 = _LongSide - _ShortSide - num5;
				break;
			case 1:
				num3 = _ShortSide + num5;
				num4 = _LongSide - _ShortSide - num5;
				num = num5;
				num2 = _ShortSide - num5;
				break;
			case 2:
				num6 = -1;
				num4 = _LongSide - _ShortSide + num5;
				num3 = _LongSide - num5;
				num2 = _ShortSide + num5;
				num = _LongSide - _ShortSide - num5;
				break;
			case 3:
				num6 = -1;
				num4 = _ShortSide + num5;
				num3 = _LongSide - _ShortSide - num5;
				num2 = _LongSide - _ShortSide + num5;
				num = _LongSide - num5;
				break;
			}
			byte[] array = new byte[3];
			int[] array2 = new int[num2 - num];
			int[] array3 = new int[num2 - num];
			int num7 = num;
			int num8 = 0;
			while (num7 < num2)
			{
				bool flag = false;
				for (int i = num3; i < num4; i += num6)
				{
					if (flag)
					{
						break;
					}
					_FloodResult.get(num7, i, array);
					if (array[0] != 0)
					{
						array2[num8] = i;
						array3[num8] = num7;
						flag = true;
						break;
					}
				}
				num7 += num6;
				num8++;
			}
			_EdgeDisplay.setTo(new Scalar(0.0, 0.0, 0.0, 0.0));
			int[] array4 = new int[num2 - num];
			SlidingWindowMedian(array2, array4);
			for (int j = 3; j < array4.Length - 3; j++)
			{
				Imgproc.circle(_EdgeDisplay, new Point(array4[j], array3[j]), 7, new Scalar(0.0, 255.0, 0.0, 255.0));
			}
		}

		public void SlidingWindowMedian(int[] src, int[] dst, int windowSize = 7)
		{
			if (windowSize % 2 == 0)
			{
				windowSize++;
			}
			if (medianBuffer.Length != windowSize)
			{
				Array.Resize(ref medianBuffer, windowSize);
			}
			if (src.Length >= windowSize)
			{
				int num = Mathf.FloorToInt((float)windowSize / 2f);
				for (int i = num; i < src.Length - num; i++)
				{
					Array.Copy(src, i - num, medianBuffer, 0, windowSize);
					Array.Sort(medianBuffer);
					dst[i] = medianBuffer[num];
				}
			}
		}
	}
}
