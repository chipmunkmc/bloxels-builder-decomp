namespace DiscoCapture
{
	public enum CornerDirection
	{
		TL = 0,
		TR = 1,
		BR = 2,
		BL = 3
	}
}
