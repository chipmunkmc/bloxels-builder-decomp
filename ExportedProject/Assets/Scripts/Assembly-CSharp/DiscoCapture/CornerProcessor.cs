using System;
using System.Collections.Generic;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class CornerProcessor
	{
		private Mat _perspectiveCorrectedSubmat;

		private Mat _warp;

		private Mat _inversePerspectiveTransform;

		private Mat _cornerCorrectionMat;

		private Mat _grayMat;

		private Mat _sobelX;

		private Mat _sobelY;

		private Mat _sobelXY;

		private Mat _threshBinary;

		private Mat _cornerSubmatBrightCheck;

		private Mat _morphKernel;

		private float _threshold = 40f;

		private int _dDepth = 3;

		private int _cornerAdjustment;

		private float _extendCornerAmount;

		private float _cornerAdjustmentPercentage = 0.08f;

		private double[] _srcPointArray;

		private double[] _dstPointArray;

		private double[] _meanBuffer = new double[3];

		private Point _tlBrightCheck = new Point();

		private Point _brBrightCheck = new Point();

		private Point _tL = new Point();

		private Point _bR = new Point();

		private int _cornerOffset;

		private float _cornerMethodBrightnessThreshold = 70f;

		private Size _morphKernelSize = new Size(21.0, 21.0);

		private byte[] _cornerMatPixels = new byte[1];

		private List<int> _xMaxVals = new List<int>(16);

		private List<int> _yMaxVals = new List<int>(16);

		private Point[] _correctedSortedCorners = new Point[4]
		{
			new Point(0.0, 0.0),
			new Point(0.0, 0.0),
			new Point(0.0, 0.0),
			new Point(0.0, 0.0)
		};

		private List<MatOfPoint> _contours = new List<MatOfPoint>(128);

		private int[] _hullIntArray = new int[64];

		private float _cornerContourMinArea = 10f;

		private float _cornerContourMaxArea = 50000f;

		private float _cornerHullPolyTolerance = 15f;

		public CornerProcessor()
		{
			Init();
		}

		public void Init()
		{
			_srcPointArray = new double[8];
			_dstPointArray = new double[8]
			{
				0.0,
				0.0,
				CaptureController.PreviewHeight - 1,
				0.0,
				CaptureController.PreviewHeight - 1,
				CaptureController.PreviewHeight - 1,
				0.0,
				CaptureController.PreviewHeight - 1
			};
			_cornerAdjustment = Mathf.FloorToInt((float)CaptureController.PreviewWidth * _cornerAdjustmentPercentage);
			_cornerOffset = (int)((float)CaptureController.PreviewHeight * (ProcessorEnvironment.GameBoard.centerToCorner() - ProcessorEnvironment.GameBoard.centerToGridCorner()) / ProcessorEnvironment.GameBoard.centerToCorner() / 2f);
			_cornerSubmatBrightCheck = new Mat(new Size(_cornerAdjustment, _cornerAdjustment), CvType.CV_8UC3);
			int num = _cornerAdjustment * 2;
			_cornerCorrectionMat = new Mat(num, num, CvType.CV_8UC3);
			_warp = new Mat(CaptureController.PreviewSize, CvType.CV_8UC3);
			_grayMat = new Mat(num, num, CvType.CV_8UC1);
			_sobelX = new Mat(num, num, CvType.CV_8UC1);
			_sobelY = new Mat(num, num, CvType.CV_8UC1);
			_sobelXY = new Mat(num, num, CvType.CV_8UC1);
			_threshBinary = new Mat(num, num, CvType.CV_8UC1);
			_morphKernel = Imgproc.getStructuringElement(0, _morphKernelSize, new Point(10.0, 10.0));
		}

		public void Process()
		{
			switch (ProcessorEnvironment.DetectionType)
			{
			case ProcessorEnvironment.Type.Grid:
				ProcessorEnvironment.BoardCorners = BoardUtils.ExtendCorners(ProcessorEnvironment.GridCorners, ProcessorEnvironment.GameBoard.centerToCorner(), ProcessorEnvironment.GameBoard.centerToGridCorner());
				break;
			}
			getPerspectiveCorrectedMat(ref ProcessorEnvironment.BoardCorners);
			_cornerAdjustment = Mathf.FloorToInt((float)_perspectiveCorrectedSubmat.width() * _cornerAdjustmentPercentage);
			if (_cornerCorrectionMat.width() != _cornerAdjustment * 2)
			{
				int num = _cornerAdjustment * 2;
				_cornerCorrectionMat = new Mat(num, num, CvType.CV_8UC3);
			}
			ProcessorEnvironment.GridCorners[0] = processWarpedCorner(ref _perspectiveCorrectedSubmat, ref _cornerCorrectionMat, CornerDirection.TL);
			ProcessorEnvironment.GridCorners[1] = processWarpedCorner(ref _perspectiveCorrectedSubmat, ref _cornerCorrectionMat, CornerDirection.TR);
			ProcessorEnvironment.GridCorners[2] = processWarpedCorner(ref _perspectiveCorrectedSubmat, ref _cornerCorrectionMat, CornerDirection.BR);
			ProcessorEnvironment.GridCorners[3] = processWarpedCorner(ref _perspectiveCorrectedSubmat, ref _cornerCorrectionMat, CornerDirection.BL);
			ProcessorEnvironment.GridCorners = BoardUtils.GetInversePerspectiveCorners(ProcessorEnvironment.GridCorners, _inversePerspectiveTransform);
			ProcessorEnvironment.BoardCorners = BoardUtils.ExtendCorners(ProcessorEnvironment.GridCorners, ProcessorEnvironment.GameBoard.centerToCorner(), ProcessorEnvironment.GameBoard.centerToGridCorner());
		}

		private bool getPerspectiveCorrectedMat(ref List<Point> sortedCorners)
		{
			int num = 0;
			int num2 = 0;
			while (num < _srcPointArray.Length)
			{
				_srcPointArray[num] = sortedCorners[num2].x;
				_srcPointArray[num + 1] = sortedCorners[num2].y;
				num += 2;
				num2++;
			}
			Mat mat = new Mat(4, 1, CvType.CV_32FC2);
			Mat mat2 = new Mat(4, 1, CvType.CV_32FC2);
			mat.put(0, 0, _srcPointArray);
			mat2.put(0, 0, _dstPointArray);
			Mat perspectiveTransform = Imgproc.getPerspectiveTransform(mat, mat2);
			_inversePerspectiveTransform = Imgproc.getPerspectiveTransform(mat2, mat);
			Imgproc.warpPerspective(CaptureController.CameraMat, _warp, perspectiveTransform, new Size(CaptureController.PreviewHeight, CaptureController.PreviewHeight));
			if (_perspectiveCorrectedSubmat == null)
			{
				int num3 = CaptureController.PreviewHeight - 1;
				OpenCVForUnity.Rect roi = new OpenCVForUnity.Rect(0, 0, num3, num3);
				_perspectiveCorrectedSubmat = _warp.submat(roi);
			}
			return true;
		}

		private Point processWarpedCorner(ref Mat warpedMat, ref Mat cornerMat, CornerDirection dir)
		{
			Point point = new Point();
			switch (dir)
			{
			case CornerDirection.TL:
				point.x = _cornerOffset;
				point.y = _cornerOffset;
				break;
			case CornerDirection.TR:
				point.x = CaptureController.PreviewHeight - _cornerOffset;
				point.y = _cornerOffset;
				break;
			case CornerDirection.BR:
				point.x = CaptureController.PreviewHeight - _cornerOffset;
				point.y = CaptureController.PreviewHeight - _cornerOffset;
				break;
			case CornerDirection.BL:
				point.x = _cornerOffset;
				point.y = CaptureController.PreviewHeight - _cornerOffset;
				break;
			}
			return point;
		}

		private Point getCorrectedCorner(Mat cornerSubmat, int cornerMatSize, CornerDirection direction, Point corner)
		{
			int num = cornerMatSize * cornerMatSize;
			if (_cornerMatPixels.Length != num)
			{
				Array.Resize(ref _cornerMatPixels, num);
			}
			cornerSubmat.get(0, 0, _cornerMatPixels);
			int num2 = 6;
			_xMaxVals.Clear();
			_yMaxVals.Clear();
			byte b = 0;
			int num3 = 0;
			int num4 = 0;
			int num5 = 0;
			switch (direction)
			{
			case CornerDirection.BR:
			{
				for (int l = 0; l < (int)((float)(cornerMatSize - 1) * 0.65f); l++)
				{
					int num11 = (int)((float)(cornerMatSize - 1) * 0.65f);
					while (num11 >= 0 && num3 < num2)
					{
						b = _cornerMatPixels[l * cornerMatSize + num11];
						if (b > 0)
						{
							_xMaxVals.Add(num11);
							num3++;
							break;
						}
						num11--;
					}
				}
				num3 = 0;
				for (int m = 0; m < (int)((float)(cornerMatSize - 1) * 0.65f); m++)
				{
					int num12 = (int)((float)(cornerMatSize - 1) * 0.65f);
					while (num12 >= 0 && num3 < num2)
					{
						b = _cornerMatPixels[num12 * cornerMatSize + m];
						if (b > 0)
						{
							_yMaxVals.Add(num12);
							num3++;
							break;
						}
						num12--;
					}
				}
				num4 = ((_xMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_xMaxVals.ToArray(), _xMaxVals.Count) : _cornerAdjustment);
				return new Point(y: (double)(((_yMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_yMaxVals.ToArray(), _yMaxVals.Count) : _cornerAdjustment) - _cornerAdjustment) + corner.y, x: (double)(num4 - _cornerAdjustment) + corner.x);
			}
			case CornerDirection.TR:
			{
				for (int num9 = (int)((float)(cornerMatSize - 1) * 0.65f); num9 >= 0; num9--)
				{
					int num10 = (int)((float)(cornerMatSize - 1) * 0.65f);
					while (num10 >= 0 && num3 < num2)
					{
						b = _cornerMatPixels[num9 * cornerMatSize + num10];
						if (b > 0)
						{
							_xMaxVals.Add(num10);
							num3++;
							break;
						}
						num10--;
					}
				}
				num3 = 0;
				for (int j = 0; j < (int)((float)(cornerMatSize - 1) * 0.65f); j++)
				{
					for (int k = 0; k < (int)((float)(cornerMatSize - 1) * 0.65f); k++)
					{
						if (num3 >= num2)
						{
							break;
						}
						b = _cornerMatPixels[k * cornerMatSize + j];
						if (b > 0)
						{
							_yMaxVals.Add(k);
							num3++;
							break;
						}
					}
				}
				num4 = ((_xMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_xMaxVals.ToArray(), _xMaxVals.Count) : _cornerAdjustment);
				return new Point(y: (double)(((_yMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_yMaxVals.ToArray(), _yMaxVals.Count) : _cornerAdjustment) - _cornerAdjustment) + corner.y, x: (double)(num4 - _cornerAdjustment) + corner.x);
			}
			case CornerDirection.TL:
			{
				for (int num13 = (int)((float)(cornerMatSize - 1) * 0.65f); num13 >= 0; num13--)
				{
					for (int n = 0; n < (int)((float)(cornerMatSize - 1) * 0.65f); n++)
					{
						if (num3 >= num2)
						{
							break;
						}
						b = _cornerMatPixels[num13 * cornerMatSize + n];
						if (b > 0)
						{
							_xMaxVals.Add(n);
							num3++;
							break;
						}
					}
				}
				num3 = 0;
				for (int num14 = (int)((float)(cornerMatSize - 1) * 0.65f); num14 >= 0; num14--)
				{
					for (int num15 = 0; num15 < (int)((float)(cornerMatSize - 1) * 0.65f); num15++)
					{
						if (num3 >= num2)
						{
							break;
						}
						b = _cornerMatPixels[num15 * cornerMatSize + num14];
						if (b > 0)
						{
							_yMaxVals.Add(num15);
							num3++;
							break;
						}
					}
				}
				num4 = ((_xMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_xMaxVals.ToArray(), _xMaxVals.Count) : _cornerAdjustment);
				return new Point(y: (double)(((_yMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_yMaxVals.ToArray(), _yMaxVals.Count) : _cornerAdjustment) - _cornerAdjustment) + corner.y, x: (double)(num4 - _cornerAdjustment) + corner.x);
			}
			case CornerDirection.BL:
			{
				for (int num6 = (int)((float)(cornerMatSize - 1) * 0.65f); num6 >= 0; num6--)
				{
					for (int i = 0; i < (int)((float)(cornerMatSize - 1) * 0.65f); i++)
					{
						if (num3 >= num2)
						{
							break;
						}
						b = _cornerMatPixels[num6 * cornerMatSize + i];
						if (b > 0)
						{
							_xMaxVals.Add(i);
							num3++;
							break;
						}
					}
				}
				num3 = 0;
				for (int num7 = (int)((float)(cornerMatSize - 1) * 0.65f); num7 >= 0; num7--)
				{
					int num8 = (int)((float)(cornerMatSize - 1) * 0.65f);
					while (num8 >= 0 && num3 < num2)
					{
						b = _cornerMatPixels[num8 * cornerMatSize + num7];
						if (b > 0)
						{
							_yMaxVals.Add(num8);
							num3++;
							break;
						}
						num8--;
					}
				}
				num4 = ((_xMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_xMaxVals.ToArray(), _xMaxVals.Count) : _cornerAdjustment);
				return new Point(y: (double)(((_yMaxVals.Count != 0) ? DiscoUtils.GetMedianInt(_yMaxVals.ToArray(), _yMaxVals.Count) : _cornerAdjustment) - _cornerAdjustment) + corner.y, x: (double)(num4 - _cornerAdjustment) + corner.x);
			}
			default:
				return corner;
			}
		}

		private Point getCorrectedCornerFromConvexHull(Mat source, Point corner, CornerDirection cornerDirection)
		{
			_contours.Clear();
			MatOfPoint2f matOfPoint2f = new MatOfPoint2f();
			MatOfPoint matOfPoint = null;
			Imgproc.findContours(source, _contours, new Mat(), 0, 4);
			int count = _contours.Count;
			for (int i = 0; i < count; i++)
			{
				double num = Imgproc.contourArea(_contours[i]);
				if (num >= (double)_cornerContourMinArea && num <= (double)_cornerContourMaxArea)
				{
					matOfPoint = _contours[i];
					break;
				}
			}
			if (matOfPoint != null)
			{
				MatOfInt matOfInt = new MatOfInt();
				Imgproc.convexHull(matOfPoint, matOfInt);
				int num2 = matOfInt.rows();
				if (_hullIntArray.Length < num2)
				{
					Array.Resize(ref _hullIntArray, num2);
				}
				Utils.copyFromMat(matOfInt, _hullIntArray);
				Point[] array = new Point[num2];
				int[] array2 = new int[2];
				for (int j = 0; j < matOfInt.rows(); j++)
				{
					matOfPoint.get(_hullIntArray[j], 0, array2);
					array[j] = new Point(array2[0], array2[1]);
				}
				matOfPoint2f.fromArray(array);
				Imgproc.approxPolyDP(matOfPoint2f, matOfPoint2f, _cornerHullPolyTolerance, true);
			}
			if (matOfPoint2f.rows() == 4)
			{
				BoardUtils.SortCorners(ref _correctedSortedCorners, matOfPoint2f);
				switch (cornerDirection)
				{
				case CornerDirection.TL:
					return new Point(_correctedSortedCorners[3].x - (double)_cornerAdjustment + corner.x, _correctedSortedCorners[1].y - (double)_cornerAdjustment + corner.y);
				case CornerDirection.TR:
					return new Point(_correctedSortedCorners[2].x - (double)_cornerAdjustment + corner.x, _correctedSortedCorners[0].y - (double)_cornerAdjustment + corner.y);
				case CornerDirection.BR:
					return new Point(_correctedSortedCorners[1].x - (double)_cornerAdjustment + corner.x, _correctedSortedCorners[3].y - (double)_cornerAdjustment + corner.y);
				case CornerDirection.BL:
					return new Point(_correctedSortedCorners[0].x - (double)_cornerAdjustment + corner.x, _correctedSortedCorners[2].y - (double)_cornerAdjustment + corner.y);
				}
			}
			return corner;
		}
	}
}
