using System;
using OpenCVForUnity;

namespace DiscoCapture
{
	public class CorrelatedColorTemperature
	{
		private double[] _avgRGB = new double[4];

		private double _avgR;

		private double _avgG;

		private double _avgB;

		private double _x;

		private double _y;

		private double _z;

		private double _cct;

		public double Process(Mat mat, Mat mask = null)
		{
			if (mask == null)
			{
				Core.meanNonAlloc(mat, _avgRGB);
			}
			else
			{
				Core.meanNonAlloc(mat, mask, _avgRGB);
			}
			_avgR = _avgRGB[0];
			_avgG = _avgRGB[1];
			_avgB = _avgRGB[2];
			return rgbToTemp(_avgRGB);
		}

		private double[] colorTemperature2rgb(double kelvin)
		{
			double num = kelvin / 100.0;
			double num2 = 0.0;
			double num3 = 0.0;
			double num4 = 0.0;
			if (num < 66.0)
			{
				num2 = 255.0;
			}
			else
			{
				num2 = num - 55.0;
				num2 = 351.97690566805693 + 0.114206453784165 * num2 - 40.25366309332127 * Math.Log(num2);
				if (num2 < 0.0)
				{
					num2 = 0.0;
				}
				if (num2 > 255.0)
				{
					num2 = 255.0;
				}
			}
			if (num < 66.0)
			{
				num3 = num - 2.0;
				num3 = -155.25485562709179 - 0.44596950469579133 * num3 + 104.49216199393888 * Math.Log(num3);
				if (num3 < 0.0)
				{
					num3 = 0.0;
				}
				if (num3 > 255.0)
				{
					num3 = 255.0;
				}
			}
			else
			{
				num3 = num - 50.0;
				num3 = 325.4494125711974 + 0.07943456536662342 * num3 - 28.0852963507957 * Math.Log(num3);
				if (num3 < 0.0)
				{
					num3 = 0.0;
				}
				if (num3 > 255.0)
				{
					num3 = 255.0;
				}
			}
			if (num >= 66.0)
			{
				num4 = 255.0;
			}
			else if (num <= 20.0)
			{
				num4 = 0.0;
			}
			else
			{
				num4 = num - 10.0;
				num4 = -254.76935184120902 + 0.8274096064007395 * num4 + 115.67994401066147 * Math.Log(num4);
				if (num4 < 0.0)
				{
					num4 = 0.0;
				}
				if (num4 > 255.0)
				{
					num4 = 255.0;
				}
			}
			return new double[3]
			{
				Math.Round(num2),
				Math.Round(num3),
				Math.Round(num4)
			};
		}

		private double rgbToTemp(double[] rgb)
		{
			double num = 0.0;
			double[] array = new double[3];
			double num2 = 0.4;
			double num3 = 1000.0;
			double num4 = 40000.0;
			while (num4 - num3 > num2)
			{
				num = (num4 + num3) / 2.0;
				array = colorTemperature2rgb(num);
				if (array[2] / array[0] >= rgb[2] / rgb[0])
				{
					num4 = num;
				}
				else
				{
					num3 = num;
				}
			}
			return Math.Round(num);
		}
	}
}
