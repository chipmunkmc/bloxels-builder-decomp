using System;
using System.Threading;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class DeviceCameraController : MonoBehaviour
	{
		public delegate void HandleNatCamPreviewUpdate();

		public delegate void HandleCameraPropertiesCached();

		public delegate void HandleExposureBiasChanged(float exposureBias);

		public WebCamTexture webcamTexture;

		public DeviceResolution resolution;

		public static Mat PreviewMatrix;

		public static Color32[] PreviewColors;

		public int currentCamera;

		public WebCamDevice webCamDevice;

		private bool _hasCachedProperties;

		public static bool CameraStarted;

		public static bool CameraInitComplete;

		public static bool FilledCameraMat;

		public static int frameCount;

		private static int _framesToWait = 1;

		public event HandleNatCamPreviewUpdate OnPreviewUpdate;

		public event HandleCameraPropertiesCached OnCachedCameraProperties;

		public event HandleExposureBiasChanged OnExposureBiasChanged;

		private void Awake()
		{
			resolution = new DeviceResolution(ResolutionPreset.Medium);
			for (int i = 0; i < WebCamTexture.devices.Length; i++)
			{
				if (!WebCamTexture.devices[i].isFrontFacing)
				{
					currentCamera = i;
					webCamDevice = WebCamTexture.devices[i];
					break;
				}
			}
			webcamTexture = new WebCamTexture(webCamDevice.name, resolution.width, resolution.height);
		}

		private void OnDestroy()
		{
			webcamTexture.Stop();
		}

		private void Update()
		{
			if (!CameraStarted || webcamTexture == null || !webcamTexture.isPlaying || !webcamTexture.didUpdateThisFrame || !tryPassingSize())
			{
				return;
			}
			if (!_hasCachedProperties)
			{
				cacheCameraProperties();
				_hasCachedProperties = true;
				return;
			}
			frameCount++;
			if (frameCount > _framesToWait)
			{
				Utils.webCamTextureToMat(webcamTexture, PreviewMatrix, PreviewColors);
				CaptureController.Instance.FillCameraMatrix();
				FilledCameraMat = true;
			}
		}

		private void LateUpdate()
		{
			FilledCameraMat = false;
		}

		public void Initialize()
		{
			webcamTexture.Play();
			CameraStarted = true;
		}

		private bool tryPassingSize()
		{
			bool flag = false;
			flag = webcamTexture.width > 20 && webcamTexture.height > 20;
			return webcamTexture.width == resolution.width && webcamTexture.height == resolution.height;
		}

		private void cacheCameraProperties()
		{
			PreviewMatrix = new Mat(new Size(webcamTexture.width, webcamTexture.height), CvType.CV_8UC3);
			PreviewColors = new Color32[webcamTexture.width * webcamTexture.height];
			CaptureController.PreviewWidth = webcamTexture.width;
			CaptureController.PreviewHeight = webcamTexture.height;
			CaptureController.PreviewSize = new Size(CaptureController.PreviewWidth, CaptureController.PreviewHeight);
			CaptureController.CameraMat = new Mat(CaptureController.PreviewSize, CvType.CV_8UC3);
			if (this.OnCachedCameraProperties != null)
			{
				this.OnCachedCameraProperties();
			}
			ColorClassifierData.Initialize();
		}
	}
}
