namespace DiscoCapture
{
	public struct DeviceResolution
	{
		public ResolutionPreset preset;

		public int width;

		public int height;

		public DeviceResolution(ResolutionPreset _preset)
		{
			preset = _preset;
			DiscoUtils.Resolution(preset, out width, out height);
		}

		public override string ToString()
		{
			return preset.ToString() + " - [" + width + ", " + height + "]";
		}
	}
}
