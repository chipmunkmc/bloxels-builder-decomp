using System.Collections.Generic;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class DiscoGameBoard
	{
		private GameBoardSize size;

		private GameBoardType type;

		public List<Point> corners;

		public UnityEngine.Rect boundingRectangle { get; private set; }

		public double contourThreshold { get; private set; }

		public int cols { get; private set; }

		public int rows { get; private set; }

		public float boardUnits { get; private set; }

		public float cubeUnits { get; private set; }

		public float cubeGutter { get; private set; }

		public float boardGutter { get; private set; }

		public float whiteValThreshold { get; private set; }

		public float whiteSatThreshold { get; private set; }

		public float blackValThreshold { get; private set; }

		public float blackSatThreshold { get; private set; }

		public float cubeSizePx { get; private set; }

		public float cubeGutterPx { get; private set; }

		public float boardGutterPx { get; private set; }

		public float roiSizePx { get; private set; }

		public float roiStepPx { get; private set; }

		public DiscoGameBoard(GameBoardSize s, GameBoardType t)
		{
			size = s;
			type = t;
			switch (s)
			{
			case GameBoardSize.Square3x3:
				cols = 3;
				rows = cols;
				boardUnits = 78.5f;
				cubeUnits = 10f;
				cubeGutter = 4.4f;
				contourThreshold = 300.0;
				break;
			case GameBoardSize.Square10x10:
				cols = 10;
				rows = cols;
				boardUnits = 204f;
				cubeUnits = 10.5f;
				cubeGutter = 3.5f;
				contourThreshold = 500.0;
				break;
			case GameBoardSize.Square13x13_original:
				cols = 13;
				rows = cols;
				boardUnits = 246f;
				cubeUnits = 10.5f;
				cubeGutter = 3.5f;
				contourThreshold = 500.0;
				break;
			case GameBoardSize.Square13x13:
				cols = 13;
				rows = cols;
				boardUnits = 259.55f;
				cubeUnits = 10.49f;
				cubeGutter = 4.75f;
				contourThreshold = 500.0;
				break;
			case GameBoardSize.Square13x13_Sinatra:
				cols = 13;
				rows = cols;
				boardUnits = 259.27f;
				cubeUnits = 10.74f;
				cubeGutter = 4.92f;
				contourThreshold = 500.0;
				break;
			case GameBoardSize.Square20x20:
				cols = 20;
				rows = cols;
				boardUnits = 408f;
				cubeUnits = 10.5f;
				cubeGutter = 3.5f;
				contourThreshold = 1000.0;
				break;
			case GameBoardSize.GoldieBlue:
				cols = 13;
				rows = cols;
				boardUnits = 254f;
				cubeUnits = 6.58f;
				cubeGutter = 11.3f;
				contourThreshold = 500.0;
				break;
			case GameBoardSize.SquareMoldFinalv1_13x13:
				cols = 13;
				rows = cols;
				boardUnits = 259f;
				cubeUnits = 10.83f;
				cubeGutter = 4.7f;
				contourThreshold = 500.0;
				break;
			}
			switch (t)
			{
			case GameBoardType.physicalBlack:
				whiteValThreshold = 177f;
				whiteSatThreshold = 15f;
				blackValThreshold = 105f;
				blackSatThreshold = 85f;
				break;
			case GameBoardType.GoldieBlue:
				whiteValThreshold = 177f;
				whiteSatThreshold = 15f;
				blackValThreshold = 105f;
				blackSatThreshold = 85f;
				break;
			}
			boardGutter = (boardUnits - cubeUnits * (float)cols - cubeGutter * (float)(cols - 1)) / 2f;
		}

		public void setWhiteValue(float v)
		{
			whiteValThreshold = v;
		}

		public void setWhiteSat(float s)
		{
			whiteSatThreshold = s;
		}

		public void setBlackVal(float v)
		{
			blackValThreshold = v;
		}

		public void setBlackSat(float s)
		{
			blackSatThreshold = s;
		}

		public void setBoundingRectangle(float x, float y, float w, float h)
		{
			boundingRectangle.Set(x, y, w, h);
			roiSizePx = innerBlockSize();
			roiStepPx = roiStep();
			cubeSizePx = cubeSize();
			cubeGutterPx = stepGutter();
			boardGutterPx = gutter();
		}

		public float cubeSize()
		{
			return boundingRectangle.width / boardUnits * cubeUnits;
		}

		public float gutter()
		{
			return boundingRectangle.width / boardUnits * boardGutter;
		}

		public float stepGutter()
		{
			return boundingRectangle.width / boardUnits * cubeGutter;
		}

		public float roiStep()
		{
			return cubeSize() + stepGutter();
		}

		public void startingPoint(out float x, out float y)
		{
			x = boundingRectangle.x + boardGutterPx + roiCubeOffset();
			y = boundingRectangle.y + boundingRectangle.height - boardGutterPx - cubeSizePx + roiCubeOffset();
		}

		public float angleOffset()
		{
			return 0f;
		}

		public float innerBlockSize()
		{
			return cubeSize() * 0.4f;
		}

		public float roiCubeOffset()
		{
			return (cubeSize() - innerBlockSize()) / 2f;
		}

		public float gridWidth()
		{
			return cubeUnits * (float)cols + cubeGutter * (float)(cols - 1);
		}

		public float centerToGridCorner()
		{
			return Mathf.Sqrt(2f) * (gridWidth() / 2f);
		}

		public float centerToCorner()
		{
			return Mathf.Sqrt(2f) * (boardUnits / 2f);
		}
	}
}
