using System;

namespace DiscoCapture
{
	public class DiscoUtils
	{
		public static void Resolution(ResolutionPreset preset, out int width, out int height)
		{
			switch (preset)
			{
			case ResolutionPreset.FullHD:
				width = 1920;
				height = 1080;
				break;
			case ResolutionPreset.HD:
				width = 1280;
				height = 720;
				break;
			case ResolutionPreset.Medium:
				width = 640;
				height = 480;
				break;
			case ResolutionPreset.Highest:
				width = 9999;
				height = 9999;
				break;
			case ResolutionPreset.Lowest:
				width = 50;
				height = 50;
				break;
			default:
				width = (height = 0);
				break;
			}
		}

		public static int GetMedianInt(int[] values, int length, int reductionFactor = 1)
		{
			int num = length;
			if (reductionFactor > 1)
			{
				int num2 = 0;
				for (int i = 0; i < length; i++)
				{
					if (i % reductionFactor == 0)
					{
						values[num2] = values[i];
						num2++;
					}
				}
				num = num2;
			}
			Array.Sort(values, 0, num);
			int num3 = num / 2;
			return (num % 2 == 0) ? ((values[num3] + values[num3 - 1]) / 2) : values[num3];
		}

		public static short GetMedianShort(short[] values, int length)
		{
			Array.Sort(values, 0, length);
			int num = length / 2;
			int num2 = ((length % 2 == 0) ? ((values[num] + values[num - 1]) / 2) : values[num]);
			return (short)num2;
		}
	}
}
