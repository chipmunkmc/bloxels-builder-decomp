namespace DiscoCapture
{
	public class EdgeDetectionParams
	{
		private int _pre_medianBlur_kSize;

		private int _pre_adaptiveThresh_blockSize;

		private int _pre_adaptiveThresh_offset;

		private double _minPolyArea;

		private double _maxPolyArea;

		private double _polyApproxTolerance;

		private int _morphOps_erodeSize;

		private int _morphOps_dilateSize;

		public int pre_medianBlur_kSize
		{
			get
			{
				return _pre_medianBlur_kSize;
			}
			set
			{
				if (value % 2 == 0)
				{
					_pre_medianBlur_kSize = ((value <= 1) ? 1 : (value - 1));
				}
				else
				{
					_pre_medianBlur_kSize = value;
				}
			}
		}

		public int pre_adaptiveThresh_blockSize
		{
			get
			{
				return _pre_adaptiveThresh_blockSize;
			}
			set
			{
				if (value % 2 == 0)
				{
					_pre_adaptiveThresh_blockSize = ((value <= 1) ? 1 : (value - 1));
				}
				else
				{
					_pre_adaptiveThresh_blockSize = value;
				}
			}
		}

		public int pre_adaptiveThresh_offset { get; set; }

		public double minPolyArea { get; set; }

		public double maxPolyArea { get; set; }

		public double polyApproxTolerance
		{
			get
			{
				return _polyApproxTolerance;
			}
			set
			{
				_polyApproxTolerance = ((!(value >= 0.0)) ? 0.0 : value);
			}
		}

		public int morphOps_erodeSize
		{
			get
			{
				return _morphOps_erodeSize;
			}
			set
			{
				_morphOps_erodeSize = ((value < 1) ? 1 : value);
			}
		}

		public int morphOps_dilateSize
		{
			get
			{
				return _morphOps_dilateSize;
			}
			set
			{
				_morphOps_dilateSize = ((value < 1) ? 1 : value);
			}
		}

		public EdgeDetectionParams()
		{
			pre_medianBlur_kSize = 3;
			pre_adaptiveThresh_blockSize = 11;
			pre_adaptiveThresh_offset = 3;
			minPolyArea = 8000.0;
			maxPolyArea = 900000.0;
			polyApproxTolerance = 15.0;
			morphOps_erodeSize = 1;
			morphOps_dilateSize = 1;
		}
	}
}
