using System;
using System.Collections.Generic;
using OpenCVForUnity;

namespace DiscoCapture
{
	public class EdgeProcessor
	{
		public EdgeDetectionParams settings;

		private Mat _binaryMat;

		private Mat _fullSizeGray;

		private Mat _warpMat;

		private MatOfInt _hullInt;

		private List<MatOfPoint> _polyPoints;

		private List<MatOfPoint> _goodPolyPoints;

		private MatOfPoint2f _finalPolyPoints;

		private List<MatOfPoint> _edgeDetectionContours;

		private int[] _hullIntArray;

		private Point[] _finalPointsArray;

		private int _maxNumberOfContours = 20;

		private Size _size;

		private double[] _srcPointArray;

		private double[] _dstPointArray;

		private Point[] _correctedSortedCorners = new Point[4]
		{
			new Point(0.0, 0.0),
			new Point(0.0, 0.0),
			new Point(0.0, 0.0),
			new Point(0.0, 0.0)
		};

		public EdgeProcessor(EdgeDetectionParams edgeParams)
		{
			init(edgeParams);
		}

		private void init(EdgeDetectionParams edgeParams)
		{
			settings = edgeParams;
			_size = new Size(CaptureController.PreviewWidth, CaptureController.PreviewHeight);
			_polyPoints = new List<MatOfPoint>(_maxNumberOfContours);
			_goodPolyPoints = new List<MatOfPoint>(_maxNumberOfContours / 2);
			_finalPolyPoints = new MatOfPoint2f();
			_edgeDetectionContours = new List<MatOfPoint>(64);
			_hullIntArray = new int[64];
			_hullInt = new MatOfInt();
			_binaryMat = new Mat(_size, 0);
			_fullSizeGray = new Mat(_size, CvType.CV_8UC1);
			_warpMat = new Mat(_size, CvType.CV_8UC4);
			_srcPointArray = new double[8];
			_dstPointArray = new double[8]
			{
				0.0,
				0.0,
				_size.height - 1.0,
				0.0,
				_size.height - 1.0,
				_size.height - 1.0,
				0.0,
				_size.height - 1.0
			};
		}

		public bool Process()
		{
			Mat mat = preProcess();
			return findPolygonHull();
		}

		private Mat preProcess()
		{
			Imgproc.cvtColor(CaptureController.CameraMat, _fullSizeGray, 6);
			Imgproc.medianBlur(_fullSizeGray, _fullSizeGray, settings.pre_medianBlur_kSize);
			Imgproc.adaptiveThreshold(_fullSizeGray, _binaryMat, 255.0, 0, 1, settings.pre_adaptiveThresh_blockSize, settings.pre_adaptiveThresh_offset);
			return _binaryMat;
		}

		private bool findPolygonHull()
		{
			_edgeDetectionContours.Clear();
			Imgproc.findContours(_binaryMat, _edgeDetectionContours, new Mat(), 0, 4);
			int count = _edgeDetectionContours.Count;
			MatOfPoint matOfPoint = null;
			MatOfPoint matOfPoint2 = null;
			for (int i = 0; i < count; i++)
			{
				matOfPoint = _edgeDetectionContours[i];
				double num = Imgproc.contourArea(matOfPoint);
				if (num >= settings.minPolyArea && num <= settings.maxPolyArea)
				{
					matOfPoint2 = matOfPoint;
					break;
				}
			}
			if (matOfPoint2 == null)
			{
				return false;
			}
			_hullInt = new MatOfInt();
			Imgproc.convexHull(matOfPoint2, _hullInt);
			int num2 = _hullInt.rows();
			if (_hullIntArray.Length < num2)
			{
				Array.Resize(ref _hullIntArray, num2);
			}
			Utils.copyFromMat(_hullInt, _hullIntArray);
			_finalPointsArray = new Point[num2];
			int[] array = new int[2];
			for (int j = 0; j < num2; j++)
			{
				matOfPoint2.get(_hullIntArray[j], 0, array);
				_finalPointsArray[j] = new Point(array[0], array[1]);
			}
			_finalPolyPoints.fromArray(_finalPointsArray);
			Imgproc.approxPolyDP(_finalPolyPoints, _finalPolyPoints, settings.polyApproxTolerance, true);
			if (_finalPolyPoints.rows() == 4)
			{
				BoardUtils.SortCorners(ref _correctedSortedCorners, _finalPolyPoints);
				float num3 = BoardUtils.ComputeROIOffset(ref _correctedSortedCorners, ProcessorEnvironment.GameBoard.cubeUnits);
				_correctedSortedCorners[2].y -= num3;
				_correctedSortedCorners[3].y -= num3;
				List<Point> list = new List<Point>();
				list.Add(_correctedSortedCorners[0]);
				list.Add(_correctedSortedCorners[1]);
				list.Add(_correctedSortedCorners[2]);
				list.Add(_correctedSortedCorners[3]);
				ProcessorEnvironment.BoardCorners = list;
				return true;
			}
			return false;
		}

		private bool verifyQuad()
		{
			if (_correctedSortedCorners[0].x < 3.0 || _correctedSortedCorners[1].x > (double)(CaptureController.PreviewWidth - 3) || _correctedSortedCorners[2].x > (double)(CaptureController.PreviewHeight - 3) || _correctedSortedCorners[3].x < 3.0 || _correctedSortedCorners[0].y < 3.0 || _correctedSortedCorners[1].y < 3.0 || _correctedSortedCorners[2].y > (double)(CaptureController.PreviewHeight - 3) || _correctedSortedCorners[3].y > (double)(CaptureController.PreviewHeight - 3))
			{
				return false;
			}
			return true;
		}
	}
}
