namespace DiscoCapture
{
	public enum GameBoardSize
	{
		Square3x3 = 0,
		Square10x10 = 1,
		Square13x13 = 2,
		Square13x13_Sinatra = 3,
		Square13x13_original = 4,
		Square20x20 = 5,
		GoldieBlue = 6,
		SquareMoldFinalv1_13x13 = 7
	}
}
