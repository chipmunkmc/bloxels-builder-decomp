namespace DiscoCapture
{
	public enum GameBoardType
	{
		physicalBlack = 0,
		physicalWhite = 1,
		physicalWoodBrownDark = 2,
		GoldieBlue = 3
	}
}
