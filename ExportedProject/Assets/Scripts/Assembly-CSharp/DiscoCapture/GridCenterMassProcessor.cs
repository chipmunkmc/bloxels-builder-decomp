using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class GridCenterMassProcessor
	{
		public Mat resultMat;

		private Point _edgesMatInvCenter;

		private int _floodPixelCount;

		private float _floodPixelMaxPercentage = 0.2f;

		private float _floodPixelMinPercentage = 0.1f;

		private float _maxFloodPixels;

		private float _minFloodPixels;

		public GridCenterMassProcessor()
		{
			Init();
		}

		public void Init()
		{
			resultMat = new Mat(new Size(CaptureController.PreviewWidth + 2, CaptureController.PreviewHeight + 2), CvType.CV_8UC1);
			_edgesMatInvCenter = new Point(CaptureController.PreviewWidth / 2, CaptureController.PreviewHeight / 2);
			_maxFloodPixels = Mathf.FloorToInt((float)(CaptureController.PreviewWidth * CaptureController.PreviewHeight) * _floodPixelMaxPercentage);
			_minFloodPixels = Mathf.FloorToInt((float)(CaptureController.PreviewWidth * CaptureController.PreviewHeight) * _floodPixelMinPercentage);
		}

		public Mat Process(Mat matToProcess)
		{
			resultMat.setTo(CaptureController.ZeroScalar);
			Imgproc.circle(matToProcess, _edgesMatInvCenter, 1, CaptureController.Black, 8);
			_floodPixelCount = Imgproc.floodFill(matToProcess, resultMat, _edgesMatInvCenter, CaptureController.White, new OpenCVForUnity.Rect(), Scalar.all(10.0), Scalar.all(150.0), 130820);
			if ((float)_floodPixelCount > _maxFloodPixels)
			{
				CaptureController.Instance.claheProcessor.DecrementWindow();
			}
			else if ((float)_floodPixelCount < _minFloodPixels)
			{
				CaptureController.Instance.claheProcessor.IncrementWindow();
			}
			CaptureController.Instance.claheProcessor.AdjustDependentClipLimits();
			return resultMat;
		}
	}
}
