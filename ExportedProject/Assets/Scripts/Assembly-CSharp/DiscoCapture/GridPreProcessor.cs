using System;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using OpenCVForUnity;

namespace DiscoCapture
{
	public class GridPreProcessor
	{
		public delegate void HandleClaheMat(Mat claheMat);

		public delegate void HandleBlur(Mat blurMat);

		private Mat _grayMat;

		private Mat _threshMat;

		private Mat _sobelXMat;

		private Mat _sobelYMat;

		private Mat _sobelXYMat;

		private Mat _absGradX;

		private Mat _absGradY;

		private Mat _preProcessKernel;

		public Mat resultMat;

		private int _gaussianAmount = 3;

		private int _gaussianX;

		private int _gaussianY;

		private int _dDepth = 3;

		private float _binaryNoiseThreshold = 40f;

		private Size _morphKernelSize = new Size(7.0, 7.0);

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private HandleClaheMat m_OnClahe;

		[CompilerGenerated]
		[DebuggerBrowsable(DebuggerBrowsableState.Never)]
		private HandleBlur m_OnBlur;

		public event HandleClaheMat OnClahe
		{
			add
			{
				HandleClaheMat handleClaheMat = this.m_OnClahe;
				HandleClaheMat handleClaheMat2;
				do
				{
					handleClaheMat2 = handleClaheMat;
					handleClaheMat = Interlocked.CompareExchange(ref this.m_OnClahe, (HandleClaheMat)Delegate.Combine(handleClaheMat2, value), handleClaheMat);
				}
				while (handleClaheMat != handleClaheMat2);
			}
			remove
			{
				HandleClaheMat handleClaheMat = this.m_OnClahe;
				HandleClaheMat handleClaheMat2;
				do
				{
					handleClaheMat2 = handleClaheMat;
					handleClaheMat = Interlocked.CompareExchange(ref this.m_OnClahe, (HandleClaheMat)Delegate.Remove(handleClaheMat2, value), handleClaheMat);
				}
				while (handleClaheMat != handleClaheMat2);
			}
		}

		public event HandleBlur OnBlur
		{
			add
			{
				HandleBlur handleBlur = this.m_OnBlur;
				HandleBlur handleBlur2;
				do
				{
					handleBlur2 = handleBlur;
					handleBlur = Interlocked.CompareExchange(ref this.m_OnBlur, (HandleBlur)Delegate.Combine(handleBlur2, value), handleBlur);
				}
				while (handleBlur != handleBlur2);
			}
			remove
			{
				HandleBlur handleBlur = this.m_OnBlur;
				HandleBlur handleBlur2;
				do
				{
					handleBlur2 = handleBlur;
					handleBlur = Interlocked.CompareExchange(ref this.m_OnBlur, (HandleBlur)Delegate.Remove(handleBlur2, value), handleBlur);
				}
				while (handleBlur != handleBlur2);
			}
		}

		public GridPreProcessor()
		{
			Init();
		}

		public void Init()
		{
			Size previewSize = CaptureController.PreviewSize;
			_grayMat = new Mat(previewSize, CvType.CV_8UC1);
			_absGradX = new Mat(previewSize, CvType.CV_8UC1);
			_absGradY = new Mat(previewSize, CvType.CV_8UC1);
			_sobelXMat = new Mat(previewSize, CvType.CV_8UC1);
			_sobelYMat = new Mat(previewSize, CvType.CV_8UC1);
			_sobelXYMat = new Mat(previewSize, CvType.CV_8UC1);
			_threshMat = new Mat(previewSize, 0);
			resultMat = new Mat(previewSize, 0);
			_preProcessKernel = Imgproc.getStructuringElement(0, _morphKernelSize);
		}

		public Mat Process()
		{
			Imgproc.cvtColor(CaptureController.CameraMat, _grayMat, 7);
			CLAHEProcessor.ApplyWindowClahe(_grayMat, _grayMat);
			Imgproc.GaussianBlur(_grayMat, _grayMat, new Size(_gaussianAmount, _gaussianAmount), _gaussianX, _gaussianY, 4);
			Imgproc.Sobel(_grayMat, _sobelXMat, _dDepth, 1, 0, 3, 1.0, 0.0, 4);
			Imgproc.Sobel(_grayMat, _sobelYMat, _dDepth, 0, 1, 3, 1.0, 0.0, 4);
			Core.convertScaleAbs(_sobelXMat, _absGradX);
			Core.convertScaleAbs(_sobelYMat, _absGradY);
			Core.addWeighted(_absGradX, 0.5, _absGradY, 0.5, 0.0, _sobelXYMat);
			Imgproc.threshold(_sobelXYMat, _threshMat, _binaryNoiseThreshold, 255.0, 1);
			Imgproc.morphologyEx(_threshMat, resultMat, 2, _preProcessKernel);
			return resultMat;
		}
	}
}
