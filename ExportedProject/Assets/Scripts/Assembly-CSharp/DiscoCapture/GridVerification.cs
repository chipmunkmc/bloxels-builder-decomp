using System.Collections.Generic;
using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class GridVerification
	{
		public static Mat WarpedMat;

		private Mat _warpedThreshold;

		private Mat _warp;

		private Mat _displayX;

		private Mat _displayY;

		private Mat _sobelX;

		private Mat _sobelY;

		private Mat _sobelXY;

		private Mat _absGradX;

		private Mat _absGradY;

		private Mat _morphStructureX;

		private Mat _morphStructureY;

		private Mat _kernelX;

		private Mat _kernelY;

		private int _morphKernelSizeX = 25;

		private int _morphKernelSizeY = 31;

		private byte[] _warpedPixelsX = new byte[1];

		private byte[] _warpedPixelsY = new byte[1];

		private int _pixelBuffer;

		private float _threshold = 65f;

		private int _dDepth = 3;

		private double[] _srcPointArray;

		private double[] _dstPointArray;

		private float _extendCornerAmount = 6.5f;

		private Size _warpedSize;

		public GridVerification()
		{
			Init();
		}

		public void Init()
		{
			_srcPointArray = new double[8];
			_dstPointArray = new double[8]
			{
				0.0,
				0.0,
				CaptureController.PreviewHeight - 1,
				0.0,
				CaptureController.PreviewHeight - 1,
				CaptureController.PreviewHeight - 1,
				0.0,
				CaptureController.PreviewHeight - 1
			};
			_warpedSize = new Size(CaptureController.PreviewHeight - 1, CaptureController.PreviewHeight - 1);
			_warp = new Mat(CaptureController.PreviewSize, CvType.CV_8UC4);
			WarpedMat = new Mat(_warpedSize, CvType.CV_8UC3);
			_warpedThreshold = new Mat(_warpedSize, CvType.CV_8UC3);
			_sobelX = new Mat(_warpedSize, CvType.CV_8UC1);
			_sobelY = new Mat(_warpedSize, CvType.CV_8UC1);
			_sobelXY = new Mat(_warpedSize, CvType.CV_8UC1);
			_absGradX = new Mat(_warpedSize, CvType.CV_8UC1);
			_absGradY = new Mat(_warpedSize, CvType.CV_8UC1);
			_morphStructureY = Imgproc.getStructuringElement(2, new Size(3.0, 3.0));
			_morphStructureX = Imgproc.getStructuringElement(2, new Size(5.0, 5.0));
			_displayX = new Mat(_warpedSize, CvType.CV_8UC1);
			_displayY = new Mat(_warpedSize, CvType.CV_8UC1);
			_kernelX = Imgproc.getStructuringElement(0, new Size(1.0, _morphKernelSizeX));
			_kernelY = Imgproc.getStructuringElement(0, new Size(_morphKernelSizeY, 1.0));
			_pixelBuffer = 13;
		}

		public bool Process()
		{
			LineSegment lineSegment = new LineSegment(ProcessorEnvironment.GridCorners[0], ProcessorEnvironment.GridCorners[1]);
			LineSegment lineSegment2 = new LineSegment(ProcessorEnvironment.GridCorners[0], ProcessorEnvironment.GridCorners[1]);
			if (Mathf.Abs((float)lineSegment.slope) > 0.15f || Mathf.Abs((float)lineSegment2.slope) > 0.15f)
			{
				return false;
			}
			ProcessorEnvironment.GridCornersWithPad = BoardUtils.ExtendCorners(ProcessorEnvironment.GridCorners, ProcessorEnvironment.GameBoard.centerToGridCorner() + _extendCornerAmount, ProcessorEnvironment.GameBoard.centerToGridCorner());
			if (!getPerspectiveCorrectedMat(ref CaptureController.CameraMat, ref ProcessorEnvironment.GridCornersWithPad, ref WarpedMat))
			{
				return false;
			}
			Imgproc.cvtColor(WarpedMat, ProcessorEnvironment.HsvMat, 41);
			if (!ColorClassifier.Initialized)
			{
				ColorClassifier.Initialize(ProcessorEnvironment.HsvMat, ProcessorEnvironment.GameBoard, CaptureController.FrameBufferLength);
			}
			Imgproc.cvtColor(WarpedMat, _warpedThreshold, 7);
			CLAHEProcessor.ApplyWarpedClahe(_warpedThreshold, _warpedThreshold);
			Imgproc.Sobel(_warpedThreshold, _sobelX, _dDepth, 1, 0, 3, 1.0, 0.0, 4);
			Imgproc.Sobel(_warpedThreshold, _sobelY, _dDepth, 0, 1, 3, 1.0, 0.0, 4);
			Core.convertScaleAbs(_sobelX, _absGradX);
			Core.convertScaleAbs(_sobelY, _absGradY);
			Core.addWeighted(_absGradX, 0.5, _absGradY, 0.5, 0.0, _sobelXY);
			Imgproc.threshold(_absGradX, _displayX, _threshold, 255.0, 0);
			Imgproc.threshold(_absGradY, _displayY, _threshold, 255.0, 0);
			Imgproc.morphologyEx(_displayX, _displayX, 3, _kernelX);
			Imgproc.morphologyEx(_displayY, _displayY, 3, _kernelY);
			Imgproc.dilate(_displayX, _displayX, _morphStructureX);
			Imgproc.dilate(_displayY, _displayY, _morphStructureY);
			if (_warpedPixelsX.Length != _displayX.total())
			{
				_warpedPixelsX = new byte[_displayX.total()];
			}
			if (_warpedPixelsY.Length != _displayY.total())
			{
				_warpedPixelsY = new byte[_displayY.total()];
			}
			Utils.copyFromMat(_displayX, _warpedPixelsX);
			Utils.copyFromMat(_displayY, _warpedPixelsY);
			int num = _displayX.cols();
			int num2 = _displayX.rows();
			int num3 = 12;
			int[] array = new int[num3];
			for (int i = 0; i < num3; i++)
			{
				array[i] = _pixelBuffer + (num2 - _pixelBuffer * 2) / num3 * (i + 1);
			}
			byte b = 0;
			bool flag = false;
			int num4 = 0;
			for (int j = _pixelBuffer; j < num2 - _pixelBuffer; j++)
			{
				for (int k = _pixelBuffer; k < num - _pixelBuffer; k++)
				{
					if (flag)
					{
						break;
					}
					if (_warpedPixelsY[j * num + k] == 0)
					{
						flag = true;
					}
				}
				if (!flag)
				{
					if (j >= array[num4])
					{
						return false;
					}
					j = array[num4] - 1;
					num4++;
					if (num4 == num3)
					{
						break;
					}
				}
				flag = false;
			}
			if (num4 != num3)
			{
				return false;
			}
			b = 0;
			flag = false;
			num4 = 0;
			for (int l = _pixelBuffer; l < num - _pixelBuffer; l++)
			{
				for (int m = _pixelBuffer; m < num2 - _pixelBuffer; m++)
				{
					if (flag)
					{
						break;
					}
					if (_warpedPixelsX[m * num + l] == 0)
					{
						flag = true;
					}
				}
				if (!flag)
				{
					if (l >= array[num4])
					{
						return false;
					}
					l = array[num4] - 1;
					num4++;
					if (num4 == num3)
					{
						break;
					}
				}
				flag = false;
			}
			if (num4 != num3)
			{
				return false;
			}
			return true;
		}

		private bool getPerspectiveCorrectedMat(ref Mat sourceMat, ref List<Point> sortedCorners, ref Mat destinationMat)
		{
			int num = sourceMat.width();
			int num2 = sourceMat.height();
			int num3 = 0;
			int num4 = 0;
			while (num3 < _srcPointArray.Length)
			{
				_srcPointArray[num3] = sortedCorners[num4].x;
				_srcPointArray[num3 + 1] = sortedCorners[num4].y;
				num3 += 2;
				num4++;
			}
			Mat mat = new Mat(4, 1, CvType.CV_32FC2);
			Mat mat2 = new Mat(4, 1, CvType.CV_32FC2);
			mat.put(0, 0, _srcPointArray);
			mat2.put(0, 0, _dstPointArray);
			Mat perspectiveTransform = Imgproc.getPerspectiveTransform(mat, mat2);
			Imgproc.warpPerspective(sourceMat, _warp, perspectiveTransform, new Size(num, num2));
			float num5 = num2 - 1;
			OpenCVForUnity.Rect roi = new OpenCVForUnity.Rect(0, 0, (int)num5, (int)num5);
			destinationMat = _warp.submat(roi);
			return true;
		}
	}
}
