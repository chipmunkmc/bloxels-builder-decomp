using OpenCVForUnity;
using UnityEngine;

namespace DiscoCapture
{
	public class OriginalBlankClassifier
	{
		private static Mat _RoiMask;

		private static Mat[,] _RoiMasks;

		private static Mat[,][] _BufferedRois;

		public static bool Initialized = false;

		public static byte[,][] GridBrighnessMap;

		private static double[] meanBuffer = new double[3];

		public static void InitSubmatBuffers(Mat[] bufferedMats, Point startingPoint, float step, float cubePixels, float gutterPixels)
		{
			int num = bufferedMats.Length;
			int num2 = bufferedMats[0].height();
			_BufferedRois = new Mat[13, 13][];
			_RoiMasks = new Mat[13, 13];
			_RoiMask = new Mat(num2, num2, 0);
			_RoiMask.setTo(new Scalar(0.0));
			GridBrighnessMap = new byte[13, 13][];
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					GridBrighnessMap[i, j] = new byte[num];
				}
			}
			for (int k = 0; k < 14; k++)
			{
				OpenCVForUnity.Rect roi = new OpenCVForUnity.Rect((int)(startingPoint.x + (double)(step * (float)k)), 0, 1, num2);
				Mat mat = _RoiMask.submat(roi);
				mat.setTo(new Scalar(255.0));
			}
			float num3 = gutterPixels * 0.45f;
			float num4 = cubePixels + gutterPixels + num3 * 2f;
			for (int l = 0; l < 13; l++)
			{
				for (int m = 0; m < 13; m++)
				{
					OpenCVForUnity.Rect roi2 = new OpenCVForUnity.Rect((int)(startingPoint.x + (double)(step * (float)l) - (double)num3), (int)(startingPoint.y + (double)(step * (float)m) - (double)num3), (int)num4, (int)num4);
					Mat[] array = new Mat[num];
					for (int n = 0; n < num; n++)
					{
						array[n] = bufferedMats[n].submat(roi2);
					}
					_BufferedRois[l, 12 - m] = array;
					_RoiMasks[l, 12 - m] = _RoiMask.submat(roi2);
				}
			}
			Initialized = true;
		}

		public static void CopyGridBrightnessMapToBuffer(int frameIndex)
		{
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					Mat src = _BufferedRois[j, i][frameIndex];
					Mat mask = _RoiMasks[j, i];
					Core.meanNonAlloc(src, mask, meanBuffer);
					GridBrighnessMap[j, i][frameIndex] = (byte)meanBuffer[2];
				}
			}
		}

		public static bool ClassifyBlankAtLocation(int col, int row, int frameIndex, byte avgVal)
		{
			byte b = GridBrighnessMap[col, row][frameIndex];
			int num = ColorClassifierData.Thresholds.originalBlankValThresholds[b];
			if (num < 80)
			{
				num = 80;
			}
			else if (num > 252)
			{
				num = 252;
			}
			return avgVal < num;
		}

		public static void UpdateGradientDisplayTex(Texture2D texture, int frameIndex)
		{
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					float num = (float)(int)GridBrighnessMap[i, j][frameIndex] / 255f;
					texture.SetPixel(i, j, new Color(num, num, num, 1f));
				}
			}
			texture.Apply();
		}
	}
}
