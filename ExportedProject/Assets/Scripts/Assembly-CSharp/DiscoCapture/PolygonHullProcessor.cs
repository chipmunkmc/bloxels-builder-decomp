using System;
using System.Collections.Generic;
using OpenCVForUnity;

namespace DiscoCapture
{
	public class PolygonHullProcessor
	{
		private List<MatOfPoint> _contours;

		private int[] _hullIntArray;

		private int _minArea = 8000;

		private int _maxArea = 900000;

		private double _polyTolerance = 15.0;

		private MatOfPoint2f _finalPolyPoints;

		private MatOfPoint _bestContour;

		public PolygonHullProcessor()
		{
			Init();
		}

		public void Init()
		{
			_contours = new List<MatOfPoint>(128);
			_hullIntArray = new int[64];
			_finalPolyPoints = new MatOfPoint2f();
			_bestContour = new MatOfPoint();
		}

		public MatOfPoint2f Process(Mat matToProcess)
		{
			_contours.Clear();
			_bestContour = new MatOfPoint();
			Imgproc.findContours(matToProcess, _contours, new Mat(), 0, 4);
			int count = _contours.Count;
			int num = 0;
			for (int i = 0; i < count; i++)
			{
				int num2 = (int)Imgproc.contourArea(_contours[i]);
				if (num2 > num && num2 >= _minArea && num2 <= _maxArea)
				{
					num = num2;
					_bestContour = _contours[i];
				}
			}
			if (_bestContour.rows() == 0)
			{
				return null;
			}
			MatOfInt matOfInt = new MatOfInt();
			Imgproc.convexHull(_bestContour, matOfInt);
			int num3 = matOfInt.rows();
			if (_hullIntArray.Length < num3)
			{
				Array.Resize(ref _hullIntArray, num3);
			}
			Utils.copyFromMat(matOfInt, _hullIntArray);
			Point[] array = new Point[num3];
			int[] array2 = new int[2];
			for (int j = 0; j < num3; j++)
			{
				_bestContour.get(_hullIntArray[j], 0, array2);
				array[j] = new Point(array2[0], array2[1]);
			}
			_finalPolyPoints.fromArray(array);
			Imgproc.approxPolyDP(_finalPolyPoints, _finalPolyPoints, _polyTolerance, true);
			return _finalPolyPoints;
		}
	}
}
