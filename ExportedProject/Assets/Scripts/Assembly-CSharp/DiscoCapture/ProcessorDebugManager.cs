using System;
using System.Collections.Generic;
using System.Diagnostics;
using OpenCVForUnity;
using UnityEngine;
using UnityEngine.UI;

namespace DiscoCapture
{
	public class ProcessorDebugManager : MonoBehaviour
	{
		public static ProcessorDebugManager Instance;

		private static bool _InitComplete;

		public RectTransform windowContainer;

		public Button uiButtonCreate;

		public Button uiButtonColors;

		public ColorVisualizer colorVisualizer;

		public GameObject debugWindowPrefab;

		private static Dictionary<ProcessOp, ProcessorDebugWindow> _DisplayWindows;

		private static Mat _LineMat;

		private static byte[] _LineData;

		private void Awake()
		{
			UnityEngine.Object.Destroy(uiButtonCreate.gameObject);
			UnityEngine.Object.Destroy(base.gameObject);
		}

		[Conditional("__NEVER_DEFINED__")]
		private void Start()
		{
			uiButtonCreate.onClick.AddListener(CreateNewDebugWindow);
			uiButtonColors.onClick.AddListener(CaptureController.Instance.StartClassification);
		}

		[Conditional("__NEVER_DEFINED__")]
		public void UpdateColorDisplay(bool darken = false)
		{
			colorVisualizer.UpdateTexture2D(darken);
		}

		[Conditional("__NEVER_DEFINED__")]
		public void UpdateColorDisplayForBlock(int col, int row)
		{
			colorVisualizer.UpdateBlockInTexture2D(col, row);
		}

		private void CreateNewDebugWindow()
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(debugWindowPrefab);
			ProcessorDebugWindow component = gameObject.GetComponent<ProcessorDebugWindow>();
			component.selfTransform.SetParent(windowContainer);
			component.selfTransform.SetAsLastSibling();
			component.selfTransform.localScale = Vector3.one;
			component.selfTransform.anchoredPosition = Vector2.zero;
		}

		[Conditional("__NEVER_DEFINED__")]
		private void OnDestroy()
		{
			if (Instance != null && Instance == this)
			{
				Instance = null;
				_InitComplete = false;
				_DisplayWindows = null;
			}
		}

		[Conditional("__NEVER_DEFINED__")]
		public static void RegisterPreviewDisplay(ProcessOp processOp, ProcessorDebugWindow displayWindow)
		{
			ProcessorDebugWindow value = null;
			if (_DisplayWindows.TryGetValue(processOp, out value))
			{
				if (value != null)
				{
					value.Close();
				}
				_DisplayWindows.Remove(processOp);
			}
			_DisplayWindows.Remove(displayWindow.currentOp);
			displayWindow.currentOp = processOp;
			_DisplayWindows[processOp] = displayWindow;
		}

		[Conditional("__NEVER_DEFINED__")]
		public static void TryUpdateDisplay(ProcessOp processOp, Mat src)
		{
			ProcessorDebugWindow value = null;
			if (_DisplayWindows.TryGetValue(processOp, out value))
			{
				value.UpdateDisplay(src);
			}
		}

		[Conditional("__NEVER_DEFINED__")]
		public static void PrepareGridVerificationForCurrentFrame(int matSize)
		{
			if (_LineMat == null || _LineMat.width() != matSize)
			{
				_LineMat = new Mat(matSize, matSize, 0);
				_LineData = new byte[matSize * matSize];
			}
			Array.Clear(_LineData, 0, _LineData.Length);
		}

		[Conditional("__NEVER_DEFINED__")]
		public static void ExtendCurrentLineByOnePixel(int index)
		{
			_LineData[index] = byte.MaxValue;
		}

		[Conditional("__NEVER_DEFINED__")]
		public static void TryUpdateVerificationLinesDisplay()
		{
			ProcessorDebugWindow value = null;
			if (_DisplayWindows.TryGetValue(ProcessOp.GridVerify_LineMat, out value))
			{
				Utils.copyToMat(_LineData, _LineMat);
				value.UpdateDisplay(_LineMat);
			}
		}
	}
}
