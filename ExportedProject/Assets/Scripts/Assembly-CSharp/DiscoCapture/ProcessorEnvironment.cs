using System.Collections.Generic;
using OpenCVForUnity;

namespace DiscoCapture
{
	public class ProcessorEnvironment
	{
		public enum Type
		{
			Grid = 0,
			Edge = 1
		}

		public static bool FlipCamera = false;

		public static Type DetectionType = Type.Grid;

		public static DiscoGameBoard GameBoard;

		public static Mat HsvMat;

		public static List<Point> GridCorners = new List<Point>
		{
			new Point(),
			new Point(),
			new Point(),
			new Point()
		};

		public static List<Point> GridCornersWithPad = new List<Point>
		{
			new Point(),
			new Point(),
			new Point(),
			new Point()
		};

		public static List<Point> BoardCorners = new List<Point>
		{
			new Point(),
			new Point(),
			new Point(),
			new Point()
		};
	}
}
