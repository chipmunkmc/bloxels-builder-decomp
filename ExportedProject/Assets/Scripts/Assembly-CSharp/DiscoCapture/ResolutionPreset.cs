namespace DiscoCapture
{
	public enum ResolutionPreset
	{
		FullHD = 0,
		HD = 1,
		Medium = 2,
		Highest = 3,
		Lowest = 4
	}
}
