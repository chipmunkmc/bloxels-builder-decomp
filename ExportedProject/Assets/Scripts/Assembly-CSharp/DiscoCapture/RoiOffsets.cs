using System;

namespace DiscoCapture
{
	[Serializable]
	public class RoiOffsets
	{
		public bool initializedFromServer;

		public float blankFirstIndexFactor = 0.25f;

		public float blankLastIndexFactor = 0.75f;

		public float whiteFirstIndexFactor = 0.5f;

		public float whiteLastIndexFactor = 5f / 6f;

		public float blankColLowFactor = 0.475f;

		public float blankColHighFactor = 0.525f;

		public float whiteColLowFactor;

		public float whiteColHighFactor = 1f;

		public override string ToString()
		{
			return string.Format("From server? {0}\nBlank First Index Factor = {1}\nBlank Last Index Factor = {2}\nWhite First Index Factor = {3}\nWhite Last Index Factor = {4}\nBlank Col Low Factor = {5}\nBlank Col High Factor = {6}\nWhite Col Low Factor = {7}\nWhite Col High Factor = {8}", initializedFromServer, blankFirstIndexFactor, blankLastIndexFactor, whiteFirstIndexFactor, whiteLastIndexFactor, blankColLowFactor, blankColHighFactor, whiteColLowFactor, whiteColHighFactor);
		}
	}
}
