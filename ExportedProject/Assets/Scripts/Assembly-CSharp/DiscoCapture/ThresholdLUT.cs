using System;
using UnityEngine;

namespace DiscoCapture
{
	[Serializable]
	public class ThresholdLUT
	{
		public bool initializedFromServer;

		public int[] originalBlankValThresholds;

		public byte[] originalWhiteSatThresholds;

		[NonSerialized]
		public byte[] adjustedWhiteSatThresholds;

		private static byte _DefaultWhiteSatThresh = 32;

		[NonSerialized]
		public int currentWhiteSatOffset;

		public static int CCTIndexCount;

		public void InitDefaultThresholds()
		{
			InitBlankDefaults();
			InitWhiteDefaults();
		}

		public void InitServerThresholds()
		{
			int num = originalWhiteSatThresholds.Length;
			if (num % 256 != 0)
			{
				InitDefaultThresholds();
				return;
			}
			CCTIndexCount = num / 256;
			adjustedWhiteSatThresholds = new byte[num];
			for (int i = 0; i < num; i++)
			{
				adjustedWhiteSatThresholds[i] = originalWhiteSatThresholds[i];
			}
		}

		public byte GetWhiteSatThreshold(int frameIndex, int col, int row)
		{
			float num = ColorClassifierData.FinalCCTs[frameIndex];
			byte b = OriginalBlankClassifier.GridBrighnessMap[col, row][frameIndex];
			int value = Mathf.FloorToInt((float)(((double)num - ColorClassifierData.MinCCTFromData) / (ColorClassifierData.MaxCCTFromData - ColorClassifierData.MinCCTFromData) * (double)CCTIndexCount));
			value = Mathf.Clamp(value, 0, CCTIndexCount - 1);
			return adjustedWhiteSatThresholds[value * 256 + b];
		}

		private void InitBlankDefaults()
		{
			originalBlankValThresholds = new int[256];
			for (int i = 0; i < originalBlankValThresholds.Length; i++)
			{
				originalBlankValThresholds[i] = (int)((float)i + (float)i / 255f * -5f);
			}
		}

		public void InitBlankWithNewOffsetFactor(int offsetFactor)
		{
			for (int i = 0; i < originalBlankValThresholds.Length; i++)
			{
				originalBlankValThresholds[i] = (int)((float)i + (float)i / 255f * (float)offsetFactor);
			}
		}

		private void InitWhiteDefaults()
		{
			CCTIndexCount = 100;
			originalWhiteSatThresholds = new byte[256 * CCTIndexCount];
			adjustedWhiteSatThresholds = new byte[256 * CCTIndexCount];
			for (int i = 0; i < originalWhiteSatThresholds.Length; i++)
			{
				originalWhiteSatThresholds[i] = _DefaultWhiteSatThresh;
				adjustedWhiteSatThresholds[i] = _DefaultWhiteSatThresh;
			}
		}

		public bool AdjustWhiteSatThresholds(int adjustmentAmount)
		{
			bool flag = false;
			adjustmentAmount = Mathf.Clamp(adjustmentAmount, -1, 1);
			currentWhiteSatOffset += adjustmentAmount;
			for (int i = 0; i < originalWhiteSatThresholds.Length; i++)
			{
				byte b = adjustedWhiteSatThresholds[i];
				byte b2 = (byte)Mathf.Clamp(originalWhiteSatThresholds[i] + currentWhiteSatOffset, 0, 255);
				adjustedWhiteSatThresholds[i] = b2;
				flag = flag || b != b2;
			}
			if (!flag)
			{
				currentWhiteSatOffset -= adjustmentAmount;
			}
			return flag;
		}
	}
}
