using UnityEngine;
using UnityEngine.EventSystems;

namespace DiscoData
{
	public class BoardCorner : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
	{
		private RectTransform _cornerRectTransform;

		private bool _isDragging;

		private void Awake()
		{
			_cornerRectTransform = GetComponent<RectTransform>();
		}

		public void OnBeginDrag(PointerEventData eventData)
		{
			_isDragging = true;
			AdjustCornerPosition(eventData);
		}

		public void OnDrag(PointerEventData eventData)
		{
			AdjustCornerPosition(eventData);
			ChoosyCornerController.Instance.SortCorners();
		}

		public void OnEndDrag(PointerEventData eventData)
		{
			_isDragging = false;
			ChoosyCornerController.Instance.SortCorners();
		}

		private void AdjustCornerPosition(PointerEventData eventData)
		{
			Vector2 localPoint = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(ChoosyCornerController.Instance.displayImage.rectTransform, eventData.position, Camera.main, out localPoint);
			Vector2 localPoint2 = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(ChoosyCornerController.Instance.displayImage.rectTransform, eventData.position - eventData.delta, Camera.main, out localPoint2);
			Vector2 vector = localPoint - localPoint2;
			_cornerRectTransform.anchoredPosition += vector;
		}
	}
}
