namespace DiscoData
{
	public class CaptureData
	{
		public int numBlankAsBlocks;

		public int numBlocksAsBlank;

		public int numWhiteAsColor;

		public int numColorAsWhite;

		public int numColorsMisclassified;

		public double percentError;

		public override string ToString()
		{
			return string.Format("NumBlankAsBlocks: {0}\nNumBlocksAsBlank: {1}\n<color=white><b>NumWhiteAsColor: {2}</b></color>\n<color=white><b>NumColorAsWhite: {3}</b></color>\nNumColorsMislassified: {4}\n<color=red>Percent Error: {5:0.00}</color>", numBlankAsBlocks, numBlocksAsBlank, numWhiteAsColor, numColorAsWhite, numColorsMisclassified, percentError);
		}
	}
}
