using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using OpenCVForUnity;
using UnityEngine;
using UnityEngine.UI;

namespace DiscoData
{
	public class ChoosyCornerController : MonoBehaviour
	{
		public Camera cornerCamera;

		public RawImage[] cornerZoomDisplays;

		private Texture2D[] cornerZoomTextures;

		public GameObject loadingOverlay;

		public GameObject savingOverlay;

		public Button uiButtonResetCorners;

		public Button uiButtonSaveMetaData;

		public Button uiButtonDelete;

		public Button uiButtonToggleBlockDisplay;

		public CapturedFrameMetaData currentMetaData = new CapturedFrameMetaData();

		public static ChoosyCornerController Instance;

		private static bool _InitComplete;

		public Canvas canvas;

		public RectTransform[] corners;

		public RawImage displayImage;

		private Texture2D _displayTexture;

		public RawImage perspectiveCorrectedDisplayImage;

		private Texture2D _perspectiveCorrectedDisplayTexture;

		public string imagesDirectoryPath;

		private string _currentRootDirectoryPath;

		private string[] _imagePaths;

		private string[] _URLs;

		private Coroutine _downloadCoroutine;

		private int _currentPathIndex;

		private Mat _originalMat;

		private Mat _displayMat;

		private Mat _perspectiveCorrectedMat;

		private Mat _perspectiveSrc;

		private Mat _perspectiveDst;

		private float[] _sortedCornerArray = new float[8];

		private Color32[] _perspectiveCorrectedColors;

		private int _originalWidth;

		private int _originalHeight;

		private int _perspCorrectedWidth;

		private int _perspCorrectedHeight;

		public RectTransform blockColorDisplay;

		private CanvasGroup _blockColorCanvasGroup;

		public RectTransform gameBoardBlockPrefab;

		private Image[,] blockDisplayImages;

		private Dictionary<BlockColor, Color> _blockColorToUnityColor = new Dictionary<BlockColor, Color>
		{
			{
				BlockColor.Red,
				Color.red
			},
			{
				BlockColor.Orange,
				Color.Lerp(Color.red, Color.yellow, 0.5f)
			},
			{
				BlockColor.Yellow,
				Color.yellow
			},
			{
				BlockColor.Green,
				Color.green
			},
			{
				BlockColor.Blue,
				Color.blue
			},
			{
				BlockColor.Purple,
				new Color(28f / 51f, 22f / 85f, 0.95686275f)
			},
			{
				BlockColor.Pink,
				new Color(0.95686275f, 22f / 85f, 0.83137256f)
			},
			{
				BlockColor.White,
				Color.white
			},
			{
				BlockColor.Blank,
				Color.black
			}
		};

		private CLAHE _clahe;

		private double _claheClip;

		private RenderTexture renderTexture;

		private void Awake()
		{
			if (Instance != null && Instance != this)
			{
				UnityEngine.Object.Destroy(base.gameObject);
			}
			else if (!_InitComplete)
			{
				Instance = this;
				Init(640, 480);
				_clahe = Imgproc.createCLAHE(_claheClip, new Size(8.0, 6.0));
				uiButtonResetCorners.onClick.AddListener(ResetCorners);
				uiButtonSaveMetaData.onClick.AddListener(SaveMetaData);
				uiButtonDelete.onClick.AddListener(Delete);
				uiButtonToggleBlockDisplay.onClick.AddListener(ToggleBlockDisplay);
			}
		}

		private IEnumerator Start()
		{
			yield return new WaitForEndOfFrame();
			GetAllImageUrls();
		}

		private void Init(int width, int height)
		{
			if (_originalWidth != width || _originalHeight != height)
			{
				_originalWidth = width;
				_originalHeight = height;
				_perspCorrectedWidth = height;
				_perspCorrectedHeight = height;
				InitPerspectiveCorrection();
				InitDisplays();
				_InitComplete = true;
			}
		}

		private void InitPerspectiveCorrection()
		{
			float[] array = null;
			if (_InitComplete)
			{
				Imgproc.resize(_perspectiveCorrectedMat, _perspectiveCorrectedMat, new Size(_perspCorrectedHeight, _perspCorrectedHeight));
				array = new float[8] { 0f, 0f, _perspCorrectedHeight, 0f, _perspCorrectedHeight, _perspCorrectedHeight, 0f, _perspCorrectedHeight };
				_perspectiveDst.put(0, 0, array);
			}
			else
			{
				_perspectiveCorrectedMat = new Mat(new Size(_perspCorrectedHeight, _perspCorrectedHeight), CvType.CV_8UC3);
				_perspectiveSrc = new Mat(4, 1, CvType.CV_32FC2);
				_perspectiveDst = new Mat(4, 1, CvType.CV_32FC2);
				array = new float[8] { 0f, 0f, _perspCorrectedHeight, 0f, _perspCorrectedHeight, _perspCorrectedHeight, 0f, _perspCorrectedHeight };
				_perspectiveDst.put(0, 0, array);
			}
		}

		private void InstantiateBlockColorsDisplay()
		{
			blockDisplayImages = new Image[13, 13];
			_blockColorCanvasGroup = blockColorDisplay.GetComponent<CanvasGroup>();
			float num = 15.53f;
			Vector2 vector = new Vector2(30.905f, 30.905f);
			Vector2 sizeDelta = new Vector2(10.83f, 10.83f);
			blockColorDisplay.sizeDelta = new Vector2(259f, 259f);
			float num2 = 1.8532819f;
			blockColorDisplay.localScale = new Vector3(num2, num2, num2);
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					Vector2 anchoredPosition = vector + new Vector2((float)j * num, (float)i * num);
					RectTransform rectTransform = UnityEngine.Object.Instantiate(gameBoardBlockPrefab);
					rectTransform.SetParent(blockColorDisplay, true);
					rectTransform.localScale = Vector3.one;
					rectTransform.sizeDelta = sizeDelta;
					rectTransform.anchoredPosition = anchoredPosition;
					Image component = rectTransform.GetComponent<Image>();
					component.color = Color.black;
					blockDisplayImages[j, i] = component;
				}
			}
		}

		private void UpdateBlockColorsDisplayFromMetaData()
		{
			int i = 0;
			int num = 0;
			for (; i < 13; i++)
			{
				int num2 = 0;
				while (num2 < 13)
				{
					Image image = blockDisplayImages[num2, i];
					Color color = _blockColorToUnityColor[currentMetaData.blockColors[num]];
					image.color = color;
					num2++;
					num++;
				}
			}
		}

		private void ToggleBlockDisplay()
		{
			_blockColorCanvasGroup.alpha = ((int)_blockColorCanvasGroup.alpha + 1) % 2;
		}

		private void InitDisplays()
		{
			InitDisplayScaling();
			if (_InitComplete)
			{
				Imgproc.resize(_originalMat, _originalMat, new Size(_originalWidth, _originalHeight));
				Imgproc.resize(_displayMat, _displayMat, new Size(_originalWidth, _originalHeight));
				_displayTexture.Resize(_originalWidth, _originalHeight, TextureFormat.RGB24, false);
				_displayTexture.filterMode = FilterMode.Point;
				_perspectiveCorrectedDisplayTexture.Resize(_perspCorrectedHeight, _perspCorrectedHeight, TextureFormat.RGB24, false);
				_perspectiveCorrectedDisplayTexture.filterMode = FilterMode.Point;
				Array.Resize(ref _perspectiveCorrectedColors, _perspCorrectedHeight * _perspCorrectedHeight);
			}
			else
			{
				InitRenderTexture();
				InstantiateBlockColorsDisplay();
				_originalMat = new Mat(new Size(_originalWidth, _originalHeight), CvType.CV_8UC3);
				_displayMat = new Mat(new Size(_originalWidth, _originalHeight), CvType.CV_8UC3);
				_displayTexture = new Texture2D(_originalWidth, _originalHeight, TextureFormat.RGB24, false);
				_displayTexture.filterMode = FilterMode.Point;
				displayImage.texture = _displayTexture;
				_perspectiveCorrectedDisplayTexture = new Texture2D(_perspCorrectedHeight, _perspCorrectedHeight, TextureFormat.RGB24, false);
				_perspectiveCorrectedDisplayTexture.filterMode = FilterMode.Point;
				perspectiveCorrectedDisplayImage.texture = _perspectiveCorrectedDisplayTexture;
				_perspectiveCorrectedColors = new Color32[_perspCorrectedHeight * _perspCorrectedHeight];
				SetDisplayTexturesToBlack();
			}
		}

		public void InitDisplayScaling()
		{
			float num = 640f / (float)_originalWidth;
			float num2 = 1f / num;
			displayImage.rectTransform.sizeDelta = new Vector2(_originalWidth, _originalHeight);
			displayImage.rectTransform.localScale = new Vector3(num, num, 1f);
			for (int i = 0; i < corners.Length; i++)
			{
				RectTransform rectTransform = corners[i];
				rectTransform.localScale = new Vector3(num2, num2, 1f);
			}
		}

		private void SetDisplayTexturesToBlack()
		{
			_originalMat.setTo(new Scalar(0.0, 0.0, 0.0));
			Utils.fastMatToTexture2D(_originalMat, _displayTexture);
			_perspectiveCorrectedMat.setTo(new Scalar(0.0, 0.0, 0.0));
			UpdateWarpDisplay();
		}

		private void UpdateWarpDisplay()
		{
			Utils.matToTexture2D(_perspectiveCorrectedMat, _perspectiveCorrectedDisplayTexture, _perspectiveCorrectedColors);
			RenderZoomedInCorners();
		}

		private void LoadDirectory(string rootDirectoryPath, bool recursive)
		{
			if (Directory.Exists(rootDirectoryPath))
			{
				_currentRootDirectoryPath = rootDirectoryPath;
				SearchOption searchOption = (recursive ? SearchOption.AllDirectories : SearchOption.TopDirectoryOnly);
				_imagePaths = Directory.GetFiles(rootDirectoryPath, "*.png", searchOption);
				if (_imagePaths == null || _imagePaths.Length == 0)
				{
					SetDisplayTexturesToBlack();
					_currentPathIndex = -1;
				}
				else
				{
					LoadFromDiskAtIndex(0);
				}
			}
		}

		private void IncrementDiskPathIndex()
		{
			LoadFromDiskAtIndex(_currentPathIndex + 1);
		}

		private void DecrementDiskPathIndex()
		{
			LoadFromDiskAtIndex(_currentPathIndex - 1);
		}

		private void LoadFromDiskAtIndex(int index)
		{
			if (_imagePaths == null || _imagePaths.Length == 0 || index < 0 || index >= _imagePaths.Length)
			{
				return;
			}
			string path = _imagePaths[index];
			if (File.Exists(path))
			{
				byte[] array = File.ReadAllBytes(path);
				if (array != null)
				{
					_displayTexture.LoadImage(array);
					Utils.fastTexture2DToMat(_displayTexture, _originalMat);
					List<Mat> list = new List<Mat>(4);
					Core.split(_originalMat, list);
					List<Mat> list2 = new List<Mat>(3);
					list2.Add(list[1]);
					list2.Add(list[2]);
					list2.Add(list[3]);
					Core.merge(list2, _originalMat);
					_currentPathIndex = index;
				}
			}
		}

		public void HandleImageDeleted()
		{
			if (_currentPathIndex >= _URLs.Length || _currentPathIndex < 0)
			{
			}
			List<string> list = new List<string>(_URLs);
			string item = _URLs[_currentPathIndex];
			list.Remove(item);
			_URLs = list.ToArray();
			LoadImageFromS3AtIndex(_currentPathIndex);
		}

		private void Delete()
		{
			if (_currentPathIndex >= _URLs.Length || _currentPathIndex < 0)
			{
			}
			List<string> list = new List<string>(_URLs);
			string text = _URLs[_currentPathIndex];
			list.Remove(text);
			_URLs = list.ToArray();
			string jsonURL = text.Replace(".jpg", ".json");
			S3Interface.instance.DeleteCaptureData(text, jsonURL);
			LoadImageFromS3AtIndex(_currentPathIndex);
		}

		private void SaveMetaData()
		{
			if (_URLs != null && _URLs.Length != 0 && _currentPathIndex >= 0 && _currentPathIndex < _URLs.Length)
			{
				UpdateMetaDataFromCorners();
				string text = _URLs[_currentPathIndex];
				savingOverlay.SetActive(true);
				currentMetaData.SaveToS3(text);
				StartCoroutine(S3Interface.instance.MarkCaptureScreenComplete(text));
			}
		}

		private void GetAllImageUrls()
		{
			StartCoroutine(S3Interface.instance.GetAllCaptureImages(delegate(List<string> response)
			{
				List<string> list = new List<string>(64);
				for (int i = 0; i < response.Count; i++)
				{
					string text = response[i].Substring(response[i].Length - 4, 4);
					if (!(text == "json") && !(text == "lete"))
					{
						list.Add(response[i]);
					}
				}
				_URLs = new string[list.Count];
				list.CopyTo(_URLs);
				System.Random random = new System.Random();
				int num = _URLs.Length;
				while (num > 1)
				{
					int num2 = random.Next(num--);
					string text2 = _URLs[num];
					_URLs[num] = _URLs[num2];
					_URLs[num2] = text2;
				}
				LoadImageFromS3AtIndex(0);
			}));
		}

		public void IncrementS3UrlIndex()
		{
			int num = 1;
			if (Input.GetKey(KeyCode.LeftShift))
			{
				num = 5;
			}
			LoadImageFromS3AtIndex(_currentPathIndex + num);
		}

		public void DecrementUrlIndex()
		{
			int num = 1;
			if (Input.GetKey(KeyCode.LeftShift))
			{
				num = 5;
			}
			LoadImageFromS3AtIndex(_currentPathIndex - num);
		}

		private void LoadImageFromS3AtIndex(int index)
		{
			if (_URLs != null && _URLs.Length != 0 && index >= 0 && index < _URLs.Length)
			{
				if (_downloadCoroutine != null)
				{
					StopCoroutine(_downloadCoroutine);
					_downloadCoroutine = null;
				}
				string text = _URLs[index];
				string text2 = text.Substring(text.Length - 4, 4);
				_currentPathIndex = index;
				_downloadCoroutine = StartCoroutine(DownloadImage(text));
			}
		}

		private IEnumerator DownloadImage(string url)
		{
			loadingOverlay.SetActive(true);
			using (WWW www = new WWW(url))
			{
				yield return www;
				if (www.error != null)
				{
					loadingOverlay.SetActive(false);
					_downloadCoroutine = null;
					yield break;
				}
				Init(www.texture.width, www.texture.height);
				www.LoadImageIntoTexture(_displayTexture);
				Utils.fastTexture2DToMat(_displayTexture, _originalMat);
				_originalMat.copyTo(_displayMat);
			}
			yield return StartCoroutine(currentMetaData.LoadFromS3(url));
			loadingOverlay.SetActive(false);
			_claheClip = 0.0;
			_clahe.setClipLimit(_claheClip);
			UpdateCornersFromMetaData();
			UpdateBlockColorsDisplayFromMetaData();
			_downloadCoroutine = null;
		}

		public void ResetCorners()
		{
			float num = (float)_originalHeight / 4f;
			Vector2 vector = new Vector2((float)_originalWidth / 2f, (float)(-_originalHeight) / 2f);
			corners[0].anchoredPosition = vector + new Vector2(0f - num, num);
			corners[1].anchoredPosition = vector + new Vector2(num, num);
			corners[2].anchoredPosition = vector + new Vector2(num, 0f - num);
			corners[3].anchoredPosition = vector + new Vector2(0f - num, 0f - num);
			SortCorners();
		}

		public Vector2 FindCenterOfPoints()
		{
			Vector2 anchoredPosition = corners[0].anchoredPosition;
			Vector2 anchoredPosition2 = corners[1].anchoredPosition;
			Vector2 anchoredPosition3 = corners[2].anchoredPosition;
			Vector2 anchoredPosition4 = corners[3].anchoredPosition;
			float x = (anchoredPosition.x + anchoredPosition2.x + anchoredPosition3.x + anchoredPosition4.x) / 4f;
			float y = (anchoredPosition.y + anchoredPosition2.y + anchoredPosition3.y + anchoredPosition4.y) / 4f;
			return new Vector2(x, y);
		}

		private void WarpFromCurrentCorners()
		{
			int num = 0;
			int num2 = 0;
			while (num < 8)
			{
				_sortedCornerArray[num] = corners[num2].anchoredPosition.x;
				_sortedCornerArray[num + 1] = 0f - corners[num2].anchoredPosition.y;
				num += 2;
				num2++;
			}
			_perspectiveSrc.put(0, 0, _sortedCornerArray);
			using (Mat m = Imgproc.getPerspectiveTransform(_perspectiveSrc, _perspectiveDst))
			{
				Imgproc.warpPerspective(_originalMat, _perspectiveCorrectedMat, m, new Size(_perspCorrectedWidth, _perspCorrectedHeight));
			}
			UpdateWarpDisplay();
		}

		public void SortCorners()
		{
			Vector2 vector = FindCenterOfPoints();
			Array.Sort(corners, delegate(RectTransform x, RectTransform y)
			{
				float x2 = x.anchoredPosition.x;
				float x3 = y.anchoredPosition.x;
				if (x2 < x3)
				{
					return -1;
				}
				return (x2 != x3) ? 1 : 0;
			});
			RectTransform rectTransform = corners[0];
			RectTransform rectTransform2 = corners[1];
			if (rectTransform.anchoredPosition.y < rectTransform2.anchoredPosition.y)
			{
				rectTransform2 = corners[0];
				rectTransform = corners[1];
			}
			RectTransform rectTransform3 = corners[2];
			RectTransform rectTransform4 = corners[3];
			if (rectTransform3.anchoredPosition.y < rectTransform4.anchoredPosition.y)
			{
				rectTransform4 = corners[2];
				rectTransform3 = corners[3];
			}
			corners[0] = rectTransform;
			corners[1] = rectTransform3;
			corners[2] = rectTransform4;
			corners[3] = rectTransform2;
			WarpFromCurrentCorners();
		}

		private void UpdateCornersFromMetaData()
		{
			for (int i = 0; i < 4; i++)
			{
				corners[i].anchoredPosition = new Vector2(currentMetaData.cornerData[2 * i], 0f - currentMetaData.cornerData[2 * i + 1]);
			}
			WarpFromCurrentCorners();
		}

		private void UpdateMetaDataFromCorners()
		{
			for (int i = 0; i < 4; i++)
			{
				currentMetaData.cornerData[2 * i] = corners[i].anchoredPosition.x;
				currentMetaData.cornerData[2 * i + 1] = 0f - corners[i].anchoredPosition.y;
			}
		}

		private void OnDestroy()
		{
			if (renderTexture != null)
			{
				renderTexture.Release();
			}
		}

		private void InitRenderTexture()
		{
			cornerCamera.aspect = 1f;
			int num = 256;
			renderTexture = new RenderTexture(num, num, -1, RenderTextureFormat.ARGB32, RenderTextureReadWrite.Default);
			renderTexture.Create();
			cornerCamera.targetTexture = renderTexture;
			cornerZoomTextures = new Texture2D[4];
			for (int i = 0; i < cornerZoomDisplays.Length; i++)
			{
				Texture2D texture2D = new Texture2D(num, num, TextureFormat.ARGB32, false);
				texture2D.filterMode = FilterMode.Point;
				texture2D.wrapMode = TextureWrapMode.Clamp;
				cornerZoomTextures[i] = texture2D;
				cornerZoomDisplays[i].texture = texture2D;
			}
		}

		public void RenderZoomedInCorners()
		{
			RenderTexture.active = renderTexture;
			for (int i = 0; i < corners.Length; i++)
			{
				cornerCamera.transform.position = corners[i].position + new Vector3(0f, 0f, -1f);
				cornerCamera.Render();
				cornerZoomTextures[i].ReadPixels(new UnityEngine.Rect(0f, 0f, 512f, 512f), 0, 0, false);
				cornerZoomTextures[i].Apply();
			}
			RenderTexture.active = null;
		}

		public void IncreaseContrast()
		{
			_claheClip += 0.1;
			_clahe.setClipLimit(_claheClip);
			ApplyClahe();
			Utils.fastMatToTexture2D(_displayMat, _displayTexture);
			RenderZoomedInCorners();
		}

		public void DecreaseContrast()
		{
			_claheClip -= 0.1;
			if (_claheClip <= 0.0)
			{
				_claheClip = 0.0;
				_clahe.setClipLimit(_claheClip);
				_originalMat.copyTo(_displayMat);
				Utils.fastMatToTexture2D(_displayMat, _displayTexture);
			}
			else
			{
				_clahe.setClipLimit(_claheClip);
				ApplyClahe();
				Utils.fastMatToTexture2D(_displayMat, _displayTexture);
				RenderZoomedInCorners();
			}
		}

		private void ApplyClahe()
		{
			using (Mat mat = new Mat(_originalMat.size(), CvType.CV_8UC3))
			{
				Imgproc.cvtColor(_originalMat, mat, 41);
				List<Mat> list = new List<Mat>(3);
				Core.split(mat, list);
				_clahe.apply(list[2], list[2]);
				Core.merge(list, mat);
				Imgproc.cvtColor(mat, _displayMat, 55);
			}
		}
	}
}
