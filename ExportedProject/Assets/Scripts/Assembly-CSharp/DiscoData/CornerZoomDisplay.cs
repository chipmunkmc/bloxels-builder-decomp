using UnityEngine;
using UnityEngine.EventSystems;

namespace DiscoData
{
	public class CornerZoomDisplay : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
	{
		private RectTransform _selfRect;

		private RectTransform _cornerRect;

		public int cornerIndex;

		private void Awake()
		{
			_selfRect = GetComponent<RectTransform>();
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			_cornerRect = ChoosyCornerController.Instance.corners[cornerIndex];
			Vector2 localPoint = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(_selfRect, eventData.position, Camera.main, out localPoint);
			localPoint = new Vector2(localPoint.x / _selfRect.sizeDelta.x, localPoint.y / _selfRect.sizeDelta.y);
			Vector2 zero = Vector2.zero;
			zero = ((localPoint.y > localPoint.x) ? ((!(localPoint.y > 1f - localPoint.x)) ? Vector2.left : Vector2.up) : ((!(localPoint.y > 1f - localPoint.x)) ? Vector2.down : Vector2.right));
			_cornerRect.anchoredPosition += zero;
			ChoosyCornerController.Instance.SortCorners();
		}
	}
}
