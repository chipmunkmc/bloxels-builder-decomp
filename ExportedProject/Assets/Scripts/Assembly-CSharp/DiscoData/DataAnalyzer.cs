using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using DiscoCapture;
using OpenCVForUnity;
using UnityEngine;
using UnityEngine.UI;

namespace DiscoData
{
	public class DataAnalyzer : MonoBehaviour
	{
		public struct DataTotals
		{
			public int totalBlankAsBlocks;

			public int totalBlocksAsBlank;

			public DataTotals(int blocksAsBlank, int blanksAsBlocks)
			{
				totalBlocksAsBlank = blocksAsBlank;
				totalBlankAsBlocks = blanksAsBlocks;
			}
		}

		public Button uiButtonDownloadAll;

		public Button uiButtonAnalyzeAll;

		public Button uiButtonAnalyzeAndRecord;

		public S3DownloadInterstitial downloadInterstitial;

		public CaptureImageLoader imageLoader;

		public static List<FinalPixelData> ColorCurveData = new List<FinalPixelData>(2048);

		public Text statusText;

		private HashSet<string> fileLookup = new HashSet<string>();

		private Coroutine _analyzerRoutine;

		private string _directoryPath;

		public PixelDataVisualizer visualizer;

		private double totalPercentages;

		private int totalImages;

		private int totalBlankAsBlocks;

		private int totalBlocksAsBlank;

		private Dictionary<int, DataTotals> blankData = new Dictionary<int, DataTotals>();

		private List<string> skippedFiles = new List<string>(16);

		private double maxCCT;

		private double minCCT;

		private static Mat _BoardMarginMask;

		private double[] _sortedCornerArray = new double[8];

		private Size _perspectiveCorrectedSize = new Size(480.0, 480.0);

		private Mat _perspectiveCorrectedMat;

		private Mat _perspectiveSrc;

		private Mat _perspectiveDst;

		private int _perspCorrectedHeight = 480;

		private int _perspCorrectedWidth = 480;

		public AnimationCurve curve;

		public string[] URLs { get; private set; }

		private IEnumerator Start()
		{
			yield return new WaitForSeconds(5f);
			uiButtonDownloadAll.onClick.AddListener(ShowDownloadInterstitial);
			uiButtonAnalyzeAll.onClick.AddListener(AnalyzeAll);
			uiButtonAnalyzeAndRecord.onClick.AddListener(AnalyzeAllAndRecord);
			Initialize();
		}

		private void Initialize()
		{
			GetAllUrls();
			downloadInterstitial.Init(this, imageLoader);
			InitializeCapture();
		}

		public void ShowDownloadInterstitial()
		{
			downloadInterstitial.gameObject.SetActive(true);
		}

		private void GetAllUrls()
		{
			StartCoroutine(S3Interface.instance.GetAllCaptureImages(delegate(List<string> responseURLs)
			{
				List<string> list = new List<string>(64);
				for (int i = 0; i < responseURLs.Count; i++)
				{
					string text = responseURLs[i].Substring(responseURLs[i].Length - 4, 4);
					if (text == "lete")
					{
						string text2 = responseURLs[i];
						string item = text2.Replace(".jpg.complete", ".json");
						if (responseURLs.Contains(item))
						{
							list.Add(text2);
						}
					}
				}
				URLs = list.ToArray();
				uiButtonDownloadAll.gameObject.SetActive(true);
			}));
		}

		public void SetStatusText(string text)
		{
			statusText.text = text;
		}

		public void AnalyzeAll()
		{
			if (_analyzerRoutine != null)
			{
			}
			_directoryPath = Application.dataPath + CaptureImageLoader.SavePath.Substring(0, CaptureImageLoader.SavePath.Length - 1);
			if (Directory.Exists(_directoryPath))
			{
				string[] files = Directory.GetFiles(_directoryPath);
				PopulateFileDicitonaty(files);
				_analyzerRoutine = StartCoroutine(AnalyzeAllDistributed(false));
			}
		}

		public void AnalyzeAllAndRecord()
		{
			if (_analyzerRoutine != null)
			{
			}
			_directoryPath = Application.dataPath + CaptureImageLoader.SavePath.Substring(0, CaptureImageLoader.SavePath.Length - 1);
			if (Directory.Exists(_directoryPath))
			{
				string[] files = Directory.GetFiles(_directoryPath);
				PopulateFileDicitonaty(files);
				_analyzerRoutine = StartCoroutine(AnalyzeAllDistributed(true));
			}
		}

		private void InitPerspectiveCorrection()
		{
			float[] array = null;
			_perspectiveCorrectedMat = new Mat(new Size(_perspCorrectedHeight, _perspCorrectedHeight), CvType.CV_8UC3);
			InitBoardMarginMask();
			_perspectiveSrc = new Mat(4, 1, CvType.CV_32FC2);
			_perspectiveDst = new Mat(4, 1, CvType.CV_32FC2);
			array = new float[8] { 0f, 0f, _perspCorrectedHeight, 0f, _perspCorrectedHeight, _perspCorrectedHeight, 0f, _perspCorrectedHeight };
			_perspectiveDst.put(0, 0, array);
		}

		private void InitBoardMarginMask()
		{
			_BoardMarginMask = Mat.zeros(new Size(_perspCorrectedHeight, _perspCorrectedHeight), 0);
			int num = 5;
			int num2 = _BoardMarginMask.width();
			int num3 = _BoardMarginMask.height();
			Mat mat = _BoardMarginMask.submat(num, num3 - num, num, num2 - num);
			mat.setTo(new Scalar(255.0));
			int num4 = 40 + num;
			Mat mat2 = _BoardMarginMask.submat(num4, num3 - num4, num4, num2 - num4);
			mat2.setTo(new Scalar(0.0));
		}

		private void WarpFromCurrentCorners()
		{
			int num = 0;
			int num2 = 0;
			while (num < 8)
			{
				_sortedCornerArray[num] = ProcessorEnvironment.GridCornersWithPad[num2].x;
				_sortedCornerArray[num + 1] = ProcessorEnvironment.GridCornersWithPad[num2].y;
				num += 2;
				num2++;
			}
			_perspectiveSrc.put(0, 0, _sortedCornerArray);
			using (Mat m = Imgproc.getPerspectiveTransform(_perspectiveSrc, _perspectiveDst))
			{
				Imgproc.warpPerspective(CaptureController.CameraMat, _perspectiveCorrectedMat, m, _perspectiveCorrectedSize);
			}
		}

		private IEnumerator FindBestBlankOffsetFactor()
		{
			blankData.Clear();
			for (int i = -10; i < 10; i++)
			{
				ColorClassifierData.Thresholds.InitBlankWithNewOffsetFactor(i);
				yield return StartCoroutine(AnalyzeAllDistributed(false));
				blankData.Add(i, new DataTotals(totalBlocksAsBlank, totalBlankAsBlocks));
			}
			int bestOffset2 = 0;
			int minCount = 10000;
			Dictionary<int, DataTotals>.Enumerator dataEnumerator = blankData.GetEnumerator();
			while (dataEnumerator.MoveNext())
			{
				int key = dataEnumerator.Current.Key;
				DataTotals value = dataEnumerator.Current.Value;
				int num = value.totalBlankAsBlocks + value.totalBlocksAsBlank;
				if (num < minCount)
				{
					minCount = num;
					bestOffset2 = key;
				}
			}
			_analyzerRoutine = null;
		}

		private IEnumerator AnalyzeAllDistributed(bool recordData)
		{
			ColorCurveData.Clear();
			SetStatusText("Analyzing all images on disk...");
			maxCCT = double.NegativeInfinity;
			minCCT = double.PositiveInfinity;
			totalPercentages = 0.0;
			totalImages = 0;
			totalBlankAsBlocks = 0;
			totalBlocksAsBlank = 0;
			skippedFiles.Clear();
			HashSet<string>.Enumerator fileEnumerator = fileLookup.GetEnumerator();
			while (fileEnumerator.MoveNext())
			{
				AnalyzeImage(fileEnumerator.Current, recordData);
				yield return new WaitForEndOfFrame();
			}
			SetStatusText("Analysis complete. Skipped frames: " + skippedFiles.Count);
			for (int i = 0; i < skippedFiles.Count; i++)
			{
			}
			if (recordData)
			{
				Visualize();
				ExportJSON();
			}
		}

		private void Visualize()
		{
			double num = 100000000.0;
			double num2 = -100000000.0;
			foreach (FinalPixelData colorCurveDatum in ColorCurveData)
			{
				double cct = colorCurveDatum.cct;
				if (cct < num)
				{
					num = cct;
				}
				else if (cct > num2)
				{
					num2 = cct;
				}
			}
			double colorTempSpan = num2 - num;
			visualizer.PlotData(ColorCurveData, BlockColor.White, num, colorTempSpan);
		}

		private void ExportJSON()
		{
			FinalPixelDataCollection obj = new FinalPixelDataCollection(ColorCurveData);
			string contents = JsonUtility.ToJson(obj);
			File.WriteAllText(Application.dataPath + "/AllTheCaptureJSON.json", contents);
		}

		private void InitializeCapture()
		{
			CaptureController.PreviewWidth = 640;
			CaptureController.PreviewHeight = 480;
			CaptureController.PreviewSize = new Size(CaptureController.PreviewWidth, CaptureController.PreviewHeight);
			CaptureController.CameraMat = new Mat(CaptureController.PreviewSize, CvType.CV_8UC3);
			CaptureController.FrameBufferLength = 1;
			ProcessorEnvironment.BoardCorners = new List<Point>(4);
			CaptureController.Instance.InitProcessors();
			InitPerspectiveCorrection();
			ColorClassifierData.Initialize();
		}

		private void AnalyzeImage(string filePath, bool recordData)
		{
			CapturedFrameMetaData capturedFrameMetaData = new CapturedFrameMetaData();
			capturedFrameMetaData.LoadFromDisk(filePath + ".json");
			if (!capturedFrameMetaData.hasBlockColorData || !imageLoader.LoadImageFromDiskIntoMat(filePath + ".jpg", CaptureController.CameraMat) || Mathf.Approximately(capturedFrameMetaData.cornerData[0], 320f))
			{
				return;
			}
			capturedFrameMetaData.imageName = filePath;
			ProcessImage(capturedFrameMetaData, recordData);
			CaptureData captureData = null;
			if (!capturedFrameMetaData.CompareBlockColors(ColorClassifier.BlockColors, out captureData))
			{
				return;
			}
			totalImages++;
			totalPercentages += captureData.percentError;
			totalBlankAsBlocks += captureData.numBlankAsBlocks;
			totalBlocksAsBlank += captureData.numBlocksAsBlank;
			if (!(captureData.percentError > 20.0))
			{
				return;
			}
			StringBuilder stringBuilder = new StringBuilder(200);
			for (int num = 12; num >= 0; num--)
			{
				for (int i = 0; i < 13; i++)
				{
					stringBuilder.Append((int)ColorClassifier.BlockColors[i, num] + ", ");
				}
				stringBuilder.Append('\n');
			}
		}

		private void ProcessImage(CapturedFrameMetaData metaData, bool recordData)
		{
			ProcessorEnvironment.BoardCorners.Clear();
			for (int i = 0; i < 4; i++)
			{
				ProcessorEnvironment.BoardCorners.Add(new Point(metaData.cornerData[2 * i], metaData.cornerData[2 * i + 1]));
			}
			ProcessorEnvironment.GridCornersWithPad = BoardUtils.ExtendCorners(ProcessorEnvironment.BoardCorners, ProcessorEnvironment.GameBoard.centerToGridCorner() + 6.5f, ProcessorEnvironment.GameBoard.centerToCorner());
			WarpFromCurrentCorners();
			Imgproc.cvtColor(_perspectiveCorrectedMat, ProcessorEnvironment.HsvMat, 41);
			if (!ColorClassifier.Initialized)
			{
				ColorClassifier.Initialize(ProcessorEnvironment.HsvMat, ProcessorEnvironment.GameBoard, 1);
			}
			double num = CaptureController.Instance.cctProcessor.Process(_perspectiveCorrectedMat, _BoardMarginMask);
			if (double.IsInfinity(num) || double.IsNaN(num))
			{
				skippedFiles.Add(metaData.imageName);
				return;
			}
			if (num < minCCT)
			{
				minCCT = num;
			}
			else if (num > maxCCT)
			{
				maxCCT = num;
			}
			ColorClassifier.CopyFrameToBuffer(0, (float)num);
			ColorClassifier.SetCCTNotchOffset(0);
			ColorClassifier.ClassifyAllBlocks(true);
			if (recordData)
			{
				ColorClassifier.RecordAllRoiData(metaData, ColorCurveData, num, 0);
			}
		}

		private void PopulateFileDicitonaty(string[] files)
		{
			fileLookup.Clear();
			foreach (string text in files)
			{
				string item = text.Substring(0, text.IndexOf('.'));
				fileLookup.Add(item);
			}
		}
	}
}
