using System;

namespace DiscoData
{
	[Serializable]
	public class FinalPixelData
	{
		public byte blockColor;

		public double cct;

		public byte[] channel1;

		public byte[] channel2;

		public byte[] channel3;

		public FinalPixelData(double cct, BlockColor blockColor, byte[] ch1, byte[] ch2, byte[] ch3)
		{
			this.blockColor = (byte)blockColor;
			this.cct = cct;
			channel1 = ch1;
			channel2 = ch2;
			channel3 = ch3;
		}
	}
}
