using System;
using System.Collections.Generic;

namespace DiscoData
{
	[Serializable]
	public class FinalPixelDataCollection
	{
		public FinalPixelData[] data;

		public FinalPixelDataCollection(List<FinalPixelData> data)
		{
			this.data = data.ToArray();
		}
	}
}
