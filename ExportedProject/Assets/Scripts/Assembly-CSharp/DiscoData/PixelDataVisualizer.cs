using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace DiscoData
{
	public class PixelDataVisualizer : MonoBehaviour
	{
		public static readonly int ChannelCount = 3;

		public static readonly int TextureWidth = 4096;

		public static readonly int TextureHeight = 256;

		public RawImage[] channelDisplays = new RawImage[ChannelCount];

		private Texture2D[] channelTextures = new Texture2D[ChannelCount];

		public Button[] channelButtons = new Button[ChannelCount];

		private void Start()
		{
			InitializeTextures();
			RegisterButtonListeners();
		}

		private void InitializeTextures()
		{
			Color32[] array = new Color32[TextureWidth * TextureHeight];
			Color32[] array2 = new Color32[3]
			{
				new Color32(byte.MaxValue, 0, 0, 0),
				new Color32(0, byte.MaxValue, 0, 0),
				new Color32(0, 0, byte.MaxValue, 0)
			};
			for (int i = 0; i < ChannelCount; i++)
			{
				Color32 color = array2[i];
				for (int j = 0; j < array.Length; j++)
				{
					array[j] = color;
				}
				InitTexture(i, array);
			}
		}

		private void InitTexture(int channelIndex, Color32[] colors)
		{
			Texture2D texture2D = new Texture2D(TextureWidth, TextureHeight, TextureFormat.RGBA32, false);
			texture2D.filterMode = FilterMode.Point;
			texture2D.wrapMode = TextureWrapMode.Clamp;
			texture2D.SetPixels32(colors);
			texture2D.Apply();
			channelDisplays[channelIndex].texture = texture2D;
			channelTextures[channelIndex] = texture2D;
		}

		private void RegisterButtonListeners()
		{
			for (int i = 0; i < channelButtons.Length; i++)
			{
				int newInt = i;
				channelButtons[i].onClick.AddListener(delegate
				{
					ToggleChannel(newInt);
				});
			}
		}

		public void ToggleChannel(int channelIndex)
		{
			channelDisplays[channelIndex].rectTransform.SetAsLastSibling();
		}

		public void PlotData(List<FinalPixelData> dataList, BlockColor blockColor, double minColorTemp, double colorTempSpan)
		{
			double num = (double)TextureWidth / colorTempSpan;
			double num2 = (double)TextureHeight / 180.0;
			double num3 = (double)TextureHeight / 256.0;
			double num4 = (double)TextureHeight / 256.0;
			for (int i = 0; i < dataList.Count; i++)
			{
				FinalPixelData finalPixelData = dataList[i];
				if ((uint)finalPixelData.blockColor == (uint)blockColor)
				{
					int x = (int)((finalPixelData.cct - minColorTemp) * num);
					for (int j = 0; j < finalPixelData.channel1.Length; j++)
					{
						int y = (int)((double)(int)finalPixelData.channel1[j] * num2);
						int y2 = (int)((double)(int)finalPixelData.channel2[j] * num3);
						int y3 = (int)((double)(int)finalPixelData.channel3[j] * num4);
						UpdatePixel(x, y, 0);
						UpdatePixel(x, y2, 1);
						UpdatePixel(x, y3, 2);
					}
				}
			}
			ApplyAllTextureChanges();
		}

		private void ApplyAllTextureChanges()
		{
			for (int i = 0; i < channelTextures.Length; i++)
			{
				channelTextures[i].Apply();
			}
		}

		private void UpdatePixel(int x, int y, int channelIndex)
		{
			Texture2D texture2D = channelTextures[channelIndex];
			Color pixel = texture2D.GetPixel(x, y);
			pixel = new Color(pixel.r, pixel.g, pixel.b, 1f);
			texture2D.SetPixel(x, y, pixel);
		}
	}
}
