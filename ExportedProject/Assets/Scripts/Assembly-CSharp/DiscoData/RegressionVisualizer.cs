using System;
using System.IO;
using DiscoCapture;
using UnityEngine;

namespace DiscoData
{
	public class RegressionVisualizer : MonoBehaviour
	{
		public GraphPoint pointPrefab;

		public GraphPoint medianPointPrefab;

		private static double Deg2Rad;

		private static double Rad2Deg;

		private CaptureServerData _serverData;

		public ColorClassifierData.ChannelData[] allColorsChannelData;

		public AnimationCurve[] colorCurves = new AnimationCurve[8];

		public Color[] colorLookup = new Color[8];

		private static double _CCTSpan = 60.0;

		private static double _CCTScale;

		private static double _HueSpan = 60.0;

		private static double _HueScale;

		private void Awake()
		{
			Deg2Rad = Math.PI / 180.0;
			Rad2Deg = 180.0 / Math.PI;
		}

		private void Start()
		{
			InitializeData();
		}

		private void InitializeData()
		{
			LoadDatasetFromDisk();
			ConvertDataToRectangularCoordinates();
			SetUpGraphScaling();
			PlotAllData();
		}

		private void LoadDatasetFromDisk()
		{
			string json = string.Empty;
			TryGettingLatestColorDataFromDisk(out json);
			_serverData = JsonUtility.FromJson<CaptureServerData>(json);
			_serverData.CorrectHueAngleWraparound();
			double num = _serverData.maxCCT - _serverData.minCCT;
			_CCTScale = _CCTSpan / num;
		}

		private bool TryGettingLatestColorDataFromDisk(out string json)
		{
			string path = Application.persistentDataPath + "/LatestColorData.json";
			if (!File.Exists(path))
			{
				json = string.Empty;
				return false;
			}
			json = File.ReadAllText(path);
			return true;
		}

		private void ConvertDataToRectangularCoordinates()
		{
			allColorsChannelData = new ColorClassifierData.ChannelData[8];
			allColorsChannelData[0] = new ColorClassifierData.ChannelData(_serverData.red.cctVals, _serverData.red.channelVals);
			allColorsChannelData[1] = new ColorClassifierData.ChannelData(_serverData.orange.cctVals, _serverData.orange.channelVals);
			allColorsChannelData[2] = new ColorClassifierData.ChannelData(_serverData.yellow.cctVals, _serverData.yellow.channelVals);
			allColorsChannelData[3] = new ColorClassifierData.ChannelData(_serverData.green.cctVals, _serverData.green.channelVals);
			allColorsChannelData[4] = new ColorClassifierData.ChannelData(_serverData.blue.cctVals, _serverData.blue.channelVals);
			allColorsChannelData[5] = new ColorClassifierData.ChannelData(_serverData.purple.cctVals, _serverData.purple.channelVals);
			allColorsChannelData[6] = new ColorClassifierData.ChannelData(_serverData.pink.cctVals, _serverData.pink.channelVals);
			allColorsChannelData[7] = new ColorClassifierData.ChannelData(_serverData.white.cctVals, _serverData.white.channelVals);
		}

		private void SetUpGraphScaling()
		{
			double num = 1000000.0;
			double num2 = -1000000.0;
			for (int i = 0; i < allColorsChannelData.Length; i++)
			{
				double[] yVals = allColorsChannelData[i].yVals;
				for (int j = 0; j < yVals.Length; j++)
				{
					if (yVals[j] < num)
					{
						num = yVals[j];
					}
					else if (yVals[j] > num2)
					{
						num2 = yVals[j];
					}
				}
			}
			double num3 = num2 - num;
			_HueScale = _HueSpan / num3;
		}

		private void PlotAllData()
		{
			for (int i = 0; i < colorCurves.Length; i++)
			{
				PlotDataForBlockColor((BlockColor)i);
			}
		}

		private void PlotDataForBlockColor(BlockColor blockColor)
		{
			ColorClassifierData.ChannelData channelData = allColorsChannelData[(uint)blockColor];
			int num = channelData.xVals.Length;
			Keyframe[] array = new Keyframe[num];
			for (int i = 0; i < num; i++)
			{
				double num2 = channelData.xVals[i] * _CCTScale;
				double num3 = channelData.yVals[i] * _HueScale;
				array[i] = new Keyframe((float)num2, (float)num3);
				GraphPoint graphPoint = UnityEngine.Object.Instantiate(medianPointPrefab);
				graphPoint.spriteRenderer.color = colorLookup[(uint)blockColor];
				graphPoint.selfTransform.position = new Vector3((float)num2, (float)num3, 0f);
			}
			colorCurves[(uint)blockColor] = new AnimationCurve(array);
		}
	}
}
