using DiscoCapture;
using UnityEngine;
using UnityEngine.UI;

namespace DiscoData
{
	public class S3DownloadInterstitial : MonoBehaviour
	{
		public Button uiButtonDownloadAll;

		public Button uiButtonCancel;

		private DataAnalyzer dataAnalyzer;

		private CaptureImageLoader imageLoader;

		public void Start()
		{
			uiButtonDownloadAll.onClick.AddListener(DownloadAll);
			uiButtonCancel.onClick.AddListener(Cancel);
		}

		public void Init(DataAnalyzer dataAnalyzer, CaptureImageLoader imageLoader)
		{
			this.dataAnalyzer = dataAnalyzer;
			this.imageLoader = imageLoader;
			this.imageLoader.OnDownloadComplete += HandleDownloadsCompleted;
		}

		private void OnDestroy()
		{
			if (imageLoader != null)
			{
				imageLoader.OnDownloadComplete -= HandleDownloadsCompleted;
			}
		}

		private void DownloadAll()
		{
			dataAnalyzer.SetStatusText("Downloading and Saving All...");
			imageLoader.DownloadAllImages(dataAnalyzer.URLs);
		}

		private void Cancel()
		{
			base.gameObject.SetActive(false);
		}

		private void HandleDownloadsCompleted()
		{
			dataAnalyzer.SetStatusText("Download and Save Complete.");
			base.gameObject.SetActive(false);
		}
	}
}
