using System.Collections.Generic;
using DiscoCapture;
using OpenCVForUnity;

public class DiscoUI
{
	private static LineSegment topBaseExtended;

	private static LineSegment rightLegExtended;

	private static LineSegment bottomBaseExtended;

	private static LineSegment leftLegExtended;

	public static void DrawBoardOutline(ref Mat displayMat, ref List<Point> corners, Scalar color)
	{
		topBaseExtended = new LineSegment(corners[0], corners[1]);
		rightLegExtended = new LineSegment(corners[1], corners[2]);
		bottomBaseExtended = new LineSegment(corners[2], corners[3]);
		leftLegExtended = new LineSegment(corners[3], corners[0]);
		topBaseExtended.Draw(ref displayMat, color, 3);
		rightLegExtended.Draw(ref displayMat, color, 3);
		bottomBaseExtended.Draw(ref displayMat, color, 3);
		leftLegExtended.Draw(ref displayMat, color, 3);
	}

	public static void DrawBoardOutline(ref Mat displayMat, ref Point[] corners, Scalar color)
	{
		topBaseExtended = new LineSegment(corners[0], corners[1]);
		rightLegExtended = new LineSegment(corners[1], corners[2]);
		bottomBaseExtended = new LineSegment(corners[2], corners[3]);
		leftLegExtended = new LineSegment(corners[3], corners[0]);
		topBaseExtended.Draw(ref displayMat, color, 3);
		rightLegExtended.Draw(ref displayMat, color, 3);
		bottomBaseExtended.Draw(ref displayMat, color, 3);
		leftLegExtended.Draw(ref displayMat, color, 3);
	}

	public static void DrawGridCorners(ref Mat theMat, ref Point[] gridCorners, Scalar lineColor, float percentLength = 0.075f, int thickness = 1)
	{
		for (int i = 0; i < 4; i++)
		{
			Point startPoint = gridCorners[i];
			Point endPoint = gridCorners[(i + 1) % 4];
			DrawPartialLinesFromEndPoints(ref theMat, startPoint, endPoint, lineColor, percentLength, thickness);
		}
	}

	public static void DrawGridCorners(ref Mat theMat, ref List<Point> gridCorners, Scalar lineColor, float percentLength = 0.075f, int thickness = 1)
	{
		for (int i = 0; i < 4; i++)
		{
			Point startPoint = gridCorners[i];
			Point endPoint = gridCorners[(i + 1) % 4];
			DrawPartialLinesFromEndPoints(ref theMat, startPoint, endPoint, lineColor, percentLength, thickness);
		}
	}

	private static void DrawPartialLinesFromEndPoints(ref Mat theMat, Point startPoint, Point EndPoint, Scalar lineColor, float percentLength = 0.075f, int thickness = 1)
	{
		DrawPartialLineFromPoint(ref theMat, startPoint, EndPoint, lineColor, percentLength, thickness);
		DrawPartialLineFromPoint(ref theMat, EndPoint, startPoint, lineColor, percentLength, thickness);
	}

	private static void DrawPartialLineFromPoint(ref Mat theMat, Point startPoint, Point EndPoint, Scalar lineColor, float percentLength = 0.075f, int thickness = 1)
	{
		Point pt = new Point(startPoint.x + (EndPoint.x - startPoint.x) * (double)percentLength, startPoint.y + (EndPoint.y - startPoint.y) * (double)percentLength);
		Imgproc.line(theMat, startPoint, pt, lineColor, thickness);
	}
}
