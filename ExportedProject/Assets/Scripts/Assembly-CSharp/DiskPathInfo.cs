using System;

public struct DiskPathInfo
{
	public string diskPathString;

	public string s3Path;

	public string fileName;

	public Guid guid;

	public string idString;

	public string subDirectory;

	public string rootDirectory;

	public ProjectType type;

	public UserAccount user;

	public DiskPathInfo(string path, UserAccount _user)
	{
		string[] array = path.Split('/');
		fileName = array[array.Length - 1];
		idString = array[array.Length - 2];
		guid = new Guid(idString);
		subDirectory = array[array.Length - 3];
		rootDirectory = array[array.Length - 4];
		user = _user;
		switch (rootDirectory)
		{
		case "Boards":
			type = ProjectType.Board;
			break;
		case "Animations":
			type = ProjectType.Animation;
			break;
		case "Levels":
			type = ProjectType.Level;
			break;
		case "Brains":
			type = ProjectType.Brain;
			break;
		case "Characters":
			type = ProjectType.Character;
			break;
		case "MegaBoards":
			type = ProjectType.MegaBoard;
			break;
		case "Games":
			type = ProjectType.Game;
			break;
		default:
			type = ProjectType.None;
			break;
		}
		diskPathString = BloxelProject.GetAbsolutePathForType(type, idString);
		s3Path = user.serverID + "/" + BloxelProject.GetRelativePathForType(type, idString);
	}
}
