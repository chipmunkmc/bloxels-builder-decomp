public class Dustin
{
	public string Name { get; private set; }

	public int Age { get; private set; }

	public string Url { get; private set; }

	public Dustin()
	{
		Name = "Dustin Horne";
		Age = 34;
		Url = "http://www.dustinhorne.com";
	}
}
