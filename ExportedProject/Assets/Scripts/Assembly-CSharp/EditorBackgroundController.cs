using DG.Tweening;
using UnityEngine;

public class EditorBackgroundController : MonoBehaviour
{
	public static EditorBackgroundController instance;

	public CanvasMode currentMode;

	private Color transitionStartColor = new Color(1f, 1f, 1f, 1f);

	private Color transitionEndColor = new Color(1f, 1f, 1f, 0f);

	public MeshRenderer transitionRenderer;

	public MeshRenderer permanentRenderer;

	public Material transitionMaterial;

	public Material permanentMaterial;

	private Texture2D purpleBkgdTex;

	private Texture2D blueBkgdTex;

	private Texture2D greenBkgdTex;

	private Texture2D redBkgdTex;

	private Texture2D orangeBkgdTex;

	public string backgroundTexturesPath;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		purpleBkgdTex = Resources.Load<Texture2D>(backgroundTexturesPath + "background_games");
		blueBkgdTex = Resources.Load<Texture2D>(backgroundTexturesPath + "background_animations");
		greenBkgdTex = Resources.Load<Texture2D>(backgroundTexturesPath + "background_backgrounds");
		redBkgdTex = Resources.Load<Texture2D>(backgroundTexturesPath + "background_characters");
		orangeBkgdTex = Resources.Load<Texture2D>(backgroundTexturesPath + "background_boards");
		transitionMaterial.mainTexture = purpleBkgdTex;
		permanentMaterial.mainTexture = purpleBkgdTex;
		transitionRenderer.material = transitionMaterial;
		permanentRenderer.material = permanentMaterial;
		Vector3 localScale = new Vector3(Screen.width, (float)Screen.width * 3f / 4f, 1f);
		permanentRenderer.transform.localScale = localScale;
		transitionRenderer.transform.localScale = localScale;
		Camera component = GetComponent<Camera>();
		component.orthographicSize = (float)Screen.height / 2f;
	}

	public void SwitchBackground(CanvasMode _mode)
	{
		currentMode = _mode;
		transitionMaterial.mainTexture = permanentMaterial.mainTexture;
		switch (currentMode)
		{
		case CanvasMode.LevelEditor:
			permanentMaterial.mainTexture = purpleBkgdTex;
			break;
		case CanvasMode.Animator:
			permanentMaterial.mainTexture = blueBkgdTex;
			break;
		case CanvasMode.MegaBoard:
			permanentMaterial.mainTexture = greenBkgdTex;
			break;
		case CanvasMode.CharacterBuilder:
			permanentMaterial.mainTexture = redBkgdTex;
			break;
		case CanvasMode.PaintBoard:
			permanentMaterial.mainTexture = orangeBkgdTex;
			break;
		}
		transitionMaterial.color = transitionStartColor;
		transitionMaterial.DOColor(transitionEndColor, UIAnimationManager.speedMedium).OnStart(delegate
		{
			transitionRenderer.gameObject.SetActive(true);
		}).OnComplete(delegate
		{
			transitionRenderer.gameObject.SetActive(false);
		});
	}
}
