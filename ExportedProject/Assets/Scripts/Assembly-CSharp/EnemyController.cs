using System;
using UnityEngine;

public abstract class EnemyController
{
	public BloxelEnemyTile tile;

	public const float gravityScale = 10f;

	public bool isEnabled;

	public bool lookingForPlayer;

	protected bool playerInAttackRange;

	protected static readonly float basePatrolSpeed = 15f;

	protected static readonly float maxPatrolSpeed = 60f;

	public static readonly float baseProjectileSpeed = 26.25f;

	protected static float patrolDistance = 60f;

	protected static float baseAttackDistance = 52f;

	private float _attackDistanceSquared;

	public bool shouldExplodeOnDeath;

	public bool shouldReflectBullets;

	public float _trackingSpeed;

	private static float _timeBetweenSpreadShotAttacks = 4f;

	private static float _timeBetweenLaserAttacks = 0.75f;

	private static int _maxSpreadShotProjectiles = 20;

	private static float _baseTimeBetweenAttacks = 1.5f;

	private static float[] _ProjectileFiringRates = new float[6] { 3f, 1.5f, 1f, 0.75f, 0.5f, 0.25f };

	public int coinBonusQuantum;

	public int coinBurgleQuantum;

	public int healthBonusQuantum;

	public int healthBurgleQuantum;

	protected static float flipTime = 0.03333f;

	protected static float baseTimeUntilDirectionSwitch = 4f;

	protected float timeUntilDirectionSwitch = 4f;

	protected float timer;

	protected int movementDirection = 1;

	protected Vector3 previousPos;

	public Action currentUpdateAction;

	public Action currentFixedUpdateAction;

	public Action attackAction;

	private float timeSinceFlip;

	protected float playerCheckTimer;

	protected float playerCheckInterval = 0.5f;

	protected float attackTimer;

	private float initialAngle;

	private int numSpreadBullets = 6;

	public EnemyLaser laser;

	public float patrolSpeed { get; protected set; }

	public int startingHealth { get; protected set; }

	public float timeBetweenAttacks { get; protected set; }

	public float projectileSpeed { get; private set; }

	public int attackDamage { get; protected set; }

	public int projectileDamage { get; protected set; }

	public float targetScale { get; protected set; }

	public BloxelBrain brain { get; protected set; }

	public virtual void InitWithBrain(BloxelBrain brain)
	{
		this.brain = brain;
		if (brain == null)
		{
			InitBrainless();
			return;
		}
		attackDamage = brain.attackDamage;
		shouldExplodeOnDeath = brain.shouldExplodeOnDeath;
		patrolSpeed = 3f + maxPatrolSpeed * (float)(int)brain.blockTotals[1] / (float)BloxelBrain.MaxBlocks;
		timeUntilDirectionSwitch = patrolDistance / patrolSpeed;
		coinBonusQuantum = brain.coinBonusQuantum;
		healthBonusQuantum = brain.healthBonusQuantum;
		coinBurgleQuantum = brain.coinBurgleQuantum;
		healthBurgleQuantum = brain.healthBurgleQuantum;
		InitScalingBehavior();
		InitProjectileBehavior();
		startingHealth = 1 + Mathf.FloorToInt((float)(int)brain.blockTotals[6] / (float)BloxelBrain.HealthAndDamageQuantum);
		shouldReflectBullets = brain.blockTotals[6] >= BloxelBrain.BulletReflectionThreshold;
	}

	private void InitBrainless()
	{
		bool flag = this is EnemyStationaryController;
		attackDamage = 1;
		shouldExplodeOnDeath = false;
		patrolSpeed = ((!flag) ? basePatrolSpeed : 0f);
		timeUntilDirectionSwitch = baseTimeUntilDirectionSwitch;
		coinBonusQuantum = 0;
		healthBonusQuantum = 0;
		coinBurgleQuantum = 0;
		healthBurgleQuantum = 0;
		targetScale = 1f;
		if (flag)
		{
			currentUpdateAction = CheckForProjectileAttack;
			attackAction = FireBullet;
			projectileDamage = attackDamage;
			projectileSpeed = baseProjectileSpeed;
			timeBetweenAttacks = _baseTimeBetweenAttacks;
			_attackDistanceSquared = baseAttackDistance * baseAttackDistance;
		}
		else
		{
			currentUpdateAction = null;
			attackAction = null;
		}
		startingHealth = 3;
		shouldReflectBullets = false;
	}

	private void InitScalingBehavior()
	{
		targetScale = 1f + (float)Mathf.Clamp(brain.blockTotals[4], 0, 26) / 13f;
	}

	private void InitProjectileBehavior()
	{
		bool flag = this is EnemyStationaryController;
		float num = Mathf.Max(tile.originalBoundsSize.x, tile.originalBoundsSize.y) * targetScale * 0.5f;
		_attackDistanceSquared = (baseAttackDistance + num) * (baseAttackDistance + num);
		projectileDamage = attackDamage;
		currentUpdateAction = CheckForProjectileAttack;
		int num2 = brain.blockTotals[5];
		if (num2 >= BloxelBrain.LaserThreshold)
		{
			timeBetweenAttacks = _timeBetweenLaserAttacks;
			_trackingSpeed = (float)num2 / (float)BloxelBrain.MaxBlocks;
			attackAction = FireLaser;
			return;
		}
		int num3 = brain.blockTotals[1];
		projectileSpeed = ((!flag) ? Mathf.Clamp(baseProjectileSpeed + patrolSpeed, baseProjectileSpeed, 150f) : Mathf.Clamp(baseProjectileSpeed + (float)num3 * 6.1875f, baseProjectileSpeed, 150f));
		if (num2 >= BloxelBrain.SpreadShotThreshold)
		{
			float num4 = (float)(num2 - BloxelBrain.SpreadShotThreshold) / (float)(BloxelBrain.LaserThreshold - BloxelBrain.SpreadShotThreshold);
			timeBetweenAttacks = _timeBetweenSpreadShotAttacks;
			if (num2 == BloxelBrain.SpreadShotThreshold)
			{
				numSpreadBullets = 3;
			}
			else if (num2 == BloxelBrain.SpreadShotThreshold + 1)
			{
				numSpreadBullets = 6;
			}
			else if (num2 == BloxelBrain.SpreadShotThreshold + 2)
			{
				numSpreadBullets = 9;
			}
			else if (num2 == BloxelBrain.SpreadShotThreshold + 3)
			{
				numSpreadBullets = 12;
			}
			else if (num2 == BloxelBrain.SpreadShotThreshold + 4)
			{
				numSpreadBullets = _maxSpreadShotProjectiles;
			}
			initialAngle = (float)numSpreadBullets / 2f * (360f / (float)_maxSpreadShotProjectiles) * ((float)Math.PI / 180f);
			attackAction = FireSpreadShot;
		}
		else if (flag || num2 > 0)
		{
			int num5 = ((!flag) ? (num2 - 1) : num2);
			timeBetweenAttacks = _ProjectileFiringRates[num5];
			attackAction = FireBullet;
		}
		else
		{
			timeBetweenAttacks = _baseTimeBetweenAttacks;
			currentUpdateAction = null;
		}
	}

	public virtual void InitForCurrentCameraMode()
	{
		BloxelCamera.CameraMode mode = BloxelCamera.instance.mode;
		if (mode == BloxelCamera.CameraMode.Gameplay)
		{
			Enable();
		}
		else
		{
			Disable();
		}
	}

	public abstract void Enable();

	public abstract void Disable();

	private void AttackPlayer()
	{
		if (attackAction != null)
		{
			attackAction();
		}
	}

	public void Flip()
	{
		timer = 0f;
		movementDirection *= -1;
		timeSinceFlip = 0f;
	}

	public virtual void Patrol()
	{
		if (isEnabled)
		{
			if ((float)movementDirection != Mathf.Sign(tile.selfTransform.localScale.x) && timeSinceFlip > flipTime)
			{
				tile.selfTransform.localScale = new Vector3(tile.selfTransform.localScale.x * -1f, tile.selfTransform.localScale.y, tile.selfTransform.localScale.z);
			}
			timeSinceFlip += Time.fixedDeltaTime;
			if (Mathf.Abs(tile.selfTransform.localPosition.x - previousPos.x) < 0.2f * patrolSpeed * Time.fixedDeltaTime)
			{
				Flip();
			}
			else if (timer >= timeUntilDirectionSwitch)
			{
				Flip();
				tile.selfTransform.localScale = new Vector3(tile.selfTransform.localScale.x * -1f, tile.selfTransform.localScale.y, tile.selfTransform.localScale.z);
			}
			tile.rigidbodyComponent.velocity = new Vector2(patrolSpeed * (float)movementDirection, tile.rigidbodyComponent.velocity.y);
			timer += Time.fixedDeltaTime;
			previousPos = tile.selfTransform.localPosition;
		}
	}

	public void CheckForProjectileAttack()
	{
		if (playerInAttackRange = PixelPlayerController.instance.GetSquaredDistanceToEnemy(tile) < _attackDistanceSquared && attackTimer > timeBetweenAttacks)
		{
			AttackPlayer();
			attackTimer = 0f;
		}
		attackTimer += Time.deltaTime;
	}

	private void FireBullet()
	{
		SoundManager.PlayOneShot(SoundManager.instance.enemyProjectile);
		Vector3 position = tile.boxCollider.bounds.center + new Vector3(0f, 0f, -0.01f - tile.selfTransform.localScale.z / 2f);
		Vector2 vector = new Vector2(PixelPlayerController.instance.selfTransform.position.x - position.x, PixelPlayerController.instance.selfTransform.position.y + PixelPlayerController.instance.boxCollider.size.y / 2f * PixelPlayerController.instance.selfTransform.localScale.y - position.y);
		vector.Normalize();
		EnemyProjectile enemyProjectile = GameplayPool.SpawnEnemyProjectile();
		enemyProjectile.InitWithSpeed(projectileSpeed);
		enemyProjectile.selfTransform.position = position;
		enemyProjectile.selfTransform.rotation = Quaternion.identity;
		enemyProjectile.damage = projectileDamage;
		enemyProjectile.rigidbodyComponent.AddForce(vector * projectileSpeed, ForceMode2D.Impulse);
		enemyProjectile.SetParticleScale(tile.selfTransform.localScale.y);
	}

	private void FireSpreadShot()
	{
		SoundManager.PlayOneShot(SoundManager.instance.enemyProjectile);
		Vector3 position = tile.boxCollider.bounds.center + new Vector3(0f, 0f, -0.01f - tile.selfTransform.localScale.z / 2f);
		Vector2 vector = new Vector2(PixelPlayerController.instance.selfTransform.position.x - position.x, PixelPlayerController.instance.selfTransform.position.y + PixelPlayerController.instance.boxCollider.size.y / 2f * PixelPlayerController.instance.selfTransform.localScale.y - position.y);
		float num = Mathf.Atan2(vector.y, vector.x);
		num = (float)Mathf.RoundToInt(num / ((float)Math.PI / 2f)) * ((float)Math.PI / 2f);
		int num2 = (int)Mathf.Sign(new Vector2(Mathf.Cos(num), Mathf.Sin(num)).x);
		float num3 = (float)(-num2) * ((float)Math.PI * 2f / (float)_maxSpreadShotProjectiles);
		num += num3 * ((float)(numSpreadBullets - 1) / 2f);
		float num4 = num;
		for (int i = 0; i < numSpreadBullets; i++)
		{
			Vector2 vector2 = new Vector2(Mathf.Cos(num4), Mathf.Sin(num4));
			EnemyProjectile enemyProjectile = GameplayPool.SpawnEnemyProjectile();
			enemyProjectile.InitWithSpeed(projectileSpeed);
			enemyProjectile.selfTransform.position = position;
			enemyProjectile.selfTransform.rotation = Quaternion.identity;
			enemyProjectile.damage = projectileDamage;
			enemyProjectile.rigidbodyComponent.AddForce(vector2 * projectileSpeed, ForceMode2D.Impulse);
			enemyProjectile.SetParticleScale(tile.selfTransform.localScale.y);
			num4 -= num3;
		}
	}

	private void FireLaser()
	{
		if (playerInAttackRange && laser == null)
		{
			laser = GameplayPool.SpawnLaser();
			laser.Init(this, tile.selfTransform, PixelPlayerController.instance.centerTransform, new Vector3(0f, tile.originalBoundsSize.y / 2f, -0.1f - tile.selfTransform.localScale.z / 2f), _trackingSpeed);
		}
	}

	public void BurgleThePlayer(Vector3 spawnPosition, float yOffsetUnscaled)
	{
		if (coinBurgleQuantum != 0)
		{
			ParticleAttractor particleAttractor = GameplayPool.SpawnAttractor();
			particleAttractor.selfTransform.position = spawnPosition;
			particleAttractor.TakeFromPlayer(tile, BonusType.Coin, coinBurgleQuantum, tile.selfTransform, new Vector3(0f, yOffsetUnscaled, -0.51f));
		}
		if (healthBurgleQuantum != 0)
		{
			ParticleAttractor particleAttractor2 = GameplayPool.SpawnAttractor();
			particleAttractor2.selfTransform.position = spawnPosition;
			particleAttractor2.TakeFromPlayer(tile, BonusType.Health, healthBurgleQuantum, tile.selfTransform, new Vector3(0f, yOffsetUnscaled, -0.51f));
		}
	}
}
