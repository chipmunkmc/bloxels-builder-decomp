using UnityEngine;

public sealed class EnemyFlyerController : EnemyController
{
	public EnemyFlyerController(BloxelEnemyTile enemyTile)
	{
		tile = enemyTile;
		base.patrolSpeed = 15f;
		currentFixedUpdateAction = Patrol;
	}

	public override void Enable()
	{
		isEnabled = true;
		tile.rigidbodyComponent.gravityScale = 0f;
		tile.rigidbodyComponent.isKinematic = false;
		if (!Mathf.Approximately(tile.selfTransform.localScale.y, base.targetScale))
		{
			tile.TryToGrow();
		}
	}

	public override void Disable()
	{
		isEnabled = false;
		tile.rigidbodyComponent.gravityScale = 0f;
		tile.rigidbodyComponent.isKinematic = true;
		tile.rigidbodyComponent.velocity = Vector2.zero;
		tile.TryNotToGrow();
	}

	public override void Patrol()
	{
		if (isEnabled)
		{
			if (Mathf.Abs(tile.selfTransform.localPosition.x - previousPos.x) < 0.2f * base.patrolSpeed * Time.fixedDeltaTime)
			{
				Flip();
			}
			else if (timer >= timeUntilDirectionSwitch)
			{
				Flip();
			}
			tile.rigidbodyComponent.velocity = new Vector2(base.patrolSpeed * (float)movementDirection, 0f);
			timer += Time.fixedDeltaTime;
			previousPos = tile.selfTransform.localPosition;
		}
	}
}
