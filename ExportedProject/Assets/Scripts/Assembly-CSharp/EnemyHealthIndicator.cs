using UnityEngine;

public class EnemyHealthIndicator : MonoBehaviour
{
	public Transform selfTransform;

	public Transform healthBarTransform;

	public SpriteRenderer healthRenderer;

	private float _expirationTime;

	private BloxelEnemyTile _currentEnemy;

	private static float _displayTime = 1.5f;

	private static float _animationSpeed = 10f;

	private static Color _noHealthColor = Color.red;

	private static Color _fullHealthColor = Color.green;

	private Color _targetColor;

	private Vector3 _targetScale;

	public void ShowEnemyHealthDecreasing(BloxelEnemyTile enemy)
	{
		float num = Mathf.Clamp01(((float)enemy.health + 1f) / (float)enemy.controller.startingHealth);
		float num2 = (float)enemy.health / (float)enemy.controller.startingHealth;
		if (_currentEnemy == null || _currentEnemy != enemy)
		{
			healthRenderer.color = Color.Lerp(_noHealthColor, _fullHealthColor, num);
			healthBarTransform.localScale = new Vector3(num, 1f, 1f);
		}
		_currentEnemy = enemy;
		_expirationTime = Time.time + _displayTime;
		_targetColor = Color.Lerp(_noHealthColor, _fullHealthColor, num2);
		_targetScale.Set(num2, 1f, 1f);
	}

	public int ShowEnemyHealthIncreasing(BloxelEnemyTile enemy, int increaseAmount)
	{
		increaseAmount = Mathf.Clamp(increaseAmount, 0, enemy.controller.startingHealth - enemy.health);
		float num = Mathf.Clamp01((float)enemy.health / (float)enemy.controller.startingHealth);
		float num2 = (float)(enemy.health + increaseAmount) / (float)enemy.controller.startingHealth;
		if (_currentEnemy == null || _currentEnemy != enemy)
		{
			healthRenderer.color = Color.Lerp(_noHealthColor, _fullHealthColor, num);
			healthBarTransform.localScale = new Vector3(num, 1f, 1f);
		}
		_currentEnemy = enemy;
		_expirationTime = Time.time + _displayTime;
		_targetColor = Color.Lerp(_noHealthColor, _fullHealthColor, num2);
		_targetScale.Set(num2, 1f, 1f);
		return increaseAmount;
	}

	private void Update()
	{
		selfTransform.localPosition = new Vector3(_currentEnemy.selfTransform.localPosition.x, _currentEnemy.selfTransform.localPosition.y + _currentEnemy.selfTransform.localScale.y * 14f, 0f);
		if (healthBarTransform.localScale.x != _targetScale.x)
		{
			healthBarTransform.localScale = Vector3.Lerp(healthBarTransform.localScale, _targetScale, Time.deltaTime * _animationSpeed);
			healthRenderer.color = Color.Lerp(healthRenderer.color, _targetColor, Time.deltaTime * _animationSpeed);
		}
		if (Time.time > _expirationTime)
		{
			_currentEnemy.healthIndicator = null;
			_currentEnemy = null;
			base.gameObject.Recycle();
		}
	}
}
