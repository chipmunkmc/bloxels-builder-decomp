using System;
using UnityEngine;

public sealed class EnemyLaser : PoolableComponent
{
	private Transform _targetTransform;

	private Transform _tileTransform;

	private EnemyController _controller;

	public Sprite[] animationFrames;

	private int _frameCount;

	public SpriteRenderer spriteRendererComponent;

	private float _timeOfNextFrameChange;

	private float _timeBetweenFrames;

	private float _framesPerSecond;

	private float _laserLength;

	private float _currentLengthTarget;

	private static float _LaserMaxLength = 400f;

	private static float _ExtensionDuration = 4f;

	private Vector3 _targetDirection;

	private Vector3 _currentDirection;

	private Vector3 _targetOffset;

	private float _trackingSpeed;

	public ParticleSystem emissionParticles;

	public ParticleSystem collisionParticles;

	private ParticleSystem.ShapeModule _shapeModule;

	private ParticleSystem.ShapeModule _collisionShapeModule;

	private float _originalEmissionRadius;

	private float _originalCollisionEmissionRadius;

	private bool _wasHitLastFrame;

	public LayerMask laserMask;

	private int _laserMaskValue;

	private static float _maxAttackDistanceSquared = 7140.25f;

	private GameObject _previouslyHitObject;

	private int _hitCount;

	public float framesPerSecond
	{
		get
		{
			return _framesPerSecond;
		}
		set
		{
			_framesPerSecond = value;
			_timeBetweenFrames = 1f / _framesPerSecond;
		}
	}

	public override void EarlyAwake()
	{
		framesPerSecond = 45f;
		_frameCount = animationFrames.Length;
		_shapeModule = emissionParticles.shape;
		_collisionShapeModule = collisionParticles.shape;
		_originalEmissionRadius = _shapeModule.radius;
		_originalCollisionEmissionRadius = _collisionShapeModule.radius;
		_laserMaskValue = laserMask.value;
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		_timeOfNextFrameChange = Time.time + _timeBetweenFrames;
		_currentLengthTarget = 0f;
		selfTransform.localScale = new Vector3(0f, 1f, 1f);
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		_controller.laser = null;
		base.gameObject.SetActive(false);
		selfTransform.SetParent(WorldWrapper.instance.runtimeObjectContainer);
	}

	public override void Despawn()
	{
		GameplayPool.DespawnLaser(this);
	}

	public void Init(EnemyController controller, Transform tileTransform, Transform targetTransform, Vector3 localPos, float trackingSpeed)
	{
		_controller = controller;
		_tileTransform = tileTransform;
		_targetTransform = targetTransform;
		selfTransform.SetParent(tileTransform);
		selfTransform.localPosition = localPos;
		_targetOffset = new Vector3(0f, 0f, localPos.z);
		_shapeModule.radius = _originalEmissionRadius * tileTransform.localScale.y;
		_collisionShapeModule.radius = _originalCollisionEmissionRadius * tileTransform.localScale.y;
		_trackingSpeed = trackingSpeed;
		int num = controller.brain.blockTotals[5] - BloxelBrain.LaserThreshold;
		if (num < 4)
		{
			_trackingSpeed = 0f;
			float f = (float)(num * 90) * ((float)Math.PI / 180f);
			_currentDirection = new Vector2(Mathf.Cos(f), Mathf.Sin(f));
		}
		else if (num < 8)
		{
			_trackingSpeed = 0f;
			float f2 = (float)(45 + num * 90) * ((float)Math.PI / 180f);
			_currentDirection = new Vector2(Mathf.Cos(f2), Mathf.Sin(f2));
		}
		else
		{
			_currentDirection = (targetTransform.position + _targetOffset - selfTransform.position).normalized;
		}
	}

	public void ReInitScale(Vector3 localPos)
	{
		selfTransform.localPosition = localPos;
		_targetOffset = new Vector3(0f, 0f, localPos.z);
		_shapeModule.radius = _originalEmissionRadius * selfTransform.parent.localScale.y;
		_collisionShapeModule.radius = _originalCollisionEmissionRadius * selfTransform.parent.localScale.y;
	}

	private void Update()
	{
		if (Time.time > _timeOfNextFrameChange)
		{
			spriteRendererComponent.sprite = animationFrames[UnityEngine.Random.Range(0, _frameCount)];
			_timeOfNextFrameChange = Time.time + _timeBetweenFrames;
		}
	}

	private void FixedUpdate()
	{
		Vector3 position = selfTransform.position;
		Vector3 position2 = _targetTransform.position;
		float z = position2.z;
		position2 += _targetOffset;
		Vector3 vector = position2 - position;
		if (vector.sqrMagnitude > _maxAttackDistanceSquared)
		{
			Despawn();
			return;
		}
		_targetDirection = vector.normalized;
		_currentDirection = Vector3.Lerp(_currentDirection, _targetDirection, _trackingSpeed * Time.fixedDeltaTime);
		if (_tileTransform.localScale.x > 0f)
		{
			selfTransform.right = _currentDirection;
		}
		else
		{
			selfTransform.right = new Vector3(0f - _currentDirection.x, _currentDirection.y, _currentDirection.z);
		}
		RaycastHit2D lastHit = Physics2D.Raycast(new Vector2(position.x, position.y), new Vector2(_currentDirection.x, _currentDirection.y), _currentLengthTarget, _laserMaskValue, z - 0.1f, z + 0.1f);
		if (lastHit.collider != null)
		{
			_wasHitLastFrame = true;
			_laserLength = (lastHit.point - new Vector2(position.x, position.y)).magnitude;
			if (!collisionParticles.isPlaying)
			{
				collisionParticles.Play();
			}
			if (lastHit.collider.gameObject.layer == PixelPlayerController.instance.colliderLayerValue && !PixelPlayerController.instance.isInvincible)
			{
				if (!PixelPlayerController.instance.isTakingDamage)
				{
					PixelPlayerController.instance.lastHitPosition = new Vector3(lastHit.point.x, lastHit.point.y, position2.z);
					PixelPlayerController.instance.HitPlayer(_controller.projectileDamage);
				}
			}
			else
			{
				HandleTileCollision(lastHit);
			}
		}
		else
		{
			_laserLength = _currentLengthTarget;
			if (_wasHitLastFrame)
			{
				_previouslyHitObject = null;
				_wasHitLastFrame = false;
				collisionParticles.Stop();
			}
		}
		_currentLengthTarget = Mathf.Clamp(_laserLength + Time.fixedDeltaTime * _LaserMaxLength / _ExtensionDuration, 0f, _LaserMaxLength);
		selfTransform.localScale = new Vector3(_laserLength / _tileTransform.localScale.y, 1f, 1f);
	}

	private void HandleTileCollision(RaycastHit2D lastHit)
	{
		LayerMask layerMask = lastHit.collider.gameObject.layer;
		Rigidbody2D attachedRigidbody = lastHit.collider.attachedRigidbody;
		BloxelTile bloxelTile = null;
		if (_previouslyHitObject != attachedRigidbody.gameObject)
		{
			_previouslyHitObject = attachedRigidbody.gameObject;
			_hitCount = 0;
		}
		if (++_hitCount < 15)
		{
			return;
		}
		switch ((long)layerMask)
		{
		case 10L:
			bloxelTile = attachedRigidbody.gameObject.GetComponent<BloxelTile>();
			if (bloxelTile != null && bloxelTile.tileInfo.isLoaded)
			{
				TileExplosionManager.instance.ExplodeChunk(bloxelTile.tileRenderer.currentFrameChunk, bloxelTile.selfTransform, bloxelTile.rigidbodyComponent, selfTransform.position, TileExplosionManager.instance.bulletExplosionForce, TileExplosionManager.instance.bulletExplosionRadius, 0f, new Vector3(bloxelTile.selfTransform.localScale.x, bloxelTile.selfTransform.localScale.y, bloxelTile.selfTransform.localScale.y));
				GameplayBuilder.instance.UnloadTile(bloxelTile, true);
			}
			break;
		case 13L:
			bloxelTile = attachedRigidbody.gameObject.GetComponent<BloxelTile>();
			if (bloxelTile != null && bloxelTile.tileInfo.isLoaded)
			{
				((BloxelPowerUpTile)bloxelTile).SpawnContents();
				TileExplosionManager.instance.ExplodeChunk(bloxelTile.tileRenderer.currentFrameChunk, bloxelTile.selfTransform, bloxelTile.rigidbodyComponent, selfTransform.position, TileExplosionManager.instance.bulletExplosionForce, TileExplosionManager.instance.bulletExplosionRadius, 0f, new Vector3(bloxelTile.selfTransform.localScale.x, bloxelTile.selfTransform.localScale.y, bloxelTile.selfTransform.localScale.y));
				GameplayBuilder.instance.UnloadTile(bloxelTile, true);
			}
			break;
		}
	}
}
