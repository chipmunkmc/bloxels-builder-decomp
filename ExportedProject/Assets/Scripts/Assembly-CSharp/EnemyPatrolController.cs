using UnityEngine;

public sealed class EnemyPatrolController : EnemyController
{
	public EnemyPatrolController(BloxelEnemyTile enemyTile)
	{
		tile = enemyTile;
		base.patrolSpeed = 15f;
		currentFixedUpdateAction = Patrol;
	}

	public override void Enable()
	{
		isEnabled = true;
		tile.rigidbodyComponent.isKinematic = false;
		tile.rigidbodyComponent.gravityScale = 10f;
		if (!Mathf.Approximately(tile.selfTransform.localScale.y, base.targetScale))
		{
			tile.TryToGrow();
		}
	}

	public override void Disable()
	{
		isEnabled = false;
		tile.rigidbodyComponent.isKinematic = true;
		tile.rigidbodyComponent.gravityScale = 0f;
		tile.rigidbodyComponent.velocity = Vector2.zero;
		tile.TryNotToGrow();
	}
}
