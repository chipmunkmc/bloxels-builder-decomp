using System;
using UnityEngine;

public sealed class EnemyProjectile : PoolableComponent
{
	public static string collider2Dlayer = "EnemyProjectile";

	[HideInInspector]
	public int damage;

	private static float _lifetime = 4f;

	private float _expirationTime;

	public Rigidbody2D rigidbodyComponent;

	public Collider2D collider2dComponent;

	public ParticleSystem explosionParticles;

	public LayerMask collisionLayers;

	public ParticleSystem particles;

	private ParticleSystem.ShapeModule _shapeModule;

	private ParticleSystem.EmissionModule _emissionModule;

	private static readonly float _originalEmissionRate = 20f;

	private static readonly float _originalStartLifetime = 1f;

	private static readonly ParticleSystem.MinMaxCurve _zeroEmissionRate = new ParticleSystem.MinMaxCurve(0f);

	private static readonly float _originalParticleSize = 1.5f;

	private static readonly float _originalParticleRadius = 0.75f;

	private Action expirationAction;

	public void SetParticleScale(float enemyScale)
	{
		particles.startSize = _originalParticleSize * enemyScale;
		_shapeModule.radius = _originalParticleRadius * enemyScale;
		explosionParticles.startSize = _originalParticleSize * enemyScale;
	}

	public void InitWithSpeed(float projectileSpeed)
	{
		float num = (projectileSpeed - EnemyController.baseProjectileSpeed) / 135f;
		_emissionModule.rate = new ParticleSystem.MinMaxCurve(_originalEmissionRate + projectileSpeed * num);
		particles.startLifetime = Mathf.Clamp(_originalStartLifetime - _originalStartLifetime * num, 0.08f, 1f);
	}

	public override void EarlyAwake()
	{
		_shapeModule = particles.shape;
		_emissionModule = particles.emission;
		particles.randomSeed = (uint)UnityEngine.Random.Range(int.MinValue, int.MaxValue);
		explosionParticles.randomSeed = (uint)UnityEngine.Random.Range(int.MinValue, int.MaxValue);
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		selfTransform.SetParent(WorldWrapper.instance.runtimeObjectContainer);
		_expirationTime = Time.time + _lifetime;
		expirationAction = KillProjectile;
		rigidbodyComponent.velocity = Vector2.zero;
		rigidbodyComponent.angularVelocity = 0f;
		collider2dComponent.enabled = true;
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		base.gameObject.SetActive(false);
	}

	public override void Despawn()
	{
		GameplayPool.DespawnEnemyProjectile(this);
	}

	private void Update()
	{
		if (Time.time >= _expirationTime)
		{
			expirationAction();
		}
	}

	private void OnCollisionEnter2D(Collision2D collision)
	{
		Explode();
	}

	public void Explode()
	{
		KillProjectile();
		explosionParticles.Emit(30);
	}

	private void KillProjectile()
	{
		_expirationTime = Time.time + 3f;
		expirationAction = Despawn;
		_emissionModule.rate = _zeroEmissionRate;
		collider2dComponent.enabled = false;
	}
}
