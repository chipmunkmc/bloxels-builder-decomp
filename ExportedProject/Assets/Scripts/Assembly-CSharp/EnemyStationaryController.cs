using UnityEngine;

public sealed class EnemyStationaryController : EnemyController
{
	public EnemyStationaryController(BloxelEnemyTile enemyTile)
	{
		tile = enemyTile;
	}

	public override void Enable()
	{
		isEnabled = true;
		playerCheckTimer = playerCheckInterval;
		attackTimer = 1.5f;
		if (!Mathf.Approximately(tile.selfTransform.localScale.y, base.targetScale))
		{
			tile.TryToGrow();
		}
	}

	public override void Disable()
	{
		isEnabled = false;
		tile.TryNotToGrow();
	}
}
