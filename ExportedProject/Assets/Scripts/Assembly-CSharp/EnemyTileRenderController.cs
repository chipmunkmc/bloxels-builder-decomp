using UnityEngine;

public sealed class EnemyTileRenderController : TileRenderController
{
	public override bool shouldGenerateColliders
	{
		get
		{
			return _shouldGenerateColliders;
		}
		set
		{
			if (value == _shouldGenerateColliders)
			{
				return;
			}
			_shouldGenerateColliders = value;
			if (_shouldGenerateColliders)
			{
				if (projectType == ProjectType.Board)
				{
					currentFrameChunk.UpdateChunk(base.bloxelBoard.localID, ref localMeshFilter, ref localCollider);
				}
				else
				{
					BloxelBoard bloxelBoard = animationProject.boards[currentFrameIndex];
					currentFrameChunk.UpdateChunk(bloxelBoard.localID, ref localMeshFilter, ref localCollider);
				}
				localCollider.enabled = true;
				collisionTimer = 0f;
				physicsEnabler.gameObject.layer = LayerMask.NameToLayer("EnemyDamage");
			}
			else
			{
				localCollider.enabled = false;
				physicsEnabler.gameObject.layer = LayerMask.NameToLayer("ColliderGenTrigger");
				physicsEnabler.circleCollider.enabled = true;
			}
		}
	}

	protected override void Awake()
	{
		selfTransform = GetComponent<Transform>();
		bloxelTile = GetComponent<BloxelTile>();
		base.physicsTriggerMaskValue = physicsTriggerMask.value;
	}
}
