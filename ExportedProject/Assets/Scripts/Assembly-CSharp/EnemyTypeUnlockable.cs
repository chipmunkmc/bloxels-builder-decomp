public class EnemyTypeUnlockable : GemUnlockable
{
	public EnemyType enemyType;

	public EnemyTypeUnlockable(EnemyType _enemyType)
	{
		enemyType = _enemyType;
		cost = GemUnlockable.EnemyTypeCost;
		type = Type.EnemyType;
		uiBuyable = UIBuyable.CreateEnemy(enemyType);
	}

	public override bool IsLocked()
	{
		return GemManager.instance.IsEnemyTypeLocked(enemyType);
	}

	public override void Unlock()
	{
		GemManager.instance.UnlockEnemyType(enemyType);
		EventUnlock(this);
	}
}
