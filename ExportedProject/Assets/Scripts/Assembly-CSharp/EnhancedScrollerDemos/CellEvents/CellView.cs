using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.CellEvents
{
	public class CellView : EnhancedScrollerCellView
	{
		private Data _data;

		public Text someTextText;

		public CellButtonTextClickedDelegate cellButtonTextClicked;

		public CellButtonIntegerClickedDelegate cellButtonFixedIntegerClicked;

		public CellButtonIntegerClickedDelegate cellButtonDataIntegerClicked;

		public void SetData(Data data)
		{
			_data = data;
			someTextText.text = ((_data.hour != 0) ? string.Format("{0} 'o clock", _data.hour.ToString()) : "Midnight");
		}

		public void CellButtonText_OnClick(string value)
		{
			if (cellButtonTextClicked != null)
			{
				cellButtonTextClicked(value);
			}
		}

		public void CellButtonFixedInteger_OnClick(int value)
		{
			if (cellButtonFixedIntegerClicked != null)
			{
				cellButtonFixedIntegerClicked(value);
			}
		}

		public void CellButtonDataInteger_OnClick()
		{
			if (cellButtonDataIntegerClicked != null)
			{
				cellButtonDataIntegerClicked(_data.hour);
			}
		}
	}
}
