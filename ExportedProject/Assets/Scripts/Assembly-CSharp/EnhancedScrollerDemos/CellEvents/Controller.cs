using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace EnhancedScrollerDemos.CellEvents
{
	public class Controller : MonoBehaviour, IEnhancedScrollerDelegate
	{
		private List<Data> _data;

		public EnhancedScroller scroller;

		public EnhancedScrollerCellView cellViewPrefab;

		public float cellSize;

		private void Start()
		{
			scroller.Delegate = this;
			LoadData();
		}

		private void LoadData()
		{
			_data = new List<Data>();
			for (int i = 0; i < 24; i++)
			{
				_data.Add(new Data
				{
					hour = i
				});
			}
		}

		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			return _data.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			return cellSize;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			CellView cellView = scroller.GetCellView(cellViewPrefab) as CellView;
			cellView.cellButtonTextClicked = CellButtonTextClicked;
			cellView.cellButtonFixedIntegerClicked = CellButtonFixedIntegerClicked;
			cellView.cellButtonDataIntegerClicked = CellButtonDataIntegerClicked;
			cellView.SetData(_data[dataIndex]);
			return cellView;
		}

		private void CellButtonTextClicked(string value)
		{
		}

		private void CellButtonFixedIntegerClicked(int value)
		{
		}

		private void CellButtonDataIntegerClicked(int value)
		{
		}
	}
}
