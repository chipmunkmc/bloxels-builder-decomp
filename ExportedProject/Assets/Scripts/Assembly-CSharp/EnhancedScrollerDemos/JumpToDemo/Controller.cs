using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.JumpToDemo
{
	public class Controller : MonoBehaviour, IEnhancedScrollerDelegate
	{
		private List<Data> _data;

		public EnhancedScroller vScroller;

		public EnhancedScroller hScroller;

		public InputField jumpIndexInput;

		public Toggle useSpacingToggle;

		public Slider scrollerOffsetSlider;

		public Slider cellOffsetSlider;

		public EnhancedScrollerCellView cellViewPrefab;

		public EnhancedScroller.TweenType vScrollerTweenType;

		public float vScrollerTweenTime;

		public EnhancedScroller.TweenType hScrollerTweenType;

		public float hScrollerTweenTime;

		private void Start()
		{
			vScroller.Delegate = this;
			hScroller.Delegate = this;
			_data = new List<Data>();
			for (int i = 0; i < 1000; i++)
			{
				_data.Add(new Data
				{
					cellText = "Cell Data Index " + i
				});
			}
			vScroller.ReloadData();
			hScroller.ReloadData();
		}

		public void JumpButton_OnClick()
		{
			int result;
			if (int.TryParse(jumpIndexInput.text, out result))
			{
				vScroller.JumpToDataIndex(result, scrollerOffsetSlider.value, cellOffsetSlider.value, useSpacingToggle.isOn, vScrollerTweenType, vScrollerTweenTime);
				hScroller.JumpToDataIndex(result, scrollerOffsetSlider.value, cellOffsetSlider.value, useSpacingToggle.isOn, hScrollerTweenType, hScrollerTweenTime);
			}
		}

		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			return _data.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			if (scroller == vScroller)
			{
				return (dataIndex % 2 != 0) ? 100f : 30f;
			}
			return 200f;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			CellView cellView = scroller.GetCellView(cellViewPrefab) as CellView;
			cellView.name = "Cell " + dataIndex;
			cellView.SetData(_data[dataIndex]);
			return cellView;
		}
	}
}
