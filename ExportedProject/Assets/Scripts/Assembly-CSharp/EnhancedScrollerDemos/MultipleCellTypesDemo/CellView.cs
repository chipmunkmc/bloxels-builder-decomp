using EnhancedUI.EnhancedScroller;

namespace EnhancedScrollerDemos.MultipleCellTypesDemo
{
	public class CellView : EnhancedScrollerCellView
	{
		protected Data _data;

		public virtual void SetData(Data data)
		{
			_data = data;
		}
	}
}
