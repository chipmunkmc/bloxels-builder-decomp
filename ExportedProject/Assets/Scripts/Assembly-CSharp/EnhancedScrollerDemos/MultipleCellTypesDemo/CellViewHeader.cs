using UnityEngine.UI;

namespace EnhancedScrollerDemos.MultipleCellTypesDemo
{
	public class CellViewHeader : CellView
	{
		private HeaderData _headerData;

		public Text categoryText;

		public override void SetData(Data data)
		{
			base.SetData(data);
			_headerData = data as HeaderData;
			categoryText.text = _headerData.category;
		}
	}
}
