using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.MultipleCellTypesDemo
{
	public class CellViewRow : CellView
	{
		private RowData _rowData;

		public Text userNameText;

		public Image userAvatarImage;

		public Text userHighScoreText;

		public override void SetData(Data data)
		{
			base.SetData(data);
			_rowData = data as RowData;
			userNameText.text = _rowData.userName;
			userAvatarImage.sprite = Resources.Load<Sprite>(_rowData.userAvatarSpritePath);
			userHighScoreText.text = string.Format("{0:n0}", _rowData.userHighScore);
		}
	}
}
