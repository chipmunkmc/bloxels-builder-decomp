using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace EnhancedScrollerDemos.MultipleCellTypesDemo
{
	public class MultipleCellTypesDemo : MonoBehaviour, IEnhancedScrollerDelegate
	{
		private SmallList<Data> _data;

		public EnhancedScroller scroller;

		public EnhancedScrollerCellView headerCellViewPrefab;

		public EnhancedScrollerCellView rowCellViewPrefab;

		public EnhancedScrollerCellView footerCellViewPrefab;

		public string resourcePath;

		private void Start()
		{
			scroller.Delegate = this;
			LoadData();
		}

		private void LoadData()
		{
			_data = new SmallList<Data>();
			_data.Add(new HeaderData
			{
				category = "Platinum Players"
			});
			_data.Add(new RowData
			{
				userName = "John Smith",
				userAvatarSpritePath = resourcePath + "/avatar_male",
				userHighScore = 21323199uL
			});
			_data.Add(new RowData
			{
				userName = "Jane Doe",
				userAvatarSpritePath = resourcePath + "/avatar_female",
				userHighScore = 20793219uL
			});
			_data.Add(new RowData
			{
				userName = "Julie Prost",
				userAvatarSpritePath = resourcePath + "/avatar_female",
				userHighScore = 19932132uL
			});
			_data.Add(new FooterData());
			_data.Add(new HeaderData
			{
				category = "Gold Players"
			});
			_data.Add(new RowData
			{
				userName = "Jim Bob",
				userAvatarSpritePath = resourcePath + "/avatar_male",
				userHighScore = 1002132uL
			});
			_data.Add(new RowData
			{
				userName = "Susan Anthony",
				userAvatarSpritePath = resourcePath + "/avatar_female",
				userHighScore = 991234uL
			});
			_data.Add(new FooterData());
			_data.Add(new HeaderData
			{
				category = "Silver Players"
			});
			_data.Add(new RowData
			{
				userName = "Gary Richards",
				userAvatarSpritePath = resourcePath + "/avatar_male",
				userHighScore = 905723uL
			});
			_data.Add(new RowData
			{
				userName = "John Doe",
				userAvatarSpritePath = resourcePath + "/avatar_male",
				userHighScore = 702318uL
			});
			_data.Add(new RowData
			{
				userName = "Lisa Ford",
				userAvatarSpritePath = resourcePath + "/avatar_female",
				userHighScore = 697767uL
			});
			_data.Add(new RowData
			{
				userName = "Jacob Morris",
				userAvatarSpritePath = resourcePath + "/avatar_male",
				userHighScore = 409393uL
			});
			_data.Add(new RowData
			{
				userName = "Carolyn Shephard",
				userAvatarSpritePath = resourcePath + "/avatar_female",
				userHighScore = 104352uL
			});
			_data.Add(new RowData
			{
				userName = "Guy Wilson",
				userAvatarSpritePath = resourcePath + "/avatar_male",
				userHighScore = 88321uL
			});
			_data.Add(new RowData
			{
				userName = "Jackie Jones",
				userAvatarSpritePath = resourcePath + "/avatar_female",
				userHighScore = 20826uL
			});
			_data.Add(new RowData
			{
				userName = "Sally Brewer",
				userAvatarSpritePath = resourcePath + "/avatar_female",
				userHighScore = 17389uL
			});
			_data.Add(new RowData
			{
				userName = "Joe West",
				userAvatarSpritePath = resourcePath + "/avatar_male",
				userHighScore = 2918uL
			});
			_data.Add(new FooterData());
			scroller.ReloadData();
		}

		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			return _data.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			if (_data[dataIndex] is HeaderData)
			{
				return 70f;
			}
			if (_data[dataIndex] is RowData)
			{
				return 100f;
			}
			return 90f;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			CellView cellView;
			if (_data[dataIndex] is HeaderData)
			{
				cellView = scroller.GetCellView(headerCellViewPrefab) as CellViewHeader;
				cellView.name = "[Header] " + (_data[dataIndex] as HeaderData).category;
			}
			else if (_data[dataIndex] is RowData)
			{
				cellView = scroller.GetCellView(rowCellViewPrefab) as CellViewRow;
				cellView.name = "[Row] " + (_data[dataIndex] as RowData).userName;
			}
			else
			{
				cellView = scroller.GetCellView(footerCellViewPrefab) as CellViewFooter;
				cellView.name = "[Footer]";
			}
			cellView.SetData(_data[dataIndex]);
			return cellView;
		}
	}
}
