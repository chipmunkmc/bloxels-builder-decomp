using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.RefreshDemo
{
	public class CellView : EnhancedScrollerCellView
	{
		private Data _data;

		public Text someTextText;

		public RectTransform RectTransform
		{
			get
			{
				return base.gameObject.GetComponent<RectTransform>();
			}
		}

		public void SetData(Data data)
		{
			_data = data;
			RefreshCellView();
		}

		public override void RefreshCellView()
		{
			someTextText.text = _data.someText;
		}
	}
}
