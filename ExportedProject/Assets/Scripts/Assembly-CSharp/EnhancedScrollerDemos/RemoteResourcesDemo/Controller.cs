using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace EnhancedScrollerDemos.RemoteResourcesDemo
{
	public class Controller : MonoBehaviour, IEnhancedScrollerDelegate
	{
		private SmallList<Data> _data;

		public EnhancedScroller scroller;

		public EnhancedScrollerCellView cellViewPrefab;

		private void Start()
		{
			scroller.Delegate = this;
			scroller.cellViewVisibilityChanged = CellViewVisibilityChanged;
			_data = new SmallList<Data>();
			for (int i = 0; i <= 12; i++)
			{
				_data.Add(new Data
				{
					imageUrl = string.Format("http://echo17.com/support/enhancedscroller/{0}.jpg", i),
					imageDimensions = new Vector2(200f, 200f)
				});
			}
			scroller.ReloadData();
		}

		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			return _data.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			return 260f;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			CellView cellView = scroller.GetCellView(cellViewPrefab) as CellView;
			cellView.name = "Cell " + dataIndex;
			return cellView;
		}

		private void CellViewVisibilityChanged(EnhancedScrollerCellView cellView)
		{
			CellView cellView2 = cellView as CellView;
			if (cellView.active)
			{
				cellView2.SetData(_data[cellView.dataIndex]);
			}
			else
			{
				cellView2.ClearImage();
			}
		}
	}
}
