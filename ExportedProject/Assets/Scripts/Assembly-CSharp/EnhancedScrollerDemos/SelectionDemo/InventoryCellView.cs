using System;
using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.SelectionDemo
{
	public class InventoryCellView : EnhancedScrollerCellView
	{
		private InventoryData _data;

		public Image selectionPanel;

		public Text itemNameText;

		public Text itemCostText;

		public Text itemDamageText;

		public Text itemDefenseText;

		public Text itemWeightText;

		public Text itemDescriptionText;

		public Image image;

		public Color selectedColor;

		public Color unSelectedColor;

		public SelectedDelegate selected;

		public int DataIndex { get; private set; }

		private void OnDestroy()
		{
			if (_data != null)
			{
				InventoryData data = _data;
				data.selectedChanged = (SelectedChangedDelegate)Delegate.Remove(data.selectedChanged, new SelectedChangedDelegate(SelectedChanged));
			}
		}

		public void SetData(int dataIndex, InventoryData data, bool isVertical)
		{
			if (_data != null)
			{
				InventoryData data2 = _data;
				data2.selectedChanged = (SelectedChangedDelegate)Delegate.Remove(data2.selectedChanged, new SelectedChangedDelegate(SelectedChanged));
			}
			DataIndex = dataIndex;
			_data = data;
			itemNameText.text = data.itemName;
			if (itemCostText != null)
			{
				itemCostText.text = ((data.itemCost <= 0) ? "-" : data.itemCost.ToString());
			}
			if (itemDamageText != null)
			{
				itemDamageText.text = ((data.itemDamage <= 0) ? "-" : data.itemDamage.ToString());
			}
			if (itemDefenseText != null)
			{
				itemDefenseText.text = ((data.itemDefense <= 0) ? "-" : data.itemDefense.ToString());
			}
			if (itemWeightText != null)
			{
				itemWeightText.text = ((data.itemWeight <= 0) ? "-" : data.itemWeight.ToString());
			}
			if (isVertical)
			{
				itemDescriptionText.text = data.itemDescription;
			}
			image.sprite = Resources.Load<Sprite>(data.spritePath + ((!isVertical) ? "_h" : "_v"));
			InventoryData data3 = _data;
			data3.selectedChanged = (SelectedChangedDelegate)Delegate.Remove(data3.selectedChanged, new SelectedChangedDelegate(SelectedChanged));
			InventoryData data4 = _data;
			data4.selectedChanged = (SelectedChangedDelegate)Delegate.Combine(data4.selectedChanged, new SelectedChangedDelegate(SelectedChanged));
			SelectedChanged(data.Selected);
		}

		private void SelectedChanged(bool selected)
		{
			selectionPanel.color = ((!selected) ? unSelectedColor : selectedColor);
		}

		public void OnSelected()
		{
			if (selected != null)
			{
				selected(this);
			}
		}
	}
}
