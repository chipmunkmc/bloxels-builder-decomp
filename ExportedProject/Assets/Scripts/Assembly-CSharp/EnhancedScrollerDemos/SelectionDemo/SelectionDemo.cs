using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.SelectionDemo
{
	public class SelectionDemo : MonoBehaviour, IEnhancedScrollerDelegate
	{
		private SmallList<InventoryData> _data;

		public EnhancedScroller vScroller;

		public EnhancedScroller hScroller;

		public EnhancedScrollerCellView vCellViewPrefab;

		public EnhancedScrollerCellView hCellViewPrefab;

		public Image selectedImage;

		public Text selectedImageText;

		public string resourcePath;

		private void Awake()
		{
			Toggle component = GameObject.Find("Mask Toggle").GetComponent<Toggle>();
			MaskToggle_OnValueChanged(component.isOn);
			Toggle component2 = GameObject.Find("Loop Toggle").GetComponent<Toggle>();
			LoopToggle_OnValueChanged(component2.isOn);
			CellViewSelected(null);
		}

		private void Start()
		{
			vScroller.Delegate = this;
			hScroller.Delegate = this;
			Reload();
		}

		private void Reload()
		{
			if (_data != null)
			{
				for (int i = 0; i < _data.Count; i++)
				{
					_data[i].selectedChanged = null;
				}
			}
			_data = new SmallList<InventoryData>();
			_data.Add(new InventoryData
			{
				itemName = "Sword",
				itemCost = 123,
				itemDamage = 50,
				itemDefense = 0,
				itemWeight = 10,
				spritePath = resourcePath + "/sword",
				itemDescription = "Broadsword with a double-edged blade"
			});
			_data.Add(new InventoryData
			{
				itemName = "Shield",
				itemCost = 80,
				itemDamage = 0,
				itemDefense = 60,
				itemWeight = 50,
				spritePath = resourcePath + "/shield",
				itemDescription = "Steel shield to deflect your enemy's blows"
			});
			_data.Add(new InventoryData
			{
				itemName = "Amulet",
				itemCost = 260,
				itemDamage = 0,
				itemDefense = 0,
				itemWeight = 1,
				spritePath = resourcePath + "/amulet",
				itemDescription = "Magic amulet restores your health points gradually over time"
			});
			_data.Add(new InventoryData
			{
				itemName = "Helmet",
				itemCost = 50,
				itemDamage = 0,
				itemDefense = 20,
				itemWeight = 20,
				spritePath = resourcePath + "/helmet",
				itemDescription = "Standard helm will decrease your vulnerability"
			});
			_data.Add(new InventoryData
			{
				itemName = "Boots",
				itemCost = 40,
				itemDamage = 0,
				itemDefense = 10,
				itemWeight = 5,
				spritePath = resourcePath + "/boots",
				itemDescription = "Boots of speed will double your movement points"
			});
			_data.Add(new InventoryData
			{
				itemName = "Bracers",
				itemCost = 30,
				itemDamage = 0,
				itemDefense = 20,
				itemWeight = 10,
				spritePath = resourcePath + "/bracers",
				itemDescription = "Bracers will upgrade your overall armor"
			});
			_data.Add(new InventoryData
			{
				itemName = "Crossbow",
				itemCost = 100,
				itemDamage = 40,
				itemDefense = 0,
				itemWeight = 30,
				spritePath = resourcePath + "/crossbow",
				itemDescription = "Crossbow can attack from long range"
			});
			_data.Add(new InventoryData
			{
				itemName = "Fire Ring",
				itemCost = 300,
				itemDamage = 100,
				itemDefense = 0,
				itemWeight = 1,
				spritePath = resourcePath + "/fireRing",
				itemDescription = "Fire ring gives you the magical ability to cast fireball spells"
			});
			_data.Add(new InventoryData
			{
				itemName = "Knapsack",
				itemCost = 22,
				itemDamage = 0,
				itemDefense = 0,
				itemWeight = 0,
				spritePath = resourcePath + "/knapsack",
				itemDescription = "Knapsack will increase your carrying capacity by twofold"
			});
			vScroller.ReloadData();
			hScroller.ReloadData();
		}

		private void CellViewSelected(EnhancedScrollerCellView cellView)
		{
			if (cellView == null)
			{
				selectedImage.gameObject.SetActive(false);
				selectedImageText.text = "None";
				return;
			}
			int dataIndex = (cellView as InventoryCellView).DataIndex;
			for (int i = 0; i < _data.Count; i++)
			{
				_data[i].Selected = dataIndex == i;
			}
			selectedImage.gameObject.SetActive(true);
			selectedImage.sprite = Resources.Load<Sprite>(_data[dataIndex].spritePath + "_v");
			selectedImageText.text = _data[dataIndex].itemName;
		}

		public void MaskToggle_OnValueChanged(bool val)
		{
			vScroller.GetComponent<Mask>().enabled = val;
			hScroller.GetComponent<Mask>().enabled = val;
		}

		public void LoopToggle_OnValueChanged(bool val)
		{
			vScroller.Loop = val;
			hScroller.Loop = val;
		}

		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			return _data.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			if (scroller == vScroller)
			{
				return 320f;
			}
			return 150f;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			InventoryCellView inventoryCellView = scroller.GetCellView((!(scroller == vScroller)) ? hCellViewPrefab : vCellViewPrefab) as InventoryCellView;
			inventoryCellView.name = ((!(scroller == vScroller)) ? "Horizontal" : "Vertical") + " " + _data[dataIndex].itemName;
			inventoryCellView.selected = CellViewSelected;
			inventoryCellView.SetData(dataIndex, _data[dataIndex], scroller == vScroller);
			return inventoryCellView;
		}
	}
}
