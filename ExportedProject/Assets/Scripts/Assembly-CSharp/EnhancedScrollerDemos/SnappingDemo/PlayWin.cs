using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.SnappingDemo
{
	public class PlayWin : MonoBehaviour
	{
		private Transform _transform;

		private float _timeLeft;

		public Text scoreText;

		public float zoomTime;

		public float holdTime;

		public float unZoomTime;

		private void Awake()
		{
			_transform = base.transform;
			_transform.localScale = Vector3.zero;
		}

		public void Play(int score)
		{
			scoreText.text = string.Format("{0:n0}", score);
			base.transform.localScale = Vector3.zero;
			_timeLeft = zoomTime;
			StartCoroutine(PlayZoom());
		}

		private IEnumerator PlayZoom()
		{
			while (_timeLeft > 0f)
			{
				_timeLeft -= Time.deltaTime;
				_transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, (zoomTime - _timeLeft) / zoomTime);
				yield return null;
			}
			base.transform.localScale = Vector3.one;
			_timeLeft = holdTime;
			StartCoroutine(PlayHold());
		}

		private IEnumerator PlayHold()
		{
			while (_timeLeft > 0f)
			{
				_timeLeft -= Time.deltaTime;
				yield return null;
			}
			_timeLeft = unZoomTime;
			StartCoroutine(PlayUnZoom());
		}

		private IEnumerator PlayUnZoom()
		{
			while (_timeLeft > 0f)
			{
				_timeLeft -= Time.deltaTime;
				_transform.localScale = Vector3.Lerp(Vector3.one, Vector3.zero, (unZoomTime - _timeLeft) / unZoomTime);
				yield return null;
			}
			base.transform.localScale = Vector3.zero;
		}
	}
}
