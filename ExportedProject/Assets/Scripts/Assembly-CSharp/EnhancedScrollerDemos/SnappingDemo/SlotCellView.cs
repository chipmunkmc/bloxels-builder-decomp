using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.SnappingDemo
{
	public class SlotCellView : EnhancedScrollerCellView
	{
		public Image slotImage;

		public void SetData(SlotData data)
		{
			if (data.sprite == null)
			{
				slotImage.color = new Color(0f, 0f, 0f, 0f);
				return;
			}
			slotImage.sprite = data.sprite;
			slotImage.color = Color.white;
		}
	}
}
