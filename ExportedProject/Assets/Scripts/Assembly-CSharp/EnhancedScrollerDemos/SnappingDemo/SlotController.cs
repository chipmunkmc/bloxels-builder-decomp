using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace EnhancedScrollerDemos.SnappingDemo
{
	public class SlotController : MonoBehaviour, IEnhancedScrollerDelegate
	{
		private SmallList<SlotData> _data;

		public EnhancedScroller scroller;

		public EnhancedScrollerCellView slotCellViewPrefab;

		private void Awake()
		{
			_data = new SmallList<SlotData>();
		}

		private void Start()
		{
			scroller.Delegate = this;
		}

		public void Reload(Sprite[] sprites)
		{
			_data.Clear();
			foreach (Sprite sprite in sprites)
			{
				_data.Add(new SlotData
				{
					sprite = sprite
				});
			}
			scroller.ReloadData();
		}

		public void AddVelocity(float amount)
		{
			scroller.LinearVelocity = amount;
		}

		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			return _data.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			return 150f;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			SlotCellView slotCellView = scroller.GetCellView(slotCellViewPrefab) as SlotCellView;
			slotCellView.SetData(_data[dataIndex]);
			return slotCellView;
		}
	}
}
