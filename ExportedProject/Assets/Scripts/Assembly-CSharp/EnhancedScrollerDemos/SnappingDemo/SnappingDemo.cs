using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.SnappingDemo
{
	public class SnappingDemo : MonoBehaviour
	{
		private enum GameStateEnum
		{
			Initializing = 0,
			Playing = 1,
			GameOver = 2
		}

		private SlotController[] _slotControllers;

		private int[] _snappedDataIndices;

		private int _credits;

		private int _snapCount;

		private GameStateEnum _gameState;

		public float minVelocity;

		public float maxVelocity;

		public int cherryIndex;

		public int sevenIndex;

		public int tripleBarIndex;

		public int bigWinIndex;

		public int blankIndex;

		public Sprite[] slotSprites;

		public Button pullLeverButton;

		public Text creditsText;

		public int startingCredits;

		public GameObject playingPanel;

		public GameObject gameOverPanel;

		public PlayWin playWin;

		private int Credits
		{
			get
			{
				return _credits;
			}
			set
			{
				_credits = ((value >= 0) ? value : 0);
				creditsText.text = string.Format("{0:n0}", _credits);
				pullLeverButton.gameObject.SetActive(_credits > 0);
			}
		}

		private GameStateEnum GameState
		{
			get
			{
				return _gameState;
			}
			set
			{
				_gameState = value;
				switch (_gameState)
				{
				case GameStateEnum.Playing:
				{
					SlotController[] slotControllers = _slotControllers;
					foreach (SlotController slotController in slotControllers)
					{
						slotController.scroller.snapping = true;
					}
					Credits = startingCredits;
					playingPanel.SetActive(true);
					gameOverPanel.SetActive(false);
					break;
				}
				case GameStateEnum.GameOver:
					playingPanel.SetActive(false);
					gameOverPanel.SetActive(true);
					break;
				}
			}
		}

		private void Awake()
		{
			GameState = GameStateEnum.Initializing;
			_slotControllers = base.gameObject.GetComponentsInChildren<SlotController>();
			_snappedDataIndices = new int[_slotControllers.Length];
			SlotController[] slotControllers = _slotControllers;
			foreach (SlotController slotController in slotControllers)
			{
				slotController.scroller.scrollerSnapped = ScrollerSnapped;
			}
		}

		private void Start()
		{
			SlotController[] slotControllers = _slotControllers;
			foreach (SlotController slotController in slotControllers)
			{
				slotController.Reload(slotSprites);
			}
		}

		private void LateUpdate()
		{
			if (GameState == GameStateEnum.Initializing)
			{
				GameState = GameStateEnum.Playing;
			}
		}

		public void PullLeverButton_OnClick()
		{
			_snapCount = 0;
			Credits--;
			pullLeverButton.interactable = false;
			SlotController[] slotControllers = _slotControllers;
			foreach (SlotController slotController in slotControllers)
			{
				slotController.AddVelocity(((!(Random.Range(0f, 1f) > 0.5f)) ? (-1f) : 1f) * Random.Range(minVelocity, maxVelocity));
			}
		}

		public void ResetButton_OnClick()
		{
			GameState = GameStateEnum.Playing;
		}

		private void ScrollerSnapped(EnhancedScroller scroller, int cellIndex, int dataIndex)
		{
			if (GameState == GameStateEnum.Playing)
			{
				_snapCount++;
				_snappedDataIndices[_snapCount - 1] = dataIndex;
				if (_snapCount == _slotControllers.Length)
				{
					TallyScore();
					pullLeverButton.interactable = true;
				}
				if (Credits == 0)
				{
					GameState = GameStateEnum.GameOver;
				}
			}
		}

		private void TallyScore()
		{
			_snapCount = 0;
			int num = 0;
			int num2 = _snappedDataIndices[0];
			int num3 = _snappedDataIndices[1];
			int num4 = _snappedDataIndices[2];
			if (num2 == blankIndex || num3 == blankIndex || num4 == blankIndex)
			{
				num = 0;
			}
			else if (num2 == num3 && num2 == num4)
			{
				num = ((num2 == sevenIndex) ? 1000 : ((num2 == bigWinIndex) ? 150 : ((num2 == tripleBarIndex) ? 70 : ((num2 != cherryIndex) ? 20 : 40))));
			}
			else if (num2 == cherryIndex || num3 == cherryIndex || num4 == cherryIndex)
			{
				num = 3;
			}
			if (num > 0)
			{
				Credits += num;
				playWin.Play(num);
			}
		}
	}
}
