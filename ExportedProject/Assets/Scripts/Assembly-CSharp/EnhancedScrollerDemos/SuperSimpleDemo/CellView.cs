using EnhancedUI.EnhancedScroller;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.SuperSimpleDemo
{
	public class CellView : EnhancedScrollerCellView
	{
		public Text someTextText;

		public void SetData(Data data)
		{
			someTextText.text = data.someText;
		}
	}
}
