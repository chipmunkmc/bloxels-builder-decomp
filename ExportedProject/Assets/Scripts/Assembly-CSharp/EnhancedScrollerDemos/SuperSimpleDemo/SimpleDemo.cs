using EnhancedUI;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace EnhancedScrollerDemos.SuperSimpleDemo
{
	public class SimpleDemo : MonoBehaviour, IEnhancedScrollerDelegate
	{
		private SmallList<Data> _data;

		public EnhancedScroller scroller;

		public EnhancedScrollerCellView cellViewPrefab;

		private void Start()
		{
			scroller.Delegate = this;
			LoadLargeData();
		}

		private void LoadLargeData()
		{
			_data = new SmallList<Data>();
			for (int i = 0; i < 1000; i++)
			{
				_data.Add(new Data
				{
					someText = "Cell Data Index " + i
				});
			}
			scroller.ReloadData();
		}

		private void LoadSmallData()
		{
			_data = new SmallList<Data>();
			_data.Add(new Data
			{
				someText = "A"
			});
			_data.Add(new Data
			{
				someText = "B"
			});
			_data.Add(new Data
			{
				someText = "C"
			});
			scroller.ReloadData();
		}

		public void LoadLargeDataButton_OnClick()
		{
			LoadLargeData();
		}

		public void LoadSmallDataButton_OnClick()
		{
			LoadSmallData();
		}

		public int GetNumberOfCells(EnhancedScroller scroller)
		{
			return _data.Count;
		}

		public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
		{
			return (dataIndex % 2 != 0) ? 100f : 30f;
		}

		public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
		{
			CellView cellView = scroller.GetCellView(cellViewPrefab) as CellView;
			cellView.name = "Cell " + dataIndex;
			cellView.SetData(_data[dataIndex]);
			return cellView;
		}
	}
}
