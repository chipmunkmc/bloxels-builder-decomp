using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

namespace EnhancedScrollerDemos.ViewDrivenCellSizes
{
	public class CellView : EnhancedScrollerCellView
	{
		public Text someTextText;

		public RectTransform textRectTransform;

		public RectOffset textBuffer;

		public void SetData(Data data)
		{
			someTextText.text = data.someText;
			float num = textRectTransform.sizeDelta.y;
			if (num > 0f)
			{
				num += (float)(textBuffer.top + textBuffer.bottom);
			}
			data.cellSize = num;
		}
	}
}
