using System;
using System.Collections;
using UnityEngine;

public class ExchangeManager : MonoBehaviour
{
	public static ExchangeManager instance;

	public string configURL;

	public int boardCostMultiplier = 2;

	public int CalculateValue(int bloxelsUsed)
	{
		return boardCostMultiplier * bloxelsUsed;
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Start()
	{
		configURL = BloxelServerInterface.instance.apiURL + "/" + BloxelServerInterface.configEndpoint + "/" + BloxelServerInterface.serverToken;
		StartCoroutine(GetConfig(configURL, delegate(string res)
		{
			if (!string.IsNullOrEmpty(res) && res != "ERROR")
			{
				JSONObject jSONObject = new JSONObject(res);
				if (jSONObject.HasField("exchangeMultiplier"))
				{
					boardCostMultiplier = (int)jSONObject.GetField("exchangeMultiplier").n;
				}
			}
		}));
	}

	public IEnumerator GetConfig(string _url, Action<string> callback)
	{
		WWW www = new WWW(_url);
		yield return www;
		if (www.error == null)
		{
			callback(www.text);
		}
		else
		{
			callback(www.error);
		}
		yield return null;
	}
}
