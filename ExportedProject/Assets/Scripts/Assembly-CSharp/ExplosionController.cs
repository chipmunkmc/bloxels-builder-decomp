using UnityEngine;
using VoxelEngine;

public class ExplosionController
{
	public struct ExpiringCube
	{
		public ExplosionCube cube;

		public float expirationTime;

		public ExpiringCube(ExplosionCube cube, float expirationTime)
		{
			this.cube = cube;
			this.expirationTime = expirationTime;
		}
	}

	public ExpiringCube[] cubesToRecycle;

	public float explosionClock;

	public int nextCubeIndex;

	public bool isExploding;

	public int totalNumberOfCubes;

	private static Vector2[] uvs = new Vector2[24];

	private static Vector2[] singleFaceUvs = new Vector2[4];

	private float lifetimeMin = 0.4f;

	private float lifetimeMax = 2.4f;

	private static int _randomForceAndTorqueCount = 64;

	private static Vector3[] _forces;

	private static Vector3[] _torques;

	private static float _forceMultiplier = 18f;

	private static float _torqueMultiplier = 10f;

	public ExplosionController()
	{
		totalNumberOfCubes = Chunk2D.chunkSize * Chunk2D.chunkSize;
		cubesToRecycle = new ExpiringCube[totalNumberOfCubes];
	}

	public static void InitRandomForceAndTorqueLookups()
	{
		_forces = new Vector3[_randomForceAndTorqueCount];
		_torques = new Vector3[_randomForceAndTorqueCount];
		for (int i = 0; i < _randomForceAndTorqueCount; i++)
		{
			_forces[i] = Random.insideUnitSphere * _forceMultiplier;
			_torques[i] = Random.insideUnitSphere * _torqueMultiplier;
		}
	}

	public void ExplodeBetter(Chunk2D chunk, Transform chunkMeshTransform, Rigidbody2D attachedRigidbody, Vector3 explosionPosition, ref int[] colIndices, ref int[] rowIndices, float explosionForce, float explosionRadius, float upwardsModifier, Vector3 scale)
	{
		isExploding = true;
		totalNumberOfCubes = chunk.numSolidBlocks;
		TileExplosionManager.instance.PrepareCubesForExplosion(this);
		TileExplosionManager.instance.AddActiveExplosion(this);
		float num = Time.time + lifetimeMin;
		float num2 = (lifetimeMax - lifetimeMin) / (float)totalNumberOfCubes;
		float min = num2 * 0.75f;
		float max = num2 * 1.25f;
		Vector3 position = chunkMeshTransform.position;
		Vector3 velocity = attachedRigidbody.velocity;
		float x = position.x;
		float y = position.y;
		float z = position.z;
		int num3 = 0;
		int num4 = 0;
		int num5 = 0;
		int num6 = 0;
		for (int i = 0; i < Chunk2D.chunkSize; i++)
		{
			for (int j = 0; j < Chunk2D.chunkSize; j++)
			{
				num4 = colIndices[j];
				num5 = colIndices[i];
				BlockSolidColor2D blockSolidColor2D = chunk.blocks[num5 * Chunk2D.chunkSize + num4] as BlockSolidColor2D;
				if (!blockSolidColor2D.isBlank)
				{
					ExplosionCube explosionCube = CubePool.instance.Spawn(blockSolidColor2D.locationInPalette);
					explosionCube.selfTransform.localPosition = new Vector3(x + scale.x * (float)num4, y + scale.y * (float)num5, z);
					explosionCube.selfTransform.localScale = scale;
					num6 = Random.Range(0, _randomForceAndTorqueCount);
					explosionCube.rigidbody3D.velocity = velocity;
					explosionCube.rigidbody3D.AddForce(_forces[num6], ForceMode.Impulse);
					explosionCube.rigidbody3D.AddTorque(_torques[num6], ForceMode.Impulse);
					explosionCube.rigidbody3D.AddExplosionForce(explosionForce, explosionPosition, explosionRadius, upwardsModifier, ForceMode.Impulse);
					num += Random.Range(min, max);
					cubesToRecycle[num3++] = new ExpiringCube(explosionCube, num);
				}
			}
		}
	}

	public int RecycleSomeExplosionCubes(int numToRecycle)
	{
		if (!isExploding)
		{
			return numToRecycle;
		}
		for (int i = 0; i < numToRecycle; i++)
		{
			if (nextCubeIndex < totalNumberOfCubes)
			{
				explosionClock = cubesToRecycle[nextCubeIndex].expirationTime;
				ExplosionCube cube = cubesToRecycle[nextCubeIndex].cube;
				cube.Despawn();
				nextCubeIndex++;
				continue;
			}
			return numToRecycle - i;
		}
		return 0;
	}
}
