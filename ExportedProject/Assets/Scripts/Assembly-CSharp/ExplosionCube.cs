using UnityEngine;

public sealed class ExplosionCube : PoolableComponent
{
	public MeshFilter meshFilter;

	public Rigidbody rigidbody3D;

	public Vector3 hiddenPosition;

	public override void EarlyAwake()
	{
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		rigidbody3D.isKinematic = false;
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		rigidbody3D.isKinematic = true;
		selfTransform.rotation = Quaternion.identity;
		rigidbody3D.velocity = Vector3.zero;
		rigidbody3D.angularVelocity = Vector3.zero;
		selfTransform.localPosition = hiddenPosition;
	}

	public override void Despawn()
	{
		PrepareDespawn();
		CubePool.instance.pool.Push(this);
	}
}
