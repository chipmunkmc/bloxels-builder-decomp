using UnityEngine;

public class ExplosionFlash : MonoBehaviour
{
	public Transform selfTransform;

	public ParticleSystem explosionParticles;

	public ParticleSystem flashParticles;

	private float _expirationTime;

	public static readonly float OriginalGravityModifier = 0.5f;

	public void PlayFlash()
	{
		_expirationTime = Time.time + flashParticles.duration;
		flashParticles.Play();
	}

	public void PlayPixelParticles()
	{
		_expirationTime = Time.time + explosionParticles.duration;
		explosionParticles.Play();
	}

	public void PlayAll()
	{
		_expirationTime = Time.time + Mathf.Max(flashParticles.duration, explosionParticles.duration);
		explosionParticles.Play();
		flashParticles.Play();
	}

	private void Update()
	{
		if (Time.time > _expirationTime)
		{
			base.gameObject.Recycle();
		}
	}
}
