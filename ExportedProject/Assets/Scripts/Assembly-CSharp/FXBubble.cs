using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FXBubble : MonoBehaviour
{
	public RectTransform rect;

	public Image uiImage;

	public Color[] colors;

	public ParticleSystem ps;

	public Transform cube;

	private ParticleSystem.Particle[] particles;

	public float minRectSize = 20f;

	public float maxRectSize = 40f;

	public int maxParticles = 20;

	private Vector2 _startPosition;

	private void Start()
	{
		particles = new ParticleSystem.Particle[ps.maxParticles];
		colors = new Color[7]
		{
			AssetManager.instance.bloxelBlue,
			AssetManager.instance.bloxelOrange,
			AssetManager.instance.bloxelYellow,
			AssetManager.instance.bloxelGreen,
			AssetManager.instance.bloxelRed,
			AssetManager.instance.bloxelPink,
			AssetManager.instance.bloxelPurple
		};
		_startPosition = rect.anchoredPosition;
		Init();
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.A))
		{
			Init();
			Animate();
		}
	}

	private void Init()
	{
		float num = Random.Range(minRectSize, maxRectSize);
		rect.DOKill();
		rect.anchoredPosition = _startPosition;
		rect.sizeDelta = new Vector2(num, num);
		cube.DOKill();
		cube.localScale = Vector3.zero;
		cube.localPosition = Vector3.zero;
		uiImage.DOKill();
		uiImage.DOFade(1f, 0f);
	}

	public void Animate()
	{
		Stretch().Play().OnComplete(delegate
		{
			Burp();
			uiImage.DOFade(0f, UIAnimationManager.speedFast).OnStart(delegate
			{
				cube.DOScale(20f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic).OnComplete(delegate
				{
					cube.DORotate(new Vector3(cube.localRotation.x + (float)Random.Range(50, 100), cube.localRotation.y + (float)Random.Range(50, 100), cube.localRotation.z + (float)Random.Range(50, 100)), UIAnimationManager.speedSlow).SetLoops(-1, LoopType.Incremental);
					cube.DOMove(new Vector3(cube.localPosition.x + (float)Random.Range(-20, 20), cube.localPosition.y + 400f, cube.localPosition.z), 30f);
				});
			});
		});
	}

	private Sequence Stretch()
	{
		Tween t = rect.DOScaleY(2f, 0f);
		Tween t2 = rect.DOAnchorPos(new Vector2(rect.anchoredPosition.x - rect.sizeDelta.x / 2f, rect.anchoredPosition.y), UIAnimationManager.speedMedium);
		Tween t3 = rect.DOScale(new Vector3(2f, 1f, 1f), UIAnimationManager.speedMedium);
		Tween t4 = rect.DOAnchorPos(new Vector2(rect.anchoredPosition.x - rect.sizeDelta.x * 2f, rect.anchoredPosition.y), UIAnimationManager.speedMedium);
		Tween t5 = rect.DOScale(1f, UIAnimationManager.speedFast);
		Tween t6 = rect.DOShakeScale(UIAnimationManager.speedMedium);
		Sequence sequence = DOTween.Sequence();
		sequence.Append(t);
		sequence.Append(t2);
		sequence.Insert(t.Duration(), t3);
		sequence.Insert(t.Duration() + t2.Duration(), t4);
		sequence.Insert(t.Duration() + t2.Duration() + t3.Duration(), t5);
		sequence.Insert(t.Duration() + t2.Duration() + t3.Duration() + t4.Duration(), t6);
		return sequence;
	}

	private void Burp()
	{
		ps.Emit(maxParticles);
		int num = ps.GetParticles(particles);
		for (int i = 0; i < num; i++)
		{
			Color color = particles[i].color;
			particles[i].color = colors[Random.Range(0, colors.Length)];
		}
		ps.SetParticles(particles, num);
	}
}
