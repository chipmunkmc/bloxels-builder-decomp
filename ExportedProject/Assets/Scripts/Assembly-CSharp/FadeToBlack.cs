using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class FadeToBlack : MonoBehaviour
{
	private Image _uiImage;

	private void Awake()
	{
		_uiImage = GetComponent<Image>();
		Reset();
	}

	public Tweener Run()
	{
		return _uiImage.DOFade(1f, UIAnimationManager.speedSlowAsCrap);
	}

	public Tweener ResetWithFade()
	{
		return _uiImage.DOFade(0f, UIAnimationManager.speedFast);
	}

	public void FadeToDestroy()
	{
		ResetWithFade().OnComplete(delegate
		{
			Object.Destroy(base.gameObject);
		});
	}

	public void Reset()
	{
		_uiImage.color = new Color(_uiImage.color.r, _uiImage.color.g, _uiImage.color.b, 0f);
	}
}
