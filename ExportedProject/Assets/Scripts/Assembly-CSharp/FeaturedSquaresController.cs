using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using VoxelEngine;

public class FeaturedSquaresController : MonoBehaviour
{
	public enum State
	{
		Featured = 0,
		Filtered = 1
	}

	public static FeaturedSquaresController instance;

	public State currentState;

	private bool _groupsLoaded;

	[Header("| ========= Movable ========= |")]
	public RectTransform rect;

	public Vector2 basePosition;

	public Vector2 hiddenPosition;

	public bool isVisible;

	[Header("| ========= UI ========= |")]
	public Button uiButtonFeatured;

	public Button uiButtonFiltered;

	private RectTransform _rectButtonFeatured;

	private RectTransform _rectButtonFiltered;

	public UIButton uiButtonDismiss;

	public RectTransform rectFeatured;

	public RectTransform rectFiltered;

	private CanvasGroup _uiGroupFeatured;

	private CanvasGroup _uiGroupFiltered;

	public UIFeaturedSquareGroup[] squareGroups;

	public UIFilteredGameSquares[] filteredSquareGroup;

	private ScrollRect _scroller;

	private ScrollRect _filteredGamesScroller;

	public Color inActiveBtnColor;

	public Color activeBtnColor;

	public Sprite spriteBtn;

	private void Awake()
	{
		_groupsLoaded = false;
		if (instance == null)
		{
			instance = this;
		}
		if (squareGroups == null)
		{
			squareGroups = rectFeatured.GetComponentsInChildren<UIFeaturedSquareGroup>();
		}
		if (filteredSquareGroup == null)
		{
			filteredSquareGroup = rectFiltered.GetComponentsInChildren<UIFilteredGameSquares>();
		}
		_scroller = rectFeatured.GetComponentInChildren<ScrollRect>();
		_filteredGamesScroller = rectFiltered.GetComponentInChildren<ScrollRect>();
		_uiGroupFeatured = rectFeatured.GetComponent<CanvasGroup>();
		_uiGroupFiltered = rectFiltered.GetComponent<CanvasGroup>();
		_rectButtonFeatured = uiButtonFeatured.GetComponent<RectTransform>();
		_rectButtonFiltered = uiButtonFiltered.GetComponent<RectTransform>();
	}

	private void LoadGroups()
	{
		if (!_groupsLoaded)
		{
			_groupsLoaded = true;
			for (int i = 0; i < squareGroups.Length; i++)
			{
				squareGroups[i].Init();
			}
			for (int j = 0; j < filteredSquareGroup.Length; j++)
			{
				filteredSquareGroup[j].Init();
			}
			uiButtonFeatured.onClick.AddListener(HandleFeaturedState);
			uiButtonFiltered.onClick.AddListener(HandleFilteredState);
			uiButtonDismiss.OnClick += HandleDismiss;
			base.transform.localScale = Vector3.zero;
		}
	}

	private void OnDestroy()
	{
		uiButtonDismiss.OnClick -= HandleDismiss;
	}

	public void ShowUser(UserAccount user)
	{
		Hide().OnComplete(delegate
		{
			base.transform.localScale = Vector3.zero;
			BloxelLibrary.instance.Show();
			UIUserProfile.instance.InitWithAccount(user);
		});
	}

	public void GoToTile(WorldPos2D coordinate)
	{
		Hide().OnComplete(delegate
		{
			base.transform.localScale = Vector3.zero;
			BloxelLibrary.instance.Show();
			UIWarpDrive.instance.ManualWarp(coordinate.x, coordinate.y);
		});
	}

	public Tweener Show()
	{
		if (isVisible)
		{
			return null;
		}
		LoadGroups();
		if (currentState == State.Featured)
		{
			HandleFeaturedState();
		}
		else
		{
			HandleFilteredState();
		}
		Tweener tweener = null;
		base.transform.localScale = Vector3.one;
		return rect.DOAnchorPos(basePosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isVisible = true;
		}).OnComplete(delegate
		{
			_scroller.verticalNormalizedPosition = 1f;
			BloxelLibrary.instance.Hide();
		});
	}

	public Tweener Hide()
	{
		if (!isVisible)
		{
			return null;
		}
		Tweener tweener = null;
		return rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isVisible = false;
		});
	}

	public void HandleFeaturedState()
	{
		currentState = State.Featured;
		uiButtonFeatured.image.DOColor(activeBtnColor, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiButtonFeatured.image.sprite = spriteBtn;
			uiButtonFiltered.image.sprite = null;
			_rectButtonFeatured.DOSizeDelta(new Vector2(_rectButtonFeatured.sizeDelta.x, 160f), UIAnimationManager.speedFast);
			_rectButtonFiltered.DOSizeDelta(new Vector2(_rectButtonFiltered.sizeDelta.x, 143f), UIAnimationManager.speedFast);
			uiButtonFiltered.image.DOColor(inActiveBtnColor, UIAnimationManager.speedMedium);
		});
		_uiGroupFeatured.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			_uiGroupFeatured.blocksRaycasts = true;
			_uiGroupFeatured.interactable = true;
			_uiGroupFiltered.DOFade(0f, UIAnimationManager.speedMedium);
			_uiGroupFiltered.blocksRaycasts = false;
			_uiGroupFiltered.interactable = false;
		});
	}

	public void HandleFilteredState()
	{
		currentState = State.Filtered;
		uiButtonFiltered.image.DOColor(activeBtnColor, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiButtonFeatured.image.sprite = null;
			uiButtonFiltered.image.sprite = spriteBtn;
			_rectButtonFiltered.DOSizeDelta(new Vector2(_rectButtonFiltered.sizeDelta.x, 160f), UIAnimationManager.speedFast);
			_rectButtonFeatured.DOSizeDelta(new Vector2(_rectButtonFeatured.sizeDelta.x, 143f), UIAnimationManager.speedFast);
			uiButtonFeatured.image.DOColor(inActiveBtnColor, UIAnimationManager.speedMedium);
		});
		_uiGroupFiltered.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			_uiGroupFiltered.blocksRaycasts = true;
			_uiGroupFiltered.interactable = true;
			_uiGroupFeatured.DOFade(0f, UIAnimationManager.speedMedium);
			_uiGroupFeatured.blocksRaycasts = false;
			_uiGroupFeatured.interactable = false;
		});
	}

	private void HandleDismiss()
	{
		Hide().OnComplete(delegate
		{
			base.transform.localScale = Vector3.zero;
			BloxelLibrary.instance.Show();
		});
	}
}
