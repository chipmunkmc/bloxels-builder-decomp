using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class FilterableSquares : MonoBehaviour
{
	public delegate void HandleSquareUpdate(UISimpleSquare simpleSquare);

	public delegate void HandlePopulateComplete();

	[Header("| ========= Data ========= |")]
	public List<string> squareIDs;

	[Header("| ========= UI ========= |")]
	public UISquareFilterToggle[] toggles;

	public Transform transformOverlay;

	public Text uiTextLoadingCount;

	public Button uiButtonResetFilters;

	private Text uiTextTotalCount;

	public Text uiTextCurrentlyViewing;

	public Transform squareGrid;

	public List<ProjectType> currentFilters;

	public List<UISimpleSquare> uiSquares;

	public List<ServerSquare> serverSquares;

	public UnityEngine.Object simpleSquarePrefab;

	public event HandleSquareUpdate OnSquare;

	public event HandlePopulateComplete OnPopulateComplete;

	private void Awake()
	{
		toggles = GetComponentsInChildren<UISquareFilterToggle>();
		uiTextTotalCount = uiButtonResetFilters.GetComponentInChildren<Text>();
	}

	private void Start()
	{
		currentFilters = new List<ProjectType>();
		uiSquares = new List<UISimpleSquare>();
		squareIDs = new List<string>();
		serverSquares = new List<ServerSquare>();
		uiButtonResetFilters.onClick.AddListener(ResetFilters);
	}

	public void Init(List<string> _squaresIDs)
	{
		squareIDs = _squaresIDs;
		uiTextTotalCount.text = squareIDs.Count.ToString();
		Reset();
		StartCoroutine("PopulateSquares");
		ShowOverlay();
	}

	private void ShowOverlay()
	{
		transformOverlay.gameObject.SetActive(true);
		transformOverlay.DOScale(1f, UIAnimationManager.speedFast);
	}

	private void HideOverlay()
	{
		transformOverlay.DOScale(0f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			transformOverlay.gameObject.SetActive(false);
		});
	}

	public void Reset()
	{
		currentFilters.Clear();
		uiSquares.Clear();
		serverSquares.Clear();
		foreach (Transform item in squareGrid)
		{
			UnityEngine.Object.Destroy(item.gameObject);
		}
		for (int i = 0; i < toggles.Length; i++)
		{
			toggles[i].count = 0;
			toggles[i].DeActivate();
			toggles[i].interactable = false;
		}
		SetCurrentViewingLabel();
	}

	private void AdjustScrollRectHeight(Transform t)
	{
		GridLayoutGroup component = t.GetComponent<GridLayoutGroup>();
		RectTransform component2 = t.GetComponent<RectTransform>();
		int num = 0;
		for (int i = 0; i < component.transform.childCount && !(Mathf.Abs(((RectTransform)component.transform.GetChild(i)).anchoredPosition.y) >= Mathf.Abs(component.cellSize.y + component.spacing.y + (float)component.padding.top)); i++)
		{
			num++;
		}
		if (num == 0)
		{
			num = 1;
		}
		int num2 = Mathf.CeilToInt(t.childCount / num);
		float num3 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num2 * num3) + Mathf.Abs(num3);
		component2.sizeDelta = new Vector2(component2.sizeDelta.x, y);
		component2.anchoredPosition = Vector2.zero;
	}

	private void ResetFilters()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		currentFilters.Clear();
		for (int i = 0; i < toggles.Length; i++)
		{
			toggles[i].DeActivate();
		}
		Filter();
	}

	public void AddFilter(ProjectType type)
	{
		if (!currentFilters.Contains(type))
		{
			currentFilters.Add(type);
		}
		Filter();
	}

	public void RemoveFilter(ProjectType type)
	{
		if (currentFilters.Contains(type))
		{
			currentFilters.Remove(type);
		}
		Filter();
	}

	public void Filter()
	{
		if (currentFilters.Count <= 0 || currentFilters.Count == toggles.Length)
		{
			foreach (Transform item in squareGrid)
			{
				if (!item.gameObject.activeInHierarchy)
				{
					item.gameObject.SetActive(true);
				}
			}
			SetCurrentViewingLabel();
			AdjustScrollRectHeight(squareGrid);
			return;
		}
		for (int i = 0; i < uiSquares.Count; i++)
		{
			if (currentFilters.Contains(uiSquares[i].dataModel.type))
			{
				if (!uiSquares[i].gameObject.activeInHierarchy)
				{
					uiSquares[i].gameObject.SetActive(true);
				}
			}
			else
			{
				uiSquares[i].gameObject.SetActive(false);
			}
		}
		AdjustScrollRectHeight(squareGrid);
		SetCurrentViewingLabel();
	}

	private void SetCurrentViewingLabel()
	{
		string empty = string.Empty;
		if (currentFilters.Count <= 0 || currentFilters.Count == toggles.Length)
		{
			empty = LocalizationManager.getInstance().getLocalizedText("gamebuilder134", "All");
		}
		else
		{
			string[] array = new string[currentFilters.Count];
			for (int i = 0; i < currentFilters.Count; i++)
			{
				array[i] = LocalizationManager.getInstance().getLocalizedText(BloxelProject.projectTypePluralizedDisplayKeys[currentFilters[i]]);
			}
			empty = string.Join(", ", array);
		}
		uiTextCurrentlyViewing.text = empty;
	}

	private IEnumerator PopulateSquares()
	{
		int i;
		for (i = 0; i < squareIDs.Count; i++)
		{
			yield return StartCoroutine(BloxelServerRequests.instance.FindSquareByID(squareIDs[i], delegate(string response)
			{
				if (!string.IsNullOrEmpty(response) && response != "ERROR")
				{
					ServerSquare serverSquare = new ServerSquare(response);
					serverSquares.Add(serverSquare);
					GameObject gameObject = UnityEngine.Object.Instantiate(simpleSquarePrefab) as GameObject;
					gameObject.transform.SetParent(squareGrid);
					gameObject.transform.localScale = Vector3.one;
					UISimpleSquare component = gameObject.GetComponent<UISimpleSquare>();
					component.Init(serverSquare);
					AdjustCountForType(serverSquare.type);
					uiSquares.Add(component);
					uiTextLoadingCount.text = LocalizationManager.getInstance().getLocalizedText("iWall100", "Loading Infinity Wall Tile") + " " + (i + 1) + " " + LocalizationManager.getInstance().getLocalizedText("iWall99", "of") + " " + squareIDs.Count;
					UIUserProfile.instance.userStats.uiTextLoadingCount.text = LocalizationManager.getInstance().getLocalizedText("iWall18", "Loading Tile") + " " + (i + 1) + " " + LocalizationManager.getInstance().getLocalizedText("iWall99", "of") + " " + squareIDs.Count;
					if (this.OnSquare != null)
					{
						this.OnSquare(component);
					}
				}
			}));
		}
		if (this.OnPopulateComplete != null)
		{
			this.OnPopulateComplete();
		}
		AdjustScrollRectHeight(squareGrid);
		for (int j = 0; j < toggles.Length; j++)
		{
			toggles[j].interactable = true;
		}
		HideOverlay();
	}

	private void AdjustCountForType(ProjectType type)
	{
		for (int i = 0; i < toggles.Length; i++)
		{
			if (toggles[i].type == type)
			{
				toggles[i].count++;
			}
		}
	}

	public void StopLoading()
	{
		StopCoroutine("PopulateSquares");
		for (int i = 0; i < toggles.Length; i++)
		{
			toggles[i].interactable = true;
		}
		HideOverlay();
		UIUserProfile.instance.userStats.HideOverlay();
	}
}
