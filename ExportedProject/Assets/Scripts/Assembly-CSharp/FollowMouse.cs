using UnityEngine;

public class FollowMouse : MonoBehaviour
{
	private void Update()
	{
		base.transform.position = PPUtilities.GetWorldPositionOnXYPlane(Camera.main, Input.mousePosition, 0f, false);
	}
}
