using System;
using UnityEngine;

public class GSFUJsonHelper
{
	[Serializable]
	private class Wrapper<T>
	{
		public T[] array = new T[0];
	}

	public static T[] JsonArray<T>(string json)
	{
		string json2 = "{ \"array\": " + json + "}";
		Wrapper<T> wrapper = JsonUtility.FromJson<Wrapper<T>>(json2);
		return wrapper.array;
	}
}
