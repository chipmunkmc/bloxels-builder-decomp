using UnityEngine;

public class GSFU_Demo_Runtime : MonoBehaviour
{
	private void OnEnable()
	{
		CloudConnectorCore.processedResponseCallback.AddListener(GSFU_Demo_Utils.ParseData);
	}

	private void OnDisable()
	{
		CloudConnectorCore.processedResponseCallback.RemoveListener(GSFU_Demo_Utils.ParseData);
	}

	private void OnGUI()
	{
		if (GUI.Button(new Rect(20f, 20f, 140f, 25f), "Create Table"))
		{
			GSFU_Demo_Utils.CreatePlayerTable(true);
		}
		if (GUI.Button(new Rect(20f, 60f, 140f, 25f), "Create Player"))
		{
			GSFU_Demo_Utils.SaveGandalf(true);
		}
		if (GUI.Button(new Rect(20f, 100f, 140f, 25f), "Update Player"))
		{
			GSFU_Demo_Utils.UpdateGandalf(true);
		}
		if (GUI.Button(new Rect(20f, 140f, 140f, 25f), "Retrieve Player"))
		{
			GSFU_Demo_Utils.RetrieveGandalf(true);
		}
		if (GUI.Button(new Rect(20f, 180f, 140f, 25f), "Retrieve All Players"))
		{
			GSFU_Demo_Utils.GetAllPlayers(true);
		}
		if (GUI.Button(new Rect(20f, 220f, 140f, 25f), "Retrieve All Tables"))
		{
			GSFU_Demo_Utils.GetAllTables(true);
		}
	}
}
