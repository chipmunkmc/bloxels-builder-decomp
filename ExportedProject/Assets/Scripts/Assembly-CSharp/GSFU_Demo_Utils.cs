using System;
using System.Collections.Generic;
using UnityEngine;

public static class GSFU_Demo_Utils
{
	[Serializable]
	public struct PlayerInfo
	{
		public string name;

		public int level;

		public float health;

		public string role;
	}

	private static PlayerInfo player;

	private static string tableName;

	static GSFU_Demo_Utils()
	{
		player = default(PlayerInfo);
		player.name = "Mithrandir";
		player.level = 99;
		player.health = 98.6f;
		player.role = "Wizzard";
		tableName = "PlayerInfo";
	}

	public static void CreatePlayerTable(bool runtime)
	{
		CloudConnectorCore.CreateTable(new string[4] { "name", "level", "health", "role" }, tableName, runtime);
	}

	public static void SaveGandalf(bool runtime)
	{
		string jsonObject = JsonUtility.ToJson(player);
		CloudConnectorCore.CreateObject(jsonObject, tableName, runtime);
	}

	public static void UpdateGandalf(bool runtime)
	{
		CloudConnectorCore.UpdateObjects(tableName, "name", "Mithrandir", "level", "100", runtime);
	}

	public static void RetrieveGandalf(bool runtime)
	{
		CloudConnectorCore.GetObjectsByField(tableName, "name", "Mithrandir", runtime);
	}

	public static void GetAllPlayers(bool runtime)
	{
		CloudConnectorCore.GetTable(tableName, runtime);
	}

	public static void GetAllTables(bool runtime)
	{
		CloudConnectorCore.GetAllTables(runtime);
	}

	public static void ParseData(CloudConnectorCore.QueryType query, List<string> objTypeNames, List<string> jsonData)
	{
		for (int i = 0; i < objTypeNames.Count; i++)
		{
		}
		if (query == CloudConnectorCore.QueryType.getObjects && string.Compare(objTypeNames[0], tableName) == 0)
		{
			PlayerInfo[] array = GSFUJsonHelper.JsonArray<PlayerInfo>(jsonData[0]);
			player = array[0];
		}
		if (query == CloudConnectorCore.QueryType.getTable && string.Compare(objTypeNames[0], tableName) == 0)
		{
			PlayerInfo[] array2 = GSFUJsonHelper.JsonArray<PlayerInfo>(jsonData[0]);
			string text = "<color=yellow>" + array2.Length + " objects retrieved from the cloud and parsed:</color>";
			for (int j = 0; j < array2.Length; j++)
			{
				string text2 = text;
				text = text2 + "\n<color=blue>Name: " + array2[j].name + "</color>\nLevel: " + array2[j].level + "\nHealth: " + array2[j].health + "\nRole: " + array2[j].role + "\n";
			}
		}
		if (query == CloudConnectorCore.QueryType.getAllTables)
		{
			string text3 = "<color=yellow>All data tables retrieved from the cloud.\n</color>";
			for (int k = 0; k < objTypeNames.Count; k++)
			{
				string text2 = text3;
				text3 = text2 + "<color=blue>Table Name: " + objTypeNames[k] + "</color>\n" + jsonData[k] + "\n";
			}
		}
	}
}
