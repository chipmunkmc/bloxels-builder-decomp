using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class GameAssetTile : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	[Header("| ========= Data ========= |")]
	public BloxelProject dataModel;

	[Header("| ========= UI ========= |")]
	public RectTransform rect;

	public RawImage uiRawImage;

	public CoverBoard cover;

	public CanvasGroup uiCanvasGroup;

	public Image uiImageOutline;

	private bool isDragging;

	public Color typeColor;

	public bool isActive;

	public int boardsUsed;

	public UIProjectCover animatedCover;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		animatedCover = GetComponentInChildren<UIProjectCover>();
		cover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	public void Init(BloxelProject _project)
	{
		dataModel = _project;
		switch (dataModel.type)
		{
		case ProjectType.Board:
			uiRawImage.texture = dataModel.coverBoard.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
			break;
		case ProjectType.Animation:
			boardsUsed = ((BloxelAnimation)dataModel).boards.Count;
			animatedCover.Init(dataModel as BloxelAnimation, new CoverStyle(BloxelBoard.baseUIBoardColor));
			break;
		case ProjectType.MegaBoard:
			uiRawImage.texture = ((BloxelMegaBoard)dataModel).GetDetailTexture();
			break;
		case ProjectType.Character:
			if (((BloxelCharacter)dataModel).animations.ContainsKey(AnimationType.Idle))
			{
				boardsUsed = ((BloxelCharacter)dataModel).animations[AnimationType.Idle].boards.Count;
				animatedCover.Init(dataModel as BloxelCharacter, AnimationType.Walk, new CoverStyle(BloxelBoard.baseUIBoardColor));
			}
			break;
		}
		uiImageOutline.color = BloxelProject.GetColorFromType(_project.type);
	}

	public void Activate()
	{
		isActive = true;
		uiCanvasGroup.DOFade(1f, UIAnimationManager.speedFast);
	}

	public void DeActivate()
	{
		isActive = false;
		uiCanvasGroup.DOFade(0.4f, UIAnimationManager.speedFast);
	}

	public void OnPointerClick(PointerEventData pData)
	{
		if (isDragging)
		{
			return;
		}
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		foreach (GameAssetTile currentTile in GameAssetsMenu.instance.currentTiles)
		{
			if (currentTile == this)
			{
				currentTile.Activate();
			}
			else
			{
				currentTile.DeActivate();
			}
		}
		GameAssetsMenu.instance.SetupAssetTileInfo(this);
	}
}
