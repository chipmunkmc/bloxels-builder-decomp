using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameAssetsMenu : MonoBehaviour
{
	public static GameAssetsMenu instance;

	[Header("| ========= Movable ========= |")]
	public RectTransform rect;

	public Vector2 basePosition;

	public Vector2 hiddenPosition;

	public bool isVisible;

	[Header("| ========= Data ========= |")]
	public BloxelGame game;

	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public ScrollRect scrollRect;

	public RectTransform scrollRectContent;

	public GridLayoutGroup gridLayout;

	public RawImage uiRawImageBackground;

	public UGCAuth ugcTextGameTitle;

	public TextMeshProUGUI uiTextAssetType;

	public TextMeshProUGUI uiTextAssetTitle;

	public UIButton uiButtonDismiss;

	public RectTransform uiRectLoading;

	public UITriggerBuyAsset uiTriggerBuy;

	public Object assetTilePrefab;

	[Header("| ========= State Tracking ========= |")]
	public List<GameAssetTile> currentTiles = new List<GameAssetTile>();

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		assetTilePrefab = Resources.Load("Prefabs/AssetCanvasTile");
		uiButtonDismiss.OnClick += Dismiss;
		uiTriggerBuy.transform.localScale = Vector3.zero;
		isVisible = false;
		base.transform.localScale = Vector3.zero;
	}

	private void OnDestroy()
	{
		uiButtonDismiss.OnClick -= Dismiss;
	}

	private void Dismiss()
	{
		Hide();
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		game = _square.project as BloxelGame;
		ugcTextGameTitle.Init(game);
		uiRawImageBackground.texture = CanvasStreamingInterface.instance.details.decoratedBoard.sprite.texture;
		Show();
		uiRectLoading.DOAnchorPos(new Vector2(uiRectLoading.sizeDelta.x, 0f), UIAnimationManager.speedFast);
		BuildTiles();
		AdjustScrollRectHeight(scrollRectContent);
		uiRectLoading.DOAnchorPos(new Vector2(uiRectLoading.sizeDelta.x, 0f - uiRectLoading.sizeDelta.y), UIAnimationManager.speedFast);
	}

	public void Reset()
	{
		DestroyCurrentTiles();
		ugcTextGameTitle.uiText.text = "---";
		uiTextAssetType.text = "Type";
		uiTextAssetTitle.text = LocalizationManager.getInstance().getLocalizedText("iWall56", "Choose a Tile Below");
		uiTriggerBuy.transform.localScale = Vector3.zero;
	}

	public void SetupAssetTileInfo(GameAssetTile tile)
	{
		uiTriggerBuy.Init(tile.dataModel, square);
		uiTriggerBuy.transform.DOScale(1f, UIAnimationManager.speedMedium);
		ProjectType type = tile.dataModel.type;
		string unityKey = BloxelProject.projectTypeDisplayKeys[type];
		string defaultText = tile.dataModel.type.ToString();
		string localizedText = LocalizationManager.getInstance().getLocalizedText(unityKey, defaultText);
		uiTextAssetType.text = localizedText;
		uiTextAssetType.color = BloxelProject.GetColorFromType(tile.dataModel.type);
		uiTextAssetTitle.text = tile.dataModel.title;
	}

	private void DestroyCurrentTiles()
	{
		for (int i = 0; i < scrollRectContent.transform.childCount; i++)
		{
			Object.Destroy(scrollRectContent.GetChild(i).gameObject);
		}
		currentTiles.Clear();
	}

	public void BuildTiles()
	{
		List<string> list = new List<string>();
		List<string> list2 = new List<string>();
		MakeTile(game.hero);
		if (game.background != null && game.background.boards.Count > 0)
		{
			MakeTile(game.background);
			foreach (BloxelBoard value in game.background.boards.Values)
			{
				if (!list.Contains(value.ID()))
				{
					list.Add(value.ID());
				}
			}
		}
		if (!list.Contains(game.coverBoard.ID()))
		{
			list.Add(game.coverBoard.ID());
		}
		foreach (BloxelLevel value2 in game.levels.Values)
		{
			if (!list.Contains(value2.coverBoard.ID()))
			{
				list.Add(value2.coverBoard.ID());
			}
			foreach (BloxelAnimation value3 in value2.animations.Values)
			{
				for (int i = 0; i < value3.boards.Count; i++)
				{
					if (!list.Contains(value3.boards[i].ID()))
					{
						list.Add(value3.boards[i].ID());
					}
				}
			}
		}
		Dictionary<string, BloxelBoard> dictionary = game.UniqueBoards();
		foreach (BloxelBoard value4 in dictionary.Values)
		{
			if (!list.Contains(value4.ID()))
			{
				MakeTile(value4);
			}
		}
		foreach (BloxelAnimation value5 in game.hero.animations.Values)
		{
			if (!list2.Contains(value5.ID()))
			{
				list2.Add(value5.ID());
			}
		}
		Dictionary<string, BloxelAnimation> dictionary2 = game.UniqueAnimations();
		foreach (BloxelAnimation value6 in dictionary2.Values)
		{
			if (!list2.Contains(value6.ID()))
			{
				MakeTile(value6);
			}
		}
	}

	private void MakeTile(BloxelProject model)
	{
		GameObject gameObject = Object.Instantiate(assetTilePrefab) as GameObject;
		gameObject.transform.SetParent(scrollRectContent);
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localPosition = Vector3.zero;
		GameAssetTile component = gameObject.GetComponent<GameAssetTile>();
		currentTiles.Add(component);
		component.Init(model);
	}

	private void AdjustScrollRectHeight(Transform t)
	{
		GridLayoutGroup component = t.GetComponent<GridLayoutGroup>();
		RectTransform component2 = t.GetComponent<RectTransform>();
		int num = 0;
		for (int i = 0; i < component.transform.childCount && !(Mathf.Abs(((RectTransform)component.transform.GetChild(i)).anchoredPosition.y) >= Mathf.Abs(component.cellSize.y + component.spacing.y + (float)component.padding.top)); i++)
		{
			num++;
		}
		if (num == 0)
		{
			num = 1;
		}
		int num2 = Mathf.CeilToInt(t.childCount / num);
		float num3 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num2 * num3) + Mathf.Abs(num3);
		component2.sizeDelta = new Vector2(component2.sizeDelta.x, y);
		component2.anchoredPosition = Vector2.zero;
	}

	public Tweener Show()
	{
		if (isVisible)
		{
			return null;
		}
		Tweener tweener = null;
		base.transform.localScale = Vector3.one;
		scrollRect.verticalNormalizedPosition = 1f;
		return rect.DOAnchorPos(basePosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isVisible = true;
		}).OnComplete(delegate
		{
			BloxelLibrary.instance.Hide();
		});
	}

	public Tweener Hide()
	{
		if (!isVisible)
		{
			return null;
		}
		Tweener tweener = null;
		return rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isVisible = false;
		}).OnComplete(delegate
		{
			Reset();
			base.transform.localScale = Vector3.zero;
			BloxelLibrary.instance.Show();
		});
	}
}
