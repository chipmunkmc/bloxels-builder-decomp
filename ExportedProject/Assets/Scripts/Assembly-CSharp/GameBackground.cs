using System.Collections.Generic;
using UnityEngine;

public class GameBackground : MonoBehaviour
{
	public MeshRenderer renderer;

	public BloxelGame dataModel;

	private Texture2D backgroundTexture;

	private static Color[] colorBufferAllClearTile;

	private static Color[] colorBufferAllClearFull;

	private static Color[] colorBufferTile = new Color[169];

	private void Awake()
	{
		if (renderer == null)
		{
			renderer = GetComponent<MeshRenderer>();
		}
		backgroundTexture = new Texture2D(169, 169, TextureFormat.ARGB32, false);
		backgroundTexture.filterMode = FilterMode.Point;
		Color clear = Color.clear;
		if (colorBufferAllClearFull == null)
		{
			colorBufferAllClearFull = new Color[28561];
			for (int i = 0; i < colorBufferAllClearFull.Length; i++)
			{
				colorBufferAllClearFull[i] = clear;
			}
		}
		backgroundTexture.SetPixels(colorBufferAllClearFull);
		backgroundTexture.Apply();
		renderer.material.mainTexture = backgroundTexture;
		base.transform.localPosition = new Vector3(1100f, 1100f, 330f);
		if (colorBufferAllClearTile == null)
		{
			colorBufferAllClearTile = new Color[169];
			for (int j = 0; j < 169; j++)
			{
				colorBufferAllClearTile[j] = clear;
			}
		}
	}

	public void Reset()
	{
		backgroundTexture.SetPixels(colorBufferAllClearFull);
		backgroundTexture.Apply();
		renderer.material.mainTexture = backgroundTexture;
	}

	public void BuildFromGame(BloxelGame game)
	{
		dataModel = game;
		Dictionary<GridLocation, BloxelBoard> boards = dataModel.background.boards;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BloxelBoard value = null;
				if (boards.TryGetValue(new GridLocation(j, i), out value))
				{
					value.GetColorsNonAlloc(ref colorBufferTile);
					backgroundTexture.SetPixels(j * 13, i * 13, 13, 13, colorBufferTile);
				}
				else
				{
					backgroundTexture.SetPixels(j * 13, i * 13, 13, 13, colorBufferAllClearTile);
				}
			}
		}
		backgroundTexture.Apply();
	}
}
