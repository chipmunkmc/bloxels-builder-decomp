public enum GameBehavior
{
	Hazard = 0,
	Destructible = 1,
	Coin = 2,
	Platform = 3,
	Water = 4,
	Enemy = 5,
	PowerUp = 6,
	NPC = 7,
	Air = 8,
	Plain = 9
}
