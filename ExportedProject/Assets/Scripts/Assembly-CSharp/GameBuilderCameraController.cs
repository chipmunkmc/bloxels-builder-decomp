using UnityEngine;

public class GameBuilderCameraController : BloxelCameraController
{
	public GameBuilderCameraController(Transform cameraTransform, Camera cam, Vector3 cameraOffset)
	{
		base.cameraTransform = cameraTransform;
		base.cam = cam;
		base.cameraOffset = cameraOffset;
	}

	public override void ApplyCameraProperties()
	{
		cam.orthographic = true;
		cam.orthographicSize = 110f;
		cameraTransform.localRotation = Quaternion.identity;
	}

	public override void UpdateCameraPosition()
	{
	}

	public void SetupCameraForTargetPosition(Vector3 positionInWorldWrapper)
	{
		cameraTransform.position = positionInWorldWrapper + cameraOffset;
	}
}
