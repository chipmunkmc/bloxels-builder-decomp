using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class GameBuilderCanvas : UIMoveable
{
	public enum DefaultShaderMode
	{
		Opaque = 0,
		Transparent = 1
	}

	public delegate void GameStart();

	public delegate void HandleModeChange(GameBuilderMode _mode);

	public delegate void HandleProjectChange();

	public delegate void HandleLevelChange(BloxelLevel _level);

	public delegate void HandleNewGame();

	public delegate void HandleConfigPressed(UIConfigItem configMenu, BloxelTile tile);

	public static GameBuilderCanvas instance;

	[Header("Materials")]
	public Material defaultMat;

	public Material characterMat;

	public Material waterMat;

	public DefaultShaderMode currentShaderMode;

	[Header("Modes")]
	public UIBuilderMode[] primaryModes;

	public UIGameBuilderSecondaryMode mapModeToggle;

	public GameObject mapZoomToggle;

	public UIDecorateTools decorateTools;

	[Header("UI")]
	public UIStatRoomInventory uiStatRoomInventory;

	public Canvas gameplayHUDCanvas;

	public GameObject inputManager;

	public GameObject gameStart;

	public GameObject touchEventsOverlay;

	public CanvasGroup modeContainer;

	public UIButton uiButtonStartNewGame;

	public UIButton uiButtonQuickStart;

	public UIButton uiButtonEditGame;

	public GameObject startScreenHR;

	public CoverBoard characterCover;

	public RawImage characterRawImage;

	public Sprite characterIcon;

	public TextMeshProUGUI uiTextModeLabel;

	public UIMapShift[] mapShiftButtons;

	public UITrashCan uiTrashCan;

	public UnityEngine.Object startPositionHelpPrefab;

	public UnityEngine.Object configNPCPrefab;

	public UnityEngine.Object configEnemyPrefab;

	public UnityEngine.Object configPowerupPrefab;

	public GameBuilderMode previousMode;

	public GameBuilderMode _currentMode;

	private BloxelGame _currentGame;

	private BloxelLevel _currentBloxelLevel;

	public string currentGameID;

	public bool autoCloseLibrary = true;

	public static ConfigIndicator[,] ConfigIncicators = new ConfigIndicator[13, 13];

	public GameBuilderMode currentMode
	{
		get
		{
			return _currentMode;
		}
		set
		{
			previousMode = _currentMode;
			_currentMode = value;
			SwitchCurrentMode();
		}
	}

	public BloxelGame currentGame
	{
		get
		{
			return _currentGame;
		}
		set
		{
			_currentGame = value;
			if (_currentGame != null)
			{
				currentGameID = _currentGame.ID();
				GameplayBuilder.instance.game = _currentGame;
				GameplayBuilder.instance.Reset();
				if (this.OnGameBuilderProjectChange != null)
				{
					this.OnGameBuilderProjectChange();
				}
			}
		}
	}

	public BloxelLevel currentBloxelLevel
	{
		get
		{
			return _currentBloxelLevel;
		}
		set
		{
			_currentBloxelLevel = value;
			if (this.OnGameBuilderLevelChange != null)
			{
				this.OnGameBuilderLevelChange(_currentBloxelLevel);
			}
		}
	}

	public event GameStart onGameStart;

	public event HandleModeChange OnGameBuilderModeChange;

	public event HandleProjectChange OnGameBuilderProjectChange;

	public event HandleLevelChange OnGameBuilderLevelChange;

	public event HandleNewGame OnNewGameInit;

	public event HandleConfigPressed OnConfigSelect;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (startPositionHelpPrefab == null)
		{
			startPositionHelpPrefab = Resources.Load("Prefabs/UIPopupStartPositionHelp");
		}
		configNPCPrefab = Resources.Load("Prefabs/UIPopupConfigNPC");
		configEnemyPrefab = Resources.Load("Prefabs/UIPopupConfigEnemy");
		configPowerupPrefab = Resources.Load("Prefabs/UIPopupConfigPowerUp");
		primaryModes = GetComponentsInChildren<UIBuilderMode>();
		characterCover = new CoverBoard(Color.clear);
	}

	private new void Start()
	{
		uiButtonStartNewGame.OnClick += NewGamePressed;
		uiButtonQuickStart.OnClick += QuickStart;
		uiButtonEditGame.OnClick += ShowLibrary;
		BloxelCamera.instance.OnModeChange += HandleCameraModeChange;
		BloxelGameLibrary.instance.OnProjectSelect += HandleOnGameChange;
		BloxelGameLibrary.instance.OnProjectDelete += HandleOnProjectDelete;
		BloxelCharacterLibrary.instance.OnProjectSelect += HandleCharacterSelect;
		BloxelMegaBoardLibrary.instance.OnProjectSelect += HandleBackgroundSelect;
		PixelEditorPaintBoard.instance.OnFlip += ReloadTileInfo;
		PixelEditorPaintBoard.instance.OnShift += ReloadTileInfo;
		ToggleModeContainerVisibility(false);
		mapZoomToggle.SetActive(false);
		decorateTools.gameObject.SetActive(false);
		HideTouchControls();
		uiTrashCan.Init(false, false);
		characterRawImage.texture = AssetManager.instance.defaultCharacterAnimations[AnimationType.Idle].coverBoard.UpdateTexture2D(ref characterCover.colors, ref characterCover.texture, characterCover.style);
		currentMode = GameBuilderMode.None;
	}

	private void HandleHandleCharacterEdited(BloxelCharacter _character)
	{
		if (currentGame != null && currentGame.hero != null && _character.id == currentGame.hero.id && GameplayController.instance.heroObject != null)
		{
			GameplayController.instance.heroObject.GetComponent<PixelPlayerController>().OnDeath -= HandlePlayerDeathInTest;
			UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
		}
	}

	private void OnDestroy()
	{
		MakeShaderOpaque(true);
		uiButtonStartNewGame.OnClick -= NewGamePressed;
		uiButtonQuickStart.OnClick -= QuickStart;
		uiButtonEditGame.OnClick -= ShowLibrary;
		BloxelCamera.instance.OnModeChange -= HandleCameraModeChange;
		BloxelGameLibrary.instance.OnProjectSelect -= HandleOnGameChange;
		BloxelGameLibrary.instance.OnProjectDelete -= HandleOnProjectDelete;
		BloxelCharacterLibrary.instance.OnProjectSelect -= HandleCharacterSelect;
		BloxelMegaBoardLibrary.instance.OnProjectSelect -= HandleBackgroundSelect;
		PixelEditorPaintBoard.instance.OnFlip -= ReloadTileInfo;
		PixelEditorPaintBoard.instance.OnShift -= ReloadTileInfo;
		if (GameplayController.instance.heroObject != null)
		{
			GameplayController.instance.heroObject.GetComponent<PixelPlayerController>().OnDeath -= HandlePlayerDeathInTest;
			UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
		}
	}

	public new void Show()
	{
		base.Show();
		if (currentGame == null)
		{
			currentMode = GameBuilderMode.None;
			return;
		}
		if (currentMode != GameBuilderMode.Build || currentMode != GameBuilderMode.Map || currentMode != 0 || currentMode != GameBuilderMode.Wireframe)
		{
			GameplayBuilder.instance.StartStreaming(currentGame);
			GameplayBuilder.instance.Reset();
			WorldWrapper.instance.Show();
		}
		currentMode = currentMode;
	}

	public new void Hide()
	{
		MakeShaderOpaque();
		StopPreviewMusic();
		if (GameplayController.instance.heroObject != null)
		{
			GameplayController.instance.heroObject.GetComponent<PixelPlayerController>().OnDeath -= HandlePlayerDeathInTest;
			UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
		}
		base.Hide();
	}

	private void ShowLibrary()
	{
		BloxelLibrary.instance.libraryToggle.Show();
	}

	private void HandleOnGameChange(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			ChangeGame(item.dataModel as BloxelGame);
		}
	}

	private void ReloadTileInfo()
	{
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			GameplayBuilder.instance.ResetLevelAtLocation(currentBloxelLevel.location);
		}
	}

	public void ChangeGame(BloxelGame game, BloxelLevel levelFromQuickStart = null)
	{
		if (currentGame != null && game.ID() == currentGame.ID())
		{
			return;
		}
		if (GameplayController.instance.heroObject != null)
		{
			GameplayController.instance.heroObject.GetComponent<PixelPlayerController>().OnDeath -= HandlePlayerDeathInTest;
			UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
		}
		currentGame = game;
		if (levelFromQuickStart == null)
		{
			BloxelLevel value = null;
			if (!AssetManager.instance.levelPool.TryGetValue(currentGame.heroStartPosition.id.ToString(), out value))
			{
				if (currentGame.levels.Count <= 0)
				{
					currentGame.Delete();
					NewGamePressed();
					return;
				}
				currentBloxelLevel = currentGame.levels.First().Value;
				if (currentGame.heroStartPosition.id != currentBloxelLevel.id)
				{
					if (currentGame.hero != null)
					{
						currentGame.SetHeroAtLocation(currentGame.hero, new WorldLocation(currentBloxelLevel.id, currentGame.heroStartPosition.locationInBoard, currentBloxelLevel.location));
					}
					else
					{
						currentGame.SetDefaultHero();
						currentGame.SetHeroAtLocation(currentGame.hero, new WorldLocation(currentBloxelLevel.id, currentGame.heroStartPosition.locationInBoard, currentBloxelLevel.location));
					}
				}
			}
			else
			{
				currentBloxelLevel = value;
			}
		}
		else
		{
			currentBloxelLevel = levelFromQuickStart;
		}
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
		BloxelCamera.instance.SetGameBuilderCamToLevel(currentBloxelLevel.location);
		characterRawImage.texture = currentGame.hero.coverBoard.UpdateTexture2D(ref characterCover.colors, ref characterCover.texture, characterCover.style);
		currentMode = GameBuilderMode.Map;
	}

	public void HandleCharacterSelect(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor && currentMode == GameBuilderMode.CharacterPlacement && !(GameplayController.instance.heroObject == null))
		{
			GameplayController.instance.heroObject.GetComponent<PixelPlayerController>().animationController.InitializeAnimations(item.dataModel as BloxelCharacter);
			currentGame.SetHero(item.dataModel as BloxelCharacter);
			characterRawImage.texture = item.dataModel.coverBoard.UpdateTexture2D(ref characterCover.colors, ref characterCover.texture, characterCover.style);
			PixelPlayerController.instance.SetAutomaticColliderBounds();
			SavedGame selectedProject = BloxelLibraryScrollController.instance.GetSelectedProject<SavedGame>();
			if (selectedProject != null)
			{
				selectedProject.SetHeroImage();
			}
		}
	}

	public void HandleBackgroundSelect(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor && currentMode == GameBuilderMode.GameBackground)
		{
			currentGame.SetBackground(item.dataModel as BloxelMegaBoard);
			LevelBuilder.instance.gameBackground.BuildFromGame(currentGame);
		}
	}

	private void HandleOnProjectDelete(SavedProject item)
	{
		if (item.isActive)
		{
			currentMode = GameBuilderMode.None;
		}
	}

	public void MegaTileClicked(MegaBoardTile tile)
	{
		if (Input.touchCount > 1)
		{
			return;
		}
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			if (tile.isBlank)
			{
				BloxelLevel bloxelLevel = new BloxelLevel(tile.loc, BloxelBoard.DefaultWireframe());
				bloxelLevel.Save();
				currentGame.SetLevelAtLocation(bloxelLevel.location, bloxelLevel);
				currentBloxelLevel = bloxelLevel;
				if (!AssetManager.instance.levelPool.ContainsKey(bloxelLevel.ID()))
				{
					AssetManager.instance.levelPool.Add(bloxelLevel.ID(), bloxelLevel);
				}
				GameplayBuilder.instance.UpdateTileInfoForDefaultLevel(currentBloxelLevel);
				currentGame.SetHeroAtLocation(currentGame.hero, new WorldLocation(bloxelLevel.id, currentGame.heroStartPosition.locationInBoard, bloxelLevel.location));
				Vector3 vector = new Vector3(currentBloxelLevel.location.c * 169, currentBloxelLevel.location.r * 169, 0f);
				Vector3 localPosition = new Vector3((float)currentGame.heroStartPosition.locationInBoard.c * 13f + 6f, (float)currentGame.heroStartPosition.locationInBoard.r * 13f, -7f) + vector;
				if (GameplayController.instance.heroObject != null)
				{
					GameplayController.instance.heroObject.transform.localPosition = localPosition;
				}
				else
				{
					GenerateHeroForEditor();
				}
				BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
				BloxelCamera.instance.SetGameBuilderCamToLevel(bloxelLevel.location);
				UIBuilderModeGroup.instance.SwitchMode(0);
				currentMode = GameBuilderMode.Build;
			}
			else
			{
				BloxelLevel levelAtLocation = currentGame.GetLevelAtLocation(tile.loc);
				if (levelAtLocation != null)
				{
					currentBloxelLevel = levelAtLocation;
				}
				bool flag = false;
				for (int i = 0; i < primaryModes.Length; i++)
				{
					if (primaryModes[i].mode == previousMode)
					{
						flag = true;
						primaryModes[i].Select();
						break;
					}
				}
				if (!flag)
				{
					for (int j = 0; j < primaryModes.Length; j++)
					{
						if (flag)
						{
							break;
						}
						for (int k = 0; k < primaryModes[j].subModes.Length; k++)
						{
							if (primaryModes[j].subModes[k].mode == previousMode)
							{
								flag = true;
								primaryModes[j].subModes[k].Select();
								break;
							}
						}
					}
				}
				if (!flag)
				{
					for (int l = 0; l < primaryModes.Length; l++)
					{
						if (primaryModes[l].mode == GameBuilderMode.Build)
						{
							flag = true;
							primaryModes[l].Select();
							break;
						}
					}
				}
				currentGame.SetHeroAtLocation(currentGame.hero, new WorldLocation(currentBloxelLevel.id, currentGame.heroStartPosition.locationInBoard, currentBloxelLevel.location));
				Vector3 vector2 = new Vector3(currentBloxelLevel.location.c * 169, currentBloxelLevel.location.r * 169, 0f);
				Vector3 localPosition2 = new Vector3((float)currentGame.heroStartPosition.locationInBoard.c * 13f + 6f, (float)currentGame.heroStartPosition.locationInBoard.r * 13f, -7f) + vector2;
				if (GameplayController.instance.heroObject != null)
				{
					GameplayController.instance.heroObject.transform.localPosition = localPosition2;
				}
				else
				{
					GenerateHeroForEditor();
				}
				PixelEditorPaintBoard.instance.SwitchBloxelBoard(currentBloxelLevel.coverBoard);
				BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
				BloxelCamera.instance.SetGameBuilderCamToLevel(currentBloxelLevel.location);
			}
			if (!PrefsManager.instance.hasSeenStartPositionHelp && !ChallengeManager.instance.hasActiveChallenge)
			{
				ShowStartPositionHelp();
			}
		}
		tile.interactable = true;
	}

	public void GenerateHeroForEditor()
	{
		Vector3 vector = new Vector3(currentBloxelLevel.location.c * 169, currentBloxelLevel.location.r * 169, 0f);
		Vector3 localPosition = new Vector3((float)currentGame.heroStartPosition.locationInBoard.c * 13f + 6f, (float)currentGame.heroStartPosition.locationInBoard.r * 13f, -7f) + vector;
		GameObject gameObject = UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Player/Player")) as GameObject;
		gameObject.transform.SetParent(WorldWrapper.instance.selfTransform);
		gameObject.transform.localPosition = localPosition;
		PixelPlayerController component = gameObject.GetComponent<PixelPlayerController>();
		component.animationController.InitializeAnimations(currentGame.hero);
		component.DisableForEditor();
		component.OnDeath += HandlePlayerDeathInTest;
		GameplayController.instance.heroObject = gameObject;
		GameplayController.instance.lastCheckpoint = GameplayController.instance.heroObject.transform.localPosition + new Vector3(0f, 0f, 7f);
	}

	public void ChangeLevel(GridLocation location)
	{
		currentBloxelLevel = currentGame.GetLevelAtLocation(location);
		currentGame.SetHeroAtLocation(currentGame.hero, new WorldLocation(currentBloxelLevel.id, currentGame.heroStartPosition.locationInBoard, currentBloxelLevel.location));
		Vector3 vector = new Vector3(currentBloxelLevel.location.c * 169, currentBloxelLevel.location.r * 169, 0f);
		Vector3 localPosition = new Vector3((float)currentGame.heroStartPosition.locationInBoard.c * 13f + 6f, (float)currentGame.heroStartPosition.locationInBoard.r * 13f, -7f) + vector;
		GameplayController.instance.heroObject.transform.localPosition = localPosition;
		GameplayController.instance.startingPosition = new Vector3(localPosition.x, localPosition.y, 0f);
		GameplayController.instance.lastCheckpoint = GameplayController.instance.startingPosition;
		if (currentMode == GameBuilderMode.ConfigBlocks)
		{
			WorldWrapper.instance.configIndicatorContainer.GetComponent<ConfigIndicatorContainer>().MoveIndicatorsForNewLevel(currentBloxelLevel);
		}
		PixelEditorPaintBoard.instance.SwitchBloxelBoard(currentBloxelLevel.coverBoard);
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
		BloxelCamera.instance.SetGameBuilderCamToLevel(currentBloxelLevel.location);
		SavedGame selectedProject = BloxelLibraryScrollController.instance.GetSelectedProject<SavedGame>();
		if (selectedProject != null)
		{
			selectedProject.BuildCover();
		}
		if (!PrefsManager.instance.hasSeenStartPositionHelp && !ChallengeManager.instance.hasActiveChallenge)
		{
			ShowStartPositionHelp();
		}
	}

	private void ShowStartPositionHelp()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(startPositionHelpPrefab);
	}

	private void HandleCameraModeChange()
	{
		if (PixelEditorController.instance.mode != CanvasMode.LevelEditor)
		{
			return;
		}
		CanvasGroup primaryCanvasGroup = PixelEditorController.instance.primaryCanvas.GetComponent<CanvasGroup>();
		if (BloxelCamera.instance.mode == BloxelCamera.CameraMode.Gameplay || BloxelCamera.instance.mode == BloxelCamera.CameraMode.Gameplay2D)
		{
			primaryCanvasGroup.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				primaryCanvasGroup.blocksRaycasts = false;
			}).OnComplete(delegate
			{
				gameplayHUDCanvas.gameObject.SetActive(true);
				gameplayHUDCanvas.GetComponent<CanvasGroup>().DOFade(1f, UIAnimationManager.speedMedium);
				ShowTouchControls();
				if (BloxelLibrary.instance.isVisible)
				{
					BloxelLibrary.instance.Hide();
				}
			});
		}
		else
		{
			gameplayHUDCanvas.GetComponent<CanvasGroup>().DOFade(0f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				gameplayHUDCanvas.gameObject.SetActive(false);
			});
			primaryCanvasGroup.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				primaryCanvasGroup.blocksRaycasts = true;
			}).OnComplete(delegate
			{
				HideTouchControls();
			});
		}
	}

	public void MakeShaderTransparent()
	{
		defaultMat.SetInt("_SrcBlend", 1);
		defaultMat.SetInt("_DstBlend", 10);
		defaultMat.SetInt("_ZWrite", 0);
		defaultMat.DisableKeyword("_ALPHATEST_ON");
		defaultMat.DisableKeyword("_ALPHABLEND_ON");
		defaultMat.EnableKeyword("_ALPHAPREMULTIPLY_ON");
		currentShaderMode = DefaultShaderMode.Transparent;
		defaultMat.DOFade(0.15f, UIAnimationManager.speedFast);
		waterMat.DOFade(0.15f, UIAnimationManager.speedFast);
	}

	public void MakeShaderOpaque(bool immediate = false)
	{
		defaultMat.SetInt("_SrcBlend", 1);
		defaultMat.SetInt("_DstBlend", 0);
		defaultMat.SetInt("_ZWrite", 1);
		defaultMat.DisableKeyword("_ALPHATEST_ON");
		defaultMat.DisableKeyword("_ALPHABLEND_ON");
		defaultMat.DisableKeyword("_ALPHAPREMULTIPLY_ON");
		currentShaderMode = DefaultShaderMode.Opaque;
		if (immediate)
		{
			defaultMat.color = new Color(defaultMat.color.r, defaultMat.color.g, defaultMat.color.b, 1f);
			waterMat.color = new Color(waterMat.color.r, waterMat.color.g, waterMat.color.b, 0.75f);
		}
		else
		{
			defaultMat.DOFade(1f, UIAnimationManager.speedFast);
			waterMat.DOFade(0.75f, UIAnimationManager.speedFast);
		}
	}

	public void ShowTouchControls()
	{
		inputManager.GetComponentInChildren<PPInputController>().canvasUIBase1024Object.SetActive(true);
	}

	public void HideTouchControls()
	{
		inputManager.GetComponentInChildren<PPInputController>().canvasUIBase1024Object.SetActive(false);
	}

	public void QuickStart()
	{
		AnalyticsManager.SendEventString(AnalyticsManager.Event.NewGame, "QuickStart");
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		PixelEditorController.instance.ShowGemBank(false);
		GameQuickStartMenu.instance.HideButtons();
		for (int i = 0; i < GameQuickStartMenu.instance.quickStartGroups.Length; i++)
		{
			GameQuickStartMenu.instance.quickStartGroups[i].Reset();
		}
		currentMode = GameBuilderMode.QuickStart;
		if (GameQuickStartMenu.instance.gameObject.activeInHierarchy)
		{
			GameQuickStartMenu.instance.rect.DOAnchorPos(GameQuickStartMenu.instance.basePosition, UIAnimationManager.speedMedium).OnStart(delegate
			{
				GameQuickStartMenu.instance.isVisible = true;
				GameQuickStartMenu.instance.VisiblityChange();
			});
		}
	}

	public void NewGamePressed()
	{
		autoCloseLibrary = false;
		AnalyticsManager.SendEventString(AnalyticsManager.Event.NewGame, "Custom");
		SoundManager.PlaySoundClip(SoundClip.confirmA);
		Tween tween = BloxelLibrary.instance.libraryToggle.Show();
		if (tween == null)
		{
			GenerateEmptyGame();
			return;
		}
		tween.OnComplete(delegate
		{
			GenerateEmptyGame();
		});
	}

	private void GenerateEmptyGame()
	{
		currentGame = BloxelGameLibrary.instance.CreateEmptyProject(LocalizationManager.getInstance().getLocalizedText("gamebuilder7", "My Game"));
		if (currentGame != null)
		{
			currentBloxelLevel = currentGame.GetLevelAtLocation(BloxelLevel.defaultLocation);
			PixelEditorPaintBoard.instance.currentBloxelBoard = currentBloxelLevel.coverBoard;
			BloxelCamera.instance.SetGameBuilderCamToLevel(currentBloxelLevel.location);
			currentMode = GameBuilderMode.Map;
			UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
			GameplayController.instance.heroObject = LevelBuilder.instance.CreateAndPlaceCharacter(currentGame);
			StartCoroutine(DelayedCloseLibrary());
		}
	}

	private IEnumerator DelayedCloseLibrary()
	{
		yield return new WaitForSeconds(0.5f);
		BloxelLibrary.instance.libraryToggle.Hide().OnComplete(delegate
		{
			autoCloseLibrary = true;
			if (this.OnNewGameInit != null)
			{
				this.OnNewGameInit();
			}
		});
	}

	public void AdjustStartForChallenge()
	{
		RectTransform startBtnRect = uiButtonStartNewGame.selfRect;
		uiButtonEditGame.selfRect.localScale = Vector3.zero;
		uiButtonQuickStart.selfRect.localScale = Vector3.zero;
		startScreenHR.SetActive(false);
		startBtnRect.DOAnchorPos(Vector2.zero, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			startBtnRect.DOShakeScale(UIAnimationManager.speedSlow);
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		});
	}

	public void ResetStart()
	{
		RectTransform selfRect = uiButtonStartNewGame.selfRect;
		uiButtonEditGame.selfRect.localScale = Vector3.one;
		uiButtonQuickStart.selfRect.localScale = Vector3.one;
		startScreenHR.SetActive(true);
		selfRect.anchoredPosition = new Vector2(0f, 125f);
	}

	public void ConfigNPCBlock(BloxelTile tile)
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(configNPCPrefab);
		UIPopupNPCConfig component = gameObject.GetComponent<UIPopupNPCConfig>();
		component.tile = tile;
		if (this.OnConfigSelect != null)
		{
			this.OnConfigSelect(component, tile);
		}
	}

	public void ConfigEnemy(BloxelTile tile)
	{
		BloxelLibrary.instance.libraryToggle.Hide();
		GameObject gameObject = UIOverlayCanvas.instance.Popup(configEnemyPrefab);
		UIPopupEnemyConfig component = gameObject.GetComponent<UIPopupEnemyConfig>();
		component.tile = tile;
	}

	public void ConfigPowerUp(BloxelTile tile)
	{
		BloxelLibrary.instance.libraryToggle.Hide();
		GameObject gameObject = UIOverlayCanvas.instance.Popup(configPowerupPrefab);
		UIPopupPowerupConfig component = gameObject.GetComponent<UIPopupPowerupConfig>();
		component.tile = tile;
		if (this.OnConfigSelect != null)
		{
			this.OnConfigSelect(component, tile);
		}
	}

	private void HandlePlayerDeathInTest()
	{
		SoundManager.PlayOneShot(SoundManager.instance.playerDeath);
		PixelPlayerController.instance.animationController.HandlePlayerDeath(false);
		GameplayController.instance.ReSpawnPlayerDelayInTest();
	}

	public void ComeBackFromTest(GameObject menu = null)
	{
		PixelEditorController.instance.primaryCanvas.gameObject.SetActive(true);
		BloxelLibrary.instance.libraryCanvas.gameObject.SetActive(true);
		GameplayController.instance.CancelReSpawnPlayerDelayInTest();
		GameplayController.instance.CancelDroppingCharacter();
		if (menu != null)
		{
			menu.GetComponent<UIPopupPause>().Dismiss();
		}
		WorldWrapper.instance.ClearRuntimeContainer();
		SoundManager.instance.PlayMusic(SoundManager.instance.editorMusic);
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
		currentMode = previousMode;
		for (int i = 0; i < primaryModes.Length; i++)
		{
			if (primaryModes[i].mode == previousMode)
			{
				primaryModes[i].Select();
			}
		}
		if (currentMode != GameBuilderMode.QuickStart && GameplayController.instance.heroObject != null)
		{
			Vector3 vector = new Vector3(currentBloxelLevel.location.c * 169, currentBloxelLevel.location.r * 169, 0f);
			Vector3 localPosition = new Vector3((float)currentGame.heroStartPosition.locationInBoard.c * 13f + 6f, (float)currentGame.heroStartPosition.locationInBoard.r * 13f, -7f) + vector;
			GameplayController.instance.heroObject.transform.localPosition = localPosition;
			PixelPlayerController component = GameplayController.instance.heroObject.GetComponent<PixelPlayerController>();
			component.DisableForEditor();
		}
	}

	private void ShowSettingsMenu()
	{
		UIGameSettings.instance.Show();
		UIGameSettings.instance.SetGame(ref _currentGame);
		UIGameSettings.instance.Init();
	}

	private void StopPreviewMusic()
	{
		if (!UIGameSettings.instance.isPreviewingMusic)
		{
			return;
		}
		for (int i = 0; i < UIGameSettings.instance.musicChoices.Length; i++)
		{
			if (UIGameSettings.instance.musicChoices[i].isPlaying)
			{
				UIGameSettings.instance.musicChoices[i].Stop(true);
			}
		}
	}

	public void UpdateDisplayModeToolbar(GameBuilderMode targetMode = GameBuilderMode.None)
	{
		if (targetMode == GameBuilderMode.None)
		{
			targetMode = currentMode;
		}
		for (int i = 0; i < primaryModes.Length; i++)
		{
			for (int j = 0; j < primaryModes[i].subModes.Length; j++)
			{
				if (primaryModes[i].subModes[j].mode != targetMode)
				{
					primaryModes[i].subModes[j].DeActivate();
				}
				else
				{
					primaryModes[i].subModes[j].Activate();
				}
			}
		}
	}

	public void ToggleModeContainerVisibility(bool isOn)
	{
		int num = 0;
		if (isOn)
		{
			num = 1;
		}
		modeContainer.DOFade(num, UIAnimationManager.speedFast).OnStart(delegate
		{
			modeContainer.blocksRaycasts = isOn;
			modeContainer.interactable = isOn;
		});
	}

	private void SwitchModeLabel()
	{
		string text = string.Empty;
		switch (currentMode)
		{
		case GameBuilderMode.None:
			text = LocalizationManager.getInstance().getLocalizedText(string.Empty, "Start");
			break;
		case GameBuilderMode.QuickStart:
			text = LocalizationManager.getInstance().getLocalizedText(string.Empty, "Quick Start");
			break;
		case GameBuilderMode.Build:
			text = LocalizationManager.getInstance().getLocalizedText("challenges14", "Layout");
			break;
		case GameBuilderMode.Wireframe:
			text = LocalizationManager.getInstance().getLocalizedText("challenges14", "Layout");
			break;
		case GameBuilderMode.Config:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder34", "Configure");
			break;
		case GameBuilderMode.ConfigBlocks:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder36", "Configurable Blocks");
			break;
		case GameBuilderMode.CharacterPlacement:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder37", "Configure Character");
			break;
		case GameBuilderMode.GameSettings:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder38", "Game Settings");
			break;
		case GameBuilderMode.Decorate:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder35", "Decorate");
			break;
		case GameBuilderMode.PaintArt:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder39", "Add Art");
			break;
		case GameBuilderMode.LevelBackground:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder40", "Room Background");
			break;
		case GameBuilderMode.GameBackground:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder41", "Game Background");
			break;
		case GameBuilderMode.Map:
			text = LocalizationManager.getInstance().getLocalizedText("gamebuilder32", "Map");
			break;
		case GameBuilderMode.Test:
			text = LocalizationManager.getInstance().getLocalizedText(string.Empty, "Test");
			break;
		}
		uiTextModeLabel.SetText(text);
	}

	public void ShowMapShift()
	{
		for (int i = 0; i < mapShiftButtons.Length; i++)
		{
			mapShiftButtons[i].gameObject.SetActive(true);
		}
	}

	public void HideMapShift()
	{
		for (int i = 0; i < mapShiftButtons.Length; i++)
		{
			mapShiftButtons[i].gameObject.SetActive(false);
		}
	}

	private void SwitchCurrentMode()
	{
		UIHelpLink.instance.location = UIHelpLink.Location.Games;
		SwitchModeLabel();
		touchEventsOverlay.GetComponent<WireframeEditorCanvas>().enabled = false;
		touchEventsOverlay.GetComponent<BackgroundEditorCanvas>().enabled = false;
		touchEventsOverlay.GetComponent<HeroEditorCanvas>().enabled = false;
		touchEventsOverlay.SetActive(false);
		UpdateDisplayModeToolbar();
		if (currentMode != 0)
		{
			if (gameStart.activeInHierarchy)
			{
				gameStart.SetActive(false);
			}
			if (modeContainer.alpha != 1f)
			{
				ToggleModeContainerVisibility(true);
			}
			if (!mapModeToggle.gameObject.activeInHierarchy)
			{
				mapModeToggle.gameObject.SetActive(true);
			}
		}
		if (currentMode != GameBuilderMode.GameSettings)
		{
			StopPreviewMusic();
		}
		if (currentMode != GameBuilderMode.QuickStart)
		{
			GameQuickStartMenu.instance.Hide();
			PixelEditorController.instance.ShowGemBank(true);
		}
		if (currentMode != GameBuilderMode.QuickStart && currentMode != GameBuilderMode.Test)
		{
			GameplayBuilder.instance.SetMode(GameplayBuilder.Mode.EditorStreaming);
			BloxelLibrary.instance.Show();
		}
		else
		{
			GameplayBuilder.instance.SetMode(GameplayBuilder.Mode.GameplayStreaming);
		}
		if (currentMode != GameBuilderMode.Wireframe && currentMode != GameBuilderMode.Build)
		{
			PixelEditorPaintBoard.instance.Hide();
		}
		if (currentMode != GameBuilderMode.Map)
		{
			mapZoomToggle.SetActive(false);
			MegaBoardCanvas.instance.Hide();
			ShowMapShift();
			uiStatRoomInventory.Hide();
		}
		if (currentMode != 0 && currentMode != GameBuilderMode.QuickStart && GameplayController.instance.heroObject == null)
		{
			GenerateHeroForEditor();
		}
		if (currentMode != GameBuilderMode.GameSettings)
		{
			UIGameSettings.instance.Hide();
		}
		if (currentMode != GameBuilderMode.CharacterPlacement)
		{
			WorldWrapper.instance.characterPlacementGrid.gameObject.SetActive(false);
		}
		if (currentMode != GameBuilderMode.Test)
		{
			WorldWrapper.instance.currentLevelBounds.gameObject.SetActive(true);
			GameplayController.instance.gameplayActive = false;
		}
		if (currentMode != GameBuilderMode.LevelBackground)
		{
			decorateTools.gameObject.SetActive(false);
			MakeShaderOpaque();
		}
		UITab[] tabsInGroup = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
		foreach (UITab uITab in tabsInGroup)
		{
			uITab.CheckAvailability(CanvasMode.LevelEditor);
		}
		switch (currentMode)
		{
		case GameBuilderMode.None:
		{
			BloxelLibrary.instance.SwitchTabWindows(0);
			currentGame = null;
			mapModeToggle.gameObject.SetActive(false);
			ToggleModeContainerVisibility(false);
			gameStart.SetActive(true);
			WorldWrapper.instance.Hide();
			UITab[] tabsInGroup3 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab3 in tabsInGroup3)
			{
				if (uITab3.id != 0)
				{
					uITab3.DeActivate();
				}
			}
			break;
		}
		case GameBuilderMode.QuickStart:
		{
			if (GameplayController.instance.heroObject != null)
			{
				GameplayController.instance.heroObject.GetComponent<PixelPlayerController>().OnDeath -= HandlePlayerDeathInTest;
				UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
			}
			BloxelLibrary.instance.SwitchTabWindows(0);
			mapModeToggle.gameObject.SetActive(false);
			ToggleModeContainerVisibility(false);
			gameStart.SetActive(false);
			WorldWrapper.instance.Hide();
			UITab[] tabsInGroup9 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab9 in tabsInGroup9)
			{
				if (uITab9.id != 0)
				{
					uITab9.DeActivate();
				}
			}
			BloxelLibrary.instance.gameObject.SetActive(false);
			GameQuickStartMenu.instance.Show();
			break;
		}
		case GameBuilderMode.Build:
		case GameBuilderMode.Wireframe:
		{
			BloxelLibrary.instance.SwitchTabWindows(0);
			if (!BloxelLibrary.instance.libraryToggle.isClosed)
			{
				BloxelLibrary.instance.libraryToggle.Toggle();
			}
			WorldWrapper.instance.Hide();
			PixelEditorPaintBoard.instance.SwitchBloxelBoard(currentBloxelLevel.coverBoard);
			PixelEditorPaintBoard.instance.Show();
			PixelToolPalette.instance.TurnOnIcons();
			UITab[] tabsInGroup6 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab6 in tabsInGroup6)
			{
				if (uITab6.id != 0)
				{
					uITab6.DeActivate();
				}
			}
			break;
		}
		case GameBuilderMode.Map:
		{
			BloxelLibrary.instance.SwitchTabWindows(0);
			WorldWrapper.instance.Hide();
			MegaBoardCanvas.instance.Show();
			MegaBoardCanvas.instance.BuildGameLayout(currentGame);
			HideMapShift();
			mapZoomToggle.SetActive(true);
			uiStatRoomInventory.Show();
			uiStatRoomInventory.InitFromGame(currentGame);
			UITab[] tabsInGroup8 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab8 in tabsInGroup8)
			{
				if (uITab8.id != 0)
				{
					uITab8.DeActivate();
				}
			}
			break;
		}
		case GameBuilderMode.Config:
			BloxelLibrary.instance.SwitchTabWindows(0);
			if (!BloxelLibrary.instance.libraryToggle.isClosed)
			{
				BloxelLibrary.instance.libraryToggle.Toggle();
			}
			WorldWrapper.instance.Show();
			currentMode = GameBuilderMode.ConfigBlocks;
			break;
		case GameBuilderMode.ConfigBlocks:
		{
			WorldWrapper.instance.Show();
			touchEventsOverlay.SetActive(true);
			touchEventsOverlay.GetComponent<WireframeEditorCanvas>().enabled = true;
			UITab[] tabsInGroup10 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab10 in tabsInGroup10)
			{
				if (uITab10.id != 0)
				{
					uITab10.DeActivate();
				}
			}
			break;
		}
		case GameBuilderMode.GameSettings:
		{
			if (mapModeToggle.gameObject.activeInHierarchy)
			{
				mapModeToggle.gameObject.SetActive(false);
			}
			BloxelLibrary.instance.SwitchTabWindows(0);
			WorldWrapper.instance.Hide();
			ShowSettingsMenu();
			UITab[] tabsInGroup4 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab4 in tabsInGroup4)
			{
				if (uITab4.id != 0)
				{
					uITab4.DeActivate();
				}
			}
			break;
		}
		case GameBuilderMode.CharacterPlacement:
		{
			touchEventsOverlay.SetActive(true);
			touchEventsOverlay.GetComponent<HeroEditorCanvas>().enabled = true;
			WorldWrapper.instance.Show();
			WorldWrapper.instance.characterPlacementGrid.gameObject.SetActive(true);
			BloxelLibrary.instance.SwitchTabWindows(3);
			UITab[] tabsInGroup12 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab12 in tabsInGroup12)
			{
				if (uITab12.id != 3 && uITab12.id != 0)
				{
					uITab12.DeActivate();
				}
			}
			BloxelLibrary.instance.libraryToggle.Show();
			break;
		}
		case GameBuilderMode.Decorate:
			BloxelLibrary.instance.SwitchTabWindows(0);
			WorldWrapper.instance.Show();
			currentMode = GameBuilderMode.PaintArt;
			break;
		case GameBuilderMode.PaintArt:
		{
			WorldWrapper.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(4);
			touchEventsOverlay.SetActive(true);
			touchEventsOverlay.GetComponent<WireframeEditorCanvas>().enabled = true;
			UITab[] tabsInGroup7 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab7 in tabsInGroup7)
			{
				if (uITab7.id != 1 && uITab7.id != 4 && uITab7.id != 0)
				{
					uITab7.DeActivate();
				}
			}
			BloxelLibrary.instance.libraryToggle.Show();
			break;
		}
		case GameBuilderMode.CoverArt:
		{
			BloxelLibrary.instance.SwitchTabWindows(4);
			touchEventsOverlay.SetActive(true);
			touchEventsOverlay.GetComponent<WireframeEditorCanvas>().enabled = true;
			UITab[] tabsInGroup5 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab5 in tabsInGroup5)
			{
				if (uITab5.id != 4 && uITab5.id != 0)
				{
					uITab5.DeActivate();
				}
			}
			BloxelLibrary.instance.libraryToggle.Show();
			break;
		}
		case GameBuilderMode.LevelBackground:
		{
			WorldWrapper.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(4);
			decorateTools.gameObject.SetActive(true);
			touchEventsOverlay.SetActive(true);
			touchEventsOverlay.GetComponent<BackgroundEditorCanvas>().enabled = true;
			MakeShaderTransparent();
			UITab[] tabsInGroup2 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab2 in tabsInGroup2)
			{
				if (uITab2.id != 4 && uITab2.id != 0)
				{
					uITab2.DeActivate();
				}
			}
			BloxelLibrary.instance.libraryToggle.Show();
			break;
		}
		case GameBuilderMode.GameBackground:
		{
			WorldWrapper.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(2);
			UITab[] tabsInGroup11 = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
			foreach (UITab uITab11 in tabsInGroup11)
			{
				if (uITab11.id != 2 && uITab11.id != 0)
				{
					uITab11.DeActivate();
				}
			}
			BloxelLibrary.instance.libraryToggle.Show();
			break;
		}
		case GameBuilderMode.Test:
			PixelEditorController.instance.primaryCanvas.gameObject.SetActive(false);
			BloxelLibrary.instance.libraryCanvas.gameObject.SetActive(false);
			SoundManager.PlayOneShot(SoundManager.instance.editorPlay);
			GameplayController.instance.currentMode = GameplayController.Mode.Test;
			GameplayController.instance.gameplayActive = true;
			GameplayController.instance.currentGame = currentGame;
			if (!BloxelLibrary.instance.libraryToggle.isClosed)
			{
				BloxelLibrary.instance.libraryToggle.Toggle();
			}
			BloxelLibrary.instance.Hide();
			if (!WorldWrapper.instance.gameObject.activeInHierarchy)
			{
				WorldWrapper.instance.Show();
			}
			else
			{
				BloxelEnemyTile[] componentsInChildren = WorldWrapper.instance.GetComponentsInChildren<BloxelEnemyTile>();
				BloxelEnemyTile[] array = componentsInChildren;
				foreach (BloxelEnemyTile bloxelEnemyTile in array)
				{
					bloxelEnemyTile.target = GameplayController.instance.heroObject.transform;
				}
			}
			GameplayController.instance.startingPosition = new Vector3(GameplayController.instance.heroObject.transform.localPosition.x, GameplayController.instance.heroObject.transform.localPosition.y, 0f);
			GameplayController.instance.lastCheckpoint = GameplayController.instance.startingPosition;
			if (PixelPlayerController.instance != null && PixelPlayerController.instance.selfTransform != null)
			{
				PixelPlayerController.instance.selfTransform.localPosition = new Vector3(PixelPlayerController.instance.selfTransform.localPosition.x, PixelPlayerController.instance.selfTransform.localPosition.y, 0f);
			}
			GameplayController.instance.StartDroppingCharacter();
			BloxelCamera.instance.target = GameplayController.instance.heroObject;
			BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
			if (this.onGameStart != null)
			{
				this.onGameStart();
			}
			SoundManager.instance.PlayMusic(SoundManager.instance.musicChoiceLookup[currentGame.musicChoice]);
			WorldWrapper.instance.currentLevelBounds.gameObject.SetActive(false);
			break;
		}
		if (this.OnGameBuilderModeChange != null)
		{
			this.OnGameBuilderModeChange(currentMode);
		}
	}

	public void SetUpConfigIndicators(GridLocation levelLocationInWorld)
	{
		int num = levelLocationInWorld.c * 13;
		int num2 = levelLocationInWorld.r * 13;
		int num3 = num;
		int num4 = 0;
		while (num3 < num + 13)
		{
			int num5 = num2;
			int num6 = 0;
			while (num5 < num2 + 13)
			{
				List<TileInfo> list = GameplayBuilder.instance.worldTileData[num3, num5];
				if (list != null && list.Count != 0)
				{
					GameBehavior gameBehavior = list[0].gameBehavior;
					if (gameBehavior == GameBehavior.Enemy || gameBehavior == GameBehavior.NPC || gameBehavior == GameBehavior.PowerUp || gameBehavior == GameBehavior.Plain)
					{
						ConfigIndicator configIndicator = ConfigIndicatorPool.instance.Spawn();
						configIndicator.selfTransform.SetParent(WorldWrapper.instance.configIndicatorContainer);
						configIndicator.Init(new GridLocation(num4, num6), new Vector3(num3 * 13, num5 * 13, 0f));
						configIndicator.Bounce();
						ConfigIndicatorContainer.ActiveIndicators[num4, num6] = configIndicator;
					}
				}
				else
				{
					ConfigIndicatorContainer.ActiveIndicators[num4, num6] = null;
				}
				num5++;
				num6++;
			}
			num3++;
			num4++;
		}
	}
}
