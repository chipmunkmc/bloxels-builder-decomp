public enum GameBuilderMode
{
	None = 0,
	QuickStart = 15,
	Build = 1,
	Wireframe = 2,
	Map = 3,
	Decorate = 4,
	PaintArt = 5,
	CoverArt = 6,
	LevelBackground = 7,
	GameBackground = 8,
	Config = 9,
	ConfigBlocks = 14,
	CharacterPlacement = 10,
	GameSettings = 11,
	Test = 13
}
