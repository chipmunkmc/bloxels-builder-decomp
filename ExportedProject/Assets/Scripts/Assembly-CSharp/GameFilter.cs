public enum GameFilter
{
	Newest = 0,
	Plays = 1,
	FeaturedCreators = 2,
	Likes = 3,
	BigGames = 4
}
