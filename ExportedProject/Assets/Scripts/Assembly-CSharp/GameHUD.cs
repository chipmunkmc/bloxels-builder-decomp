using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour
{
	public enum ResetMethod
	{
		Full = 0,
		FromCheckpoint = 2
	}

	public static GameHUD instance;

	public BloxelGame gameData;

	public Canvas canvasParent;

	public Canvas inputCanvas2;

	public Transform touchControls;

	public Button uiButtonPause;

	public Text uiTextTimer;

	public Text uiTextCoinCount;

	private int _elapsedTime;

	private int _coinCount;

	private int _hearts;

	private int _maxHearts = 3;

	private int _totalCoins;

	public Image iconTimer;

	public Image iconBomb;

	public Image iconHeart;

	public Image iconCoin;

	public Sprite[] hudInventorySprites;

	public Sprite[] hudItemElementSprites;

	public Text uiTextPlayerBank;

	public int blocksDestroyed;

	private int _enemiesDefeated;

	public int blocksDestroyedSinceCheckpoint;

	public int enemiesDefeatedSinceCheckpoint;

	public Button leftArrow;

	public Button rightArrow;

	public Button actionButton1;

	private RectTransform actionButtonTransform;

	public Text uiTextActionButton1;

	public Button actionButton2;

	public Image uiImageActionButton2;

	private RectTransform movementButtonTransform;

	public Text uiTextActionButton2;

	private ColorBlock actionButtonColorBlock;

	public RectTransform hudItemContainer;

	public UIHudItem hudItemTextPrefab;

	public UIHudItemElements hudItemElementsPrefab;

	private Vector3 actionHudPosition = new Vector3(-540f, 0f, 0f);

	public Image actionCockIcon;

	public CanvasGroup actionIndicator;

	public UIHudItem actionHUD;

	public InventoryData actionDataModel;

	public CanvasGroup cockContainerAction;

	public RectTransform actionButtonContainer;

	private Vector2 _actionButtonContainerNormal;

	private Vector2 _actionButtonContainerEquipped;

	public bool isActionButtonReset;

	private Vector2 _actionButtonStartPos;

	private Vector3 movementHudPosition = new Vector3(-720f, 0f, 0f);

	public Image movementCockIcon;

	public CanvasGroup movementIndicator;

	public UIHudItem movementHUD;

	public CanvasGroup cockContainerMovement;

	public RectTransform movementButtonContainer;

	private Vector2 _movementButtonContainerNormal;

	private Vector2 _movementButtonContainerEquipped;

	public bool isMovementButtonReset;

	private Vector2 _movementButtonStartPos;

	public UIHudItem healthHUD;

	private Coroutine _timerCoroutine;

	public GameObject inputReceiver;

	public UnityEngine.Object uiPopupPausePrefab;

	private bool actionEquipped;

	private bool movementEquipped;

	public bool npcActive;

	public int enemiesDefeated
	{
		get
		{
			return _enemiesDefeated;
		}
		set
		{
			_enemiesDefeated = value;
			if (_enemiesDefeated > 0)
			{
				enemiesDefeatedSinceCheckpoint++;
			}
		}
	}

	private void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		movementButtonTransform = actionButton1.GetComponent<RectTransform>();
		actionButtonTransform = actionButton2.GetComponent<RectTransform>();
		_actionButtonStartPos = actionButtonTransform.anchoredPosition;
		_movementButtonStartPos = movementButtonTransform.anchoredPosition;
		actionButtonColorBlock = actionButton2.colors;
		_movementButtonContainerNormal = new Vector2(movementButtonContainer.sizeDelta.x, movementButtonContainer.sizeDelta.y);
		_movementButtonContainerEquipped = new Vector2(movementButtonContainer.sizeDelta.x, movementButtonContainer.sizeDelta.y + 55f);
		_actionButtonContainerNormal = new Vector2(actionButtonContainer.anchoredPosition.x, actionButtonContainer.anchoredPosition.y);
		_actionButtonContainerEquipped = new Vector2(actionButtonContainer.anchoredPosition.x, actionButtonContainer.anchoredPosition.y + 20f);
		uiPopupPausePrefab = Resources.Load("Prefabs/UIPopupPause");
	}

	private void OnEnable()
	{
		SetupEvents();
		uiButtonPause.gameObject.SetActive(true);
		_elapsedTime = 0;
		_coinCount = 0;
		MaxHealth();
		if (GameplayController.instance.currentGame != null)
		{
			_totalCoins = GameplayController.instance.currentGame.TotalCoinsInGame();
		}
		uiTextCoinCount.text = _coinCount.ToString() + "/" + _totalCoins;
		uiTextTimer.text = "00:00";
		uiTextPlayerBank.text = PlayerBankManager.instance.GetCoinCount().ToString("N0");
		EnableTimer();
	}

	private void OnDisable()
	{
		RemoveEvents();
		_elapsedTime = 0;
		_hearts = 3;
		_coinCount = 0;
		uiTextCoinCount.text = _coinCount.ToString() + "/" + _totalCoins;
		uiTextTimer.text = "00:00";
		StopTimer();
	}

	private void Start()
	{
		canvasParent.worldCamera = UIOverlayCanvas.instance.overlayCamera;
		inputCanvas2.worldCamera = UIOverlayCanvas.instance.overlayCamera;
	}

	public void EnableTimer()
	{
		if (_timerCoroutine == null)
		{
			_timerCoroutine = StartCoroutine(Timer());
		}
	}

	public void StopTimer()
	{
		if (_timerCoroutine != null)
		{
			StopCoroutine(_timerCoroutine);
			_timerCoroutine = null;
		}
	}

	private IEnumerator Timer()
	{
		WaitForSeconds waitForOneSecond = new WaitForSeconds(1f);
		while (true)
		{
			yield return waitForOneSecond;
			_elapsedTime++;
			uiTextTimer.text = FormatTimerText();
		}
	}

	public string FormatTimerText()
	{
		TimeSpan timeSpan = TimeSpan.FromSeconds(_elapsedTime);
		return string.Format("{0:D2}:{1:D2}", timeSpan.Minutes, timeSpan.Seconds);
	}

	private void RemoveEvents()
	{
		uiButtonPause.onClick.RemoveListener(PauseButtonPressed);
		PlayerBankManager.instance.OnCoinUpdate -= PlayerBankManager_instance_OnCoinUpdate;
	}

	private void SetupEvents()
	{
		uiButtonPause.onClick.AddListener(PauseButtonPressed);
		PlayerBankManager.instance.OnCoinUpdate += PlayerBankManager_instance_OnCoinUpdate;
	}

	public void InitActionHUDItem(InventoryData dataModel)
	{
		UIHudItem uIHudItem = actionHUD;
		switch (dataModel.displayType)
		{
		default:
			return;
		case InventoryData.DisplayType.Text:
			actionHUD = ObjectPool.Spawn(hudItemTextPrefab, hudItemContainer.transform);
			break;
		case InventoryData.DisplayType.Elements:
			actionHUD = ObjectPool.Spawn((UIHudItem)hudItemElementsPrefab, hudItemContainer.transform);
			((UIHudItemElements)actionHUD).SetElementSprites(hudItemElementSprites[(int)dataModel.type]);
			break;
		}
		if (uIHudItem != null)
		{
			uIHudItem.Disable();
		}
		actionHUD.selfRect.localScale = Vector3.one;
		actionHUD.selfRect.anchoredPosition3D = actionHudPosition;
		actionHUD.Init(dataModel.remainingQuantity);
		actionHUD.icon.sprite = hudInventorySprites[(int)dataModel.type];
		actionHUD.Enable();
		ActivateActionButton();
	}

	public void InitMovementHUDItem(InventoryData dataModel)
	{
		UIHudItem uIHudItem = movementHUD;
		switch (dataModel.displayType)
		{
		default:
			return;
		case InventoryData.DisplayType.Text:
			movementHUD = ObjectPool.Spawn(hudItemTextPrefab, hudItemContainer.transform);
			break;
		case InventoryData.DisplayType.Elements:
			movementHUD = ObjectPool.Spawn((UIHudItem)hudItemElementsPrefab, hudItemContainer.transform);
			((UIHudItemElements)movementHUD).SetElementSprites(hudItemElementSprites[(int)dataModel.type]);
			break;
		}
		if (uIHudItem != null)
		{
			uIHudItem.Disable();
		}
		movementHUD.selfRect.localScale = Vector3.one;
		movementHUD.selfRect.anchoredPosition3D = movementHudPosition;
		movementHUD.Init(dataModel.remainingQuantity);
		movementHUD.icon.sprite = hudInventorySprites[(int)dataModel.type];
		movementHUD.Enable();
		ActivateMovementButton();
	}

	public void InitHUDItem(InventoryData dataModel)
	{
		switch (dataModel.behavior)
		{
		case InventoryData.Behavior.Action:
			InitActionHUDItem(dataModel);
			break;
		case InventoryData.Behavior.Movement:
			InitMovementHUDItem(dataModel);
			break;
		}
	}

	public void UpdateActionHUDItem(int newCount)
	{
		actionHUD.UpdateDisplay(newCount);
	}

	public void UpdateMovementHUDItem(int newCount)
	{
		movementHUD.UpdateDisplay(newCount);
	}

	private void PlayerBankManager_instance_OnCoinUpdate(int _coins)
	{
		if (!(uiTextPlayerBank.text == _coins.ToString()))
		{
			uiTextPlayerBank.DOText(_coins.ToString("N0"), UIAnimationManager.speedSlow);
		}
	}

	public void CoinReward(int val)
	{
		_coinCount += val;
		PlayerBankManager.instance.AddCoins(val);
		if (GameplayController.instance.currentMode != 0)
		{
			uiTextPlayerBank.DOText(PlayerBankManager.instance.GetCoinCount().ToString("N0"), UIAnimationManager.speedSlow);
		}
	}

	public void CollectCoins(int val)
	{
		_coinCount += val;
		uiTextCoinCount.text = _coinCount.ToString() + "/" + _totalCoins;
		if (GameplayController.instance.currentMode != 0)
		{
			uiTextPlayerBank.text = PlayerBankManager.instance.AddCoin().ToString("N0");
		}
	}

	public int RemoveCoins(int val)
	{
		if (_coinCount == 0)
		{
			return 0;
		}
		if (val > _coinCount)
		{
			val = _coinCount;
		}
		_coinCount -= val;
		uiTextCoinCount.text = _coinCount.ToString() + "/" + _totalCoins;
		if (GameplayController.instance.currentMode != 0)
		{
			uiTextPlayerBank.text = PlayerBankManager.instance.RemoveCoins(val).ToString("N0");
		}
		return val;
	}

	public void CollectCoin()
	{
		_coinCount++;
		uiTextCoinCount.text = _coinCount.ToString() + "/" + _totalCoins;
		if (GameplayController.instance.currentMode != 0)
		{
			uiTextPlayerBank.text = PlayerBankManager.instance.AddCoin().ToString("N0");
		}
	}

	public int GetTotalCoins()
	{
		return _totalCoins;
	}

	public void SetTotalCoinsFromConfig()
	{
		if (!(GameBuilderCanvas.instance == null) && GameBuilderCanvas.instance.currentGame != null)
		{
			_totalCoins = GameBuilderCanvas.instance.currentGame.TotalCoinsInGame();
			if (uiTextCoinCount != null && uiTextCoinCount.text != null)
			{
				uiTextCoinCount.text = _coinCount + "/" + _totalCoins;
			}
		}
	}

	public int GetCoinsCollected()
	{
		return _coinCount;
	}

	public int GetRemaingHealth()
	{
		return _hearts;
	}

	public int GetMaxHealth()
	{
		return _maxHearts;
	}

	public void MaxHealth()
	{
		_hearts = _maxHearts;
		healthHUD.Init(_maxHearts, true);
	}

	public void AddHearts(int amount)
	{
		_hearts += amount;
		healthHUD.Increment(amount);
	}

	public void RemoveHearts(int amount = 1)
	{
		_hearts -= amount;
		healthHUD.Decrement(amount);
	}

	public void SetControls_NPCInteraction()
	{
		uiTextActionButton2.fontSize = 32;
		uiTextActionButton2.DOColor(new Color(0.7f, 0.7f, 0.7f), UIAnimationManager.speedFast);
		uiTextActionButton2.DOText(LocalizationManager.getInstance().getLocalizedText("gameplay16", "Talk"), UIAnimationManager.speedFast);
		uiImageActionButton2.DOColor(Color.white, UIAnimationManager.speedMedium);
		npcActive = true;
	}

	public void SetControls_Normal()
	{
		uiTextActionButton2.fontSize = 75;
		uiTextActionButton2.DOColor(new Color(1f, 0.78f, 0.78f, 1f), UIAnimationManager.speedFast);
		uiTextActionButton2.DOText("A", UIAnimationManager.speedFast);
		uiImageActionButton2.DOColor(new Color(73f / 85f, 0.1764706f, 0.24313726f, 1f), UIAnimationManager.speedMedium);
		npcActive = false;
	}

	public void ActivateActionButton()
	{
		actionEquipped = true;
		actionCockIcon.sprite = hudInventorySprites[(int)GameplayController.instance.inventoryManager.currentAction.type];
		actionButtonContainer.DOKill(true);
		cockContainerAction.DOKill(true);
		actionIndicator.DOKill(true);
		actionButtonTransform.DOKill(true);
		actionButtonContainer.DOAnchorPos(_actionButtonContainerEquipped, UIAnimationManager.speedFast).OnComplete(delegate
		{
			cockContainerAction.DOFade(1f, UIAnimationManager.speedFast);
			actionButtonTransform.DOAnchorPos(new Vector2(actionButtonTransform.anchoredPosition.x, actionButtonTransform.anchoredPosition.y - 55f), UIAnimationManager.speedFast).OnStart(delegate
			{
				actionIndicator.DOFade(1f, UIAnimationManager.speedSlow);
			}).OnComplete(delegate
			{
				actionButtonTransform.DOAnchorPos(new Vector2(actionButtonTransform.anchoredPosition.x, actionButtonTransform.anchoredPosition.y + 55f), UIAnimationManager.speedFast);
			});
		});
	}

	public void ActivateMovementButton()
	{
		movementEquipped = true;
		movementCockIcon.sprite = hudInventorySprites[(int)GameplayController.instance.inventoryManager.currentMovement.type];
		movementButtonContainer.DOKill(true);
		cockContainerMovement.DOKill(true);
		movementIndicator.DOKill(true);
		movementButtonTransform.DOKill(true);
		cockContainerMovement.DOFade(1f, UIAnimationManager.speedFast);
		movementButtonTransform.DOAnchorPos(new Vector2(movementButtonTransform.anchoredPosition.x, movementButtonTransform.anchoredPosition.y - 55f), UIAnimationManager.speedFast).OnStart(delegate
		{
			movementIndicator.DOFade(1f, UIAnimationManager.speedSlow);
		}).OnComplete(delegate
		{
			movementButtonTransform.DOAnchorPos(new Vector2(movementButtonTransform.anchoredPosition.x, movementButtonTransform.anchoredPosition.y + 55f), UIAnimationManager.speedFast);
		});
	}

	public void DeactivateActionButton()
	{
		actionEquipped = false;
		ResetActionButton();
		actionButtonContainer.DOKill(true);
		cockContainerAction.DOKill(true);
		actionButtonContainer.DOAnchorPos(_actionButtonContainerNormal, UIAnimationManager.speedMedium);
		cockContainerAction.DOFade(0f, UIAnimationManager.speedFast);
	}

	public void DeactivateMovementButton()
	{
		movementEquipped = false;
		ResetMovementButton();
		movementButtonContainer.DOKill(true);
		cockContainerMovement.DOKill(true);
		movementButtonContainer.DOSizeDelta(_movementButtonContainerNormal, UIAnimationManager.speedMedium);
		cockContainerMovement.DOFade(0f, UIAnimationManager.speedFast);
	}

	public void DeactivateActionHUD(bool immediate = false)
	{
		if (!(actionHUD == null))
		{
			actionHUD.Disable(immediate);
			actionHUD = null;
			DeactivateActionButton();
		}
	}

	public void DeactivateMovementHUD(bool immediate = false)
	{
		if (!(movementHUD == null))
		{
			movementHUD.Disable(immediate);
			movementHUD = null;
			DeactivateMovementButton();
		}
	}

	public void SetJetpackFuel(float percentage)
	{
		InventoryData inventoryData = GameplayController.instance.inventoryManager.inventory[1];
		int num = Mathf.CeilToInt(percentage * (float)inventoryData.maxQuantity);
		if (num != inventoryData.remainingQuantity)
		{
			inventoryData.remainingQuantity = num;
			UpdateMovementHUDItem(num);
			if (!inventoryData.available)
			{
				GameplayController.instance.inventoryManager.RemoveFromInventory(InventoryData.Type.Jetpack);
			}
		}
	}

	public void Reset(ResetMethod method)
	{
		GameplayController.instance.inventoryManager.ClearInventoryHUD();
		switch (method)
		{
		case ResetMethod.Full:
			blocksDestroyed = 0;
			enemiesDefeated = 0;
			_elapsedTime = 0;
			_coinCount = 0;
			_totalCoins = GameplayController.instance.currentGame.TotalCoinsInGame();
			uiTextCoinCount.text = _coinCount + "/" + _totalCoins;
			GameplayController.instance.inventoryManager.ResetInventory();
			break;
		case ResetMethod.FromCheckpoint:
			blocksDestroyed -= blocksDestroyedSinceCheckpoint;
			enemiesDefeated -= enemiesDefeatedSinceCheckpoint;
			blocksDestroyedSinceCheckpoint = 0;
			enemiesDefeatedSinceCheckpoint = 0;
			GameplayController.instance.inventoryManager.LoadInventoryFromCheckpoint();
			break;
		}
		MaxHealth();
		SetControls_Normal();
		EnableTimer();
	}

	public void ResetMovementButton()
	{
		if (!isMovementButtonReset)
		{
			movementButtonTransform.DOKill(true);
			movementButtonTransform.DOAnchorPos(_movementButtonStartPos, UIAnimationManager.speedFast).OnComplete(delegate
			{
				isMovementButtonReset = true;
			});
		}
	}

	public bool MoveMovementButtonYAxis(float amount)
	{
		isMovementButtonReset = false;
		amount /= PPInputController.instance.canvasUIBase1024.scaleFactor;
		float num = Mathf.Clamp(amount, -60f, 0f);
		movementButtonTransform.anchoredPosition = new Vector2(_movementButtonStartPos.x, _movementButtonStartPos.y + num);
		if (num < -55f)
		{
			return true;
		}
		return false;
	}

	public void ResetActionButton()
	{
		if (!isActionButtonReset)
		{
			actionButtonTransform.DOKill(true);
			actionButtonTransform.DOAnchorPos(_actionButtonStartPos, UIAnimationManager.speedFast).OnComplete(delegate
			{
				isActionButtonReset = true;
			});
		}
	}

	public bool MoveActionButtonYAxis(float amount)
	{
		isActionButtonReset = false;
		amount /= PPInputController.instance.canvasUIBase1024.scaleFactor;
		float num = Mathf.Clamp(amount, -60f, 0f);
		actionButtonTransform.anchoredPosition = new Vector2(_actionButtonStartPos.x, _actionButtonStartPos.y + num);
		if (num < -55f)
		{
			return true;
		}
		return false;
	}

	public void GoToEditor()
	{
		SoundManager.PlaySoundClip(SoundClip.cancelA);
		if (PixelEditorController.instance.mode == CanvasMode.CharacterBuilder)
		{
			Reset(ResetMethod.Full);
			CharacterBuilder.instance.ComeBackFromPreview();
		}
		else
		{
			Reset(ResetMethod.Full);
			GameBuilderCanvas.instance.ComeBackFromTest();
		}
	}

	public void PauseButtonPressed()
	{
		UIInventoryItem.currentActionItem = null;
		UIInventoryItem.currentMovementItem = null;
		SoundManager.PlaySoundClip(SoundClip.pause);
		GameObject gameObject = UIOverlayCanvas.instance.Popup(uiPopupPausePrefab);
		UIPopupPause component = gameObject.GetComponent<UIPopupPause>();
		component.SetGame(ref GameplayBuilder.instance.game);
		component.Init();
		touchControls.localScale = Vector3.zero;
	}
}
