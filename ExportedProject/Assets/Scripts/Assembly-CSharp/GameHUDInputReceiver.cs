using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using VoxelEngine;

public class GameHUDInputReceiver : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	public enum InputMode
	{
		None = 0,
		Layout = 1,
		Config = 2,
		Decorate = 3
	}

	public static GameHUDInputReceiver instance;

	public InputMode mode;

	public BloxelBoard currentBloxelBoard;

	public SavedProject currentProject;

	private UIPopupEnemyConfig uiConfigPurpleMenu;

	private bool isDragging;

	private int numberOfTiles = 169;

	private bool[] changedSpaces;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		changedSpaces = new bool[169];
	}

	private void Start()
	{
		BloxelBoardLibrary.instance.OnProjectSelect += HandleOnBoardChange;
		BloxelAnimationLibrary.instance.OnProjectSelect += HandleOnBoardChange;
	}

	private void DeActivate()
	{
		mode = InputMode.None;
		base.gameObject.SetActive(false);
	}

	private void HandleOnBoardChange(SavedProject project)
	{
		currentProject = project;
		switch (project.dataModel.type)
		{
		case ProjectType.Board:
			if (!AssetManager.instance.boardPool.TryGetValue(project.dataModel.ID(), out currentBloxelBoard))
			{
				currentBloxelBoard = project.dataModel as BloxelBoard;
			}
			break;
		case ProjectType.Animation:
			if (!AssetManager.instance.boardPool.TryGetValue(((BloxelAnimation)project.dataModel).coverBoard.ID(), out currentBloxelBoard))
			{
				currentBloxelBoard = ((BloxelAnimation)project.dataModel).boards[0];
			}
			break;
		}
	}

	private void ConfigItem()
	{
		WorldPos2D tileWorldPosition = GetTileWorldPosition();
		BloxelTile value = null;
		GameplayBuilder.instance.tiles.TryGetValue(tileWorldPosition, out value);
		if (!(value == null) && mode == InputMode.Config && value.configurable)
		{
			switch (value.tileInfo.wireframeColor)
			{
			case BlockColor.White:
				break;
			case BlockColor.Purple:
				break;
			case BlockColor.Pink:
				break;
			}
		}
	}

	private void PaintSelectedBoardIntoWireframe()
	{
		if (!CheckValidPaintState())
		{
			return;
		}
		WorldPos2D tileWorldPosition = GetTileWorldPosition();
		int num = tileWorldPosition.x / 13;
		int num2 = tileWorldPosition.y / 13;
		List<TileInfo> list = GameplayBuilder.instance.worldTileData[num, num2];
		if (list.Count != 0)
		{
			TileInfo tileInfo = list[0];
			tileInfo.data = currentProject.dataModel;
			BloxelTile value = null;
			if (GameplayBuilder.instance.tiles.TryGetValue(tileWorldPosition, out value))
			{
				GameplayBuilder.instance.UnloadTile(value);
			}
		}
	}

	private bool CheckValidPaintState()
	{
		if (currentBloxelBoard != null && mode == InputMode.Decorate)
		{
			return true;
		}
		return false;
	}

	private bool CheckValidConfigState()
	{
		if (mode == InputMode.Config)
		{
			return true;
		}
		return false;
	}

	public static WorldPos2D GetTileWorldPosition()
	{
		Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.mainCamera, Input.mousePosition, WorldWrapper.instance.selfTransform.position.z);
		int x = Mathf.FloorToInt(worldPositionOnXYPlane.x / 13f) * 13;
		int y = Mathf.FloorToInt(worldPositionOnXYPlane.y / 13f) * 13;
		return new WorldPos2D(x, y);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!isDragging && mode != 0)
		{
			if (CheckValidConfigState())
			{
				ConfigItem();
			}
			else
			{
				PaintSelectedBoardIntoWireframe();
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		Array.Clear(changedSpaces, 0, changedSpaces.Length);
		if (!CheckValidConfigState())
		{
			PaintSelectedBoardIntoWireframe();
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		isDragging = true;
		PaintSelectedBoardIntoWireframe();
	}

	public void OnDrag(PointerEventData eventData)
	{
		PaintSelectedBoardIntoWireframe();
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		isDragging = false;
	}
}
