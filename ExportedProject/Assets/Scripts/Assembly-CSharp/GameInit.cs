using System.Collections;
using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameInit : MonoBehaviour
{
	public TextMeshProUGUI uiTextLoading;

	public TextMeshProUGUI uiTextDidYouKnow;

	public TextMeshProUGUI uiTextInfo;

	private DirectoryInfo[] _guestDirectories;

	private bool _shouldQueueGuestFiles;

	private int _currentTipIndex;

	private string[] tips = new string[9] { "Everything in Bloxels starts with a Board and grows from there.", "You can drag and drop rooms into new positions from the \"Map\" view", "You can drag and drop blocks into new positions from the \"Configure\" section", "You can create 169 rooms or \"levels\" in a Bloxels Game.", "You can animate Enemies and Terrain Blocks to make disappearing obstacles.", "You can make small pathways in Terrain Blocks to use when shrunk.", "You can add art to terrain to make different shapes and sizes!", "You can combine multiple boards to make one big art piece!", "You can upload and share your creations on the infinity wall." };

	private string[] keys = new string[9] { "loadingscreen9", "loadingscreen10", "loadingscreen11", "loadingscreen12", "loadingscreen13", "loadingscreen14", "loadingscreen15", "loadingscreen16", "loadingscreen17" };

	private Tweener _loadingTween;

	private void Awake()
	{
		uiTextLoading.text = "Loading";
	}

	private void Start()
	{
		AppStateManager.instance.SetCurrentScene(SceneName.GameInit);
		SoundManager.instance.PlayMusic(SoundManager.instance.choice5);
		InvokeRepeating("LoadRandomTip", 0f, 4f);
		IEnumerator enumerator = null;
		if (CurrentUser.instance.active && !PrefsManager.instance.hasRunInitialS3Sync)
		{
			enumerator = guestMoveAndLoad();
		}
		else if (shouldCloudSyncFirst())
		{
			enumerator = cloudSyncAndLoad();
		}
		else
		{
			AssetManager.instance.Setup();
			enumerator = delayLoad();
		}
		StartCoroutine(enumerator);
	}

	private void OnDestroy()
	{
		unload();
	}

	private void unload()
	{
		CancelInvoke("LoadRandomTip");
		if (_loadingTween != null)
		{
			_loadingTween.Kill();
		}
		_loadingTween = null;
		uiTextLoading = null;
		uiTextDidYouKnow = null;
		uiTextInfo = null;
		tips = null;
	}

	private bool shouldCloudSyncFirst()
	{
		return Application.internetReachability != 0 && CurrentUser.instance.active;
	}

	private IEnumerator guestMoveAndLoad()
	{
		AssetManager.instance.SetBaseStoragePath();
		_shouldQueueGuestFiles = true;
		_guestDirectories = copyGuestFiles();
		AssetManager.instance.Setup();
		yield return StartCoroutine(delayLoad());
	}

	private IEnumerator cloudSyncAndLoad()
	{
		AssetManager.instance.SetBaseStoragePath();
		CloudManager.Instance.SetupSyncFile();
		string[] diskIds = File.ReadAllLines(CloudManager.SyncFile);
		bool hasValidIds = false;
		for (int i = 0; i < diskIds.Length; i++)
		{
			if (!string.IsNullOrEmpty(diskIds[i]))
			{
				hasValidIds = true;
				break;
			}
		}
		if (hasValidIds)
		{
			AssetManager.instance.Setup();
			StartCoroutine(delayLoad());
			yield break;
		}
		if (!PrefsManager.instance.hasRunInitialS3Sync)
		{
			_shouldQueueGuestFiles = true;
			_guestDirectories = copyGuestFiles();
		}
		yield return StartCoroutine(CloudManager.Instance.GetCloudZip(delegate(byte[] stream)
		{
			if (stream != null)
			{
				Zipper.ExtractFromStream(stream, AssetManager.baseStoragePath, false);
			}
			AssetManager.instance.Setup();
			StartCoroutine(delayLoad());
		}));
	}

	private IEnumerator delayLoad()
	{
		while (!AssetManager.initComplete)
		{
			yield return new WaitForEndOfFrame();
		}
		if (_shouldQueueGuestFiles)
		{
			queueGuestFilesForSync(_guestDirectories);
		}
		SceneName scene = SceneName.Home;
		string preferredLanguage = PrefsManager.instance.preferredLanguage;
		if (preferredLanguage != null && preferredLanguage.Length > 0)
		{
			scene = SceneName.Home;
		}
		else if (!LocalizationManager.SysLanguageIsSupported)
		{
			scene = SceneName.LanguageSelect;
		}
		StartCoroutine(LoadScene(scene));
	}

	private DirectoryInfo[] copyGuestFiles()
	{
		DirectoryInfo[] topLevelGuestDirectories = BloxelProject.GetTopLevelGuestDirectories();
		for (int i = 0; i < topLevelGuestDirectories.Length; i++)
		{
			PPUtilities.CopyDirectory(topLevelGuestDirectories[i].FullName, AssetManager.baseStoragePath + topLevelGuestDirectories[i].Name);
		}
		return topLevelGuestDirectories;
	}

	private void queueGuestFilesForSync(DirectoryInfo[] directories)
	{
		foreach (DirectoryInfo directoryInfo in directories)
		{
			IEnumerator<string> enumerator = PPUtilities.GetFileList(directoryInfo.FullName, "*").GetEnumerator();
			while (enumerator.MoveNext())
			{
				DiskPathInfo diskPathInfo = new DiskPathInfo(enumerator.Current, CurrentUser.instance.account);
				ProjectSyncInfo syncInfo = new ProjectSyncInfo(diskPathInfo.diskPathString, diskPathInfo.s3Path, diskPathInfo.guid, diskPathInfo.type);
				CloudManager.Instance.EnqueueFileForSync(syncInfo, false);
			}
		}
	}

	private void LoadRandomTip()
	{
		int num = Random.Range(0, tips.Length);
		if (num == _currentTipIndex)
		{
			num = (num + 1) % tips.Length;
		}
		_currentTipIndex = num;
		string unityKey = keys[_currentTipIndex];
		string defaultText = tips[_currentTipIndex];
		uiTextInfo.SetText(LocalizationManager.getInstance().getLocalizedText(unityKey, defaultText));
	}

	private IEnumerator LoadScene(SceneName name)
	{
		AsyncOperation loader = SceneManager.LoadSceneAsync(name.ToString());
		while (!loader.isDone)
		{
			yield return null;
		}
	}
}
