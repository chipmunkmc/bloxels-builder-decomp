using System.Collections.Generic;
using UnityEngine;

public class GameMap
{
	public enum Type
	{
		Gameplay = 0,
		iWall = 1
	}

	public bool playerHasPowerup;

	public Vector2 mapSize;

	public static Texture2D gameMapTexture = new Texture2D(169, 169, TextureFormat.RGBA32, false);

	public BloxelGame currentGame;

	public Vector2 gameStartPosition = Vector2.zero;

	public Vector2 gameEndPosition = Vector2.zero;

	public Vector2 fullGameCenter;

	public GameMap(BloxelGame game, Type _type, bool hasPowerup = false)
	{
		gameMapTexture.filterMode = FilterMode.Point;
		gameMapTexture.wrapMode = TextureWrapMode.Clamp;
		currentGame = game;
		playerHasPowerup = hasPowerup;
		mapSize = new Vector2(currentGame.worldBoundingRect.width, currentGame.worldBoundingRect.height);
		fullGameCenter = new Vector2(1098.5f, 1098.5f);
		gameStartPosition = new Vector2((float)(currentGame.heroStartPosition.levelLocationInWorld.c * 169 + currentGame.heroStartPosition.locationInBoard.c * 13) + 6.5f, currentGame.heroStartPosition.levelLocationInWorld.r * 169 + currentGame.heroStartPosition.locationInBoard.r * 13);
		if (!currentGame.gameEndFlag.Equals(WorldLocation.InvalidLocation()))
		{
			gameEndPosition = new Vector2((float)(currentGame.gameEndFlag.levelLocationInWorld.c * 169 + currentGame.gameEndFlag.locationInBoard.c * 13) + 6.5f, currentGame.gameEndFlag.levelLocationInWorld.r * 169 + currentGame.gameEndFlag.locationInBoard.r * 13);
		}
		if (_type == Type.iWall)
		{
			InitMapFromModel();
		}
		else if (hasPowerup)
		{
			InitFullMap();
		}
		else
		{
			InitObscuredMap();
		}
	}

	public void InitMapFromModel()
	{
		Color[] colors = AssetManager.instance.colorBuffer13x13;
		Color[] clearColors13x = AssetManager.instance.clearColors13x13;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				GridLocation key = new GridLocation(j, i);
				BloxelLevel value;
				if (!currentGame.levels.TryGetValue(key, out value))
				{
					gameMapTexture.SetPixels(j * 13, i * 13, 13, 13, clearColors13x);
					continue;
				}
				value.coverBoard.GetColorsNonAlloc(ref colors);
				gameMapTexture.SetPixels(j * 13, i * 13, 13, 13, colors);
			}
		}
		gameMapTexture.Apply();
	}

	public void InitFullMap()
	{
		Color[] colorBuffer13x = AssetManager.instance.colorBuffer13x13;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				int num = j * 13;
				int num2 = i * 13;
				for (int k = 0; k < 13; k++)
				{
					for (int l = 0; l < 13; l++)
					{
						BlockColor blockColor = BlockColor.Blank;
						List<TileInfo> list = GameplayBuilder.instance.worldTileData[num + l, num2 + k];
						if (list != null && list.Count != 0)
						{
							TileInfo tileInfo = list[0];
							if (tileInfo.shouldRespawn)
							{
								blockColor = (BlockColor)tileInfo.gameBehavior;
								if (blockColor == BlockColor.Purple)
								{
									blockColor = BlockColor.Blank;
								}
							}
						}
						colorBuffer13x[k * 13 + l] = ColorUtilities.blockColorArrayRGB[(uint)blockColor];
					}
				}
				gameMapTexture.SetPixels(num, num2, 13, 13, colorBuffer13x);
			}
		}
		gameMapTexture.Apply();
	}

	public void InitObscuredMap()
	{
		Color[] colorBuffer13x = AssetManager.instance.colorBuffer13x13;
		Color color = new Color(0.7f, 0.7f, 0.7f, 1f);
		Color clear = Color.clear;
		Color color2 = clear;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				int num = j * 13;
				int num2 = i * 13;
				for (int k = 0; k < 13; k++)
				{
					for (int l = 0; l < 13; l++)
					{
						color2 = clear;
						BlockColor blockColor = BlockColor.Blank;
						List<TileInfo> list = GameplayBuilder.instance.worldTileData[num + l, num2 + k];
						if (list != null && list.Count != 0)
						{
							TileInfo tileInfo = list[0];
							if (tileInfo.shouldRespawn)
							{
								blockColor = (BlockColor)tileInfo.gameBehavior;
								switch (blockColor)
								{
								case BlockColor.Green:
									color2 = color;
									break;
								case BlockColor.Pink:
									color2 = color;
									break;
								case BlockColor.Orange:
									color2 = color;
									break;
								case BlockColor.Purple:
									color2 = clear;
									break;
								case BlockColor.Yellow:
									color2 = clear;
									break;
								default:
									color2 = ColorUtilities.blockColorArrayRGB[(uint)blockColor];
									break;
								}
							}
						}
						colorBuffer13x[k * 13 + l] = color2;
					}
				}
				gameMapTexture.SetPixels(num, num2, 13, 13, colorBuffer13x);
			}
		}
		gameMapTexture.Apply();
	}
}
