using System;
using System.IO;
using System.Threading;
using Newtonsoft.Json;

public class GameParserJob : ThreadedJob
{
	public delegate void HandleParseComplete(BloxelGame _game);

	public string responseFromServer;

	public bool isRunning;

	public BloxelGame game;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		JsonTextReader reader = new JsonTextReader(new StringReader(responseFromServer));
		game = new BloxelGame(reader);
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(game);
		}
	}
}
