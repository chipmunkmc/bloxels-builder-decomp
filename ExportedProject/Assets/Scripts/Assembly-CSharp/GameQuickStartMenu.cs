using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class GameQuickStartMenu : UIMoveable
{
	public static GameQuickStartMenu instance;

	[Header("UI")]
	public UIButton uiButtonCustomize;

	public UIButton uiButtonPlay;

	[Header("Data")]
	public QuickStartGroup.Type captureType;

	public BloxelBoard templateLevelChoice;

	public BloxelCharacter templateCharacterChoice;

	public BloxelMegaBoard templateBackgroundChoice;

	public ThemeType templateThemeChoice;

	public QuickStartGroup[] quickStartGroups;

	private CoverBoard[] layoutPreviewCovers;

	private CoverBoard[] characterPreviewCovers;

	public Object loadingPrefab;

	private new void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		loadingPrefab = Resources.Load("Prefabs/UIPopupUploading");
		instance = this;
		layoutPreviewCovers = new CoverBoard[quickStartGroups.Length];
		characterPreviewCovers = new CoverBoard[quickStartGroups.Length];
		for (int i = 0; i < layoutPreviewCovers.Length; i++)
		{
			layoutPreviewCovers[i] = new CoverBoard(new CoverStyle(BloxelBoard.baseUIBoardColor, 4, 2, 1, true));
			characterPreviewCovers[i] = new CoverBoard(new CoverStyle(BloxelBoard.baseUIBoardColor));
		}
	}

	private new void Start()
	{
		uiButtonPlay.OnClick += QuickPlay;
		uiButtonCustomize.OnClick += Customize;
		HideButtons();
	}

	private void OnDestroy()
	{
		uiButtonPlay.OnClick -= QuickPlay;
		uiButtonCustomize.OnClick -= Customize;
	}

	private new void Show()
	{
		base.Show();
		HideButtons();
		CheckForReady();
	}

	public void HideButtons()
	{
		uiButtonCustomize.transform.localScale = Vector3.zero;
		uiButtonPlay.transform.localScale = Vector3.zero;
	}

	public Tweener ShowButtons()
	{
		return uiButtonCustomize.transform.DOScale(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiButtonPlay.transform.DOScale(1f, UIAnimationManager.speedMedium);
		});
	}

	private void GenerateGameForQuickPlay(out BloxelGame _game, out BloxelLevel _level)
	{
		BloxelLevel bloxelLevel = new BloxelLevel(templateLevelChoice);
		Dictionary<BlockColor, BloxelProject> dictionary = AssetManager.instance.themes[(int)templateThemeChoice];
		for (int i = 0; i < bloxelLevel.coverBoard.blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < bloxelLevel.coverBoard.blockColors.GetLength(1); j++)
			{
				BlockColor blockColor = bloxelLevel.coverBoard.blockColors[i, j];
				if (blockColor != BlockColor.Blank)
				{
					BloxelProject bloxelProject = dictionary[blockColor];
					if (bloxelProject.type == ProjectType.Animation)
					{
						bloxelLevel.SetAnimationAtLocation(new GridLocation(i, j), bloxelProject as BloxelAnimation, false);
					}
					else
					{
						bloxelLevel.SetBoardAtLocation(new GridLocation(i, j), bloxelProject as BloxelBoard, false);
					}
				}
			}
		}
		BloxelGame bloxelGame = new BloxelGame(bloxelLevel, false);
		bloxelGame.SetHeroAtLocation(templateCharacterChoice, new WorldLocation(bloxelLevel.id, new GridLocation(1, 2), bloxelLevel.location), false);
		bloxelGame.SetBackground(templateBackgroundChoice, false);
		_game = bloxelGame;
		_level = bloxelLevel;
	}

	private void GenerateGameToCustomize(out BloxelGame _game)
	{
		BloxelBoard b = new BloxelBoard(templateLevelChoice.blockColors.Clone() as BlockColor[,], false);
		BloxelLevel bloxelLevel = new BloxelLevel(b);
		Dictionary<BlockColor, BloxelProject> dictionary = AssetManager.instance.themes[(int)templateThemeChoice];
		for (int i = 0; i < bloxelLevel.coverBoard.blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < bloxelLevel.coverBoard.blockColors.GetLength(1); j++)
			{
				BlockColor blockColor = bloxelLevel.coverBoard.blockColors[i, j];
				if (blockColor != BlockColor.Blank)
				{
					BloxelProject bloxelProject = dictionary[blockColor];
					if (bloxelProject.type == ProjectType.Animation)
					{
						bloxelLevel.SetAnimationAtLocation(new GridLocation(i, j), bloxelProject as BloxelAnimation, false);
					}
					else
					{
						bloxelLevel.SetBoardAtLocation(new GridLocation(i, j), bloxelProject as BloxelBoard, false);
					}
				}
			}
		}
		BloxelGame bloxelGame = new BloxelGame(bloxelLevel, false);
		bloxelGame.SetHeroAtLocation(templateCharacterChoice, new WorldLocation(bloxelLevel.id, new GridLocation(1, 2), bloxelLevel.location), false);
		bloxelGame.SetBackground(templateBackgroundChoice, false);
		_game = bloxelGame;
	}

	public void CaptureComplete(BlockColor[,] blockColors)
	{
		int num = 0;
		for (int i = 0; i < blockColors.GetLength(0); i++)
		{
			for (int j = 0; j < blockColors.GetLength(1); j++)
			{
				if (blockColors[i, j] == BlockColor.Blank)
				{
					num++;
				}
			}
		}
		if (num == 169)
		{
			return;
		}
		for (int k = 0; k < quickStartGroups.Length; k++)
		{
			QuickStartGroup.Type templateType = quickStartGroups[k].templateType;
			if (captureType == templateType && templateType == QuickStartGroup.Type.Layout)
			{
				if (blockColors[1, 1] == BlockColor.Blank && blockColors[1, 0] == BlockColor.Blank)
				{
					blockColors[1, 0] = BlockColor.Green;
				}
				templateLevelChoice = new BloxelBoard((BlockColor[,])blockColors.Clone(), false);
				quickStartGroups[k].previewImage.texture = templateLevelChoice.UpdateTexture2D(ref layoutPreviewCovers[k].colors, ref layoutPreviewCovers[k].texture, layoutPreviewCovers[k].style);
				quickStartGroups[k].isReady = true;
			}
			else if (captureType == templateType && templateType == QuickStartGroup.Type.Character)
			{
				BloxelBoard board = new BloxelBoard((BlockColor[,])blockColors.Clone(), false);
				BloxelBoard board2 = new BloxelBoard((BlockColor[,])blockColors.Clone(), false);
				BloxelBoard board3 = new BloxelBoard((BlockColor[,])blockColors.Clone(), false);
				List<BloxelAnimation> list = new List<BloxelAnimation>();
				list.Add(new BloxelAnimation(board));
				list.Add(new BloxelAnimation(board2));
				list.Add(new BloxelAnimation(board3));
				List<BloxelAnimation> anims = list;
				templateCharacterChoice = new BloxelCharacter(anims);
				templateCharacterChoice.coverBoard.blockColors = blockColors;
				quickStartGroups[k].previewImage.texture = templateCharacterChoice.coverBoard.UpdateTexture2D(ref characterPreviewCovers[k].colors, ref characterPreviewCovers[k].texture, characterPreviewCovers[k].style);
				quickStartGroups[k].isReady = true;
			}
		}
		CheckForReady();
	}

	public void CheckForReady()
	{
		bool flag = true;
		for (int i = 0; i < quickStartGroups.Length; i++)
		{
			if (!quickStartGroups[i].isReady)
			{
				flag = false;
				break;
			}
		}
		if (flag)
		{
			ShowButtons();
		}
		else
		{
			HideButtons();
		}
	}

	public void QuickPlay()
	{
		SoundManager.PlayOneShot(SoundManager.instance.editorPlay);
		for (int i = 0; i < quickStartGroups.Length; i++)
		{
			if (!quickStartGroups[i].isReady)
			{
				return;
			}
		}
		BloxelGame _game = null;
		BloxelLevel _level = null;
		GenerateGameForQuickPlay(out _game, out _level);
		GameBuilderCanvas.instance.currentGame = _game;
		GameBuilderCanvas.instance.currentBloxelLevel = _level;
		GameplayBuilder.instance.StartStreaming(_game);
		GameplayBuilder.instance.SetMode(GameplayBuilder.Mode.GameplayStreaming);
		GameBuilderCanvas.instance.currentMode = GameBuilderMode.Test;
		BloxelCamera.instance.selfTransform.position = new Vector3(GameplayController.instance.heroObject.transform.position.x, GameplayController.instance.heroObject.transform.position.y, BloxelCamera.instance.selfTransform.position.z);
	}

	public void Customize()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		for (int i = 0; i < quickStartGroups.Length; i++)
		{
			if (!quickStartGroups[i].isReady)
			{
				return;
			}
		}
		BloxelGame _game = null;
		GenerateGameToCustomize(out _game);
		BloxelGameLibrary.instance.CreateAndSelectFromQuickstart(_game);
	}

	public IEnumerator SaveProject(BloxelProject project, bool deepSave = false)
	{
		yield return new WaitForSeconds(0.2f);
		project.SetBuildVersion(AppStateManager.instance.internalVersion);
		switch (project.type)
		{
		case ProjectType.Board:
			((BloxelBoard)project).Save();
			break;
		case ProjectType.Animation:
			((BloxelAnimation)project).Save(deepSave);
			break;
		case ProjectType.Character:
			((BloxelCharacter)project).Save(deepSave);
			break;
		case ProjectType.MegaBoard:
			((BloxelMegaBoard)project).Save(deepSave);
			break;
		case ProjectType.Level:
			((BloxelLevel)project).Save(deepSave);
			break;
		case ProjectType.Game:
			((BloxelGame)project).Save(deepSave);
			break;
		}
	}
}
