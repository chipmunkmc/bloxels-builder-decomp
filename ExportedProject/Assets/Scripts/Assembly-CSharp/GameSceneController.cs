using UnityEngine;

public class GameSceneController : MonoBehaviour
{
	public static GameSceneController instance;

	public GameHUD hud;

	public GameObject touchControls;

	[Tooltip("Server Game ID")]
	public string gameToLoad = "57a8e8d529468b792977bdf0";

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Start()
	{
		AppStateManager.instance.SetCurrentScene(SceneName.Gameplay);
		GameplayController.instance.gameplayActive = true;
		WorldWrapper.instance.Show();
		GameplayController.instance.currentMode = GameplayController.Mode.Gameplay;
		if (AppStateManager.instance.loadedGame != null)
		{
			GameplayController.instance.iWallGame = ((!(AppStateManager.instance.loadedGame.ID() == AssetManager.demoGameID) && !(AppStateManager.instance.loadedGame.ID() == AssetManager.poultryPanicVersion2) && !(AppStateManager.instance.loadedGame.ID() == AssetManager.poultryPanicID) && !(AppStateManager.instance.loadedGame.ID() == AssetManager.homeMiniGameID)) ? true : false);
			GameplayController.instance.LoadGame(AppStateManager.instance.loadedGame, true, false, true);
		}
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.None);
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		GameplayBuilder.instance.StartStreaming(GameplayController.instance.currentGame);
		GameplayBuilder.instance.InitWorldTileData();
		if (!hud.gameObject.activeInHierarchy)
		{
			hud.transform.parent.gameObject.SetActive(true);
			touchControls.SetActive(true);
		}
		if (BloxelLibrary.instance != null && BloxelLibrary.instance.gameObject != null && BloxelLibrary.instance.gameObject.activeInHierarchy)
		{
			BloxelLibrary.instance.Hide();
		}
	}
}
