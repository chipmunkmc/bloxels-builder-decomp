using UnityEngine;

public class GameplayBackgroundCamera : MonoBehaviour
{
	public MeshRenderer quadRenderer;

	public Material quadMaterial;

	public string backgroundTexturesPath;

	private void Awake()
	{
		Texture2D mainTexture = Resources.Load<Texture2D>(backgroundTexturesPath + "background_games");
		quadMaterial.mainTexture = mainTexture;
		quadRenderer.material = quadMaterial;
		Vector3 localScale = new Vector3(Screen.width, (float)Screen.width * 3f / 4f, 1f);
		quadRenderer.transform.localScale = localScale;
		Camera component = GetComponent<Camera>();
		component.orthographicSize = (float)Screen.height / 2f;
	}
}
