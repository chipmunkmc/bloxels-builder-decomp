using System;
using System.Collections.Generic;
using UnityEngine;
using VoxelEngine;

public class GameplayBuilder : MonoBehaviour
{
	public enum Mode
	{
		GameplayStreaming = 0,
		EditorStreaming = 1
	}

	private Mode currentMode;

	public static GameplayBuilder instance;

	public Transform worldWrapper;

	public BloxelGame game;

	public Dictionary<WorldPos2D, LevelBackground> backgrounds;

	private List<WorldPos2D> backgroundsToLoad;

	public WorldPos2D[] backgroundsToDespawn;

	private short backgroundLoadingFrameCounter = 10;

	public List<TileInfo>[,] worldTileData;

	public Dictionary<WorldPos2D, BloxelTile> tiles;

	private List<TileInfo> tilesToLoad;

	public BloxelTile[] tilesToDespawn;

	private static WorldPos2D[] tilePositions;

	private static WorldPos2D[] tilePositionsGameplay;

	private static WorldPos2D[] tilePositionsEditor;

	private int chunkPosGameplayWidth = 20;

	private int chunkPosGameplayHeight = 18;

	private int chunkPosEditorWidth = 26;

	private int chunkPosEditorHeight = 18;

	private static WorldPos2D[] backgroundPositions;

	public int backgroundPositionsWidth;

	public int backgroundPositionsHeight;

	public int loadBufferSize;

	public int unloadBufferSize;

	public int bkgdLoadBufferSize;

	public int bkgdUnloadBufferSize;

	private int tileUnloadFrameCounter;

	public Transform cameraTransform;

	public bool isStreaming;

	private int unloadOffsetY;

	public float unloadMaxDistanceX { get; private set; }

	public float unloadMaxDistanceY { get; private set; }

	public float unloadBkgdMaxDistX { get; private set; }

	public float unloadBkgdMaxDistY { get; private set; }

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			backgrounds = new Dictionary<WorldPos2D, LevelBackground>(16);
			backgroundPositions = new WorldPos2D[backgroundPositionsWidth * backgroundPositionsHeight];
			for (int i = 0; i < backgroundPositionsHeight; i++)
			{
				for (int j = 0; j < backgroundPositionsWidth; j++)
				{
					backgroundPositions[i * backgroundPositionsHeight + j] = new WorldPos2D(j - Mathf.FloorToInt((float)backgroundPositionsWidth / 2f), i - Mathf.FloorToInt((float)backgroundPositionsHeight / 2f));
				}
			}
			Array.Sort(backgroundPositions, (WorldPos2D s1, WorldPos2D s2) => s1.CompareDistanceToOrigin(s2));
			unloadBkgdMaxDistX = (float)backgroundPositionsWidth / 2f;
			unloadBkgdMaxDistY = unloadBkgdMaxDistX;
			backgroundsToLoad = new List<WorldPos2D>(bkgdLoadBufferSize);
			backgroundsToDespawn = new WorldPos2D[unloadBufferSize];
			tiles = new Dictionary<WorldPos2D, BloxelTile>(128);
			worldTileData = new List<TileInfo>[169, 169];
			for (int k = 0; k < 169; k++)
			{
				for (int l = 0; l < 169; l++)
				{
					worldTileData[k, l] = new List<TileInfo>(4);
				}
			}
			tilesToLoad = new List<TileInfo>(loadBufferSize);
			tilesToDespawn = new BloxelTile[unloadBufferSize];
			float num = (float)Screen.width / (float)Screen.height;
			if (num >= 1.5f)
			{
				chunkPosEditorWidth = 34;
				chunkPosEditorHeight = 18;
			}
			else
			{
				chunkPosEditorWidth = 24;
				chunkPosEditorHeight = 18;
			}
			tilePositionsEditor = new WorldPos2D[chunkPosEditorWidth * chunkPosEditorHeight];
			for (int m = 0; m < chunkPosEditorHeight; m++)
			{
				for (int n = 0; n < chunkPosEditorWidth; n++)
				{
					tilePositionsEditor[m * chunkPosEditorWidth + n] = new WorldPos2D(n - Mathf.FloorToInt((float)chunkPosEditorWidth / 2f), m - Mathf.FloorToInt((float)chunkPosEditorHeight / 2f));
				}
			}
			Array.Sort(tilePositionsEditor, (WorldPos2D s1, WorldPos2D s2) => s1.CompareDistanceToOrigin(s2));
			tilePositionsGameplay = new WorldPos2D[chunkPosGameplayWidth * chunkPosGameplayHeight];
			for (int num2 = 0; num2 < chunkPosGameplayHeight; num2++)
			{
				for (int num3 = 0; num3 < chunkPosGameplayWidth; num3++)
				{
					tilePositionsGameplay[num2 * chunkPosGameplayWidth + num3] = new WorldPos2D(num3 - Mathf.FloorToInt((float)chunkPosGameplayWidth / 2f), num2 - Mathf.FloorToInt((float)chunkPosGameplayHeight / 2f) - 2);
				}
			}
			Array.Sort(tilePositionsGameplay, (WorldPos2D s1, WorldPos2D s2) => s1.CompareDistanceToOrigin(s2));
			currentMode = Mode.EditorStreaming;
			SetMode(Mode.GameplayStreaming);
			base.enabled = false;
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void ResetAnimations()
	{
		if (!isStreaming)
		{
			return;
		}
		Dictionary<WorldPos2D, BloxelTile>.Enumerator enumerator = tiles.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelTile value = enumerator.Current.Value;
			if (value.tileRenderer.projectType == ProjectType.Animation)
			{
				value.tileRenderer.ResetAnimation();
			}
		}
	}

	public void SetMode(Mode mode)
	{
		if (currentMode != mode)
		{
			switch (mode)
			{
			case Mode.EditorStreaming:
				tilePositions = tilePositionsEditor;
				unloadMaxDistanceX = (float)(chunkPosEditorWidth * Chunk2D.chunkSize) / 2f;
				unloadMaxDistanceY = (float)(chunkPosEditorHeight * Chunk2D.chunkSize) / 2f;
				unloadOffsetY = 0;
				break;
			case Mode.GameplayStreaming:
				tilePositions = tilePositionsGameplay;
				unloadMaxDistanceX = (float)(chunkPosGameplayWidth * Chunk2D.chunkSize) / 2f;
				unloadMaxDistanceY = (float)(chunkPosGameplayHeight * Chunk2D.chunkSize) / 2f;
				unloadOffsetY = 2 * Chunk2D.chunkSize;
				break;
			}
			currentMode = mode;
		}
	}

	public void StartStreaming(BloxelGame bloxelGame = null)
	{
		cameraTransform = BloxelCamera.instance.selfTransform;
		if (bloxelGame == null)
		{
			if ((bool)GameBuilderCanvas.instance && GameBuilderCanvas.instance.currentGame != null)
			{
				game = GameBuilderCanvas.instance.currentGame;
			}
			else if ((bool)GameplayController.instance && GameplayController.instance.currentGame != null)
			{
				game = GameplayController.instance.currentGame;
			}
		}
		else
		{
			game = bloxelGame;
		}
		if (!isStreaming)
		{
			backgroundLoadingFrameCounter = 10;
			base.enabled = true;
			isStreaming = true;
		}
	}

	public void UpdateTileInfoForDefaultLevel(BloxelLevel levelToCreate)
	{
		BloxelBoard bloxelBoard = BloxelBoard.DefaultWireframe();
		GridLocation location = levelToCreate.location;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = bloxelBoard.blockColors[j, i];
				GridLocation gridLocation = new GridLocation(location.c * Chunk2D.chunkSize + j, location.r * Chunk2D.chunkSize + i);
				if (blockColor != BlockColor.Blank)
				{
					worldTileData[gridLocation.c, gridLocation.r].Add(new TileInfo(LevelBuilder.SolidColorBoards[(uint)blockColor], levelToCreate, (GameBehavior)blockColor, new GridLocation(j, i), location));
				}
			}
		}
	}

	public void UnloadGameplay(bool shouldDestroyHero = true)
	{
		UnloadAllTiles();
		UnloadAllBackgrounds();
		ClearWorldTileData();
		if (shouldDestroyHero)
		{
			UnityEngine.Object.Destroy(GameplayController.instance.heroObject);
		}
	}

	public void Reset()
	{
		UnloadAllTiles();
		UnloadAllBackgrounds();
		InitWorldTileData();
	}

	public void ResetForCheckPoint()
	{
		UnloadAllTiles(true);
		UnloadAllBackgrounds();
		LoadWorldTileDataFromCheckpoint();
	}

	public void RemoveLevelAtLocation(GridLocation levelLocationInWorld)
	{
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				GridLocation gridLocation = new GridLocation(levelLocationInWorld.c * Chunk2D.chunkSize + j, levelLocationInWorld.r * Chunk2D.chunkSize + i);
				worldTileData[gridLocation.c, gridLocation.r].Clear();
			}
		}
		UnloadAllTilesForLevelAtLocation(levelLocationInWorld, false);
		LevelBackground value = null;
		WorldPos2D key = new WorldPos2D(levelLocationInWorld.c, levelLocationInWorld.r);
		if (backgrounds.TryGetValue(key, out value))
		{
			value.ClearBackgroundTexture();
			backgrounds.Remove(key);
		}
	}

	public void AddLevel(BloxelLevel level)
	{
		GridLocation location = level.location;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = level.coverBoard.blockColors[j, i];
				if (blockColor == BlockColor.Blank)
				{
					continue;
				}
				GridLocation gridLocation = new GridLocation(j, i);
				BloxelProject bloxelProject = level.GetBoardOrAnimationAtLocation(gridLocation);
				if (bloxelProject == null)
				{
					bloxelProject = LevelBuilder.SolidColorBoards[(uint)blockColor];
				}
				TileInfo tileInfo = new TileInfo(bloxelProject, level, (GameBehavior)blockColor, gridLocation, location);
				worldTileData[location.c * Chunk2D.chunkSize + j, location.r * Chunk2D.chunkSize + i].Add(tileInfo);
				if (blockColor == BlockColor.Purple)
				{
					BloxelBrain value = null;
					if (level.brains.TryGetValue(gridLocation, out value))
					{
						GameplayController.gameState[tileInfo.xTileDataIndex, tileInfo.yTileDataIndex].coinBonus = value.coinBonusQuantum;
					}
				}
			}
		}
	}

	public void ResetLevelAtLocation(GridLocation levelLocationInWorld)
	{
		UnloadAllTilesForLevelAtLocation(levelLocationInWorld);
		BloxelLevel value = null;
		if (!game.levels.TryGetValue(levelLocationInWorld, out value))
		{
			return;
		}
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BloxelProject bloxelProject = value.GetProjectModelAtLocation(new GridLocation(j, i));
				BlockColor blockColor = value.coverBoard.blockColors[j, i];
				GridLocation gridLocation = new GridLocation(levelLocationInWorld.c * Chunk2D.chunkSize + j, levelLocationInWorld.r * Chunk2D.chunkSize + i);
				worldTileData[gridLocation.c, gridLocation.r].Clear();
				if (blockColor != BlockColor.Blank)
				{
					if (bloxelProject == null)
					{
						bloxelProject = LevelBuilder.SolidColorBoards[(uint)blockColor];
					}
					worldTileData[gridLocation.c, gridLocation.r].Add(new TileInfo(bloxelProject, value, (GameBehavior)blockColor, new GridLocation(j, i), levelLocationInWorld));
				}
			}
		}
	}

	private void UnloadAllBackgrounds()
	{
		Dictionary<WorldPos2D, LevelBackground>.Enumerator enumerator = backgrounds.GetEnumerator();
		while (enumerator.MoveNext())
		{
			LevelBackground value = enumerator.Current.Value;
			value.Despawn();
		}
		backgrounds.Clear();
	}

	private void UnloadAllTiles(bool checkpoint = false)
	{
		Dictionary<WorldPos2D, BloxelTile>.Enumerator enumerator = tiles.GetEnumerator();
		while (enumerator.MoveNext())
		{
			BloxelTile value = enumerator.Current.Value;
			if (!checkpoint)
			{
				value.tileInfo.shouldRespawn = true;
			}
			value.Despawn();
		}
		tiles.Clear();
		tilesToLoad.Clear();
		Array.Clear(tilesToDespawn, 0, tilesToDespawn.Length);
	}

	private void UnloadAllTilesForLevelAtLocation(GridLocation levelLocationInWorld, bool permanent = true)
	{
		int num = levelLocationInWorld.c * 169;
		int num2 = levelLocationInWorld.r * 169;
		WorldPos2D key = new WorldPos2D(0, 0);
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				key.x = num + i * 13;
				key.y = num2 + j * 13;
				BloxelTile value = null;
				if (tiles.TryGetValue(key, out value))
				{
					UnloadTile(value, true);
				}
			}
		}
	}

	public void InitWorldTileData()
	{
		ClearWorldTileData();
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = game.levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GridLocation key = enumerator.Current.Key;
			BloxelLevel value = enumerator.Current.Value;
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					BlockColor blockColor = value.coverBoard.blockColors[j, i];
					if (blockColor == BlockColor.Blank)
					{
						continue;
					}
					GridLocation gridLocation = new GridLocation(j, i);
					BloxelProject bloxelProject = value.GetBoardOrAnimationAtLocation(gridLocation);
					if (bloxelProject == null)
					{
						bloxelProject = LevelBuilder.SolidColorBoards[(uint)blockColor];
					}
					TileInfo tileInfo = new TileInfo(bloxelProject, value, (GameBehavior)blockColor, gridLocation, key);
					worldTileData[key.c * Chunk2D.chunkSize + j, key.r * Chunk2D.chunkSize + i].Add(tileInfo);
					if (blockColor == BlockColor.Purple)
					{
						BloxelBrain value2 = null;
						if (value.brains.TryGetValue(gridLocation, out value2))
						{
							GameplayController.gameState[tileInfo.xTileDataIndex, tileInfo.yTileDataIndex].coinBonus = value2.coinBonusQuantum;
						}
					}
				}
			}
		}
	}

	public void LoadWorldTileDataFromCheckpoint()
	{
		Dictionary<GridLocation, BloxelLevel>.Enumerator enumerator = game.levels.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GridLocation key = enumerator.Current.Key;
			BloxelLevel value = enumerator.Current.Value;
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					GridLocation gridLocation = new GridLocation(key.c * Chunk2D.chunkSize + j, key.r * Chunk2D.chunkSize + i);
					BlockColor blockColor = value.coverBoard.blockColors[j, i];
					List<TileInfo> list = worldTileData[gridLocation.c, gridLocation.r];
					if (blockColor == BlockColor.Yellow)
					{
						int count = list.Count;
						bool flag = false;
						for (int k = 0; k < count; k++)
						{
							TileInfo tileInfo = list[k];
							if (tileInfo.gameBehavior == GameBehavior.Coin)
							{
								if (tileInfo.shouldRespawn)
								{
									flag = true;
								}
								break;
							}
						}
						if (!flag)
						{
							list.Clear();
							continue;
						}
					}
					list.Clear();
					BloxelProject bloxelProject = value.GetProjectModelAtLocation(new GridLocation(j, i));
					if (blockColor != BlockColor.Blank)
					{
						if (bloxelProject == null)
						{
							bloxelProject = LevelBuilder.SolidColorBoards[(uint)blockColor];
						}
						list.Add(new TileInfo(bloxelProject, value, (GameBehavior)blockColor, new GridLocation(j, i), key));
					}
				}
			}
		}
	}

	private void ClearWorldTileData()
	{
		for (int i = 0; i < 169; i++)
		{
			for (int j = 0; j < 169; j++)
			{
				worldTileData[i, j].Clear();
				GameplayController.gameState[i, j].Clear();
			}
		}
	}

	public void StopStreaming()
	{
		base.enabled = false;
	}

	public void ReplaceLevelAtLocation(BloxelLevel _level)
	{
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				instance.UpdateTileFromWireframe(_level.coverBoard.blockColors[i, j], _level, new GridLocation(i, j), LevelBuilder.SolidColorBoards[(uint)_level.coverBoard.blockColors[i, j]]);
			}
		}
	}

	public void UpdateTileFromWireframe(BlockColor newTileColor, BloxelLevel level, GridLocation locationInLevel, BloxelProject solidBoard)
	{
		int num = level.location.c * 13 + locationInLevel.c;
		int num2 = level.location.r * 13 + locationInLevel.r;
		List<TileInfo> list = worldTileData[num, num2];
		if (newTileColor == BlockColor.Blank)
		{
			list.Clear();
			BloxelTile value = null;
			if (tiles.TryGetValue(new WorldPos2D(num * 13, num2 * 13), out value))
			{
				value.tileInfo.isLoaded = true;
				UnloadTile(value);
			}
			return;
		}
		TileInfo tileInfo = null;
		if (list.Count > 0)
		{
			tileInfo = list[0];
			if (tileInfo != null)
			{
				if (tileInfo.gameBehavior == (GameBehavior)newTileColor)
				{
					return;
				}
				tileInfo.Initialize(solidBoard, level, (GameBehavior)newTileColor, locationInLevel, level.location);
				list[0] = tileInfo;
			}
		}
		else
		{
			tileInfo = new TileInfo(solidBoard, level, (GameBehavior)newTileColor, locationInLevel, level.location);
			list.Add(tileInfo);
		}
		BloxelTile value2 = null;
		if (tiles.TryGetValue(tileInfo.worldLocation, out value2))
		{
			value2.tileInfo.isLoaded = true;
			UnloadTile(value2);
		}
	}

	public void UpdateTileAtLocation(GridLocation locationInBoard, BloxelProject project = null, BloxelLevel _level = null)
	{
		BloxelTile value = null;
		if (_level == null)
		{
			_level = GameBuilderCanvas.instance.currentBloxelLevel;
		}
		WorldPos2D key = new WorldPos2D(_level.location.c * 169 + locationInBoard.c * 13, _level.location.r * 169 + locationInBoard.r * 13);
		tiles.TryGetValue(key, out value);
		if (value != null)
		{
			if (project != null)
			{
				worldTileData[value.tileInfo.levelLocationInWorld.c * 13 + locationInBoard.c, value.tileInfo.levelLocationInWorld.r * 13 + locationInBoard.r][0].data = project;
			}
			UnloadTile(value);
		}
	}

	public void UpdateTileAtLocation(GridLocation locationInBoard, BloxelLevel _level)
	{
		UpdateTileAtLocation(locationInBoard, null, _level);
	}

	public void UpdateTilesInLevel(BloxelLevel _level)
	{
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				if (_level.coverBoard.blockColors[i, j] != BlockColor.Blank)
				{
					instance.UpdateTileAtLocation(new GridLocation(i, j), _level);
				}
			}
		}
	}

	private void Update()
	{
		FindBackgroundsToLoad();
		LoadAndRenderBackgrounds();
		if (!UnloadTilesAndBackgrounds())
		{
			FindTilesToLoad();
			LoadAndRenderTiles();
		}
	}

	public void LoadLevelAtLocation(GridLocation levelLocationInWorld)
	{
		BloxelLevel value = null;
		if (!game.levels.TryGetValue(levelLocationInWorld, out value))
		{
			return;
		}
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BloxelProject projectModelAtLocation = value.GetProjectModelAtLocation(new GridLocation(j, i));
				BlockColor blockColor = value.coverBoard.blockColors[j, i];
				if (blockColor == BlockColor.Blank)
				{
					continue;
				}
				WorldPos2D key = new WorldPos2D(levelLocationInWorld.c * 169 + j * 13, levelLocationInWorld.r * 169 + i * 13);
				BloxelTile value2 = null;
				if (!tiles.TryGetValue(key, out value2))
				{
					List<TileInfo> list = worldTileData[levelLocationInWorld.c * 13 + j, levelLocationInWorld.r + i];
					if (list != null && list.Count != 0)
					{
						LoadTile(list[0]);
					}
				}
			}
		}
	}

	private void FindTilesToLoad()
	{
		Vector3 vector = worldWrapper.InverseTransformPoint(cameraTransform.position);
		WorldPos2D worldPos2D = new WorldPos2D(Mathf.FloorToInt(vector.x / (float)Chunk2D.chunkSize), Mathf.FloorToInt(vector.y / (float)Chunk2D.chunkSize));
		if (tilesToLoad.Count != 0)
		{
			return;
		}
		for (int i = 0; i < tilePositions.Length; i++)
		{
			WorldPos2D worldPos2D2 = new WorldPos2D(Mathf.Clamp(tilePositions[i].x + worldPos2D.x, 0, 168), Mathf.Clamp(tilePositions[i].y + worldPos2D.y, 0, 168));
			List<TileInfo> list = worldTileData[worldPos2D2.x, worldPos2D2.y];
			for (int j = 0; j < list.Count; j++)
			{
				TileInfo tileInfo = list[j];
				if (tileInfo != null && tileInfo.shouldRespawn && !tileInfo.queuedForLoading && !tileInfo.isLoaded)
				{
					tileInfo.queuedForLoading = true;
					tilesToLoad.Add(tileInfo);
					if (tilesToLoad.Count >= loadBufferSize)
					{
						return;
					}
				}
			}
		}
	}

	private void LoadAndRenderTiles()
	{
		for (int i = 0; i < loadBufferSize; i++)
		{
			if (tilesToLoad.Count == 0)
			{
				break;
			}
			LoadTile(tilesToLoad[0]);
			tilesToLoad.RemoveAt(0);
		}
	}

	private bool UnloadTilesAndBackgrounds()
	{
		if (tileUnloadFrameCounter++ < 5)
		{
			return false;
		}
		tileUnloadFrameCounter = 0;
		Vector3 vector = worldWrapper.InverseTransformPoint(cameraTransform.position);
		int num = 0;
		Dictionary<WorldPos2D, BloxelTile>.Enumerator enumerator = tiles.GetEnumerator();
		while (enumerator.MoveNext() && num != unloadBufferSize)
		{
			BloxelTile value = enumerator.Current.Value;
			if ((float)(Mathf.FloorToInt(Mathf.Abs((float)value.tileInfo.worldLocation.x - vector.x) / (float)Chunk2D.chunkSize) * Chunk2D.chunkSize) > unloadMaxDistanceX || (float)(Mathf.FloorToInt(Mathf.Abs((float)value.tileInfo.worldLocation.y - (vector.y - (float)unloadOffsetY)) / (float)Chunk2D.chunkSize) * Chunk2D.chunkSize) > unloadMaxDistanceY)
			{
				tilesToDespawn[num++] = value;
			}
		}
		for (int i = 0; i < num; i++)
		{
			BloxelTile bloxelTile = tilesToDespawn[i];
			if (bloxelTile.tileInfo.gameBehavior != GameBehavior.Enemy || ((BloxelEnemyTile)bloxelTile).type == EnemyType.Stationary)
			{
				UnloadTile(bloxelTile);
				continue;
			}
			Vector3 localPositionWhenUnloaded = ((!(bloxelTile is BloxelEnemyTile)) ? bloxelTile.selfTransform.localPosition : (bloxelTile.selfTransform.localPosition + new Vector3(0f, 1f, 0f)));
			int num2 = Mathf.Clamp(Mathf.FloorToInt(localPositionWhenUnloaded.x / (float)Chunk2D.chunkSize), 0, 168);
			int num3 = Mathf.Clamp(Mathf.FloorToInt(localPositionWhenUnloaded.y / (float)Chunk2D.chunkSize), 0, 168);
			WorldPos2D worldPos2D = new WorldPos2D(num2 * Chunk2D.chunkSize, num3 * Chunk2D.chunkSize);
			WorldPos2D worldPos2D2 = new WorldPos2D(worldPos2D.x, worldPos2D.y);
			tiles.Remove(bloxelTile.tileInfo.worldLocation);
			for (int j = 0; j < 100; j++)
			{
				if (!tiles.ContainsKey(worldPos2D2))
				{
					break;
				}
				worldPos2D2.x = UnityEngine.Random.Range(worldPos2D.x, worldPos2D.x + Chunk2D.chunkSize);
				worldPos2D2.y = UnityEngine.Random.Range(worldPos2D.y, worldPos2D.y + Chunk2D.chunkSize);
			}
			worldTileData[bloxelTile.tileInfo.xTileDataIndex, bloxelTile.tileInfo.yTileDataIndex].Remove(bloxelTile.tileInfo);
			bloxelTile.tileInfo.localPositionWhenUnloaded = localPositionWhenUnloaded;
			bloxelTile.tileInfo.worldLocation = worldPos2D2;
			bloxelTile.tileInfo.xTileDataIndex = num2;
			bloxelTile.tileInfo.yTileDataIndex = num3;
			if (!worldTileData[num2, num3].Contains(bloxelTile.tileInfo))
			{
				worldTileData[num2, num3].Add(bloxelTile.tileInfo);
			}
			tiles.Add(worldPos2D2, bloxelTile);
			if (Mathf.Abs(localPositionWhenUnloaded.x - vector.x) > unloadMaxDistanceX || Mathf.Abs(localPositionWhenUnloaded.y - vector.y) > unloadMaxDistanceY)
			{
				UnloadTile(bloxelTile);
			}
		}
		int num4 = 0;
		Dictionary<WorldPos2D, LevelBackground>.Enumerator enumerator2 = backgrounds.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			LevelBackground value2 = enumerator2.Current.Value;
			WorldPos2D key = enumerator2.Current.Key;
			if ((float)Mathf.Abs(key.x - Mathf.FloorToInt(vector.x / 169f)) > unloadBkgdMaxDistX || (float)Mathf.Abs(key.y - Mathf.FloorToInt(vector.y / 169f)) > unloadBkgdMaxDistY)
			{
				backgroundsToDespawn[num4++] = key;
				value2.Despawn();
			}
		}
		for (int k = 0; k < num4; k++)
		{
			backgrounds.Remove(backgroundsToDespawn[k]);
		}
		return num > 0 || num4 > 0;
	}

	public void UnloadTile(BloxelTile tile, bool permanent = false)
	{
		tiles.Remove(tile.tileInfo.worldLocation);
		if (tile.tileInfo.isLoaded)
		{
			tile.tileInfo.isLoaded = false;
			tile.tileInfo.shouldRespawn = !permanent;
			tile.Despawn();
		}
	}

	private void LoadTile(TileInfo tileInfo)
	{
		if (tileInfo == null)
		{
			return;
		}
		tileInfo.queuedForLoading = false;
		BloxelTile bloxelTile = null;
		if (!tiles.ContainsKey(tileInfo.worldLocation))
		{
			switch (tileInfo.gameBehavior)
			{
			case GameBehavior.Coin:
				bloxelTile = TilePool.instance.SpawnCoinTile();
				break;
			case GameBehavior.Destructible:
				bloxelTile = TilePool.instance.SpawnDestructibleTile();
				break;
			case GameBehavior.Enemy:
				bloxelTile = TilePool.instance.SpawnEnemyTile();
				break;
			case GameBehavior.Hazard:
				bloxelTile = TilePool.instance.SpawnHazardTile();
				break;
			case GameBehavior.NPC:
				bloxelTile = TilePool.instance.SpawnNPCTile();
				break;
			case GameBehavior.Platform:
				bloxelTile = TilePool.instance.SpawnPlatformTile();
				break;
			case GameBehavior.PowerUp:
				bloxelTile = TilePool.instance.SpawnPowerUpTile();
				break;
			case GameBehavior.Water:
				bloxelTile = TilePool.instance.SpawnWaterTile();
				break;
			}
			bloxelTile.InitWithNewTileInfo(tileInfo);
			tiles[tileInfo.worldLocation] = bloxelTile;
			tileInfo.isLoaded = true;
		}
	}

	private void FindBackgroundsToLoad()
	{
		if (backgroundLoadingFrameCounter++ < 10)
		{
			return;
		}
		backgroundLoadingFrameCounter = 0;
		Vector3 vector = worldWrapper.InverseTransformPoint(cameraTransform.position);
		WorldPos2D worldPos2D = new WorldPos2D(Mathf.FloorToInt(vector.x / 169f), Mathf.FloorToInt(vector.y / 169f));
		if (backgroundsToLoad.Count != 0)
		{
			return;
		}
		for (int i = 0; i < backgroundPositions.Length; i++)
		{
			WorldPos2D worldPos2D2 = new WorldPos2D(Mathf.Clamp(backgroundPositions[i].x + worldPos2D.x, 0, 12), Mathf.Clamp(backgroundPositions[i].y + worldPos2D.y, 0, 12));
			BloxelLevel value = null;
			game.levels.TryGetValue(new GridLocation(worldPos2D2.x, worldPos2D2.y), out value);
			if (value != null && !backgroundsToLoad.Contains(worldPos2D2) && !backgrounds.ContainsKey(worldPos2D2))
			{
				backgroundsToLoad.Add(worldPos2D2);
				if (backgroundsToLoad.Count >= bkgdLoadBufferSize)
				{
					break;
				}
			}
		}
	}

	private void LoadAndRenderBackgrounds()
	{
		for (int i = 0; i < bkgdLoadBufferSize; i++)
		{
			if (backgroundsToLoad.Count == 0)
			{
				break;
			}
			LoadBackground(backgroundsToLoad[0]);
			backgroundsToLoad.RemoveAt(0);
		}
	}

	private void LoadBackground(WorldPos2D levelLocation)
	{
		BloxelLevel level = game.levels[new GridLocation(levelLocation.x, levelLocation.y)];
		LevelBackground levelBackground = LevelBackgroundPool.instance.Spawn();
		levelBackground.InitForLevel(level);
		levelBackground.selfTransform.SetParent(worldWrapper);
		levelBackground.selfTransform.localPosition = new Vector3(((float)levelLocation.x + 0.5f) * 169f - 0.5f, ((float)levelLocation.y + 0.5f) * 169f - 0.5f, 6.5f);
		backgrounds[levelLocation] = levelBackground;
	}
}
