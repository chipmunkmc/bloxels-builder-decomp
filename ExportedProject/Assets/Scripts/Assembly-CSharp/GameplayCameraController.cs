using System;
using DG.Tweening;
using UnityEngine;

[Serializable]
public class GameplayCameraController : BloxelCameraController
{
	private Vector3 lastKnownTargetPosition;

	private Vector3 _smoothDampVelocity;

	private float smoothDampTime = 0.2f;

	private Vector3 standardOffset;

	private Vector3 shrunkenOffset = new Vector3(0f, 1f, -165f);

	public GameplayCameraController()
	{
	}

	public GameplayCameraController(Transform cameraTransform, Camera cam, Vector3 cameraOffset)
	{
		base.cameraTransform = cameraTransform;
		base.cam = cam;
		base.cameraOffset = cameraOffset;
		previousCameraOffset = cameraOffset;
		standardOffset = cameraOffset;
	}

	public void SetToShrunken(bool immediate = false)
	{
		float fieldOfView = cam.fieldOfView;
		float fieldOfView2 = 6f;
		float derpConstant = Mathf.Abs(cam.transform.position.z - LevelBuilder.instance.gameBackground.transform.position.z) * 2f * Mathf.Tan(cam.fieldOfView * 0.5f * ((float)Math.PI / 180f)) / LevelBuilder.instance.gameBackground.transform.localScale.y;
		cameraOffset = shrunkenOffset;
		LevelBuilder.instance.gameBackground.transform.DOKill();
		if (immediate)
		{
			LevelBuilder.instance.gameBackground.transform.localScale = new Vector3(105f, 105f, 1f);
			cam.fieldOfView = fieldOfView2;
			return;
		}
		LevelBuilder.instance.gameBackground.transform.DOScale(new Vector3(105f, 105f, 1f), UIAnimationManager.playerShrinkSpeed).OnUpdate(delegate
		{
			float num = Mathf.Abs(cam.transform.position.z - LevelBuilder.instance.gameBackground.transform.position.z);
			cam.fieldOfView = Mathf.Clamp(2f * Mathf.Atan(LevelBuilder.instance.gameBackground.transform.localScale.y * derpConstant * 0.5f / num) * 57.29578f, 0f, 60f);
		}).SetUpdate(false)
			.SetEase(Ease.OutCubic);
	}

	public void SetToStandard(bool immediate = false)
	{
		float fieldOfView = cam.fieldOfView;
		float fieldOfView2 = 60f;
		float derpConstant = Mathf.Abs(cam.transform.position.z - LevelBuilder.instance.gameBackground.transform.position.z) * 2f * Mathf.Tan(cam.fieldOfView * 0.5f * ((float)Math.PI / 180f)) / LevelBuilder.instance.gameBackground.transform.localScale.y;
		cameraOffset = standardOffset;
		LevelBuilder.instance.gameBackground.transform.DOKill();
		if (immediate)
		{
			LevelBuilder.instance.gameBackground.transform.localScale = new Vector3(1050f, 1050f, 1f);
			cam.fieldOfView = fieldOfView2;
			return;
		}
		LevelBuilder.instance.gameBackground.transform.DOScale(new Vector3(1050f, 1050f, 1f), UIAnimationManager.playerShrinkSpeed).OnUpdate(delegate
		{
			float num = Mathf.Abs(cam.transform.position.z - LevelBuilder.instance.gameBackground.transform.position.z);
			cam.fieldOfView = Mathf.Clamp(2f * Mathf.Atan(LevelBuilder.instance.gameBackground.transform.localScale.y * derpConstant * 0.5f / num) * 57.29578f, 0f, 60f);
		}).SetUpdate(false)
			.SetEase(Ease.OutCubic);
	}

	public override void ApplyCameraProperties()
	{
		cam.orthographic = false;
		AdjustBackgroundProperties();
	}

	private void AdjustBackgroundProperties()
	{
		LevelBuilder.instance.gameBackground.transform.rotation = Quaternion.identity;
		LevelBuilder.instance.gameBackground.transform.localScale = new Vector3(cam.farClipPlane * 1.05f, cam.farClipPlane * 1.05f, 1f);
	}

	public override void UpdateCameraPosition()
	{
		target = BloxelCamera.instance.target;
		Vector3 vector = cameraOffset;
		float smoothTime = smoothDampTime;
		if (target != null)
		{
			if (target.GetComponent<PixelPlayerController>() != null && !(PixelPlayerController.instance.velocity.x < -0.1f) && !(PixelPlayerController.instance.velocity.y < -0.1f) && PixelPlayerController.instance.velocity.x < 0.1f && PixelPlayerController.instance.velocity.y < 0.1f)
			{
				smoothTime = 0.4f;
			}
			Transform transform = target.transform;
			lastKnownTargetPosition = transform.position;
			cameraTransform.position = Vector3.SmoothDamp(cameraTransform.position, transform.position + vector, ref _smoothDampVelocity, smoothTime);
			AdjustBackgroundPosition();
		}
		else
		{
			cameraTransform.position = Vector3.SmoothDamp(cameraTransform.position, lastKnownTargetPosition + vector, ref _smoothDampVelocity, smoothTime);
		}
	}

	protected virtual void AdjustBackgroundPosition()
	{
		float z = cam.farClipPlane / 2f - 0.01f;
		Vector3 backgroundOffset = GetBackgroundOffset();
		LevelBuilder.instance.gameBackground.transform.position = cam.transform.TransformPoint(new Vector3(0f, 0f, z));
		LevelBuilder.instance.gameBackground.transform.position = cam.transform.TransformPoint(new Vector3(backgroundOffset.x, backgroundOffset.y, z));
	}

	protected virtual Vector3 GetBackgroundOffset()
	{
		float num = LevelBuilder.instance.gameBackground.transform.localScale.y / 1050f;
		Rect worldBoundingRect = LevelBuilder.instance.game.worldBoundingRect;
		Vector3 position = cameraTransform.position;
		Vector2 vector = new Vector2(worldBoundingRect.center.x - position.x, worldBoundingRect.center.y - position.y);
		Vector2 vector2 = new Vector2(worldBoundingRect.width / 2f, worldBoundingRect.height / 2f);
		float num2 = Mathf.Tan((float)Math.PI / 6f) * (cam.farClipPlane / 2f - 0.01f) * 2f * cam.aspect;
		float num3 = 1050f;
		float num4 = (num3 - num2) / 2f;
		float num5 = vector.x / vector2.x * num4 / 1.5f;
		float num6 = vector.y / vector2.y * num4 / 4f;
		return new Vector3(num5 * num, num6 * num, 0f);
	}

	public void SetupCameraForTargetPosition(Vector3 positionInWorldWrapper)
	{
		cameraTransform.position = positionInWorldWrapper + cameraOffset;
	}
}
