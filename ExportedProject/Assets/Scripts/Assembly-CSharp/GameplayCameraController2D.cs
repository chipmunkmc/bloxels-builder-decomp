using UnityEngine;

public class GameplayCameraController2D : GameplayCameraController
{
	private Vector3 _smoothDampVelocity;

	private float smoothDampTime = 0.2f;

	public GameplayCameraController2D(Transform cameraTransform, Camera cam, Vector3 cameraOffset)
	{
		base.cameraTransform = cameraTransform;
		base.cam = cam;
		base.cameraOffset = cameraOffset;
	}

	public override void ApplyCameraProperties()
	{
		cam.orthographic = true;
		cam.orthographicSize = 50f;
		cameraTransform.localRotation = Quaternion.identity;
		AdjustBackgroundProperties();
	}

	private void AdjustBackgroundProperties()
	{
		LevelBuilder.instance.gameBackground.transform.rotation = Quaternion.identity;
		float num = cam.orthographicSize * cam.aspect;
		LevelBuilder.instance.gameBackground.transform.localScale = new Vector3(num * 2f * 1.25f, num * 2f * 1.25f, 1f);
	}

	public override void UpdateCameraPosition()
	{
		target = BloxelCamera.instance.target;
		Vector3 vector = cameraOffset;
		float smoothTime = smoothDampTime;
		if (target != null)
		{
			if (target.GetComponent<PixelPlayerController>() != null && !(PixelPlayerController.instance.velocity.x < -0.1f) && !(PixelPlayerController.instance.velocity.y < -0.1f) && PixelPlayerController.instance.velocity.x < 0.1f && PixelPlayerController.instance.velocity.y < 0.1f)
			{
				smoothTime = 0.4f;
			}
			Transform transform = target.transform;
			cameraTransform.position = Vector3.SmoothDamp(cameraTransform.position, transform.position + vector, ref _smoothDampVelocity, smoothTime);
			AdjustBackgroundPosition();
		}
	}

	protected override void AdjustBackgroundPosition()
	{
		LevelBuilder.instance.gameBackground.transform.position = cam.transform.TransformPoint(new Vector3(0f, 0f, 500f));
		Vector3 backgroundOffset = GetBackgroundOffset();
		LevelBuilder.instance.gameBackground.transform.position = cam.transform.TransformPoint(new Vector3(backgroundOffset.x, backgroundOffset.y, 500f));
	}

	protected override Vector3 GetBackgroundOffset()
	{
		Rect worldBoundingRect = LevelBuilder.instance.game.worldBoundingRect;
		Vector3 localPosition = PixelPlayerController.instance.transform.localPosition;
		Vector2 vector = new Vector2(worldBoundingRect.center.x - localPosition.x, worldBoundingRect.center.y - localPosition.y);
		Vector2 vector2 = new Vector2(worldBoundingRect.width / 2f, worldBoundingRect.height / 2f);
		float num = cam.orthographicSize * cam.aspect * 2f;
		float x = LevelBuilder.instance.gameBackground.transform.localScale.x;
		float num2 = (x - num) / 4f;
		float x2 = vector.x / vector2.x * num2;
		float y = vector.y / vector2.y * num2 / 4f;
		return new Vector3(x2, y, 0f);
	}
}
