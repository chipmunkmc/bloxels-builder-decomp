using System;
using UnityEngine;

public class GameplayCaptureCameraController : BloxelCameraController
{
	private Vector3 lastKnownTargetPosition;

	private Vector3 _smoothDampVelocity;

	private float smoothDampTime = 0.2f;

	public GameplayCaptureCameraController()
	{
	}

	public GameplayCaptureCameraController(Transform cameraTransform, Camera cam, Vector3 cameraOffset)
	{
		base.cameraTransform = cameraTransform;
		base.cam = cam;
		base.cameraOffset = cameraOffset;
	}

	public override void ApplyCameraProperties()
	{
		cam.orthographic = false;
		AdjustBackgroundProperties();
	}

	private void AdjustBackgroundProperties()
	{
		LevelBuilder.instance.gameBackground.transform.rotation = Quaternion.identity;
		LevelBuilder.instance.gameBackground.transform.localScale = new Vector3(cam.farClipPlane * 1.05f, cam.farClipPlane * 1.05f, 1f);
	}

	public override void UpdateCameraPosition()
	{
		Vector3 vector = cameraOffset;
		float smoothTime = smoothDampTime;
		lastKnownTargetPosition = cameraTargetPosition;
		cameraTransform.position = Vector3.SmoothDamp(cameraTransform.position, cameraTargetPosition + vector, ref _smoothDampVelocity, smoothTime);
		AdjustBackgroundPosition();
	}

	protected virtual void AdjustBackgroundPosition()
	{
		float z = cam.farClipPlane / 2f - 0.01f;
		Vector3 backgroundOffset = GetBackgroundOffset();
		LevelBuilder.instance.gameBackground.transform.position = cam.transform.TransformPoint(new Vector3(0f, 0f, z));
		LevelBuilder.instance.gameBackground.transform.position = cam.transform.TransformPoint(new Vector3(backgroundOffset.x, backgroundOffset.y, z));
	}

	protected virtual Vector3 GetBackgroundOffset()
	{
		Rect worldBoundingRect = LevelBuilder.instance.game.worldBoundingRect;
		Vector3 position = cam.transform.position;
		Vector2 vector = new Vector2(worldBoundingRect.center.x - position.x, worldBoundingRect.center.y - position.y);
		Vector2 vector2 = new Vector2(worldBoundingRect.width / 2f, worldBoundingRect.height / 2f);
		float num = Mathf.Tan(cam.fov * ((float)Math.PI / 180f) * 0.5f) * (cam.farClipPlane / 2f - 0.01f) * 2f * cam.aspect;
		float x = LevelBuilder.instance.gameBackground.transform.localScale.x;
		float num2 = (x - num) / 2f;
		float x2 = vector.x / vector2.x * num2 / 1.5f;
		float y = vector.y / vector2.y * num2 / 4f;
		return new Vector3(x2, y, 0f);
	}

	public void SetupCameraForTargetPosition(Vector3 positionInWorldWrapper)
	{
		cameraTransform.position = positionInWorldWrapper + cameraOffset;
	}
}
