using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(InventoryController))]
public class GameplayController : MonoBehaviour
{
	public struct GameStateInfo
	{
		public int stolenCoins;

		public int coinBonus;

		public GameStateInfo(int stolenCoins, int coinBonus)
		{
			this.stolenCoins = stolenCoins;
			this.coinBonus = coinBonus;
		}

		public void Clear()
		{
			stolenCoins = 0;
			coinBonus = 0;
		}

		public int HandleEnemyDeathAndCalculateCoinDrop()
		{
			int result = stolenCoins + coinBonus;
			Clear();
			return result;
		}
	}

	public struct EnemyToKill
	{
		public BloxelEnemyTile enemy;

		public float expirationTime;

		public EnemyToKill(BloxelEnemyTile enemy, float expirationTime)
		{
			this.enemy = enemy;
			this.expirationTime = expirationTime;
		}
	}

	public enum Mode
	{
		Test = 0,
		Gameplay = 1
	}

	public delegate void GameStart();

	public delegate void HandleReset();

	public static GameStateInfo[,] gameState = new GameStateInfo[169, 169];

	public static Queue<EnemyToKill> EnemiesToKill = new Queue<EnemyToKill>(16);

	public InventoryController inventoryManager;

	public GameObject checkPointPrefab;

	public static GameplayController instance;

	public Mode currentMode;

	public BloxelGame currentGame;

	public GameHUD hud;

	public CanvasGroup uiCanvasGroupHUD;

	public PPInputController inputController;

	public GameObject heroObject;

	public bool fromServer;

	public bool iWallGame;

	public bool inWater;

	private float waterFullAlpha = 0.75f;

	private float waterSemiTransparentAlpha = 0.35f;

	private string waterMaterialPath = "3DMaterials/Water";

	private bool _gameplayActive;

	public Vector3 lastCheckpoint;

	public Vector3 startingPosition;

	public UnityEngine.Object popupMessagePrefab;

	public UnityEngine.Object gameWinPrefab;

	public UnityEngine.Object gameOverPrefab;

	private static ExplosionFlash _explosionFlashPrefab;

	private static EnemyHealthIndicator _enemyHealthPrefab;

	[Header("Powerup Indicator Sprites")]
	public Sprite bombSprite;

	public Sprite healthSprite;

	public Sprite jetPackSprite;

	public Sprite shrinkSprite;

	public Sprite mapSprite;

	public Sprite invincibilitySprite;

	public static Dictionary<PowerUpType, Sprite> powerupSprites;

	public Material waterMat { get; private set; }

	public bool gameplayActive
	{
		get
		{
			return _gameplayActive;
		}
		set
		{
			_gameplayActive = value;
		}
	}

	public event GameStart onGameStart;

	public event HandleReset OnReset;

	private void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		inventoryManager = GetComponent<InventoryController>();
		Dictionary<PowerUpType, Sprite> dictionary = new Dictionary<PowerUpType, Sprite>();
		dictionary.Add(PowerUpType.Bomb, bombSprite);
		dictionary.Add(PowerUpType.Health, healthSprite);
		dictionary.Add(PowerUpType.Jetpack, jetPackSprite);
		dictionary.Add(PowerUpType.Shrink, shrinkSprite);
		dictionary.Add(PowerUpType.Map, mapSprite);
		dictionary.Add(PowerUpType.Invincibility, invincibilitySprite);
		powerupSprites = dictionary;
		popupMessagePrefab = Resources.Load("Prefabs/UIPopupMessage");
		gameWinPrefab = Resources.Load("Prefabs/UIPopupGameWin");
		gameOverPrefab = Resources.Load("Prefabs/UIPopupGameOver");
		waterMat = Resources.Load<Material>(waterMaterialPath);
		_explosionFlashPrefab = Resources.Load<GameObject>("Prefabs/Gameplay/Explosion").GetComponent<ExplosionFlash>();
		_enemyHealthPrefab = Resources.Load<GameObject>("Prefabs/Gameplay/EnemyHealthIndicator").GetComponent<EnemyHealthIndicator>();
		ExplosionController.InitRandomForceAndTorqueLookups();
	}

	private void Update()
	{
		int num = EnemiesToKill.Count;
		if (num == 0)
		{
			return;
		}
		float time = Time.time;
		EnemyToKill enemyToKill = EnemiesToKill.Peek();
		while (enemyToKill.expirationTime < time)
		{
			enemyToKill.enemy.Kill(null, 9.25f);
			EnemiesToKill.Dequeue();
			if (--num == 0)
			{
				break;
			}
			enemyToKill = EnemiesToKill.Peek();
		}
	}

	public static ExplosionFlash SpawnExplosionFlash()
	{
		return _explosionFlashPrefab.Spawn(WorldWrapper.instance.runtimeObjectContainer);
	}

	public static EnemyHealthIndicator SpawnEnemyHealthIndicator()
	{
		return _enemyHealthPrefab.Spawn(WorldWrapper.instance.runtimeObjectContainer);
	}

	private void OnDisable()
	{
		if (heroObject != null)
		{
			heroObject.GetComponent<PixelPlayerController>().OnDeath -= ShowGameLoss;
		}
	}

	public void ShowTouchControls()
	{
		PPInputController.instance.canvasUIBase1024Object.SetActive(true);
	}

	public void HideTouchControls()
	{
		PPInputController.instance.canvasUIBase1024Object.SetActive(false);
	}

	public void LoadGame(BloxelGame game, bool shouldInitCheckpoint = true, bool isStoryGame = false, bool _fromServer = false)
	{
		fromServer = _fromServer;
		gameplayActive = true;
		currentGame = game;
		SoundManager.instance.PlayMusic(SoundManager.instance.musicChoiceLookup[currentGame.musicChoice]);
		LevelBuilder.instance.SetupGameBackground(currentGame);
		if ((bool)BloxelLibrary.instance && BloxelLibrary.instance.isVisible)
		{
			BloxelLibrary.instance.Hide();
		}
		if (heroObject == null && currentGame.hero != null)
		{
			heroObject = LevelBuilder.instance.CreateAndPlaceCharacter(currentGame, fromServer);
			if (isStoryGame)
			{
				heroObject.transform.localPosition = new Vector3(1108f, 1026.5f, heroObject.transform.localPosition.z);
			}
			StartDroppingCharacter();
			heroObject.GetComponent<PixelPlayerController>().OnDeath += ShowGameLoss;
			if (shouldInitCheckpoint)
			{
				startingPosition = heroObject.transform.localPosition;
				lastCheckpoint = heroObject.transform.localPosition;
			}
			PlayerMovementController component = heroObject.GetComponent<PlayerMovementController>();
			component.jumpHeight = currentGame.hero.characterJumpHeight;
			component.groundDamping = currentGame.hero.characterInertia;
			component.doubleJump = currentGame.hero.doubleJump;
			component.enabled = false;
			inventoryManager.ResetInventory();
			component.Reset();
		}
		if (this.onGameStart != null)
		{
			this.onGameStart();
		}
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
	}

	public void Replay()
	{
		WorldWrapper.instance.ClearRuntimeContainer();
		GameplayBuilder.instance.Reset();
		if (inventoryManager != null)
		{
			inventoryManager.ResetInventory();
		}
		if (this.OnReset != null)
		{
			this.OnReset();
		}
	}

	public void ReplayFromCheckpiont()
	{
		WorldWrapper.instance.ClearRuntimeContainer();
		GameplayBuilder.instance.ResetForCheckPoint();
		if (this.OnReset != null)
		{
			this.OnReset();
		}
	}

	public void StartDroppingCharacter()
	{
		CancelDroppingCharacter();
		StartCoroutine("DropCharacter");
	}

	public void CancelDroppingCharacter()
	{
		StopCoroutine("DropCharacter");
	}

	public IEnumerator DropCharacter()
	{
		instance.PlayerExitedWater();
		BloxelCamera.instance.target = PixelPlayerController.instance.gameObject;
		heroObject.GetComponent<PlayerMovementController>().enabled = false;
		Camera cam = BloxelCamera.instance.mainCamera;
		float elapsedTime = 0f;
		float maxWaitTime = 10f;
		PixelPlayerController.instance.PrepareToDrop();
		while (true)
		{
			if (elapsedTime >= maxWaitTime)
			{
				yield return false;
			}
			Vector3 heroPosition = heroObject.transform.position;
			Vector3 cameraPosition = cam.transform.position;
			float heroCamDistZ = heroPosition.z - cameraPosition.z;
			float h = Mathf.Tan(cam.fov * ((float)Math.PI / 180f) * 0.5f) * heroCamDistZ * 2f;
			Bounds cameraBounds = new Bounds(size: new Vector3(h * cam.aspect, h, 0f), center: new Vector3(cameraPosition.x, cameraPosition.y, heroPosition.z));
			Bounds heroBounds = new Bounds(heroObject.transform.position, Vector3.one * 13f);
			if (!cameraBounds.Intersects(heroBounds))
			{
				elapsedTime += Time.deltaTime;
				yield return new WaitForEndOfFrame();
				continue;
			}
			break;
		}
		yield return new WaitForSeconds(1f);
		PixelPlayerController.instance.EnableForGameplay();
	}

	public void ShowGameWin()
	{
		if (ChallengeManager.instance != null && ChallengeManager.instance.hasActiveChallenge && ChallengeManager.instance.latestActiveChallenge == Challenge.GameBuilder_Easy)
		{
			Challenge_GameBuilder_Easy.instance.Complete();
			return;
		}
		GameObject gameObject = UIOverlayCanvas.instance.Popup(gameWinPrefab);
		UIPopupGameEnd component = gameObject.GetComponent<UIPopupGameEnd>();
		component.SetGame(ref GameplayBuilder.instance.game);
		if (currentGame.ID() == AssetManager.poultryPanicVersion2 && !GemManager.instance.HasGemsForType(GemEarnable.Type.PoultryPanicV1))
		{
			component.InitWinForPoultryPanic();
		}
		else if (fromServer && AppStateManager.instance.iWallGameSquare != null && BloxelServerInterface.featuredGameIds.Contains(AppStateManager.instance.iWallGameSquare._id))
		{
			if (!PlayerBankManager.instance.AddGemForFeaturedGame(AppStateManager.instance.iWallGameSquare._id))
			{
				component.InitFeaturedWin();
			}
		}
		else
		{
			component.InitWin();
		}
	}

	public void ShowGameLoss()
	{
		SoundManager.PlayOneShot(SoundManager.instance.playerDeath);
		heroObject.GetComponent<PixelPlayerController>().animationController.HandlePlayerDeath(false);
		heroObject.GetComponent<PlayerMovementController>().enabled = false;
		PixelPlayerController.instance.selfTransform.position = new Vector3(PixelPlayerController.instance.selfTransform.position.x, PixelPlayerController.instance.selfTransform.position.y, BloxelCamera.instance.standardGameplayOffset.z - 50f);
		BloxelCamera.instance.target = null;
		StartCoroutine(WaitToShowGameLossPopup());
	}

	private IEnumerator WaitToShowGameLossPopup()
	{
		yield return new WaitForSeconds(1.5f);
		GameObject menuObj = UIOverlayCanvas.instance.Popup(gameOverPrefab);
		UIPopupGameEnd gameConfig = menuObj.GetComponent<UIPopupGameEnd>();
		gameConfig.SetGame(ref GameplayBuilder.instance.game);
		gameConfig.InitLose();
	}

	public void ReSpawnPlayerDelayInTest()
	{
		StartCoroutine("ReSpawnPlayerDelay");
	}

	public void CancelReSpawnPlayerDelayInTest()
	{
		StopCoroutine("ReSpawnPlayerDelay");
	}

	private IEnumerator ReSpawnPlayerDelay()
	{
		PixelPlayerController.instance.movementController.enabled = false;
		PixelPlayerController.instance.selfTransform.position = new Vector3(PixelPlayerController.instance.selfTransform.position.x, PixelPlayerController.instance.selfTransform.position.y, BloxelCamera.instance.standardGameplayOffset.z - 50f);
		BloxelCamera.instance.target = null;
		yield return new WaitForSeconds(1.5f);
		PixelPlayerController.instance.Reset();
		instance.ReplayFromCheckpiont();
		instance.heroObject.transform.localPosition = instance.lastCheckpoint;
		instance.StartDroppingCharacter();
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		GameHUD.instance.MaxHealth();
		yield return new WaitForSeconds(0.5f);
		GameHUD.instance.Reset(GameHUD.ResetMethod.FromCheckpoint);
		PixelPlayerController.instance.ResetInventoryFromCheckpoint();
	}

	public void PlayerEnteredWater()
	{
		inWater = true;
		PixelPlayerController.instance.movementController.EnableWaterProperties();
		waterMat.DOFade(waterSemiTransparentAlpha, UIAnimationManager.speedMedium);
	}

	public void PlayerExitedWater()
	{
		inWater = false;
		PixelPlayerController.instance.movementController.DisableWaterProperties();
		waterMat.DOFade(waterFullAlpha, UIAnimationManager.speedMedium);
	}

	public UIPopupMessage PopupMessage(string title, string description)
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(popupMessagePrefab);
		UIPopupMessage component = gameObject.GetComponent<UIPopupMessage>();
		component.title = title;
		component.description = description;
		return component;
	}
}
