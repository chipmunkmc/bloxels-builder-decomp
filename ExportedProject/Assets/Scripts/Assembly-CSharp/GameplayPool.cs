using System.Collections.Generic;
using UnityEngine;

public class GameplayPool : MonoBehaviour
{
	public static GameplayPool Instance;

	public static Transform SelfTransform;

	[Header("Prefab Resource Paths")]
	public string bulletPath;

	public string reflectedBulletPath;

	public string enemyProjectilePath;

	public string bombPath;

	public string laserPath;

	public string attractorPath;

	public string powerupIndicatorPath;

	[Header("Pool Sizes")]
	public int bulletPoolSize;

	public int reflectedBulletPoolSize;

	public int enemyProjectilePoolSize;

	public int bombPoolSize;

	public int laserPoolSize;

	public int attractorPoolSize;

	public int powerupIndicatorPoolSize;

	private static GameObject _bulletPrefab;

	private static GameObject _reflectedBulletPrefab;

	private static GameObject _enemyProjectilePrefab;

	private static GameObject _bombPrefab;

	private static GameObject _laserPrefab;

	private static GameObject _attractorPrefab;

	private static GameObject _powerupIndicatorPrefab;

	private static Stack<Bullet> _bulletPool;

	private static Stack<ReflectedBullet> _reflectedBulletPool;

	private static Stack<EnemyProjectile> _enemyProjectilePool;

	private static Stack<Bomb> _bombPool;

	private static Stack<EnemyLaser> _laserPool;

	private static Stack<ParticleAttractor> _attractorPool;

	private static Stack<PowerupIndicator> _powerupIndicatorPool;

	private static int _poolExtensionSize = 16;

	private static bool _InitComplete;

	private void Awake()
	{
		if (Instance != null && Instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			Instance = this;
			_InitComplete = true;
			SelfTransform = base.transform;
			Object.DontDestroyOnLoad(base.gameObject);
			Initialize();
		}
	}

	private void Initialize()
	{
		_bulletPrefab = Resources.Load<GameObject>(bulletPath);
		_reflectedBulletPrefab = Resources.Load<GameObject>(reflectedBulletPath);
		_enemyProjectilePrefab = Resources.Load<GameObject>(enemyProjectilePath);
		_bombPrefab = Resources.Load<GameObject>(bombPath);
		_laserPrefab = Resources.Load<GameObject>(laserPath);
		_attractorPrefab = Resources.Load<GameObject>(attractorPath);
		_powerupIndicatorPrefab = Resources.Load<GameObject>(powerupIndicatorPath);
		_bulletPool = new Stack<Bullet>(bulletPoolSize);
		_reflectedBulletPool = new Stack<ReflectedBullet>(reflectedBulletPoolSize);
		_enemyProjectilePool = new Stack<EnemyProjectile>(enemyProjectilePoolSize);
		_bombPool = new Stack<Bomb>(bombPoolSize);
		_laserPool = new Stack<EnemyLaser>(laserPoolSize);
		_attractorPool = new Stack<ParticleAttractor>(attractorPoolSize);
		_powerupIndicatorPool = new Stack<PowerupIndicator>(powerupIndicatorPoolSize);
		FillPools();
		Bullet.InitEmissionRates(_bulletPool.Peek());
		ReflectedBullet.InitEmissionRates(_reflectedBulletPool.Peek());
		Bomb.InitStaticValues(_bombPool.Peek());
		PowerupIndicator.InitStaticValues(_powerupIndicatorPool.Peek());
	}

	private void FillPools()
	{
		AllocatePooledComponents(_bulletPrefab, _bulletPool, bulletPoolSize);
		AllocatePooledComponents(_reflectedBulletPrefab, _reflectedBulletPool, reflectedBulletPoolSize);
		AllocatePooledComponents(_enemyProjectilePrefab, _enemyProjectilePool, enemyProjectilePoolSize);
		AllocatePooledComponents(_bombPrefab, _bombPool, bombPoolSize);
		AllocatePooledComponents(_laserPrefab, _laserPool, laserPoolSize);
		AllocatePooledComponents(_attractorPrefab, _attractorPool, attractorPoolSize);
		AllocatePooledComponents(_powerupIndicatorPrefab, _powerupIndicatorPool, powerupIndicatorPoolSize);
	}

	private static void AllocatePooledComponents<T>(GameObject prefab, Stack<T> pool, int numOfNewObjects) where T : PoolableComponent
	{
		for (int i = 0; i < numOfNewObjects; i++)
		{
			GameObject gameObject = Object.Instantiate(prefab);
			gameObject.name = prefab.name;
			Object.DontDestroyOnLoad(gameObject);
			T component = gameObject.GetComponent<T>();
			component.EarlyAwake();
			component.selfTransform.SetParent(SelfTransform);
			pool.Push(component);
		}
	}

	public static Bullet SpawnBullet()
	{
		if (_bulletPool.Count == 0)
		{
			AllocatePooledComponents(_bulletPrefab, _bulletPool, _poolExtensionSize);
		}
		Bullet bullet = _bulletPool.Pop();
		bullet.PrepareSpawn();
		return bullet;
	}

	public static void DespawnBullet(Bullet returningToPool)
	{
		returningToPool.PrepareDespawn();
		returningToPool.selfTransform.SetParent(SelfTransform);
		_bulletPool.Push(returningToPool);
	}

	public static ReflectedBullet SpawnReflectedBullet()
	{
		if (_reflectedBulletPool.Count == 0)
		{
			AllocatePooledComponents(_reflectedBulletPrefab, _reflectedBulletPool, _poolExtensionSize);
		}
		ReflectedBullet reflectedBullet = _reflectedBulletPool.Pop();
		reflectedBullet.PrepareSpawn();
		return reflectedBullet;
	}

	public static void DespawnReflectedBullet(ReflectedBullet returningToPool)
	{
		returningToPool.PrepareDespawn();
		returningToPool.selfTransform.SetParent(SelfTransform);
		_reflectedBulletPool.Push(returningToPool);
	}

	public static EnemyProjectile SpawnEnemyProjectile()
	{
		if (_enemyProjectilePool.Count == 0)
		{
			AllocatePooledComponents(_enemyProjectilePrefab, _enemyProjectilePool, _poolExtensionSize);
		}
		EnemyProjectile enemyProjectile = _enemyProjectilePool.Pop();
		enemyProjectile.PrepareSpawn();
		return enemyProjectile;
	}

	public static void DespawnEnemyProjectile(EnemyProjectile returningToPool)
	{
		returningToPool.PrepareDespawn();
		returningToPool.selfTransform.SetParent(SelfTransform);
		_enemyProjectilePool.Push(returningToPool);
	}

	public static Bomb SpawnBomb()
	{
		if (_bombPool.Count == 0)
		{
			AllocatePooledComponents(_bombPrefab, _bombPool, _poolExtensionSize);
		}
		Bomb bomb = _bombPool.Pop();
		bomb.PrepareSpawn();
		return bomb;
	}

	public static void DespawnBomb(Bomb returningToPool)
	{
		returningToPool.PrepareDespawn();
		returningToPool.selfTransform.SetParent(SelfTransform);
		_bombPool.Push(returningToPool);
	}

	public static EnemyLaser SpawnLaser()
	{
		if (_laserPool.Count == 0)
		{
			AllocatePooledComponents(_laserPrefab, _laserPool, _poolExtensionSize);
		}
		EnemyLaser enemyLaser = _laserPool.Pop();
		enemyLaser.PrepareSpawn();
		return enemyLaser;
	}

	public static void DespawnLaser(EnemyLaser returningToPool)
	{
		returningToPool.PrepareDespawn();
		returningToPool.selfTransform.SetParent(SelfTransform);
		_laserPool.Push(returningToPool);
	}

	public static ParticleAttractor SpawnAttractor()
	{
		if (_attractorPool.Count == 0)
		{
			AllocatePooledComponents(_attractorPrefab, _attractorPool, _poolExtensionSize);
		}
		ParticleAttractor particleAttractor = _attractorPool.Pop();
		particleAttractor.PrepareSpawn();
		return particleAttractor;
	}

	public static void DespawnAttractor(ParticleAttractor returningToPool)
	{
		returningToPool.PrepareDespawn();
		returningToPool.selfTransform.parent = SelfTransform;
		_attractorPool.Push(returningToPool);
	}

	public static PowerupIndicator SpawnPowerupIndicator()
	{
		if (_powerupIndicatorPool.Count == 0)
		{
			AllocatePooledComponents(_powerupIndicatorPrefab, _powerupIndicatorPool, _poolExtensionSize);
		}
		PowerupIndicator powerupIndicator = _powerupIndicatorPool.Pop();
		powerupIndicator.PrepareSpawn();
		return powerupIndicator;
	}

	public static void DespawnPowerupIndicator(PowerupIndicator returningToPool)
	{
		returningToPool.PrepareDespawn();
		returningToPool.selfTransform.parent = SelfTransform;
		_powerupIndicatorPool.Push(returningToPool);
	}

	public static void PrepareForSceneChange()
	{
		Stack<Bullet>.Enumerator enumerator = _bulletPool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.selfTransform.SetParent(SelfTransform);
			Object.DontDestroyOnLoad(enumerator.Current.gameObject);
		}
		Stack<ReflectedBullet>.Enumerator enumerator2 = _reflectedBulletPool.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.selfTransform.SetParent(SelfTransform);
			Object.DontDestroyOnLoad(enumerator2.Current.gameObject);
		}
		Stack<EnemyProjectile>.Enumerator enumerator3 = _enemyProjectilePool.GetEnumerator();
		while (enumerator3.MoveNext())
		{
			enumerator3.Current.selfTransform.SetParent(SelfTransform);
			Object.DontDestroyOnLoad(enumerator3.Current.gameObject);
		}
		Stack<Bomb>.Enumerator enumerator4 = _bombPool.GetEnumerator();
		while (enumerator4.MoveNext())
		{
			enumerator4.Current.selfTransform.SetParent(SelfTransform);
			Object.DontDestroyOnLoad(enumerator4.Current.gameObject);
		}
		Stack<EnemyLaser>.Enumerator enumerator5 = _laserPool.GetEnumerator();
		while (enumerator5.MoveNext())
		{
			enumerator5.Current.selfTransform.SetParent(SelfTransform);
			Object.DontDestroyOnLoad(enumerator5.Current.gameObject);
		}
		Stack<ParticleAttractor>.Enumerator enumerator6 = _attractorPool.GetEnumerator();
		while (enumerator6.MoveNext())
		{
			enumerator6.Current.selfTransform.SetParent(SelfTransform);
			Object.DontDestroyOnLoad(enumerator6.Current.gameObject);
		}
		Stack<PowerupIndicator>.Enumerator enumerator7 = _powerupIndicatorPool.GetEnumerator();
		while (enumerator7.MoveNext())
		{
			enumerator7.Current.selfTransform.SetParent(SelfTransform);
			Object.DontDestroyOnLoad(enumerator7.Current.gameObject);
		}
	}
}
