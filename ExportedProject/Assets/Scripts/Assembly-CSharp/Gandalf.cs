using System.Collections.Generic;
using UnityEngine;

public class Gandalf : MonoBehaviour
{
	public static Gandalf instance;

	public LinkedList<CanvasSquare> squareList;

	public bool isProcessing;

	private ProjectParseWorker parseWorker;

	private Coroutine parseWorkerWaitRoutine;

	private float timeBetweenUpdates = 0.05f;

	private float timeOfLastUpdate;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		parseWorker = new ProjectParseWorker();
		parseWorker.Init();
		parseWorker.Start();
		parseWorkerWaitRoutine = StartCoroutine(parseWorker.WaitFor());
		squareList = new LinkedList<CanvasSquare>();
		isProcessing = false;
	}

	private void Update()
	{
		if (Time.time - timeBetweenUpdates < timeOfLastUpdate)
		{
			return;
		}
		timeOfLastUpdate = Time.time;
		CanvasSquare canvasSquare = null;
		int num = squareList.Count;
		do
		{
			if (num == 0)
			{
				return;
			}
			LinkedListNode<CanvasSquare> first = squareList.First;
			squareList.RemoveFirst();
			canvasSquare = first.Value;
			num--;
		}
		while (canvasSquare.serverSquare == null);
		Run(canvasSquare);
	}

	private void OnDestroy()
	{
		Unload();
	}

	private void Unload()
	{
		instance = null;
		squareList = null;
		StopCoroutine(parseWorkerWaitRoutine);
		parseWorker.Interrupt();
		parseWorkerWaitRoutine = null;
		parseWorker = null;
	}

	public void Request(CanvasSquare square)
	{
		if (squareList.Remove(square))
		{
		}
		squareList.AddLast(square);
	}

	public void Run(CanvasSquare square)
	{
		StartCoroutine(BloxelServerRequests.instance.FindByID(square.serverSquare.type, square.serverSquare.projectID, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && !response.Equals(BloxelServerRequests.ErrorString))
			{
				if (square.serverSquare != null)
				{
					parseWorker.EnqueueParseJob(square, square.iWallCoordinate, response);
				}
			}
			else
			{
				square.SetFailedState();
			}
		}));
	}
}
