using System.Collections.Generic;

public class GemEarnable
{
	public enum Type
	{
		AppInit = 0,
		AccountSetup = 1,
		PoultryPanicV1 = 2,
		BoardVerify = 3,
		GameBuilder_Easy = 4,
		CharacterBuilder_Easy = 5,
		Bonus1 = 6,
		Bonus2 = 7,
		Bonus5 = 8,
		Bonus10 = 9,
		BulkRoomAddBack = 10
	}

	public static Dictionary<Type, int> valueLookup = new Dictionary<Type, int>
	{
		{
			Type.AppInit,
			5
		},
		{
			Type.AccountSetup,
			5
		},
		{
			Type.PoultryPanicV1,
			5
		},
		{
			Type.BoardVerify,
			250
		},
		{
			Type.GameBuilder_Easy,
			5
		},
		{
			Type.CharacterBuilder_Easy,
			5
		},
		{
			Type.Bonus1,
			1
		},
		{
			Type.Bonus2,
			2
		},
		{
			Type.Bonus5,
			5
		},
		{
			Type.Bonus10,
			10
		},
		{
			Type.BulkRoomAddBack,
			166
		}
	};

	public Type type;

	public int value;

	public bool isAvailable;

	public GemEarnable(Type _type)
	{
		type = _type;
		value = valueLookup[_type];
	}
}
