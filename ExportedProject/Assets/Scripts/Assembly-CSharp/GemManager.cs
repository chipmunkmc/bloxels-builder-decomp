using System;
using System.Collections.Generic;
using UnityEngine;

public class GemManager : MonoBehaviour
{
	public static GemManager instance;

	public static bool initComplete;

	public static List<EnemyType> enemyTypeUnlocks = new List<EnemyType>(3);

	public static List<PowerUpType> powerupUnlocks = new List<PowerUpType>(6);

	public static bool[] brainSlotPurchases = new bool[6];

	public static int roomsUnlocked = 0;

	public static bool animationFramesUnlocked = false;

	public static readonly int MaxAnimationFramesWithoutUnlock = 4;

	public static readonly int MaxRoomsWithoutUnlock = 20;

	public static readonly int InitialPowerups = 3;

	public static readonly int InitialEnemyTypes = 1;

	public static readonly int InitialBrainSlots = 2;

	public static readonly Dictionary<GemUnlockable.Type, int> maxUnlock = new Dictionary<GemUnlockable.Type, int>
	{
		{
			GemUnlockable.Type.MapRoom,
			169
		},
		{
			GemUnlockable.Type.PowerupType,
			Enum.GetValues(typeof(PowerUpType)).Length
		},
		{
			GemUnlockable.Type.EnemyType,
			Enum.GetValues(typeof(EnemyType)).Length
		},
		{
			GemUnlockable.Type.AnimationFrames,
			1
		},
		{
			GemUnlockable.Type.BrainBoardSlot,
			brainSlotPurchases.Length
		}
	};

	public static Dictionary<GemUnlockable.Type, int> startIndices = new Dictionary<GemUnlockable.Type, int>();

	[Header("Product Icons")]
	public Sprite iconBrainSlot;

	public Sprite iconMapRoomUnlock;

	public Sprite iconEnemyPatroller;

	public Sprite iconEnemyFlyer;

	public Sprite iconEnemyTurret;

	public Sprite iconPowerupBomb;

	public Sprite iconPowerupHealth;

	public Sprite iconPowerupJetpack;

	public Sprite iconPowerupShrink;

	public Sprite iconPowerupInvincible;

	public Sprite iconAnimationFrame;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (!initComplete)
		{
			SetBrainSlotsFromDisk();
			SetRoomsUnlockedFromDisk();
			SetEnemyTypeUnlocksFromDisk();
			SetPowerupUnlocksFromDisk();
			SetFrameValueFromDisk();
			SetupStartIndices();
			initComplete = true;
		}
	}

	public bool HasGemsForType(GemEarnable.Type type)
	{
		return PrefsManager.instance.HasGemsForType(type);
	}

	public int AddGemsForType(GemEarnable.Type type)
	{
		if (PrefsManager.instance.HasGemsForType(type))
		{
			return PlayerBankManager.instance.GetGemCount();
		}
		PrefsManager.instance.AddGemsForType(type);
		return PlayerBankManager.instance.AddGems(GemEarnable.valueLookup[type]);
	}

	public static int GemValueFromGemBoolString(string gemBool)
	{
		char[] array = gemBool.ToCharArray();
		GemEarnable.Type[] array2 = Enum.GetValues(typeof(GemEarnable.Type)) as GemEarnable.Type[];
		int num = 0;
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] == '1')
			{
				num += GemEarnable.valueLookup[array2[i]];
			}
		}
		return num;
	}

	public static int GemValueFromFeaturedBoolString(string featuredBool)
	{
		char[] array = featuredBool.ToCharArray();
		int num = 0;
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] == '1')
			{
				num++;
			}
		}
		return num;
	}

	public static int GetSpendFromSpendBool(string spendBool)
	{
		char[] array = spendBool.ToCharArray();
		int num = 0;
		Dictionary<GemUnlockable.Type, int>.Enumerator enumerator = startIndices.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GemUnlockable.Type key = enumerator.Current.Key;
			int value = enumerator.Current.Value;
			int num2 = maxUnlock[key] + value;
			int num3 = GemUnlockable.valueLookup[key];
			for (int i = value; i < num2; i++)
			{
				if (array[i] == '1')
				{
					num += num3;
				}
			}
		}
		return num;
	}

	public bool IsBrainSlotLocked(int idx)
	{
		return !brainSlotPurchases[idx];
	}

	public void UnlockBrainSlot(int idx)
	{
		brainSlotPurchases[idx] = true;
		PrefsManager.instance.BrainSlotUnlock(idx);
	}

	public void SetBrainSlotsFromDisk()
	{
		for (int i = 0; i < brainSlotPurchases.Length; i++)
		{
			brainSlotPurchases[i] = PrefsManager.instance.CheckBrainSlot(i);
		}
	}

	public int BulkRoomUnlock()
	{
		roomsUnlocked = 169;
		PrefsManager.instance.BulkUnlockRooms();
		return roomsUnlocked;
	}

	public int UnlockMapRoom()
	{
		roomsUnlocked++;
		PrefsManager.instance.UnlockMapRoom();
		return roomsUnlocked;
	}

	public void SetRoomsUnlockedFromDisk()
	{
		roomsUnlocked = PrefsManager.instance.GetMapRoomCount();
	}

	public bool IsEnemyTypeLocked(EnemyType type)
	{
		if (enemyTypeUnlocks.Contains(type))
		{
			return false;
		}
		return true;
	}

	public void UnlockEnemyType(EnemyType type)
	{
		if (!enemyTypeUnlocks.Contains(type))
		{
			enemyTypeUnlocks.Add(type);
		}
		PrefsManager.instance.EnemyTypeUnlock(type);
	}

	public void SetEnemyTypeUnlocksFromDisk()
	{
		EnemyType[] array = Enum.GetValues(typeof(EnemyType)) as EnemyType[];
		foreach (EnemyType enemyType in array)
		{
			if (PrefsManager.instance.CheckEnemyType(enemyType) && !enemyTypeUnlocks.Contains(enemyType))
			{
				enemyTypeUnlocks.Add(enemyType);
			}
		}
	}

	public bool IsPowerupLocked(PowerUpType type)
	{
		if (powerupUnlocks.Contains(type))
		{
			return false;
		}
		return true;
	}

	public void UnlockPowerup(PowerUpType type)
	{
		if (!powerupUnlocks.Contains(type))
		{
			powerupUnlocks.Add(type);
		}
		PrefsManager.instance.PowerupUnlock(type);
	}

	public void SetPowerupUnlocksFromDisk()
	{
		PowerUpType[] array = Enum.GetValues(typeof(PowerUpType)) as PowerUpType[];
		foreach (PowerUpType powerUpType in array)
		{
			if (PrefsManager.instance.CheckPowerup(powerUpType) && !powerupUnlocks.Contains(powerUpType))
			{
				powerupUnlocks.Add(powerUpType);
			}
		}
	}

	public void UnlockAnimationFrames()
	{
		animationFramesUnlocked = true;
		PrefsManager.instance.hasAnimationFrames = true;
		PrefsManager.instance.UpdateSpend(GemUnlockable.Type.AnimationFrames);
	}

	public void SetFrameValueFromDisk()
	{
		animationFramesUnlocked = PrefsManager.instance.hasAnimationFrames;
	}

	private void SetupStartIndices()
	{
		int num = 0;
		Dictionary<GemUnlockable.Type, int>.Enumerator enumerator = maxUnlock.GetEnumerator();
		while (enumerator.MoveNext())
		{
			startIndices.Add(enumerator.Current.Key, num);
			num += enumerator.Current.Value;
		}
	}
}
