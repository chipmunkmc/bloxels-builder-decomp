using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public abstract class GemUnlockable
{
	public enum Type
	{
		MapRoom = 0,
		PowerupType = 1,
		EnemyType = 2,
		AnimationFrames = 3,
		BrainBoardSlot = 4
	}

	public struct UIBuyable
	{
		public string title;

		public string bodyText;

		public Sprite sprite;

		public int gemCost;

		public Type type;

		public Color productTint;

		public UIBuyable(string _title, string _body, Sprite _sprite, int cost, Type _type)
		{
			title = _title;
			bodyText = _body;
			sprite = _sprite;
			gemCost = cost;
			type = _type;
			productTint = Color.white;
		}

		public UIBuyable(string _title, string _body, Sprite _sprite, int cost, Type _type, Color tint)
		{
			title = _title;
			bodyText = _body;
			sprite = _sprite;
			gemCost = cost;
			type = _type;
			productTint = tint;
		}

		public static UIBuyable CreateBrainSlot()
		{
			return new UIBuyable(LocalizationManager.getInstance().getLocalizedText("gamebuilder175", "Brain Save Slot"), LocalizationManager.getInstance().getLocalizedText("gamebuilder174", "Want to unlock this Brain slot? Spend your gems to make it so!"), GemManager.instance.iconBrainSlot, BrainSlotCost, Type.BrainBoardSlot);
		}

		public static UIBuyable CreateAnimationFrame()
		{
			return new UIBuyable(LocalizationManager.getInstance().getLocalizedText("gamebuilder182", "Animation Frames"), LocalizationManager.getInstance().getLocalizedText("gamebuilder195", "Max Frames is currently") + " " + GemManager.MaxAnimationFramesWithoutUnlock + " " + LocalizationManager.getInstance().getLocalizedText("gamebuilder196", ". Spend Gems to unlock") + " " + BloxelAnimation.MaxFrames + "!", GemManager.instance.iconAnimationFrame, AnimationFramesCost, Type.AnimationFrames);
		}

		public static UIBuyable CreateMapRoom()
		{
			return new UIBuyable(LocalizationManager.getInstance().getLocalizedText("gamebuilder180", "Map Room"), LocalizationManager.getInstance().getLocalizedText("gamebuilder179", "Want to add another room to your arsenal? Spend your gems to make it so!"), GemManager.instance.iconMapRoomUnlock, MapRoomCost, Type.MapRoom);
		}

		public static UIBuyable CreateEnemy(EnemyType type)
		{
			string text = string.Empty;
			Sprite sprite = null;
			switch (type)
			{
			case EnemyType.Patrol:
				text = LocalizationManager.getInstance().getLocalizedText("gamebuilder162", "Patroller");
				sprite = GemManager.instance.iconEnemyPatroller;
				break;
			case EnemyType.Flyer:
				text = LocalizationManager.getInstance().getLocalizedText("gamebuilder163", "Flyer");
				sprite = GemManager.instance.iconEnemyFlyer;
				break;
			case EnemyType.Stationary:
				text = LocalizationManager.getInstance().getLocalizedText("gamebuilder164", "Turret");
				sprite = GemManager.instance.iconEnemyTurret;
				break;
			}
			return new UIBuyable(LocalizationManager.getInstance().getLocalizedText("gamebuilder176", "Enemy Type"), LocalizationManager.getInstance().getLocalizedText("gamebuilder177", "Want to add the") + " " + text + LocalizationManager.getInstance().getLocalizedText("gamebuilder178", " to your arsenal? Spend your gems to make it so!"), sprite, EnemyTypeCost, Type.EnemyType);
		}

		public static UIBuyable CreatePowerup(PowerUpType type)
		{
			KeyValuePair<string, string> keyValuePair = BloxelPowerUpTile.PowerUpNames[type];
			string localizedText = LocalizationManager.getInstance().getLocalizedText(keyValuePair.Key, keyValuePair.Value);
			Sprite sprite = null;
			switch (type)
			{
			case PowerUpType.Bomb:
				sprite = GemManager.instance.iconPowerupBomb;
				break;
			case PowerUpType.Health:
				sprite = GemManager.instance.iconPowerupHealth;
				break;
			case PowerUpType.Jetpack:
				sprite = GemManager.instance.iconPowerupJetpack;
				break;
			case PowerUpType.Map:
				sprite = GemManager.instance.iconMapRoomUnlock;
				break;
			case PowerUpType.Shrink:
				sprite = GemManager.instance.iconPowerupShrink;
				break;
			case PowerUpType.Invincibility:
				sprite = GemManager.instance.iconPowerupInvincible;
				break;
			}
			return new UIBuyable(LocalizationManager.getInstance().getLocalizedText("gamebuilder194", "Powerup Type"), LocalizationManager.getInstance().getLocalizedText("gamebuilder177", "Want to add the") + " " + localizedText + LocalizationManager.getInstance().getLocalizedText("gamebuilder178", " to your arsenal? Spend your gems to make it so!"), sprite, PowerupCost, Type.PowerupType);
		}
	}

	public delegate void HandleUnlock(GemUnlockable _model);

	public static readonly int MapRoomCost = 3;

	public static readonly int PowerupCost = 5;

	public static readonly int EnemyTypeCost = 5;

	public static readonly int AnimationFramesCost = 5;

	public static readonly int BrainSlotCost = 2;

	public static readonly int BulkRoomCost = 0;

	public static Dictionary<Type, int> valueLookup = new Dictionary<Type, int>
	{
		{
			Type.MapRoom,
			MapRoomCost
		},
		{
			Type.PowerupType,
			PowerupCost
		},
		{
			Type.EnemyType,
			EnemyTypeCost
		},
		{
			Type.AnimationFrames,
			AnimationFramesCost
		},
		{
			Type.BrainBoardSlot,
			BrainSlotCost
		}
	};

	public Type type;

	public int cost;

	public UIBuyable uiBuyable;

	public event HandleUnlock OnUnlock;

	public abstract void Unlock();

	public abstract bool IsLocked();

	public void EventUnlock(GemUnlockable _model)
	{
		if (this.OnUnlock != null)
		{
			this.OnUnlock(_model);
		}
	}
}
