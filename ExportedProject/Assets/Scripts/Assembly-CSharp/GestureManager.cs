using UnityEngine;

public class GestureManager : MonoBehaviour
{
	public static GestureManager instance;

	public GameObject _primaryCanvas;

	private BoardZoomManager _boardZoomManager;

	private PixelToolPalette _pixelToolPalette;

	private BloxelLibrary _library;

	private UIOverlayCanvas _uiOverlayCanvas;

	private PixelEditorPaintBoard _pixelEditorPaintBoard;

	private GestureTutorialManager _gestureTutorialManager;

	public bool isMultiTouching;

	public bool isPixelEditorIpadScene;

	public bool isInfinityWallScene;

	private float _minimumDragDistance;

	private float _minimumPinchExpandDistance;

	private float _screenWidth;

	private Vector3 _firstTouchPosition;

	private Vector3 _lastTouchPosition;

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
		}
		else
		{
			instance = this;
		}
	}

	private void Start()
	{
		_boardZoomManager = BoardZoomManager.instance;
		_gestureTutorialManager = GestureTutorialManager.instance;
		_pixelToolPalette = PixelToolPalette.instance;
		_pixelEditorPaintBoard = PixelEditorPaintBoard.instance;
		_uiOverlayCanvas = UIOverlayCanvas.instance;
		_library = BloxelLibrary.instance;
		DetermineScene();
		DetermineScreenWidth();
		DetermineMinimumDragDistance();
	}

	private void Update()
	{
		if (Input.touchCount > 0 && _primaryCanvas.activeInHierarchy)
		{
			handlePinchGestures();
			handleSwipeGestures();
		}
	}

	public void handlePinchGestures()
	{
		if (Input.touchCount != 2)
		{
			return;
		}
		Touch touch = Input.GetTouch(0);
		Touch touch2 = Input.GetTouch(1);
		Vector2 vector = touch.position - touch.deltaPosition;
		Vector2 vector2 = touch2.position - touch2.deltaPosition;
		float magnitude = (vector - vector2).magnitude;
		float magnitude2 = (touch.position - touch2.position).magnitude;
		float num = magnitude - magnitude2;
		if (num > _minimumPinchExpandDistance)
		{
			if (isPixelEditorIpadScene)
			{
				if (MegaBoardCanvas.instance.isVisible)
				{
					MegaBoardCanvas.instance.ZoomOut();
				}
				else
				{
					_boardZoomManager.ZoomOutOnGameBoardIfZoomable();
				}
			}
			else if (isInfinityWallScene && PlayerHUD.instance.infinityViewOn)
			{
				PlayerHUD.instance.ToggleInfinityView();
			}
		}
		else
		{
			if (!(num < 0f - _minimumPinchExpandDistance))
			{
				return;
			}
			if (isPixelEditorIpadScene)
			{
				if (MegaBoardCanvas.instance.isVisible && !PixelEditorController.instance.mode.Equals(CanvasMode.MegaBoard) && !MegaBoardCanvas.instance.isDragging)
				{
					MegaBoardCanvas.instance.ZoomIn();
				}
				else
				{
					_boardZoomManager.ZoomInOnGameBoardIfZoomable();
				}
			}
			else if (isInfinityWallScene && !PlayerHUD.instance.infinityViewOn)
			{
				PlayerHUD.instance.ToggleInfinityView();
			}
		}
	}

	private void handleSwipeGestures()
	{
		if (_uiOverlayCanvas.transform.childCount > _uiOverlayCanvas.staticChildCount || Input.touchCount != 1)
		{
			return;
		}
		Touch touch = Input.GetTouch(0);
		if (touch.phase == TouchPhase.Began)
		{
			_firstTouchPosition = touch.position;
			_lastTouchPosition = touch.position;
		}
		else
		{
			if (touch.phase != TouchPhase.Moved)
			{
				return;
			}
			_lastTouchPosition = touch.position;
			if ((!(Mathf.Abs(_lastTouchPosition.x - _firstTouchPosition.x) > _minimumDragDistance) && !(Mathf.Abs(_lastTouchPosition.y - _firstTouchPosition.y) > _minimumDragDistance)) || !(Mathf.Abs(_lastTouchPosition.x - _firstTouchPosition.x) > Mathf.Abs(_lastTouchPosition.y - _firstTouchPosition.y)))
			{
				return;
			}
			if (_lastTouchPosition.x > _firstTouchPosition.x)
			{
				if (isPixelEditorIpadScene)
				{
					if (_firstTouchPosition.x < _screenWidth * 0.08f && !_boardZoomManager.isZoomed && !_library.isVisible && !_pixelToolPalette.currentState.Equals(PixelToolPalette.State.Open))
					{
						_library.libraryToggle.Show();
					}
					else if (_pixelToolPalette.currentState.Equals(PixelToolPalette.State.Open) && !_library.isVisible && _pixelEditorPaintBoard.isVisible && !PixelEditorPaintBoard.instance.isPainting)
					{
						_pixelToolPalette.Dock();
					}
				}
				else if (isInfinityWallScene && _firstTouchPosition.x < _screenWidth * 0.08f && !_library.isVisible && Input.touchCount == 1)
				{
					_library.libraryToggle.Show();
				}
			}
			else if (isPixelEditorIpadScene)
			{
				if (_firstTouchPosition.x > _screenWidth * 0.85f && !_pixelToolPalette.currentState.Equals(PixelToolPalette.State.Open) && !_library.isVisible && _pixelEditorPaintBoard.isVisible)
				{
					_pixelToolPalette.Open();
				}
				else if (!_boardZoomManager.isZoomed && _library.isVisible && !_pixelToolPalette.currentState.Equals(PixelToolPalette.State.Open))
				{
					_library.libraryToggle.Hide();
				}
			}
			else if (isInfinityWallScene && _firstTouchPosition.x < _screenWidth * 0.28f && _library.isVisible)
			{
				_library.libraryToggle.Hide();
			}
		}
	}

	private void DetermineScreenWidth()
	{
		_screenWidth = Screen.width;
	}

	private void DetermineMinimumDragDistance()
	{
		_minimumDragDistance = Screen.height * 12 / 100;
		_minimumPinchExpandDistance = 5f;
	}

	private void DetermineIfUserIsMultiTouching()
	{
		if (Input.touchCount > 1)
		{
			isMultiTouching = true;
		}
		else
		{
			isMultiTouching = false;
		}
	}

	private void DetermineScene()
	{
	}
}
