using System.Collections;
using DG.Tweening;
using UnityEngine;

public class GestureTutorialManager : MonoBehaviour
{
	public static GestureTutorialManager instance;

	public GameObject PinchedFingersImage;

	public GameObject ExpandedFingersImage;

	public GameObject UpArrow;

	public GameObject DownArrow;

	private int currentLoops;

	private int maxLoops = 4;

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
		}
		else
		{
			instance = this;
		}
	}

	public void DetermineIfUserIsReadyToSeePinchZoomInTutorial()
	{
		if (!PrefsManager.instance.hasSeenPinchZoomTutorial && PixelEditorController.instance.mode.Equals(CanvasMode.LevelEditor) && PixelEditorPaintBoard.instance.isVisible && UIOverlayCanvas.instance.transform.childCount <= UIOverlayCanvas.instance.staticChildCount && !PixelToolPalette.instance.isOpen && BoardZoomManager.instance.is16by10OrWider)
		{
			InitiatePinchTutorial();
		}
	}

	public void DetermineIfUserIsReadyToSeePinchInfinityViewTutorial()
	{
		if (!PrefsManager.instance.hasSeenPinchInfinityViewTutorial)
		{
			InitiatePinchTutorial();
		}
	}

	public void InitiatePinchTutorial()
	{
		StartCoroutine(initiatePinchZoomInPixelBoardTutorialCo());
	}

	private IEnumerator initiatePinchZoomInPixelBoardTutorialCo()
	{
		if (GestureManager.instance.isInfinityWallScene)
		{
			yield return new WaitForSeconds(1f);
		}
		PinchedFingersImage.transform.DOScale(1f, UIAnimationManager.speedFast);
		UpArrow.transform.DOScale(1f, UIAnimationManager.speedFast);
		DownArrow.transform.DOScaleX(-1f, UIAnimationManager.speedFast);
		DownArrow.transform.DOScaleY(1f, UIAnimationManager.speedFast);
		yield return new WaitForSeconds(1f);
		PinchedFingersImage.transform.localScale = Vector3.zero;
		ExpandedFingersImage.transform.localScale = Vector3.one;
		yield return new WaitForSeconds(2f);
		ExpandedFingersImage.transform.DOScale(0f, UIAnimationManager.speedFast);
		UpArrow.transform.DOScale(0f, UIAnimationManager.speedFast);
		DownArrow.transform.DOScale(0f, UIAnimationManager.speedFast);
		yield return new WaitForSeconds(2f);
		if (GestureManager.instance.isInfinityWallScene)
		{
			if (currentLoops < maxLoops)
			{
				DetermineIfUserIsReadyToSeePinchInfinityViewTutorial();
				currentLoops++;
			}
		}
		else
		{
			DetermineIfUserIsReadyToSeePinchZoomInTutorial();
		}
	}
}
