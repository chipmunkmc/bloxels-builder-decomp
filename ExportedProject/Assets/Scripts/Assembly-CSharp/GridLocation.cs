using System;

public struct GridLocation : IEquatable<GridLocation>
{
	public int c;

	public int r;

	public static GridLocation InvalidLocation = new GridLocation(-1, -1);

	public GridLocation(int col, int row)
	{
		c = col;
		r = row;
	}

	public static bool operator ==(GridLocation left, GridLocation right)
	{
		return left.c == right.c && left.r == right.r;
	}

	public static bool operator !=(GridLocation left, GridLocation right)
	{
		return left.c != right.c || left.r != right.r;
	}

	public override int GetHashCode()
	{
		int num = 17;
		num = num * 23 + c.GetHashCode();
		return num * 23 + r.GetHashCode();
	}

	public override bool Equals(object obj)
	{
		if (!(obj is GridLocation))
		{
			return false;
		}
		return Equals((GridLocation)obj);
	}

	public bool Equals(GridLocation loc)
	{
		return c == loc.c && r == loc.r;
	}

	public override string ToString()
	{
		return "GridLocation: [column: " + c + ", row: " + r + "]";
	}
}
