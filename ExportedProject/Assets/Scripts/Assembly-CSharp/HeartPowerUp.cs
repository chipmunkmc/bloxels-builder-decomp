using DG.Tweening;
using UnityEngine;

public class HeartPowerUp : PowerUp
{
	private new void Start()
	{
		base.Start();
		powerUpType = PowerUpType.Health;
		Bounce();
	}

	private void OnDestroy()
	{
		base.transform.DOKill();
	}

	public override void Collect()
	{
		base.Collect();
		PixelPlayerController.instance.GetHealth();
		if ((bool)GameHUD.instance)
		{
			GameHUD.instance.MaxHealth();
		}
		Object.Destroy(base.gameObject);
	}
}
