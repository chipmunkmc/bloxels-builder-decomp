using UnityEngine;
using UnityEngine.EventSystems;

public class HeroEditorCanvas : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	private bool isDragging;

	private bool shouldDragHero;

	private int numberOfTiles = 169;

	private bool[,] occupiedSpaces;

	private GridLocation pointerDownLocation;

	private GridLocation lastValidDropLocation;

	private float xMin;

	private float xMax;

	private float yMin;

	private float yMax;

	private void Awake()
	{
		occupiedSpaces = new bool[13, 13];
	}

	private void Start()
	{
		GameBuilderCanvas.instance.OnGameBuilderLevelChange += HandleOnGameBuilderLevelChange;
		Init();
	}

	private void Init()
	{
		SetUpOccupiedSpacesForLevel(GameBuilderCanvas.instance.currentBloxelLevel);
	}

	private void SetUpOccupiedSpacesForLevel(BloxelLevel level)
	{
		BlockColor[,] blockColors = level.coverBoard.blockColors;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = blockColors[j, i];
				if (blockColor != BlockColor.Blank)
				{
					occupiedSpaces[j, i] = true;
				}
			}
		}
		xMin = (float)(level.location.c * 169) + 6f;
		xMax = xMin + 169f - 13f;
		yMin = level.location.r * 169;
		yMax = yMin + 169f - 13f;
	}

	private void HandleOnGameBuilderLevelChange(BloxelLevel level)
	{
		SetUpOccupiedSpacesForLevel(level);
	}

	private void OnDestroy()
	{
		GameBuilderCanvas.instance.OnGameBuilderLevelChange -= HandleOnGameBuilderLevelChange;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!isDragging && !(GameBuilderCanvas.instance.currentGame.heroStartPosition.locationInBoard == pointerDownLocation))
		{
			Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.mainCamera, Input.mousePosition, WorldWrapper.instance.selfTransform.position.z);
			Vector3 localPosition = new Vector3(Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane.x / 13f) * 13 + 6, xMin, xMax), Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane.y / 13f) * 13, yMin, yMax), -7f);
			GameplayController.instance.heroObject.transform.localPosition = localPosition;
			GridLocation gridLocationFromScreenPosition = WireframeEditorCanvas.GetGridLocationFromScreenPosition();
			int col = Mathf.Clamp(gridLocationFromScreenPosition.c, 0, 12);
			int row = Mathf.Clamp(gridLocationFromScreenPosition.r, 0, 12);
			GridLocation locationInBoard = new GridLocation(col, row);
			GameBuilderCanvas.instance.currentGame.SetHeroAtLocation(GameBuilderCanvas.instance.currentGame.hero, new WorldLocation(GameBuilderCanvas.instance.currentBloxelLevel.id, locationInBoard, GameBuilderCanvas.instance.currentBloxelLevel.location));
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		GridLocation gridLocationFromScreenPosition = WireframeEditorCanvas.GetGridLocationFromScreenPosition();
		int col = Mathf.Clamp(gridLocationFromScreenPosition.c, 0, 12);
		int row = Mathf.Clamp(gridLocationFromScreenPosition.r, 0, 12);
		pointerDownLocation = new GridLocation(col, row);
		lastValidDropLocation = GameBuilderCanvas.instance.currentGame.heroStartPosition.locationInBoard;
		SoundManager.PlayOneShot(SoundManager.instance.confirmC);
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		isDragging = true;
		if (!(GameBuilderCanvas.instance.currentGame.heroStartPosition.locationInBoard != pointerDownLocation))
		{
			shouldDragHero = true;
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (shouldDragHero)
		{
			Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.mainCamera, Input.mousePosition, WorldWrapper.instance.selfTransform.position.z);
			GridLocation gridLocationFromScreenPosition = WireframeEditorCanvas.GetGridLocationFromScreenPosition();
			int col = Mathf.Clamp(gridLocationFromScreenPosition.c, 0, 12);
			int row = Mathf.Clamp(gridLocationFromScreenPosition.r, 0, 12);
			lastValidDropLocation = new GridLocation(col, row);
			Vector3 worldPositionOnXYPlane2 = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.mainCamera, Input.mousePosition, WorldWrapper.instance.selfTransform.position.z);
			Vector3 localPosition = new Vector3(Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane2.x / 13f) * 13 + 6, xMin, xMax), Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane2.y / 13f) * 13, yMin, yMax), -7f);
			GameplayController.instance.heroObject.transform.localPosition = localPosition;
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		isDragging = false;
		if (shouldDragHero)
		{
			shouldDragHero = false;
			Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.mainCamera, Input.mousePosition, WorldWrapper.instance.selfTransform.position.z);
			Vector3 localPosition = new Vector3(Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane.x / 13f) * 13 + 6, xMin, xMax), Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane.y / 13f) * 13, yMin, yMax), -7f);
			SoundManager.PlayOneShot(SoundManager.instance.confirmC);
			GameplayController.instance.heroObject.transform.localPosition = localPosition;
			GridLocation gridLocationFromScreenPosition = WireframeEditorCanvas.GetGridLocationFromScreenPosition();
			int col = Mathf.Clamp(gridLocationFromScreenPosition.c, 0, 12);
			int row = Mathf.Clamp(gridLocationFromScreenPosition.r, 0, 12);
			GridLocation locationInBoard = new GridLocation(col, row);
			GameBuilderCanvas.instance.currentGame.SetHeroAtLocation(GameBuilderCanvas.instance.currentGame.hero, new WorldLocation(GameBuilderCanvas.instance.currentBloxelLevel.id, locationInBoard, GameBuilderCanvas.instance.currentBloxelLevel.location));
		}
	}
}
