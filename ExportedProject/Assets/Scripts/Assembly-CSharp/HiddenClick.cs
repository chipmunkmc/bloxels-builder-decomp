using UnityEngine;
using UnityEngine.EventSystems;

public class HiddenClick : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public void OnPointerClick(PointerEventData eData)
	{
		if (CurrentUser.instance.account != null && CurrentUser.instance.account.role >= 8)
		{
			BloxelCamera.instance.RenderAndUpload();
		}
	}
}
