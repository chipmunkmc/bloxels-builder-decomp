using UnityEngine;

public class HideForEdu : MonoBehaviour
{
	[HideInInspector]
	public RectTransform rect;

	private void Awake()
	{
		rect = base.transform as RectTransform;
	}

	private void Start()
	{
		if (CurrentUser.instance.IsEduUser())
		{
			rect.localScale = Vector3.zero;
		}
	}
}
