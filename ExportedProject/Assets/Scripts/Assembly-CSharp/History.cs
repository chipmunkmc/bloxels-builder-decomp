using System;
using System.Collections.Generic;

[Serializable]
public class History<T>
{
	private List<T> items = new List<T>();

	public int Count()
	{
		return items.Count;
	}

	public void Clear()
	{
		items.Clear();
	}

	public bool Contains(T item)
	{
		return items.Contains(item);
	}

	public void Push(T item)
	{
		items.Add(item);
	}

	public T Pop()
	{
		if (items.Count > 1)
		{
			T result = items[items.Count - 1];
			items.RemoveAt(items.Count - 1);
			return result;
		}
		return default(T);
	}

	public T PeekABoo()
	{
		return default(T);
	}

	public void RemoveAt(int itemAtPosition)
	{
		items.RemoveAt(itemAtPosition);
	}
}
