using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class HomeController : MonoBehaviour
{
	public static HomeController instance;

	[Header("UI")]
	public HomeMenu menu;

	public RectTransform rectLoading1;

	public RectTransform rectLoading2;

	public TextMeshProUGUI loadingText;

	public GameHUD hud;

	public GameObject touchControls;

	public UIHeroSwap heroSwap;

	public UIRoomSwap roomSwap;

	public UIBrainSwap brainSwap;

	public Bounds iWallWarpBounds;

	public Bounds editorWarpBounds;

	[Tooltip("Server Game ID to load")]
	public string gameToLoad = "57a8e8d529468b792977bdf0";

	private static readonly string _repeatingMethod = "CheckPlayerMovement";

	private static readonly string _watiForAssetManager = "WaitForAssetManager";

	private static readonly string _menuShowTimer = "menuShowTimer";

	private bool _initComplete;

	private bool _playerMoving;

	private bool _playerHitGround;

	public bool heroSwapVisible;

	public Object heroSwapPrefab;

	public Object roomSwapPrefab;

	public Object brainSwapPrefab;

	public Object fadePrefab;

	public Vector3 initialMenuScale;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Start()
	{
		touchControls.transform.localScale = Vector3.zero;
		AppStateManager.instance.SetCurrentScene(SceneName.Home);
		GameplayController.instance.gameplayActive = true;
		WorldWrapper.instance.Show();
		GameplayController.instance.currentMode = GameplayController.Mode.Gameplay;
		GameplayController.instance.OnReset += HandleReset;
		StartCoroutine(_watiForAssetManager);
		StartCoroutine(UINavMenu.instance.RotateMenuItemsWheel());
		initialMenuScale = menu.transform.localScale;
	}

	private void LateUpdate()
	{
		if (!_initComplete || PixelPlayerController.instance == null || PixelPlayerController.instance.selfTransform == null || PixelPlayerController.instance.isShrunk || PixelPlayerController.instance.isUsingShrinkPotion)
		{
			return;
		}
		heroSwap.playerInZone = (heroSwap.bounds.Contains(PixelPlayerController.instance.selfTransform.localPosition) ? true : false);
		if (heroSwap.playerInZone)
		{
			playerHitGround();
		}
		roomSwap.playerInZone = (roomSwap.bounds.Contains(PixelPlayerController.instance.selfTransform.localPosition) ? true : false);
		brainSwap.playerInZone = (brainSwap.bounds.Contains(PixelPlayerController.instance.selfTransform.localPosition) ? true : false);
		if (!roomSwap.playerInZone && !brainSwap.playerInZone)
		{
			BloxelCamera.instance.activeController.SetCamerOffsetToPrevious();
		}
		if (iWallWarpBounds.Contains(PixelPlayerController.instance.selfTransform.localPosition))
		{
			warpTo(SceneName.Viewer);
		}
		if (editorWarpBounds.Contains(PixelPlayerController.instance.selfTransform.localPosition))
		{
			warpTo(SceneName.PixelEditor_iPad);
		}
		if (!UINavMenu.instance.canShowNavBarWheel)
		{
			return;
		}
		UINavMenu.instance.menuWheelButtons.transform.Rotate(0f, 0f, -30f * Time.deltaTime);
		for (int i = 0; i < UINavMenu.instance._buttonPositions.Length; i++)
		{
			if (UINavMenu.instance._buttonPositions[i] != UINavMenu.instance.wheelParent)
			{
				UINavMenu.instance._buttonPositions[i].Rotate(0f, 0f, 15f * Time.deltaTime);
			}
		}
	}

	private void OnDisable()
	{
		heroSwap.ResetMaterials();
	}

	private void OnDestroy()
	{
		GameplayController.instance.OnReset -= HandleReset;
		PixelPlayerController.instance.OnReadyForGameplay -= handlePlayerReady;
		if (heroSwap != null && heroSwap.gameObject != null)
		{
			Object.Destroy(heroSwap.gameObject);
		}
		if (roomSwap != null && roomSwap.gameObject != null)
		{
			Object.Destroy(roomSwap.gameObject);
		}
		if (brainSwap != null && brainSwap.gameObject != null)
		{
			Object.Destroy(brainSwap.gameObject);
		}
		CancelInvoke(_repeatingMethod);
		StopCoroutine(_menuShowTimer);
	}

	private void HandleReset()
	{
		BloxelCamera.instance.activeController.SetCamerOffsetToPrevious();
		heroSwap.Reset();
		roomSwap.Reset();
		brainSwap.Reset();
	}

	public void HideForOverlay()
	{
		menu.transform.localScale = Vector3.zero;
		TryHideCaptureIndicators();
	}

	public void ShowAfterOverlay()
	{
		menu.transform.localScale = new Vector3(0.7f, 0.7f, 0.7f);
		TryShowCaptureIndicators();
	}

	public void TryHideCaptureIndicators()
	{
		heroSwap.transform.localScale = Vector3.zero;
		roomSwap.transform.localScale = Vector3.zero;
		brainSwap.transform.localScale = Vector3.zero;
	}

	public void TryShowCaptureIndicators()
	{
		heroSwap.transform.localScale = Vector3.one;
		roomSwap.transform.localScale = Vector3.one;
		brainSwap.transform.localScale = Vector3.one;
	}

	private FadeToBlack fadeOut()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(fadePrefab);
		return gameObject.GetComponent<FadeToBlack>();
	}

	private void warpTo(SceneName scene)
	{
		if (scene == SceneName.Viewer)
		{
			if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified)
			{
				BloxelsSceneManager.instance.ShowInternetMessage();
				GameHUD.instance.Reset(GameHUD.ResetMethod.FromCheckpoint);
				GameplayController.instance.ReplayFromCheckpiont();
				PixelPlayerController.instance.Reset();
				GameplayController.instance.heroObject.transform.localPosition = GameplayController.instance.lastCheckpoint;
				GameplayController.instance.StartDroppingCharacter();
				PixelPlayerController.instance.ResetInventoryFromCheckpoint();
				BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
				return;
			}
			if (AppStateManager.instance.outDatedVersion)
			{
				BloxelsSceneManager.instance.ShowVersionUpdateMessage();
				return;
			}
		}
		_initComplete = false;
		FadeToBlack fade = fadeOut();
		fade.Run().OnStart(delegate
		{
		}).OnComplete(delegate
		{
			BloxelsSceneManager.instance.GoTo(scene);
			fade.FadeToDestroy();
		});
	}

	private void handlePlayerReady()
	{
		PixelPlayerController.instance.OnReadyForGameplay -= handlePlayerReady;
		loadingText.enabled = false;
		rectLoading1.DOMoveX(-1000f, UIAnimationManager.speedSlow).SetEase(Ease.InExpo).OnStart(delegate
		{
			rectLoading2.DOMoveX(1000f, UIAnimationManager.speedSlow).SetEase(Ease.InExpo);
		})
			.OnComplete(delegate
			{
				if (!PrefsManager.instance.hasSeenHomeIntro)
				{
					menu.rectLogo.DOScale(1f, UIAnimationManager.speedMedium).OnStart(delegate
					{
						SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
					}).OnComplete(delegate
					{
						menu.rectLogo.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 0, 0f);
					});
				}
				setupHeroSwap();
				setupRoomSwap();
				setupBrainSwap();
				PrefsManager.instance.hasSeenHomeIntro = true;
				_initComplete = true;
				rectLoading1.localScale = Vector3.zero;
				rectLoading2.localScale = Vector3.zero;
			});
		if (AppStateManager.instance.attemptedViewerRefresh)
		{
			UIOverlayCanvas.instance.Popup(BloxelServerInterface.instance.socketErrorPrefab);
			BloxelServerInterface.instance.connectAttempts = 0;
			AppStateManager.instance.attemptedViewerRefresh = false;
		}
	}

	private void playerHitGround()
	{
		if (_playerHitGround)
		{
			return;
		}
		UINavMenu.instance.Open();
		_playerHitGround = true;
		menu.Init().OnComplete(delegate
		{
			menu.rectLogo.DOScale(0f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				startPlayerMovementCheck();
				showVirtualControls();
				heroSwap.HandleIndicatorPress(true);
			});
		});
	}

	private void showVirtualControls()
	{
		touchControls.SetActive(true);
		touchControls.transform.localScale = Vector3.one;
	}

	private void startPlayerMovementCheck()
	{
		InvokeRepeating(_repeatingMethod, 1f, 0.5f);
	}

	private void CheckPlayerMovement()
	{
		if (_initComplete && !(PixelPlayerController.instance == null))
		{
			_playerMoving = PixelPlayerController.instance.velocity.sqrMagnitude > 1f;
			if (!_playerMoving)
			{
				StartCoroutine(_menuShowTimer);
				return;
			}
			StopCoroutine(_menuShowTimer);
			showMenu(false);
		}
	}

	private IEnumerator menuShowTimer()
	{
		yield return new WaitForSeconds(1f);
		showMenu(true);
	}

	private void showMenu(bool shouldShow)
	{
		if (!shouldShow)
		{
			menu.Hide();
			hud.transform.localScale = Vector3.one;
		}
		else if (!heroSwapVisible)
		{
			menu.rectLogo.localScale = Vector3.zero;
			menu.Show();
			hud.transform.localScale = Vector3.zero;
		}
	}

	private void setupHeroSwap()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(heroSwapPrefab);
		heroSwap = gameObject.GetComponent<UIHeroSwap>();
	}

	private void setupRoomSwap()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(roomSwapPrefab);
		roomSwap = gameObject.GetComponent<UIRoomSwap>();
		roomSwap.SetPosition();
	}

	private void setupBrainSwap()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(brainSwapPrefab);
		brainSwap = gameObject.GetComponent<UIBrainSwap>();
		brainSwap.SetPosition();
	}

	private IEnumerator WaitForAssetManager()
	{
		while (AssetManager.instance.homeMiniGame == null)
		{
			yield return new WaitForEndOfFrame();
		}
		BloxelGame game2 = null;
		GridLocation correctRoom2 = GridLocation.InvalidLocation;
		GridLocation correctSpot2 = GridLocation.InvalidLocation;
		if (!PrefsManager.instance.hasSeenHomeIntro)
		{
			correctRoom2 = new GridLocation(6, 12);
			correctSpot2 = new GridLocation(6, 6);
		}
		else
		{
			correctRoom2 = new GridLocation(6, 9);
			correctSpot2 = new GridLocation(6, 2);
		}
		game2 = AssetManager.instance.homeMiniGame;
		game2.MoveHero(correctRoom2, correctSpot2);
		GameplayController.instance.LoadGame(game2, true, false, true);
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.None);
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		GameplayBuilder.instance.StartStreaming(GameplayController.instance.currentGame);
		GameplayBuilder.instance.InitWorldTileData();
		if (!hud.gameObject.activeInHierarchy)
		{
			hud.transform.parent.gameObject.SetActive(true);
		}
		hud.transform.localScale = Vector3.zero;
		if (BloxelLibrary.instance != null && BloxelLibrary.instance.gameObject != null && BloxelLibrary.instance.gameObject.activeInHierarchy)
		{
			BloxelLibrary.instance.Hide();
		}
		PixelPlayerController.instance.OnReadyForGameplay += handlePlayerReady;
	}
}
