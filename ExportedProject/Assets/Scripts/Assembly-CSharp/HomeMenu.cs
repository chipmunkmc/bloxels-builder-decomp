using System;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class HomeMenu : MonoBehaviour
{
	public delegate void HandleVisibility(bool _isVisible);

	[Header("UI")]
	public Canvas canvas;

	public RectTransform rectTopBar;

	public Vector2 topBarVisible;

	public Vector2 topBarHidden;

	public RectTransform rectLogo;

	public Vector2 logoVisible;

	public Vector2 logoHidden;

	public RectTransform rectAccount;

	public RectTransform rectMenu;

	public TextMeshProUGUI _uiTextGemCount;

	public TextMeshProUGUI _uiTextCoinCount;

	public RectTransform rectBanks;

	private UIButton _uiButtonUserAccount;

	public Image uiImageUserIcon;

	public RawImage uiRawImageAvatar;

	private CoverBoard _cover;

	private Vector3 _rectPlayStartScale;

	private Vector3 _rectWallStartScale;

	private Vector3 _rectCreateStartScale;

	private Vector3 _rectAccountStartScale;

	private Vector3 _rectMenuStartScale;

	[Header("Animations")]
	public Tweener tweenShow;

	public Tweener tweenHide;

	[Header("State Tracking")]
	public bool isVisible;

	public event HandleVisibility OnChange;

	private void Awake()
	{
		_uiButtonUserAccount = rectAccount.GetComponent<UIButton>();
		_cover = new CoverBoard(Color.clear);
		ResetUI();
	}

	private void Start()
	{
		canvas.worldCamera = UIOverlayCanvas.instance.overlayCamera;
		_uiButtonUserAccount.OnClick += UserAccountPressed;
		PlayerBankManager.instance.OnCoinUpdate += HandleCoinUpdate;
		PlayerBankManager.instance.OnGemUpdate += HandleGemUpdate;
		CurrentUser.instance.OnUpdate += UpdateUserMenu;
	}

	private void OnDestroy()
	{
		_uiButtonUserAccount.OnClick -= UserAccountPressed;
		unload();
		PlayerBankManager.instance.OnCoinUpdate -= HandleCoinUpdate;
		PlayerBankManager.instance.OnGemUpdate -= HandleGemUpdate;
		CurrentUser.instance.OnUpdate -= UpdateUserMenu;
	}

	private void unload()
	{
		rectTopBar = null;
		rectLogo = null;
		rectAccount = null;
		rectMenu = null;
		_uiTextGemCount = null;
		_uiTextCoinCount = null;
		rectBanks = null;
		_uiButtonUserAccount = null;
		uiImageUserIcon = null;
		uiRawImageAvatar.texture = null;
		uiRawImageAvatar = null;
		tweenShow = null;
		tweenHide = null;
	}

	private void ResetUI()
	{
		_rectAccountStartScale = rectAccount.localScale;
		_rectMenuStartScale = rectMenu.localScale;
		rectAccount.localScale = Vector3.zero;
		rectMenu.localScale = Vector3.zero;
		rectLogo.localScale = Vector3.zero;
		rectBanks.localScale = Vector3.zero;
		CheckCurrentUserLogin();
	}

	public Sequence Init()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(rectAccount.DOScale(_rectAccountStartScale, UIAnimationManager.speedFast).SetEase(Ease.InOutBounce).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.megaTileUnlock);
		}));
		sequence.Append(rectMenu.DOScale(_rectMenuStartScale, UIAnimationManager.speedFast).SetEase(Ease.InOutBounce).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.megaTileUnlock);
		}));
		sequence.Play();
		return sequence;
	}

	private void CreatePressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
		BloxelsSceneManager.instance.GoTo(SceneName.PixelEditor_iPad);
	}

	private void InfinityWallPressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
		if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified)
		{
			BloxelsSceneManager.instance.ShowInternetMessage();
		}
		else if (AppStateManager.instance.outDatedVersion)
		{
			BloxelsSceneManager.instance.ShowVersionUpdateMessage();
		}
		else
		{
			BloxelsSceneManager.instance.GoTo(SceneName.Viewer);
		}
	}

	private void PlayGamePressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
		AppStateManager.instance.loadedGame = AssetManager.instance.poultryPanicGame;
		BloxelsSceneManager.instance.GoTo(SceneName.Gameplay);
	}

	private void CheckCurrentUserLogin()
	{
		if (CurrentUser.instance.account != null)
		{
			if (CurrentUser.instance.account.avatar != null)
			{
				ShowAvatar(true);
				uiRawImageAvatar.texture = CurrentUser.instance.account.avatar.UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
			}
			else
			{
				ShowAvatar(false);
			}
			rectBanks.localScale = Vector3.one;
			_uiTextGemCount.SetText(PlayerBankManager.instance.GetGemCount().ToString());
			_uiTextCoinCount.SetText(PlayerBankManager.instance.GetCoinCount().ToString());
		}
	}

	private void UserAccountPressed()
	{
		UINavMenu.instance.UserAction();
	}

	private void UpdateUserMenu(UserAccount acct)
	{
		if (acct == null || (acct != null && acct.avatar == null))
		{
			ShowAvatar(false);
			rectBanks.localScale = Vector3.zero;
			return;
		}
		uiRawImageAvatar.texture = acct.avatar.UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
		ShowAvatar(true);
		rectBanks.localScale = Vector3.one;
		_uiTextGemCount.SetText(PlayerBankManager.instance.GetGemCount().ToString());
		_uiTextCoinCount.SetText(PlayerBankManager.instance.GetCoinCount().ToString());
	}

	private void HandleGemUpdate(int gems)
	{
		_uiTextGemCount.SetText(gems.ToString());
	}

	private void HandleCoinUpdate(int coins)
	{
		_uiTextCoinCount.SetText(coins.ToString());
	}

	public void Show()
	{
		if (isVisible)
		{
			return;
		}
		isVisible = true;
		tweenHide.Kill();
		tweenShow = rectTopBar.DOAnchorPos(topBarVisible, UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
		})
			.OnComplete(delegate
			{
				rectLogo.DOAnchorPos(logoVisible, UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnStart(delegate
				{
					SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
				});
				if (this.OnChange != null)
				{
					this.OnChange(isVisible);
				}
			});
	}

	public void Hide()
	{
		if (!isVisible)
		{
			return;
		}
		isVisible = false;
		tweenShow.Kill();
		tweenHide = rectTopBar.DOAnchorPos(topBarHidden, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
		})
			.OnComplete(delegate
			{
				rectLogo.DOAnchorPos(logoHidden, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnStart(delegate
				{
					SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
				});
				if (this.OnChange != null)
				{
					this.OnChange(isVisible);
				}
			});
	}

	public void ShowAvatar(bool isOn)
	{
		if (isOn)
		{
			uiImageUserIcon.transform.localScale = Vector3.zero;
			uiRawImageAvatar.transform.localScale = Vector3.one;
			uiRawImageAvatar.color = Color.white;
		}
		else
		{
			uiImageUserIcon.transform.localScale = Vector3.one;
			uiRawImageAvatar.transform.localScale = Vector3.zero;
		}
	}
}
