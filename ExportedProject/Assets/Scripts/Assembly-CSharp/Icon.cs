using System;
using System.IO;
using System.Text;
using Newtonsoft.Json;
using UnityEngine;

public class Icon
{
	public GridLocation[] paletteCoordinates;

	private static string metadataExtension = ".iconData";

	public int iconDimensionX { get; private set; }

	public int iconDimensionY { get; private set; }

	public Icon(int xSize, int ySize)
	{
		iconDimensionX = xSize;
		iconDimensionY = ySize;
		paletteCoordinates = new GridLocation[iconDimensionX * iconDimensionY];
		for (int i = 0; i < iconDimensionY; i++)
		{
			for (int j = 0; j < iconDimensionX; j++)
			{
				paletteCoordinates[i * iconDimensionX + j] = GridLocation.InvalidLocation;
			}
		}
	}

	public void SaveToJson(string fileName, out bool createdNewFile)
	{
		createdNewFile = false;
		StringBuilder sb = new StringBuilder();
		StringWriter stringWriter = new StringWriter(sb);
		JsonWriter jsonWriter = new JsonTextWriter(stringWriter);
		jsonWriter.WriteStartObject();
		jsonWriter.WritePropertyName("width");
		jsonWriter.WriteValue(iconDimensionX);
		jsonWriter.WritePropertyName("height");
		jsonWriter.WriteValue(iconDimensionY);
		jsonWriter.WritePropertyName("paletteCoordinates");
		jsonWriter.WriteStartArray();
		for (int i = 0; i < paletteCoordinates.Length; i++)
		{
			GridLocation gridLocation = paletteCoordinates[i];
			jsonWriter.WriteValue(gridLocation.c);
			jsonWriter.WriteValue(gridLocation.r);
		}
		jsonWriter.WriteEndArray();
		jsonWriter.WriteEndObject();
		string contents = stringWriter.ToString();
		jsonWriter.Close();
		stringWriter.Close();
		string text = Application.persistentDataPath + IconEditor.iconDirectoryPath;
		if (!Directory.Exists(text))
		{
			Directory.CreateDirectory(text);
		}
		string path = text + fileName + ".json";
		if (!File.Exists(path))
		{
			createdNewFile = true;
		}
		File.WriteAllText(path, contents);
	}

	public static Icon LoadFromJson(string fileName)
	{
		string text = Application.persistentDataPath + IconEditor.iconDirectoryPath;
		if (!Directory.Exists(text))
		{
			return null;
		}
		string path = text + fileName + ".json";
		string text2 = File.ReadAllText(path);
		if (string.IsNullOrEmpty(text2))
		{
			return null;
		}
		Icon icon = null;
		int xSize = 0;
		int ySize = 0;
		JsonTextReader jsonTextReader = new JsonTextReader(new StringReader(text2));
		string text3 = string.Empty;
		while (jsonTextReader.Read())
		{
			if (jsonTextReader.Value != null)
			{
				if (jsonTextReader.TokenType == JsonToken.PropertyName)
				{
					text3 = jsonTextReader.Value.ToString();
					if (!(text3 == "paletteCoordinates"))
					{
						continue;
					}
					icon = new Icon(xSize, ySize);
					GridLocation invalidLocation = GridLocation.InvalidLocation;
					int num = 0;
					while (jsonTextReader.Read())
					{
						if (jsonTextReader.Value != null)
						{
							invalidLocation.c = int.Parse(jsonTextReader.Value.ToString());
							jsonTextReader.Read();
							invalidLocation.r = int.Parse(jsonTextReader.Value.ToString());
							icon.paletteCoordinates[num++] = invalidLocation;
						}
						else if (jsonTextReader.TokenType == JsonToken.EndArray)
						{
							break;
						}
					}
				}
				else
				{
					switch (text3)
					{
					case "width":
						xSize = int.Parse(jsonTextReader.Value.ToString());
						break;
					case "height":
						ySize = int.Parse(jsonTextReader.Value.ToString());
						break;
					}
				}
			}
			else if (jsonTextReader.TokenType != JsonToken.EndObject)
			{
			}
		}
		return icon;
	}

	public void SaveToBinary(string fileName)
	{
		string text = Application.persistentDataPath + IconEditor.iconDirectoryPath;
		if (!Directory.Exists(text))
		{
			Directory.CreateDirectory(text);
		}
		string path = text + fileName + metadataExtension;
		using (BinaryWriter binaryWriter = new BinaryWriter(File.Open(path, FileMode.Create)))
		{
			binaryWriter.Write(iconDimensionX);
			binaryWriter.Write(iconDimensionY);
			for (int i = 0; i < paletteCoordinates.Length; i++)
			{
				GridLocation gridLocation = paletteCoordinates[i];
				binaryWriter.Write(gridLocation.c);
				binaryWriter.Write(gridLocation.r);
			}
		}
	}

	public static Icon LoadFromBinary(string fileName)
	{
		string text = Application.persistentDataPath + IconEditor.iconDirectoryPath;
		if (!Directory.Exists(text))
		{
			return null;
		}
		string path = text + fileName + metadataExtension;
		Icon icon = null;
		using (FileStream input = File.OpenRead(path))
		{
			BinaryReader binaryReader = new BinaryReader(input);
			int num = 0;
			int num2 = 0;
			int num3 = 0;
			try
			{
				num = binaryReader.ReadInt32();
				num2 = binaryReader.ReadInt32();
			}
			catch (Exception)
			{
				return null;
			}
			icon = new Icon(num, num2);
			try
			{
				while (num3 < num * num2)
				{
					int col = binaryReader.ReadInt32();
					int row = binaryReader.ReadInt32();
					GridLocation gridLocation = new GridLocation(col, row);
					icon.paletteCoordinates[num3++] = gridLocation;
				}
			}
			catch (Exception)
			{
			}
		}
		return icon;
	}
}
