using System.Diagnostics;
using System.IO;
using UnityEngine;

public class IconCamera2 : MonoBehaviour
{
	private Camera cam;

	public bool shouldSaveThisFrame;

	private Texture2D iconTexture;

	public string pngFile;

	private void Awake()
	{
		cam = GetComponent<Camera>();
	}

	private void Update()
	{
		cam.Render();
	}

	private void OnPostRender()
	{
		if (shouldSaveThisFrame)
		{
			int renderTextureSize = IconEditor.instance.renderTextureSize;
			if (iconTexture == null || iconTexture.width != renderTextureSize)
			{
				iconTexture = new Texture2D(renderTextureSize, renderTextureSize, TextureFormat.ARGB32, false);
			}
			iconTexture.ReadPixels(new Rect(0f, 0f, renderTextureSize, renderTextureSize), 0, 0, false);
			iconTexture.Apply();
			byte[] bytes = iconTexture.EncodeToPNG();
			string text = Application.persistentDataPath + IconEditor.iconDirectoryPath;
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			pngFile = text + IconEditor.instance.iconFileName + ".png";
			File.WriteAllBytes(pngFile, bytes);
			Process.Start("open", pngFile);
			shouldSaveThisFrame = false;
		}
	}
}
