using System.Collections.Generic;
using UnityEngine;
using VoxelEngine;

public class IconChunk2DPool : MonoBehaviour
{
	public static IconChunk2DPool instance;

	public Transform selfTransform;

	public int poolSize;

	public int poolExpansionSize;

	private static bool _InitComplete;

	public Stack<IconChunk2D> pool { get; private set; }

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			instance = this;
			_InitComplete = true;
			Object.DontDestroyOnLoad(base.gameObject);
			selfTransform = base.transform;
			Initialize();
		}
	}

	private void Initialize()
	{
		pool = new Stack<IconChunk2D>(poolSize);
		AllocatePoolObjects();
	}

	private void AllocatePoolObjects()
	{
		for (int i = 0; i < poolSize; i++)
		{
			IconChunk2D iconChunk2D = new IconChunk2D();
			iconChunk2D.EarlyAwake();
			pool.Push(iconChunk2D);
		}
	}

	public IconChunk2D Spawn()
	{
		if (pool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				IconChunk2D iconChunk2D = new IconChunk2D();
				iconChunk2D.EarlyAwake();
				pool.Push(iconChunk2D);
			}
		}
		IconChunk2D iconChunk2D2 = pool.Pop();
		iconChunk2D2.PrepareSpawn();
		return iconChunk2D2;
	}
}
