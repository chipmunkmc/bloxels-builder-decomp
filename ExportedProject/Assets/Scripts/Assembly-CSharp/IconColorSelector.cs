using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class IconColorSelector : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public static IconColorSelector instance;

	private RawImage uiImage;

	private RectTransform selfRectTransform;

	private RectTransform indicatorRect;

	public Camera uiCamera;

	public Button eraserButton;

	public GridLocation currentPaletteChoice { get; private set; }

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		selfRectTransform = GetComponent<RectTransform>();
		uiImage = GetComponent<RawImage>();
		indicatorRect = selfRectTransform.GetChild(0).GetComponent<RectTransform>();
		eraserButton.onClick.AddListener(SelectEraser);
	}

	private void Start()
	{
		uiImage.texture = ColorPalette.instance.paletteColorReference;
	}

	public void SelectEraser()
	{
		currentPaletteChoice = GridLocation.InvalidLocation;
		indicatorRect.gameObject.SetActive(false);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		Vector2 localPoint = Vector2.zero;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(selfRectTransform, eventData.position, uiCamera, out localPoint);
		localPoint.Set(localPoint.x + selfRectTransform.sizeDelta.x / 2f, localPoint.y + selfRectTransform.sizeDelta.y / 2f);
		currentPaletteChoice = new GridLocation(Mathf.FloorToInt(localPoint.x / (selfRectTransform.sizeDelta.x / 8f)), Mathf.FloorToInt(localPoint.y / (selfRectTransform.sizeDelta.y / 8f)));
		if (!indicatorRect.gameObject.activeInHierarchy)
		{
			indicatorRect.gameObject.SetActive(true);
		}
		indicatorRect.anchoredPosition = new Vector2((float)currentPaletteChoice.c * (selfRectTransform.sizeDelta.x / 8f) + selfRectTransform.sizeDelta.x / 16f, (float)currentPaletteChoice.r * (selfRectTransform.sizeDelta.y / 8f) + selfRectTransform.sizeDelta.y / 16f);
	}
}
