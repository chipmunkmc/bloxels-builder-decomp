using System.Collections.Generic;
using System.IO;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using VoxelEngine;

public class IconEditor : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	public static IconEditor instance;

	private RectTransform _selfRectTransform;

	public Object gridLinePrefab;

	public GameObject gridLinesContainer;

	private UILineRenderer[] _gridLines;

	public Color gridLineColor;

	public Color boundaryGridColor;

	public RawImage uiImage;

	public RawImage previewImage;

	public Dropdown uiDropdown;

	public Button loadButton;

	public InputField fileNameField;

	public Button exportButton;

	public Button clearButton;

	public Button uiButtonUndo;

	public Button uiButton13BitToggle;

	public Image toggle13BitIcon;

	public Sprite spriteToggle13BitOn;

	public Sprite spriteToggle13BitOff;

	public Camera uiCamera;

	public Camera iconCamera;

	public IconCamera2 iconCameraController;

	private const int iconSize = 26;

	private IconChunk2D _iconChunk;

	private GameObject _iconGameObject;

	private MeshFilter _iconMeshFilter;

	private bool isDragging;

	private GridLocation currentLocation = GridLocation.InvalidLocation;

	private RenderTexture _renderTex;

	public int renderTextureSize = 3840;

	public static string iconDirectoryPath = "/Icons/";

	public RectTransform guidesContainer;

	[Header("State Tracking")]
	public Stack<Icon> iconStack = new Stack<Icon>();

	private bool _mode13Bit;

	public string iconFileName { get; private set; }

	public bool mode13Bit
	{
		get
		{
			return _mode13Bit;
		}
		set
		{
			_mode13Bit = value;
			if (iconCamera != null)
			{
				iconCamera.orthographicSize = ((!_mode13Bit) ? 13f : 6.5f);
				iconCamera.transform.localPosition = ((!_mode13Bit) ? new Vector3(-0.5f, -0.5f, -10f) : new Vector3(-7f, -7f, -10f));
			}
		}
	}

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		_selfRectTransform = GetComponent<RectTransform>();
		uiImage = GetComponent<RawImage>();
		_renderTex = new RenderTexture(renderTextureSize, renderTextureSize, 0, RenderTextureFormat.ARGB32);
		iconCamera.targetTexture = _renderTex;
		uiImage.texture = _renderTex;
		previewImage.texture = _renderTex;
		iconCameraController = iconCamera.GetComponent<IconCamera2>();
		exportButton.onClick.AddListener(Save);
		clearButton.onClick.AddListener(Clear);
		loadButton.onClick.AddListener(LoadSelectedDropdownIcon);
		uiButtonUndo.onClick.AddListener(Undo);
		uiButton13BitToggle.onClick.AddListener(Toggle13Bit);
		InitDropdown();
		iconCamera.orthographicSize = ((!mode13Bit) ? 13f : 6.5f);
	}

	private void Start()
	{
		Icon data = new Icon(26, 26);
		_iconChunk = IconChunk2DPool.instance.Spawn();
		_iconGameObject = new GameObject("3D Icon");
		_iconGameObject.layer = LayerMask.NameToLayer("Voxel Mesh");
		_iconMeshFilter = _iconGameObject.AddComponent<MeshFilter>();
		_iconChunk.mesh = new Mesh();
		_iconMeshFilter.mesh = _iconChunk.mesh;
		_iconChunk.InitWithIcon(data);
		_iconGameObject.AddComponent<MeshRenderer>().material = ColorPalette.instance.defaultTileMaterial;
		_iconGameObject.transform.position = new Vector3(-13f, -13f, 0f);
		_iconGameObject.transform.localScale = Vector3.one;
		BuildLines(26);
	}

	private void Update()
	{
		if ((Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand)) && Input.GetKeyDown(KeyCode.Z))
		{
			Undo();
		}
		if ((Input.GetKey(KeyCode.LeftCommand) || Input.GetKey(KeyCode.RightCommand)) && Input.GetKeyDown(KeyCode.S))
		{
			Save();
		}
		if (Input.GetKeyDown(KeyCode.UpArrow))
		{
			ShiftBlocksUp();
		}
		if (Input.GetKeyDown(KeyCode.DownArrow))
		{
			ShiftBlocksDown();
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow))
		{
			ShiftBlocksLeft();
		}
		if (Input.GetKeyDown(KeyCode.RightArrow))
		{
			ShiftBlocksRight();
		}
	}

	private void Toggle13Bit()
	{
		if (mode13Bit)
		{
			BuildLines(26);
			toggle13BitIcon.sprite = spriteToggle13BitOn;
		}
		else
		{
			BuildLines(13);
			toggle13BitIcon.sprite = spriteToggle13BitOff;
		}
		mode13Bit = !mode13Bit;
	}

	private void Undo()
	{
		Icon data = iconStack.Pop();
		_iconChunk.InitWithIcon(data);
	}

	private void InitDropdown()
	{
		string[] array = GetSavedIconPaths();
		if (array == null)
		{
			array = new string[0];
		}
		List<Dropdown.OptionData> list = new List<Dropdown.OptionData>();
		for (int i = 0; i < array.Length; i++)
		{
			string[] array2 = array[i].Split('/');
			string[] array3 = array2[array2.Length - 1].Split('.');
			list.Add(new Dropdown.OptionData(array3[0]));
		}
		uiDropdown.options = list;
	}

	public void LoadSelectedDropdownIcon()
	{
		LoadIconFromJSON(uiDropdown.captionText.text);
		fileNameField.text = uiDropdown.captionText.text;
		iconFileName = uiDropdown.captionText.text;
	}

	public string[] GetSavedIconPaths()
	{
		string path = Application.persistentDataPath + iconDirectoryPath;
		if (Directory.Exists(path))
		{
			return Directory.GetFiles(path, "*.json");
		}
		Directory.CreateDirectory(path);
		return null;
	}

	public void Clear()
	{
		SaveState();
		fileNameField.text = string.Empty;
		_iconChunk.InitWithIcon(new Icon(26, 26));
	}

	public void Save()
	{
		if (fileNameField.text == string.Empty)
		{
			fileNameField.transform.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedMedium);
			return;
		}
		iconFileName = fileNameField.text;
		iconCameraController.shouldSaveThisFrame = true;
		SaveIconToJSON(iconFileName);
	}

	public void SaveIconToJSON(string fileName)
	{
		bool createdNewFile = false;
		_iconChunk.iconDataModel.SaveToJson(iconFileName, out createdNewFile);
		if (createdNewFile)
		{
			InitDropdown();
		}
		for (int i = 0; i < uiDropdown.options.Count; i++)
		{
			if (uiDropdown.options[i].text == iconFileName)
			{
				uiDropdown.value = i;
				break;
			}
		}
	}

	public void LoadIconFromJSON(string fileName)
	{
		Icon icon = Icon.LoadFromJson(fileName);
		if (icon != null)
		{
			_iconChunk.InitWithIcon(icon);
		}
	}

	public void LoadIconFromBinary(string fileName)
	{
		Icon icon = Icon.LoadFromBinary(fileName);
		if (icon != null)
		{
			_iconChunk.InitWithIcon(icon);
		}
	}

	private void SaveState()
	{
		Icon icon = new Icon(_iconChunk.iconDataModel.iconDimensionX, _iconChunk.iconDataModel.iconDimensionY);
		icon.paletteCoordinates = (GridLocation[])_iconChunk.iconDataModel.paletteCoordinates.Clone();
		iconStack.Push(icon);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (CalculateCurrentBlockLocation(eventData.position))
		{
			SaveState();
			UpdateIconAtCurrentLocation();
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (CalculateCurrentBlockLocation(eventData.position))
		{
			SaveState();
			UpdateIconAtCurrentLocation();
			isDragging = true;
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (CalculateCurrentBlockLocation(eventData.position))
		{
			UpdateIconAtCurrentLocation();
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		isDragging = false;
		if (CalculateCurrentBlockLocation(eventData.position))
		{
			UpdateIconAtCurrentLocation();
		}
	}

	private bool CalculateCurrentBlockLocation(Vector2 inputPos)
	{
		Vector2 localPoint = Vector2.zero;
		RectTransformUtility.ScreenPointToLocalPointInRectangle(_selfRectTransform, inputPos, uiCamera, out localPoint);
		localPoint.Set(localPoint.x + _selfRectTransform.sizeDelta.x / 2f, localPoint.y + _selfRectTransform.sizeDelta.y / 2f);
		GridLocation gridLocation = (currentLocation = new GridLocation(Mathf.FloorToInt(localPoint.x / (_selfRectTransform.sizeDelta.x / 26f)), Mathf.FloorToInt(localPoint.y / (_selfRectTransform.sizeDelta.y / 26f))));
		if (gridLocation.c < 0 || gridLocation.c >= 26)
		{
			return false;
		}
		if (gridLocation.r < 0 || gridLocation.r >= 26)
		{
			return false;
		}
		return true;
	}

	private void UpdateIconAtCurrentLocation()
	{
		_iconChunk.UpdateBlockAtLocation(currentLocation.c, currentLocation.r, IconColorSelector.instance.currentPaletteChoice);
	}

	private void BuildLines(int blockSpan)
	{
		foreach (Transform item in gridLinesContainer.transform)
		{
			Object.Destroy(item.gameObject);
		}
		int num = blockSpan + 1;
		int num2 = blockSpan + 1;
		float num3 = _selfRectTransform.sizeDelta.x / (float)blockSpan;
		_gridLines = new UILineRenderer[num + num2];
		int num4 = 0;
		for (int i = 0; i < num; i++)
		{
			GameObject gameObject = Object.Instantiate(gridLinePrefab) as GameObject;
			RectTransform component = gameObject.GetComponent<RectTransform>();
			UILineRenderer component2 = gameObject.GetComponent<UILineRenderer>();
			gameObject.transform.SetParent(gridLinesContainer.transform);
			gameObject.transform.localScale = Vector3.one;
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = Quaternion.identity;
			component.sizeDelta = new Vector2(2f, (float)blockSpan * num3);
			component.anchoredPosition = new Vector2((float)i * num3, 0f);
			component2.Points = new Vector2[2]
			{
				Vector2.zero,
				new Vector2(0f, 1f)
			};
			if (i % 13 == 0)
			{
				component2.color = boundaryGridColor;
			}
			else
			{
				component2.color = gridLineColor;
			}
			_gridLines[num4] = component2;
			num4++;
		}
		for (int j = 0; j < num2; j++)
		{
			GameObject gameObject2 = Object.Instantiate(gridLinePrefab) as GameObject;
			RectTransform component3 = gameObject2.GetComponent<RectTransform>();
			UILineRenderer component4 = gameObject2.GetComponent<UILineRenderer>();
			gameObject2.transform.SetParent(gridLinesContainer.transform);
			gameObject2.transform.localScale = Vector3.one;
			gameObject2.transform.localPosition = Vector3.zero;
			gameObject2.transform.localRotation = Quaternion.identity;
			component3.sizeDelta = new Vector2((float)blockSpan * num3, 2f);
			component3.anchoredPosition = new Vector2(0f, (float)j * num3);
			component4.Points = new Vector2[2]
			{
				Vector2.zero,
				new Vector2(1f, 0f)
			};
			if (j % 13 == 0)
			{
				component4.color = boundaryGridColor;
			}
			else
			{
				component4.color = gridLineColor;
			}
			_gridLines[num4] = component4;
			num4++;
		}
	}

	public void ColorGridLines(Color color)
	{
		for (int i = 0; i < _gridLines.Length; i++)
		{
			_gridLines[i].color = color;
		}
	}

	public void ShiftBlocksRight()
	{
		SaveState();
		Icon iconDataModel = _iconChunk.iconDataModel;
		for (int i = 0; i < iconDataModel.iconDimensionY; i++)
		{
			int num = i * iconDataModel.iconDimensionX;
			for (int num2 = iconDataModel.iconDimensionX - 1; num2 > 0; num2--)
			{
				iconDataModel.paletteCoordinates[num + num2] = iconDataModel.paletteCoordinates[num + num2 - 1];
			}
			iconDataModel.paletteCoordinates[num] = GridLocation.InvalidLocation;
		}
		_iconChunk.InitWithIcon(iconDataModel);
	}

	public void ShiftBlocksLeft()
	{
		SaveState();
		Icon iconDataModel = _iconChunk.iconDataModel;
		for (int i = 0; i < iconDataModel.iconDimensionY; i++)
		{
			int num = i * iconDataModel.iconDimensionX;
			for (int j = 0; j < iconDataModel.iconDimensionX - 1; j++)
			{
				iconDataModel.paletteCoordinates[num + j] = iconDataModel.paletteCoordinates[num + j + 1];
			}
			iconDataModel.paletteCoordinates[num + iconDataModel.iconDimensionX - 1] = GridLocation.InvalidLocation;
		}
		_iconChunk.InitWithIcon(iconDataModel);
	}

	public void ShiftBlocksUp()
	{
		SaveState();
		Icon iconDataModel = _iconChunk.iconDataModel;
		for (int num = iconDataModel.paletteCoordinates.Length - 1; num >= iconDataModel.iconDimensionX; num--)
		{
			iconDataModel.paletteCoordinates[num] = iconDataModel.paletteCoordinates[num - iconDataModel.iconDimensionX];
		}
		for (int i = 0; i < iconDataModel.iconDimensionX; i++)
		{
			iconDataModel.paletteCoordinates[i] = GridLocation.InvalidLocation;
		}
		_iconChunk.InitWithIcon(iconDataModel);
	}

	public void ShiftBlocksDown()
	{
		SaveState();
		Icon iconDataModel = _iconChunk.iconDataModel;
		for (int i = 0; i < iconDataModel.paletteCoordinates.Length - iconDataModel.iconDimensionX; i++)
		{
			iconDataModel.paletteCoordinates[i] = iconDataModel.paletteCoordinates[i + iconDataModel.iconDimensionX];
		}
		for (int j = iconDataModel.paletteCoordinates.Length - iconDataModel.iconDimensionX; j < iconDataModel.paletteCoordinates.Length; j++)
		{
			iconDataModel.paletteCoordinates[j] = GridLocation.InvalidLocation;
		}
		_iconChunk.InitWithIcon(iconDataModel);
	}
}
