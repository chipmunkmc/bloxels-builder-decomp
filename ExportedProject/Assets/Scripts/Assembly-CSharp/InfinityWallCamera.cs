using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;
using UnityStandardAssets.ImageEffects;

public class InfinityWallCamera : MonoBehaviour
{
	public static InfinityWallCamera instance;

	private bool shouldResetInstanceOnDestroy;

	private BlurOptimized blur;

	public Tweener blurTween;

	public Tweener deBlurTween;

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		shouldResetInstanceOnDestroy = true;
		blur = GetComponent<BlurOptimized>();
		blurTween = null;
		deBlurTween = null;
		ResetBlur();
	}

	private void OnDestroy()
	{
		if (shouldResetInstanceOnDestroy)
		{
			instance = null;
		}
	}

	private void ResetBlur()
	{
		if (DeviceManager.isPoopDevice)
		{
			Object.Destroy(blur);
			return;
		}
		blur.enabled = false;
		blur.downsample = 0;
		blur.blurSize = 0f;
	}

	public Tweener Blur(Graphic graphic)
	{
		if (DeviceManager.isPoopDevice)
		{
			return null;
		}
		if (deBlurTween != null && deBlurTween.IsPlaying())
		{
			deBlurTween.Kill();
		}
		graphic.color = new Color(0f, 0f, 0f, 0f);
		blurTween = graphic.DOFade(0.35f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			blur.enabled = true;
			DOTween.To(() => blur.blurSize, delegate(float x)
			{
				blur.blurSize = x;
			}, 3f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				blur.downsample = 2;
			});
		});
		return blurTween;
	}

	public Tweener DeBlur(Graphic graphic)
	{
		if (DeviceManager.isPoopDevice)
		{
			return null;
		}
		if (blurTween != null && blurTween.IsPlaying())
		{
			blurTween.Kill();
		}
		deBlurTween = graphic.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			DOTween.To(() => blur.blurSize, delegate(float x)
			{
				blur.blurSize = x;
			}, 0f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				blur.downsample = 0;
			}).OnComplete(delegate
			{
				blur.enabled = false;
			});
		});
		return deBlurTween;
	}
}
