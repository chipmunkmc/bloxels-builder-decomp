using System;
using UnityEngine;

public class InventoryController : MonoBehaviour
{
	public InventoryData currentAction;

	public InventoryData currentMovement;

	public InventoryData[] inventory;

	public static InventoryData checkpointAction;

	public static InventoryData checkpointMovement;

	public static int[] checkpointInventoryQuantities;

	public static int numberOfInventoryTypes;

	public void Awake()
	{
		InitInventory();
	}

	public void ClearInventoryHUD()
	{
		GameHUD.instance.DeactivateActionHUD();
		GameHUD.instance.DeactivateMovementHUD();
		currentAction = null;
		currentMovement = null;
	}

	private void InitInventory()
	{
		Array values = Enum.GetValues(typeof(InventoryData.Type));
		numberOfInventoryTypes = values.Length;
		inventory = new InventoryData[numberOfInventoryTypes];
		inventory[0] = new InventoryData(InventoryData.Type.Bomb, InventoryData.DisplayType.Text, InventoryData.Behavior.Action, 5, 1, 10000);
		inventory[1] = new InventoryData(InventoryData.Type.Jetpack, InventoryData.DisplayType.Elements, InventoryData.Behavior.Movement, 3, 1, 3);
		inventory[3] = new InventoryData(InventoryData.Type.Map, InventoryData.DisplayType.None, InventoryData.Behavior.Persistent, 1, 1, 1);
		inventory[2] = new InventoryData(InventoryData.Type.Shrink, InventoryData.DisplayType.Elements, InventoryData.Behavior.Action, 3, 1, 3);
		checkpointInventoryQuantities = new int[inventory.Length];
	}

	public bool HasItem(InventoryData.Type type)
	{
		return inventory[(int)type].available;
	}

	public bool ItemIsEquipped(InventoryData.Type type)
	{
		if (currentAction != null && currentAction.type == type)
		{
			return true;
		}
		if (currentMovement != null && currentMovement.type == type)
		{
			return true;
		}
		InventoryData inventoryData = inventory[(int)type];
		if (inventoryData.behavior == InventoryData.Behavior.Persistent && inventoryData.available)
		{
			return true;
		}
		return false;
	}

	public void AssignItem(InventoryData.Type type)
	{
		InventoryData inventoryData = inventory[(int)type];
		switch (inventoryData.behavior)
		{
		case InventoryData.Behavior.Action:
			AssignItemToActionButton(type);
			break;
		case InventoryData.Behavior.Movement:
			AssignItemToMovementButton(type);
			break;
		}
	}

	public void UnassignItem(InventoryData.Type type, bool updateGameplayHUD = true)
	{
		InventoryData inventoryData = inventory[(int)type];
		if (!updateGameplayHUD)
		{
			return;
		}
		switch (inventoryData.behavior)
		{
		case InventoryData.Behavior.Action:
			if (currentAction != null && currentAction.type == type)
			{
				GameHUD.instance.DeactivateActionHUD();
				currentAction = null;
			}
			break;
		case InventoryData.Behavior.Movement:
			if (currentMovement != null && currentMovement.type == type)
			{
				GameHUD.instance.DeactivateMovementHUD();
				currentMovement = null;
			}
			break;
		}
	}

	public void AssignItemToActionButton(InventoryData.Type type)
	{
		InventoryData inventoryData = inventory[(int)type];
		if (inventoryData.behavior == InventoryData.Behavior.Action)
		{
			currentAction = inventoryData;
			GameHUD.instance.InitActionHUDItem(currentAction);
		}
	}

	public void AssignItemToMovementButton(InventoryData.Type type)
	{
		InventoryData inventoryData = inventory[(int)type];
		if (inventoryData.behavior == InventoryData.Behavior.Movement)
		{
			currentMovement = inventoryData;
			GameHUD.instance.InitMovementHUDItem(currentMovement);
		}
	}

	public void AddToInventory(InventoryData.Type type)
	{
		InventoryData inventoryData = inventory[(int)type];
		inventoryData.Increment();
		if (currentAction == null && inventoryData.behavior == InventoryData.Behavior.Action)
		{
			AssignItemToActionButton(type);
		}
		else if (currentMovement == null && inventoryData.behavior == InventoryData.Behavior.Movement)
		{
			AssignItemToMovementButton(type);
		}
		if (currentAction != null && currentAction.type == inventoryData.type)
		{
			GameHUD.instance.UpdateActionHUDItem(currentAction.remainingQuantity);
		}
		else if (currentMovement != null && currentMovement.type == inventoryData.type)
		{
			GameHUD.instance.UpdateMovementHUDItem(currentMovement.remainingQuantity);
		}
	}

	public void DecrementInventoryItem(InventoryData.Type type)
	{
		InventoryData inventoryData = inventory[(int)type];
		if (inventoryData.available)
		{
			inventoryData.Decrement();
			if (!inventoryData.available)
			{
				RemoveFromInventory(inventoryData);
			}
			else if (currentAction != null && currentAction.type == type)
			{
				GameHUD.instance.UpdateActionHUDItem(inventoryData.remainingQuantity);
			}
			else if (currentMovement != null && currentMovement.type == type)
			{
				GameHUD.instance.UpdateMovementHUDItem(inventoryData.remainingQuantity);
			}
		}
	}

	public void RemoveFromInventory(InventoryData.Type type)
	{
		InventoryData dataModel = inventory[(int)type];
		RemoveFromInventory(dataModel);
	}

	public void RemoveFromInventory(InventoryData dataModel)
	{
		if (currentAction == dataModel)
		{
			currentAction = null;
			GameHUD.instance.DeactivateActionHUD();
		}
		else if (currentMovement == dataModel)
		{
			currentMovement = null;
			GameHUD.instance.DeactivateMovementHUD();
		}
		dataModel.Reset();
	}

	public void ResetInventory()
	{
		for (int i = 0; i < inventory.Length; i++)
		{
			inventory[i].Reset();
		}
		Array.Clear(checkpointInventoryQuantities, 0, checkpointInventoryQuantities.Length);
		checkpointAction = null;
		checkpointMovement = null;
		if (currentAction != null)
		{
			RemoveFromInventory(currentAction);
		}
		if (currentMovement != null)
		{
			RemoveFromInventory(currentMovement);
		}
	}

	public void SaveCheckpointInventory()
	{
		checkpointAction = currentAction;
		checkpointMovement = currentMovement;
		for (int i = 0; i < inventory.Length; i++)
		{
			checkpointInventoryQuantities[i] = inventory[i].remainingQuantity;
		}
		int remainingQuantity = inventory[2].remainingQuantity;
		if (remainingQuantity > 0 && remainingQuantity % 2 != 0)
		{
			checkpointInventoryQuantities[2]++;
		}
	}

	public void LoadInventoryFromCheckpoint()
	{
		for (int i = 0; i < inventory.Length; i++)
		{
			inventory[i].remainingQuantity = checkpointInventoryQuantities[i];
		}
		currentAction = checkpointAction;
		currentMovement = checkpointMovement;
		if (currentAction != null)
		{
			AssignItemToActionButton(currentAction.type);
		}
		if (currentMovement != null)
		{
			AssignItemToMovementButton(currentMovement.type);
		}
	}
}
