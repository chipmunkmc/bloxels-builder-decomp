public class InventoryData
{
	public enum Type
	{
		Bomb = 0,
		Jetpack = 1,
		Shrink = 2,
		Map = 3
	}

	public enum DisplayType
	{
		None = 0,
		Text = 1,
		Elements = 2
	}

	public enum Behavior
	{
		Persistent = 0,
		Action = 1,
		Movement = 2
	}

	public Type type;

	public DisplayType displayType;

	public Behavior behavior;

	private int _remainingQuantity;

	[ExposeProperty]
	public int remainingQuantity
	{
		get
		{
			return _remainingQuantity;
		}
		set
		{
			_remainingQuantity = value;
			isFull = false;
			available = true;
			if (_remainingQuantity <= 0)
			{
				_remainingQuantity = 0;
				available = false;
			}
			else if (_remainingQuantity > maxQuantity)
			{
				_remainingQuantity = maxQuantity;
				isFull = true;
			}
		}
	}

	public int maxQuantity { get; private set; }

	public int incrementQuantity { get; private set; }

	public int decrementQuantity { get; private set; }

	public bool available { get; private set; }

	public bool isFull { get; private set; }

	public InventoryData(Type type, DisplayType displayType, Behavior behavior, int incrementQuantity, int decrementQuantity, int maxQuantity)
	{
		this.type = type;
		this.displayType = displayType;
		this.behavior = behavior;
		this.incrementQuantity = incrementQuantity;
		this.decrementQuantity = decrementQuantity;
		this.maxQuantity = maxQuantity;
	}

	public void Increment()
	{
		if (!isFull)
		{
			remainingQuantity += incrementQuantity;
		}
	}

	public void Decrement()
	{
		remainingQuantity -= decrementQuantity;
	}

	public void Reset()
	{
		remainingQuantity = 0;
	}
}
