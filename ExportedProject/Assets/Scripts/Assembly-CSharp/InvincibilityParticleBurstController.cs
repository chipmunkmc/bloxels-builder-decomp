using UnityEngine;

public class InvincibilityParticleBurstController : MonoBehaviour
{
	public ParticleSystem particleSystem;

	public int burstCount;

	public float burstLifetime;

	private float burstStartTime;

	public void BeginInvincibility()
	{
		base.gameObject.SetActive(true);
		particleSystem.time = Random.Range(0f, particleSystem.duration);
		particleSystem.Emit(burstCount);
		base.enabled = false;
	}

	public void EndInvincibility(bool immediate = false)
	{
		burstStartTime = Time.time;
		if (!immediate)
		{
			particleSystem.Emit(burstCount);
			base.enabled = true;
			SoundManager.PlayOneShot(SoundManager.instance.endInvincibilitySFX);
		}
		else
		{
			particleSystem.Clear();
			base.gameObject.SetActive(false);
		}
	}

	private void Update()
	{
		if (Time.time > burstStartTime + burstLifetime)
		{
			base.enabled = false;
			base.gameObject.SetActive(false);
		}
	}
}
