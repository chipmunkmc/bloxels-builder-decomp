using DG.Tweening;
using UnityEngine;

public class InvincibilityPowerUp : PowerUp
{
	private new void Start()
	{
		base.Start();
		powerUpType = PowerUpType.Invincibility;
		Bounce();
	}

	private void OnDestroy()
	{
		base.transform.DOKill();
	}

	public override void Collect()
	{
		base.Collect();
		PixelPlayerController.instance.EnableInvincibility();
		Object.Destroy(base.gameObject);
	}
}
