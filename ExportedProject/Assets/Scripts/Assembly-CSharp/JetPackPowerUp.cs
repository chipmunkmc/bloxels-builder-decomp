using DG.Tweening;
using UnityEngine;

public class JetPackPowerUp : PowerUp, InventoryItem
{
	private new void Start()
	{
		base.Start();
		powerUpType = PowerUpType.Jetpack;
		Bounce();
	}

	private void OnDestroy()
	{
		base.transform.DOKill();
	}

	public override void Collect()
	{
		base.Collect();
		PixelPlayerController.instance.GetJetpack();
		Object.Destroy(base.gameObject);
	}

	public void AddItemToInventory()
	{
		GameplayController.instance.inventoryManager.AddToInventory(InventoryData.Type.Jetpack);
	}
}
