using UnityEngine;

public class Job : ThreadedJob
{
	public Vector3[] inData;

	public Vector3[] outData;

	protected override void ThreadFunction()
	{
		for (int i = 0; i < 100000000; i++)
		{
			inData[i % inData.Length] += inData[(i + 1) % inData.Length];
		}
	}

	protected override void OnFinished()
	{
		for (int i = 0; i < inData.Length; i++)
		{
		}
	}
}
