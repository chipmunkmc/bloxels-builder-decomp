using UnityEngine;

public class Level : MonoBehaviour
{
	public Transform movableChunkContainer;

	public BloxelLevel bloxelLevel;

	public BloxelTile[,] tiles;

	public LevelBackground background;

	private void Awake()
	{
		tiles = new BloxelTile[13, 13];
	}

	public void UpdateTileAtLocation(GridLocation locationInBoard, BloxelProject project)
	{
		BloxelTile bloxelTile = tiles[locationInBoard.c, locationInBoard.r];
		if (bloxelTile != null)
		{
			bloxelTile.tileInfo = new TileInfo(project, bloxelLevel, (GameBehavior)bloxelLevel.coverBoard.blockColors[locationInBoard.c, locationInBoard.r], locationInBoard, bloxelLevel.location);
			bloxelTile.CreateTile();
		}
	}

	public void CreateTileAtLocation(GridLocation locationInBoard, BlockColor blockColor)
	{
		Vector3 positionInWorld = LevelBuilder.instance.GetPositionInWorld(bloxelLevel.location, locationInBoard);
		GameObject gameObject = null;
		BloxelTile bloxelTile = null;
		BloxelProject projectModelAtLocation = bloxelLevel.GetProjectModelAtLocation(locationInBoard);
		switch (blockColor)
		{
		case BlockColor.Green:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelPlatformTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		case BlockColor.Orange:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelDestructibleTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		case BlockColor.Red:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelHazardTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		case BlockColor.Pink:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelPowerUpTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		case BlockColor.Blue:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelWaterTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		case BlockColor.Yellow:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelCoinTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		case BlockColor.Purple:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelEnemyTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		case BlockColor.White:
			gameObject = Object.Instantiate(LevelBuilder.instance.bloxelNPCTilePrefab, positionInWorld, Quaternion.identity) as GameObject;
			gameObject.transform.SetParent(movableChunkContainer);
			break;
		}
		bloxelTile = gameObject.GetComponent<BloxelTile>();
		bloxelTile.tileInfo = new TileInfo(projectModelAtLocation, bloxelLevel, (GameBehavior)blockColor, locationInBoard, bloxelLevel.location);
		tiles[locationInBoard.c, locationInBoard.r] = bloxelTile;
	}
}
