using System.Collections.Generic;
using UnityEngine;

public sealed class LevelBackground : PoolableComponent
{
	public MeshRenderer renderer;

	public Texture2D backgroundTexture;

	private static Color[] clearColorBufferFull;

	public static Color[] colorBufferTile;

	public override void EarlyAwake()
	{
		if (selfTransform != null)
		{
			return;
		}
		if (clearColorBufferFull == null || colorBufferTile == null)
		{
			clearColorBufferFull = new Color[28561];
			for (int i = 0; i < clearColorBufferFull.Length; i++)
			{
				clearColorBufferFull[i] = Color.clear;
			}
			colorBufferTile = new Color[169];
			for (int j = 0; j < colorBufferTile.Length; j++)
			{
				colorBufferTile[j] = Color.clear;
			}
		}
		selfTransform = base.transform;
		backgroundTexture = new Texture2D(169, 169, TextureFormat.ARGB32, false);
		backgroundTexture.filterMode = FilterMode.Point;
		backgroundTexture.wrapMode = TextureWrapMode.Clamp;
		renderer.material.mainTexture = backgroundTexture;
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		base.gameObject.SetActive(false);
	}

	public override void Despawn()
	{
		PrepareDespawn();
		selfTransform.SetParent(LevelBackgroundPool.instance.selfTransform);
		LevelBackgroundPool.instance.pool.Push(this);
	}

	public void InitForLevel(BloxelLevel level)
	{
		backgroundTexture.SetPixels(clearColorBufferFull);
		Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator = level.backgroundTiles.GetEnumerator();
		while (enumerator.MoveNext())
		{
			GridLocation key = enumerator.Current.Key;
			BloxelBoard value = enumerator.Current.Value;
			value.GetColorsNonAlloc(ref colorBufferTile);
			backgroundTexture.SetPixels(key.c * 13, key.r * 13, 13, 13, colorBufferTile);
		}
		backgroundTexture.Apply();
	}

	public void ClearBackgroundTexture()
	{
		backgroundTexture.SetPixels(clearColorBufferFull);
		backgroundTexture.Apply();
	}

	public void SetPixelsAtLocation(BloxelBoard board, GridLocation loc)
	{
		board.GetColorsNonAlloc(ref colorBufferTile);
		backgroundTexture.SetPixels(loc.c * 13, loc.r * 13, 13, 13, colorBufferTile);
		backgroundTexture.Apply();
	}

	public void ErasePixelsAtLocation(GridLocation loc)
	{
		for (int i = 0; i < colorBufferTile.Length; i++)
		{
			colorBufferTile[i] = Color.clear;
		}
		backgroundTexture.SetPixels(loc.c * 13, loc.r * 13, 13, 13, colorBufferTile);
		backgroundTexture.Apply();
	}
}
