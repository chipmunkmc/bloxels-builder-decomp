using System.Collections.Generic;
using UnityEngine;

public class LevelBackgroundPool : MonoBehaviour
{
	public static LevelBackgroundPool instance;

	public Transform selfTransform;

	public int poolSize;

	public int poolExpansionSize;

	public GameObject prefab;

	private int totalBackgrounds;

	public int[] levelInstances;

	private static bool _InitComplete;

	public Stack<LevelBackground> pool { get; private set; }

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			instance = this;
			_InitComplete = true;
			Object.DontDestroyOnLoad(base.gameObject);
			selfTransform = base.transform;
			Initialize();
		}
	}

	private void Initialize()
	{
		pool = new Stack<LevelBackground>(poolSize);
		AllocatePoolObjects();
	}

	private void AllocatePoolObjects()
	{
		levelInstances = new int[poolSize * 2];
		for (int i = 0; i < poolSize; i++)
		{
			GameObject gameObject = Object.Instantiate(prefab);
			gameObject.name = prefab.name;
			LevelBackground component = gameObject.GetComponent<LevelBackground>();
			component.EarlyAwake();
			component.selfTransform.SetParent(selfTransform);
			levelInstances[totalBackgrounds++] = component.GetInstanceID();
			gameObject.SetActive(false);
			pool.Push(component);
		}
	}

	public LevelBackground Spawn()
	{
		if (pool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(prefab);
				gameObject.name = prefab.name;
				LevelBackground component = gameObject.GetComponent<LevelBackground>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				levelInstances[totalBackgrounds++] = component.GetInstanceID();
				gameObject.SetActive(false);
				pool.Push(component);
			}
		}
		LevelBackground levelBackground = pool.Pop();
		levelBackground.PrepareSpawn();
		return levelBackground;
	}

	public void PrepareForSceneChange()
	{
		Stack<LevelBackground>.Enumerator enumerator = pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.selfTransform.SetParent(selfTransform);
			Object.DontDestroyOnLoad(enumerator.Current.gameObject);
		}
	}
}
