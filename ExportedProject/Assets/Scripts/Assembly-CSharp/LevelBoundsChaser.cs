using UnityEngine;

public class LevelBoundsChaser : MonoBehaviour
{
	private TrailRenderer _trailRenderer;

	public Transform selfTransform;

	private Vector3[] _points;

	private int _currentIdx;

	private int _targetIdx;

	private bool _shouldTrace;

	private float _timerStart;

	public float duration = 2f;

	public float traceTime
	{
		get
		{
			return _trailRenderer.time;
		}
		set
		{
			_trailRenderer.time = value;
		}
	}

	private void Awake()
	{
		selfTransform = base.transform;
		_trailRenderer = GetComponent<TrailRenderer>();
		_points = new Vector3[4];
	}

	private void Update()
	{
		if (_shouldTrace)
		{
			if (_timerStart + duration < Time.time)
			{
				_currentIdx = _targetIdx;
				_targetIdx = (_targetIdx + 1) % _points.Length;
				_timerStart = Time.time;
			}
			float t = (Time.time - _timerStart) / duration;
			selfTransform.localPosition = Vector3.Lerp(_points[_currentIdx], _points[_targetIdx], t);
		}
	}

	public void Init(GridLocation loc)
	{
		_points[0] = new Vector3(loc.c * 169, (loc.r + 1) * 169, -7f);
		_points[1] = new Vector3((loc.c + 1) * 169, (loc.r + 1) * 169, -7f);
		_points[2] = new Vector3((loc.c + 1) * 169, loc.r * 169, -7f);
		_points[3] = new Vector3(loc.c * 169, loc.r * 169, -7f);
		_shouldTrace = true;
	}

	public void Stop()
	{
		_shouldTrace = false;
		base.enabled = false;
	}
}
