using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Threading;
using UnityEngine;

public class LevelBuilder : MonoBehaviour
{
	public delegate void HandleChunkChange();

	public static LevelBuilder instance;

	private Transform selfTransform;

	private UnityEngine.Object levelPrefab;

	public UnityEngine.Object bloxelCoinTilePrefab;

	public UnityEngine.Object bloxelDestructibleTilePrefab;

	public UnityEngine.Object bloxelEnemyTilePrefab;

	public UnityEngine.Object bloxelHazardTilePrefab;

	public UnityEngine.Object bloxelNPCTilePrefab;

	public UnityEngine.Object bloxelPlatformTilePrefab;

	public UnityEngine.Object bloxelPowerUpTilePrefab;

	public UnityEngine.Object bloxelWaterTilePrefab;

	public UnityEngine.Object bloxelPlainTilePrefab;

	public GameBackground gameBackground;

	public BloxelGame game;

	public static BloxelBoard[] SolidColorBoards;

	public Level currentLevel;

	public Dictionary<GridLocation, Level> levelsInWorld;

	[CompilerGenerated]
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private HandleChunkChange m_OnChunkUpdated;

	public event HandleChunkChange OnChunkUpdated
	{
		add
		{
			HandleChunkChange handleChunkChange = this.m_OnChunkUpdated;
			HandleChunkChange handleChunkChange2;
			do
			{
				handleChunkChange2 = handleChunkChange;
				handleChunkChange = Interlocked.CompareExchange(ref this.m_OnChunkUpdated, (HandleChunkChange)Delegate.Combine(handleChunkChange2, value), handleChunkChange);
			}
			while (handleChunkChange != handleChunkChange2);
		}
		remove
		{
			HandleChunkChange handleChunkChange = this.m_OnChunkUpdated;
			HandleChunkChange handleChunkChange2;
			do
			{
				handleChunkChange2 = handleChunkChange;
				handleChunkChange = Interlocked.CompareExchange(ref this.m_OnChunkUpdated, (HandleChunkChange)Delegate.Remove(handleChunkChange2, value), handleChunkChange);
			}
			while (handleChunkChange != handleChunkChange2);
		}
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		selfTransform = base.transform;
		Application.targetFrameRate = 60;
		levelPrefab = Resources.Load("Prefabs/Level");
		bloxelCoinTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelCoin");
		bloxelDestructibleTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelDestructible");
		bloxelEnemyTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelEnemy");
		bloxelHazardTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelHazard");
		bloxelNPCTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelNPC");
		bloxelPlatformTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelPlatform");
		bloxelPowerUpTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelPowerUp");
		bloxelWaterTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelWater");
		bloxelPlainTilePrefab = Resources.Load("Prefabs/BloxelTiles/BloxelPlain");
		levelsInWorld = new Dictionary<GridLocation, Level>();
		SetupSolidColorBoards();
	}

	private void Start()
	{
		if ((bool)PixelEditorController.instance)
		{
			GameBuilderCanvas.instance.OnGameBuilderProjectChange += HandleOnGameBuilderProjectChange;
		}
	}

	private void HandleOnGameBuilderProjectChange()
	{
		SetupGameBackground(GameBuilderCanvas.instance.currentGame);
	}

	public void UpdateChunkFromWireframe(BlockColor newBlockColor, GridLocation locationInLevel, bool isPainting)
	{
		if (newBlockColor != GameBuilderCanvas.instance.currentBloxelLevel.coverBoard.blockColors[locationInLevel.c, locationInLevel.r] && PixelEditorController.instance.mode == CanvasMode.LevelEditor && isPainting)
		{
			GameBuilderCanvas.instance.currentBloxelLevel.RemoveItemAtLocation(locationInLevel, false);
			GameplayBuilder.instance.UpdateTileFromWireframe(newBlockColor, GameBuilderCanvas.instance.currentBloxelLevel, locationInLevel, SolidColorBoards[(uint)newBlockColor]);
		}
	}

	public GameObject CreateAndPlaceCharacter(BloxelGame _game, bool fromServer = false)
	{
		BloxelLevel value = null;
		bool flag = false;
		if (!fromServer)
		{
			flag = AssetManager.instance.levelPool.TryGetValue(_game.heroStartPosition.id.ToString(), out value);
		}
		else if (_game.heroStartPosition != WorldLocation.InvalidLocation())
		{
			flag = game.levels.TryGetValue(_game.heroStartPosition.levelLocationInWorld, out value);
		}
		if (!flag)
		{
			value = _game.levels.First().Value;
		}
		Vector3 localPosition = GetPositionInWorld(value.location, _game.heroStartPosition.locationInBoard) + new Vector3(6f, 0f, 0f);
		GameObject gameObject = UnityEngine.Object.Instantiate(Resources.Load("Prefabs/Player/Player")) as GameObject;
		gameObject.transform.SetParent(WorldWrapper.instance.selfTransform);
		gameObject.transform.localPosition = localPosition;
		gameObject.transform.GetComponent<PixelPlayerController>().animationController.InitializeAnimations(_game.hero);
		return gameObject;
	}

	public Vector3 GetPositionInWorld(GridLocation boardPosition, GridLocation tilePosition)
	{
		Vector3 vector = new Vector3(boardPosition.c * 169, boardPosition.r * 169, 0f);
		return new Vector3((float)tilePosition.c * 13f, (float)tilePosition.r * 13f, 0f) + vector;
	}

	public void SetupGameBackground(BloxelGame _game)
	{
		game = _game;
		if (game.background != null)
		{
			gameBackground.BuildFromGame(game);
		}
		else
		{
			gameBackground.Reset();
		}
	}

	private void DestroyCurrentLayout()
	{
		levelsInWorld.Clear();
		for (int i = 0; i < WorldWrapper.instance.selfTransform.childCount; i++)
		{
			Transform child = WorldWrapper.instance.selfTransform.GetChild(i);
			GameBackground component = child.GetComponent<GameBackground>();
			SpriteRenderer component2 = child.GetComponent<SpriteRenderer>();
			PixelPlayerController component3 = child.GetComponent<PixelPlayerController>();
			if (!component && !component2 && !component3)
			{
				UnityEngine.Object.Destroy(child.gameObject);
			}
		}
	}

	private void SetupSolidColorBoards()
	{
		SolidColorBoards = new BloxelBoard[Enum.GetValues(typeof(BlockColor)).Length];
		for (int i = 0; i < SolidColorBoards.Length; i++)
		{
			BlockColor blockColor = (BlockColor)i;
			BlockColor[,] array = new BlockColor[13, 13];
			for (int j = 0; j < array.GetLength(1); j++)
			{
				for (int k = 0; k < array.GetLength(0); k++)
				{
					array[k, j] = blockColor;
				}
			}
			SolidColorBoards[i] = new BloxelBoard(array, false);
		}
	}

	public static BloxelBoard[] GetSolidColorBoards()
	{
		BloxelBoard[] array = new BloxelBoard[Enum.GetValues(typeof(BlockColor)).Length];
		for (int i = 0; i < array.Length; i++)
		{
			BlockColor blockColor = (BlockColor)i;
			BlockColor[,] array2 = new BlockColor[13, 13];
			for (int j = 0; j < array2.GetLength(1); j++)
			{
				for (int k = 0; k < array2.GetLength(0); k++)
				{
					array2[k, j] = blockColor;
				}
			}
			array[i] = new BloxelBoard(array2, false);
		}
		return array;
	}
}
