using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(GridLayoutGroup))]
public class LibraryScrollElement : EnhancedScrollerCellView
{
	public RectTransform selfRect;

	public SavedProject savedProjectPrefab;

	public SavedProject[] savedProjectArray;

	private int maxProjectCount;

	public static int boardCols = 2;

	public static int animationCols = 2;

	public static int characterCols = 2;

	public static int megaBoardCols = 1;

	public static int gameCols = 1;

	public static int currentRows = 1;

	public static int currentCols;

	public static float viewHeight;

	public GridLayoutGroup gridLayout { get; set; }

	public int rows { get; private set; }

	public int cols { get; private set; }

	private void Awake()
	{
		rows = currentRows;
		cols = currentCols;
		Init(rows, cols);
	}

	public void Init(int numRows, int numCols)
	{
		float width = BloxelLibraryScrollController.instance.scroller.ScrollRect.content.rect.width;
		float spacing = BloxelLibraryScrollController.instance.scroller.spacing;
		gridLayout = GetComponent<GridLayoutGroup>();
		selfRect = base.transform as RectTransform;
		savedProjectArray = new SavedProject[numRows * numCols];
		gridLayout.spacing = new Vector2(spacing / 2f, spacing / 2f);
		gridLayout.constraint = GridLayoutGroup.Constraint.FixedRowCount;
		gridLayout.constraintCount = numRows;
		gridLayout.cellSize = new Vector2((width - spacing - gridLayout.spacing.x * (float)(numCols - 1)) / (float)numCols, (width - spacing - gridLayout.spacing.y * (float)(numCols - 1)) / (float)numCols);
		int i = 0;
		int num = 0;
		for (; i < numRows; i++)
		{
			int num2 = 0;
			while (num2 < numCols)
			{
				SavedProject savedProject = Object.Instantiate(savedProjectPrefab);
				savedProject.transform.SetParent(base.transform, false);
				savedProject.transform.localScale = Vector3.one;
				savedProjectArray[num] = savedProject;
				num2++;
				num++;
			}
		}
		maxProjectCount = numRows * numCols;
	}

	public void SetData(BloxelProject project)
	{
		savedProjectArray[0].SetData(project);
	}

	public void SetData(params BloxelProject[] bloxelProjects)
	{
		int num = bloxelProjects.Length;
		if (num > maxProjectCount)
		{
			num = maxProjectCount;
		}
		for (int i = 0; i < num; i++)
		{
			savedProjectArray[i].SetData(bloxelProjects[i]);
		}
	}
}
