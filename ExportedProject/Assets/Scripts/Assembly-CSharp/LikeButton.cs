using UnityEngine;
using UnityEngine.UI;

public class LikeButton : MonoBehaviour
{
	public Text likeText;

	public Text likeCounter;

	public Sprite iconNormal;

	public Sprite iconFilled;

	public Image image;

	public void SetImage(bool hasLiked)
	{
		if (hasLiked)
		{
			image.sprite = iconFilled;
		}
		else
		{
			image.sprite = iconNormal;
		}
	}
}
