using UnityEngine;

public class Loading : MonoBehaviour
{
	public UIProgressBar progressBar;

	public Camera camera;

	public RectTransform rectDerp;

	private void Awake()
	{
		rectDerp.localScale = Vector3.zero;
	}

	private void Start()
	{
		if (DeviceManager.isPoopDevice && Application.systemLanguage.ToString().Equals("English"))
		{
			rectDerp.localScale = Vector3.one;
		}
	}

	private void OnDestroy()
	{
		Resources.UnloadUnusedAssets();
	}
}
