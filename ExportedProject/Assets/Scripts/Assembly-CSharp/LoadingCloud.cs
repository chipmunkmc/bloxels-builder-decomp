using DG.Tweening;
using UnityEngine;

public class LoadingCloud : MonoBehaviour
{
	private RectTransform rect;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
	}

	private void Start()
	{
		rect.DOAnchorPos(new Vector2(rect.anchoredPosition.x - 400f, rect.anchoredPosition.y), 4f);
	}
}
