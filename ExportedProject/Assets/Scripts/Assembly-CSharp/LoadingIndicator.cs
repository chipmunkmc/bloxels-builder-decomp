using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class LoadingIndicator : MonoBehaviour
{
	public static LoadingIndicator instance;

	public Image background;

	public Text uiText;

	public Image loadingBackground;

	public Image loadingBar;

	private Color baseColor;

	private Color completeColor;

	private RectTransform loadingBarRect;

	private Vector2 baseLoadingBarSize;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
		baseColor = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
		completeColor = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green);
		loadingBackground.color = baseColor;
		loadingBar.color = Color.white;
		loadingBarRect = loadingBar.GetComponent<RectTransform>();
		baseLoadingBarSize = loadingBarRect.sizeDelta;
	}

	private void Start()
	{
	}

	public void Load(float val)
	{
		loadingBarRect.DOSizeDelta(new Vector2(loadingBarRect.sizeDelta.x + val, loadingBarRect.sizeDelta.y), UIAnimationManager.speedFast);
	}

	public void UnLoad()
	{
		loadingBarRect.DOSizeDelta(baseLoadingBarSize, UIAnimationManager.speedFast);
	}

	public void Complete()
	{
		loadingBackground.DOColor(completeColor, UIAnimationManager.speedFast);
		loadingBar.DOColor(completeColor, UIAnimationManager.speedFast);
		loadingBackground.transform.DOPunchScale(new Vector3(1.25f, 1.25f, 1.25f), UIAnimationManager.speedFast).OnComplete(delegate
		{
			base.gameObject.transform.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				Object.Destroy(base.gameObject, UIAnimationManager.speedMedium);
			});
		});
	}
}
