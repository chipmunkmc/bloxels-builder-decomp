using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class LocalizationManager
{
	private static LocalizationManager instance;

	public JSONObject languages;

	public JSONObject currentLocalization;

	public string currentLanguage;

	private static readonly string resourcesPath = "./Assets/Resources/";

	private static readonly string locPrefix = "Localization/localization-";

	private static readonly string locSuffix = ".json";

	private static readonly string languageMasterFileKey = "languages";

	private List<Localizable> subscribers = new List<Localizable>();

	private static TextAsset _CurrentAtlas;

	public static bool AutoSelectedLanguage;

	public static bool SysLanguageIsSupported;

	private LocalizationManager()
	{
		string path = locPrefix + languageMasterFileKey;
		TextAsset textAsset = Resources.Load<TextAsset>(path);
		languages = new JSONObject(textAsset.text);
		if (PrefsManager.instance != null)
		{
			string preferredLanguage = PrefsManager.instance.preferredLanguage;
			if (preferredLanguage != null && preferredLanguage.Length > 0)
			{
				loadLocalization(preferredLanguage);
				AutoSelectedLanguage = true;
				return;
			}
		}
		string language = Application.systemLanguage.ToString();
		if (isLanguageSupported(language))
		{
			SysLanguageIsSupported = true;
			loadLocalization(language);
		}
		else
		{
			SysLanguageIsSupported = false;
			loadLocalization(languages[0].ToString());
		}
		AutoSelectedLanguage = true;
		instance = this;
	}

	public static LocalizationManager getInstance()
	{
		if (instance == null)
		{
			instance = new LocalizationManager();
		}
		return instance;
	}

	public void loadLocalization(string language)
	{
		if (_CurrentAtlas != null)
		{
			Resources.UnloadAsset(_CurrentAtlas);
		}
		currentLanguage = clean(language);
		currentLocalization = readLocalization(language);
		forceTextUpdate();
		Resources.UnloadUnusedAssets();
	}

	public static JSONObject readLocalization(string language)
	{
		language = clean(language);
		_CurrentAtlas = Resources.Load<TextAsset>(locPrefix + language);
		return new JSONObject(_CurrentAtlas.text);
	}

	private bool isLanguageSupported(string language)
	{
		for (int i = 0; i < languages.Count; i++)
		{
			if (language.Equals(clean(languages[i].ToString()), StringComparison.Ordinal))
			{
				return true;
			}
		}
		return false;
	}

	public string getLocalizedText(string unityKey, string defaultText = null)
	{
		if (!currentLocalization.HasField(unityKey))
		{
			return defaultText;
		}
		string text = currentLocalization.GetField(unityKey).ToString();
		if (text != null && text != string.Empty)
		{
			text = text.Replace("\\n", "\n");
			text = text.Replace("\\\"", "\"");
			return clean(text);
		}
		return defaultText;
	}

	public List<string> getLocalizedText(Dictionary<string, string> input)
	{
		return input.Select((KeyValuePair<string, string> kvp) => getLocalizedText(kvp.Key, kvp.Value)).ToList();
	}

	public static string getSavePath(string language)
	{
		return resourcesPath + locPrefix + language + locSuffix;
	}

	public static string getSavePath_MasterLanguageFile()
	{
		return getSavePath(languageMasterFileKey);
	}

	public static string clean(string input)
	{
		if (input.StartsWith("\"") && input.EndsWith("\""))
		{
			return input.Substring(1, input.Length - 2);
		}
		return input;
	}

	public void addSubscriber(Localizable tl)
	{
		subscribers.Add(tl);
	}

	public void removeSubscriber(Localizable tl)
	{
		subscribers.Remove(tl);
	}

	private void forceTextUpdate()
	{
		for (int i = 0; i < subscribers.Count; i++)
		{
			subscribers[i].localizeText();
		}
	}
}
