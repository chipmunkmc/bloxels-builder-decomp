using System;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;

public class LongPressEventTrigger : UIInteractable
{
	public delegate void HandleLongPress();

	[Tooltip("How long must pointer be down on this object to trigger a long press")]
	public float durationThreshold = 1f;

	private bool isPointerDown;

	private bool longPressTriggered;

	private float timePressStarted;

	public event HandleLongPress OnLongPress;

	private void Update()
	{
		if (isPointerDown && !longPressTriggered && Time.time - timePressStarted > durationThreshold)
		{
			longPressTriggered = true;
			if (this.OnLongPress != null)
			{
				this.OnLongPress();
			}
		}
	}

	public override void OnPointerDown(PointerEventData eventData)
	{
		timePressStarted = Time.time;
		isPointerDown = true;
		longPressTriggered = false;
		base.OnPointerDown(eventData);
	}

	public override void OnPointerUp(PointerEventData eventData)
	{
		isPointerDown = false;
		base.OnPointerUp(eventData);
	}

	public override void OnPointerExit(PointerEventData eventData)
	{
		isPointerDown = false;
		base.OnPointerExit(eventData);
	}
}
