using DG.Tweening;
using UnityEngine;

public class MapPowerUp : PowerUp, InventoryItem
{
	private new void Start()
	{
		base.Start();
		powerUpType = PowerUpType.Map;
		Bounce();
	}

	private void OnDestroy()
	{
		base.transform.DOKill();
	}

	public override void Collect()
	{
		base.Collect();
		Object.Destroy(base.gameObject);
	}

	public void AddItemToInventory()
	{
		GameplayController.instance.inventoryManager.AddToInventory(InventoryData.Type.Map);
	}
}
