public class MapRoomUnlockable : GemUnlockable
{
	private GridLocation _gridLocation;

	public GridLocation gridLocation
	{
		get
		{
			return _gridLocation;
		}
		private set
		{
			_gridLocation = value;
		}
	}

	public MapRoomUnlockable(GridLocation loc)
	{
		gridLocation = loc;
		cost = GemUnlockable.MapRoomCost;
		type = Type.MapRoom;
		uiBuyable = UIBuyable.CreateMapRoom();
	}

	public override bool IsLocked()
	{
		return true;
	}

	public override void Unlock()
	{
		GemManager.instance.UnlockMapRoom();
		EventUnlock(this);
	}
}
