using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MapZoomToggle : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public static MapZoomToggle instance;

	public Sprite iconZoomIn;

	public Sprite iconZoomOut;

	public bool currentlyZoomedIn;

	public Transform selfTransform;

	public RectTransform rect;

	public Image icon;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		rect = GetComponent<RectTransform>();
		selfTransform = GetComponent<Transform>();
	}

	private void OnDisable()
	{
		ZoomOut();
	}

	public void ToggleZoom()
	{
		if (currentlyZoomedIn)
		{
			ZoomOut();
		}
		else
		{
			ZoomIn();
		}
	}

	private void ZoomIn()
	{
		MegaBoardCanvas.instance.ZoomIn();
		currentlyZoomedIn = true;
		icon.sprite = iconZoomOut;
	}

	private void ZoomOut()
	{
		MegaBoardCanvas.instance.ZoomOut();
		currentlyZoomedIn = false;
		icon.sprite = iconZoomIn;
	}

	public void OnPointerClick(PointerEventData pEvent)
	{
		SoundManager.PlayOneShot(SoundManager.instance.popC);
		ToggleZoom();
	}
}
