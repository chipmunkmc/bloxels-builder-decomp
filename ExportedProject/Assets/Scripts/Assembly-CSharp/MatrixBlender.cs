using System.Collections;
using UnityEngine;

[RequireComponent(typeof(Camera))]
public class MatrixBlender : MonoBehaviour
{
	public static Matrix4x4 MatrixLerp(Matrix4x4 from, Matrix4x4 to, float time)
	{
		Matrix4x4 result = default(Matrix4x4);
		for (int i = 0; i < 16; i++)
		{
			result[i] = Mathf.Lerp(from[i], to[i], time);
		}
		return result;
	}

	private IEnumerator LerpFromTo(Matrix4x4 src, Matrix4x4 dest, float duration)
	{
		float startTime = Time.time;
		while (Time.time - startTime < duration)
		{
			BloxelCamera.instance.mainCamera.projectionMatrix = MatrixLerp(src, dest, (Time.time - startTime) / duration);
			yield return 1;
		}
		BloxelCamera.instance.mainCamera.projectionMatrix = dest;
	}

	public Coroutine BlendToMatrix(Matrix4x4 targetMatrix, float duration)
	{
		StopAllCoroutines();
		return StartCoroutine(LerpFromTo(BloxelCamera.instance.mainCamera.projectionMatrix, targetMatrix, duration));
	}
}
