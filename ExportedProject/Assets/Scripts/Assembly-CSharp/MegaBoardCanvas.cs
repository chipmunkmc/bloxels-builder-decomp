using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class MegaBoardCanvas : UIMoveable
{
	public delegate void HandleTileSelect(MegaBoardTile tile);

	public static MegaBoardCanvas instance;

	public MegaBoardTile megaBoardTilePrefab;

	private UnityEngine.Object gridLinePrefab;

	public GameObject megaBoardTilesOutput;

	public GameObject gridLinesOutput;

	public Color gridLineColor;

	public MegaBoardTile[] tiles;

	public int tileRows;

	public int tileCols;

	public SavedProject currentProjectFromLibrary;

	public BloxelBoard currentBloxelBoard;

	public BloxelGame currentGame;

	public RectTransform currentLevelOutline;

	public RectTransform heroStartIndicator;

	public RectTransform megaCanvasScaler;

	public RectTransform megaCanvasScalerContainer;

	private CanvasGroup megaCanvasGroup;

	public GridLocation focusedTile;

	private CoverBoard heroCover;

	private RawImage uiRawImageHero;

	public UILineRenderer[] gridLines;

	public CanvasGroup uiGroupStartMenu;

	public UIButton uiButtonStartInit;

	public UIButton uiButtonEdit;

	public UIDecorateTools uiDecorateTools;

	public float totalBoardUnits = 858f;

	public float tileGutter;

	public float tileSize = 66f;

	public bool isDragging;

	public float currentScale = 1f;

	private UnityEngine.Object messagePrefab;

	private UnityEngine.Object tapIndicatorPrefab;

	private string messageTitle = "How To";

	private string messageDescription = "Select a board from the Boards library and paint in the canvas to create backgrounds";

	public GameObject tapIndicator;

	public GameObject cantDeleteText;

	public Sprite[] detailSprites = new Sprite[169];

	private Texture2D detailAtlas;

	private Texture2D detailAtlasOverflow;

	[Header("Particles")]
	public ParticleSystem ps;

	public Color[] particleColors;

	private ParticleSystem.Particle[] particles;

	public int maxParticles = 20;

	[Header("Room Drag")]
	public UIMapTileDragVisualizer currentDraggable;

	public UnityEngine.Object draggablePrefab;

	public MegaBoardTile currentActiveTile;

	public Vector3 dragOffset;

	private System.Random rand = new System.Random();

	public event HandleTileSelect OnTileSelect;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		tiles = new MegaBoardTile[169];
		tapIndicatorPrefab = Resources.Load("Prefabs/UI/TapIndicator");
		megaBoardTilePrefab = Resources.Load<MegaBoardTile>("Prefabs/MegaBoardTile");
		gridLinePrefab = Resources.Load("Prefabs/GridLine");
		messagePrefab = Resources.Load("Prefabs/UIPopupMessage");
		megaCanvasGroup = megaCanvasScalerContainer.GetComponent<CanvasGroup>();
		uiRawImageHero = heroStartIndicator.GetComponent<RawImage>();
		heroCover = new CoverBoard(Color.clear);
		InitDetailSprites();
		InitTiles();
	}

	private new void Start()
	{
		particles = new ParticleSystem.Particle[ps.maxParticles];
		particleColors = new Color[7]
		{
			AssetManager.instance.bloxelBlue,
			AssetManager.instance.bloxelOrange,
			AssetManager.instance.bloxelYellow,
			AssetManager.instance.bloxelGreen,
			AssetManager.instance.bloxelRed,
			AssetManager.instance.bloxelPink,
			AssetManager.instance.bloxelPurple
		};
		BloxelBoardLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
		BloxelMegaBoardLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
		BloxelMegaBoardLibrary.instance.OnProjectDelete += HandleProjectDelete;
		BuildLines();
		uiButtonStartInit.OnClick += BloxelMegaBoardLibrary.instance.AddButtonPressed;
		uiButtonEdit.OnClick += ShowLibrary;
		Init();
	}

	private void OnDestroy()
	{
		BloxelBoardLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
		BloxelMegaBoardLibrary.instance.OnProjectSelect -= HandleOnProjectSelect;
		BloxelMegaBoardLibrary.instance.OnProjectDelete -= HandleProjectDelete;
		uiButtonStartInit.OnClick -= BloxelMegaBoardLibrary.instance.AddButtonPressed;
		uiButtonEdit.OnClick -= ShowLibrary;
		Unload();
	}

	private void ShowLibrary()
	{
		BloxelLibrary.instance.libraryToggle.Show();
	}

	private void Unload()
	{
		instance = null;
		megaBoardTilePrefab = null;
		gridLinePrefab = null;
		megaBoardTilesOutput = null;
		gridLinesOutput = null;
		tiles = null;
		currentProjectFromLibrary = null;
		currentBloxelBoard = null;
		currentLevelOutline = null;
		heroStartIndicator = null;
		megaCanvasScaler = null;
		megaCanvasScalerContainer = null;
		megaCanvasGroup = null;
		heroCover = null;
		uiRawImageHero = null;
		gridLines = null;
		uiGroupStartMenu = null;
		uiButtonStartInit = null;
		uiButtonEdit = null;
		uiDecorateTools = null;
		messagePrefab = null;
		tapIndicatorPrefab = null;
		messageTitle = null;
		messageDescription = null;
		tapIndicator = null;
		detailSprites = null;
		detailAtlas = null;
		detailAtlasOverflow = null;
		ps = null;
		particleColors = null;
		particles = null;
	}

	private void InitDetailSprites()
	{
		Sprite[] array = Resources.LoadAll<Sprite>("DetailSprites");
		Sprite[] array2 = Resources.LoadAll<Sprite>("DetailOverflowSprites");
		int num = 0;
		for (int i = 0; i < array.Length; i++)
		{
			detailSprites[num++] = array[i];
		}
		for (int j = 0; j < array2.Length; j++)
		{
			if (num >= 169)
			{
				break;
			}
			detailSprites[num++] = array2[j];
		}
		detailAtlas = array[0].texture;
		detailAtlasOverflow = array2[0].texture;
	}

	private void InitTiles()
	{
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				MegaBoardTile megaBoardTile = UnityEngine.Object.Instantiate(megaBoardTilePrefab);
				megaBoardTile.rect.SetParent(megaBoardTilesOutput.transform);
				megaBoardTile.rect.localScale = Vector3.one;
				megaBoardTile.rect.localPosition = Vector3.zero;
				megaBoardTile.rect.localRotation = Quaternion.identity;
				megaBoardTile.rect.sizeDelta = new Vector2(tileSize, tileSize);
				megaBoardTile.rect.anchoredPosition = new Vector2((float)i * tileSize, (float)j * tileSize);
				megaBoardTile.loc = new GridLocation(i, j);
				megaBoardTile.uiImageAdd.enabled = false;
				tiles[i * 13 + j] = megaBoardTile;
			}
		}
		heroStartIndicator.SetAsLastSibling();
		currentLevelOutline.SetAsLastSibling();
		SetupTileListeners();
	}

	public new void Show()
	{
		base.Show();
		PixelEditorPaintBoard.instance.Hide();
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			uiDecorateTools.gameObject.SetActive(false);
			HideStart();
		}
		else if (PixelEditorController.instance.mode == CanvasMode.MegaBoard)
		{
			if (BloxelMegaBoardLibrary.instance.currentMega == null)
			{
				ShowStart();
			}
			else
			{
				HideStart();
			}
		}
	}

	public void Init()
	{
		uiDecorateTools.gameObject.SetActive(false);
		uiGroupStartMenu.gameObject.SetActive(true);
		uiGroupStartMenu.DOKill();
		uiGroupStartMenu.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			megaCanvasGroup.alpha = 0f;
			megaCanvasGroup.interactable = false;
			megaCanvasGroup.blocksRaycasts = false;
		});
	}

	private void HandleTileDragStart(MegaBoardTile _tile)
	{
		if (_tile.isBlank)
		{
			return;
		}
		GameObject gameObject = UnityEngine.Object.Instantiate(draggablePrefab) as GameObject;
		gameObject.transform.SetParent(UIOverlayCanvas.instance.transform);
		gameObject.transform.SetAsLastSibling();
		currentDraggable = gameObject.GetComponent<UIMapTileDragVisualizer>();
		currentDraggable.Create(_tile, UIOverlayCanvas.instance.canvas, dragOffset);
		GameBuilderCanvas.instance.uiTrashCan.Show();
		_tile.tmpLevelBuffer = _tile.project as BloxelLevel;
		_tile.Init(null, detailSprites[_tile.loc.r * 13 + _tile.loc.c]);
		for (int i = 0; i < tiles.Length; i++)
		{
			if (tiles[i].isBlank)
			{
				tiles[i].uiGemLock.ManualUnlock();
				tiles[i].uiImageAdd.enabled = true;
			}
		}
	}

	private void HandleTileDragEnd(MegaBoardTile _tile, bool shouldErase)
	{
		if (currentDraggable == null)
		{
			return;
		}
		currentDraggable.DestroyMe();
		currentDraggable = null;
		if (shouldErase)
		{
			if (currentGame.levels.Count <= 1 || currentGame.heroStartPosition.levelLocationInWorld == _tile.loc)
			{
				_tile.Init(_tile.tmpLevelBuffer, detailSprites[_tile.loc.r * 13 + _tile.loc.c]);
				CheckLockStatusForGame();
				cantDeleteText.transform.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
				{
					cantDeleteText.transform.DOScale(0f, UIAnimationManager.speedMedium).SetDelay(2f);
				});
			}
			else
			{
				currentGame.RemoveLevelAtLocation(_tile.loc);
				_tile.Init(null, detailSprites[_tile.loc.r * 13 + _tile.loc.c]);
				CheckLockStatusForGame();
				GameBuilderCanvas.instance.uiStatRoomInventory.InitFromGame(currentGame);
				GameplayBuilder.instance.RemoveLevelAtLocation(_tile.loc);
			}
			return;
		}
		if (currentActiveTile == null || currentActiveTile == _tile || !currentActiveTile.isBlank)
		{
			_tile.Init(_tile.tmpLevelBuffer, detailSprites[_tile.loc.r * 13 + _tile.loc.c]);
			CheckLockStatusForGame();
			return;
		}
		if (!currentGame.MoveLevel(_tile.loc, currentActiveTile.loc))
		{
			_tile.Init(_tile.tmpLevelBuffer, detailSprites[_tile.loc.r * 13 + _tile.loc.c]);
			CheckLockStatusForGame();
			return;
		}
		BloxelLevel bloxelLevel = currentGame.levels[currentActiveTile.loc];
		GameplayBuilder.instance.RemoveLevelAtLocation(_tile.loc);
		GameplayBuilder.instance.AddLevel(bloxelLevel);
		if (currentGame.heroStartPosition.levelLocationInWorld == currentActiveTile.loc)
		{
			focusedTile = currentActiveTile.loc;
			GameBuilderCanvas.instance.currentBloxelLevel = bloxelLevel;
			heroStartIndicator.anchoredPosition = new Vector2((float)currentActiveTile.loc.c * tileSize + tileSize / 4f, (float)currentActiveTile.loc.r * tileSize + tileSize / 4f);
			currentLevelOutline.anchoredPosition = new Vector2((float)currentActiveTile.loc.c * tileSize, (float)currentActiveTile.loc.r * tileSize);
			Vector3 vector = new Vector3(bloxelLevel.location.c * 169, bloxelLevel.location.r * 169, 0f);
			Vector3 localPosition = new Vector3((float)currentGame.heroStartPosition.locationInBoard.c * 13f + 6f, (float)currentGame.heroStartPosition.locationInBoard.r * 13f, -7f) + vector;
			if (GameplayController.instance.heroObject != null)
			{
				GameplayController.instance.heroObject.transform.localPosition = localPosition;
			}
			PixelEditorPaintBoard.instance.SwitchBloxelBoard(bloxelLevel.coverBoard);
			BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
			BloxelCamera.instance.SetGameBuilderCamToLevel(bloxelLevel.location);
		}
		_tile.ClearDetailTexture(detailSprites[_tile.loc.r * 13 + _tile.loc.c]);
		Sprite sprite = detailSprites[currentActiveTile.loc.r * 13 + currentActiveTile.loc.c];
		currentActiveTile.Init(_tile.tmpLevelBuffer, sprite);
		if (sprite.texture == detailAtlas)
		{
			detailAtlas.Apply();
		}
		else
		{
			detailAtlasOverflow.Apply();
		}
		CheckLockStatusForGame();
	}

	private void HandleProjectDelete(SavedProject item)
	{
		if (item.isActive)
		{
			Init();
		}
	}

	private void HandleOnProjectSelect(SavedProject item)
	{
		if (tapIndicator != null)
		{
			UnityEngine.Object.Destroy(tapIndicator);
		}
		currentProjectFromLibrary = item;
		switch (item.dataModel.type)
		{
		case ProjectType.MegaBoard:
			BuildMegaBoard(item.dataModel as BloxelMegaBoard);
			break;
		case ProjectType.Board:
			currentBloxelBoard = item.dataModel as BloxelBoard;
			break;
		}
		if (uiDecorateTools.currentTool != null)
		{
			uiDecorateTools.currentTool.Toggle();
		}
		HideStart();
	}

	public void ShowStart()
	{
		uiGroupStartMenu.DOKill();
		megaCanvasGroup.DOKill();
		uiGroupStartMenu.gameObject.SetActive(true);
		uiGroupStartMenu.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			megaCanvasGroup.DOFade(0f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				megaCanvasGroup.interactable = false;
				megaCanvasGroup.blocksRaycasts = false;
			});
			uiDecorateTools.gameObject.SetActive(false);
		});
	}

	public void HideStart()
	{
		uiGroupStartMenu.DOKill();
		megaCanvasGroup.DOKill();
		uiGroupStartMenu.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			megaCanvasGroup.DOFade(1f, UIAnimationManager.speedFast).OnStart(delegate
			{
				megaCanvasGroup.interactable = true;
				megaCanvasGroup.blocksRaycasts = true;
			});
			uiDecorateTools.gameObject.SetActive(true);
		});
	}

	public void CreateNewBackground()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmA);
		BloxelMegaBoard bloxelMegaBoard = BloxelMegaBoardLibrary.instance.AddProject(new BloxelMegaBoard(new MegaBoardSize(13, 13)));
		bloxelMegaBoard.SetTitle(BloxelMegaBoard.defaultTitle);
	}

	public void ZoomIn()
	{
		currentScale = 4f;
		float num = megaCanvasScalerContainer.sizeDelta.x / 13f * currentScale;
		float x = num * (float)(6 - focusedTile.c) * (2f / currentScale);
		float y = num * (float)(6 - focusedTile.r) * (2f / currentScale);
		megaCanvasScalerContainer.DOScale(currentScale, UIAnimationManager.speedMedium);
		megaCanvasScalerContainer.DOAnchorPos(new Vector2(x, y), UIAnimationManager.speedMedium);
		for (int i = 0; i < tiles.Length; i++)
		{
			tiles[i].ShowDetailTexture();
		}
	}

	public void ZoomOut()
	{
		currentScale = 1f;
		megaCanvasScalerContainer.DOScale(currentScale, UIAnimationManager.speedMedium);
		megaCanvasScalerContainer.DOAnchorPos(new Vector2(0f, 40f), UIAnimationManager.speedMedium);
		for (int i = 0; i < tiles.Length; i++)
		{
			tiles[i].ShowNormalTexture();
		}
	}

	public void BuildMegaBoard(BloxelMegaBoard project)
	{
		currentGame = null;
		ColorGridLines(ColorUtilities.blockRGB[BlockColor.Green]);
		int i = 0;
		int num = 0;
		for (; i < project.size.c; i++)
		{
			int num2 = 0;
			while (num2 < project.size.r)
			{
				MegaBoardTile megaBoardTile = tiles[num];
				GridLocation key = new GridLocation(i, num2);
				BloxelBoard value = null;
				project.boards.TryGetValue(key, out value);
				megaBoardTile.Init(value, detailSprites[num2 * 13 + i]);
				tiles[i * 13 + num2] = megaBoardTile;
				num2++;
				num++;
			}
		}
		heroStartIndicator.gameObject.SetActive(false);
		currentLevelOutline.gameObject.SetActive(false);
		detailAtlas.Apply();
		detailAtlasOverflow.Apply();
	}

	public void BuildGameLayout(BloxelGame game)
	{
		currentGame = game;
		ColorGridLines(ColorUtilities.blockRGB[BlockColor.Purple]);
		uiGroupStartMenu.DOKill();
		megaCanvasGroup.DOKill();
		uiGroupStartMenu.DOFade(0f, 0f).OnComplete(delegate
		{
			uiGroupStartMenu.gameObject.SetActive(false);
		});
		megaCanvasGroup.DOFade(1f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			megaCanvasGroup.interactable = true;
			megaCanvasGroup.blocksRaycasts = true;
		});
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			int num2 = 0;
			while (num2 < 13)
			{
				GridLocation gridLocation = new GridLocation(i, num2);
				MegaBoardTile megaBoardTile = tiles[num];
				BloxelLevel value = null;
				currentGame.levels.TryGetValue(gridLocation, out value);
				megaBoardTile.Init(value, detailSprites[num2 * 13 + i]);
				if (gridLocation == GameBuilderCanvas.instance.currentBloxelLevel.location)
				{
					focusedTile = gridLocation;
					currentLevelOutline.gameObject.SetActive(true);
					currentLevelOutline.sizeDelta = new Vector2(tileSize, tileSize);
					currentLevelOutline.anchoredPosition = new Vector2((float)i * tileSize, (float)num2 * tileSize);
				}
				if (currentGame.heroStartPosition.levelLocationInWorld == gridLocation)
				{
					heroStartIndicator.anchoredPosition = new Vector2((float)i * tileSize + tileSize / 4f, (float)num2 * tileSize + tileSize / 4f);
					uiRawImageHero.texture = currentGame.hero.coverBoard.UpdateTexture2D(ref heroCover.colors, ref heroCover.texture, heroCover.style);
				}
				num2++;
				num++;
			}
		}
		heroStartIndicator.gameObject.SetActive(true);
		currentLevelOutline.gameObject.SetActive(true);
		detailAtlas.Apply();
		detailAtlasOverflow.Apply();
		CheckLockStatusForGame();
	}

	public void CheckLockStatusForGame()
	{
		if (currentGame.levels.Count < GemManager.roomsUnlocked || (CurrentUser.instance.active && CurrentUser.instance.account.eduAccount > UserAccount.EduStatus.None))
		{
			return;
		}
		for (int i = 0; i < tiles.Length; i++)
		{
			MegaBoardTile megaBoardTile = tiles[i];
			if (megaBoardTile.isBlank)
			{
				megaBoardTile.SwapAddForGemLock();
			}
		}
	}

	public void TileSelected(MegaBoardTile tile)
	{
		if (currentGame != null)
		{
			CheckLockStatusForGame();
		}
		if (this.OnTileSelect != null)
		{
			this.OnTileSelect(tile);
		}
	}

	private void SetupTileListeners()
	{
		for (int i = 0; i < tiles.Length; i++)
		{
			tiles[i].OnSelect += TileSelected;
			tiles[i].OnDragStart += HandleTileDragStart;
			tiles[i].OnDragEnd += HandleTileDragEnd;
		}
	}

	private void BuildLines()
	{
		int num = tileCols + 1;
		int num2 = tileRows + 1;
		gridLines = new UILineRenderer[num + num2];
		int num3 = 0;
		for (int i = 0; i < num; i++)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(gridLinePrefab) as GameObject;
			RectTransform component = gameObject.GetComponent<RectTransform>();
			UILineRenderer component2 = gameObject.GetComponent<UILineRenderer>();
			gameObject.transform.SetParent(gridLinesOutput.transform);
			gameObject.transform.localScale = Vector3.one;
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = Quaternion.identity;
			component.sizeDelta = new Vector2(3f, (float)tileRows * tileSize);
			component.anchoredPosition = new Vector2((float)i * tileSize, 0f);
			component2.Points = new Vector2[2]
			{
				Vector2.zero,
				new Vector2(0f, 1f)
			};
			component2.color = gridLineColor;
			gridLines[num3] = component2;
			num3++;
		}
		for (int j = 0; j < num2; j++)
		{
			GameObject gameObject2 = UnityEngine.Object.Instantiate(gridLinePrefab) as GameObject;
			RectTransform component3 = gameObject2.GetComponent<RectTransform>();
			UILineRenderer component4 = gameObject2.GetComponent<UILineRenderer>();
			gameObject2.transform.SetParent(gridLinesOutput.transform);
			gameObject2.transform.localScale = Vector3.one;
			gameObject2.transform.localPosition = Vector3.zero;
			gameObject2.transform.localRotation = Quaternion.identity;
			component3.sizeDelta = new Vector2((float)tileCols * tileSize, 3f);
			component3.anchoredPosition = new Vector2(0f, (float)j * tileSize);
			component4.Points = new Vector2[2]
			{
				Vector2.zero,
				new Vector2(1f, 0f)
			};
			component4.color = gridLineColor;
			gridLines[num3] = component4;
			num3++;
		}
	}

	public void ColorGridLines(Color color)
	{
		for (int i = 0; i < gridLines.Length; i++)
		{
			gridLines[i].color = color;
		}
	}

	public void UnlockTiles()
	{
		StartCoroutine("UnlockRoutine");
	}

	private void Shuffle(ref int[] array)
	{
		int num = array.Length;
		while (num > 1)
		{
			int num2 = rand.Next(num--);
			int num3 = array[num];
			array[num] = array[num2];
			array[num2] = num3;
		}
	}

	private IEnumerator UnlockRoutine()
	{
		int[] nums = new int[tiles.Length];
		for (int j = 0; j < nums.Length; j++)
		{
			nums[j] = j;
		}
		Shuffle(ref nums);
		ps.Play();
		for (int i = 0; i < tiles.Length; i++)
		{
			if (tiles[nums[i]].isBlank && tiles[nums[i]].uiGemLock.isLocked)
			{
				yield return new WaitForSeconds(0.025f);
				tiles[nums[i]].Unlock();
			}
		}
		ps.Stop();
	}

	public void BurstAtLocation(Vector3 position)
	{
		ps.transform.localPosition = position + new Vector3(tileSize / 2f, tileSize / 2f, 0f);
		ps.Emit(maxParticles);
	}

	public void ToggleScrollable(bool scrollable)
	{
		if (scrollable)
		{
			base.gameObject.transform.GetComponent<ScrollRect>().enabled = true;
		}
		else
		{
			base.gameObject.transform.GetComponent<ScrollRect>().enabled = false;
		}
	}
}
