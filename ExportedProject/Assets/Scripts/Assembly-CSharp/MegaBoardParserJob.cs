using System;
using System.Threading;

public class MegaBoardParserJob : ThreadedJob
{
	public delegate void HandleParseComplete(BloxelMegaBoard _mega);

	public bool isRunning;

	public string responseFromServer;

	public BloxelMegaBoard megaBoard;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		megaBoard = new BloxelMegaBoard(responseFromServer, DataSource.JSONString);
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(megaBoard);
		}
	}
}
