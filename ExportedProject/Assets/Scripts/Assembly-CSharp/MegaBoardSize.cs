public class MegaBoardSize
{
	public int c;

	public int r;

	public MegaBoardSize(int col, int row)
	{
		c = col;
		r = row;
	}

	public override string ToString()
	{
		return "[Size: c=" + c + " r=" + r + "]";
	}
}
