using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MegaBoardTile : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
{
	public delegate void HandleProjectSelect(MegaBoardTile tile);

	public delegate void HandleDragStart(MegaBoardTile tile);

	public delegate void HandleDragEnd(MegaBoardTile tile, bool shouldErase);

	public RectTransform rect;

	public BloxelProject project;

	public bool isActive;

	public GridLocation loc;

	public bool isBlank = true;

	public bool pointerDownWhileZoomed;

	public bool interactable = true;

	public Color uiBoardColor;

	public RawImage image;

	public CoverBoard cover;

	private UIPopupGemUnlock _uiPopupGemUnlock;

	public UIGemLock uiGemLock;

	public Graphic uiImageAdd;

	public Image mainImage;

	public RectTransform dropzone;

	private bool _dropzoneActive;

	public BloxelLevel tmpLevelBuffer;

	private Color clearColor = new Color(0f, 0f, 0f, 0f);

	private Color tmpImageColor;

	public float gemLockScale = 0.4f;

	public float iconAddScale = 0.8f;

	private ScrollRect _scrollRect;

	public event HandleProjectSelect OnSelect;

	public event HandleDragStart OnDragStart;

	public event HandleDragEnd OnDragEnd;

	private void Awake()
	{
		cover = new CoverBoard(new CoverStyle(clearColor, 0));
	}

	private void Start()
	{
		uiGemLock.Init(new MapRoomUnlockable(loc));
		uiGemLock.ManualUnlock();
		uiImageAdd.enabled = false;
		_scrollRect = GetComponentInParent<ScrollRect>();
		dropzone.localScale = Vector3.zero;
	}

	public void Init(BloxelProject _project, Sprite sprite, bool isCurrent = false)
	{
		project = _project;
		dropzone.localScale = Vector3.zero;
		BuildTexture();
		if (project == null)
		{
			mainImage.rectTransform.localScale = Vector3.zero;
			isBlank = true;
			image.color = clearColor;
			ClearDetailTexture(sprite);
			if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
			{
				mainImage.sprite = sprite;
				uiImageAdd.enabled = true;
				uiGemLock.ManualUnlock();
			}
			else
			{
				uiImageAdd.enabled = false;
				uiGemLock.ManualUnlock();
			}
		}
		else
		{
			if (project.type == ProjectType.Level)
			{
				((BloxelLevel)_project).SetDetailTextureForSprite(sprite, BloxelBoard.BaseBoardDetailLookupColor32);
				mainImage.sprite = sprite;
				mainImage.rectTransform.localScale = Vector3.one;
			}
			else
			{
				mainImage.rectTransform.localScale = Vector3.zero;
				ClearDetailTexture(sprite);
			}
			isBlank = false;
			image.color = Color.white;
			uiGemLock.ManualUnlock();
			uiImageAdd.enabled = false;
		}
	}

	public void SwapAddForGemLock()
	{
		uiImageAdd.enabled = false;
		uiGemLock.ManualLock(gemLockScale);
	}

	public void SwapGemLockForDropzone()
	{
		uiGemLock.ManualUnlock();
		dropzone.localScale = Vector3.one;
	}

	public Tweener Unlock()
	{
		if (uiGemLock.isLocked)
		{
			uiGemLock.ManualUnlock();
		}
		GameBuilderCanvas.instance.uiStatRoomInventory.IncrementAvailable();
		uiImageAdd.enabled = true;
		return uiImageAdd.transform.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedMedium).OnStart(delegate
		{
			MegaBoardCanvas.instance.BurstAtLocation(base.transform.localPosition);
			SoundManager.instance.PlaySound(SoundManager.instance.megaTileUnlock);
		});
	}

	private void BuildTexture()
	{
		if (project == null)
		{
			cover.texture.SetPixels(AssetManager.instance.clearColors38x38);
			cover.texture.Apply();
		}
		else
		{
			cover.style.emptyColor = ((project.type != ProjectType.Level) ? clearColor : BloxelBoard.baseUIBoardColor);
			image.texture = project.coverBoard.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
		}
	}

	public void ClearDetailTexture(Sprite sprite)
	{
		Texture2D texture = sprite.texture;
		int x = (int)sprite.rect.x;
		int y = (int)sprite.rect.y;
		texture.SetPixels32(x, y, 169, 169, AssetManager.instance.clearDetailLookupColors32);
	}

	public void ShowDetailTexture()
	{
		if (!isBlank)
		{
			image.color = clearColor;
			mainImage.color = Color.white;
		}
	}

	public void ShowNormalTexture()
	{
		if (!isBlank)
		{
			mainImage.color = clearColor;
			image.color = Color.white;
		}
	}

	private void HandleGemUnlock(GemUnlockable model)
	{
		_uiPopupGemUnlock.OnUnlock -= HandleGemUnlock;
		Unlock();
	}

	private void HandleBulkUnlock()
	{
		_uiPopupGemUnlock.OnBulkRooms -= HandleBulkUnlock;
		MegaBoardCanvas.instance.UnlockTiles();
	}

	public void PaintSelectedBoard()
	{
		if (CheckValidPaintState() && CheckValidBoardPlacement())
		{
			SoundManager.PlayOneShot(SoundManager.instance.popC);
			project = MegaBoardCanvas.instance.currentBloxelBoard;
			cover.style.emptyColor = clearColor;
			image.texture = project.coverBoard.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
			image.color = Color.white;
			isBlank = false;
			uiImageAdd.enabled = false;
			BloxelMegaBoardLibrary.instance.currentMega.SetBoardAtLocation(loc, project as BloxelBoard);
		}
	}

	private bool CheckValidBoardPlacement()
	{
		BloxelMegaBoard currentMega = BloxelMegaBoardLibrary.instance.currentMega;
		BloxelBoard value = null;
		if (!currentMega.boards.TryGetValue(loc, out value))
		{
			return true;
		}
		if (currentMega.boards[loc] == MegaBoardCanvas.instance.currentBloxelBoard)
		{
			return false;
		}
		return true;
	}

	private bool CheckValidPaintState()
	{
		if (MegaBoardCanvas.instance.currentBloxelBoard != null && BloxelLibrary.instance.currentTabID == 4)
		{
			return true;
		}
		return false;
	}

	private void EraseBoard()
	{
		if (project != null && project.type == ProjectType.Board && BloxelMegaBoardLibrary.instance.currentMega.RemoveTileAtLocation(loc))
		{
			SoundManager.PlayOneShot(SoundManager.instance.eraser);
			image.texture = AssetManager.instance.boardSolidClear;
			image.color = clearColor;
			isBlank = true;
		}
	}

	private bool CanAddRoom()
	{
		if (PixelEditorController.instance.mode != CanvasMode.LevelEditor)
		{
			return false;
		}
		if (isBlank && uiGemLock.isLocked)
		{
			return false;
		}
		return true;
	}

	private void tryShowDropzone()
	{
		if (isBlank && PixelEditorController.instance.mode == CanvasMode.LevelEditor && MegaBoardCanvas.instance.isDragging && !(MegaBoardCanvas.instance.currentDraggable == null))
		{
			_dropzoneActive = true;
			MegaBoardCanvas.instance.currentActiveTile = this;
			tmpImageColor = image.color;
			image.DOColor(new Color(0.5f, 0.5f, 0.5f, 0.5f), UIAnimationManager.speedFast);
			uiImageAdd.enabled = false;
			dropzone.localScale = Vector3.one;
		}
	}

	private void tryHideDropzone()
	{
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor && MegaBoardCanvas.instance.isDragging && _dropzoneActive && isBlank)
		{
			uiImageAdd.enabled = true;
			dropzone.localScale = Vector3.zero;
			image.DOColor(tmpImageColor, UIAnimationManager.speedFast);
		}
	}

	public void OnPointerClick(PointerEventData pEvent)
	{
		if (MegaBoardCanvas.instance.isDragging || !interactable)
		{
			return;
		}
		if (PixelEditorController.instance.mode == CanvasMode.MegaBoard && MegaBoardCanvas.instance.uiDecorateTools.currentTool == null)
		{
			PaintSelectedBoard();
			return;
		}
		if (PixelEditorController.instance.mode == CanvasMode.MegaBoard && MegaBoardCanvas.instance.uiDecorateTools.currentTool != null)
		{
			EraseBoard();
			return;
		}
		if (!CanAddRoom())
		{
			_uiPopupGemUnlock = UIOverlayCanvas.instance.GemBuyPopup(uiGemLock.model);
			_uiPopupGemUnlock.OnUnlock += HandleGemUnlock;
			_uiPopupGemUnlock.OnBulkRooms += HandleBulkUnlock;
			return;
		}
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor && !ChallengeManager.instance.hasActiveChallenge)
		{
			GameBuilderCanvas.instance.MegaTileClicked(this);
		}
		if (this.OnSelect != null)
		{
			this.OnSelect(this);
		}
	}

	public void OnPointerEnter(PointerEventData pEvent)
	{
		if (PixelEditorController.instance.mode == CanvasMode.MegaBoard)
		{
			if (MegaBoardCanvas.instance.isDragging && MegaBoardCanvas.instance.currentScale <= 1f && MegaBoardCanvas.instance.uiDecorateTools.currentTool == null)
			{
				PaintSelectedBoard();
			}
			else if (MegaBoardCanvas.instance.isDragging && MegaBoardCanvas.instance.currentScale <= 1f)
			{
				EraseBoard();
			}
		}
		else
		{
			tryShowDropzone();
		}
	}

	public void OnPointerExit(PointerEventData eData)
	{
		tryHideDropzone();
	}

	public void OnBeginDrag(PointerEventData pEvent)
	{
		if (Input.touchCount > 1)
		{
			return;
		}
		MegaBoardCanvas.instance.isDragging = true;
		if (MegaBoardCanvas.instance.currentScale > 1f)
		{
			ExecuteEvents.beginDragHandler(_scrollRect, pEvent);
		}
		else if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			if (this.OnDragStart != null)
			{
				this.OnDragStart(this);
			}
		}
		else if (MegaBoardCanvas.instance.uiDecorateTools.currentTool != null)
		{
			EraseBoard();
		}
		else
		{
			PaintSelectedBoard();
		}
	}

	public void OnDrag(PointerEventData pEvent)
	{
		if (Input.touchCount <= 1)
		{
			if (MegaBoardCanvas.instance.currentScale > 1f)
			{
				ExecuteEvents.dragHandler(_scrollRect, pEvent);
			}
			else if (MegaBoardCanvas.instance.uiDecorateTools.currentTool != null)
			{
				EraseBoard();
			}
			else
			{
				PaintSelectedBoard();
			}
		}
	}

	public void OnEndDrag(PointerEventData pEvent)
	{
		if (Input.touchCount > 1)
		{
			return;
		}
		MegaBoardCanvas.instance.isDragging = false;
		if (MegaBoardCanvas.instance.currentScale > 1f)
		{
			ExecuteEvents.endDragHandler(_scrollRect, pEvent);
		}
		else if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			bool shouldErase = GameBuilderCanvas.instance.uiTrashCan.TryDelete();
			if (this.OnDragEnd != null)
			{
				this.OnDragEnd(this, shouldErase);
			}
		}
	}

	public void OnPointerDown(PointerEventData pEvent)
	{
		if (PixelEditorController.instance.mode != CanvasMode.MegaBoard)
		{
			tmpImageColor = image.color;
			image.DOColor(new Color(0.25f, 0.25f, 0.25f, 0.5f), UIAnimationManager.speedFast);
			if (MegaBoardCanvas.instance.currentScale > 1f)
			{
				pointerDownWhileZoomed = true;
			}
			else
			{
				pointerDownWhileZoomed = false;
			}
		}
	}

	public void OnPointerUp(PointerEventData pEvent)
	{
		if (PixelEditorController.instance.mode != CanvasMode.MegaBoard)
		{
			if (isBlank)
			{
				image.DOColor(tmpImageColor, UIAnimationManager.speedFast);
			}
			else if (!isBlank && !pointerDownWhileZoomed)
			{
				image.DOColor(tmpImageColor, UIAnimationManager.speedFast);
			}
		}
	}
}
