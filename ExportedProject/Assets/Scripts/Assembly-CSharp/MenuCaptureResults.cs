using DG.Tweening;
using DiscoCapture;
using UnityEngine;

public class MenuCaptureResults : UIMenuPanel
{
	public BloxelBrain tmpBrain;

	public UIBrainStats uiBrainStats;

	[Header("UI")]
	public UICaptureAdjustments uiCaptureAdjustments;

	private CaptureController _captureController;

	public CapturePreviewDisplay previewDisplay;

	private SequentialMenu _seqMenu;

	private new void Awake()
	{
		base.Awake();
		_seqMenu = GetComponent<SequentialMenu>();
	}

	private new void Start()
	{
		base.Start();
		OverrideDismiss();
		_seqMenu.UnsubscribeNext();
		_seqMenu.UnsubscribeBack();
		_seqMenu.uiButtonNext.OnClick += ConfirmCapture;
		_seqMenu.uiButtonBack.OnClick += RepeatCapture;
		_captureController = ((UIPopupCapture)controller).captureController;
	}

	private new void OnDestroy()
	{
		_seqMenu.uiButtonNext.OnClick -= ConfirmCapture;
		_seqMenu.uiButtonBack.OnClick -= RepeatCapture;
		_captureController.OnBlockClassified -= handleBlockClassified;
		_captureController.RemoveListener(CaptureEvent.ClassificationStart, HandleClassificationStart);
		_captureController.RemoveListener(CaptureEvent.ClassificationComplete, HandleClassificationComplete);
		base.OnDestroy();
	}

	public new void Init()
	{
		if (!((UIPopupCapture)controller).isBrainCapture)
		{
			uiBrainStats.transform.localScale = Vector3.zero;
		}
		_captureController.StartClassification();
		_captureController.OnBlockClassified += handleBlockClassified;
		_captureController.AddListener(CaptureEvent.ClassificationStart, HandleClassificationStart);
		_captureController.AddListener(CaptureEvent.ClassificationComplete, HandleClassificationComplete);
	}

	private void handleBlockClassified(int col, int row, BlockColor color)
	{
		previewDisplay.UpdateBlock(col, row, color, true);
	}

	public void HandleClassificationStart()
	{
		_seqMenu.uiButtonBack.interactable = false;
		_seqMenu.uiButtonNext.interactable = false;
	}

	public void HandleClassificationComplete()
	{
		_seqMenu.uiButtonBack.interactable = true;
		_seqMenu.uiButtonNext.interactable = true;
		if (((UIPopupCapture)controller).isBrainCapture)
		{
			tmpBrain = new BloxelBrain(ColorClassifier.BlockColors.Clone() as BlockColor[,]);
			uiBrainStats.Init(tmpBrain, ((UIPopupCapture)controller).enemyTile);
		}
		else
		{
			uiBrainStats.transform.localScale = Vector3.zero;
		}
		StartCoroutine(uiCaptureAdjustments.Init());
	}

	public void RepeatCapture()
	{
		_captureController.RemoveListener(CaptureEvent.ClassificationComplete, HandleClassificationComplete);
		_captureController.OnBlockClassified -= handleBlockClassified;
		AnalyticsManager.SendEventBool(AnalyticsManager.Event.CaptureConfirm, false);
		SoundManager.PlayOneShot(SoundManager.instance.cancelA);
		((UIPopupCapture)controller).menuCaptureViewport.uiProgressBar.Reset();
		((UIPopupCapture)controller).CaptureRepeat();
		if (((UIPopupCapture)controller).isBrainCapture && uiBrainStats.isVisible)
		{
			uiBrainStats.Hide().OnComplete(delegate
			{
				_seqMenu.Back();
			});
		}
		else
		{
			_seqMenu.Back();
		}
	}

	public void ConfirmCapture()
	{
		_captureController.RemoveListener(CaptureEvent.ClassificationComplete, HandleClassificationComplete);
		_captureController.OnBlockClassified -= handleBlockClassified;
		AnalyticsManager.SendEventBool(AnalyticsManager.Event.CaptureConfirm, true);
		SoundManager.PlayOneShot(SoundManager.instance.gameboardUnlock);
		((UIPopupCapture)controller).CaptureComplete();
	}

	public void HandleYellowServerValues(float min, float max)
	{
		uiCaptureAdjustments.minWarmthAmount = min;
		uiCaptureAdjustments.maxWarmthAmount = max;
	}
}
