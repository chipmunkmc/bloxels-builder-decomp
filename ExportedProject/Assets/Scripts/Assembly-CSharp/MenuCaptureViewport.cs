using System.Collections;
using DG.Tweening;
using DiscoCapture;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class MenuCaptureViewport : UIMenuPanel
{
	[Header("UI")]
	public RectTransform container;

	public RectTransform selfRectTransform;

	public RectTransform rectModeToggle;

	public RectTransform rectMessage;

	public RectTransform rectProgressContainer;

	public UIProgressBar uiProgressBar;

	public RawImage uiRawImage;

	public Color rendererInitializeColor;

	public RectTransform rendererInitMessage;

	public UIButton uiButtonHelp;

	public RectTransform rectHelpOverlay;

	public UICaptureFlipToggle _uiFlipToggle;

	public GameObject retardDerpDerp;

	public ParticleSystem confetti;

	private UIInteractable _helpOverlay;

	private UICaptureModeToggle _uiCaptureModeToggle;

	private TextMeshProUGUI _uiTextMessage;

	private Vector2 _androidSizeDelta = new Vector2(800f, 654f);

	private CaptureController _captureController;

	private SequentialMenu _seqMenu;

	[Header("State Tracking")]
	public float helpTimer = 5f;

	private bool _helpVisible;

	private int _successfulPasses;

	private float _timer;

	private bool _helpOverlayActive;

	private int _progressBarIndex;

	private new void Awake()
	{
		base.Awake();
		_seqMenu = GetComponent<SequentialMenu>();
		_uiTextMessage = rectMessage.GetComponentInChildren<TextMeshProUGUI>();
		_uiCaptureModeToggle = rectModeToggle.GetComponentInChildren<UICaptureModeToggle>();
		_helpOverlay = rectHelpOverlay.GetComponent<UIInteractable>();
		selfRectTransform = GetComponent<RectTransform>();
		rectMessage.localScale = Vector3.zero;
		rectModeToggle.localScale = Vector3.zero;
		rectHelpOverlay.localScale = Vector3.zero;
		uiRawImage.color = rendererInitializeColor;
	}

	private new void Start()
	{
		base.Start();
		OverrideDismiss();
		uiButtonDismiss.OnClick += Dismiss;
		uiButtonHelp.OnClick += HandleHelpOverlay;
		_helpOverlay.OnClick += HandleHelpOverlay;
		_captureController = ((UIPopupCapture)controller).captureController;
		_captureController.AddListener(CaptureEvent.ProcessPass, HandleProcessPass);
		_captureController.AddListener(CaptureEvent.ProcessComplete, HandleProcessComplete);
		_uiCaptureModeToggle.OnToggle += HandleCaptureModeToggle;
		_uiFlipToggle.OnToggle += HandleCaptureFlipToggle;
		StartCoroutine("HelpTimer");
	}

	private void Update()
	{
		if (Input.deviceOrientation == DeviceOrientation.LandscapeRight)
		{
			if (SystemInfo.deviceModel.Contains("5X") || SystemInfo.deviceModel.Contains("KFSAW"))
			{
				retardDerpDerp.SetActive(false);
			}
			else
			{
				retardDerpDerp.SetActive(true);
			}
		}
		else
		{
			retardDerpDerp.SetActive(false);
		}
	}

	private new void OnDestroy()
	{
		_captureController.RemoveListener(CaptureEvent.ProcessPass, HandleProcessPass);
		_captureController.RemoveListener(CaptureEvent.ProcessComplete, HandleProcessComplete);
		uiButtonDismiss.OnClick -= Dismiss;
		_uiCaptureModeToggle.OnToggle -= HandleCaptureModeToggle;
		_uiFlipToggle.OnToggle -= HandleCaptureFlipToggle;
		uiButtonHelp.OnClick -= HandleHelpOverlay;
		_helpOverlay.OnClick -= HandleHelpOverlay;
		base.OnDestroy();
	}

	public override void Dismiss()
	{
		CaptureManager.instance.DismissPopup();
	}

	private void HandleHelpOverlay()
	{
		SoundManager.PlayOneShot(SoundManager.instance.appearance);
		if (!_helpOverlayActive)
		{
			rectHelpOverlay.localScale = Vector3.one;
			if (!_helpVisible)
			{
				StopCoroutine("HelpTimer");
				ShowHelp();
			}
			CaptureManager.instance.shouldProcess = false;
		}
		else
		{
			rectHelpOverlay.localScale = Vector3.zero;
			CaptureManager.instance.shouldProcess = true;
		}
		_helpOverlayActive = !_helpOverlayActive;
	}

	private void HandleCaptureFlipToggle(bool flip)
	{
		PrefsManager.instance.shouldFlipCamera = flip;
		CaptureManager.instance.ToggleCaptureFlip(flip);
	}

	private void HandleCaptureModeToggle(bool isGrid)
	{
		CaptureManager.instance.ToggleBoardFindingMode(isGrid);
	}

	private void HandleProcessPass()
	{
		if (_progressBarIndex < uiProgressBar.elements.Length - 2)
		{
			uiProgressBar.Increment();
			uiProgressBar.Increment();
			_progressBarIndex += 2;
		}
		_successfulPasses++;
	}

	private void HandleProcessComplete()
	{
		_progressBarIndex = 0;
		uiProgressBar.Increment();
		SoundManager.instance.PlaySound(SoundManager.instance.challengeUnlock);
		CaptureManager.instance.ResetOnProcessComplete();
		confetti.Play();
		uiProgressBar.Reset();
		((UIPopupCapture)controller).menuCaptureResults.Init();
		_seqMenu.Next();
	}

	private IEnumerator HelpTimer()
	{
		yield return new WaitForSeconds(helpTimer);
		if (_successfulPasses < 2)
		{
			ShowHelp();
		}
	}

	private void ShowHelp()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(rectMessage.DOScaleY(1f, UIAnimationManager.speedFast));
		sequence.Append(rectMessage.DOScaleX(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		sequence.Append(rectModeToggle.DOScale(1f, UIAnimationManager.speedMedium));
		sequence.Append(rectModeToggle.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f));
		sequence.Play();
		_helpVisible = true;
	}

	public void SetRenderer()
	{
		uiRawImage.texture = CaptureManager.instance.displayTexture;
		uiRawImage.color = Color.white;
		rendererInitMessage.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce);
	}
}
