using System.Collections.Generic;
using UnityEngine;

public class MeshCacheManager : MonoBehaviour
{
	public static MeshCacheManager Instance;

	private static bool _InitComplete;

	public int initialSize;

	public Dictionary<int, CachedMesh> cachedMeshes;

	[HideInInspector]
	public List<int> idsToRemove = new List<int>(64);

	private void Awake()
	{
		if (Instance != null && Instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			Instance = this;
			_InitComplete = true;
			cachedMeshes = new Dictionary<int, CachedMesh>(initialSize);
			CachedMesh.CacheManager = Instance;
		}
	}

	private void Update()
	{
		Dictionary<int, CachedMesh>.Enumerator enumerator = cachedMeshes.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.Value.UpdateTick();
		}
		int count = idsToRemove.Count;
		List<int>.Enumerator enumerator2 = idsToRemove.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			cachedMeshes.Remove(enumerator2.Current);
		}
		idsToRemove.Clear();
	}
}
