using System.Collections.Generic;
using UnityEngine;
using VoxelEngine;

public class MovableChunk2DPool : MonoBehaviour
{
	public static MovableChunk2DPool instance;

	public Transform selfTransform;

	public int poolSize;

	public int poopPoolSize;

	public int poolExpansionSize;

	private static bool _InitComplete;

	public Stack<MovableChunk2D> pool { get; private set; }

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			instance = this;
			_InitComplete = true;
			Object.DontDestroyOnLoad(base.gameObject);
			selfTransform = base.transform;
			Initialize();
		}
	}

	private void Initialize()
	{
		if (SystemInfo.systemMemorySize < 750)
		{
			poolSize = 1200;
		}
		pool = new Stack<MovableChunk2D>(poolSize);
		AllocatePoolObjects();
	}

	private void AllocatePoolObjects()
	{
		for (int i = 0; i < poolSize; i++)
		{
			MovableChunk2D movableChunk2D = new MovableChunk2D();
			movableChunk2D.EarlyAwake();
			pool.Push(movableChunk2D);
		}
	}

	public MovableChunk2D Spawn()
	{
		if (pool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				MovableChunk2D movableChunk2D = new MovableChunk2D();
				movableChunk2D.EarlyAwake();
				pool.Push(movableChunk2D);
			}
		}
		MovableChunk2D movableChunk2D2 = pool.Pop();
		movableChunk2D2.PrepareSpawn();
		return movableChunk2D2;
	}
}
