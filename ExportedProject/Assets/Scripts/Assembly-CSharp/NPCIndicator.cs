using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public sealed class NPCIndicator : PoolableComponent
{
	public Canvas theCanvas;

	public BloxelNPCTile tile;

	public RectTransform rect;

	public Image icon;

	public Button uiButtonOpen;

	public Vector2 position;

	public override void EarlyAwake()
	{
		if (!(selfTransform != null))
		{
			selfTransform = base.transform;
			theCanvas.worldCamera = BloxelCamera.instance.mainCamera;
		}
	}

	public override void PrepareSpawn()
	{
		base.gameObject.SetActive(true);
		spawned = true;
		uiButtonOpen.onClick.AddListener(OpenSpeechBubble);
		Bounce();
	}

	public override void PrepareDespawn()
	{
		rect.DOKill(true);
		uiButtonOpen.onClick.RemoveListener(OpenSpeechBubble);
		base.gameObject.SetActive(false);
		spawned = false;
	}

	public override void Despawn()
	{
		PrepareDespawn();
		NPCIndicatorPool.instance.pool.Push(this);
	}

	private void Bounce()
	{
		if (!spawned)
		{
			return;
		}
		rect.DOAnchorPos(new Vector2(position.x, position.y + 1f), UIAnimationManager.speedMedium).OnComplete(delegate
		{
			rect.DOAnchorPos(new Vector2(position.x, position.y), UIAnimationManager.speedMedium).OnComplete(delegate
			{
				Bounce();
			});
		});
	}

	public void OpenSpeechBubble()
	{
		tile.AttachSpeechBubble();
	}
}
