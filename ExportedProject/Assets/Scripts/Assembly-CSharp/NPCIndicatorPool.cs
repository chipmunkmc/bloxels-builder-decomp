using System.Collections.Generic;
using UnityEngine;

public class NPCIndicatorPool : MonoBehaviour
{
	public static NPCIndicatorPool instance;

	public Transform selfTransform;

	public int poolSize;

	public int poolExpansionSize;

	public GameObject prefab;

	private static bool _InitComplete;

	public Stack<NPCIndicator> pool { get; private set; }

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			instance = this;
			_InitComplete = true;
			Object.DontDestroyOnLoad(base.gameObject);
			selfTransform = base.transform;
			Initialize();
		}
	}

	private void Initialize()
	{
		pool = new Stack<NPCIndicator>(poolSize);
		AllocatePoolObjects();
	}

	private void AllocatePoolObjects()
	{
		for (int i = 0; i < poolSize; i++)
		{
			GameObject gameObject = Object.Instantiate(prefab);
			gameObject.name = prefab.name;
			NPCIndicator component = gameObject.GetComponent<NPCIndicator>();
			component.EarlyAwake();
			component.selfTransform.SetParent(selfTransform);
			gameObject.SetActive(false);
			pool.Push(component);
		}
	}

	public NPCIndicator Spawn()
	{
		if (pool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(prefab);
				gameObject.name = prefab.name;
				NPCIndicator component = gameObject.GetComponent<NPCIndicator>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				pool.Push(component);
			}
		}
		NPCIndicator nPCIndicator = pool.Pop();
		nPCIndicator.PrepareSpawn();
		return nPCIndicator;
	}

	public void PrepareForSceneChange()
	{
		Stack<NPCIndicator>.Enumerator enumerator = pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.selfTransform.SetParent(selfTransform);
			Object.DontDestroyOnLoad(enumerator.Current.gameObject);
		}
	}
}
