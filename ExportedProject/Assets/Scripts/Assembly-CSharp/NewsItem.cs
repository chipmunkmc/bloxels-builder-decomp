using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;
using UnityEngine;

public class NewsItem
{
	public enum Type
	{
		None = 0,
		Link = 1,
		Warp = 2
	}

	public Type type;

	private string _serverID;

	private string _title;

	private string _body;

	private DateTime _date;

	private string _imageURL;

	private string _interactive;

	public Texture2D image;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map10;

	public string serverID
	{
		get
		{
			return _serverID;
		}
		set
		{
			_serverID = value;
		}
	}

	public string title
	{
		get
		{
			return _title;
		}
		private set
		{
			_title = value;
		}
	}

	public string body
	{
		get
		{
			return _body;
		}
		private set
		{
			_body = value;
		}
	}

	public DateTime date
	{
		get
		{
			return _date;
		}
		private set
		{
			_date = value;
		}
	}

	public string imageURL
	{
		get
		{
			return _imageURL;
		}
		private set
		{
			_imageURL = value;
		}
	}

	public string interactive
	{
		get
		{
			return _interactive;
		}
		private set
		{
			_interactive = value;
		}
	}

	public NewsItem(string jsonString)
	{
		FromJSONString(jsonString);
	}

	public void FromJSONString(string jsonString)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader);
	}

	public void FromReader(JsonTextReader reader)
	{
		string text = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
				}
				else
				{
					if (text == null)
					{
						continue;
					}
					if (_003C_003Ef__switch_0024map10 == null)
					{
						Dictionary<string, int> dictionary = new Dictionary<string, int>(7);
						dictionary.Add("_id", 0);
						dictionary.Add("createdAt", 1);
						dictionary.Add("title", 2);
						dictionary.Add("body", 3);
						dictionary.Add("imageURL", 4);
						dictionary.Add("interactive", 5);
						dictionary.Add("type", 6);
						_003C_003Ef__switch_0024map10 = dictionary;
					}
					int value;
					if (_003C_003Ef__switch_0024map10.TryGetValue(text, out value))
					{
						switch (value)
						{
						case 0:
							_serverID = reader.Value.ToString();
							break;
						case 1:
							date = Convert.ToDateTime(reader.Value.ToString());
							break;
						case 2:
							title = reader.Value.ToString();
							break;
						case 3:
							body = reader.Value.ToString();
							break;
						case 4:
							imageURL = reader.Value.ToString();
							break;
						case 5:
							interactive = reader.Value.ToString();
							break;
						case 6:
							type = (Type)int.Parse(reader.Value.ToString());
							break;
						}
					}
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				break;
			}
		}
	}
}
