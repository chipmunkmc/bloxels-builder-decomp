using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class ANN_MLP : StatModel
	{
		public const int BACKPROP = 0;

		public const int RPROP = 1;

		public const int IDENTITY = 0;

		public const int SIGMOID_SYM = 1;

		public const int GAUSSIAN = 2;

		public const int UPDATE_WEIGHTS = 1;

		public const int NO_INPUT_SCALE = 2;

		public const int NO_OUTPUT_SCALE = 4;

		private const string LIBNAME = "opencvforunity";

		protected ANN_MLP(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_ANN_1MLP_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getLayerSizes()
		{
			ThrowIfDisposed();
			return new Mat(ml_ANN_1MLP_getLayerSizes_10(nativeObj));
		}

		public Mat getWeights(int layerIdx)
		{
			ThrowIfDisposed();
			return new Mat(ml_ANN_1MLP_getWeights_10(nativeObj, layerIdx));
		}

		public static ANN_MLP create()
		{
			return new ANN_MLP(ml_ANN_1MLP_create_10());
		}

		public TermCriteria getTermCriteria()
		{
			ThrowIfDisposed();
			double[] vals = new double[3];
			ml_ANN_1MLP_getTermCriteria_10(nativeObj, vals);
			return new TermCriteria(vals);
		}

		public double getBackpropMomentumScale()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getBackpropMomentumScale_10(nativeObj);
		}

		public double getBackpropWeightScale()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getBackpropWeightScale_10(nativeObj);
		}

		public double getRpropDW0()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getRpropDW0_10(nativeObj);
		}

		public double getRpropDWMax()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getRpropDWMax_10(nativeObj);
		}

		public double getRpropDWMin()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getRpropDWMin_10(nativeObj);
		}

		public double getRpropDWMinus()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getRpropDWMinus_10(nativeObj);
		}

		public double getRpropDWPlus()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getRpropDWPlus_10(nativeObj);
		}

		public int getTrainMethod()
		{
			ThrowIfDisposed();
			return ml_ANN_1MLP_getTrainMethod_10(nativeObj);
		}

		public void setActivationFunction(int type, double param1, double param2)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setActivationFunction_10(nativeObj, type, param1, param2);
		}

		public void setActivationFunction(int type)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setActivationFunction_11(nativeObj, type);
		}

		public void setBackpropMomentumScale(double val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setBackpropMomentumScale_10(nativeObj, val);
		}

		public void setBackpropWeightScale(double val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setBackpropWeightScale_10(nativeObj, val);
		}

		public void setLayerSizes(Mat _layer_sizes)
		{
			ThrowIfDisposed();
			if (_layer_sizes != null)
			{
				_layer_sizes.ThrowIfDisposed();
			}
			ml_ANN_1MLP_setLayerSizes_10(nativeObj, _layer_sizes.nativeObj);
		}

		public void setRpropDW0(double val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setRpropDW0_10(nativeObj, val);
		}

		public void setRpropDWMax(double val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setRpropDWMax_10(nativeObj, val);
		}

		public void setRpropDWMin(double val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setRpropDWMin_10(nativeObj, val);
		}

		public void setRpropDWMinus(double val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setRpropDWMinus_10(nativeObj, val);
		}

		public void setRpropDWPlus(double val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setRpropDWPlus_10(nativeObj, val);
		}

		public void setTermCriteria(TermCriteria val)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setTermCriteria_10(nativeObj, val.type, val.maxCount, val.epsilon);
		}

		public void setTrainMethod(int method, double param1, double param2)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setTrainMethod_10(nativeObj, method, param1, param2);
		}

		public void setTrainMethod(int method)
		{
			ThrowIfDisposed();
			ml_ANN_1MLP_setTrainMethod_11(nativeObj, method);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_ANN_1MLP_getLayerSizes_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_ANN_1MLP_getWeights_10(IntPtr nativeObj, int layerIdx);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_ANN_1MLP_create_10();

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_getTermCriteria_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern double ml_ANN_1MLP_getBackpropMomentumScale_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_ANN_1MLP_getBackpropWeightScale_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_ANN_1MLP_getRpropDW0_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_ANN_1MLP_getRpropDWMax_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_ANN_1MLP_getRpropDWMin_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_ANN_1MLP_getRpropDWMinus_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_ANN_1MLP_getRpropDWPlus_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_ANN_1MLP_getTrainMethod_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setActivationFunction_10(IntPtr nativeObj, int type, double param1, double param2);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setActivationFunction_11(IntPtr nativeObj, int type);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setBackpropMomentumScale_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setBackpropWeightScale_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setLayerSizes_10(IntPtr nativeObj, IntPtr _layer_sizes_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setRpropDW0_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setRpropDWMax_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setRpropDWMin_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setRpropDWMinus_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setRpropDWPlus_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setTermCriteria_10(IntPtr nativeObj, int val_type, int val_maxCount, double val_epsilon);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setTrainMethod_10(IntPtr nativeObj, int method, double param1, double param2);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_setTrainMethod_11(IntPtr nativeObj, int method);

		[DllImport("opencvforunity")]
		private static extern void ml_ANN_1MLP_delete(IntPtr nativeObj);
	}
}
