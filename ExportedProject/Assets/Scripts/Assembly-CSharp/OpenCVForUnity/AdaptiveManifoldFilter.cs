using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class AdaptiveManifoldFilter : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public AdaptiveManifoldFilter(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_AdaptiveManifoldFilter_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public static AdaptiveManifoldFilter create()
		{
			return new AdaptiveManifoldFilter(ximgproc_AdaptiveManifoldFilter_create_10());
		}

		public void collectGarbage()
		{
			ThrowIfDisposed();
			ximgproc_AdaptiveManifoldFilter_collectGarbage_10(nativeObj);
		}

		public void filter(Mat src, Mat dst, Mat joint)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (joint != null)
			{
				joint.ThrowIfDisposed();
			}
			ximgproc_AdaptiveManifoldFilter_filter_10(nativeObj, src.nativeObj, dst.nativeObj, joint.nativeObj);
		}

		public void filter(Mat src, Mat dst)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_AdaptiveManifoldFilter_filter_11(nativeObj, src.nativeObj, dst.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_AdaptiveManifoldFilter_create_10();

		[DllImport("opencvforunity")]
		private static extern void ximgproc_AdaptiveManifoldFilter_collectGarbage_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_AdaptiveManifoldFilter_filter_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr joint_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_AdaptiveManifoldFilter_filter_11(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_AdaptiveManifoldFilter_delete(IntPtr nativeObj);
	}
}
