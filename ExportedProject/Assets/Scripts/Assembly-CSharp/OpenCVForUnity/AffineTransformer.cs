using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class AffineTransformer : ShapeTransformer
	{
		private const string LIBNAME = "opencvforunity";

		public AffineTransformer(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_AffineTransformer_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool getFullAffine()
		{
			ThrowIfDisposed();
			return shape_AffineTransformer_getFullAffine_10(nativeObj);
		}

		public void setFullAffine(bool fullAffine)
		{
			ThrowIfDisposed();
			shape_AffineTransformer_setFullAffine_10(nativeObj, fullAffine);
		}

		[DllImport("opencvforunity")]
		private static extern bool shape_AffineTransformer_getFullAffine_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_AffineTransformer_setFullAffine_10(IntPtr nativeObj, bool fullAffine);

		[DllImport("opencvforunity")]
		private static extern void shape_AffineTransformer_delete(IntPtr nativeObj);
	}
}
