using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Algorithm : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		protected Algorithm(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						core_Algorithm_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public string getDefaultName()
		{
			ThrowIfDisposed();
			return Marshal.PtrToStringAnsi(core_Algorithm_getDefaultName_10(nativeObj));
		}

		public void clear()
		{
			ThrowIfDisposed();
			core_Algorithm_clear_10(nativeObj);
		}

		public virtual void save(string filename)
		{
			ThrowIfDisposed();
			core_Algorithm_save_10(nativeObj, filename);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Algorithm_getDefaultName_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Algorithm_clear_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Algorithm_save_10(IntPtr nativeObj, string filename);

		[DllImport("opencvforunity")]
		private static extern void core_Algorithm_delete(IntPtr nativeObj);
	}
}
