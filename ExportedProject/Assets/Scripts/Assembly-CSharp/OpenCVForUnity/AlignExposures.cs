using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class AlignExposures : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected AlignExposures(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_AlignExposures_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public virtual void process(List<Mat> src, List<Mat> dst, Mat times, Mat response)
		{
			ThrowIfDisposed();
			if (times != null)
			{
				times.ThrowIfDisposed();
			}
			if (response != null)
			{
				response.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			Mat mat2 = Converters.vector_Mat_to_Mat(dst);
			photo_AlignExposures_process_10(nativeObj, mat.nativeObj, mat2.nativeObj, times.nativeObj, response.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void photo_AlignExposures_process_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_mat_nativeObj, IntPtr times_nativeObj, IntPtr response_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignExposures_delete(IntPtr nativeObj);
	}
}
