using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class AlignMTB : AlignExposures
	{
		private const string LIBNAME = "opencvforunity";

		public AlignMTB(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_AlignMTB_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Point calculateShift(Mat img0, Mat img1)
		{
			ThrowIfDisposed();
			if (img0 != null)
			{
				img0.ThrowIfDisposed();
			}
			if (img1 != null)
			{
				img1.ThrowIfDisposed();
			}
			double[] vals = new double[2];
			photo_AlignMTB_calculateShift_10(nativeObj, img0.nativeObj, img1.nativeObj, vals);
			return new Point(vals);
		}

		public bool getCut()
		{
			ThrowIfDisposed();
			return photo_AlignMTB_getCut_10(nativeObj);
		}

		public int getExcludeRange()
		{
			ThrowIfDisposed();
			return photo_AlignMTB_getExcludeRange_10(nativeObj);
		}

		public int getMaxBits()
		{
			ThrowIfDisposed();
			return photo_AlignMTB_getMaxBits_10(nativeObj);
		}

		public void computeBitmaps(Mat img, Mat tb, Mat eb)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (tb != null)
			{
				tb.ThrowIfDisposed();
			}
			if (eb != null)
			{
				eb.ThrowIfDisposed();
			}
			photo_AlignMTB_computeBitmaps_10(nativeObj, img.nativeObj, tb.nativeObj, eb.nativeObj);
		}

		public override void process(List<Mat> src, List<Mat> dst, Mat times, Mat response)
		{
			ThrowIfDisposed();
			if (times != null)
			{
				times.ThrowIfDisposed();
			}
			if (response != null)
			{
				response.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			Mat mat2 = Converters.vector_Mat_to_Mat(dst);
			photo_AlignMTB_process_10(nativeObj, mat.nativeObj, mat2.nativeObj, times.nativeObj, response.nativeObj);
		}

		public void process(List<Mat> src, List<Mat> dst)
		{
			ThrowIfDisposed();
			Mat mat = Converters.vector_Mat_to_Mat(src);
			Mat mat2 = Converters.vector_Mat_to_Mat(dst);
			photo_AlignMTB_process_11(nativeObj, mat.nativeObj, mat2.nativeObj);
		}

		public void setCut(bool value)
		{
			ThrowIfDisposed();
			photo_AlignMTB_setCut_10(nativeObj, value);
		}

		public void setExcludeRange(int exclude_range)
		{
			ThrowIfDisposed();
			photo_AlignMTB_setExcludeRange_10(nativeObj, exclude_range);
		}

		public void setMaxBits(int max_bits)
		{
			ThrowIfDisposed();
			photo_AlignMTB_setMaxBits_10(nativeObj, max_bits);
		}

		public void shiftMat(Mat src, Mat dst, Point shift)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_AlignMTB_shiftMat_10(nativeObj, src.nativeObj, dst.nativeObj, shift.x, shift.y);
		}

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_calculateShift_10(IntPtr nativeObj, IntPtr img0_nativeObj, IntPtr img1_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern bool photo_AlignMTB_getCut_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int photo_AlignMTB_getExcludeRange_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int photo_AlignMTB_getMaxBits_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_computeBitmaps_10(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr tb_nativeObj, IntPtr eb_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_process_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_mat_nativeObj, IntPtr times_nativeObj, IntPtr response_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_process_11(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_setCut_10(IntPtr nativeObj, bool value);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_setExcludeRange_10(IntPtr nativeObj, int exclude_range);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_setMaxBits_10(IntPtr nativeObj, int max_bits);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_shiftMat_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, double shift_x, double shift_y);

		[DllImport("opencvforunity")]
		private static extern void photo_AlignMTB_delete(IntPtr nativeObj);
	}
}
