using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class BackgroundSubtractor : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected BackgroundSubtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						video_BackgroundSubtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void apply(Mat image, Mat fgmask, double learningRate)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (fgmask != null)
			{
				fgmask.ThrowIfDisposed();
			}
			video_BackgroundSubtractor_apply_10(nativeObj, image.nativeObj, fgmask.nativeObj, learningRate);
		}

		public void apply(Mat image, Mat fgmask)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (fgmask != null)
			{
				fgmask.ThrowIfDisposed();
			}
			video_BackgroundSubtractor_apply_11(nativeObj, image.nativeObj, fgmask.nativeObj);
		}

		public void getBackgroundImage(Mat backgroundImage)
		{
			ThrowIfDisposed();
			if (backgroundImage != null)
			{
				backgroundImage.ThrowIfDisposed();
			}
			video_BackgroundSubtractor_getBackgroundImage_10(nativeObj, backgroundImage.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractor_apply_10(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr fgmask_nativeObj, double learningRate);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractor_apply_11(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr fgmask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractor_getBackgroundImage_10(IntPtr nativeObj, IntPtr backgroundImage_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractor_delete(IntPtr nativeObj);
	}
}
