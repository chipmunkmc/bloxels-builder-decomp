using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class BackgroundSubtractorGMG : BackgroundSubtractor
	{
		private const string LIBNAME = "opencvforunity";

		public BackgroundSubtractorGMG(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						bgsegm_BackgroundSubtractorGMG_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool getUpdateBackgroundModel()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getUpdateBackgroundModel_10(nativeObj);
		}

		public double getBackgroundPrior()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getBackgroundPrior_10(nativeObj);
		}

		public double getDecisionThreshold()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getDecisionThreshold_10(nativeObj);
		}

		public double getDefaultLearningRate()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getDefaultLearningRate_10(nativeObj);
		}

		public double getMaxVal()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getMaxVal_10(nativeObj);
		}

		public double getMinVal()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getMinVal_10(nativeObj);
		}

		public int getMaxFeatures()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getMaxFeatures_10(nativeObj);
		}

		public int getNumFrames()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getNumFrames_10(nativeObj);
		}

		public int getQuantizationLevels()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getQuantizationLevels_10(nativeObj);
		}

		public int getSmoothingRadius()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorGMG_getSmoothingRadius_10(nativeObj);
		}

		public void setBackgroundPrior(double bgprior)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setBackgroundPrior_10(nativeObj, bgprior);
		}

		public void setDecisionThreshold(double thresh)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setDecisionThreshold_10(nativeObj, thresh);
		}

		public void setDefaultLearningRate(double lr)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setDefaultLearningRate_10(nativeObj, lr);
		}

		public void setMaxFeatures(int maxFeatures)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setMaxFeatures_10(nativeObj, maxFeatures);
		}

		public void setMaxVal(double val)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setMaxVal_10(nativeObj, val);
		}

		public void setMinVal(double val)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setMinVal_10(nativeObj, val);
		}

		public void setNumFrames(int nframes)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setNumFrames_10(nativeObj, nframes);
		}

		public void setQuantizationLevels(int nlevels)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setQuantizationLevels_10(nativeObj, nlevels);
		}

		public void setSmoothingRadius(int radius)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setSmoothingRadius_10(nativeObj, radius);
		}

		public void setUpdateBackgroundModel(bool update)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorGMG_setUpdateBackgroundModel_10(nativeObj, update);
		}

		[DllImport("opencvforunity")]
		private static extern bool bgsegm_BackgroundSubtractorGMG_getUpdateBackgroundModel_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double bgsegm_BackgroundSubtractorGMG_getBackgroundPrior_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double bgsegm_BackgroundSubtractorGMG_getDecisionThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double bgsegm_BackgroundSubtractorGMG_getDefaultLearningRate_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double bgsegm_BackgroundSubtractorGMG_getMaxVal_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double bgsegm_BackgroundSubtractorGMG_getMinVal_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int bgsegm_BackgroundSubtractorGMG_getMaxFeatures_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int bgsegm_BackgroundSubtractorGMG_getNumFrames_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int bgsegm_BackgroundSubtractorGMG_getQuantizationLevels_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int bgsegm_BackgroundSubtractorGMG_getSmoothingRadius_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setBackgroundPrior_10(IntPtr nativeObj, double bgprior);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setDecisionThreshold_10(IntPtr nativeObj, double thresh);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setDefaultLearningRate_10(IntPtr nativeObj, double lr);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setMaxFeatures_10(IntPtr nativeObj, int maxFeatures);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setMaxVal_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setMinVal_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setNumFrames_10(IntPtr nativeObj, int nframes);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setQuantizationLevels_10(IntPtr nativeObj, int nlevels);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setSmoothingRadius_10(IntPtr nativeObj, int radius);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_setUpdateBackgroundModel_10(IntPtr nativeObj, bool update);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorGMG_delete(IntPtr nativeObj);
	}
}
