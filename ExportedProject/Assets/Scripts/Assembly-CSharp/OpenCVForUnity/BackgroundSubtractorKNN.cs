using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class BackgroundSubtractorKNN : BackgroundSubtractor
	{
		private const string LIBNAME = "opencvforunity";

		public BackgroundSubtractorKNN(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						video_BackgroundSubtractorKNN_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool getDetectShadows()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorKNN_getDetectShadows_10(nativeObj);
		}

		public double getDist2Threshold()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorKNN_getDist2Threshold_10(nativeObj);
		}

		public double getShadowThreshold()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorKNN_getShadowThreshold_10(nativeObj);
		}

		public int getHistory()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorKNN_getHistory_10(nativeObj);
		}

		public int getNSamples()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorKNN_getNSamples_10(nativeObj);
		}

		public int getShadowValue()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorKNN_getShadowValue_10(nativeObj);
		}

		public int getkNNSamples()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorKNN_getkNNSamples_10(nativeObj);
		}

		public void setDetectShadows(bool detectShadows)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorKNN_setDetectShadows_10(nativeObj, detectShadows);
		}

		public void setDist2Threshold(double _dist2Threshold)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorKNN_setDist2Threshold_10(nativeObj, _dist2Threshold);
		}

		public void setHistory(int history)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorKNN_setHistory_10(nativeObj, history);
		}

		public void setNSamples(int _nN)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorKNN_setNSamples_10(nativeObj, _nN);
		}

		public void setShadowThreshold(double threshold)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorKNN_setShadowThreshold_10(nativeObj, threshold);
		}

		public void setShadowValue(int value)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorKNN_setShadowValue_10(nativeObj, value);
		}

		public void setkNNSamples(int _nkNN)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorKNN_setkNNSamples_10(nativeObj, _nkNN);
		}

		[DllImport("opencvforunity")]
		private static extern bool video_BackgroundSubtractorKNN_getDetectShadows_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorKNN_getDist2Threshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorKNN_getShadowThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_BackgroundSubtractorKNN_getHistory_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_BackgroundSubtractorKNN_getNSamples_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_BackgroundSubtractorKNN_getShadowValue_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_BackgroundSubtractorKNN_getkNNSamples_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_setDetectShadows_10(IntPtr nativeObj, bool detectShadows);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_setDist2Threshold_10(IntPtr nativeObj, double _dist2Threshold);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_setHistory_10(IntPtr nativeObj, int history);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_setNSamples_10(IntPtr nativeObj, int _nN);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_setShadowThreshold_10(IntPtr nativeObj, double threshold);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_setShadowValue_10(IntPtr nativeObj, int value);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_setkNNSamples_10(IntPtr nativeObj, int _nkNN);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorKNN_delete(IntPtr nativeObj);
	}
}
