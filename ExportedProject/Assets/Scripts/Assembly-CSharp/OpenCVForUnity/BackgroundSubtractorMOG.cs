using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class BackgroundSubtractorMOG : BackgroundSubtractor
	{
		private const string LIBNAME = "opencvforunity";

		public BackgroundSubtractorMOG(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						bgsegm_BackgroundSubtractorMOG_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public double getBackgroundRatio()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorMOG_getBackgroundRatio_10(nativeObj);
		}

		public double getNoiseSigma()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorMOG_getNoiseSigma_10(nativeObj);
		}

		public int getHistory()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorMOG_getHistory_10(nativeObj);
		}

		public int getNMixtures()
		{
			ThrowIfDisposed();
			return bgsegm_BackgroundSubtractorMOG_getNMixtures_10(nativeObj);
		}

		public void setBackgroundRatio(double backgroundRatio)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorMOG_setBackgroundRatio_10(nativeObj, backgroundRatio);
		}

		public void setHistory(int nframes)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorMOG_setHistory_10(nativeObj, nframes);
		}

		public void setNMixtures(int nmix)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorMOG_setNMixtures_10(nativeObj, nmix);
		}

		public void setNoiseSigma(double noiseSigma)
		{
			ThrowIfDisposed();
			bgsegm_BackgroundSubtractorMOG_setNoiseSigma_10(nativeObj, noiseSigma);
		}

		[DllImport("opencvforunity")]
		private static extern double bgsegm_BackgroundSubtractorMOG_getBackgroundRatio_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double bgsegm_BackgroundSubtractorMOG_getNoiseSigma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int bgsegm_BackgroundSubtractorMOG_getHistory_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int bgsegm_BackgroundSubtractorMOG_getNMixtures_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorMOG_setBackgroundRatio_10(IntPtr nativeObj, double backgroundRatio);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorMOG_setHistory_10(IntPtr nativeObj, int nframes);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorMOG_setNMixtures_10(IntPtr nativeObj, int nmix);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorMOG_setNoiseSigma_10(IntPtr nativeObj, double noiseSigma);

		[DllImport("opencvforunity")]
		private static extern void bgsegm_BackgroundSubtractorMOG_delete(IntPtr nativeObj);
	}
}
