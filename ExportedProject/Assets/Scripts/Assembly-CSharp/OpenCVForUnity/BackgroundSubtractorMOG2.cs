using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class BackgroundSubtractorMOG2 : BackgroundSubtractor
	{
		private const string LIBNAME = "opencvforunity";

		public BackgroundSubtractorMOG2(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						video_BackgroundSubtractorMOG2_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool getDetectShadows()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getDetectShadows_10(nativeObj);
		}

		public double getBackgroundRatio()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getBackgroundRatio_10(nativeObj);
		}

		public double getComplexityReductionThreshold()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getComplexityReductionThreshold_10(nativeObj);
		}

		public double getShadowThreshold()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getShadowThreshold_10(nativeObj);
		}

		public double getVarInit()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getVarInit_10(nativeObj);
		}

		public double getVarMax()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getVarMax_10(nativeObj);
		}

		public double getVarMin()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getVarMin_10(nativeObj);
		}

		public double getVarThreshold()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getVarThreshold_10(nativeObj);
		}

		public double getVarThresholdGen()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getVarThresholdGen_10(nativeObj);
		}

		public int getHistory()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getHistory_10(nativeObj);
		}

		public int getNMixtures()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getNMixtures_10(nativeObj);
		}

		public int getShadowValue()
		{
			ThrowIfDisposed();
			return video_BackgroundSubtractorMOG2_getShadowValue_10(nativeObj);
		}

		public void setBackgroundRatio(double ratio)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setBackgroundRatio_10(nativeObj, ratio);
		}

		public void setComplexityReductionThreshold(double ct)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setComplexityReductionThreshold_10(nativeObj, ct);
		}

		public void setDetectShadows(bool detectShadows)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setDetectShadows_10(nativeObj, detectShadows);
		}

		public void setHistory(int history)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setHistory_10(nativeObj, history);
		}

		public void setNMixtures(int nmixtures)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setNMixtures_10(nativeObj, nmixtures);
		}

		public void setShadowThreshold(double threshold)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setShadowThreshold_10(nativeObj, threshold);
		}

		public void setShadowValue(int value)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setShadowValue_10(nativeObj, value);
		}

		public void setVarInit(double varInit)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setVarInit_10(nativeObj, varInit);
		}

		public void setVarMax(double varMax)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setVarMax_10(nativeObj, varMax);
		}

		public void setVarMin(double varMin)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setVarMin_10(nativeObj, varMin);
		}

		public void setVarThreshold(double varThreshold)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setVarThreshold_10(nativeObj, varThreshold);
		}

		public void setVarThresholdGen(double varThresholdGen)
		{
			ThrowIfDisposed();
			video_BackgroundSubtractorMOG2_setVarThresholdGen_10(nativeObj, varThresholdGen);
		}

		[DllImport("opencvforunity")]
		private static extern bool video_BackgroundSubtractorMOG2_getDetectShadows_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getBackgroundRatio_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getComplexityReductionThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getShadowThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getVarInit_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getVarMax_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getVarMin_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getVarThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_BackgroundSubtractorMOG2_getVarThresholdGen_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_BackgroundSubtractorMOG2_getHistory_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_BackgroundSubtractorMOG2_getNMixtures_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_BackgroundSubtractorMOG2_getShadowValue_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setBackgroundRatio_10(IntPtr nativeObj, double ratio);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setComplexityReductionThreshold_10(IntPtr nativeObj, double ct);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setDetectShadows_10(IntPtr nativeObj, bool detectShadows);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setHistory_10(IntPtr nativeObj, int history);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setNMixtures_10(IntPtr nativeObj, int nmixtures);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setShadowThreshold_10(IntPtr nativeObj, double threshold);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setShadowValue_10(IntPtr nativeObj, int value);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setVarInit_10(IntPtr nativeObj, double varInit);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setVarMax_10(IntPtr nativeObj, double varMax);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setVarMin_10(IntPtr nativeObj, double varMin);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setVarThreshold_10(IntPtr nativeObj, double varThreshold);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_setVarThresholdGen_10(IntPtr nativeObj, double varThresholdGen);

		[DllImport("opencvforunity")]
		private static extern void video_BackgroundSubtractorMOG2_delete(IntPtr nativeObj);
	}
}
