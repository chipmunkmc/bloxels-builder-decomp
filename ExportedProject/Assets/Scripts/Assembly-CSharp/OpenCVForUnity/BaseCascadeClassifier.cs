using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class BaseCascadeClassifier : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected BaseCascadeClassifier(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						objdetect_BaseCascadeClassifier_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DllImport("opencvforunity")]
		private static extern void objdetect_BaseCascadeClassifier_delete(IntPtr nativeObj);
	}
}
