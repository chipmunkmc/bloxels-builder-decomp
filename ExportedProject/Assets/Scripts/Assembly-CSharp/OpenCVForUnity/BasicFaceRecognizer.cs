using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class BasicFaceRecognizer : FaceRecognizer
	{
		private const string LIBNAME = "opencvforunity";

		public BasicFaceRecognizer(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						face_BasicFaceRecognizer_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getEigenValues()
		{
			ThrowIfDisposed();
			return new Mat(face_BasicFaceRecognizer_getEigenValues_10(nativeObj));
		}

		public Mat getEigenVectors()
		{
			ThrowIfDisposed();
			return new Mat(face_BasicFaceRecognizer_getEigenVectors_10(nativeObj));
		}

		public Mat getLabels()
		{
			ThrowIfDisposed();
			return new Mat(face_BasicFaceRecognizer_getLabels_10(nativeObj));
		}

		public Mat getMean()
		{
			ThrowIfDisposed();
			return new Mat(face_BasicFaceRecognizer_getMean_10(nativeObj));
		}

		public double getThreshold()
		{
			ThrowIfDisposed();
			return face_BasicFaceRecognizer_getThreshold_10(nativeObj);
		}

		public int getNumComponents()
		{
			ThrowIfDisposed();
			return face_BasicFaceRecognizer_getNumComponents_10(nativeObj);
		}

		public List<Mat> getProjections()
		{
			ThrowIfDisposed();
			List<Mat> list = new List<Mat>();
			Mat m = new Mat(face_BasicFaceRecognizer_getProjections_10(nativeObj));
			Converters.Mat_to_vector_Mat(m, list);
			return list;
		}

		public void setNumComponents(int val)
		{
			ThrowIfDisposed();
			face_BasicFaceRecognizer_setNumComponents_10(nativeObj, val);
		}

		public void setThreshold(double val)
		{
			ThrowIfDisposed();
			face_BasicFaceRecognizer_setThreshold_10(nativeObj, val);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr face_BasicFaceRecognizer_getEigenValues_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_BasicFaceRecognizer_getEigenVectors_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_BasicFaceRecognizer_getLabels_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_BasicFaceRecognizer_getMean_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double face_BasicFaceRecognizer_getThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int face_BasicFaceRecognizer_getNumComponents_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_BasicFaceRecognizer_getProjections_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void face_BasicFaceRecognizer_setNumComponents_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void face_BasicFaceRecognizer_setThreshold_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void face_BasicFaceRecognizer_delete(IntPtr nativeObj);
	}
}
