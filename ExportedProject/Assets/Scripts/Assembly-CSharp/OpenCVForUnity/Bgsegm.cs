using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Bgsegm
	{
		private const string LIBNAME = "opencvforunity";

		public static BackgroundSubtractorGMG createBackgroundSubtractorGMG(int initializationFrames, double decisionThreshold)
		{
			return new BackgroundSubtractorGMG(bgsegm_Bgsegm_createBackgroundSubtractorGMG_10(initializationFrames, decisionThreshold));
		}

		public static BackgroundSubtractorGMG createBackgroundSubtractorGMG()
		{
			return new BackgroundSubtractorGMG(bgsegm_Bgsegm_createBackgroundSubtractorGMG_11());
		}

		public static BackgroundSubtractorMOG createBackgroundSubtractorMOG(int history, int nmixtures, double backgroundRatio, double noiseSigma)
		{
			return new BackgroundSubtractorMOG(bgsegm_Bgsegm_createBackgroundSubtractorMOG_10(history, nmixtures, backgroundRatio, noiseSigma));
		}

		public static BackgroundSubtractorMOG createBackgroundSubtractorMOG()
		{
			return new BackgroundSubtractorMOG(bgsegm_Bgsegm_createBackgroundSubtractorMOG_11());
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr bgsegm_Bgsegm_createBackgroundSubtractorGMG_10(int initializationFrames, double decisionThreshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr bgsegm_Bgsegm_createBackgroundSubtractorGMG_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr bgsegm_Bgsegm_createBackgroundSubtractorMOG_10(int history, int nmixtures, double backgroundRatio, double noiseSigma);

		[DllImport("opencvforunity")]
		private static extern IntPtr bgsegm_Bgsegm_createBackgroundSubtractorMOG_11();
	}
}
