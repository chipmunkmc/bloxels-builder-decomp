using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Bioinspired
	{
		public const int RETINA_COLOR_RANDOM = 0;

		public const int RETINA_COLOR_DIAGONAL = 1;

		public const int RETINA_COLOR_BAYER = 2;

		private const string LIBNAME = "opencvforunity";

		public static Retina createRetina(Size inputSize, bool colorMode, int colorSamplingMethod, bool useRetinaLogSampling, float reductionFactor, float samplingStrenght)
		{
			return new Retina(bioinspired_Bioinspired_createRetina_10(inputSize.width, inputSize.height, colorMode, colorSamplingMethod, useRetinaLogSampling, reductionFactor, samplingStrenght));
		}

		public static Retina createRetina(Size inputSize, bool colorMode)
		{
			return new Retina(bioinspired_Bioinspired_createRetina_11(inputSize.width, inputSize.height, colorMode));
		}

		public static Retina createRetina(Size inputSize)
		{
			return new Retina(bioinspired_Bioinspired_createRetina_12(inputSize.width, inputSize.height));
		}

		public static RetinaFastToneMapping createRetinaFastToneMapping(Size inputSize)
		{
			return new RetinaFastToneMapping(bioinspired_Bioinspired_createRetinaFastToneMapping_10(inputSize.width, inputSize.height));
		}

		public static TransientAreasSegmentationModule createTransientAreasSegmentationModule(Size inputSize)
		{
			return new TransientAreasSegmentationModule(bioinspired_Bioinspired_createTransientAreasSegmentationModule_10(inputSize.width, inputSize.height));
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Bioinspired_createRetina_10(double inputSize_width, double inputSize_height, bool colorMode, int colorSamplingMethod, bool useRetinaLogSampling, float reductionFactor, float samplingStrenght);

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Bioinspired_createRetina_11(double inputSize_width, double inputSize_height, bool colorMode);

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Bioinspired_createRetina_12(double inputSize_width, double inputSize_height);

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Bioinspired_createRetinaFastToneMapping_10(double inputSize_width, double inputSize_height);

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Bioinspired_createTransientAreasSegmentationModule_10(double inputSize_width, double inputSize_height);
	}
}
