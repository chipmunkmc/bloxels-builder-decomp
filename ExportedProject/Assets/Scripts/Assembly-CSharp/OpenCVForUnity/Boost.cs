using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Boost : DTrees
	{
		public const int DISCRETE = 0;

		public const int REAL = 1;

		public const int LOGIT = 2;

		public const int GENTLE = 3;

		private const string LIBNAME = "opencvforunity";

		protected Boost(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_Boost_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public new static Boost create()
		{
			return new Boost(ml_Boost_create_10());
		}

		public double getWeightTrimRate()
		{
			ThrowIfDisposed();
			return ml_Boost_getWeightTrimRate_10(nativeObj);
		}

		public int getBoostType()
		{
			ThrowIfDisposed();
			return ml_Boost_getBoostType_10(nativeObj);
		}

		public int getWeakCount()
		{
			ThrowIfDisposed();
			return ml_Boost_getWeakCount_10(nativeObj);
		}

		public void setBoostType(int val)
		{
			ThrowIfDisposed();
			ml_Boost_setBoostType_10(nativeObj, val);
		}

		public void setWeakCount(int val)
		{
			ThrowIfDisposed();
			ml_Boost_setWeakCount_10(nativeObj, val);
		}

		public void setWeightTrimRate(double val)
		{
			ThrowIfDisposed();
			ml_Boost_setWeightTrimRate_10(nativeObj, val);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_Boost_create_10();

		[DllImport("opencvforunity")]
		private static extern double ml_Boost_getWeightTrimRate_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_Boost_getBoostType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_Boost_getWeakCount_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_Boost_setBoostType_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_Boost_setWeakCount_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_Boost_setWeightTrimRate_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_Boost_delete(IntPtr nativeObj);
	}
}
