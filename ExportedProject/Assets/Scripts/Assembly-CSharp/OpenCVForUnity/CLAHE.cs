using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class CLAHE : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public CLAHE(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						imgproc_CLAHE_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Size getTilesGridSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			imgproc_CLAHE_getTilesGridSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public double getClipLimit()
		{
			ThrowIfDisposed();
			return imgproc_CLAHE_getClipLimit_10(nativeObj);
		}

		public void apply(Mat src, Mat dst)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_CLAHE_apply_10(nativeObj, src.nativeObj, dst.nativeObj);
		}

		public void collectGarbage()
		{
			ThrowIfDisposed();
			imgproc_CLAHE_collectGarbage_10(nativeObj);
		}

		public void setClipLimit(double clipLimit)
		{
			ThrowIfDisposed();
			imgproc_CLAHE_setClipLimit_10(nativeObj, clipLimit);
		}

		public void setTilesGridSize(Size tileGridSize)
		{
			ThrowIfDisposed();
			imgproc_CLAHE_setTilesGridSize_10(nativeObj, tileGridSize.width, tileGridSize.height);
		}

		[DllImport("opencvforunity")]
		private static extern void imgproc_CLAHE_getTilesGridSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern double imgproc_CLAHE_getClipLimit_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_CLAHE_apply_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_CLAHE_collectGarbage_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_CLAHE_setClipLimit_10(IntPtr nativeObj, double clipLimit);

		[DllImport("opencvforunity")]
		private static extern void imgproc_CLAHE_setTilesGridSize_10(IntPtr nativeObj, double tileGridSize_width, double tileGridSize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_CLAHE_delete(IntPtr nativeObj);
	}
}
