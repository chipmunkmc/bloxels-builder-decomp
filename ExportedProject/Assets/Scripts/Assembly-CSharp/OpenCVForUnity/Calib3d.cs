using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Calib3d
	{
		public const int CALIB_USE_INTRINSIC_GUESS = 1;

		public const int CALIB_RECOMPUTE_EXTRINSIC = 2;

		public const int CALIB_CHECK_COND = 4;

		public const int CALIB_FIX_SKEW = 8;

		public const int CALIB_FIX_K1 = 16;

		public const int CALIB_FIX_K2 = 32;

		public const int CALIB_FIX_K3 = 64;

		public const int CALIB_FIX_K4 = 128;

		public const int CALIB_FIX_INTRINSIC = 256;

		public const int CV_ITERATIVE = 0;

		public const int CV_EPNP = 1;

		public const int CV_P3P = 2;

		public const int CV_DLS = 3;

		public const int LMEDS = 4;

		public const int RANSAC = 8;

		public const int RHO = 16;

		public const int SOLVEPNP_ITERATIVE = 0;

		public const int SOLVEPNP_EPNP = 1;

		public const int SOLVEPNP_P3P = 2;

		public const int SOLVEPNP_DLS = 3;

		public const int SOLVEPNP_UPNP = 4;

		public const int CALIB_CB_ADAPTIVE_THRESH = 1;

		public const int CALIB_CB_NORMALIZE_IMAGE = 2;

		public const int CALIB_CB_FILTER_QUADS = 4;

		public const int CALIB_CB_FAST_CHECK = 8;

		public const int CALIB_CB_SYMMETRIC_GRID = 1;

		public const int CALIB_CB_ASYMMETRIC_GRID = 2;

		public const int CALIB_CB_CLUSTERING = 4;

		public const int CALIB_FIX_ASPECT_RATIO = 2;

		public const int CALIB_FIX_PRINCIPAL_POINT = 4;

		public const int CALIB_ZERO_TANGENT_DIST = 8;

		public const int CALIB_FIX_FOCAL_LENGTH = 16;

		public const int CALIB_FIX_K5 = 4096;

		public const int CALIB_FIX_K6 = 8192;

		public const int CALIB_RATIONAL_MODEL = 16384;

		public const int CALIB_THIN_PRISM_MODEL = 32768;

		public const int CALIB_FIX_S1_S2_S3_S4 = 65536;

		public const int CALIB_TILTED_MODEL = 262144;

		public const int CALIB_FIX_TAUX_TAUY = 524288;

		public const int CALIB_SAME_FOCAL_LENGTH = 512;

		public const int CALIB_ZERO_DISPARITY = 1024;

		public const int CALIB_USE_LU = 131072;

		public const int FM_7POINT = 1;

		public const int FM_8POINT = 2;

		public const int FM_LMEDS = 4;

		public const int FM_RANSAC = 8;

		private const string LIBNAME = "opencvforunity";

		public static Mat findEssentialMat(Mat points1, Mat points2, Mat cameraMatrix, int method, double prob, double threshold, Mat mask)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findEssentialMat_10(points1.nativeObj, points2.nativeObj, cameraMatrix.nativeObj, method, prob, threshold, mask.nativeObj));
		}

		public static Mat findEssentialMat(Mat points1, Mat points2, Mat cameraMatrix, int method, double prob, double threshold)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findEssentialMat_11(points1.nativeObj, points2.nativeObj, cameraMatrix.nativeObj, method, prob, threshold));
		}

		public static Mat findEssentialMat(Mat points1, Mat points2, Mat cameraMatrix)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findEssentialMat_12(points1.nativeObj, points2.nativeObj, cameraMatrix.nativeObj));
		}

		public static Mat findEssentialMat(Mat points1, Mat points2, double focal, Point pp, int method, double prob, double threshold, Mat mask)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findEssentialMat_13(points1.nativeObj, points2.nativeObj, focal, pp.x, pp.y, method, prob, threshold, mask.nativeObj));
		}

		public static Mat findEssentialMat(Mat points1, Mat points2, double focal, Point pp, int method, double prob, double threshold)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findEssentialMat_14(points1.nativeObj, points2.nativeObj, focal, pp.x, pp.y, method, prob, threshold));
		}

		public static Mat findEssentialMat(Mat points1, Mat points2)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findEssentialMat_15(points1.nativeObj, points2.nativeObj));
		}

		public static Mat findFundamentalMat(MatOfPoint2f points1, MatOfPoint2f points2, int method, double param1, double param2, Mat mask)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findFundamentalMat_10(points1.nativeObj, points2.nativeObj, method, param1, param2, mask.nativeObj));
		}

		public static Mat findFundamentalMat(MatOfPoint2f points1, MatOfPoint2f points2, int method, double param1, double param2)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findFundamentalMat_11(points1.nativeObj, points2.nativeObj, method, param1, param2));
		}

		public static Mat findFundamentalMat(MatOfPoint2f points1, MatOfPoint2f points2)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findFundamentalMat_12(points1.nativeObj, points2.nativeObj));
		}

		public static Mat findHomography(MatOfPoint2f srcPoints, MatOfPoint2f dstPoints, int method, double ransacReprojThreshold, Mat mask, int maxIters, double confidence)
		{
			if (srcPoints != null)
			{
				srcPoints.ThrowIfDisposed();
			}
			if (dstPoints != null)
			{
				dstPoints.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findHomography_10(srcPoints.nativeObj, dstPoints.nativeObj, method, ransacReprojThreshold, mask.nativeObj, maxIters, confidence));
		}

		public static Mat findHomography(MatOfPoint2f srcPoints, MatOfPoint2f dstPoints, int method, double ransacReprojThreshold)
		{
			if (srcPoints != null)
			{
				srcPoints.ThrowIfDisposed();
			}
			if (dstPoints != null)
			{
				dstPoints.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findHomography_11(srcPoints.nativeObj, dstPoints.nativeObj, method, ransacReprojThreshold));
		}

		public static Mat findHomography(MatOfPoint2f srcPoints, MatOfPoint2f dstPoints)
		{
			if (srcPoints != null)
			{
				srcPoints.ThrowIfDisposed();
			}
			if (dstPoints != null)
			{
				dstPoints.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_findHomography_12(srcPoints.nativeObj, dstPoints.nativeObj));
		}

		public static Mat getOptimalNewCameraMatrix(Mat cameraMatrix, Mat distCoeffs, Size imageSize, double alpha, Size newImgSize, Rect validPixROI, bool centerPrincipalPoint)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			double[] array = new double[4];
			Mat result = new Mat(calib3d_Calib3d_getOptimalNewCameraMatrix_10(cameraMatrix.nativeObj, distCoeffs.nativeObj, imageSize.width, imageSize.height, alpha, newImgSize.width, newImgSize.height, array, centerPrincipalPoint));
			if (validPixROI != null)
			{
				validPixROI.x = (int)array[0];
				validPixROI.y = (int)array[1];
				validPixROI.width = (int)array[2];
				validPixROI.height = (int)array[3];
			}
			return result;
		}

		public static Mat getOptimalNewCameraMatrix(Mat cameraMatrix, Mat distCoeffs, Size imageSize, double alpha)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			return new Mat(calib3d_Calib3d_getOptimalNewCameraMatrix_11(cameraMatrix.nativeObj, distCoeffs.nativeObj, imageSize.width, imageSize.height, alpha));
		}

		public static Mat initCameraMatrix2D(List<MatOfPoint3f> objectPoints, List<MatOfPoint2f> imagePoints, Size imageSize, double aspectRatio)
		{
			List<Mat> mats = new List<Mat>((objectPoints != null) ? objectPoints.Count : 0);
			Mat mat = Converters.vector_vector_Point3f_to_Mat(objectPoints, mats);
			List<Mat> mats2 = new List<Mat>((imagePoints != null) ? imagePoints.Count : 0);
			Mat mat2 = Converters.vector_vector_Point2f_to_Mat(imagePoints, mats2);
			return new Mat(calib3d_Calib3d_initCameraMatrix2D_10(mat.nativeObj, mat2.nativeObj, imageSize.width, imageSize.height, aspectRatio));
		}

		public static Mat initCameraMatrix2D(List<MatOfPoint3f> objectPoints, List<MatOfPoint2f> imagePoints, Size imageSize)
		{
			List<Mat> mats = new List<Mat>((objectPoints != null) ? objectPoints.Count : 0);
			Mat mat = Converters.vector_vector_Point3f_to_Mat(objectPoints, mats);
			List<Mat> mats2 = new List<Mat>((imagePoints != null) ? imagePoints.Count : 0);
			Mat mat2 = Converters.vector_vector_Point2f_to_Mat(imagePoints, mats2);
			return new Mat(calib3d_Calib3d_initCameraMatrix2D_11(mat.nativeObj, mat2.nativeObj, imageSize.width, imageSize.height));
		}

		public static Rect getValidDisparityROI(Rect roi1, Rect roi2, int minDisparity, int numberOfDisparities, int SADWindowSize)
		{
			double[] vals = new double[4];
			calib3d_Calib3d_getValidDisparityROI_10(roi1.x, roi1.y, roi1.width, roi1.height, roi2.x, roi2.y, roi2.width, roi2.height, minDisparity, numberOfDisparities, SADWindowSize, vals);
			return new Rect(vals);
		}

		public static double[] RQDecomp3x3(Mat src, Mat mtxR, Mat mtxQ, Mat Qx, Mat Qy, Mat Qz)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mtxR != null)
			{
				mtxR.ThrowIfDisposed();
			}
			if (mtxQ != null)
			{
				mtxQ.ThrowIfDisposed();
			}
			if (Qx != null)
			{
				Qx.ThrowIfDisposed();
			}
			if (Qy != null)
			{
				Qy.ThrowIfDisposed();
			}
			if (Qz != null)
			{
				Qz.ThrowIfDisposed();
			}
			double[] array = new double[3];
			calib3d_Calib3d_RQDecomp3x3_10(src.nativeObj, mtxR.nativeObj, mtxQ.nativeObj, Qx.nativeObj, Qy.nativeObj, Qz.nativeObj, array);
			return array;
		}

		public static double[] RQDecomp3x3(Mat src, Mat mtxR, Mat mtxQ)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mtxR != null)
			{
				mtxR.ThrowIfDisposed();
			}
			if (mtxQ != null)
			{
				mtxQ.ThrowIfDisposed();
			}
			double[] array = new double[3];
			calib3d_Calib3d_RQDecomp3x3_11(src.nativeObj, mtxR.nativeObj, mtxQ.nativeObj, array);
			return array;
		}

		public static bool findChessboardCorners(Mat image, Size patternSize, MatOfPoint2f corners, int flags)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (corners != null)
			{
				corners.ThrowIfDisposed();
			}
			return calib3d_Calib3d_findChessboardCorners_10(image.nativeObj, patternSize.width, patternSize.height, corners.nativeObj, flags);
		}

		public static bool findChessboardCorners(Mat image, Size patternSize, MatOfPoint2f corners)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (corners != null)
			{
				corners.ThrowIfDisposed();
			}
			return calib3d_Calib3d_findChessboardCorners_11(image.nativeObj, patternSize.width, patternSize.height, corners.nativeObj);
		}

		public static bool findCirclesGrid(Mat image, Size patternSize, Mat centers, int flags)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (centers != null)
			{
				centers.ThrowIfDisposed();
			}
			return calib3d_Calib3d_findCirclesGrid_10(image.nativeObj, patternSize.width, patternSize.height, centers.nativeObj, flags);
		}

		public static bool findCirclesGrid(Mat image, Size patternSize, Mat centers)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (centers != null)
			{
				centers.ThrowIfDisposed();
			}
			return calib3d_Calib3d_findCirclesGrid_11(image.nativeObj, patternSize.width, patternSize.height, centers.nativeObj);
		}

		public static bool solvePnP(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat cameraMatrix, MatOfDouble distCoeffs, Mat rvec, Mat tvec, bool useExtrinsicGuess, int flags)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			return calib3d_Calib3d_solvePnP_10(objectPoints.nativeObj, imagePoints.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, rvec.nativeObj, tvec.nativeObj, useExtrinsicGuess, flags);
		}

		public static bool solvePnP(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat cameraMatrix, MatOfDouble distCoeffs, Mat rvec, Mat tvec)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			return calib3d_Calib3d_solvePnP_11(objectPoints.nativeObj, imagePoints.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, rvec.nativeObj, tvec.nativeObj);
		}

		public static bool solvePnPRansac(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat cameraMatrix, MatOfDouble distCoeffs, Mat rvec, Mat tvec, bool useExtrinsicGuess, int iterationsCount, float reprojectionError, double confidence, Mat inliers, int flags)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (inliers != null)
			{
				inliers.ThrowIfDisposed();
			}
			return calib3d_Calib3d_solvePnPRansac_10(objectPoints.nativeObj, imagePoints.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, rvec.nativeObj, tvec.nativeObj, useExtrinsicGuess, iterationsCount, reprojectionError, confidence, inliers.nativeObj, flags);
		}

		public static bool solvePnPRansac(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat cameraMatrix, MatOfDouble distCoeffs, Mat rvec, Mat tvec)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			return calib3d_Calib3d_solvePnPRansac_11(objectPoints.nativeObj, imagePoints.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, rvec.nativeObj, tvec.nativeObj);
		}

		public static bool stereoRectifyUncalibrated(Mat points1, Mat points2, Mat F, Size imgSize, Mat H1, Mat H2, double threshold)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			if (H1 != null)
			{
				H1.ThrowIfDisposed();
			}
			if (H2 != null)
			{
				H2.ThrowIfDisposed();
			}
			return calib3d_Calib3d_stereoRectifyUncalibrated_10(points1.nativeObj, points2.nativeObj, F.nativeObj, imgSize.width, imgSize.height, H1.nativeObj, H2.nativeObj, threshold);
		}

		public static bool stereoRectifyUncalibrated(Mat points1, Mat points2, Mat F, Size imgSize, Mat H1, Mat H2)
		{
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			if (H1 != null)
			{
				H1.ThrowIfDisposed();
			}
			if (H2 != null)
			{
				H2.ThrowIfDisposed();
			}
			return calib3d_Calib3d_stereoRectifyUncalibrated_11(points1.nativeObj, points2.nativeObj, F.nativeObj, imgSize.width, imgSize.height, H1.nativeObj, H2.nativeObj);
		}

		public static double calibrateCamera(List<Mat> objectPoints, List<Mat> imagePoints, Size imageSize, Mat cameraMatrix, Mat distCoeffs, List<Mat> rvecs, List<Mat> tvecs, int flags, TermCriteria criteria)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints);
			Mat mat3 = new Mat();
			Mat mat4 = new Mat();
			double result = calib3d_Calib3d_calibrateCamera_10(mat.nativeObj, mat2.nativeObj, imageSize.width, imageSize.height, cameraMatrix.nativeObj, distCoeffs.nativeObj, mat3.nativeObj, mat4.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon);
			Converters.Mat_to_vector_Mat(mat3, rvecs);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, tvecs);
			mat4.release();
			return result;
		}

		public static double calibrateCamera(List<Mat> objectPoints, List<Mat> imagePoints, Size imageSize, Mat cameraMatrix, Mat distCoeffs, List<Mat> rvecs, List<Mat> tvecs, int flags)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints);
			Mat mat3 = new Mat();
			Mat mat4 = new Mat();
			double result = calib3d_Calib3d_calibrateCamera_11(mat.nativeObj, mat2.nativeObj, imageSize.width, imageSize.height, cameraMatrix.nativeObj, distCoeffs.nativeObj, mat3.nativeObj, mat4.nativeObj, flags);
			Converters.Mat_to_vector_Mat(mat3, rvecs);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, tvecs);
			mat4.release();
			return result;
		}

		public static double calibrateCamera(List<Mat> objectPoints, List<Mat> imagePoints, Size imageSize, Mat cameraMatrix, Mat distCoeffs, List<Mat> rvecs, List<Mat> tvecs)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints);
			Mat mat3 = new Mat();
			Mat mat4 = new Mat();
			double result = calib3d_Calib3d_calibrateCamera_12(mat.nativeObj, mat2.nativeObj, imageSize.width, imageSize.height, cameraMatrix.nativeObj, distCoeffs.nativeObj, mat3.nativeObj, mat4.nativeObj);
			Converters.Mat_to_vector_Mat(mat3, rvecs);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, tvecs);
			mat4.release();
			return result;
		}

		public static double sampsonDistance(Mat pt1, Mat pt2, Mat F)
		{
			if (pt1 != null)
			{
				pt1.ThrowIfDisposed();
			}
			if (pt2 != null)
			{
				pt2.ThrowIfDisposed();
			}
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			return calib3d_Calib3d_sampsonDistance_10(pt1.nativeObj, pt2.nativeObj, F.nativeObj);
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Mat cameraMatrix1, Mat distCoeffs1, Mat cameraMatrix2, Mat distCoeffs2, Size imageSize, Mat R, Mat T, Mat E, Mat F, int flags, TermCriteria criteria)
		{
			if (cameraMatrix1 != null)
			{
				cameraMatrix1.ThrowIfDisposed();
			}
			if (distCoeffs1 != null)
			{
				distCoeffs1.ThrowIfDisposed();
			}
			if (cameraMatrix2 != null)
			{
				cameraMatrix2.ThrowIfDisposed();
			}
			if (distCoeffs2 != null)
			{
				distCoeffs2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			return calib3d_Calib3d_stereoCalibrate_10(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, cameraMatrix1.nativeObj, distCoeffs1.nativeObj, cameraMatrix2.nativeObj, distCoeffs2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj, E.nativeObj, F.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon);
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Mat cameraMatrix1, Mat distCoeffs1, Mat cameraMatrix2, Mat distCoeffs2, Size imageSize, Mat R, Mat T, Mat E, Mat F, int flags)
		{
			if (cameraMatrix1 != null)
			{
				cameraMatrix1.ThrowIfDisposed();
			}
			if (distCoeffs1 != null)
			{
				distCoeffs1.ThrowIfDisposed();
			}
			if (cameraMatrix2 != null)
			{
				cameraMatrix2.ThrowIfDisposed();
			}
			if (distCoeffs2 != null)
			{
				distCoeffs2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			return calib3d_Calib3d_stereoCalibrate_11(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, cameraMatrix1.nativeObj, distCoeffs1.nativeObj, cameraMatrix2.nativeObj, distCoeffs2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj, E.nativeObj, F.nativeObj, flags);
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Mat cameraMatrix1, Mat distCoeffs1, Mat cameraMatrix2, Mat distCoeffs2, Size imageSize, Mat R, Mat T, Mat E, Mat F)
		{
			if (cameraMatrix1 != null)
			{
				cameraMatrix1.ThrowIfDisposed();
			}
			if (distCoeffs1 != null)
			{
				distCoeffs1.ThrowIfDisposed();
			}
			if (cameraMatrix2 != null)
			{
				cameraMatrix2.ThrowIfDisposed();
			}
			if (distCoeffs2 != null)
			{
				distCoeffs2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			return calib3d_Calib3d_stereoCalibrate_12(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, cameraMatrix1.nativeObj, distCoeffs1.nativeObj, cameraMatrix2.nativeObj, distCoeffs2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj, E.nativeObj, F.nativeObj);
		}

		public static double calibrate(List<Mat> objectPoints, List<Mat> imagePoints, Size image_size, Mat K, Mat D, List<Mat> rvecs, List<Mat> tvecs, int flags, TermCriteria criteria)
		{
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints);
			Mat mat3 = new Mat();
			Mat mat4 = new Mat();
			double result = calib3d_Calib3d_calibrate_10(mat.nativeObj, mat2.nativeObj, image_size.width, image_size.height, K.nativeObj, D.nativeObj, mat3.nativeObj, mat4.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon);
			Converters.Mat_to_vector_Mat(mat3, rvecs);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, tvecs);
			mat4.release();
			return result;
		}

		public static double calibrate(List<Mat> objectPoints, List<Mat> imagePoints, Size image_size, Mat K, Mat D, List<Mat> rvecs, List<Mat> tvecs, int flags)
		{
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints);
			Mat mat3 = new Mat();
			Mat mat4 = new Mat();
			double result = calib3d_Calib3d_calibrate_11(mat.nativeObj, mat2.nativeObj, image_size.width, image_size.height, K.nativeObj, D.nativeObj, mat3.nativeObj, mat4.nativeObj, flags);
			Converters.Mat_to_vector_Mat(mat3, rvecs);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, tvecs);
			mat4.release();
			return result;
		}

		public static double calibrate(List<Mat> objectPoints, List<Mat> imagePoints, Size image_size, Mat K, Mat D, List<Mat> rvecs, List<Mat> tvecs)
		{
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints);
			Mat mat3 = new Mat();
			Mat mat4 = new Mat();
			double result = calib3d_Calib3d_calibrate_12(mat.nativeObj, mat2.nativeObj, image_size.width, image_size.height, K.nativeObj, D.nativeObj, mat3.nativeObj, mat4.nativeObj);
			Converters.Mat_to_vector_Mat(mat3, rvecs);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, tvecs);
			mat4.release();
			return result;
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Mat K1, Mat D1, Mat K2, Mat D2, Size imageSize, Mat R, Mat T, int flags, TermCriteria criteria)
		{
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			return calib3d_Calib3d_stereoCalibrate_13(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, K1.nativeObj, D1.nativeObj, K2.nativeObj, D2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon);
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Mat K1, Mat D1, Mat K2, Mat D2, Size imageSize, Mat R, Mat T, int flags)
		{
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			return calib3d_Calib3d_stereoCalibrate_14(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, K1.nativeObj, D1.nativeObj, K2.nativeObj, D2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj, flags);
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Mat K1, Mat D1, Mat K2, Mat D2, Size imageSize, Mat R, Mat T)
		{
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			return calib3d_Calib3d_stereoCalibrate_15(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, K1.nativeObj, D1.nativeObj, K2.nativeObj, D2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj);
		}

		public static float rectify3Collinear(Mat cameraMatrix1, Mat distCoeffs1, Mat cameraMatrix2, Mat distCoeffs2, Mat cameraMatrix3, Mat distCoeffs3, List<Mat> imgpt1, List<Mat> imgpt3, Size imageSize, Mat R12, Mat T12, Mat R13, Mat T13, Mat R1, Mat R2, Mat R3, Mat P1, Mat P2, Mat P3, Mat Q, double alpha, Size newImgSize, Rect roi1, Rect roi2, int flags)
		{
			if (cameraMatrix1 != null)
			{
				cameraMatrix1.ThrowIfDisposed();
			}
			if (distCoeffs1 != null)
			{
				distCoeffs1.ThrowIfDisposed();
			}
			if (cameraMatrix2 != null)
			{
				cameraMatrix2.ThrowIfDisposed();
			}
			if (distCoeffs2 != null)
			{
				distCoeffs2.ThrowIfDisposed();
			}
			if (cameraMatrix3 != null)
			{
				cameraMatrix3.ThrowIfDisposed();
			}
			if (distCoeffs3 != null)
			{
				distCoeffs3.ThrowIfDisposed();
			}
			if (R12 != null)
			{
				R12.ThrowIfDisposed();
			}
			if (T12 != null)
			{
				T12.ThrowIfDisposed();
			}
			if (R13 != null)
			{
				R13.ThrowIfDisposed();
			}
			if (T13 != null)
			{
				T13.ThrowIfDisposed();
			}
			if (R1 != null)
			{
				R1.ThrowIfDisposed();
			}
			if (R2 != null)
			{
				R2.ThrowIfDisposed();
			}
			if (R3 != null)
			{
				R3.ThrowIfDisposed();
			}
			if (P1 != null)
			{
				P1.ThrowIfDisposed();
			}
			if (P2 != null)
			{
				P2.ThrowIfDisposed();
			}
			if (P3 != null)
			{
				P3.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(imgpt1);
			Mat mat2 = Converters.vector_Mat_to_Mat(imgpt3);
			double[] array = new double[4];
			double[] array2 = new double[4];
			float result = calib3d_Calib3d_rectify3Collinear_10(cameraMatrix1.nativeObj, distCoeffs1.nativeObj, cameraMatrix2.nativeObj, distCoeffs2.nativeObj, cameraMatrix3.nativeObj, distCoeffs3.nativeObj, mat.nativeObj, mat2.nativeObj, imageSize.width, imageSize.height, R12.nativeObj, T12.nativeObj, R13.nativeObj, T13.nativeObj, R1.nativeObj, R2.nativeObj, R3.nativeObj, P1.nativeObj, P2.nativeObj, P3.nativeObj, Q.nativeObj, alpha, newImgSize.width, newImgSize.height, array, array2, flags);
			if (roi1 != null)
			{
				roi1.x = (int)array[0];
				roi1.y = (int)array[1];
				roi1.width = (int)array[2];
				roi1.height = (int)array[3];
			}
			if (roi2 != null)
			{
				roi2.x = (int)array2[0];
				roi2.y = (int)array2[1];
				roi2.width = (int)array2[2];
				roi2.height = (int)array2[3];
			}
			return result;
		}

		public static int decomposeHomographyMat(Mat H, Mat K, List<Mat> rotations, List<Mat> translations, List<Mat> normals)
		{
			if (H != null)
			{
				H.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			Mat mat2 = new Mat();
			Mat mat3 = new Mat();
			int result = calib3d_Calib3d_decomposeHomographyMat_10(H.nativeObj, K.nativeObj, mat.nativeObj, mat2.nativeObj, mat3.nativeObj);
			Converters.Mat_to_vector_Mat(mat, rotations);
			mat.release();
			Converters.Mat_to_vector_Mat(mat2, translations);
			mat2.release();
			Converters.Mat_to_vector_Mat(mat3, normals);
			mat3.release();
			return result;
		}

		public static int estimateAffine3D(Mat src, Mat dst, Mat _out, Mat inliers, double ransacThreshold, double confidence)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (_out != null)
			{
				_out.ThrowIfDisposed();
			}
			if (inliers != null)
			{
				inliers.ThrowIfDisposed();
			}
			return calib3d_Calib3d_estimateAffine3D_10(src.nativeObj, dst.nativeObj, _out.nativeObj, inliers.nativeObj, ransacThreshold, confidence);
		}

		public static int estimateAffine3D(Mat src, Mat dst, Mat _out, Mat inliers)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (_out != null)
			{
				_out.ThrowIfDisposed();
			}
			if (inliers != null)
			{
				inliers.ThrowIfDisposed();
			}
			return calib3d_Calib3d_estimateAffine3D_11(src.nativeObj, dst.nativeObj, _out.nativeObj, inliers.nativeObj);
		}

		public static int recoverPose(Mat E, Mat points1, Mat points2, Mat R, Mat t, double focal, Point pp, Mat mask)
		{
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (t != null)
			{
				t.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return calib3d_Calib3d_recoverPose_10(E.nativeObj, points1.nativeObj, points2.nativeObj, R.nativeObj, t.nativeObj, focal, pp.x, pp.y, mask.nativeObj);
		}

		public static int recoverPose(Mat E, Mat points1, Mat points2, Mat R, Mat t, double focal, Point pp)
		{
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (t != null)
			{
				t.ThrowIfDisposed();
			}
			return calib3d_Calib3d_recoverPose_11(E.nativeObj, points1.nativeObj, points2.nativeObj, R.nativeObj, t.nativeObj, focal, pp.x, pp.y);
		}

		public static int recoverPose(Mat E, Mat points1, Mat points2, Mat R, Mat t)
		{
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (t != null)
			{
				t.ThrowIfDisposed();
			}
			return calib3d_Calib3d_recoverPose_12(E.nativeObj, points1.nativeObj, points2.nativeObj, R.nativeObj, t.nativeObj);
		}

		public static int recoverPose(Mat E, Mat points1, Mat points2, Mat cameraMatrix, Mat R, Mat t, Mat mask)
		{
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (t != null)
			{
				t.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return calib3d_Calib3d_recoverPose_13(E.nativeObj, points1.nativeObj, points2.nativeObj, cameraMatrix.nativeObj, R.nativeObj, t.nativeObj, mask.nativeObj);
		}

		public static int recoverPose(Mat E, Mat points1, Mat points2, Mat cameraMatrix, Mat R, Mat t)
		{
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (t != null)
			{
				t.ThrowIfDisposed();
			}
			return calib3d_Calib3d_recoverPose_14(E.nativeObj, points1.nativeObj, points2.nativeObj, cameraMatrix.nativeObj, R.nativeObj, t.nativeObj);
		}

		public static void Rodrigues(Mat src, Mat dst, Mat jacobian)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (jacobian != null)
			{
				jacobian.ThrowIfDisposed();
			}
			calib3d_Calib3d_Rodrigues_10(src.nativeObj, dst.nativeObj, jacobian.nativeObj);
		}

		public static void Rodrigues(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			calib3d_Calib3d_Rodrigues_11(src.nativeObj, dst.nativeObj);
		}

		public static void calibrationMatrixValues(Mat cameraMatrix, Size imageSize, double apertureWidth, double apertureHeight, double[] fovx, double[] fovy, double[] focalLength, Point principalPoint, double[] aspectRatio)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			double[] array = new double[1];
			double[] array2 = new double[1];
			double[] array3 = new double[1];
			double[] array4 = new double[2];
			double[] array5 = new double[1];
			calib3d_Calib3d_calibrationMatrixValues_10(cameraMatrix.nativeObj, imageSize.width, imageSize.height, apertureWidth, apertureHeight, array, array2, array3, array4, array5);
			if (fovx != null)
			{
				fovx[0] = array[0];
			}
			if (fovy != null)
			{
				fovy[0] = array2[0];
			}
			if (focalLength != null)
			{
				focalLength[0] = array3[0];
			}
			if (principalPoint != null)
			{
				principalPoint.x = array4[0];
				principalPoint.y = array4[1];
			}
			if (aspectRatio != null)
			{
				aspectRatio[0] = array5[0];
			}
		}

		public static void composeRT(Mat rvec1, Mat tvec1, Mat rvec2, Mat tvec2, Mat rvec3, Mat tvec3, Mat dr3dr1, Mat dr3dt1, Mat dr3dr2, Mat dr3dt2, Mat dt3dr1, Mat dt3dt1, Mat dt3dr2, Mat dt3dt2)
		{
			if (rvec1 != null)
			{
				rvec1.ThrowIfDisposed();
			}
			if (tvec1 != null)
			{
				tvec1.ThrowIfDisposed();
			}
			if (rvec2 != null)
			{
				rvec2.ThrowIfDisposed();
			}
			if (tvec2 != null)
			{
				tvec2.ThrowIfDisposed();
			}
			if (rvec3 != null)
			{
				rvec3.ThrowIfDisposed();
			}
			if (tvec3 != null)
			{
				tvec3.ThrowIfDisposed();
			}
			if (dr3dr1 != null)
			{
				dr3dr1.ThrowIfDisposed();
			}
			if (dr3dt1 != null)
			{
				dr3dt1.ThrowIfDisposed();
			}
			if (dr3dr2 != null)
			{
				dr3dr2.ThrowIfDisposed();
			}
			if (dr3dt2 != null)
			{
				dr3dt2.ThrowIfDisposed();
			}
			if (dt3dr1 != null)
			{
				dt3dr1.ThrowIfDisposed();
			}
			if (dt3dt1 != null)
			{
				dt3dt1.ThrowIfDisposed();
			}
			if (dt3dr2 != null)
			{
				dt3dr2.ThrowIfDisposed();
			}
			if (dt3dt2 != null)
			{
				dt3dt2.ThrowIfDisposed();
			}
			calib3d_Calib3d_composeRT_10(rvec1.nativeObj, tvec1.nativeObj, rvec2.nativeObj, tvec2.nativeObj, rvec3.nativeObj, tvec3.nativeObj, dr3dr1.nativeObj, dr3dt1.nativeObj, dr3dr2.nativeObj, dr3dt2.nativeObj, dt3dr1.nativeObj, dt3dt1.nativeObj, dt3dr2.nativeObj, dt3dt2.nativeObj);
		}

		public static void composeRT(Mat rvec1, Mat tvec1, Mat rvec2, Mat tvec2, Mat rvec3, Mat tvec3)
		{
			if (rvec1 != null)
			{
				rvec1.ThrowIfDisposed();
			}
			if (tvec1 != null)
			{
				tvec1.ThrowIfDisposed();
			}
			if (rvec2 != null)
			{
				rvec2.ThrowIfDisposed();
			}
			if (tvec2 != null)
			{
				tvec2.ThrowIfDisposed();
			}
			if (rvec3 != null)
			{
				rvec3.ThrowIfDisposed();
			}
			if (tvec3 != null)
			{
				tvec3.ThrowIfDisposed();
			}
			calib3d_Calib3d_composeRT_11(rvec1.nativeObj, tvec1.nativeObj, rvec2.nativeObj, tvec2.nativeObj, rvec3.nativeObj, tvec3.nativeObj);
		}

		public static void computeCorrespondEpilines(Mat points, int whichImage, Mat F, Mat lines)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			if (lines != null)
			{
				lines.ThrowIfDisposed();
			}
			calib3d_Calib3d_computeCorrespondEpilines_10(points.nativeObj, whichImage, F.nativeObj, lines.nativeObj);
		}

		public static void convertPointsFromHomogeneous(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			calib3d_Calib3d_convertPointsFromHomogeneous_10(src.nativeObj, dst.nativeObj);
		}

		public static void convertPointsToHomogeneous(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			calib3d_Calib3d_convertPointsToHomogeneous_10(src.nativeObj, dst.nativeObj);
		}

		public static void correctMatches(Mat F, Mat points1, Mat points2, Mat newPoints1, Mat newPoints2)
		{
			if (F != null)
			{
				F.ThrowIfDisposed();
			}
			if (points1 != null)
			{
				points1.ThrowIfDisposed();
			}
			if (points2 != null)
			{
				points2.ThrowIfDisposed();
			}
			if (newPoints1 != null)
			{
				newPoints1.ThrowIfDisposed();
			}
			if (newPoints2 != null)
			{
				newPoints2.ThrowIfDisposed();
			}
			calib3d_Calib3d_correctMatches_10(F.nativeObj, points1.nativeObj, points2.nativeObj, newPoints1.nativeObj, newPoints2.nativeObj);
		}

		public static void decomposeEssentialMat(Mat E, Mat R1, Mat R2, Mat t)
		{
			if (E != null)
			{
				E.ThrowIfDisposed();
			}
			if (R1 != null)
			{
				R1.ThrowIfDisposed();
			}
			if (R2 != null)
			{
				R2.ThrowIfDisposed();
			}
			if (t != null)
			{
				t.ThrowIfDisposed();
			}
			calib3d_Calib3d_decomposeEssentialMat_10(E.nativeObj, R1.nativeObj, R2.nativeObj, t.nativeObj);
		}

		public static void decomposeProjectionMatrix(Mat projMatrix, Mat cameraMatrix, Mat rotMatrix, Mat transVect, Mat rotMatrixX, Mat rotMatrixY, Mat rotMatrixZ, Mat eulerAngles)
		{
			if (projMatrix != null)
			{
				projMatrix.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (rotMatrix != null)
			{
				rotMatrix.ThrowIfDisposed();
			}
			if (transVect != null)
			{
				transVect.ThrowIfDisposed();
			}
			if (rotMatrixX != null)
			{
				rotMatrixX.ThrowIfDisposed();
			}
			if (rotMatrixY != null)
			{
				rotMatrixY.ThrowIfDisposed();
			}
			if (rotMatrixZ != null)
			{
				rotMatrixZ.ThrowIfDisposed();
			}
			if (eulerAngles != null)
			{
				eulerAngles.ThrowIfDisposed();
			}
			calib3d_Calib3d_decomposeProjectionMatrix_10(projMatrix.nativeObj, cameraMatrix.nativeObj, rotMatrix.nativeObj, transVect.nativeObj, rotMatrixX.nativeObj, rotMatrixY.nativeObj, rotMatrixZ.nativeObj, eulerAngles.nativeObj);
		}

		public static void decomposeProjectionMatrix(Mat projMatrix, Mat cameraMatrix, Mat rotMatrix, Mat transVect)
		{
			if (projMatrix != null)
			{
				projMatrix.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (rotMatrix != null)
			{
				rotMatrix.ThrowIfDisposed();
			}
			if (transVect != null)
			{
				transVect.ThrowIfDisposed();
			}
			calib3d_Calib3d_decomposeProjectionMatrix_11(projMatrix.nativeObj, cameraMatrix.nativeObj, rotMatrix.nativeObj, transVect.nativeObj);
		}

		public static void drawChessboardCorners(Mat image, Size patternSize, MatOfPoint2f corners, bool patternWasFound)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (corners != null)
			{
				corners.ThrowIfDisposed();
			}
			calib3d_Calib3d_drawChessboardCorners_10(image.nativeObj, patternSize.width, patternSize.height, corners.nativeObj, patternWasFound);
		}

		public static void filterSpeckles(Mat img, double newVal, int maxSpeckleSize, double maxDiff, Mat buf)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (buf != null)
			{
				buf.ThrowIfDisposed();
			}
			calib3d_Calib3d_filterSpeckles_10(img.nativeObj, newVal, maxSpeckleSize, maxDiff, buf.nativeObj);
		}

		public static void filterSpeckles(Mat img, double newVal, int maxSpeckleSize, double maxDiff)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			calib3d_Calib3d_filterSpeckles_11(img.nativeObj, newVal, maxSpeckleSize, maxDiff);
		}

		public static void matMulDeriv(Mat A, Mat B, Mat dABdA, Mat dABdB)
		{
			if (A != null)
			{
				A.ThrowIfDisposed();
			}
			if (B != null)
			{
				B.ThrowIfDisposed();
			}
			if (dABdA != null)
			{
				dABdA.ThrowIfDisposed();
			}
			if (dABdB != null)
			{
				dABdB.ThrowIfDisposed();
			}
			calib3d_Calib3d_matMulDeriv_10(A.nativeObj, B.nativeObj, dABdA.nativeObj, dABdB.nativeObj);
		}

		public static void projectPoints(MatOfPoint3f objectPoints, Mat rvec, Mat tvec, Mat cameraMatrix, MatOfDouble distCoeffs, MatOfPoint2f imagePoints, Mat jacobian, double aspectRatio)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (jacobian != null)
			{
				jacobian.ThrowIfDisposed();
			}
			calib3d_Calib3d_projectPoints_10(objectPoints.nativeObj, rvec.nativeObj, tvec.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, imagePoints.nativeObj, jacobian.nativeObj, aspectRatio);
		}

		public static void projectPoints(MatOfPoint3f objectPoints, Mat rvec, Mat tvec, Mat cameraMatrix, MatOfDouble distCoeffs, MatOfPoint2f imagePoints)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			calib3d_Calib3d_projectPoints_11(objectPoints.nativeObj, rvec.nativeObj, tvec.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, imagePoints.nativeObj);
		}

		public static void reprojectImageTo3D(Mat disparity, Mat _3dImage, Mat Q, bool handleMissingValues, int ddepth)
		{
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			if (_3dImage != null)
			{
				_3dImage.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			calib3d_Calib3d_reprojectImageTo3D_10(disparity.nativeObj, _3dImage.nativeObj, Q.nativeObj, handleMissingValues, ddepth);
		}

		public static void reprojectImageTo3D(Mat disparity, Mat _3dImage, Mat Q, bool handleMissingValues)
		{
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			if (_3dImage != null)
			{
				_3dImage.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			calib3d_Calib3d_reprojectImageTo3D_11(disparity.nativeObj, _3dImage.nativeObj, Q.nativeObj, handleMissingValues);
		}

		public static void reprojectImageTo3D(Mat disparity, Mat _3dImage, Mat Q)
		{
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			if (_3dImage != null)
			{
				_3dImage.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			calib3d_Calib3d_reprojectImageTo3D_12(disparity.nativeObj, _3dImage.nativeObj, Q.nativeObj);
		}

		public static void stereoRectify(Mat cameraMatrix1, Mat distCoeffs1, Mat cameraMatrix2, Mat distCoeffs2, Size imageSize, Mat R, Mat T, Mat R1, Mat R2, Mat P1, Mat P2, Mat Q, int flags, double alpha, Size newImageSize, Rect validPixROI1, Rect validPixROI2)
		{
			if (cameraMatrix1 != null)
			{
				cameraMatrix1.ThrowIfDisposed();
			}
			if (distCoeffs1 != null)
			{
				distCoeffs1.ThrowIfDisposed();
			}
			if (cameraMatrix2 != null)
			{
				cameraMatrix2.ThrowIfDisposed();
			}
			if (distCoeffs2 != null)
			{
				distCoeffs2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (R1 != null)
			{
				R1.ThrowIfDisposed();
			}
			if (R2 != null)
			{
				R2.ThrowIfDisposed();
			}
			if (P1 != null)
			{
				P1.ThrowIfDisposed();
			}
			if (P2 != null)
			{
				P2.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			double[] array = new double[4];
			double[] array2 = new double[4];
			calib3d_Calib3d_stereoRectify_10(cameraMatrix1.nativeObj, distCoeffs1.nativeObj, cameraMatrix2.nativeObj, distCoeffs2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj, R1.nativeObj, R2.nativeObj, P1.nativeObj, P2.nativeObj, Q.nativeObj, flags, alpha, newImageSize.width, newImageSize.height, array, array2);
			if (validPixROI1 != null)
			{
				validPixROI1.x = (int)array[0];
				validPixROI1.y = (int)array[1];
				validPixROI1.width = (int)array[2];
				validPixROI1.height = (int)array[3];
			}
			if (validPixROI2 != null)
			{
				validPixROI2.x = (int)array2[0];
				validPixROI2.y = (int)array2[1];
				validPixROI2.width = (int)array2[2];
				validPixROI2.height = (int)array2[3];
			}
		}

		public static void stereoRectify(Mat cameraMatrix1, Mat distCoeffs1, Mat cameraMatrix2, Mat distCoeffs2, Size imageSize, Mat R, Mat T, Mat R1, Mat R2, Mat P1, Mat P2, Mat Q)
		{
			if (cameraMatrix1 != null)
			{
				cameraMatrix1.ThrowIfDisposed();
			}
			if (distCoeffs1 != null)
			{
				distCoeffs1.ThrowIfDisposed();
			}
			if (cameraMatrix2 != null)
			{
				cameraMatrix2.ThrowIfDisposed();
			}
			if (distCoeffs2 != null)
			{
				distCoeffs2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (R1 != null)
			{
				R1.ThrowIfDisposed();
			}
			if (R2 != null)
			{
				R2.ThrowIfDisposed();
			}
			if (P1 != null)
			{
				P1.ThrowIfDisposed();
			}
			if (P2 != null)
			{
				P2.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			calib3d_Calib3d_stereoRectify_11(cameraMatrix1.nativeObj, distCoeffs1.nativeObj, cameraMatrix2.nativeObj, distCoeffs2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, T.nativeObj, R1.nativeObj, R2.nativeObj, P1.nativeObj, P2.nativeObj, Q.nativeObj);
		}

		public static void triangulatePoints(Mat projMatr1, Mat projMatr2, Mat projPoints1, Mat projPoints2, Mat points4D)
		{
			if (projMatr1 != null)
			{
				projMatr1.ThrowIfDisposed();
			}
			if (projMatr2 != null)
			{
				projMatr2.ThrowIfDisposed();
			}
			if (projPoints1 != null)
			{
				projPoints1.ThrowIfDisposed();
			}
			if (projPoints2 != null)
			{
				projPoints2.ThrowIfDisposed();
			}
			if (points4D != null)
			{
				points4D.ThrowIfDisposed();
			}
			calib3d_Calib3d_triangulatePoints_10(projMatr1.nativeObj, projMatr2.nativeObj, projPoints1.nativeObj, projPoints2.nativeObj, points4D.nativeObj);
		}

		public static void validateDisparity(Mat disparity, Mat cost, int minDisparity, int numberOfDisparities, int disp12MaxDisp)
		{
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			if (cost != null)
			{
				cost.ThrowIfDisposed();
			}
			calib3d_Calib3d_validateDisparity_10(disparity.nativeObj, cost.nativeObj, minDisparity, numberOfDisparities, disp12MaxDisp);
		}

		public static void validateDisparity(Mat disparity, Mat cost, int minDisparity, int numberOfDisparities)
		{
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			if (cost != null)
			{
				cost.ThrowIfDisposed();
			}
			calib3d_Calib3d_validateDisparity_11(disparity.nativeObj, cost.nativeObj, minDisparity, numberOfDisparities);
		}

		public static void distortPoints(Mat undistorted, Mat distorted, Mat K, Mat D, double alpha)
		{
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			calib3d_Calib3d_distortPoints_10(undistorted.nativeObj, distorted.nativeObj, K.nativeObj, D.nativeObj, alpha);
		}

		public static void distortPoints(Mat undistorted, Mat distorted, Mat K, Mat D)
		{
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			calib3d_Calib3d_distortPoints_11(undistorted.nativeObj, distorted.nativeObj, K.nativeObj, D.nativeObj);
		}

		public static void estimateNewCameraMatrixForUndistortRectify(Mat K, Mat D, Size image_size, Mat R, Mat P, double balance, Size new_size, double fov_scale)
		{
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (P != null)
			{
				P.ThrowIfDisposed();
			}
			calib3d_Calib3d_estimateNewCameraMatrixForUndistortRectify_10(K.nativeObj, D.nativeObj, image_size.width, image_size.height, R.nativeObj, P.nativeObj, balance, new_size.width, new_size.height, fov_scale);
		}

		public static void estimateNewCameraMatrixForUndistortRectify(Mat K, Mat D, Size image_size, Mat R, Mat P)
		{
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (P != null)
			{
				P.ThrowIfDisposed();
			}
			calib3d_Calib3d_estimateNewCameraMatrixForUndistortRectify_11(K.nativeObj, D.nativeObj, image_size.width, image_size.height, R.nativeObj, P.nativeObj);
		}

		public static void initUndistortRectifyMap(Mat K, Mat D, Mat R, Mat P, Size size, int m1type, Mat map1, Mat map2)
		{
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (P != null)
			{
				P.ThrowIfDisposed();
			}
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			calib3d_Calib3d_initUndistortRectifyMap_10(K.nativeObj, D.nativeObj, R.nativeObj, P.nativeObj, size.width, size.height, m1type, map1.nativeObj, map2.nativeObj);
		}

		public static void projectPoints(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat rvec, Mat tvec, Mat K, Mat D, double alpha, Mat jacobian)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (jacobian != null)
			{
				jacobian.ThrowIfDisposed();
			}
			calib3d_Calib3d_projectPoints_12(objectPoints.nativeObj, imagePoints.nativeObj, rvec.nativeObj, tvec.nativeObj, K.nativeObj, D.nativeObj, alpha, jacobian.nativeObj);
		}

		public static void projectPoints(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat rvec, Mat tvec, Mat K, Mat D)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			calib3d_Calib3d_projectPoints_13(objectPoints.nativeObj, imagePoints.nativeObj, rvec.nativeObj, tvec.nativeObj, K.nativeObj, D.nativeObj);
		}

		public static void stereoRectify(Mat K1, Mat D1, Mat K2, Mat D2, Size imageSize, Mat R, Mat tvec, Mat R1, Mat R2, Mat P1, Mat P2, Mat Q, int flags, Size newImageSize, double balance, double fov_scale)
		{
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (R1 != null)
			{
				R1.ThrowIfDisposed();
			}
			if (R2 != null)
			{
				R2.ThrowIfDisposed();
			}
			if (P1 != null)
			{
				P1.ThrowIfDisposed();
			}
			if (P2 != null)
			{
				P2.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			calib3d_Calib3d_stereoRectify_12(K1.nativeObj, D1.nativeObj, K2.nativeObj, D2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, tvec.nativeObj, R1.nativeObj, R2.nativeObj, P1.nativeObj, P2.nativeObj, Q.nativeObj, flags, newImageSize.width, newImageSize.height, balance, fov_scale);
		}

		public static void stereoRectify(Mat K1, Mat D1, Mat K2, Mat D2, Size imageSize, Mat R, Mat tvec, Mat R1, Mat R2, Mat P1, Mat P2, Mat Q, int flags)
		{
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (R1 != null)
			{
				R1.ThrowIfDisposed();
			}
			if (R2 != null)
			{
				R2.ThrowIfDisposed();
			}
			if (P1 != null)
			{
				P1.ThrowIfDisposed();
			}
			if (P2 != null)
			{
				P2.ThrowIfDisposed();
			}
			if (Q != null)
			{
				Q.ThrowIfDisposed();
			}
			calib3d_Calib3d_stereoRectify_13(K1.nativeObj, D1.nativeObj, K2.nativeObj, D2.nativeObj, imageSize.width, imageSize.height, R.nativeObj, tvec.nativeObj, R1.nativeObj, R2.nativeObj, P1.nativeObj, P2.nativeObj, Q.nativeObj, flags);
		}

		public static void undistortImage(Mat distorted, Mat undistorted, Mat K, Mat D, Mat Knew, Size new_size)
		{
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (Knew != null)
			{
				Knew.ThrowIfDisposed();
			}
			calib3d_Calib3d_undistortImage_10(distorted.nativeObj, undistorted.nativeObj, K.nativeObj, D.nativeObj, Knew.nativeObj, new_size.width, new_size.height);
		}

		public static void undistortImage(Mat distorted, Mat undistorted, Mat K, Mat D)
		{
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			calib3d_Calib3d_undistortImage_11(distorted.nativeObj, undistorted.nativeObj, K.nativeObj, D.nativeObj);
		}

		public static void undistortPoints(Mat distorted, Mat undistorted, Mat K, Mat D, Mat R, Mat P)
		{
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (P != null)
			{
				P.ThrowIfDisposed();
			}
			calib3d_Calib3d_undistortPoints_10(distorted.nativeObj, undistorted.nativeObj, K.nativeObj, D.nativeObj, R.nativeObj, P.nativeObj);
		}

		public static void undistortPoints(Mat distorted, Mat undistorted, Mat K, Mat D)
		{
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			calib3d_Calib3d_undistortPoints_11(distorted.nativeObj, undistorted.nativeObj, K.nativeObj, D.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findEssentialMat_10(IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr cameraMatrix_nativeObj, int method, double prob, double threshold, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findEssentialMat_11(IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr cameraMatrix_nativeObj, int method, double prob, double threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findEssentialMat_12(IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr cameraMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findEssentialMat_13(IntPtr points1_nativeObj, IntPtr points2_nativeObj, double focal, double pp_x, double pp_y, int method, double prob, double threshold, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findEssentialMat_14(IntPtr points1_nativeObj, IntPtr points2_nativeObj, double focal, double pp_x, double pp_y, int method, double prob, double threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findEssentialMat_15(IntPtr points1_nativeObj, IntPtr points2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findFundamentalMat_10(IntPtr points1_mat_nativeObj, IntPtr points2_mat_nativeObj, int method, double param1, double param2, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findFundamentalMat_11(IntPtr points1_mat_nativeObj, IntPtr points2_mat_nativeObj, int method, double param1, double param2);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findFundamentalMat_12(IntPtr points1_mat_nativeObj, IntPtr points2_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findHomography_10(IntPtr srcPoints_mat_nativeObj, IntPtr dstPoints_mat_nativeObj, int method, double ransacReprojThreshold, IntPtr mask_nativeObj, int maxIters, double confidence);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findHomography_11(IntPtr srcPoints_mat_nativeObj, IntPtr dstPoints_mat_nativeObj, int method, double ransacReprojThreshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_findHomography_12(IntPtr srcPoints_mat_nativeObj, IntPtr dstPoints_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_getOptimalNewCameraMatrix_10(IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, double imageSize_width, double imageSize_height, double alpha, double newImgSize_width, double newImgSize_height, double[] validPixROI_out, bool centerPrincipalPoint);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_getOptimalNewCameraMatrix_11(IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, double imageSize_width, double imageSize_height, double alpha);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_initCameraMatrix2D_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double imageSize_width, double imageSize_height, double aspectRatio);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_Calib3d_initCameraMatrix2D_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double imageSize_width, double imageSize_height);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_getValidDisparityROI_10(int roi1_x, int roi1_y, int roi1_width, int roi1_height, int roi2_x, int roi2_y, int roi2_width, int roi2_height, int minDisparity, int numberOfDisparities, int SADWindowSize, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_RQDecomp3x3_10(IntPtr src_nativeObj, IntPtr mtxR_nativeObj, IntPtr mtxQ_nativeObj, IntPtr Qx_nativeObj, IntPtr Qy_nativeObj, IntPtr Qz_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_RQDecomp3x3_11(IntPtr src_nativeObj, IntPtr mtxR_nativeObj, IntPtr mtxQ_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_findChessboardCorners_10(IntPtr image_nativeObj, double patternSize_width, double patternSize_height, IntPtr corners_mat_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_findChessboardCorners_11(IntPtr image_nativeObj, double patternSize_width, double patternSize_height, IntPtr corners_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_findCirclesGrid_10(IntPtr image_nativeObj, double patternSize_width, double patternSize_height, IntPtr centers_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_findCirclesGrid_11(IntPtr image_nativeObj, double patternSize_width, double patternSize_height, IntPtr centers_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_solvePnP_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, bool useExtrinsicGuess, int flags);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_solvePnP_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_solvePnPRansac_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, bool useExtrinsicGuess, int iterationsCount, float reprojectionError, double confidence, IntPtr inliers_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_solvePnPRansac_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_stereoRectifyUncalibrated_10(IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr F_nativeObj, double imgSize_width, double imgSize_height, IntPtr H1_nativeObj, IntPtr H2_nativeObj, double threshold);

		[DllImport("opencvforunity")]
		private static extern bool calib3d_Calib3d_stereoRectifyUncalibrated_11(IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr F_nativeObj, double imgSize_width, double imgSize_height, IntPtr H1_nativeObj, IntPtr H2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_calibrateCamera_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double imageSize_width, double imageSize_height, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_calibrateCamera_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double imageSize_width, double imageSize_height, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_calibrateCamera_12(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double imageSize_width, double imageSize_height, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_sampsonDistance_10(IntPtr pt1_nativeObj, IntPtr pt2_nativeObj, IntPtr F_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_stereoCalibrate_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, IntPtr cameraMatrix1_nativeObj, IntPtr distCoeffs1_nativeObj, IntPtr cameraMatrix2_nativeObj, IntPtr distCoeffs2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj, IntPtr E_nativeObj, IntPtr F_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_stereoCalibrate_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, IntPtr cameraMatrix1_nativeObj, IntPtr distCoeffs1_nativeObj, IntPtr cameraMatrix2_nativeObj, IntPtr distCoeffs2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj, IntPtr E_nativeObj, IntPtr F_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_stereoCalibrate_12(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, IntPtr cameraMatrix1_nativeObj, IntPtr distCoeffs1_nativeObj, IntPtr cameraMatrix2_nativeObj, IntPtr distCoeffs2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj, IntPtr E_nativeObj, IntPtr F_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_calibrate_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double image_size_width, double image_size_height, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_calibrate_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double image_size_width, double image_size_height, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_calibrate_12(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, double image_size_width, double image_size_height, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_stereoCalibrate_13(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, IntPtr K1_nativeObj, IntPtr D1_nativeObj, IntPtr K2_nativeObj, IntPtr D2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_stereoCalibrate_14(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, IntPtr K1_nativeObj, IntPtr D1_nativeObj, IntPtr K2_nativeObj, IntPtr D2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern double calib3d_Calib3d_stereoCalibrate_15(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, IntPtr K1_nativeObj, IntPtr D1_nativeObj, IntPtr K2_nativeObj, IntPtr D2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj);

		[DllImport("opencvforunity")]
		private static extern float calib3d_Calib3d_rectify3Collinear_10(IntPtr cameraMatrix1_nativeObj, IntPtr distCoeffs1_nativeObj, IntPtr cameraMatrix2_nativeObj, IntPtr distCoeffs2_nativeObj, IntPtr cameraMatrix3_nativeObj, IntPtr distCoeffs3_nativeObj, IntPtr imgpt1_mat_nativeObj, IntPtr imgpt3_mat_nativeObj, double imageSize_width, double imageSize_height, IntPtr R12_nativeObj, IntPtr T12_nativeObj, IntPtr R13_nativeObj, IntPtr T13_nativeObj, IntPtr R1_nativeObj, IntPtr R2_nativeObj, IntPtr R3_nativeObj, IntPtr P1_nativeObj, IntPtr P2_nativeObj, IntPtr P3_nativeObj, IntPtr Q_nativeObj, double alpha, double newImgSize_width, double newImgSize_height, double[] roi1_out, double[] roi2_out, int flags);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_decomposeHomographyMat_10(IntPtr H_nativeObj, IntPtr K_nativeObj, IntPtr rotations_mat_nativeObj, IntPtr translations_mat_nativeObj, IntPtr normals_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_estimateAffine3D_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr out_nativeObj, IntPtr inliers_nativeObj, double ransacThreshold, double confidence);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_estimateAffine3D_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr out_nativeObj, IntPtr inliers_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_recoverPose_10(IntPtr E_nativeObj, IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr R_nativeObj, IntPtr t_nativeObj, double focal, double pp_x, double pp_y, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_recoverPose_11(IntPtr E_nativeObj, IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr R_nativeObj, IntPtr t_nativeObj, double focal, double pp_x, double pp_y);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_recoverPose_12(IntPtr E_nativeObj, IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr R_nativeObj, IntPtr t_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_recoverPose_13(IntPtr E_nativeObj, IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr R_nativeObj, IntPtr t_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_Calib3d_recoverPose_14(IntPtr E_nativeObj, IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr R_nativeObj, IntPtr t_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_Rodrigues_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr jacobian_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_Rodrigues_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_calibrationMatrixValues_10(IntPtr cameraMatrix_nativeObj, double imageSize_width, double imageSize_height, double apertureWidth, double apertureHeight, double[] fovx_out, double[] fovy_out, double[] focalLength_out, double[] principalPoint_out, double[] aspectRatio_out);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_composeRT_10(IntPtr rvec1_nativeObj, IntPtr tvec1_nativeObj, IntPtr rvec2_nativeObj, IntPtr tvec2_nativeObj, IntPtr rvec3_nativeObj, IntPtr tvec3_nativeObj, IntPtr dr3dr1_nativeObj, IntPtr dr3dt1_nativeObj, IntPtr dr3dr2_nativeObj, IntPtr dr3dt2_nativeObj, IntPtr dt3dr1_nativeObj, IntPtr dt3dt1_nativeObj, IntPtr dt3dr2_nativeObj, IntPtr dt3dt2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_composeRT_11(IntPtr rvec1_nativeObj, IntPtr tvec1_nativeObj, IntPtr rvec2_nativeObj, IntPtr tvec2_nativeObj, IntPtr rvec3_nativeObj, IntPtr tvec3_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_computeCorrespondEpilines_10(IntPtr points_nativeObj, int whichImage, IntPtr F_nativeObj, IntPtr lines_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_convertPointsFromHomogeneous_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_convertPointsToHomogeneous_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_correctMatches_10(IntPtr F_nativeObj, IntPtr points1_nativeObj, IntPtr points2_nativeObj, IntPtr newPoints1_nativeObj, IntPtr newPoints2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_decomposeEssentialMat_10(IntPtr E_nativeObj, IntPtr R1_nativeObj, IntPtr R2_nativeObj, IntPtr t_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_decomposeProjectionMatrix_10(IntPtr projMatrix_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr rotMatrix_nativeObj, IntPtr transVect_nativeObj, IntPtr rotMatrixX_nativeObj, IntPtr rotMatrixY_nativeObj, IntPtr rotMatrixZ_nativeObj, IntPtr eulerAngles_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_decomposeProjectionMatrix_11(IntPtr projMatrix_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr rotMatrix_nativeObj, IntPtr transVect_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_drawChessboardCorners_10(IntPtr image_nativeObj, double patternSize_width, double patternSize_height, IntPtr corners_mat_nativeObj, bool patternWasFound);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_filterSpeckles_10(IntPtr img_nativeObj, double newVal, int maxSpeckleSize, double maxDiff, IntPtr buf_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_filterSpeckles_11(IntPtr img_nativeObj, double newVal, int maxSpeckleSize, double maxDiff);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_matMulDeriv_10(IntPtr A_nativeObj, IntPtr B_nativeObj, IntPtr dABdA_nativeObj, IntPtr dABdB_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_projectPoints_10(IntPtr objectPoints_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr jacobian_nativeObj, double aspectRatio);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_projectPoints_11(IntPtr objectPoints_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_mat_nativeObj, IntPtr imagePoints_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_reprojectImageTo3D_10(IntPtr disparity_nativeObj, IntPtr _3dImage_nativeObj, IntPtr Q_nativeObj, bool handleMissingValues, int ddepth);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_reprojectImageTo3D_11(IntPtr disparity_nativeObj, IntPtr _3dImage_nativeObj, IntPtr Q_nativeObj, bool handleMissingValues);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_reprojectImageTo3D_12(IntPtr disparity_nativeObj, IntPtr _3dImage_nativeObj, IntPtr Q_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_stereoRectify_10(IntPtr cameraMatrix1_nativeObj, IntPtr distCoeffs1_nativeObj, IntPtr cameraMatrix2_nativeObj, IntPtr distCoeffs2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj, IntPtr R1_nativeObj, IntPtr R2_nativeObj, IntPtr P1_nativeObj, IntPtr P2_nativeObj, IntPtr Q_nativeObj, int flags, double alpha, double newImageSize_width, double newImageSize_height, double[] validPixROI1_out, double[] validPixROI2_out);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_stereoRectify_11(IntPtr cameraMatrix1_nativeObj, IntPtr distCoeffs1_nativeObj, IntPtr cameraMatrix2_nativeObj, IntPtr distCoeffs2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr T_nativeObj, IntPtr R1_nativeObj, IntPtr R2_nativeObj, IntPtr P1_nativeObj, IntPtr P2_nativeObj, IntPtr Q_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_triangulatePoints_10(IntPtr projMatr1_nativeObj, IntPtr projMatr2_nativeObj, IntPtr projPoints1_nativeObj, IntPtr projPoints2_nativeObj, IntPtr points4D_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_validateDisparity_10(IntPtr disparity_nativeObj, IntPtr cost_nativeObj, int minDisparity, int numberOfDisparities, int disp12MaxDisp);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_validateDisparity_11(IntPtr disparity_nativeObj, IntPtr cost_nativeObj, int minDisparity, int numberOfDisparities);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_distortPoints_10(IntPtr undistorted_nativeObj, IntPtr distorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj, double alpha);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_distortPoints_11(IntPtr undistorted_nativeObj, IntPtr distorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_estimateNewCameraMatrixForUndistortRectify_10(IntPtr K_nativeObj, IntPtr D_nativeObj, double image_size_width, double image_size_height, IntPtr R_nativeObj, IntPtr P_nativeObj, double balance, double new_size_width, double new_size_height, double fov_scale);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_estimateNewCameraMatrixForUndistortRectify_11(IntPtr K_nativeObj, IntPtr D_nativeObj, double image_size_width, double image_size_height, IntPtr R_nativeObj, IntPtr P_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_initUndistortRectifyMap_10(IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr R_nativeObj, IntPtr P_nativeObj, double size_width, double size_height, int m1type, IntPtr map1_nativeObj, IntPtr map2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_projectPoints_12(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj, double alpha, IntPtr jacobian_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_projectPoints_13(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_stereoRectify_12(IntPtr K1_nativeObj, IntPtr D1_nativeObj, IntPtr K2_nativeObj, IntPtr D2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr tvec_nativeObj, IntPtr R1_nativeObj, IntPtr R2_nativeObj, IntPtr P1_nativeObj, IntPtr P2_nativeObj, IntPtr Q_nativeObj, int flags, double newImageSize_width, double newImageSize_height, double balance, double fov_scale);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_stereoRectify_13(IntPtr K1_nativeObj, IntPtr D1_nativeObj, IntPtr K2_nativeObj, IntPtr D2_nativeObj, double imageSize_width, double imageSize_height, IntPtr R_nativeObj, IntPtr tvec_nativeObj, IntPtr R1_nativeObj, IntPtr R2_nativeObj, IntPtr P1_nativeObj, IntPtr P2_nativeObj, IntPtr Q_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_undistortImage_10(IntPtr distorted_nativeObj, IntPtr undistorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr Knew_nativeObj, double new_size_width, double new_size_height);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_undistortImage_11(IntPtr distorted_nativeObj, IntPtr undistorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_undistortPoints_10(IntPtr distorted_nativeObj, IntPtr undistorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr R_nativeObj, IntPtr P_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_Calib3d_undistortPoints_11(IntPtr distorted_nativeObj, IntPtr undistorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj);
	}
}
