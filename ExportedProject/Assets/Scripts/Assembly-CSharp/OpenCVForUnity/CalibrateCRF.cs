using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class CalibrateCRF : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected CalibrateCRF(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_CalibrateCRF_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void process(List<Mat> src, Mat dst, Mat times)
		{
			ThrowIfDisposed();
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (times != null)
			{
				times.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			photo_CalibrateCRF_process_10(nativeObj, mat.nativeObj, dst.nativeObj, times.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateCRF_process_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_nativeObj, IntPtr times_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateCRF_delete(IntPtr nativeObj);
	}
}
