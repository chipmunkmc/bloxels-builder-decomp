using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class CalibrateDebevec : CalibrateCRF
	{
		private const string LIBNAME = "opencvforunity";

		public CalibrateDebevec(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_CalibrateDebevec_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool getRandom()
		{
			ThrowIfDisposed();
			return photo_CalibrateDebevec_getRandom_10(nativeObj);
		}

		public float getLambda()
		{
			ThrowIfDisposed();
			return photo_CalibrateDebevec_getLambda_10(nativeObj);
		}

		public int getSamples()
		{
			ThrowIfDisposed();
			return photo_CalibrateDebevec_getSamples_10(nativeObj);
		}

		public void setLambda(float lambda)
		{
			ThrowIfDisposed();
			photo_CalibrateDebevec_setLambda_10(nativeObj, lambda);
		}

		public void setRandom(bool random)
		{
			ThrowIfDisposed();
			photo_CalibrateDebevec_setRandom_10(nativeObj, random);
		}

		public void setSamples(int samples)
		{
			ThrowIfDisposed();
			photo_CalibrateDebevec_setSamples_10(nativeObj, samples);
		}

		[DllImport("opencvforunity")]
		private static extern bool photo_CalibrateDebevec_getRandom_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_CalibrateDebevec_getLambda_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int photo_CalibrateDebevec_getSamples_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateDebevec_setLambda_10(IntPtr nativeObj, float lambda);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateDebevec_setRandom_10(IntPtr nativeObj, bool random);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateDebevec_setSamples_10(IntPtr nativeObj, int samples);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateDebevec_delete(IntPtr nativeObj);
	}
}
