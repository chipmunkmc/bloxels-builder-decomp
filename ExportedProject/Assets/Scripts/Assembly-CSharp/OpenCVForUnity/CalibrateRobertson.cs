using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class CalibrateRobertson : CalibrateCRF
	{
		private const string LIBNAME = "opencvforunity";

		public CalibrateRobertson(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_CalibrateRobertson_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getRadiance()
		{
			ThrowIfDisposed();
			return new Mat(photo_CalibrateRobertson_getRadiance_10(nativeObj));
		}

		public float getThreshold()
		{
			ThrowIfDisposed();
			return photo_CalibrateRobertson_getThreshold_10(nativeObj);
		}

		public int getMaxIter()
		{
			ThrowIfDisposed();
			return photo_CalibrateRobertson_getMaxIter_10(nativeObj);
		}

		public void setMaxIter(int max_iter)
		{
			ThrowIfDisposed();
			photo_CalibrateRobertson_setMaxIter_10(nativeObj, max_iter);
		}

		public void setThreshold(float threshold)
		{
			ThrowIfDisposed();
			photo_CalibrateRobertson_setThreshold_10(nativeObj, threshold);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_CalibrateRobertson_getRadiance_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_CalibrateRobertson_getThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int photo_CalibrateRobertson_getMaxIter_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateRobertson_setMaxIter_10(IntPtr nativeObj, int max_iter);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateRobertson_setThreshold_10(IntPtr nativeObj, float threshold);

		[DllImport("opencvforunity")]
		private static extern void photo_CalibrateRobertson_delete(IntPtr nativeObj);
	}
}
