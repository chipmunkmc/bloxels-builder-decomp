using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class CascadeClassifier : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		protected CascadeClassifier(IntPtr addr)
			: base(addr)
		{
		}

		public CascadeClassifier(string filename)
		{
			nativeObj = objdetect_CascadeClassifier_CascadeClassifier_10(filename);
		}

		public CascadeClassifier()
		{
			nativeObj = objdetect_CascadeClassifier_CascadeClassifier_11();
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						objdetect_CascadeClassifier_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Size getOriginalWindowSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			objdetect_CascadeClassifier_getOriginalWindowSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public static bool convert(string oldcascade, string newcascade)
		{
			return objdetect_CascadeClassifier_convert_10(oldcascade, newcascade);
		}

		public bool empty()
		{
			ThrowIfDisposed();
			return objdetect_CascadeClassifier_empty_10(nativeObj);
		}

		public bool isOldFormatCascade()
		{
			ThrowIfDisposed();
			return objdetect_CascadeClassifier_isOldFormatCascade_10(nativeObj);
		}

		public bool load(string filename)
		{
			ThrowIfDisposed();
			return objdetect_CascadeClassifier_load_10(nativeObj, filename);
		}

		public int getFeatureType()
		{
			ThrowIfDisposed();
			return objdetect_CascadeClassifier_getFeatureType_10(nativeObj);
		}

		public void detectMultiScale(Mat image, MatOfRect objects, double scaleFactor, int minNeighbors, int flags, Size minSize, Size maxSize)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (objects != null)
			{
				objects.ThrowIfDisposed();
			}
			objdetect_CascadeClassifier_detectMultiScale_10(nativeObj, image.nativeObj, objects.nativeObj, scaleFactor, minNeighbors, flags, minSize.width, minSize.height, maxSize.width, maxSize.height);
		}

		public void detectMultiScale(Mat image, MatOfRect objects)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (objects != null)
			{
				objects.ThrowIfDisposed();
			}
			objdetect_CascadeClassifier_detectMultiScale_11(nativeObj, image.nativeObj, objects.nativeObj);
		}

		public void detectMultiScale2(Mat image, MatOfRect objects, MatOfInt numDetections, double scaleFactor, int minNeighbors, int flags, Size minSize, Size maxSize)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (objects != null)
			{
				objects.ThrowIfDisposed();
			}
			if (numDetections != null)
			{
				numDetections.ThrowIfDisposed();
			}
			objdetect_CascadeClassifier_detectMultiScale2_10(nativeObj, image.nativeObj, objects.nativeObj, numDetections.nativeObj, scaleFactor, minNeighbors, flags, minSize.width, minSize.height, maxSize.width, maxSize.height);
		}

		public void detectMultiScale2(Mat image, MatOfRect objects, MatOfInt numDetections)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (objects != null)
			{
				objects.ThrowIfDisposed();
			}
			if (numDetections != null)
			{
				numDetections.ThrowIfDisposed();
			}
			objdetect_CascadeClassifier_detectMultiScale2_11(nativeObj, image.nativeObj, objects.nativeObj, numDetections.nativeObj);
		}

		public void detectMultiScale3(Mat image, MatOfRect objects, MatOfInt rejectLevels, MatOfDouble levelWeights, double scaleFactor, int minNeighbors, int flags, Size minSize, Size maxSize, bool outputRejectLevels)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (objects != null)
			{
				objects.ThrowIfDisposed();
			}
			if (rejectLevels != null)
			{
				rejectLevels.ThrowIfDisposed();
			}
			if (levelWeights != null)
			{
				levelWeights.ThrowIfDisposed();
			}
			objdetect_CascadeClassifier_detectMultiScale3_10(nativeObj, image.nativeObj, objects.nativeObj, rejectLevels.nativeObj, levelWeights.nativeObj, scaleFactor, minNeighbors, flags, minSize.width, minSize.height, maxSize.width, maxSize.height, outputRejectLevels);
		}

		public void detectMultiScale3(Mat image, MatOfRect objects, MatOfInt rejectLevels, MatOfDouble levelWeights)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (objects != null)
			{
				objects.ThrowIfDisposed();
			}
			if (rejectLevels != null)
			{
				rejectLevels.ThrowIfDisposed();
			}
			if (levelWeights != null)
			{
				levelWeights.ThrowIfDisposed();
			}
			objdetect_CascadeClassifier_detectMultiScale3_11(nativeObj, image.nativeObj, objects.nativeObj, rejectLevels.nativeObj, levelWeights.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_CascadeClassifier_CascadeClassifier_10(string filename);

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_CascadeClassifier_CascadeClassifier_11();

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_getOriginalWindowSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_CascadeClassifier_convert_10(string oldcascade, string newcascade);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_CascadeClassifier_empty_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_CascadeClassifier_isOldFormatCascade_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_CascadeClassifier_load_10(IntPtr nativeObj, string filename);

		[DllImport("opencvforunity")]
		private static extern int objdetect_CascadeClassifier_getFeatureType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_detectMultiScale_10(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr objects_mat_nativeObj, double scaleFactor, int minNeighbors, int flags, double minSize_width, double minSize_height, double maxSize_width, double maxSize_height);

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_detectMultiScale_11(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr objects_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_detectMultiScale2_10(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr objects_mat_nativeObj, IntPtr numDetections_mat_nativeObj, double scaleFactor, int minNeighbors, int flags, double minSize_width, double minSize_height, double maxSize_width, double maxSize_height);

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_detectMultiScale2_11(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr objects_mat_nativeObj, IntPtr numDetections_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_detectMultiScale3_10(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr objects_mat_nativeObj, IntPtr rejectLevels_mat_nativeObj, IntPtr levelWeights_mat_nativeObj, double scaleFactor, int minNeighbors, int flags, double minSize_width, double minSize_height, double maxSize_width, double maxSize_height, bool outputRejectLevels);

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_detectMultiScale3_11(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr objects_mat_nativeObj, IntPtr rejectLevels_mat_nativeObj, IntPtr levelWeights_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_CascadeClassifier_delete(IntPtr nativeObj);
	}
}
