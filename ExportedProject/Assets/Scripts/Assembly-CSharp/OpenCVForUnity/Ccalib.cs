using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Ccalib
	{
		public const int CALIB_USE_GUESS = 1;

		public const int CALIB_FIX_SKEW = 2;

		public const int CALIB_FIX_K1 = 4;

		public const int CALIB_FIX_K2 = 8;

		public const int CALIB_FIX_P1 = 16;

		public const int CALIB_FIX_P2 = 32;

		public const int CALIB_FIX_XI = 64;

		public const int CALIB_FIX_GAMMA = 128;

		public const int CALIB_FIX_CENTER = 256;

		public const int RECTIFY_PERSPECTIVE = 1;

		public const int RECTIFY_CYLINDRICAL = 2;

		public const int RECTIFY_LONGLATI = 3;

		public const int RECTIFY_STEREOGRAPHIC = 4;

		public const int XYZRGB = 1;

		public const int XYZ = 2;

		private const string LIBNAME = "opencvforunity";

		public static double calibrate(Mat objectPoints, Mat imagePoints, Size size, Mat K, Mat xi, Mat D, List<Mat> rvecs, List<Mat> tvecs, int flags, TermCriteria criteria, Mat idx)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (xi != null)
			{
				xi.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (idx != null)
			{
				idx.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			Mat mat2 = new Mat();
			double result = ccalib_Ccalib_calibrate_10(objectPoints.nativeObj, imagePoints.nativeObj, size.width, size.height, K.nativeObj, xi.nativeObj, D.nativeObj, mat.nativeObj, mat2.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon, idx.nativeObj);
			Converters.Mat_to_vector_Mat(mat, rvecs);
			mat.release();
			Converters.Mat_to_vector_Mat(mat2, tvecs);
			mat2.release();
			return result;
		}

		public static double calibrate(Mat objectPoints, Mat imagePoints, Size size, Mat K, Mat xi, Mat D, List<Mat> rvecs, List<Mat> tvecs, int flags, TermCriteria criteria)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (xi != null)
			{
				xi.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			Mat mat2 = new Mat();
			double result = ccalib_Ccalib_calibrate_11(objectPoints.nativeObj, imagePoints.nativeObj, size.width, size.height, K.nativeObj, xi.nativeObj, D.nativeObj, mat.nativeObj, mat2.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon);
			Converters.Mat_to_vector_Mat(mat, rvecs);
			mat.release();
			Converters.Mat_to_vector_Mat(mat2, tvecs);
			mat2.release();
			return result;
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Size imageSize1, Size imageSize2, Mat K1, Mat xi1, Mat D1, Mat K2, Mat xi2, Mat D2, Mat rvec, Mat tvec, List<Mat> rvecsL, List<Mat> tvecsL, int flags, TermCriteria criteria, Mat idx)
		{
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (xi1 != null)
			{
				xi1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (xi2 != null)
			{
				xi2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (idx != null)
			{
				idx.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			Mat mat4 = new Mat();
			Mat mat5 = new Mat();
			double result = ccalib_Ccalib_stereoCalibrate_10(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, imageSize1.width, imageSize1.height, imageSize2.width, imageSize2.height, K1.nativeObj, xi1.nativeObj, D1.nativeObj, K2.nativeObj, xi2.nativeObj, D2.nativeObj, rvec.nativeObj, tvec.nativeObj, mat4.nativeObj, mat5.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon, idx.nativeObj);
			Converters.Mat_to_vector_Mat(mat, objectPoints);
			mat.release();
			Converters.Mat_to_vector_Mat(mat2, imagePoints1);
			mat2.release();
			Converters.Mat_to_vector_Mat(mat3, imagePoints2);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, rvecsL);
			mat4.release();
			Converters.Mat_to_vector_Mat(mat5, tvecsL);
			mat5.release();
			return result;
		}

		public static double stereoCalibrate(List<Mat> objectPoints, List<Mat> imagePoints1, List<Mat> imagePoints2, Size imageSize1, Size imageSize2, Mat K1, Mat xi1, Mat D1, Mat K2, Mat xi2, Mat D2, Mat rvec, Mat tvec, List<Mat> rvecsL, List<Mat> tvecsL, int flags, TermCriteria criteria)
		{
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (xi1 != null)
			{
				xi1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (xi2 != null)
			{
				xi2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(objectPoints);
			Mat mat2 = Converters.vector_Mat_to_Mat(imagePoints1);
			Mat mat3 = Converters.vector_Mat_to_Mat(imagePoints2);
			Mat mat4 = new Mat();
			Mat mat5 = new Mat();
			double result = ccalib_Ccalib_stereoCalibrate_11(mat.nativeObj, mat2.nativeObj, mat3.nativeObj, imageSize1.width, imageSize1.height, imageSize2.width, imageSize2.height, K1.nativeObj, xi1.nativeObj, D1.nativeObj, K2.nativeObj, xi2.nativeObj, D2.nativeObj, rvec.nativeObj, tvec.nativeObj, mat4.nativeObj, mat5.nativeObj, flags, criteria.type, criteria.maxCount, criteria.epsilon);
			Converters.Mat_to_vector_Mat(mat, objectPoints);
			mat.release();
			Converters.Mat_to_vector_Mat(mat2, imagePoints1);
			mat2.release();
			Converters.Mat_to_vector_Mat(mat3, imagePoints2);
			mat3.release();
			Converters.Mat_to_vector_Mat(mat4, rvecsL);
			mat4.release();
			Converters.Mat_to_vector_Mat(mat5, tvecsL);
			mat5.release();
			return result;
		}

		public static void initUndistortRectifyMap(Mat K, Mat D, Mat xi, Mat R, Mat P, Size size, int mltype, Mat map1, Mat map2, int flags)
		{
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (xi != null)
			{
				xi.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (P != null)
			{
				P.ThrowIfDisposed();
			}
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			ccalib_Ccalib_initUndistortRectifyMap_10(K.nativeObj, D.nativeObj, xi.nativeObj, R.nativeObj, P.nativeObj, size.width, size.height, mltype, map1.nativeObj, map2.nativeObj, flags);
		}

		public static void projectPoints(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat rvec, Mat tvec, Mat K, double xi, Mat D, Mat jacobian)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (jacobian != null)
			{
				jacobian.ThrowIfDisposed();
			}
			ccalib_Ccalib_projectPoints_10(objectPoints.nativeObj, imagePoints.nativeObj, rvec.nativeObj, tvec.nativeObj, K.nativeObj, xi, D.nativeObj, jacobian.nativeObj);
		}

		public static void projectPoints(MatOfPoint3f objectPoints, MatOfPoint2f imagePoints, Mat rvec, Mat tvec, Mat K, double xi, Mat D)
		{
			if (objectPoints != null)
			{
				objectPoints.ThrowIfDisposed();
			}
			if (imagePoints != null)
			{
				imagePoints.ThrowIfDisposed();
			}
			if (rvec != null)
			{
				rvec.ThrowIfDisposed();
			}
			if (tvec != null)
			{
				tvec.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			ccalib_Ccalib_projectPoints_11(objectPoints.nativeObj, imagePoints.nativeObj, rvec.nativeObj, tvec.nativeObj, K.nativeObj, xi, D.nativeObj);
		}

		public static void stereoReconstruct(Mat image1, Mat image2, Mat K1, Mat D1, Mat xi1, Mat K2, Mat D2, Mat xi2, Mat R, Mat T, int flag, int numDisparities, int SADWindowSize, Mat disparity, Mat image1Rec, Mat image2Rec, Size newSize, Mat Knew, Mat pointCloud, int pointType)
		{
			if (image1 != null)
			{
				image1.ThrowIfDisposed();
			}
			if (image2 != null)
			{
				image2.ThrowIfDisposed();
			}
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (xi1 != null)
			{
				xi1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (xi2 != null)
			{
				xi2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			if (image1Rec != null)
			{
				image1Rec.ThrowIfDisposed();
			}
			if (image2Rec != null)
			{
				image2Rec.ThrowIfDisposed();
			}
			if (Knew != null)
			{
				Knew.ThrowIfDisposed();
			}
			if (pointCloud != null)
			{
				pointCloud.ThrowIfDisposed();
			}
			ccalib_Ccalib_stereoReconstruct_10(image1.nativeObj, image2.nativeObj, K1.nativeObj, D1.nativeObj, xi1.nativeObj, K2.nativeObj, D2.nativeObj, xi2.nativeObj, R.nativeObj, T.nativeObj, flag, numDisparities, SADWindowSize, disparity.nativeObj, image1Rec.nativeObj, image2Rec.nativeObj, newSize.width, newSize.height, Knew.nativeObj, pointCloud.nativeObj, pointType);
		}

		public static void stereoReconstruct(Mat image1, Mat image2, Mat K1, Mat D1, Mat xi1, Mat K2, Mat D2, Mat xi2, Mat R, Mat T, int flag, int numDisparities, int SADWindowSize, Mat disparity, Mat image1Rec, Mat image2Rec)
		{
			if (image1 != null)
			{
				image1.ThrowIfDisposed();
			}
			if (image2 != null)
			{
				image2.ThrowIfDisposed();
			}
			if (K1 != null)
			{
				K1.ThrowIfDisposed();
			}
			if (D1 != null)
			{
				D1.ThrowIfDisposed();
			}
			if (xi1 != null)
			{
				xi1.ThrowIfDisposed();
			}
			if (K2 != null)
			{
				K2.ThrowIfDisposed();
			}
			if (D2 != null)
			{
				D2.ThrowIfDisposed();
			}
			if (xi2 != null)
			{
				xi2.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			if (image1Rec != null)
			{
				image1Rec.ThrowIfDisposed();
			}
			if (image2Rec != null)
			{
				image2Rec.ThrowIfDisposed();
			}
			ccalib_Ccalib_stereoReconstruct_11(image1.nativeObj, image2.nativeObj, K1.nativeObj, D1.nativeObj, xi1.nativeObj, K2.nativeObj, D2.nativeObj, xi2.nativeObj, R.nativeObj, T.nativeObj, flag, numDisparities, SADWindowSize, disparity.nativeObj, image1Rec.nativeObj, image2Rec.nativeObj);
		}

		public static void stereoRectify(Mat R, Mat T, Mat R1, Mat R2)
		{
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (T != null)
			{
				T.ThrowIfDisposed();
			}
			if (R1 != null)
			{
				R1.ThrowIfDisposed();
			}
			if (R2 != null)
			{
				R2.ThrowIfDisposed();
			}
			ccalib_Ccalib_stereoRectify_10(R.nativeObj, T.nativeObj, R1.nativeObj, R2.nativeObj);
		}

		public static void undistortImage(Mat distorted, Mat undistorted, Mat K, Mat D, Mat xi, int flags, Mat Knew, Size new_size, Mat R)
		{
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (xi != null)
			{
				xi.ThrowIfDisposed();
			}
			if (Knew != null)
			{
				Knew.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			ccalib_Ccalib_undistortImage_10(distorted.nativeObj, undistorted.nativeObj, K.nativeObj, D.nativeObj, xi.nativeObj, flags, Knew.nativeObj, new_size.width, new_size.height, R.nativeObj);
		}

		public static void undistortImage(Mat distorted, Mat undistorted, Mat K, Mat D, Mat xi, int flags)
		{
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (xi != null)
			{
				xi.ThrowIfDisposed();
			}
			ccalib_Ccalib_undistortImage_11(distorted.nativeObj, undistorted.nativeObj, K.nativeObj, D.nativeObj, xi.nativeObj, flags);
		}

		public static void undistortPoints(Mat distorted, Mat undistorted, Mat K, Mat D, Mat xi, Mat R)
		{
			if (distorted != null)
			{
				distorted.ThrowIfDisposed();
			}
			if (undistorted != null)
			{
				undistorted.ThrowIfDisposed();
			}
			if (K != null)
			{
				K.ThrowIfDisposed();
			}
			if (D != null)
			{
				D.ThrowIfDisposed();
			}
			if (xi != null)
			{
				xi.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			ccalib_Ccalib_undistortPoints_10(distorted.nativeObj, undistorted.nativeObj, K.nativeObj, D.nativeObj, xi.nativeObj, R.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern double ccalib_Ccalib_calibrate_10(IntPtr objectPoints_nativeObj, IntPtr imagePoints_nativeObj, double size_width, double size_height, IntPtr K_nativeObj, IntPtr xi_nativeObj, IntPtr D_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon, IntPtr idx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ccalib_Ccalib_calibrate_11(IntPtr objectPoints_nativeObj, IntPtr imagePoints_nativeObj, double size_width, double size_height, IntPtr K_nativeObj, IntPtr xi_nativeObj, IntPtr D_nativeObj, IntPtr rvecs_mat_nativeObj, IntPtr tvecs_mat_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern double ccalib_Ccalib_stereoCalibrate_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, double imageSize1_width, double imageSize1_height, double imageSize2_width, double imageSize2_height, IntPtr K1_nativeObj, IntPtr xi1_nativeObj, IntPtr D1_nativeObj, IntPtr K2_nativeObj, IntPtr xi2_nativeObj, IntPtr D2_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr rvecsL_mat_nativeObj, IntPtr tvecsL_mat_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon, IntPtr idx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ccalib_Ccalib_stereoCalibrate_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints1_mat_nativeObj, IntPtr imagePoints2_mat_nativeObj, double imageSize1_width, double imageSize1_height, double imageSize2_width, double imageSize2_height, IntPtr K1_nativeObj, IntPtr xi1_nativeObj, IntPtr D1_nativeObj, IntPtr K2_nativeObj, IntPtr xi2_nativeObj, IntPtr D2_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr rvecsL_mat_nativeObj, IntPtr tvecsL_mat_nativeObj, int flags, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_initUndistortRectifyMap_10(IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr xi_nativeObj, IntPtr R_nativeObj, IntPtr P_nativeObj, double size_width, double size_height, int mltype, IntPtr map1_nativeObj, IntPtr map2_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_projectPoints_10(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr K_nativeObj, double xi, IntPtr D_nativeObj, IntPtr jacobian_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_projectPoints_11(IntPtr objectPoints_mat_nativeObj, IntPtr imagePoints_mat_nativeObj, IntPtr rvec_nativeObj, IntPtr tvec_nativeObj, IntPtr K_nativeObj, double xi, IntPtr D_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_stereoReconstruct_10(IntPtr image1_nativeObj, IntPtr image2_nativeObj, IntPtr K1_nativeObj, IntPtr D1_nativeObj, IntPtr xi1_nativeObj, IntPtr K2_nativeObj, IntPtr D2_nativeObj, IntPtr xi2_nativeObj, IntPtr R_nativeObj, IntPtr T_nativeObj, int flag, int numDisparities, int SADWindowSize, IntPtr disparity_nativeObj, IntPtr image1Rec_nativeObj, IntPtr image2Rec_nativeObj, double newSize_width, double newSize_height, IntPtr Knew_nativeObj, IntPtr pointCloud_nativeObj, int pointType);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_stereoReconstruct_11(IntPtr image1_nativeObj, IntPtr image2_nativeObj, IntPtr K1_nativeObj, IntPtr D1_nativeObj, IntPtr xi1_nativeObj, IntPtr K2_nativeObj, IntPtr D2_nativeObj, IntPtr xi2_nativeObj, IntPtr R_nativeObj, IntPtr T_nativeObj, int flag, int numDisparities, int SADWindowSize, IntPtr disparity_nativeObj, IntPtr image1Rec_nativeObj, IntPtr image2Rec_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_stereoRectify_10(IntPtr R_nativeObj, IntPtr T_nativeObj, IntPtr R1_nativeObj, IntPtr R2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_undistortImage_10(IntPtr distorted_nativeObj, IntPtr undistorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr xi_nativeObj, int flags, IntPtr Knew_nativeObj, double new_size_width, double new_size_height, IntPtr R_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_undistortImage_11(IntPtr distorted_nativeObj, IntPtr undistorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr xi_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void ccalib_Ccalib_undistortPoints_10(IntPtr distorted_nativeObj, IntPtr undistorted_nativeObj, IntPtr K_nativeObj, IntPtr D_nativeObj, IntPtr xi_nativeObj, IntPtr R_nativeObj);
	}
}
