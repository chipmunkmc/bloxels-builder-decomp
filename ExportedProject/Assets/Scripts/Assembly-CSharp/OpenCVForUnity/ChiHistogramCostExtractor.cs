using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class ChiHistogramCostExtractor : HistogramCostExtractor
	{
		private const string LIBNAME = "opencvforunity";

		protected ChiHistogramCostExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_ChiHistogramCostExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DllImport("opencvforunity")]
		private static extern void shape_ChiHistogramCostExtractor_delete(IntPtr nativeObj);
	}
}
