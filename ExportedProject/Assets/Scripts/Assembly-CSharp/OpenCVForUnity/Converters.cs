using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class Converters
	{
		public static Mat vector_Point_to_Mat(List<Point> pts)
		{
			return vector_Point_to_Mat(pts, 4);
		}

		public static Mat vector_Point2f_to_Mat(List<Point> pts)
		{
			return vector_Point_to_Mat(pts, 5);
		}

		public static Mat vector_Point2d_to_Mat(List<Point> pts)
		{
			return vector_Point_to_Mat(pts, 6);
		}

		public static Mat vector_Point_to_Mat(List<Point> pts, int typeDepth)
		{
			int num = ((pts != null) ? pts.Count : 0);
			Mat mat;
			if (num > 0)
			{
				switch (typeDepth)
				{
				case 4:
				{
					mat = new Mat(num, 1, CvType.CV_32SC2);
					int[] array3 = new int[num * 2];
					for (int k = 0; k < num; k++)
					{
						Point point3 = pts[k];
						array3[k * 2] = (int)point3.x;
						array3[k * 2 + 1] = (int)point3.y;
					}
					mat.put(0, 0, array3);
					break;
				}
				case 5:
				{
					mat = new Mat(num, 1, CvType.CV_32FC2);
					float[] array2 = new float[num * 2];
					for (int j = 0; j < num; j++)
					{
						Point point2 = pts[j];
						array2[j * 2] = (float)point2.x;
						array2[j * 2 + 1] = (float)point2.y;
					}
					mat.put(0, 0, array2);
					break;
				}
				case 6:
				{
					mat = new Mat(num, 1, CvType.CV_64FC2);
					double[] array = new double[num * 2];
					for (int i = 0; i < num; i++)
					{
						Point point = pts[i];
						array[i * 2] = point.x;
						array[i * 2 + 1] = point.y;
					}
					mat.put(0, 0, array);
					break;
				}
				default:
					throw new CvException("'typeDepth' can be CV_32S, CV_32F or CV_64F");
				}
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static Mat vector_Point3i_to_Mat(List<Point3> pts)
		{
			return vector_Point3_to_Mat(pts, 4);
		}

		public static Mat vector_Point3f_to_Mat(List<Point3> pts)
		{
			return vector_Point3_to_Mat(pts, 5);
		}

		public static Mat vector_Point3d_to_Mat(List<Point3> pts)
		{
			return vector_Point3_to_Mat(pts, 6);
		}

		public static Mat vector_Point3_to_Mat(List<Point3> pts, int typeDepth)
		{
			int num = ((pts != null) ? pts.Count : 0);
			Mat mat;
			if (num > 0)
			{
				switch (typeDepth)
				{
				case 4:
				{
					mat = new Mat(num, 1, CvType.CV_32SC3);
					int[] array3 = new int[num * 3];
					for (int k = 0; k < num; k++)
					{
						Point3 point3 = pts[k];
						array3[k * 3] = (int)point3.x;
						array3[k * 3 + 1] = (int)point3.y;
						array3[k * 3 + 2] = (int)point3.z;
					}
					mat.put(0, 0, array3);
					break;
				}
				case 5:
				{
					mat = new Mat(num, 1, CvType.CV_32FC3);
					float[] array2 = new float[num * 3];
					for (int j = 0; j < num; j++)
					{
						Point3 point2 = pts[j];
						array2[j * 3] = (float)point2.x;
						array2[j * 3 + 1] = (float)point2.y;
						array2[j * 3 + 2] = (float)point2.z;
					}
					mat.put(0, 0, array2);
					break;
				}
				case 6:
				{
					mat = new Mat(num, 1, CvType.CV_64FC3);
					double[] array = new double[num * 3];
					for (int i = 0; i < num; i++)
					{
						Point3 point = pts[i];
						array[i * 3] = point.x;
						array[i * 3 + 1] = point.y;
						array[i * 3 + 2] = point.z;
					}
					mat.put(0, 0, array);
					break;
				}
				default:
					throw new CvException("'typeDepth' can be CV_32S, CV_32F or CV_64F");
				}
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_Point2f(Mat m, List<Point> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			Mat_to_vector_Point(m, pts);
		}

		public static void Mat_to_vector_Point2d(Mat m, List<Point> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			Mat_to_vector_Point(m, pts);
		}

		public static void Mat_to_vector_Point(Mat m, List<Point> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (pts == null)
			{
				throw new CvException("Output List can't be null");
			}
			int num = m.rows();
			int num2 = m.type();
			if (m.cols() != 1)
			{
				throw new CvException("Input Mat should have one column\n" + m);
			}
			pts.Clear();
			if (num2 == CvType.CV_32SC2)
			{
				int[] array = new int[2 * num];
				m.get(0, 0, array);
				for (int i = 0; i < num; i++)
				{
					pts.Add(new Point(array[i * 2], array[i * 2 + 1]));
				}
				return;
			}
			if (num2 == CvType.CV_32FC2)
			{
				float[] array2 = new float[2 * num];
				m.get(0, 0, array2);
				for (int j = 0; j < num; j++)
				{
					pts.Add(new Point(array2[j * 2], array2[j * 2 + 1]));
				}
				return;
			}
			if (num2 == CvType.CV_64FC2)
			{
				double[] array3 = new double[2 * num];
				m.get(0, 0, array3);
				for (int k = 0; k < num; k++)
				{
					pts.Add(new Point(array3[k * 2], array3[k * 2 + 1]));
				}
				return;
			}
			throw new CvException("Input Mat should be of CV_32SC2, CV_32FC2 or CV_64FC2 type\n" + m);
		}

		public static void Mat_to_vector_Point3i(Mat m, List<Point3> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			Mat_to_vector_Point3(m, pts);
		}

		public static void Mat_to_vector_Point3f(Mat m, List<Point3> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			Mat_to_vector_Point3(m, pts);
		}

		public static void Mat_to_vector_Point3d(Mat m, List<Point3> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			Mat_to_vector_Point3(m, pts);
		}

		public static void Mat_to_vector_Point3(Mat m, List<Point3> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (pts == null)
			{
				throw new CvException("Output List can't be null");
			}
			int num = m.rows();
			int num2 = m.type();
			if (m.cols() != 1)
			{
				throw new CvException("Input Mat should have one column\n" + m);
			}
			pts.Clear();
			if (num2 == CvType.CV_32SC3)
			{
				int[] array = new int[3 * num];
				m.get(0, 0, array);
				for (int i = 0; i < num; i++)
				{
					pts.Add(new Point3(array[i * 3], array[i * 3 + 1], array[i * 3 + 2]));
				}
				return;
			}
			if (num2 == CvType.CV_32FC3)
			{
				float[] array2 = new float[3 * num];
				m.get(0, 0, array2);
				for (int j = 0; j < num; j++)
				{
					pts.Add(new Point3(array2[j * 3], array2[j * 3 + 1], array2[j * 3 + 2]));
				}
				return;
			}
			if (num2 == CvType.CV_64FC3)
			{
				double[] array3 = new double[3 * num];
				m.get(0, 0, array3);
				for (int k = 0; k < num; k++)
				{
					pts.Add(new Point3(array3[k * 3], array3[k * 3 + 1], array3[k * 3 + 2]));
				}
				return;
			}
			throw new CvException("Input Mat should be of CV_32SC3, CV_32FC3 or CV_64FC3 type\n" + m);
		}

		public static Mat vector_Mat_to_Mat(List<Mat> mats)
		{
			int num = ((mats != null) ? mats.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_32SC2);
				int[] array = new int[num * 2];
				for (int i = 0; i < num; i++)
				{
					long num2 = mats[i].nativeObj.ToInt64();
					array[i * 2] = (int)(num2 >> 32);
					array[i * 2 + 1] = (int)(num2 & 0xFFFFFFFFu);
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_Mat(Mat m, List<Mat> mats)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (mats == null)
			{
				throw new CvException("mats == null");
			}
			int num = m.rows();
			if (CvType.CV_32SC2 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_32SC2 != m.type() ||  m.cols()!=1\n" + m);
			}
			mats.Clear();
			int[] array = new int[num * 2];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				long value = ((long)array[i * 2] << 32) | (array[i * 2 + 1] & 0xFFFFFFFFu);
				mats.Add(new Mat(new IntPtr(value)));
			}
		}

		public static Mat vector_float_to_Mat(List<float> fs)
		{
			int num = ((fs != null) ? fs.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_32FC1);
				float[] array = new float[num];
				for (int i = 0; i < num; i++)
				{
					float num2 = fs[i];
					array[i] = num2;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_float(Mat m, List<float> fs)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (fs == null)
			{
				throw new CvException("fs == null");
			}
			int num = m.rows();
			if (CvType.CV_32FC1 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_32FC1 != m.type() ||  m.cols()!=1\n" + m);
			}
			fs.Clear();
			float[] array = new float[num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				fs.Add(array[i]);
			}
		}

		public static Mat vector_uchar_to_Mat(List<byte> bs)
		{
			int num = ((bs != null) ? bs.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_8UC1);
				byte[] array = new byte[num];
				for (int i = 0; i < num; i++)
				{
					byte b = bs[i];
					array[i] = b;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_uchar(Mat m, List<byte> us)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (us == null)
			{
				throw new CvException("Output List can't be null");
			}
			int num = m.rows();
			if (CvType.CV_8UC1 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_8UC1 != m.type() ||  m.cols()!=1\n" + m);
			}
			us.Clear();
			byte[] array = new byte[num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				us.Add(array[i]);
			}
		}

		public static Mat vector_char_to_Mat(List<byte> bs)
		{
			int num = ((bs != null) ? bs.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_8SC1);
				byte[] array = new byte[num];
				for (int i = 0; i < num; i++)
				{
					byte b = bs[i];
					array[i] = b;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static Mat vector_int_to_Mat(List<int> _is)
		{
			int num = ((_is != null) ? _is.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_32SC1);
				int[] array = new int[num];
				for (int i = 0; i < num; i++)
				{
					int num2 = _is[i];
					array[i] = num2;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_int(Mat m, List<int> _is)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (_is == null)
			{
				throw new CvException("is == null");
			}
			int num = m.rows();
			if (CvType.CV_32SC1 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_32SC1 != m.type() ||  m.cols()!=1\n" + m);
			}
			_is.Clear();
			int[] array = new int[num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				_is.Add(array[i]);
			}
		}

		public static void Mat_to_vector_char(Mat m, List<byte> bs)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (bs == null)
			{
				throw new CvException("Output List can't be null");
			}
			int num = m.rows();
			if (CvType.CV_8SC1 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_8SC1 != m.type() ||  m.cols()!=1\n" + m);
			}
			bs.Clear();
			byte[] array = new byte[num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				bs.Add(array[i]);
			}
		}

		public static Mat vector_Rect_to_Mat(List<Rect> rs)
		{
			int num = ((rs != null) ? rs.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_32SC4);
				int[] array = new int[4 * num];
				for (int i = 0; i < num; i++)
				{
					Rect rect = rs[i];
					array[4 * i] = rect.x;
					array[4 * i + 1] = rect.y;
					array[4 * i + 2] = rect.width;
					array[4 * i + 3] = rect.height;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_Rect(Mat m, List<Rect> rs)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (rs == null)
			{
				throw new CvException("rs == null");
			}
			int num = m.rows();
			if (CvType.CV_32SC4 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_32SC4 != m.type() ||  m.rows()!=1\n" + m);
			}
			rs.Clear();
			int[] array = new int[4 * num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				rs.Add(new Rect(array[4 * i], array[4 * i + 1], array[4 * i + 2], array[4 * i + 3]));
			}
		}

		public static Mat vector_KeyPoint_to_Mat(List<KeyPoint> kps)
		{
			int num = ((kps != null) ? kps.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_64FC(7));
				double[] array = new double[num * 7];
				for (int i = 0; i < num; i++)
				{
					KeyPoint keyPoint = kps[i];
					array[7 * i] = keyPoint.pt.x;
					array[7 * i + 1] = keyPoint.pt.y;
					array[7 * i + 2] = keyPoint.size;
					array[7 * i + 3] = keyPoint.angle;
					array[7 * i + 4] = keyPoint.response;
					array[7 * i + 5] = keyPoint.octave;
					array[7 * i + 6] = keyPoint.class_id;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_KeyPoint(Mat m, List<KeyPoint> kps)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (kps == null)
			{
				throw new CvException("Output List can't be null");
			}
			int num = m.rows();
			if (CvType.CV_64FC(7) != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_64FC(7) != m.type() ||  m.cols()!=1\n" + m);
			}
			kps.Clear();
			double[] array = new double[7 * num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				kps.Add(new KeyPoint((float)array[7 * i], (float)array[7 * i + 1], (float)array[7 * i + 2], (float)array[7 * i + 3], (float)array[7 * i + 4], (int)array[7 * i + 5], (int)array[7 * i + 6]));
			}
		}

		public static Mat vector_vector_Point_to_Mat(List<MatOfPoint> pts, List<Mat> mats)
		{
			int num = ((pts != null) ? pts.Count : 0);
			if (num > 0)
			{
				foreach (MatOfPoint pt in pts)
				{
					mats.Add(pt);
				}
				return vector_Mat_to_Mat(mats);
			}
			return new Mat();
		}

		public static void Mat_to_vector_vector_Point(Mat m, List<MatOfPoint> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (pts == null)
			{
				throw new CvException("Output List can't be null");
			}
			if (m == null)
			{
				throw new CvException("Input Mat can't be null");
			}
			List<Mat> list = new List<Mat>(m.rows());
			Mat_to_vector_Mat(m, list);
			foreach (Mat item2 in list)
			{
				MatOfPoint item = new MatOfPoint(item2);
				pts.Add(item);
				item2.release();
			}
			list.Clear();
		}

		public static void Mat_to_vector_vector_Point2f(Mat m, List<MatOfPoint2f> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (pts == null)
			{
				throw new CvException("Output List can't be null");
			}
			if (m == null)
			{
				throw new CvException("Input Mat can't be null");
			}
			List<Mat> list = new List<Mat>(m.rows());
			Mat_to_vector_Mat(m, list);
			foreach (Mat item2 in list)
			{
				MatOfPoint2f item = new MatOfPoint2f(item2);
				pts.Add(item);
				item2.release();
			}
			list.Clear();
		}

		public static Mat vector_vector_Point2f_to_Mat(List<MatOfPoint2f> pts, List<Mat> mats)
		{
			int num = ((pts != null) ? pts.Count : 0);
			if (num > 0)
			{
				foreach (MatOfPoint2f pt in pts)
				{
					mats.Add(pt);
				}
				return vector_Mat_to_Mat(mats);
			}
			return new Mat();
		}

		public static void Mat_to_vector_vector_Point3f(Mat m, List<MatOfPoint3f> pts)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (pts == null)
			{
				throw new CvException("Output List can't be null");
			}
			if (m == null)
			{
				throw new CvException("Input Mat can't be null");
			}
			List<Mat> list = new List<Mat>(m.rows());
			Mat_to_vector_Mat(m, list);
			foreach (Mat item2 in list)
			{
				MatOfPoint3f item = new MatOfPoint3f(item2);
				pts.Add(item);
				item2.release();
			}
			list.Clear();
		}

		public static Mat vector_vector_Point3f_to_Mat(List<MatOfPoint3f> pts, List<Mat> mats)
		{
			int num = ((pts != null) ? pts.Count : 0);
			if (num > 0)
			{
				foreach (MatOfPoint3f pt in pts)
				{
					mats.Add(pt);
				}
				return vector_Mat_to_Mat(mats);
			}
			return new Mat();
		}

		public static Mat vector_vector_KeyPoint_to_Mat(List<MatOfKeyPoint> kps, List<Mat> mats)
		{
			int num = ((kps != null) ? kps.Count : 0);
			if (num > 0)
			{
				foreach (MatOfKeyPoint kp in kps)
				{
					mats.Add(kp);
				}
				return vector_Mat_to_Mat(mats);
			}
			return new Mat();
		}

		public static void Mat_to_vector_vector_KeyPoint(Mat m, List<MatOfKeyPoint> kps)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (kps == null)
			{
				throw new CvException("Output List can't be null");
			}
			if (m == null)
			{
				throw new CvException("Input Mat can't be null");
			}
			List<Mat> list = new List<Mat>(m.rows());
			Mat_to_vector_Mat(m, list);
			foreach (Mat item2 in list)
			{
				MatOfKeyPoint item = new MatOfKeyPoint(item2);
				kps.Add(item);
				item2.release();
			}
			list.Clear();
		}

		public static Mat vector_double_to_Mat(List<double> ds)
		{
			int num = ((ds != null) ? ds.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_64FC1);
				double[] array = new double[num];
				for (int i = 0; i < num; i++)
				{
					double num2 = ds[i];
					array[i] = num2;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_double(Mat m, List<double> ds)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (ds == null)
			{
				throw new CvException("ds == null");
			}
			int num = m.rows();
			if (CvType.CV_64FC1 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_64FC1 != m.type() ||  m.cols()!=1\n" + m);
			}
			ds.Clear();
			double[] array = new double[num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				ds.Add(array[i]);
			}
		}

		public static Mat vector_DMatch_to_Mat(List<DMatch> matches)
		{
			int num = ((matches != null) ? matches.Count : 0);
			Mat mat;
			if (num > 0)
			{
				mat = new Mat(num, 1, CvType.CV_64FC4);
				double[] array = new double[num * 4];
				for (int i = 0; i < num; i++)
				{
					DMatch dMatch = matches[i];
					array[4 * i] = dMatch.queryIdx;
					array[4 * i + 1] = dMatch.trainIdx;
					array[4 * i + 2] = dMatch.imgIdx;
					array[4 * i + 3] = dMatch.distance;
				}
				mat.put(0, 0, array);
			}
			else
			{
				mat = new Mat();
			}
			return mat;
		}

		public static void Mat_to_vector_DMatch(Mat m, List<DMatch> matches)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (matches == null)
			{
				throw new CvException("Output List can't be null");
			}
			int num = m.rows();
			if (CvType.CV_64FC4 != m.type() || m.cols() != 1)
			{
				throw new CvException("CvType.CV_64FC4 != m.type() ||  m.cols()!=1\n" + m);
			}
			matches.Clear();
			double[] array = new double[4 * num];
			m.get(0, 0, array);
			for (int i = 0; i < num; i++)
			{
				matches.Add(new DMatch((int)array[4 * i], (int)array[4 * i + 1], (int)array[4 * i + 2], (float)array[4 * i + 3]));
			}
		}

		public static Mat vector_vector_DMatch_to_Mat(List<MatOfDMatch> lvdm, List<Mat> mats)
		{
			int num = ((lvdm != null) ? lvdm.Count : 0);
			if (num > 0)
			{
				foreach (MatOfDMatch item in lvdm)
				{
					mats.Add(item);
				}
				return vector_Mat_to_Mat(mats);
			}
			return new Mat();
		}

		public static void Mat_to_vector_vector_DMatch(Mat m, List<MatOfDMatch> lvdm)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (lvdm == null)
			{
				throw new CvException("Output List can't be null");
			}
			if (m == null)
			{
				throw new CvException("Input Mat can't be null");
			}
			List<Mat> list = new List<Mat>(m.rows());
			Mat_to_vector_Mat(m, list);
			lvdm.Clear();
			foreach (Mat item2 in list)
			{
				MatOfDMatch item = new MatOfDMatch(item2);
				lvdm.Add(item);
				item2.release();
			}
			list.Clear();
		}

		public static Mat vector_vector_char_to_Mat(List<MatOfByte> lvb, List<Mat> mats)
		{
			int num = ((lvb != null) ? lvb.Count : 0);
			if (num > 0)
			{
				foreach (MatOfByte item in lvb)
				{
					mats.Add(item);
				}
				return vector_Mat_to_Mat(mats);
			}
			return new Mat();
		}

		public static void Mat_to_vector_vector_char(Mat m, List<List<byte>> llb)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (llb == null)
			{
				throw new CvException("Output List can't be null");
			}
			if (m == null)
			{
				throw new CvException("Input Mat can't be null");
			}
			List<Mat> list = new List<Mat>(m.rows());
			Mat_to_vector_Mat(m, list);
			foreach (Mat item in list)
			{
				List<byte> list2 = new List<byte>();
				Mat_to_vector_char(item, list2);
				llb.Add(list2);
				item.release();
			}
			list.Clear();
		}
	}
}
