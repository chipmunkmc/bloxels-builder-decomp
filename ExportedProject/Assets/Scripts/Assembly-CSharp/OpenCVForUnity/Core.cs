using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Core
	{
		public class MinMaxLocResult
		{
			public double minVal;

			public double maxVal;

			public Point minLoc;

			public Point maxLoc;

			public MinMaxLocResult()
			{
				minVal = 0.0;
				maxVal = 0.0;
				minLoc = new Point();
				maxLoc = new Point();
			}
		}

		public static readonly string VERSION = getVersion();

		public static readonly string NATIVE_LIBRARY_NAME = getNativeLibraryName();

		public static readonly int VERSION_MAJOR = getVersionMajor();

		public static readonly int VERSION_MINOR = getVersionMinor();

		public static readonly int VERSION_REVISION = getVersionRevision();

		public static readonly string VERSION_STATUS = getVersionStatus();

		private const int CV_8U = 0;

		private const int CV_8S = 1;

		private const int CV_16U = 2;

		private const int CV_16S = 3;

		private const int CV_32S = 4;

		private const int CV_32F = 5;

		private const int CV_64F = 6;

		private const int CV_USRTYPE1 = 7;

		public const int SVD_MODIFY_A = 1;

		public const int SVD_NO_UV = 2;

		public const int SVD_FULL_UV = 4;

		public const int FILLED = -1;

		public const int REDUCE_SUM = 0;

		public const int REDUCE_AVG = 1;

		public const int REDUCE_MAX = 2;

		public const int REDUCE_MIN = 3;

		public const int StsOk = 0;

		public const int StsBackTrace = -1;

		public const int StsError = -2;

		public const int StsInternal = -3;

		public const int StsNoMem = -4;

		public const int StsBadArg = -5;

		public const int StsBadFunc = -6;

		public const int StsNoConv = -7;

		public const int StsAutoTrace = -8;

		public const int HeaderIsNull = -9;

		public const int BadImageSize = -10;

		public const int BadOffset = -11;

		public const int BadDataPtr = -12;

		public const int BadStep = -13;

		public const int BadModelOrChSeq = -14;

		public const int BadNumChannels = -15;

		public const int BadNumChannel1U = -16;

		public const int BadDepth = -17;

		public const int BadAlphaChannel = -18;

		public const int BadOrder = -19;

		public const int BadOrigin = -20;

		public const int BadAlign = -21;

		public const int BadCallBack = -22;

		public const int BadTileSize = -23;

		public const int BadCOI = -24;

		public const int BadROISize = -25;

		public const int MaskIsTiled = -26;

		public const int StsNullPtr = -27;

		public const int StsVecLengthErr = -28;

		public const int StsFilterStructContentErr = -29;

		public const int StsKernelStructContentErr = -30;

		public const int StsFilterOffsetErr = -31;

		public const int StsBadSize = -201;

		public const int StsDivByZero = -202;

		public const int StsInplaceNotSupported = -203;

		public const int StsObjectNotFound = -204;

		public const int StsUnmatchedFormats = -205;

		public const int StsBadFlag = -206;

		public const int StsBadPoint = -207;

		public const int StsBadMask = -208;

		public const int StsUnmatchedSizes = -209;

		public const int StsUnsupportedFormat = -210;

		public const int StsOutOfRange = -211;

		public const int StsParseError = -212;

		public const int StsNotImplemented = -213;

		public const int StsBadMemBlock = -214;

		public const int StsAssert = -215;

		public const int GpuNotSupported = -216;

		public const int GpuApiCallError = -217;

		public const int OpenGlNotSupported = -218;

		public const int OpenGlApiCallError = -219;

		public const int OpenCLApiCallError = -220;

		public const int OpenCLDoubleNotSupported = -221;

		public const int OpenCLInitError = -222;

		public const int OpenCLNoAMDBlasFft = -223;

		public const int DECOMP_LU = 0;

		public const int DECOMP_SVD = 1;

		public const int DECOMP_EIG = 2;

		public const int DECOMP_CHOLESKY = 3;

		public const int DECOMP_QR = 4;

		public const int DECOMP_NORMAL = 16;

		public const int NORM_INF = 1;

		public const int NORM_L1 = 2;

		public const int NORM_L2 = 4;

		public const int NORM_L2SQR = 5;

		public const int NORM_HAMMING = 6;

		public const int NORM_HAMMING2 = 7;

		public const int NORM_TYPE_MASK = 7;

		public const int NORM_RELATIVE = 8;

		public const int NORM_MINMAX = 32;

		public const int CMP_EQ = 0;

		public const int CMP_GT = 1;

		public const int CMP_GE = 2;

		public const int CMP_LT = 3;

		public const int CMP_LE = 4;

		public const int CMP_NE = 5;

		public const int GEMM_1_T = 1;

		public const int GEMM_2_T = 2;

		public const int GEMM_3_T = 4;

		public const int DFT_INVERSE = 1;

		public const int DFT_SCALE = 2;

		public const int DFT_ROWS = 4;

		public const int DFT_COMPLEX_OUTPUT = 16;

		public const int DFT_REAL_OUTPUT = 32;

		public const int DCT_INVERSE = 1;

		public const int DCT_ROWS = 4;

		public const int BORDER_CONSTANT = 0;

		public const int BORDER_REPLICATE = 1;

		public const int BORDER_REFLECT = 2;

		public const int BORDER_WRAP = 3;

		public const int BORDER_REFLECT_101 = 4;

		public const int BORDER_TRANSPARENT = 5;

		public const int BORDER_REFLECT101 = 4;

		public const int BORDER_DEFAULT = 4;

		public const int BORDER_ISOLATED = 16;

		public const int SORT_EVERY_ROW = 0;

		public const int SORT_EVERY_COLUMN = 1;

		public const int SORT_ASCENDING = 0;

		public const int SORT_DESCENDING = 16;

		public const int COVAR_SCRAMBLED = 0;

		public const int COVAR_NORMAL = 1;

		public const int COVAR_USE_AVG = 2;

		public const int COVAR_SCALE = 4;

		public const int COVAR_ROWS = 8;

		public const int COVAR_COLS = 16;

		public const int KMEANS_RANDOM_CENTERS = 0;

		public const int KMEANS_PP_CENTERS = 2;

		public const int KMEANS_USE_INITIAL_LABELS = 1;

		public const int LINE_4 = 4;

		public const int LINE_8 = 8;

		public const int LINE_AA = 16;

		public const int FONT_HERSHEY_SIMPLEX = 0;

		public const int FONT_HERSHEY_PLAIN = 1;

		public const int FONT_HERSHEY_DUPLEX = 2;

		public const int FONT_HERSHEY_COMPLEX = 3;

		public const int FONT_HERSHEY_TRIPLEX = 4;

		public const int FONT_HERSHEY_COMPLEX_SMALL = 5;

		public const int FONT_HERSHEY_SCRIPT_SIMPLEX = 6;

		public const int FONT_HERSHEY_SCRIPT_COMPLEX = 7;

		public const int FONT_ITALIC = 16;

		private const string LIBNAME = "opencvforunity";

		public static void meanNonAlloc(Mat src, Mat mask, double[] values)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_mean_10(src.nativeObj, mask.nativeObj, values);
		}

		public static void meanNonAlloc(Mat src, double[] values)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			core_Core_mean_11(src.nativeObj, values);
		}

		private static string getVersion()
		{
			return "3.1.0-dev";
		}

		private static string getNativeLibraryName()
		{
			return "opencvforunity";
		}

		private static int getVersionMajor()
		{
			return 3;
		}

		private static int getVersionMinor()
		{
			return 1;
		}

		private static int getVersionRevision()
		{
			return 0;
		}

		private static string getVersionStatus()
		{
			return "-dev";
		}

		public static Scalar mean(Mat src, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			double[] vals = new double[4];
			core_Core_mean_10(src.nativeObj, mask.nativeObj, vals);
			return new Scalar(vals);
		}

		public static Scalar mean(Mat src)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			double[] vals = new double[4];
			core_Core_mean_11(src.nativeObj, vals);
			return new Scalar(vals);
		}

		public static Scalar sumElems(Mat src)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			double[] vals = new double[4];
			core_Core_sumElems_10(src.nativeObj, vals);
			return new Scalar(vals);
		}

		public static Scalar trace(Mat mtx)
		{
			if (mtx != null)
			{
				mtx.ThrowIfDisposed();
			}
			double[] vals = new double[4];
			core_Core_trace_10(mtx.nativeObj, vals);
			return new Scalar(vals);
		}

		public static string getBuildInformation()
		{
			return Marshal.PtrToStringAnsi(core_Core_getBuildInformation_10());
		}

		public static bool checkRange(Mat a, bool quiet, double minVal, double maxVal)
		{
			if (a != null)
			{
				a.ThrowIfDisposed();
			}
			return core_Core_checkRange_10(a.nativeObj, quiet, minVal, maxVal);
		}

		public static bool checkRange(Mat a)
		{
			if (a != null)
			{
				a.ThrowIfDisposed();
			}
			return core_Core_checkRange_11(a.nativeObj);
		}

		public static bool eigen(Mat src, Mat eigenvalues, Mat eigenvectors)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (eigenvalues != null)
			{
				eigenvalues.ThrowIfDisposed();
			}
			if (eigenvectors != null)
			{
				eigenvectors.ThrowIfDisposed();
			}
			return core_Core_eigen_10(src.nativeObj, eigenvalues.nativeObj, eigenvectors.nativeObj);
		}

		public static bool eigen(Mat src, Mat eigenvalues)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (eigenvalues != null)
			{
				eigenvalues.ThrowIfDisposed();
			}
			return core_Core_eigen_11(src.nativeObj, eigenvalues.nativeObj);
		}

		public static bool solve(Mat src1, Mat src2, Mat dst, int flags)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return core_Core_solve_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, flags);
		}

		public static bool solve(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return core_Core_solve_11(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static double Mahalanobis(Mat v1, Mat v2, Mat icovar)
		{
			if (v1 != null)
			{
				v1.ThrowIfDisposed();
			}
			if (v2 != null)
			{
				v2.ThrowIfDisposed();
			}
			if (icovar != null)
			{
				icovar.ThrowIfDisposed();
			}
			return core_Core_Mahalanobis_10(v1.nativeObj, v2.nativeObj, icovar.nativeObj);
		}

		public static double PSNR(Mat src1, Mat src2)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			return core_Core_PSNR_10(src1.nativeObj, src2.nativeObj);
		}

		public static double determinant(Mat mtx)
		{
			if (mtx != null)
			{
				mtx.ThrowIfDisposed();
			}
			return core_Core_determinant_10(mtx.nativeObj);
		}

		public static double getTickFrequency()
		{
			return core_Core_getTickFrequency_10();
		}

		public static double invert(Mat src, Mat dst, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return core_Core_invert_10(src.nativeObj, dst.nativeObj, flags);
		}

		public static double invert(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return core_Core_invert_11(src.nativeObj, dst.nativeObj);
		}

		public static double kmeans(Mat data, int K, Mat bestLabels, TermCriteria criteria, int attempts, int flags, Mat centers)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			if (bestLabels != null)
			{
				bestLabels.ThrowIfDisposed();
			}
			if (centers != null)
			{
				centers.ThrowIfDisposed();
			}
			return core_Core_kmeans_10(data.nativeObj, K, bestLabels.nativeObj, criteria.type, criteria.maxCount, criteria.epsilon, attempts, flags, centers.nativeObj);
		}

		public static double kmeans(Mat data, int K, Mat bestLabels, TermCriteria criteria, int attempts, int flags)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			if (bestLabels != null)
			{
				bestLabels.ThrowIfDisposed();
			}
			return core_Core_kmeans_11(data.nativeObj, K, bestLabels.nativeObj, criteria.type, criteria.maxCount, criteria.epsilon, attempts, flags);
		}

		public static double norm(Mat src1, Mat src2, int normType, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return core_Core_norm_10(src1.nativeObj, src2.nativeObj, normType, mask.nativeObj);
		}

		public static double norm(Mat src1, Mat src2, int normType)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			return core_Core_norm_11(src1.nativeObj, src2.nativeObj, normType);
		}

		public static double norm(Mat src1, Mat src2)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			return core_Core_norm_12(src1.nativeObj, src2.nativeObj);
		}

		public static double norm(Mat src1, int normType, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return core_Core_norm_13(src1.nativeObj, normType, mask.nativeObj);
		}

		public static double norm(Mat src1, int normType)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			return core_Core_norm_14(src1.nativeObj, normType);
		}

		public static double norm(Mat src1)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			return core_Core_norm_15(src1.nativeObj);
		}

		public static double solvePoly(Mat coeffs, Mat roots, int maxIters)
		{
			if (coeffs != null)
			{
				coeffs.ThrowIfDisposed();
			}
			if (roots != null)
			{
				roots.ThrowIfDisposed();
			}
			return core_Core_solvePoly_10(coeffs.nativeObj, roots.nativeObj, maxIters);
		}

		public static double solvePoly(Mat coeffs, Mat roots)
		{
			if (coeffs != null)
			{
				coeffs.ThrowIfDisposed();
			}
			if (roots != null)
			{
				roots.ThrowIfDisposed();
			}
			return core_Core_solvePoly_11(coeffs.nativeObj, roots.nativeObj);
		}

		public static float cubeRoot(float val)
		{
			return core_Core_cubeRoot_10(val);
		}

		public static float fastAtan2(float y, float x)
		{
			return core_Core_fastAtan2_10(y, x);
		}

		public static int borderInterpolate(int p, int len, int borderType)
		{
			return core_Core_borderInterpolate_10(p, len, borderType);
		}

		public static int countNonZero(Mat src)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			return core_Core_countNonZero_10(src.nativeObj);
		}

		public static int getNumThreads()
		{
			return core_Core_getNumThreads_10();
		}

		public static int getNumberOfCPUs()
		{
			return core_Core_getNumberOfCPUs_10();
		}

		public static int getOptimalDFTSize(int vecsize)
		{
			return core_Core_getOptimalDFTSize_10(vecsize);
		}

		public static int getThreadNum()
		{
			return core_Core_getThreadNum_10();
		}

		public static int solveCubic(Mat coeffs, Mat roots)
		{
			if (coeffs != null)
			{
				coeffs.ThrowIfDisposed();
			}
			if (roots != null)
			{
				roots.ThrowIfDisposed();
			}
			return core_Core_solveCubic_10(coeffs.nativeObj, roots.nativeObj);
		}

		public static long getCPUTickCount()
		{
			return core_Core_getCPUTickCount_10();
		}

		public static long getTickCount()
		{
			return core_Core_getTickCount_10();
		}

		public static void LUT(Mat src, Mat lut, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (lut != null)
			{
				lut.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_LUT_10(src.nativeObj, lut.nativeObj, dst.nativeObj);
		}

		public static void PCABackProject(Mat data, Mat mean, Mat eigenvectors, Mat result)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			if (eigenvectors != null)
			{
				eigenvectors.ThrowIfDisposed();
			}
			if (result != null)
			{
				result.ThrowIfDisposed();
			}
			core_Core_PCABackProject_10(data.nativeObj, mean.nativeObj, eigenvectors.nativeObj, result.nativeObj);
		}

		public static void PCACompute(Mat data, Mat mean, Mat eigenvectors, double retainedVariance)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			if (eigenvectors != null)
			{
				eigenvectors.ThrowIfDisposed();
			}
			core_Core_PCACompute_10(data.nativeObj, mean.nativeObj, eigenvectors.nativeObj, retainedVariance);
		}

		public static void PCACompute(Mat data, Mat mean, Mat eigenvectors, int maxComponents)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			if (eigenvectors != null)
			{
				eigenvectors.ThrowIfDisposed();
			}
			core_Core_PCACompute_11(data.nativeObj, mean.nativeObj, eigenvectors.nativeObj, maxComponents);
		}

		public static void PCACompute(Mat data, Mat mean, Mat eigenvectors)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			if (eigenvectors != null)
			{
				eigenvectors.ThrowIfDisposed();
			}
			core_Core_PCACompute_12(data.nativeObj, mean.nativeObj, eigenvectors.nativeObj);
		}

		public static void PCAProject(Mat data, Mat mean, Mat eigenvectors, Mat result)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			if (eigenvectors != null)
			{
				eigenvectors.ThrowIfDisposed();
			}
			if (result != null)
			{
				result.ThrowIfDisposed();
			}
			core_Core_PCAProject_10(data.nativeObj, mean.nativeObj, eigenvectors.nativeObj, result.nativeObj);
		}

		public static void SVBackSubst(Mat w, Mat u, Mat vt, Mat rhs, Mat dst)
		{
			if (w != null)
			{
				w.ThrowIfDisposed();
			}
			if (u != null)
			{
				u.ThrowIfDisposed();
			}
			if (vt != null)
			{
				vt.ThrowIfDisposed();
			}
			if (rhs != null)
			{
				rhs.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_SVBackSubst_10(w.nativeObj, u.nativeObj, vt.nativeObj, rhs.nativeObj, dst.nativeObj);
		}

		public static void SVDecomp(Mat src, Mat w, Mat u, Mat vt, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (w != null)
			{
				w.ThrowIfDisposed();
			}
			if (u != null)
			{
				u.ThrowIfDisposed();
			}
			if (vt != null)
			{
				vt.ThrowIfDisposed();
			}
			core_Core_SVDecomp_10(src.nativeObj, w.nativeObj, u.nativeObj, vt.nativeObj, flags);
		}

		public static void SVDecomp(Mat src, Mat w, Mat u, Mat vt)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (w != null)
			{
				w.ThrowIfDisposed();
			}
			if (u != null)
			{
				u.ThrowIfDisposed();
			}
			if (vt != null)
			{
				vt.ThrowIfDisposed();
			}
			core_Core_SVDecomp_11(src.nativeObj, w.nativeObj, u.nativeObj, vt.nativeObj);
		}

		public static void absdiff(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_absdiff_10(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void absdiff(Mat src1, Scalar src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_absdiff_11(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj);
		}

		public static void add(Mat src1, Mat src2, Mat dst, Mat mask, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_add_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj, dtype);
		}

		public static void add(Mat src1, Mat src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_add_11(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void add(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_add_12(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void add(Mat src1, Scalar src2, Mat dst, Mat mask, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_add_13(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, mask.nativeObj, dtype);
		}

		public static void add(Mat src1, Scalar src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_add_14(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, mask.nativeObj);
		}

		public static void add(Mat src1, Scalar src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_add_15(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj);
		}

		public static void addWeighted(Mat src1, double alpha, Mat src2, double beta, double gamma, Mat dst, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_addWeighted_10(src1.nativeObj, alpha, src2.nativeObj, beta, gamma, dst.nativeObj, dtype);
		}

		public static void addWeighted(Mat src1, double alpha, Mat src2, double beta, double gamma, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_addWeighted_11(src1.nativeObj, alpha, src2.nativeObj, beta, gamma, dst.nativeObj);
		}

		public static void batchDistance(Mat src1, Mat src2, Mat dist, int dtype, Mat nidx, int normType, int K, Mat mask, int update, bool crosscheck)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dist != null)
			{
				dist.ThrowIfDisposed();
			}
			if (nidx != null)
			{
				nidx.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_batchDistance_10(src1.nativeObj, src2.nativeObj, dist.nativeObj, dtype, nidx.nativeObj, normType, K, mask.nativeObj, update, crosscheck);
		}

		public static void batchDistance(Mat src1, Mat src2, Mat dist, int dtype, Mat nidx, int normType, int K)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dist != null)
			{
				dist.ThrowIfDisposed();
			}
			if (nidx != null)
			{
				nidx.ThrowIfDisposed();
			}
			core_Core_batchDistance_11(src1.nativeObj, src2.nativeObj, dist.nativeObj, dtype, nidx.nativeObj, normType, K);
		}

		public static void batchDistance(Mat src1, Mat src2, Mat dist, int dtype, Mat nidx)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dist != null)
			{
				dist.ThrowIfDisposed();
			}
			if (nidx != null)
			{
				nidx.ThrowIfDisposed();
			}
			core_Core_batchDistance_12(src1.nativeObj, src2.nativeObj, dist.nativeObj, dtype, nidx.nativeObj);
		}

		public static void bitwise_and(Mat src1, Mat src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_bitwise_1and_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void bitwise_and(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_bitwise_1and_11(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void bitwise_not(Mat src, Mat dst, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_bitwise_1not_10(src.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void bitwise_not(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_bitwise_1not_11(src.nativeObj, dst.nativeObj);
		}

		public static void bitwise_or(Mat src1, Mat src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_bitwise_1or_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void bitwise_or(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_bitwise_1or_11(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void bitwise_xor(Mat src1, Mat src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_bitwise_1xor_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void bitwise_xor(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_bitwise_1xor_11(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void calcCovarMatrix(Mat samples, Mat covar, Mat mean, int flags, int ctype)
		{
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (covar != null)
			{
				covar.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			core_Core_calcCovarMatrix_10(samples.nativeObj, covar.nativeObj, mean.nativeObj, flags, ctype);
		}

		public static void calcCovarMatrix(Mat samples, Mat covar, Mat mean, int flags)
		{
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (covar != null)
			{
				covar.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			core_Core_calcCovarMatrix_11(samples.nativeObj, covar.nativeObj, mean.nativeObj, flags);
		}

		public static void cartToPolar(Mat x, Mat y, Mat magnitude, Mat angle, bool angleInDegrees)
		{
			if (x != null)
			{
				x.ThrowIfDisposed();
			}
			if (y != null)
			{
				y.ThrowIfDisposed();
			}
			if (magnitude != null)
			{
				magnitude.ThrowIfDisposed();
			}
			if (angle != null)
			{
				angle.ThrowIfDisposed();
			}
			core_Core_cartToPolar_10(x.nativeObj, y.nativeObj, magnitude.nativeObj, angle.nativeObj, angleInDegrees);
		}

		public static void cartToPolar(Mat x, Mat y, Mat magnitude, Mat angle)
		{
			if (x != null)
			{
				x.ThrowIfDisposed();
			}
			if (y != null)
			{
				y.ThrowIfDisposed();
			}
			if (magnitude != null)
			{
				magnitude.ThrowIfDisposed();
			}
			if (angle != null)
			{
				angle.ThrowIfDisposed();
			}
			core_Core_cartToPolar_11(x.nativeObj, y.nativeObj, magnitude.nativeObj, angle.nativeObj);
		}

		public static void compare(Mat src1, Mat src2, Mat dst, int cmpop)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_compare_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, cmpop);
		}

		public static void compare(Mat src1, Scalar src2, Mat dst, int cmpop)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_compare_11(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, cmpop);
		}

		public static void completeSymm(Mat mtx, bool lowerToUpper)
		{
			if (mtx != null)
			{
				mtx.ThrowIfDisposed();
			}
			core_Core_completeSymm_10(mtx.nativeObj, lowerToUpper);
		}

		public static void completeSymm(Mat mtx)
		{
			if (mtx != null)
			{
				mtx.ThrowIfDisposed();
			}
			core_Core_completeSymm_11(mtx.nativeObj);
		}

		public static void convertScaleAbs(Mat src, Mat dst, double alpha, double beta)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_convertScaleAbs_10(src.nativeObj, dst.nativeObj, alpha, beta);
		}

		public static void convertScaleAbs(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_convertScaleAbs_11(src.nativeObj, dst.nativeObj);
		}

		public static void copyMakeBorder(Mat src, Mat dst, int top, int bottom, int left, int right, int borderType, Scalar value)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_copyMakeBorder_10(src.nativeObj, dst.nativeObj, top, bottom, left, right, borderType, value.val[0], value.val[1], value.val[2], value.val[3]);
		}

		public static void copyMakeBorder(Mat src, Mat dst, int top, int bottom, int left, int right, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_copyMakeBorder_11(src.nativeObj, dst.nativeObj, top, bottom, left, right, borderType);
		}

		public static void dct(Mat src, Mat dst, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_dct_10(src.nativeObj, dst.nativeObj, flags);
		}

		public static void dct(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_dct_11(src.nativeObj, dst.nativeObj);
		}

		public static void dft(Mat src, Mat dst, int flags, int nonzeroRows)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_dft_10(src.nativeObj, dst.nativeObj, flags, nonzeroRows);
		}

		public static void dft(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_dft_11(src.nativeObj, dst.nativeObj);
		}

		public static void divide(Mat src1, Mat src2, Mat dst, double scale, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, scale, dtype);
		}

		public static void divide(Mat src1, Mat src2, Mat dst, double scale)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_11(src1.nativeObj, src2.nativeObj, dst.nativeObj, scale);
		}

		public static void divide(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_12(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void divide(Mat src1, Scalar src2, Mat dst, double scale, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_13(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, scale, dtype);
		}

		public static void divide(Mat src1, Scalar src2, Mat dst, double scale)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_14(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, scale);
		}

		public static void divide(Mat src1, Scalar src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_15(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj);
		}

		public static void divide(double scale, Mat src2, Mat dst, int dtype)
		{
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_16(scale, src2.nativeObj, dst.nativeObj, dtype);
		}

		public static void divide(double scale, Mat src2, Mat dst)
		{
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_divide_17(scale, src2.nativeObj, dst.nativeObj);
		}

		public static void exp(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_exp_10(src.nativeObj, dst.nativeObj);
		}

		public static void extractChannel(Mat src, Mat dst, int coi)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_extractChannel_10(src.nativeObj, dst.nativeObj, coi);
		}

		public static void findNonZero(Mat src, Mat idx)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (idx != null)
			{
				idx.ThrowIfDisposed();
			}
			core_Core_findNonZero_10(src.nativeObj, idx.nativeObj);
		}

		public static void flip(Mat src, Mat dst, int flipCode)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_flip_10(src.nativeObj, dst.nativeObj, flipCode);
		}

		public static void gemm(Mat src1, Mat src2, double alpha, Mat src3, double beta, Mat dst, int flags)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (src3 != null)
			{
				src3.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_gemm_10(src1.nativeObj, src2.nativeObj, alpha, src3.nativeObj, beta, dst.nativeObj, flags);
		}

		public static void gemm(Mat src1, Mat src2, double alpha, Mat src3, double beta, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (src3 != null)
			{
				src3.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_gemm_11(src1.nativeObj, src2.nativeObj, alpha, src3.nativeObj, beta, dst.nativeObj);
		}

		public static void hconcat(List<Mat> src, Mat dst)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			core_Core_hconcat_10(mat.nativeObj, dst.nativeObj);
		}

		public static void idct(Mat src, Mat dst, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_idct_10(src.nativeObj, dst.nativeObj, flags);
		}

		public static void idct(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_idct_11(src.nativeObj, dst.nativeObj);
		}

		public static void idft(Mat src, Mat dst, int flags, int nonzeroRows)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_idft_10(src.nativeObj, dst.nativeObj, flags, nonzeroRows);
		}

		public static void idft(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_idft_11(src.nativeObj, dst.nativeObj);
		}

		public static void inRange(Mat src, Scalar lowerb, Scalar upperb, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_inRange_10(src.nativeObj, lowerb.val[0], lowerb.val[1], lowerb.val[2], lowerb.val[3], upperb.val[0], upperb.val[1], upperb.val[2], upperb.val[3], dst.nativeObj);
		}

		public static void insertChannel(Mat src, Mat dst, int coi)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_insertChannel_10(src.nativeObj, dst.nativeObj, coi);
		}

		public static void log(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_log_10(src.nativeObj, dst.nativeObj);
		}

		public static void magnitude(Mat x, Mat y, Mat magnitude)
		{
			if (x != null)
			{
				x.ThrowIfDisposed();
			}
			if (y != null)
			{
				y.ThrowIfDisposed();
			}
			if (magnitude != null)
			{
				magnitude.ThrowIfDisposed();
			}
			core_Core_magnitude_10(x.nativeObj, y.nativeObj, magnitude.nativeObj);
		}

		public static void max(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_max_10(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void max(Mat src1, Scalar src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_max_11(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj);
		}

		public static void meanStdDev(Mat src, MatOfDouble mean, MatOfDouble stddev, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			if (stddev != null)
			{
				stddev.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_meanStdDev_10(src.nativeObj, mean.nativeObj, stddev.nativeObj, mask.nativeObj);
		}

		public static void meanStdDev(Mat src, MatOfDouble mean, MatOfDouble stddev)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mean != null)
			{
				mean.ThrowIfDisposed();
			}
			if (stddev != null)
			{
				stddev.ThrowIfDisposed();
			}
			core_Core_meanStdDev_11(src.nativeObj, mean.nativeObj, stddev.nativeObj);
		}

		public static void merge(List<Mat> mv, Mat dst)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(mv);
			core_Core_merge_10(mat.nativeObj, dst.nativeObj);
		}

		public static void min(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_min_10(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void min(Mat src1, Scalar src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_min_11(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj);
		}

		public static void mixChannels(List<Mat> src, List<Mat> dst, MatOfInt fromTo)
		{
			if (fromTo != null)
			{
				fromTo.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			Mat mat2 = Converters.vector_Mat_to_Mat(dst);
			core_Core_mixChannels_10(mat.nativeObj, mat2.nativeObj, fromTo.nativeObj);
		}

		public static void mulSpectrums(Mat a, Mat b, Mat c, int flags, bool conjB)
		{
			if (a != null)
			{
				a.ThrowIfDisposed();
			}
			if (b != null)
			{
				b.ThrowIfDisposed();
			}
			if (c != null)
			{
				c.ThrowIfDisposed();
			}
			core_Core_mulSpectrums_10(a.nativeObj, b.nativeObj, c.nativeObj, flags, conjB);
		}

		public static void mulSpectrums(Mat a, Mat b, Mat c, int flags)
		{
			if (a != null)
			{
				a.ThrowIfDisposed();
			}
			if (b != null)
			{
				b.ThrowIfDisposed();
			}
			if (c != null)
			{
				c.ThrowIfDisposed();
			}
			core_Core_mulSpectrums_11(a.nativeObj, b.nativeObj, c.nativeObj, flags);
		}

		public static void mulTransposed(Mat src, Mat dst, bool aTa, Mat delta, double scale, int dtype)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (delta != null)
			{
				delta.ThrowIfDisposed();
			}
			core_Core_mulTransposed_10(src.nativeObj, dst.nativeObj, aTa, delta.nativeObj, scale, dtype);
		}

		public static void mulTransposed(Mat src, Mat dst, bool aTa, Mat delta, double scale)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (delta != null)
			{
				delta.ThrowIfDisposed();
			}
			core_Core_mulTransposed_11(src.nativeObj, dst.nativeObj, aTa, delta.nativeObj, scale);
		}

		public static void mulTransposed(Mat src, Mat dst, bool aTa)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_mulTransposed_12(src.nativeObj, dst.nativeObj, aTa);
		}

		public static void multiply(Mat src1, Mat src2, Mat dst, double scale, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_multiply_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, scale, dtype);
		}

		public static void multiply(Mat src1, Mat src2, Mat dst, double scale)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_multiply_11(src1.nativeObj, src2.nativeObj, dst.nativeObj, scale);
		}

		public static void multiply(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_multiply_12(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void multiply(Mat src1, Scalar src2, Mat dst, double scale, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_multiply_13(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, scale, dtype);
		}

		public static void multiply(Mat src1, Scalar src2, Mat dst, double scale)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_multiply_14(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, scale);
		}

		public static void multiply(Mat src1, Scalar src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_multiply_15(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj);
		}

		public static void normalize(Mat src, Mat dst, double alpha, double beta, int norm_type, int dtype, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_normalize_10(src.nativeObj, dst.nativeObj, alpha, beta, norm_type, dtype, mask.nativeObj);
		}

		public static void normalize(Mat src, Mat dst, double alpha, double beta, int norm_type, int dtype)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_normalize_11(src.nativeObj, dst.nativeObj, alpha, beta, norm_type, dtype);
		}

		public static void normalize(Mat src, Mat dst, double alpha, double beta, int norm_type)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_normalize_12(src.nativeObj, dst.nativeObj, alpha, beta, norm_type);
		}

		public static void normalize(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_normalize_13(src.nativeObj, dst.nativeObj);
		}

		public static void patchNaNs(Mat a, double val)
		{
			if (a != null)
			{
				a.ThrowIfDisposed();
			}
			core_Core_patchNaNs_10(a.nativeObj, val);
		}

		public static void patchNaNs(Mat a)
		{
			if (a != null)
			{
				a.ThrowIfDisposed();
			}
			core_Core_patchNaNs_11(a.nativeObj);
		}

		public static void perspectiveTransform(Mat src, Mat dst, Mat m)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			core_Core_perspectiveTransform_10(src.nativeObj, dst.nativeObj, m.nativeObj);
		}

		public static void phase(Mat x, Mat y, Mat angle, bool angleInDegrees)
		{
			if (x != null)
			{
				x.ThrowIfDisposed();
			}
			if (y != null)
			{
				y.ThrowIfDisposed();
			}
			if (angle != null)
			{
				angle.ThrowIfDisposed();
			}
			core_Core_phase_10(x.nativeObj, y.nativeObj, angle.nativeObj, angleInDegrees);
		}

		public static void phase(Mat x, Mat y, Mat angle)
		{
			if (x != null)
			{
				x.ThrowIfDisposed();
			}
			if (y != null)
			{
				y.ThrowIfDisposed();
			}
			if (angle != null)
			{
				angle.ThrowIfDisposed();
			}
			core_Core_phase_11(x.nativeObj, y.nativeObj, angle.nativeObj);
		}

		public static void polarToCart(Mat magnitude, Mat angle, Mat x, Mat y, bool angleInDegrees)
		{
			if (magnitude != null)
			{
				magnitude.ThrowIfDisposed();
			}
			if (angle != null)
			{
				angle.ThrowIfDisposed();
			}
			if (x != null)
			{
				x.ThrowIfDisposed();
			}
			if (y != null)
			{
				y.ThrowIfDisposed();
			}
			core_Core_polarToCart_10(magnitude.nativeObj, angle.nativeObj, x.nativeObj, y.nativeObj, angleInDegrees);
		}

		public static void polarToCart(Mat magnitude, Mat angle, Mat x, Mat y)
		{
			if (magnitude != null)
			{
				magnitude.ThrowIfDisposed();
			}
			if (angle != null)
			{
				angle.ThrowIfDisposed();
			}
			if (x != null)
			{
				x.ThrowIfDisposed();
			}
			if (y != null)
			{
				y.ThrowIfDisposed();
			}
			core_Core_polarToCart_11(magnitude.nativeObj, angle.nativeObj, x.nativeObj, y.nativeObj);
		}

		public static void pow(Mat src, double power, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_pow_10(src.nativeObj, power, dst.nativeObj);
		}

		public static void randShuffle(Mat dst, double iterFactor)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_randShuffle_10(dst.nativeObj, iterFactor);
		}

		public static void randShuffle(Mat dst)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_randShuffle_11(dst.nativeObj);
		}

		public static void randn(Mat dst, double mean, double stddev)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_randn_10(dst.nativeObj, mean, stddev);
		}

		public static void randu(Mat dst, double low, double high)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_randu_10(dst.nativeObj, low, high);
		}

		public static void reduce(Mat src, Mat dst, int dim, int rtype, int dtype)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_reduce_10(src.nativeObj, dst.nativeObj, dim, rtype, dtype);
		}

		public static void reduce(Mat src, Mat dst, int dim, int rtype)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_reduce_11(src.nativeObj, dst.nativeObj, dim, rtype);
		}

		public static void repeat(Mat src, int ny, int nx, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_repeat_10(src.nativeObj, ny, nx, dst.nativeObj);
		}

		public static void scaleAdd(Mat src1, double alpha, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_scaleAdd_10(src1.nativeObj, alpha, src2.nativeObj, dst.nativeObj);
		}

		public static void setErrorVerbosity(bool verbose)
		{
			core_Core_setErrorVerbosity_10(verbose);
		}

		public static void setIdentity(Mat mtx, Scalar s)
		{
			if (mtx != null)
			{
				mtx.ThrowIfDisposed();
			}
			core_Core_setIdentity_10(mtx.nativeObj, s.val[0], s.val[1], s.val[2], s.val[3]);
		}

		public static void setIdentity(Mat mtx)
		{
			if (mtx != null)
			{
				mtx.ThrowIfDisposed();
			}
			core_Core_setIdentity_11(mtx.nativeObj);
		}

		public static void setNumThreads(int nthreads)
		{
			core_Core_setNumThreads_10(nthreads);
		}

		public static void sort(Mat src, Mat dst, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_sort_10(src.nativeObj, dst.nativeObj, flags);
		}

		public static void sortIdx(Mat src, Mat dst, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_sortIdx_10(src.nativeObj, dst.nativeObj, flags);
		}

		public static void split(Mat m, List<Mat> mv)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			core_Core_split_10(m.nativeObj, mat.nativeObj);
			Converters.Mat_to_vector_Mat(mat, mv);
			mat.release();
		}

		public static void sqrt(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_sqrt_10(src.nativeObj, dst.nativeObj);
		}

		public static void subtract(Mat src1, Mat src2, Mat dst, Mat mask, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_subtract_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj, dtype);
		}

		public static void subtract(Mat src1, Mat src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_subtract_11(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void subtract(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_subtract_12(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void subtract(Mat src1, Scalar src2, Mat dst, Mat mask, int dtype)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_subtract_13(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, mask.nativeObj, dtype);
		}

		public static void subtract(Mat src1, Scalar src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			core_Core_subtract_14(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj, mask.nativeObj);
		}

		public static void subtract(Mat src1, Scalar src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_subtract_15(src1.nativeObj, src2.val[0], src2.val[1], src2.val[2], src2.val[3], dst.nativeObj);
		}

		public static void transform(Mat src, Mat dst, Mat m)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			core_Core_transform_10(src.nativeObj, dst.nativeObj, m.nativeObj);
		}

		public static void transpose(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			core_Core_transpose_10(src.nativeObj, dst.nativeObj);
		}

		public static void vconcat(List<Mat> src, Mat dst)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			core_Core_vconcat_10(mat.nativeObj, dst.nativeObj);
		}

		public static MinMaxLocResult minMaxLoc(Mat src, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			MinMaxLocResult minMaxLocResult = new MinMaxLocResult();
			IntPtr mask_nativeObj = IntPtr.Zero;
			if (mask != null)
			{
				mask_nativeObj = mask.nativeObj;
			}
			double[] array = new double[6];
			core_Core_n_1minMaxLocManual(src.nativeObj, mask_nativeObj, array);
			minMaxLocResult.minVal = array[0];
			minMaxLocResult.maxVal = array[1];
			minMaxLocResult.minLoc.x = array[2];
			minMaxLocResult.minLoc.y = array[3];
			minMaxLocResult.maxLoc.x = array[4];
			minMaxLocResult.maxLoc.y = array[5];
			return minMaxLocResult;
		}

		public static MinMaxLocResult minMaxLoc(Mat src)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			return minMaxLoc(src, null);
		}

		[DllImport("opencvforunity")]
		private static extern void core_Core_mean_10(IntPtr src_nativeObj, IntPtr mask_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void core_Core_mean_11(IntPtr src_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void core_Core_sumElems_10(IntPtr src_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void core_Core_trace_10(IntPtr mtx_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Core_getBuildInformation_10();

		[DllImport("opencvforunity")]
		private static extern bool core_Core_checkRange_10(IntPtr a_nativeObj, bool quiet, double minVal, double maxVal);

		[DllImport("opencvforunity")]
		private static extern bool core_Core_checkRange_11(IntPtr a_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool core_Core_eigen_10(IntPtr src_nativeObj, IntPtr eigenvalues_nativeObj, IntPtr eigenvectors_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool core_Core_eigen_11(IntPtr src_nativeObj, IntPtr eigenvalues_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool core_Core_solve_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern bool core_Core_solve_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_Mahalanobis_10(IntPtr v1_nativeObj, IntPtr v2_nativeObj, IntPtr icovar_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_PSNR_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_determinant_10(IntPtr mtx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_getTickFrequency_10();

		[DllImport("opencvforunity")]
		private static extern double core_Core_invert_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern double core_Core_invert_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_kmeans_10(IntPtr data_nativeObj, int K, IntPtr bestLabels_nativeObj, int criteria_type, int criteria_maxCount, double criteria_epsilon, int attempts, int flags, IntPtr centers_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_kmeans_11(IntPtr data_nativeObj, int K, IntPtr bestLabels_nativeObj, int criteria_type, int criteria_maxCount, double criteria_epsilon, int attempts, int flags);

		[DllImport("opencvforunity")]
		private static extern double core_Core_norm_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, int normType, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_norm_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, int normType);

		[DllImport("opencvforunity")]
		private static extern double core_Core_norm_12(IntPtr src1_nativeObj, IntPtr src2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_norm_13(IntPtr src1_nativeObj, int normType, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_norm_14(IntPtr src1_nativeObj, int normType);

		[DllImport("opencvforunity")]
		private static extern double core_Core_norm_15(IntPtr src1_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Core_solvePoly_10(IntPtr coeffs_nativeObj, IntPtr roots_nativeObj, int maxIters);

		[DllImport("opencvforunity")]
		private static extern double core_Core_solvePoly_11(IntPtr coeffs_nativeObj, IntPtr roots_nativeObj);

		[DllImport("opencvforunity")]
		private static extern float core_Core_cubeRoot_10(float val);

		[DllImport("opencvforunity")]
		private static extern float core_Core_fastAtan2_10(float y, float x);

		[DllImport("opencvforunity")]
		private static extern int core_Core_borderInterpolate_10(int p, int len, int borderType);

		[DllImport("opencvforunity")]
		private static extern int core_Core_countNonZero_10(IntPtr src_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int core_Core_getNumThreads_10();

		[DllImport("opencvforunity")]
		private static extern int core_Core_getNumberOfCPUs_10();

		[DllImport("opencvforunity")]
		private static extern int core_Core_getOptimalDFTSize_10(int vecsize);

		[DllImport("opencvforunity")]
		private static extern int core_Core_getThreadNum_10();

		[DllImport("opencvforunity")]
		private static extern int core_Core_solveCubic_10(IntPtr coeffs_nativeObj, IntPtr roots_nativeObj);

		[DllImport("opencvforunity")]
		private static extern long core_Core_getCPUTickCount_10();

		[DllImport("opencvforunity")]
		private static extern long core_Core_getTickCount_10();

		[DllImport("opencvforunity")]
		private static extern void core_Core_LUT_10(IntPtr src_nativeObj, IntPtr lut_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_PCABackProject_10(IntPtr data_nativeObj, IntPtr mean_nativeObj, IntPtr eigenvectors_nativeObj, IntPtr result_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_PCACompute_10(IntPtr data_nativeObj, IntPtr mean_nativeObj, IntPtr eigenvectors_nativeObj, double retainedVariance);

		[DllImport("opencvforunity")]
		private static extern void core_Core_PCACompute_11(IntPtr data_nativeObj, IntPtr mean_nativeObj, IntPtr eigenvectors_nativeObj, int maxComponents);

		[DllImport("opencvforunity")]
		private static extern void core_Core_PCACompute_12(IntPtr data_nativeObj, IntPtr mean_nativeObj, IntPtr eigenvectors_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_PCAProject_10(IntPtr data_nativeObj, IntPtr mean_nativeObj, IntPtr eigenvectors_nativeObj, IntPtr result_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_SVBackSubst_10(IntPtr w_nativeObj, IntPtr u_nativeObj, IntPtr vt_nativeObj, IntPtr rhs_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_SVDecomp_10(IntPtr src_nativeObj, IntPtr w_nativeObj, IntPtr u_nativeObj, IntPtr vt_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_SVDecomp_11(IntPtr src_nativeObj, IntPtr w_nativeObj, IntPtr u_nativeObj, IntPtr vt_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_absdiff_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_absdiff_11(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_add_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_add_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_add_12(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_add_13(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, IntPtr mask_nativeObj, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_add_14(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_add_15(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_addWeighted_10(IntPtr src1_nativeObj, double alpha, IntPtr src2_nativeObj, double beta, double gamma, IntPtr dst_nativeObj, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_addWeighted_11(IntPtr src1_nativeObj, double alpha, IntPtr src2_nativeObj, double beta, double gamma, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_batchDistance_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dist_nativeObj, int dtype, IntPtr nidx_nativeObj, int normType, int K, IntPtr mask_nativeObj, int update, bool crosscheck);

		[DllImport("opencvforunity")]
		private static extern void core_Core_batchDistance_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dist_nativeObj, int dtype, IntPtr nidx_nativeObj, int normType, int K);

		[DllImport("opencvforunity")]
		private static extern void core_Core_batchDistance_12(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dist_nativeObj, int dtype, IntPtr nidx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1and_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1and_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1not_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1not_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1or_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1or_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1xor_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_bitwise_1xor_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_calcCovarMatrix_10(IntPtr samples_nativeObj, IntPtr covar_nativeObj, IntPtr mean_nativeObj, int flags, int ctype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_calcCovarMatrix_11(IntPtr samples_nativeObj, IntPtr covar_nativeObj, IntPtr mean_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_cartToPolar_10(IntPtr x_nativeObj, IntPtr y_nativeObj, IntPtr magnitude_nativeObj, IntPtr angle_nativeObj, bool angleInDegrees);

		[DllImport("opencvforunity")]
		private static extern void core_Core_cartToPolar_11(IntPtr x_nativeObj, IntPtr y_nativeObj, IntPtr magnitude_nativeObj, IntPtr angle_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_compare_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, int cmpop);

		[DllImport("opencvforunity")]
		private static extern void core_Core_compare_11(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, int cmpop);

		[DllImport("opencvforunity")]
		private static extern void core_Core_completeSymm_10(IntPtr mtx_nativeObj, bool lowerToUpper);

		[DllImport("opencvforunity")]
		private static extern void core_Core_completeSymm_11(IntPtr mtx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_convertScaleAbs_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double alpha, double beta);

		[DllImport("opencvforunity")]
		private static extern void core_Core_convertScaleAbs_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_copyMakeBorder_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int top, int bottom, int left, int right, int borderType, double value_val0, double value_val1, double value_val2, double value_val3);

		[DllImport("opencvforunity")]
		private static extern void core_Core_copyMakeBorder_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int top, int bottom, int left, int right, int borderType);

		[DllImport("opencvforunity")]
		private static extern void core_Core_dct_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_dct_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_dft_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags, int nonzeroRows);

		[DllImport("opencvforunity")]
		private static extern void core_Core_dft_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, double scale, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, double scale);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_12(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_13(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, double scale, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_14(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, double scale);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_15(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_16(double scale, IntPtr src2_nativeObj, IntPtr dst_nativeObj, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_divide_17(double scale, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_exp_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_extractChannel_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int coi);

		[DllImport("opencvforunity")]
		private static extern void core_Core_findNonZero_10(IntPtr src_nativeObj, IntPtr idx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_flip_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flipCode);

		[DllImport("opencvforunity")]
		private static extern void core_Core_gemm_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, double alpha, IntPtr src3_nativeObj, double beta, IntPtr dst_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_gemm_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, double alpha, IntPtr src3_nativeObj, double beta, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_hconcat_10(IntPtr src_mat_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_idct_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_idct_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_idft_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags, int nonzeroRows);

		[DllImport("opencvforunity")]
		private static extern void core_Core_idft_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_inRange_10(IntPtr src_nativeObj, double lowerb_val0, double lowerb_val1, double lowerb_val2, double lowerb_val3, double upperb_val0, double upperb_val1, double upperb_val2, double upperb_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_insertChannel_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int coi);

		[DllImport("opencvforunity")]
		private static extern void core_Core_log_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_magnitude_10(IntPtr x_nativeObj, IntPtr y_nativeObj, IntPtr magnitude_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_max_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_max_11(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_meanStdDev_10(IntPtr src_nativeObj, IntPtr mean_mat_nativeObj, IntPtr stddev_mat_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_meanStdDev_11(IntPtr src_nativeObj, IntPtr mean_mat_nativeObj, IntPtr stddev_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_merge_10(IntPtr mv_mat_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_min_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_min_11(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_mixChannels_10(IntPtr src_mat_nativeObj, IntPtr dst_mat_nativeObj, IntPtr fromTo_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_mulSpectrums_10(IntPtr a_nativeObj, IntPtr b_nativeObj, IntPtr c_nativeObj, int flags, bool conjB);

		[DllImport("opencvforunity")]
		private static extern void core_Core_mulSpectrums_11(IntPtr a_nativeObj, IntPtr b_nativeObj, IntPtr c_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_mulTransposed_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, bool aTa, IntPtr delta_nativeObj, double scale, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_mulTransposed_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, bool aTa, IntPtr delta_nativeObj, double scale);

		[DllImport("opencvforunity")]
		private static extern void core_Core_mulTransposed_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, bool aTa);

		[DllImport("opencvforunity")]
		private static extern void core_Core_multiply_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, double scale, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_multiply_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, double scale);

		[DllImport("opencvforunity")]
		private static extern void core_Core_multiply_12(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_multiply_13(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, double scale, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_multiply_14(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, double scale);

		[DllImport("opencvforunity")]
		private static extern void core_Core_multiply_15(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_normalize_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double alpha, double beta, int norm_type, int dtype, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_normalize_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double alpha, double beta, int norm_type, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_normalize_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, double alpha, double beta, int norm_type);

		[DllImport("opencvforunity")]
		private static extern void core_Core_normalize_13(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_patchNaNs_10(IntPtr a_nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void core_Core_patchNaNs_11(IntPtr a_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_perspectiveTransform_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_phase_10(IntPtr x_nativeObj, IntPtr y_nativeObj, IntPtr angle_nativeObj, bool angleInDegrees);

		[DllImport("opencvforunity")]
		private static extern void core_Core_phase_11(IntPtr x_nativeObj, IntPtr y_nativeObj, IntPtr angle_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_polarToCart_10(IntPtr magnitude_nativeObj, IntPtr angle_nativeObj, IntPtr x_nativeObj, IntPtr y_nativeObj, bool angleInDegrees);

		[DllImport("opencvforunity")]
		private static extern void core_Core_polarToCart_11(IntPtr magnitude_nativeObj, IntPtr angle_nativeObj, IntPtr x_nativeObj, IntPtr y_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_pow_10(IntPtr src_nativeObj, double power, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_randShuffle_10(IntPtr dst_nativeObj, double iterFactor);

		[DllImport("opencvforunity")]
		private static extern void core_Core_randShuffle_11(IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_randn_10(IntPtr dst_nativeObj, double mean, double stddev);

		[DllImport("opencvforunity")]
		private static extern void core_Core_randu_10(IntPtr dst_nativeObj, double low, double high);

		[DllImport("opencvforunity")]
		private static extern void core_Core_reduce_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int dim, int rtype, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_reduce_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int dim, int rtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_repeat_10(IntPtr src_nativeObj, int ny, int nx, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_scaleAdd_10(IntPtr src1_nativeObj, double alpha, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_setErrorVerbosity_10(bool verbose);

		[DllImport("opencvforunity")]
		private static extern void core_Core_setIdentity_10(IntPtr mtx_nativeObj, double s_val0, double s_val1, double s_val2, double s_val3);

		[DllImport("opencvforunity")]
		private static extern void core_Core_setIdentity_11(IntPtr mtx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_setNumThreads_10(int nthreads);

		[DllImport("opencvforunity")]
		private static extern void core_Core_sort_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_sortIdx_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void core_Core_split_10(IntPtr m_nativeObj, IntPtr mv_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_sqrt_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_subtract_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_subtract_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_subtract_12(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_subtract_13(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, IntPtr mask_nativeObj, int dtype);

		[DllImport("opencvforunity")]
		private static extern void core_Core_subtract_14(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_subtract_15(IntPtr src1_nativeObj, double src2_val0, double src2_val1, double src2_val2, double src2_val3, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_transform_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_transpose_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_vconcat_10(IntPtr src_mat_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Core_n_1minMaxLocManual(IntPtr src_nativeObj, IntPtr mask_nativeObj, double[] vals);
	}
}
