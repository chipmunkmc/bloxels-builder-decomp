using System;

namespace OpenCVForUnity
{
	public class CvException : ApplicationException
	{
		public CvException()
		{
		}

		public CvException(string message)
			: base(message)
		{
		}

		public CvException(string messageFormat, params object[] args)
			: base(string.Format(messageFormat, args))
		{
		}

		public CvException(string message, Exception innerException)
			: base(message, innerException)
		{
		}
	}
}
