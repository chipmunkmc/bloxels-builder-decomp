namespace OpenCVForUnity
{
	public class DMatch
	{
		public int queryIdx;

		public int trainIdx;

		public int imgIdx;

		public float distance;

		public DMatch()
			: this(-1, -1, float.MaxValue)
		{
		}

		public DMatch(int _queryIdx, int _trainIdx, float _distance)
		{
			queryIdx = _queryIdx;
			trainIdx = _trainIdx;
			imgIdx = -1;
			distance = _distance;
		}

		public DMatch(int _queryIdx, int _trainIdx, int _imgIdx, float _distance)
		{
			queryIdx = _queryIdx;
			trainIdx = _trainIdx;
			imgIdx = _imgIdx;
			distance = _distance;
		}

		public bool lessThan(DMatch it)
		{
			return distance < it.distance;
		}

		public override string ToString()
		{
			return "DMatch [queryIdx=" + queryIdx + ", trainIdx=" + trainIdx + ", imgIdx=" + imgIdx + ", distance=" + distance + "]";
		}
	}
}
