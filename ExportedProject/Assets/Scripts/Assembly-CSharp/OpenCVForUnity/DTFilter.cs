using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DTFilter : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public DTFilter(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_DTFilter_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void filter(Mat src, Mat dst, int dDepth)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_DTFilter_filter_10(nativeObj, src.nativeObj, dst.nativeObj, dDepth);
		}

		public void filter(Mat src, Mat dst)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_DTFilter_filter_11(nativeObj, src.nativeObj, dst.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DTFilter_filter_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, int dDepth);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DTFilter_filter_11(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DTFilter_delete(IntPtr nativeObj);
	}
}
