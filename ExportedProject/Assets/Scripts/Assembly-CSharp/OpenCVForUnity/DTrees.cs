using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DTrees : StatModel
	{
		public const int PREDICT_AUTO = 0;

		public const int PREDICT_SUM = 256;

		public const int PREDICT_MAX_VOTE = 512;

		public const int PREDICT_MASK = 768;

		private const string LIBNAME = "opencvforunity";

		protected DTrees(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_DTrees_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getPriors()
		{
			ThrowIfDisposed();
			return new Mat(ml_DTrees_getPriors_10(nativeObj));
		}

		public static DTrees create()
		{
			return new DTrees(ml_DTrees_create_10());
		}

		public bool getTruncatePrunedTree()
		{
			ThrowIfDisposed();
			return ml_DTrees_getTruncatePrunedTree_10(nativeObj);
		}

		public bool getUse1SERule()
		{
			ThrowIfDisposed();
			return ml_DTrees_getUse1SERule_10(nativeObj);
		}

		public bool getUseSurrogates()
		{
			ThrowIfDisposed();
			return ml_DTrees_getUseSurrogates_10(nativeObj);
		}

		public float getRegressionAccuracy()
		{
			ThrowIfDisposed();
			return ml_DTrees_getRegressionAccuracy_10(nativeObj);
		}

		public int getCVFolds()
		{
			ThrowIfDisposed();
			return ml_DTrees_getCVFolds_10(nativeObj);
		}

		public int getMaxCategories()
		{
			ThrowIfDisposed();
			return ml_DTrees_getMaxCategories_10(nativeObj);
		}

		public int getMaxDepth()
		{
			ThrowIfDisposed();
			return ml_DTrees_getMaxDepth_10(nativeObj);
		}

		public int getMinSampleCount()
		{
			ThrowIfDisposed();
			return ml_DTrees_getMinSampleCount_10(nativeObj);
		}

		public void setCVFolds(int val)
		{
			ThrowIfDisposed();
			ml_DTrees_setCVFolds_10(nativeObj, val);
		}

		public void setMaxCategories(int val)
		{
			ThrowIfDisposed();
			ml_DTrees_setMaxCategories_10(nativeObj, val);
		}

		public void setMaxDepth(int val)
		{
			ThrowIfDisposed();
			ml_DTrees_setMaxDepth_10(nativeObj, val);
		}

		public void setMinSampleCount(int val)
		{
			ThrowIfDisposed();
			ml_DTrees_setMinSampleCount_10(nativeObj, val);
		}

		public void setPriors(Mat val)
		{
			ThrowIfDisposed();
			if (val != null)
			{
				val.ThrowIfDisposed();
			}
			ml_DTrees_setPriors_10(nativeObj, val.nativeObj);
		}

		public void setRegressionAccuracy(float val)
		{
			ThrowIfDisposed();
			ml_DTrees_setRegressionAccuracy_10(nativeObj, val);
		}

		public void setTruncatePrunedTree(bool val)
		{
			ThrowIfDisposed();
			ml_DTrees_setTruncatePrunedTree_10(nativeObj, val);
		}

		public void setUse1SERule(bool val)
		{
			ThrowIfDisposed();
			ml_DTrees_setUse1SERule_10(nativeObj, val);
		}

		public void setUseSurrogates(bool val)
		{
			ThrowIfDisposed();
			ml_DTrees_setUseSurrogates_10(nativeObj, val);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_DTrees_getPriors_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_DTrees_create_10();

		[DllImport("opencvforunity")]
		private static extern bool ml_DTrees_getTruncatePrunedTree_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_DTrees_getUse1SERule_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_DTrees_getUseSurrogates_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ml_DTrees_getRegressionAccuracy_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_DTrees_getCVFolds_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_DTrees_getMaxCategories_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_DTrees_getMaxDepth_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_DTrees_getMinSampleCount_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setCVFolds_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setMaxCategories_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setMaxDepth_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setMinSampleCount_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setPriors_10(IntPtr nativeObj, IntPtr val_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setRegressionAccuracy_10(IntPtr nativeObj, float val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setTruncatePrunedTree_10(IntPtr nativeObj, bool val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setUse1SERule_10(IntPtr nativeObj, bool val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_setUseSurrogates_10(IntPtr nativeObj, bool val);

		[DllImport("opencvforunity")]
		private static extern void ml_DTrees_delete(IntPtr nativeObj);
	}
}
