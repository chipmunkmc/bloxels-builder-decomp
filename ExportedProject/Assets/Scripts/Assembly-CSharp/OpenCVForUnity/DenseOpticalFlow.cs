using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DenseOpticalFlow : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected DenseOpticalFlow(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						video_DenseOpticalFlow_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void calc(Mat I0, Mat I1, Mat flow)
		{
			ThrowIfDisposed();
			if (I0 != null)
			{
				I0.ThrowIfDisposed();
			}
			if (I1 != null)
			{
				I1.ThrowIfDisposed();
			}
			if (flow != null)
			{
				flow.ThrowIfDisposed();
			}
			video_DenseOpticalFlow_calc_10(nativeObj, I0.nativeObj, I1.nativeObj, flow.nativeObj);
		}

		public void collectGarbage()
		{
			ThrowIfDisposed();
			video_DenseOpticalFlow_collectGarbage_10(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void video_DenseOpticalFlow_calc_10(IntPtr nativeObj, IntPtr I0_nativeObj, IntPtr I1_nativeObj, IntPtr flow_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_DenseOpticalFlow_collectGarbage_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_DenseOpticalFlow_delete(IntPtr nativeObj);
	}
}
