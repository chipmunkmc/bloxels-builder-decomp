using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DescriptorExtractor : DisposableOpenCVObject
	{
		private const int OPPONENTEXTRACTOR = 1000;

		public const int ORB = 3;

		public const int BRISK = 5;

		public const int AKAZE = 7;

		public const int OPPONENT_ORB = 1003;

		public const int OPPONENT_BRISK = 1005;

		public const int OPPONENT_AKAZE = 1007;

		private const string LIBNAME = "opencvforunity";

		protected DescriptorExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						features2d_DescriptorExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool empty()
		{
			ThrowIfDisposed();
			return features2d_DescriptorExtractor_empty_10(nativeObj);
		}

		public int descriptorSize()
		{
			ThrowIfDisposed();
			return features2d_DescriptorExtractor_descriptorSize_10(nativeObj);
		}

		public int descriptorType()
		{
			ThrowIfDisposed();
			return features2d_DescriptorExtractor_descriptorType_10(nativeObj);
		}

		public static DescriptorExtractor create(int extractorType)
		{
			return new DescriptorExtractor(features2d_DescriptorExtractor_create_10(extractorType));
		}

		public void compute(Mat image, MatOfKeyPoint keypoints, Mat descriptors)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (keypoints != null)
			{
				keypoints.ThrowIfDisposed();
			}
			if (descriptors != null)
			{
				descriptors.ThrowIfDisposed();
			}
			features2d_DescriptorExtractor_compute_10(nativeObj, image.nativeObj, keypoints.nativeObj, descriptors.nativeObj);
		}

		public void compute(List<Mat> images, List<MatOfKeyPoint> keypoints, List<Mat> descriptors)
		{
			ThrowIfDisposed();
			Mat mat = Converters.vector_Mat_to_Mat(images);
			List<Mat> mats = new List<Mat>((keypoints != null) ? keypoints.Count : 0);
			Mat mat2 = Converters.vector_vector_KeyPoint_to_Mat(keypoints, mats);
			Mat mat3 = new Mat();
			features2d_DescriptorExtractor_compute_11(nativeObj, mat.nativeObj, mat2.nativeObj, mat3.nativeObj);
			Converters.Mat_to_vector_vector_KeyPoint(mat2, keypoints);
			mat2.release();
			Converters.Mat_to_vector_Mat(mat3, descriptors);
			mat3.release();
		}

		public void read(string fileName)
		{
			ThrowIfDisposed();
			features2d_DescriptorExtractor_read_10(nativeObj, fileName);
		}

		public void write(string fileName)
		{
			ThrowIfDisposed();
			features2d_DescriptorExtractor_write_10(nativeObj, fileName);
		}

		[DllImport("opencvforunity")]
		private static extern bool features2d_DescriptorExtractor_empty_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int features2d_DescriptorExtractor_descriptorSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int features2d_DescriptorExtractor_descriptorType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr features2d_DescriptorExtractor_create_10(int extractorType);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorExtractor_compute_10(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr keypoints_mat_nativeObj, IntPtr descriptors_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorExtractor_compute_11(IntPtr nativeObj, IntPtr images_mat_nativeObj, IntPtr keypoints_mat_nativeObj, IntPtr descriptors_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorExtractor_read_10(IntPtr nativeObj, string fileName);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorExtractor_write_10(IntPtr nativeObj, string fileName);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorExtractor_delete(IntPtr nativeObj);
	}
}
