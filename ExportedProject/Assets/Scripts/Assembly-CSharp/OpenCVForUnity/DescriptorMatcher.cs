using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DescriptorMatcher : DisposableOpenCVObject
	{
		public const int FLANNBASED = 1;

		public const int BRUTEFORCE = 2;

		public const int BRUTEFORCE_L1 = 3;

		public const int BRUTEFORCE_HAMMING = 4;

		public const int BRUTEFORCE_HAMMINGLUT = 5;

		public const int BRUTEFORCE_SL2 = 6;

		private const string LIBNAME = "opencvforunity";

		protected DescriptorMatcher(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						features2d_DescriptorMatcher_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool empty()
		{
			ThrowIfDisposed();
			return features2d_DescriptorMatcher_empty_10(nativeObj);
		}

		public bool isMaskSupported()
		{
			ThrowIfDisposed();
			return features2d_DescriptorMatcher_isMaskSupported_10(nativeObj);
		}

		public static DescriptorMatcher create(int matcherType)
		{
			return new DescriptorMatcher(features2d_DescriptorMatcher_create_10(matcherType));
		}

		public DescriptorMatcher clone(bool emptyTrainData)
		{
			ThrowIfDisposed();
			return new DescriptorMatcher(features2d_DescriptorMatcher_clone_10(nativeObj, emptyTrainData));
		}

		public DescriptorMatcher clone()
		{
			ThrowIfDisposed();
			return new DescriptorMatcher(features2d_DescriptorMatcher_clone_11(nativeObj));
		}

		public List<Mat> getTrainDescriptors()
		{
			ThrowIfDisposed();
			List<Mat> list = new List<Mat>();
			Mat m = new Mat(features2d_DescriptorMatcher_getTrainDescriptors_10(nativeObj));
			Converters.Mat_to_vector_Mat(m, list);
			return list;
		}

		public void add(List<Mat> descriptors)
		{
			ThrowIfDisposed();
			Mat mat = Converters.vector_Mat_to_Mat(descriptors);
			features2d_DescriptorMatcher_add_10(nativeObj, mat.nativeObj);
		}

		public void clear()
		{
			ThrowIfDisposed();
			features2d_DescriptorMatcher_clear_10(nativeObj);
		}

		public void knnMatch(Mat queryDescriptors, Mat trainDescriptors, List<MatOfDMatch> matches, int k, Mat mask, bool compactResult)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (trainDescriptors != null)
			{
				trainDescriptors.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			features2d_DescriptorMatcher_knnMatch_10(nativeObj, queryDescriptors.nativeObj, trainDescriptors.nativeObj, mat.nativeObj, k, mask.nativeObj, compactResult);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void knnMatch(Mat queryDescriptors, Mat trainDescriptors, List<MatOfDMatch> matches, int k)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (trainDescriptors != null)
			{
				trainDescriptors.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			features2d_DescriptorMatcher_knnMatch_11(nativeObj, queryDescriptors.nativeObj, trainDescriptors.nativeObj, mat.nativeObj, k);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void knnMatch(Mat queryDescriptors, List<MatOfDMatch> matches, int k, List<Mat> masks, bool compactResult)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			Mat mat2 = Converters.vector_Mat_to_Mat(masks);
			features2d_DescriptorMatcher_knnMatch_12(nativeObj, queryDescriptors.nativeObj, mat.nativeObj, k, mat2.nativeObj, compactResult);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void knnMatch(Mat queryDescriptors, List<MatOfDMatch> matches, int k)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			features2d_DescriptorMatcher_knnMatch_13(nativeObj, queryDescriptors.nativeObj, mat.nativeObj, k);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void match(Mat queryDescriptors, Mat trainDescriptors, MatOfDMatch matches, Mat mask)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (trainDescriptors != null)
			{
				trainDescriptors.ThrowIfDisposed();
			}
			if (matches != null)
			{
				matches.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			features2d_DescriptorMatcher_match_10(nativeObj, queryDescriptors.nativeObj, trainDescriptors.nativeObj, matches.nativeObj, mask.nativeObj);
		}

		public void match(Mat queryDescriptors, Mat trainDescriptors, MatOfDMatch matches)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (trainDescriptors != null)
			{
				trainDescriptors.ThrowIfDisposed();
			}
			if (matches != null)
			{
				matches.ThrowIfDisposed();
			}
			features2d_DescriptorMatcher_match_11(nativeObj, queryDescriptors.nativeObj, trainDescriptors.nativeObj, matches.nativeObj);
		}

		public void match(Mat queryDescriptors, MatOfDMatch matches, List<Mat> masks)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (matches != null)
			{
				matches.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(masks);
			features2d_DescriptorMatcher_match_12(nativeObj, queryDescriptors.nativeObj, matches.nativeObj, mat.nativeObj);
		}

		public void match(Mat queryDescriptors, MatOfDMatch matches)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (matches != null)
			{
				matches.ThrowIfDisposed();
			}
			features2d_DescriptorMatcher_match_13(nativeObj, queryDescriptors.nativeObj, matches.nativeObj);
		}

		public void radiusMatch(Mat queryDescriptors, Mat trainDescriptors, List<MatOfDMatch> matches, float maxDistance, Mat mask, bool compactResult)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (trainDescriptors != null)
			{
				trainDescriptors.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			features2d_DescriptorMatcher_radiusMatch_10(nativeObj, queryDescriptors.nativeObj, trainDescriptors.nativeObj, mat.nativeObj, maxDistance, mask.nativeObj, compactResult);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void radiusMatch(Mat queryDescriptors, Mat trainDescriptors, List<MatOfDMatch> matches, float maxDistance)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			if (trainDescriptors != null)
			{
				trainDescriptors.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			features2d_DescriptorMatcher_radiusMatch_11(nativeObj, queryDescriptors.nativeObj, trainDescriptors.nativeObj, mat.nativeObj, maxDistance);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void radiusMatch(Mat queryDescriptors, List<MatOfDMatch> matches, float maxDistance, List<Mat> masks, bool compactResult)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			Mat mat2 = Converters.vector_Mat_to_Mat(masks);
			features2d_DescriptorMatcher_radiusMatch_12(nativeObj, queryDescriptors.nativeObj, mat.nativeObj, maxDistance, mat2.nativeObj, compactResult);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void radiusMatch(Mat queryDescriptors, List<MatOfDMatch> matches, float maxDistance)
		{
			ThrowIfDisposed();
			if (queryDescriptors != null)
			{
				queryDescriptors.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			features2d_DescriptorMatcher_radiusMatch_13(nativeObj, queryDescriptors.nativeObj, mat.nativeObj, maxDistance);
			Converters.Mat_to_vector_vector_DMatch(mat, matches);
			mat.release();
		}

		public void read(string fileName)
		{
			ThrowIfDisposed();
			features2d_DescriptorMatcher_read_10(nativeObj, fileName);
		}

		public void train()
		{
			ThrowIfDisposed();
			features2d_DescriptorMatcher_train_10(nativeObj);
		}

		public void write(string fileName)
		{
			ThrowIfDisposed();
			features2d_DescriptorMatcher_write_10(nativeObj, fileName);
		}

		[DllImport("opencvforunity")]
		private static extern bool features2d_DescriptorMatcher_empty_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool features2d_DescriptorMatcher_isMaskSupported_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr features2d_DescriptorMatcher_create_10(int matcherType);

		[DllImport("opencvforunity")]
		private static extern IntPtr features2d_DescriptorMatcher_clone_10(IntPtr nativeObj, bool emptyTrainData);

		[DllImport("opencvforunity")]
		private static extern IntPtr features2d_DescriptorMatcher_clone_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr features2d_DescriptorMatcher_getTrainDescriptors_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_add_10(IntPtr nativeObj, IntPtr descriptors_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_clear_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_knnMatch_10(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr trainDescriptors_nativeObj, IntPtr matches_mat_nativeObj, int k, IntPtr mask_nativeObj, bool compactResult);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_knnMatch_11(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr trainDescriptors_nativeObj, IntPtr matches_mat_nativeObj, int k);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_knnMatch_12(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr matches_mat_nativeObj, int k, IntPtr masks_mat_nativeObj, bool compactResult);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_knnMatch_13(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr matches_mat_nativeObj, int k);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_match_10(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr trainDescriptors_nativeObj, IntPtr matches_mat_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_match_11(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr trainDescriptors_nativeObj, IntPtr matches_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_match_12(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr matches_mat_nativeObj, IntPtr masks_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_match_13(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr matches_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_radiusMatch_10(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr trainDescriptors_nativeObj, IntPtr matches_mat_nativeObj, float maxDistance, IntPtr mask_nativeObj, bool compactResult);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_radiusMatch_11(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr trainDescriptors_nativeObj, IntPtr matches_mat_nativeObj, float maxDistance);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_radiusMatch_12(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr matches_mat_nativeObj, float maxDistance, IntPtr masks_mat_nativeObj, bool compactResult);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_radiusMatch_13(IntPtr nativeObj, IntPtr queryDescriptors_nativeObj, IntPtr matches_mat_nativeObj, float maxDistance);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_read_10(IntPtr nativeObj, string fileName);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_train_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_write_10(IntPtr nativeObj, string fileName);

		[DllImport("opencvforunity")]
		private static extern void features2d_DescriptorMatcher_delete(IntPtr nativeObj);
	}
}
