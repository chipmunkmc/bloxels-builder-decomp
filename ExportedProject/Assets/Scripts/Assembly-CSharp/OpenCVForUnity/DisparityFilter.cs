using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DisparityFilter : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected DisparityFilter(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_DisparityFilter_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void filter(Mat disparity_map_left, Mat left_view, Mat filtered_disparity_map, Mat disparity_map_right, Rect ROI, Mat right_view)
		{
			ThrowIfDisposed();
			if (disparity_map_left != null)
			{
				disparity_map_left.ThrowIfDisposed();
			}
			if (left_view != null)
			{
				left_view.ThrowIfDisposed();
			}
			if (filtered_disparity_map != null)
			{
				filtered_disparity_map.ThrowIfDisposed();
			}
			if (disparity_map_right != null)
			{
				disparity_map_right.ThrowIfDisposed();
			}
			if (right_view != null)
			{
				right_view.ThrowIfDisposed();
			}
			ximgproc_DisparityFilter_filter_10(nativeObj, disparity_map_left.nativeObj, left_view.nativeObj, filtered_disparity_map.nativeObj, disparity_map_right.nativeObj, ROI.x, ROI.y, ROI.width, ROI.height, right_view.nativeObj);
		}

		public void filter(Mat disparity_map_left, Mat left_view, Mat filtered_disparity_map)
		{
			ThrowIfDisposed();
			if (disparity_map_left != null)
			{
				disparity_map_left.ThrowIfDisposed();
			}
			if (left_view != null)
			{
				left_view.ThrowIfDisposed();
			}
			if (filtered_disparity_map != null)
			{
				filtered_disparity_map.ThrowIfDisposed();
			}
			ximgproc_DisparityFilter_filter_11(nativeObj, disparity_map_left.nativeObj, left_view.nativeObj, filtered_disparity_map.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityFilter_filter_10(IntPtr nativeObj, IntPtr disparity_map_left_nativeObj, IntPtr left_view_nativeObj, IntPtr filtered_disparity_map_nativeObj, IntPtr disparity_map_right_nativeObj, int ROI_x, int ROI_y, int ROI_width, int ROI_height, IntPtr right_view_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityFilter_filter_11(IntPtr nativeObj, IntPtr disparity_map_left_nativeObj, IntPtr left_view_nativeObj, IntPtr filtered_disparity_map_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityFilter_delete(IntPtr nativeObj);
	}
}
