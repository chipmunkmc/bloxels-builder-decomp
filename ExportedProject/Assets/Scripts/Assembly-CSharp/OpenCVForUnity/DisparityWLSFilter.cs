using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DisparityWLSFilter : DisparityFilter
	{
		private const string LIBNAME = "opencvforunity";

		public DisparityWLSFilter(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_DisparityWLSFilter_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getConfidenceMap()
		{
			ThrowIfDisposed();
			return new Mat(ximgproc_DisparityWLSFilter_getConfidenceMap_10(nativeObj));
		}

		public Rect getROI()
		{
			ThrowIfDisposed();
			double[] vals = new double[3];
			ximgproc_DisparityWLSFilter_getROI_10(nativeObj, vals);
			return new Rect(vals);
		}

		public double getLambda()
		{
			ThrowIfDisposed();
			return ximgproc_DisparityWLSFilter_getLambda_10(nativeObj);
		}

		public double getSigmaColor()
		{
			ThrowIfDisposed();
			return ximgproc_DisparityWLSFilter_getSigmaColor_10(nativeObj);
		}

		public int getDepthDiscontinuityRadius()
		{
			ThrowIfDisposed();
			return ximgproc_DisparityWLSFilter_getDepthDiscontinuityRadius_10(nativeObj);
		}

		public int getLRCthresh()
		{
			ThrowIfDisposed();
			return ximgproc_DisparityWLSFilter_getLRCthresh_10(nativeObj);
		}

		public void setDepthDiscontinuityRadius(int _disc_radius)
		{
			ThrowIfDisposed();
			ximgproc_DisparityWLSFilter_setDepthDiscontinuityRadius_10(nativeObj, _disc_radius);
		}

		public void setLRCthresh(int _LRC_thresh)
		{
			ThrowIfDisposed();
			ximgproc_DisparityWLSFilter_setLRCthresh_10(nativeObj, _LRC_thresh);
		}

		public void setLambda(double _lambda)
		{
			ThrowIfDisposed();
			ximgproc_DisparityWLSFilter_setLambda_10(nativeObj, _lambda);
		}

		public void setSigmaColor(double _sigma_color)
		{
			ThrowIfDisposed();
			ximgproc_DisparityWLSFilter_setSigmaColor_10(nativeObj, _sigma_color);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_DisparityWLSFilter_getConfidenceMap_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityWLSFilter_getROI_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern double ximgproc_DisparityWLSFilter_getLambda_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ximgproc_DisparityWLSFilter_getSigmaColor_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ximgproc_DisparityWLSFilter_getDepthDiscontinuityRadius_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ximgproc_DisparityWLSFilter_getLRCthresh_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityWLSFilter_setDepthDiscontinuityRadius_10(IntPtr nativeObj, int _disc_radius);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityWLSFilter_setLRCthresh_10(IntPtr nativeObj, int _LRC_thresh);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityWLSFilter_setLambda_10(IntPtr nativeObj, double _lambda);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityWLSFilter_setSigmaColor_10(IntPtr nativeObj, double _sigma_color);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_DisparityWLSFilter_delete(IntPtr nativeObj);
	}
}
