using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class DualTVL1OpticalFlow : DenseOpticalFlow
	{
		private const string LIBNAME = "opencvforunity";

		public DualTVL1OpticalFlow(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						video_DualTVL1OpticalFlow_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DllImport("opencvforunity")]
		private static extern void video_DualTVL1OpticalFlow_delete(IntPtr nativeObj);
	}
}
