using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class EM : StatModel
	{
		public const int COV_MAT_SPHERICAL = 0;

		public const int COV_MAT_DIAGONAL = 1;

		public const int COV_MAT_GENERIC = 2;

		public const int COV_MAT_DEFAULT = 1;

		public const int DEFAULT_NCLUSTERS = 5;

		public const int DEFAULT_MAX_ITERS = 100;

		public const int START_E_STEP = 1;

		public const int START_M_STEP = 2;

		public const int START_AUTO_STEP = 0;

		private const string LIBNAME = "opencvforunity";

		protected EM(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_EM_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getMeans()
		{
			ThrowIfDisposed();
			return new Mat(ml_EM_getMeans_10(nativeObj));
		}

		public Mat getWeights()
		{
			ThrowIfDisposed();
			return new Mat(ml_EM_getWeights_10(nativeObj));
		}

		public static EM create()
		{
			return new EM(ml_EM_create_10());
		}

		public TermCriteria getTermCriteria()
		{
			ThrowIfDisposed();
			double[] vals = new double[3];
			ml_EM_getTermCriteria_10(nativeObj, vals);
			return new TermCriteria(vals);
		}

		public double[] predict2(Mat sample, Mat probs)
		{
			ThrowIfDisposed();
			if (sample != null)
			{
				sample.ThrowIfDisposed();
			}
			if (probs != null)
			{
				probs.ThrowIfDisposed();
			}
			double[] array = new double[2];
			ml_EM_predict2_10(nativeObj, sample.nativeObj, probs.nativeObj, array);
			return array;
		}

		public bool trainE(Mat samples, Mat means0, Mat covs0, Mat weights0, Mat logLikelihoods, Mat labels, Mat probs)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (means0 != null)
			{
				means0.ThrowIfDisposed();
			}
			if (covs0 != null)
			{
				covs0.ThrowIfDisposed();
			}
			if (weights0 != null)
			{
				weights0.ThrowIfDisposed();
			}
			if (logLikelihoods != null)
			{
				logLikelihoods.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			if (probs != null)
			{
				probs.ThrowIfDisposed();
			}
			return ml_EM_trainE_10(nativeObj, samples.nativeObj, means0.nativeObj, covs0.nativeObj, weights0.nativeObj, logLikelihoods.nativeObj, labels.nativeObj, probs.nativeObj);
		}

		public bool trainE(Mat samples, Mat means0)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (means0 != null)
			{
				means0.ThrowIfDisposed();
			}
			return ml_EM_trainE_11(nativeObj, samples.nativeObj, means0.nativeObj);
		}

		public bool trainEM(Mat samples, Mat logLikelihoods, Mat labels, Mat probs)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (logLikelihoods != null)
			{
				logLikelihoods.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			if (probs != null)
			{
				probs.ThrowIfDisposed();
			}
			return ml_EM_trainEM_10(nativeObj, samples.nativeObj, logLikelihoods.nativeObj, labels.nativeObj, probs.nativeObj);
		}

		public bool trainEM(Mat samples)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			return ml_EM_trainEM_11(nativeObj, samples.nativeObj);
		}

		public bool trainM(Mat samples, Mat probs0, Mat logLikelihoods, Mat labels, Mat probs)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (probs0 != null)
			{
				probs0.ThrowIfDisposed();
			}
			if (logLikelihoods != null)
			{
				logLikelihoods.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			if (probs != null)
			{
				probs.ThrowIfDisposed();
			}
			return ml_EM_trainM_10(nativeObj, samples.nativeObj, probs0.nativeObj, logLikelihoods.nativeObj, labels.nativeObj, probs.nativeObj);
		}

		public bool trainM(Mat samples, Mat probs0)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (probs0 != null)
			{
				probs0.ThrowIfDisposed();
			}
			return ml_EM_trainM_11(nativeObj, samples.nativeObj, probs0.nativeObj);
		}

		public int getClustersNumber()
		{
			ThrowIfDisposed();
			return ml_EM_getClustersNumber_10(nativeObj);
		}

		public int getCovarianceMatrixType()
		{
			ThrowIfDisposed();
			return ml_EM_getCovarianceMatrixType_10(nativeObj);
		}

		public void getCovs(List<Mat> covs)
		{
			ThrowIfDisposed();
			Mat mat = new Mat();
			ml_EM_getCovs_10(nativeObj, mat.nativeObj);
			Converters.Mat_to_vector_Mat(mat, covs);
			mat.release();
		}

		public void setClustersNumber(int val)
		{
			ThrowIfDisposed();
			ml_EM_setClustersNumber_10(nativeObj, val);
		}

		public void setCovarianceMatrixType(int val)
		{
			ThrowIfDisposed();
			ml_EM_setCovarianceMatrixType_10(nativeObj, val);
		}

		public void setTermCriteria(TermCriteria val)
		{
			ThrowIfDisposed();
			ml_EM_setTermCriteria_10(nativeObj, val.type, val.maxCount, val.epsilon);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_EM_getMeans_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_EM_getWeights_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_EM_create_10();

		[DllImport("opencvforunity")]
		private static extern void ml_EM_getTermCriteria_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void ml_EM_predict2_10(IntPtr nativeObj, IntPtr sample_nativeObj, IntPtr probs_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern bool ml_EM_trainE_10(IntPtr nativeObj, IntPtr samples_nativeObj, IntPtr means0_nativeObj, IntPtr covs0_nativeObj, IntPtr weights0_nativeObj, IntPtr logLikelihoods_nativeObj, IntPtr labels_nativeObj, IntPtr probs_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_EM_trainE_11(IntPtr nativeObj, IntPtr samples_nativeObj, IntPtr means0_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_EM_trainEM_10(IntPtr nativeObj, IntPtr samples_nativeObj, IntPtr logLikelihoods_nativeObj, IntPtr labels_nativeObj, IntPtr probs_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_EM_trainEM_11(IntPtr nativeObj, IntPtr samples_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_EM_trainM_10(IntPtr nativeObj, IntPtr samples_nativeObj, IntPtr probs0_nativeObj, IntPtr logLikelihoods_nativeObj, IntPtr labels_nativeObj, IntPtr probs_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_EM_trainM_11(IntPtr nativeObj, IntPtr samples_nativeObj, IntPtr probs0_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_EM_getClustersNumber_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_EM_getCovarianceMatrixType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_EM_getCovs_10(IntPtr nativeObj, IntPtr covs_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_EM_setClustersNumber_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_EM_setCovarianceMatrixType_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_EM_setTermCriteria_10(IntPtr nativeObj, int val_type, int val_maxCount, double val_epsilon);

		[DllImport("opencvforunity")]
		private static extern void ml_EM_delete(IntPtr nativeObj);
	}
}
