using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class EMDHistogramCostExtractor : HistogramCostExtractor
	{
		private const string LIBNAME = "opencvforunity";

		protected EMDHistogramCostExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_EMDHistogramCostExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public int getNormFlag()
		{
			ThrowIfDisposed();
			return shape_EMDHistogramCostExtractor_getNormFlag_10(nativeObj);
		}

		public void setNormFlag(int flag)
		{
			ThrowIfDisposed();
			shape_EMDHistogramCostExtractor_setNormFlag_10(nativeObj, flag);
		}

		[DllImport("opencvforunity")]
		private static extern int shape_EMDHistogramCostExtractor_getNormFlag_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_EMDHistogramCostExtractor_setNormFlag_10(IntPtr nativeObj, int flag);

		[DllImport("opencvforunity")]
		private static extern void shape_EMDHistogramCostExtractor_delete(IntPtr nativeObj);
	}
}
