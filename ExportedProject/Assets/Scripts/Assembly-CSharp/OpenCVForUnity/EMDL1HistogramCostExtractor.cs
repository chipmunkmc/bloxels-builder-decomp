using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class EMDL1HistogramCostExtractor : HistogramCostExtractor
	{
		private const string LIBNAME = "opencvforunity";

		protected EMDL1HistogramCostExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_EMDL1HistogramCostExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		[DllImport("opencvforunity")]
		private static extern void shape_EMDL1HistogramCostExtractor_delete(IntPtr nativeObj);
	}
}
