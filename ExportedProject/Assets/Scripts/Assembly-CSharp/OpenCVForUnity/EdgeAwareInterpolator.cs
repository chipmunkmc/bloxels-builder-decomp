using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class EdgeAwareInterpolator : SparseMatchInterpolator
	{
		private const string LIBNAME = "opencvforunity";

		public EdgeAwareInterpolator(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_EdgeAwareInterpolator_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool getUsePostProcessing()
		{
			ThrowIfDisposed();
			return ximgproc_EdgeAwareInterpolator_getUsePostProcessing_10(nativeObj);
		}

		public float getFGSLambda()
		{
			ThrowIfDisposed();
			return ximgproc_EdgeAwareInterpolator_getFGSLambda_10(nativeObj);
		}

		public float getFGSSigma()
		{
			ThrowIfDisposed();
			return ximgproc_EdgeAwareInterpolator_getFGSSigma_10(nativeObj);
		}

		public float getLambda()
		{
			ThrowIfDisposed();
			return ximgproc_EdgeAwareInterpolator_getLambda_10(nativeObj);
		}

		public float getSigma()
		{
			ThrowIfDisposed();
			return ximgproc_EdgeAwareInterpolator_getSigma_10(nativeObj);
		}

		public int getK()
		{
			ThrowIfDisposed();
			return ximgproc_EdgeAwareInterpolator_getK_10(nativeObj);
		}

		public void setFGSLambda(float _lambda)
		{
			ThrowIfDisposed();
			ximgproc_EdgeAwareInterpolator_setFGSLambda_10(nativeObj, _lambda);
		}

		public void setFGSSigma(float _sigma)
		{
			ThrowIfDisposed();
			ximgproc_EdgeAwareInterpolator_setFGSSigma_10(nativeObj, _sigma);
		}

		public void setK(int _k)
		{
			ThrowIfDisposed();
			ximgproc_EdgeAwareInterpolator_setK_10(nativeObj, _k);
		}

		public void setLambda(float _lambda)
		{
			ThrowIfDisposed();
			ximgproc_EdgeAwareInterpolator_setLambda_10(nativeObj, _lambda);
		}

		public void setSigma(float _sigma)
		{
			ThrowIfDisposed();
			ximgproc_EdgeAwareInterpolator_setSigma_10(nativeObj, _sigma);
		}

		public void setUsePostProcessing(bool _use_post_proc)
		{
			ThrowIfDisposed();
			ximgproc_EdgeAwareInterpolator_setUsePostProcessing_10(nativeObj, _use_post_proc);
		}

		[DllImport("opencvforunity")]
		private static extern bool ximgproc_EdgeAwareInterpolator_getUsePostProcessing_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ximgproc_EdgeAwareInterpolator_getFGSLambda_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ximgproc_EdgeAwareInterpolator_getFGSSigma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ximgproc_EdgeAwareInterpolator_getLambda_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ximgproc_EdgeAwareInterpolator_getSigma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ximgproc_EdgeAwareInterpolator_getK_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_EdgeAwareInterpolator_setFGSLambda_10(IntPtr nativeObj, float _lambda);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_EdgeAwareInterpolator_setFGSSigma_10(IntPtr nativeObj, float _sigma);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_EdgeAwareInterpolator_setK_10(IntPtr nativeObj, int _k);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_EdgeAwareInterpolator_setLambda_10(IntPtr nativeObj, float _lambda);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_EdgeAwareInterpolator_setSigma_10(IntPtr nativeObj, float _sigma);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_EdgeAwareInterpolator_setUsePostProcessing_10(IntPtr nativeObj, bool _use_post_proc);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_EdgeAwareInterpolator_delete(IntPtr nativeObj);
	}
}
