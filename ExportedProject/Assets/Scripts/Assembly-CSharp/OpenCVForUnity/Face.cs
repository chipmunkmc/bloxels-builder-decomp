using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Face
	{
		private const string LIBNAME = "opencvforunity";

		public static BasicFaceRecognizer createEigenFaceRecognizer(int num_components, double threshold)
		{
			return new BasicFaceRecognizer(face_Face_createEigenFaceRecognizer_10(num_components, threshold));
		}

		public static BasicFaceRecognizer createEigenFaceRecognizer()
		{
			return new BasicFaceRecognizer(face_Face_createEigenFaceRecognizer_11());
		}

		public static BasicFaceRecognizer createFisherFaceRecognizer(int num_components, double threshold)
		{
			return new BasicFaceRecognizer(face_Face_createFisherFaceRecognizer_10(num_components, threshold));
		}

		public static BasicFaceRecognizer createFisherFaceRecognizer()
		{
			return new BasicFaceRecognizer(face_Face_createFisherFaceRecognizer_11());
		}

		public static LBPHFaceRecognizer createLBPHFaceRecognizer(int radius, int neighbors, int grid_x, int grid_y, double threshold)
		{
			return new LBPHFaceRecognizer(face_Face_createLBPHFaceRecognizer_10(radius, neighbors, grid_x, grid_y, threshold));
		}

		public static LBPHFaceRecognizer createLBPHFaceRecognizer()
		{
			return new LBPHFaceRecognizer(face_Face_createLBPHFaceRecognizer_11());
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr face_Face_createEigenFaceRecognizer_10(int num_components, double threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_Face_createEigenFaceRecognizer_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr face_Face_createFisherFaceRecognizer_10(int num_components, double threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_Face_createFisherFaceRecognizer_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr face_Face_createLBPHFaceRecognizer_10(int radius, int neighbors, int grid_x, int grid_y, double threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_Face_createLBPHFaceRecognizer_11();
	}
}
