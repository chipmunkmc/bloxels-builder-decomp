using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class FaceRecognizer : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected FaceRecognizer(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						face_FaceRecognizer_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public string getLabelInfo(int label)
		{
			ThrowIfDisposed();
			return Marshal.PtrToStringAnsi(face_FaceRecognizer_getLabelInfo_10(nativeObj, label));
		}

		public int predict(Mat src)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			return face_FaceRecognizer_predict_10(nativeObj, src.nativeObj);
		}

		public MatOfInt getLabelsBystring(string str)
		{
			ThrowIfDisposed();
			return MatOfInt.fromNativeAddr(face_FaceRecognizer_getLabelsByString_10(nativeObj, str));
		}

		public void load(string filename)
		{
			ThrowIfDisposed();
			face_FaceRecognizer_load_10(nativeObj, filename);
		}

		public void predict(Mat src, int[] label, double[] confidence)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			double[] array = new double[1];
			double[] array2 = new double[1];
			face_FaceRecognizer_predict_11(nativeObj, src.nativeObj, array, array2);
			if (label != null)
			{
				label[0] = (int)array[0];
			}
			if (confidence != null)
			{
				confidence[0] = array2[0];
			}
		}

		public override void save(string filename)
		{
			ThrowIfDisposed();
			face_FaceRecognizer_save_10(nativeObj, filename);
		}

		public void setLabelInfo(int label, string strInfo)
		{
			ThrowIfDisposed();
			face_FaceRecognizer_setLabelInfo_10(nativeObj, label, strInfo);
		}

		public void train(List<Mat> src, Mat labels)
		{
			ThrowIfDisposed();
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			face_FaceRecognizer_train_10(nativeObj, mat.nativeObj, labels.nativeObj);
		}

		public void update(List<Mat> src, Mat labels)
		{
			ThrowIfDisposed();
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			face_FaceRecognizer_update_10(nativeObj, mat.nativeObj, labels.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr face_FaceRecognizer_getLabelInfo_10(IntPtr nativeObj, int label);

		[DllImport("opencvforunity")]
		private static extern int face_FaceRecognizer_predict_10(IntPtr nativeObj, IntPtr src_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_FaceRecognizer_getLabelsByString_10(IntPtr nativeObj, string str);

		[DllImport("opencvforunity")]
		private static extern void face_FaceRecognizer_load_10(IntPtr nativeObj, string filename);

		[DllImport("opencvforunity")]
		private static extern void face_FaceRecognizer_predict_11(IntPtr nativeObj, IntPtr src_nativeObj, double[] label_out, double[] confidence_out);

		[DllImport("opencvforunity")]
		private static extern void face_FaceRecognizer_save_10(IntPtr nativeObj, string filename);

		[DllImport("opencvforunity")]
		private static extern void face_FaceRecognizer_setLabelInfo_10(IntPtr nativeObj, int label, string strInfo);

		[DllImport("opencvforunity")]
		private static extern void face_FaceRecognizer_train_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr labels_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void face_FaceRecognizer_update_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr labels_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void face_FaceRecognizer_delete(IntPtr nativeObj);
	}
}
