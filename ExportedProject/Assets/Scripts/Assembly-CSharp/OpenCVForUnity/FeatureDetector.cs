using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class FeatureDetector : DisposableOpenCVObject
	{
		private const int GRIDDETECTOR = 1000;

		private const int PYRAMIDDETECTOR = 2000;

		private const int DYNAMICDETECTOR = 3000;

		public const int FAST = 1;

		public const int ORB = 5;

		public const int MSER = 6;

		public const int GFTT = 7;

		public const int HARRIS = 8;

		public const int SIMPLEBLOB = 9;

		public const int BRISK = 11;

		public const int AKAZE = 12;

		public const int GRID_FAST = 1001;

		public const int GRID_ORB = 1005;

		public const int GRID_MSER = 1006;

		public const int GRID_GFTT = 1007;

		public const int GRID_HARRIS = 1008;

		public const int GRID_SIMPLEBLOB = 1009;

		public const int GRID_BRISK = 1011;

		public const int GRID_AKAZE = 1012;

		public const int PYRAMID_FAST = 2001;

		public const int PYRAMID_ORB = 2005;

		public const int PYRAMID_MSER = 2006;

		public const int PYRAMID_GFTT = 2007;

		public const int PYRAMID_HARRIS = 2008;

		public const int PYRAMID_SIMPLEBLOB = 2009;

		public const int PYRAMID_BRISK = 2011;

		public const int PYRAMID_AKAZE = 2012;

		public const int DYNAMIC_FAST = 3001;

		public const int DYNAMIC_ORB = 3005;

		public const int DYNAMIC_MSER = 3006;

		public const int DYNAMIC_GFTT = 3007;

		public const int DYNAMIC_HARRIS = 3008;

		public const int DYNAMIC_SIMPLEBLOB = 3009;

		public const int DYNAMIC_BRISK = 3011;

		public const int DYNAMIC_AKAZE = 3012;

		private const string LIBNAME = "opencvforunity";

		protected FeatureDetector(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						features2d_FeatureDetector_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool empty()
		{
			ThrowIfDisposed();
			return features2d_FeatureDetector_empty_10(nativeObj);
		}

		public static FeatureDetector create(int detectorType)
		{
			return new FeatureDetector(features2d_FeatureDetector_create_10(detectorType));
		}

		public void detect(Mat image, MatOfKeyPoint keypoints, Mat mask)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (keypoints != null)
			{
				keypoints.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			features2d_FeatureDetector_detect_10(nativeObj, image.nativeObj, keypoints.nativeObj, mask.nativeObj);
		}

		public void detect(Mat image, MatOfKeyPoint keypoints)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (keypoints != null)
			{
				keypoints.ThrowIfDisposed();
			}
			features2d_FeatureDetector_detect_11(nativeObj, image.nativeObj, keypoints.nativeObj);
		}

		public void detect(List<Mat> images, List<MatOfKeyPoint> keypoints, List<Mat> masks)
		{
			ThrowIfDisposed();
			Mat mat = Converters.vector_Mat_to_Mat(images);
			Mat mat2 = new Mat();
			Mat mat3 = Converters.vector_Mat_to_Mat(masks);
			features2d_FeatureDetector_detect_12(nativeObj, mat.nativeObj, mat2.nativeObj, mat3.nativeObj);
			Converters.Mat_to_vector_vector_KeyPoint(mat2, keypoints);
			mat2.release();
		}

		public void detect(List<Mat> images, List<MatOfKeyPoint> keypoints)
		{
			ThrowIfDisposed();
			Mat mat = Converters.vector_Mat_to_Mat(images);
			Mat mat2 = new Mat();
			features2d_FeatureDetector_detect_13(nativeObj, mat.nativeObj, mat2.nativeObj);
			Converters.Mat_to_vector_vector_KeyPoint(mat2, keypoints);
			mat2.release();
		}

		public void read(string fileName)
		{
			ThrowIfDisposed();
			features2d_FeatureDetector_read_10(nativeObj, fileName);
		}

		public void write(string fileName)
		{
			ThrowIfDisposed();
			features2d_FeatureDetector_write_10(nativeObj, fileName);
		}

		[DllImport("opencvforunity")]
		private static extern bool features2d_FeatureDetector_empty_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr features2d_FeatureDetector_create_10(int detectorType);

		[DllImport("opencvforunity")]
		private static extern void features2d_FeatureDetector_detect_10(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr keypoints_mat_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_FeatureDetector_detect_11(IntPtr nativeObj, IntPtr image_nativeObj, IntPtr keypoints_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_FeatureDetector_detect_12(IntPtr nativeObj, IntPtr images_mat_nativeObj, IntPtr keypoints_mat_nativeObj, IntPtr masks_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_FeatureDetector_detect_13(IntPtr nativeObj, IntPtr images_mat_nativeObj, IntPtr keypoints_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_FeatureDetector_read_10(IntPtr nativeObj, string fileName);

		[DllImport("opencvforunity")]
		private static extern void features2d_FeatureDetector_write_10(IntPtr nativeObj, string fileName);

		[DllImport("opencvforunity")]
		private static extern void features2d_FeatureDetector_delete(IntPtr nativeObj);
	}
}
