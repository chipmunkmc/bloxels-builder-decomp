using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Features2d
	{
		public const int DRAW_OVER_OUTIMG = 1;

		public const int NOT_DRAW_SINGLE_POINTS = 2;

		public const int DRAW_RICH_KEYPOINTS = 4;

		private const string LIBNAME = "opencvforunity";

		public static void drawKeypoints(Mat image, MatOfKeyPoint keypoints, Mat outImage, Scalar color, int flags)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (keypoints != null)
			{
				keypoints.ThrowIfDisposed();
			}
			if (outImage != null)
			{
				outImage.ThrowIfDisposed();
			}
			features2d_Features2d_drawKeypoints_10(image.nativeObj, keypoints.nativeObj, outImage.nativeObj, color.val[0], color.val[1], color.val[2], color.val[3], flags);
		}

		public static void drawKeypoints(Mat image, MatOfKeyPoint keypoints, Mat outImage)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (keypoints != null)
			{
				keypoints.ThrowIfDisposed();
			}
			if (outImage != null)
			{
				outImage.ThrowIfDisposed();
			}
			features2d_Features2d_drawKeypoints_11(image.nativeObj, keypoints.nativeObj, outImage.nativeObj);
		}

		public static void drawMatches(Mat img1, MatOfKeyPoint keypoints1, Mat img2, MatOfKeyPoint keypoints2, MatOfDMatch matches1to2, Mat outImg, Scalar matchColor, Scalar singlePointColor, MatOfByte matchesMask, int flags)
		{
			if (img1 != null)
			{
				img1.ThrowIfDisposed();
			}
			if (keypoints1 != null)
			{
				keypoints1.ThrowIfDisposed();
			}
			if (img2 != null)
			{
				img2.ThrowIfDisposed();
			}
			if (keypoints2 != null)
			{
				keypoints2.ThrowIfDisposed();
			}
			if (matches1to2 != null)
			{
				matches1to2.ThrowIfDisposed();
			}
			if (outImg != null)
			{
				outImg.ThrowIfDisposed();
			}
			if (matchesMask != null)
			{
				matchesMask.ThrowIfDisposed();
			}
			features2d_Features2d_drawMatches_10(img1.nativeObj, keypoints1.nativeObj, img2.nativeObj, keypoints2.nativeObj, matches1to2.nativeObj, outImg.nativeObj, matchColor.val[0], matchColor.val[1], matchColor.val[2], matchColor.val[3], singlePointColor.val[0], singlePointColor.val[1], singlePointColor.val[2], singlePointColor.val[3], matchesMask.nativeObj, flags);
		}

		public static void drawMatches(Mat img1, MatOfKeyPoint keypoints1, Mat img2, MatOfKeyPoint keypoints2, MatOfDMatch matches1to2, Mat outImg)
		{
			if (img1 != null)
			{
				img1.ThrowIfDisposed();
			}
			if (keypoints1 != null)
			{
				keypoints1.ThrowIfDisposed();
			}
			if (img2 != null)
			{
				img2.ThrowIfDisposed();
			}
			if (keypoints2 != null)
			{
				keypoints2.ThrowIfDisposed();
			}
			if (matches1to2 != null)
			{
				matches1to2.ThrowIfDisposed();
			}
			if (outImg != null)
			{
				outImg.ThrowIfDisposed();
			}
			features2d_Features2d_drawMatches_11(img1.nativeObj, keypoints1.nativeObj, img2.nativeObj, keypoints2.nativeObj, matches1to2.nativeObj, outImg.nativeObj);
		}

		public static void drawMatches2(Mat img1, MatOfKeyPoint keypoints1, Mat img2, MatOfKeyPoint keypoints2, List<MatOfDMatch> matches1to2, Mat outImg, Scalar matchColor, Scalar singlePointColor, List<MatOfByte> matchesMask, int flags)
		{
			if (img1 != null)
			{
				img1.ThrowIfDisposed();
			}
			if (keypoints1 != null)
			{
				keypoints1.ThrowIfDisposed();
			}
			if (img2 != null)
			{
				img2.ThrowIfDisposed();
			}
			if (keypoints2 != null)
			{
				keypoints2.ThrowIfDisposed();
			}
			if (outImg != null)
			{
				outImg.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((matches1to2 != null) ? matches1to2.Count : 0);
			Mat mat = Converters.vector_vector_DMatch_to_Mat(matches1to2, mats);
			List<Mat> mats2 = new List<Mat>((matchesMask != null) ? matchesMask.Count : 0);
			Mat mat2 = Converters.vector_vector_char_to_Mat(matchesMask, mats2);
			features2d_Features2d_drawMatches2_10(img1.nativeObj, keypoints1.nativeObj, img2.nativeObj, keypoints2.nativeObj, mat.nativeObj, outImg.nativeObj, matchColor.val[0], matchColor.val[1], matchColor.val[2], matchColor.val[3], singlePointColor.val[0], singlePointColor.val[1], singlePointColor.val[2], singlePointColor.val[3], mat2.nativeObj, flags);
		}

		public static void drawMatches2(Mat img1, MatOfKeyPoint keypoints1, Mat img2, MatOfKeyPoint keypoints2, List<MatOfDMatch> matches1to2, Mat outImg)
		{
			if (img1 != null)
			{
				img1.ThrowIfDisposed();
			}
			if (keypoints1 != null)
			{
				keypoints1.ThrowIfDisposed();
			}
			if (img2 != null)
			{
				img2.ThrowIfDisposed();
			}
			if (keypoints2 != null)
			{
				keypoints2.ThrowIfDisposed();
			}
			if (outImg != null)
			{
				outImg.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((matches1to2 != null) ? matches1to2.Count : 0);
			Mat mat = Converters.vector_vector_DMatch_to_Mat(matches1to2, mats);
			features2d_Features2d_drawMatches2_11(img1.nativeObj, keypoints1.nativeObj, img2.nativeObj, keypoints2.nativeObj, mat.nativeObj, outImg.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void features2d_Features2d_drawKeypoints_10(IntPtr image_nativeObj, IntPtr keypoints_mat_nativeObj, IntPtr outImage_nativeObj, double color_val0, double color_val1, double color_val2, double color_val3, int flags);

		[DllImport("opencvforunity")]
		private static extern void features2d_Features2d_drawKeypoints_11(IntPtr image_nativeObj, IntPtr keypoints_mat_nativeObj, IntPtr outImage_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_Features2d_drawMatches_10(IntPtr img1_nativeObj, IntPtr keypoints1_mat_nativeObj, IntPtr img2_nativeObj, IntPtr keypoints2_mat_nativeObj, IntPtr matches1to2_mat_nativeObj, IntPtr outImg_nativeObj, double matchColor_val0, double matchColor_val1, double matchColor_val2, double matchColor_val3, double singlePointColor_val0, double singlePointColor_val1, double singlePointColor_val2, double singlePointColor_val3, IntPtr matchesMask_mat_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void features2d_Features2d_drawMatches_11(IntPtr img1_nativeObj, IntPtr keypoints1_mat_nativeObj, IntPtr img2_nativeObj, IntPtr keypoints2_mat_nativeObj, IntPtr matches1to2_mat_nativeObj, IntPtr outImg_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void features2d_Features2d_drawMatches2_10(IntPtr img1_nativeObj, IntPtr keypoints1_mat_nativeObj, IntPtr img2_nativeObj, IntPtr keypoints2_mat_nativeObj, IntPtr matches1to2_mat_nativeObj, IntPtr outImg_nativeObj, double matchColor_val0, double matchColor_val1, double matchColor_val2, double matchColor_val3, double singlePointColor_val0, double singlePointColor_val1, double singlePointColor_val2, double singlePointColor_val3, IntPtr matchesMask_mat_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void features2d_Features2d_drawMatches2_11(IntPtr img1_nativeObj, IntPtr keypoints1_mat_nativeObj, IntPtr img2_nativeObj, IntPtr keypoints2_mat_nativeObj, IntPtr matches1to2_mat_nativeObj, IntPtr outImg_nativeObj);
	}
}
