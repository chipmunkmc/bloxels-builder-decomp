using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class GraphSegmentation : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public GraphSegmentation(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_GraphSegmentation_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public double getSigma()
		{
			ThrowIfDisposed();
			return ximgproc_GraphSegmentation_getSigma_10(nativeObj);
		}

		public float getK()
		{
			ThrowIfDisposed();
			return ximgproc_GraphSegmentation_getK_10(nativeObj);
		}

		public int getMinSize()
		{
			ThrowIfDisposed();
			return ximgproc_GraphSegmentation_getMinSize_10(nativeObj);
		}

		public void processImage(Mat src, Mat dst)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_GraphSegmentation_processImage_10(nativeObj, src.nativeObj, dst.nativeObj);
		}

		public void setK(float k)
		{
			ThrowIfDisposed();
			ximgproc_GraphSegmentation_setK_10(nativeObj, k);
		}

		public void setMinSize(int min_size)
		{
			ThrowIfDisposed();
			ximgproc_GraphSegmentation_setMinSize_10(nativeObj, min_size);
		}

		public void setSigma(double sigma)
		{
			ThrowIfDisposed();
			ximgproc_GraphSegmentation_setSigma_10(nativeObj, sigma);
		}

		[DllImport("opencvforunity")]
		private static extern double ximgproc_GraphSegmentation_getSigma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ximgproc_GraphSegmentation_getK_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ximgproc_GraphSegmentation_getMinSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_GraphSegmentation_processImage_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_GraphSegmentation_setK_10(IntPtr nativeObj, float k);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_GraphSegmentation_setMinSize_10(IntPtr nativeObj, int min_size);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_GraphSegmentation_setSigma_10(IntPtr nativeObj, double sigma);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_GraphSegmentation_delete(IntPtr nativeObj);
	}
}
