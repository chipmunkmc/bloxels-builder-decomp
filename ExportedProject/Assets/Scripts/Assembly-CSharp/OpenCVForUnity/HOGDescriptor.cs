using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class HOGDescriptor : DisposableOpenCVObject
	{
		public const int L2Hys = 0;

		public const int DEFAULT_NLEVELS = 64;

		private const string LIBNAME = "opencvforunity";

		protected HOGDescriptor(IntPtr addr)
			: base(addr)
		{
		}

		public HOGDescriptor(Size _winSize, Size _blockSize, Size _blockStride, Size _cellSize, int _nbins, int _derivAperture, double _winSigma, int _histogramNormType, double _L2HysThreshold, bool _gammaCorrection, int _nlevels, bool _signedGradient)
		{
			nativeObj = objdetect_HOGDescriptor_HOGDescriptor_10(_winSize.width, _winSize.height, _blockSize.width, _blockSize.height, _blockStride.width, _blockStride.height, _cellSize.width, _cellSize.height, _nbins, _derivAperture, _winSigma, _histogramNormType, _L2HysThreshold, _gammaCorrection, _nlevels, _signedGradient);
		}

		public HOGDescriptor(Size _winSize, Size _blockSize, Size _blockStride, Size _cellSize, int _nbins)
		{
			nativeObj = objdetect_HOGDescriptor_HOGDescriptor_11(_winSize.width, _winSize.height, _blockSize.width, _blockSize.height, _blockStride.width, _blockStride.height, _cellSize.width, _cellSize.height, _nbins);
		}

		public HOGDescriptor(string filename)
		{
			nativeObj = objdetect_HOGDescriptor_HOGDescriptor_12(filename);
		}

		public HOGDescriptor()
		{
			nativeObj = objdetect_HOGDescriptor_HOGDescriptor_13();
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						objdetect_HOGDescriptor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool checkDetectorSize()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_checkDetectorSize_10(nativeObj);
		}

		public bool load(string filename, string objname)
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_load_10(nativeObj, filename, objname);
		}

		public bool load(string filename)
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_load_11(nativeObj, filename);
		}

		public double getWinSigma()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_getWinSigma_10(nativeObj);
		}

		public long getDescriptorSize()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_getDescriptorSize_10(nativeObj);
		}

		public static MatOfFloat getDaimlerPeopleDetector()
		{
			return MatOfFloat.fromNativeAddr(objdetect_HOGDescriptor_getDaimlerPeopleDetector_10());
		}

		public static MatOfFloat getDefaultPeopleDetector()
		{
			return MatOfFloat.fromNativeAddr(objdetect_HOGDescriptor_getDefaultPeopleDetector_10());
		}

		public void compute(Mat img, MatOfFloat descriptors, Size winStride, Size padding, MatOfPoint locations)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (descriptors != null)
			{
				descriptors.ThrowIfDisposed();
			}
			if (locations != null)
			{
				locations.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_compute_10(nativeObj, img.nativeObj, descriptors.nativeObj, winStride.width, winStride.height, padding.width, padding.height, locations.nativeObj);
		}

		public void compute(Mat img, MatOfFloat descriptors)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (descriptors != null)
			{
				descriptors.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_compute_11(nativeObj, img.nativeObj, descriptors.nativeObj);
		}

		public void computeGradient(Mat img, Mat grad, Mat angleOfs, Size paddingTL, Size paddingBR)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (grad != null)
			{
				grad.ThrowIfDisposed();
			}
			if (angleOfs != null)
			{
				angleOfs.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_computeGradient_10(nativeObj, img.nativeObj, grad.nativeObj, angleOfs.nativeObj, paddingTL.width, paddingTL.height, paddingBR.width, paddingBR.height);
		}

		public void computeGradient(Mat img, Mat grad, Mat angleOfs)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (grad != null)
			{
				grad.ThrowIfDisposed();
			}
			if (angleOfs != null)
			{
				angleOfs.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_computeGradient_11(nativeObj, img.nativeObj, grad.nativeObj, angleOfs.nativeObj);
		}

		public void detect(Mat img, MatOfPoint foundLocations, MatOfDouble weights, double hitThreshold, Size winStride, Size padding, MatOfPoint searchLocations)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (foundLocations != null)
			{
				foundLocations.ThrowIfDisposed();
			}
			if (weights != null)
			{
				weights.ThrowIfDisposed();
			}
			if (searchLocations != null)
			{
				searchLocations.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_detect_10(nativeObj, img.nativeObj, foundLocations.nativeObj, weights.nativeObj, hitThreshold, winStride.width, winStride.height, padding.width, padding.height, searchLocations.nativeObj);
		}

		public void detect(Mat img, MatOfPoint foundLocations, MatOfDouble weights)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (foundLocations != null)
			{
				foundLocations.ThrowIfDisposed();
			}
			if (weights != null)
			{
				weights.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_detect_11(nativeObj, img.nativeObj, foundLocations.nativeObj, weights.nativeObj);
		}

		public void detectMultiScale(Mat img, MatOfRect foundLocations, MatOfDouble foundWeights, double hitThreshold, Size winStride, Size padding, double scale, double finalThreshold, bool useMeanshiftGrouping)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (foundLocations != null)
			{
				foundLocations.ThrowIfDisposed();
			}
			if (foundWeights != null)
			{
				foundWeights.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_detectMultiScale_10(nativeObj, img.nativeObj, foundLocations.nativeObj, foundWeights.nativeObj, hitThreshold, winStride.width, winStride.height, padding.width, padding.height, scale, finalThreshold, useMeanshiftGrouping);
		}

		public void detectMultiScale(Mat img, MatOfRect foundLocations, MatOfDouble foundWeights)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (foundLocations != null)
			{
				foundLocations.ThrowIfDisposed();
			}
			if (foundWeights != null)
			{
				foundWeights.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_detectMultiScale_11(nativeObj, img.nativeObj, foundLocations.nativeObj, foundWeights.nativeObj);
		}

		public void save(string filename, string objname)
		{
			ThrowIfDisposed();
			objdetect_HOGDescriptor_save_10(nativeObj, filename, objname);
		}

		public void save(string filename)
		{
			ThrowIfDisposed();
			objdetect_HOGDescriptor_save_11(nativeObj, filename);
		}

		public void setSVMDetector(Mat _svmdetector)
		{
			ThrowIfDisposed();
			if (_svmdetector != null)
			{
				_svmdetector.ThrowIfDisposed();
			}
			objdetect_HOGDescriptor_setSVMDetector_10(nativeObj, _svmdetector.nativeObj);
		}

		public Size get_winSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			objdetect_HOGDescriptor_get_1winSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public Size get_blockSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			objdetect_HOGDescriptor_get_1blockSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public Size get_blockStride()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			objdetect_HOGDescriptor_get_1blockStride_10(nativeObj, vals);
			return new Size(vals);
		}

		public Size get_cellSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			objdetect_HOGDescriptor_get_1cellSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public int get_nbins()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1nbins_10(nativeObj);
		}

		public int get_derivAperture()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1derivAperture_10(nativeObj);
		}

		public double get_winSigma()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1winSigma_10(nativeObj);
		}

		public int get_histogramNormType()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1histogramNormType_10(nativeObj);
		}

		public double get_L2HysThreshold()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1L2HysThreshold_10(nativeObj);
		}

		public bool get_gammaCorrection()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1gammaCorrection_10(nativeObj);
		}

		public MatOfFloat get_svmDetector()
		{
			ThrowIfDisposed();
			return MatOfFloat.fromNativeAddr(objdetect_HOGDescriptor_get_1svmDetector_10(nativeObj));
		}

		public int get_nlevels()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1nlevels_10(nativeObj);
		}

		public bool get_signedGradient()
		{
			ThrowIfDisposed();
			return objdetect_HOGDescriptor_get_1signedGradient_10(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_HOGDescriptor_HOGDescriptor_10(double _winSize_width, double _winSize_height, double _blockSize_width, double _blockSize_height, double _blockStride_width, double _blockStride_height, double _cellSize_width, double _cellSize_height, int _nbins, int _derivAperture, double _winSigma, int _histogramNormType, double _L2HysThreshold, bool _gammaCorrection, int _nlevels, bool _signedGradient);

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_HOGDescriptor_HOGDescriptor_11(double _winSize_width, double _winSize_height, double _blockSize_width, double _blockSize_height, double _blockStride_width, double _blockStride_height, double _cellSize_width, double _cellSize_height, int _nbins);

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_HOGDescriptor_HOGDescriptor_12(string filename);

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_HOGDescriptor_HOGDescriptor_13();

		[DllImport("opencvforunity")]
		private static extern bool objdetect_HOGDescriptor_checkDetectorSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_HOGDescriptor_load_10(IntPtr nativeObj, string filename, string objname);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_HOGDescriptor_load_11(IntPtr nativeObj, string filename);

		[DllImport("opencvforunity")]
		private static extern double objdetect_HOGDescriptor_getWinSigma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern long objdetect_HOGDescriptor_getDescriptorSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_HOGDescriptor_getDaimlerPeopleDetector_10();

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_HOGDescriptor_getDefaultPeopleDetector_10();

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_compute_10(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr descriptors_mat_nativeObj, double winStride_width, double winStride_height, double padding_width, double padding_height, IntPtr locations_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_compute_11(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr descriptors_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_computeGradient_10(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr grad_nativeObj, IntPtr angleOfs_nativeObj, double paddingTL_width, double paddingTL_height, double paddingBR_width, double paddingBR_height);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_computeGradient_11(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr grad_nativeObj, IntPtr angleOfs_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_detect_10(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr foundLocations_mat_nativeObj, IntPtr weights_mat_nativeObj, double hitThreshold, double winStride_width, double winStride_height, double padding_width, double padding_height, IntPtr searchLocations_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_detect_11(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr foundLocations_mat_nativeObj, IntPtr weights_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_detectMultiScale_10(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr foundLocations_mat_nativeObj, IntPtr foundWeights_mat_nativeObj, double hitThreshold, double winStride_width, double winStride_height, double padding_width, double padding_height, double scale, double finalThreshold, bool useMeanshiftGrouping);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_detectMultiScale_11(IntPtr nativeObj, IntPtr img_nativeObj, IntPtr foundLocations_mat_nativeObj, IntPtr foundWeights_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_save_10(IntPtr nativeObj, string filename, string objname);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_save_11(IntPtr nativeObj, string filename);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_setSVMDetector_10(IntPtr nativeObj, IntPtr _svmdetector_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_get_1winSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_get_1blockSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_get_1blockStride_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_get_1cellSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern int objdetect_HOGDescriptor_get_1nbins_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int objdetect_HOGDescriptor_get_1derivAperture_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double objdetect_HOGDescriptor_get_1winSigma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int objdetect_HOGDescriptor_get_1histogramNormType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double objdetect_HOGDescriptor_get_1L2HysThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_HOGDescriptor_get_1gammaCorrection_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr objdetect_HOGDescriptor_get_1svmDetector_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int objdetect_HOGDescriptor_get_1nlevels_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool objdetect_HOGDescriptor_get_1signedGradient_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void objdetect_HOGDescriptor_delete(IntPtr nativeObj);
	}
}
