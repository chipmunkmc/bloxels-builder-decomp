using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class HausdorffDistanceExtractor : ShapeDistanceExtractor
	{
		private const string LIBNAME = "opencvforunity";

		public HausdorffDistanceExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_HausdorffDistanceExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getRankProportion()
		{
			ThrowIfDisposed();
			return shape_HausdorffDistanceExtractor_getRankProportion_10(nativeObj);
		}

		public int getDistanceFlag()
		{
			ThrowIfDisposed();
			return shape_HausdorffDistanceExtractor_getDistanceFlag_10(nativeObj);
		}

		public void setDistanceFlag(int distanceFlag)
		{
			ThrowIfDisposed();
			shape_HausdorffDistanceExtractor_setDistanceFlag_10(nativeObj, distanceFlag);
		}

		public void setRankProportion(float rankProportion)
		{
			ThrowIfDisposed();
			shape_HausdorffDistanceExtractor_setRankProportion_10(nativeObj, rankProportion);
		}

		[DllImport("opencvforunity")]
		private static extern float shape_HausdorffDistanceExtractor_getRankProportion_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int shape_HausdorffDistanceExtractor_getDistanceFlag_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_HausdorffDistanceExtractor_setDistanceFlag_10(IntPtr nativeObj, int distanceFlag);

		[DllImport("opencvforunity")]
		private static extern void shape_HausdorffDistanceExtractor_setRankProportion_10(IntPtr nativeObj, float rankProportion);

		[DllImport("opencvforunity")]
		private static extern void shape_HausdorffDistanceExtractor_delete(IntPtr nativeObj);
	}
}
