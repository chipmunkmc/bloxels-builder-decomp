using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class HistogramCostExtractor : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public HistogramCostExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_HistogramCostExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getDefaultCost()
		{
			ThrowIfDisposed();
			return shape_HistogramCostExtractor_getDefaultCost_10(nativeObj);
		}

		public int getNDummies()
		{
			ThrowIfDisposed();
			return shape_HistogramCostExtractor_getNDummies_10(nativeObj);
		}

		public void buildCostMatrix(Mat descriptors1, Mat descriptors2, Mat costMatrix)
		{
			ThrowIfDisposed();
			if (descriptors1 != null)
			{
				descriptors1.ThrowIfDisposed();
			}
			if (descriptors2 != null)
			{
				descriptors2.ThrowIfDisposed();
			}
			if (costMatrix != null)
			{
				costMatrix.ThrowIfDisposed();
			}
			shape_HistogramCostExtractor_buildCostMatrix_10(nativeObj, descriptors1.nativeObj, descriptors2.nativeObj, costMatrix.nativeObj);
		}

		public void setDefaultCost(float defaultCost)
		{
			ThrowIfDisposed();
			shape_HistogramCostExtractor_setDefaultCost_10(nativeObj, defaultCost);
		}

		public void setNDummies(int nDummies)
		{
			ThrowIfDisposed();
			shape_HistogramCostExtractor_setNDummies_10(nativeObj, nDummies);
		}

		[DllImport("opencvforunity")]
		private static extern float shape_HistogramCostExtractor_getDefaultCost_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int shape_HistogramCostExtractor_getNDummies_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_HistogramCostExtractor_buildCostMatrix_10(IntPtr nativeObj, IntPtr descriptors1_nativeObj, IntPtr descriptors2_nativeObj, IntPtr costMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_HistogramCostExtractor_setDefaultCost_10(IntPtr nativeObj, float defaultCost);

		[DllImport("opencvforunity")]
		private static extern void shape_HistogramCostExtractor_setNDummies_10(IntPtr nativeObj, int nDummies);

		[DllImport("opencvforunity")]
		private static extern void shape_HistogramCostExtractor_delete(IntPtr nativeObj);
	}
}
