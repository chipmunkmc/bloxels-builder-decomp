using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Imgcodecs
	{
		public const int CV_LOAD_IMAGE_UNCHANGED = -1;

		public const int CV_LOAD_IMAGE_GRAYSCALE = 0;

		public const int CV_LOAD_IMAGE_COLOR = 1;

		public const int CV_LOAD_IMAGE_ANYDEPTH = 2;

		public const int CV_LOAD_IMAGE_ANYCOLOR = 4;

		public const int CV_IMWRITE_JPEG_QUALITY = 1;

		public const int CV_IMWRITE_JPEG_PROGRESSIVE = 2;

		public const int CV_IMWRITE_JPEG_OPTIMIZE = 3;

		public const int CV_IMWRITE_JPEG_RST_INTERVAL = 4;

		public const int CV_IMWRITE_JPEG_LUMA_QUALITY = 5;

		public const int CV_IMWRITE_JPEG_CHROMA_QUALITY = 6;

		public const int CV_IMWRITE_PNG_COMPRESSION = 16;

		public const int CV_IMWRITE_PNG_STRATEGY = 17;

		public const int CV_IMWRITE_PNG_BILEVEL = 18;

		public const int CV_IMWRITE_PNG_STRATEGY_DEFAULT = 0;

		public const int CV_IMWRITE_PNG_STRATEGY_FILTERED = 1;

		public const int CV_IMWRITE_PNG_STRATEGY_HUFFMAN_ONLY = 2;

		public const int CV_IMWRITE_PNG_STRATEGY_RLE = 3;

		public const int CV_IMWRITE_PNG_STRATEGY_FIXED = 4;

		public const int CV_IMWRITE_PXM_BINARY = 32;

		public const int CV_IMWRITE_WEBP_QUALITY = 64;

		public const int CV_CVTIMG_FLIP = 1;

		public const int CV_CVTIMG_SWAP_RB = 2;

		public const int IMREAD_UNCHANGED = -1;

		public const int IMREAD_GRAYSCALE = 0;

		public const int IMREAD_COLOR = 1;

		public const int IMREAD_ANYDEPTH = 2;

		public const int IMREAD_ANYCOLOR = 4;

		public const int IMREAD_LOAD_GDAL = 8;

		public const int IMREAD_REDUCED_GRAYSCALE_2 = 16;

		public const int IMREAD_REDUCED_COLOR_2 = 17;

		public const int IMREAD_REDUCED_GRAYSCALE_4 = 32;

		public const int IMREAD_REDUCED_COLOR_4 = 33;

		public const int IMREAD_REDUCED_GRAYSCALE_8 = 64;

		public const int IMREAD_REDUCED_COLOR_8 = 65;

		public const int IMWRITE_JPEG_QUALITY = 1;

		public const int IMWRITE_JPEG_PROGRESSIVE = 2;

		public const int IMWRITE_JPEG_OPTIMIZE = 3;

		public const int IMWRITE_JPEG_RST_INTERVAL = 4;

		public const int IMWRITE_JPEG_LUMA_QUALITY = 5;

		public const int IMWRITE_JPEG_CHROMA_QUALITY = 6;

		public const int IMWRITE_PNG_COMPRESSION = 16;

		public const int IMWRITE_PNG_STRATEGY = 17;

		public const int IMWRITE_PNG_BILEVEL = 18;

		public const int IMWRITE_PXM_BINARY = 32;

		public const int IMWRITE_WEBP_QUALITY = 64;

		public const int IMWRITE_PNG_STRATEGY_DEFAULT = 0;

		public const int IMWRITE_PNG_STRATEGY_FILTERED = 1;

		public const int IMWRITE_PNG_STRATEGY_HUFFMAN_ONLY = 2;

		public const int IMWRITE_PNG_STRATEGY_RLE = 3;

		public const int IMWRITE_PNG_STRATEGY_FIXED = 4;

		private const string LIBNAME = "opencvforunity";

		public static Mat imdecode(Mat buf, int flags)
		{
			if (buf != null)
			{
				buf.ThrowIfDisposed();
			}
			return new Mat(imgcodecs_Imgcodecs_imdecode_10(buf.nativeObj, flags));
		}

		public static Mat imread(string filename, int flags)
		{
			return new Mat(imgcodecs_Imgcodecs_imread_10(filename, flags));
		}

		public static Mat imread(string filename)
		{
			return new Mat(imgcodecs_Imgcodecs_imread_11(filename));
		}

		public static bool imencode(string ext, Mat img, MatOfByte buf, MatOfInt _params)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (buf != null)
			{
				buf.ThrowIfDisposed();
			}
			if (_params != null)
			{
				_params.ThrowIfDisposed();
			}
			return imgcodecs_Imgcodecs_imencode_10(ext, img.nativeObj, buf.nativeObj, _params.nativeObj);
		}

		public static bool imencode(string ext, Mat img, MatOfByte buf)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (buf != null)
			{
				buf.ThrowIfDisposed();
			}
			return imgcodecs_Imgcodecs_imencode_11(ext, img.nativeObj, buf.nativeObj);
		}

		public static bool imreadmulti(string filename, List<Mat> mats, int flags)
		{
			Mat mat = Converters.vector_Mat_to_Mat(mats);
			return imgcodecs_Imgcodecs_imreadmulti_10(filename, mat.nativeObj, flags);
		}

		public static bool imreadmulti(string filename, List<Mat> mats)
		{
			Mat mat = Converters.vector_Mat_to_Mat(mats);
			return imgcodecs_Imgcodecs_imreadmulti_11(filename, mat.nativeObj);
		}

		public static bool imwrite(string filename, Mat img, MatOfInt _params)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (_params != null)
			{
				_params.ThrowIfDisposed();
			}
			return imgcodecs_Imgcodecs_imwrite_10(filename, img.nativeObj, _params.nativeObj);
		}

		public static bool imwrite(string filename, Mat img)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			return imgcodecs_Imgcodecs_imwrite_11(filename, img.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr imgcodecs_Imgcodecs_imdecode_10(IntPtr buf_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgcodecs_Imgcodecs_imread_10(string filename, int flags);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgcodecs_Imgcodecs_imread_11(string filename);

		[DllImport("opencvforunity")]
		private static extern bool imgcodecs_Imgcodecs_imencode_10(string ext, IntPtr img_nativeObj, IntPtr buf_mat_nativeObj, IntPtr params_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool imgcodecs_Imgcodecs_imencode_11(string ext, IntPtr img_nativeObj, IntPtr buf_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool imgcodecs_Imgcodecs_imreadmulti_10(string filename, IntPtr mats_mat_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern bool imgcodecs_Imgcodecs_imreadmulti_11(string filename, IntPtr mats_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool imgcodecs_Imgcodecs_imwrite_10(string filename, IntPtr img_nativeObj, IntPtr params_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool imgcodecs_Imgcodecs_imwrite_11(string filename, IntPtr img_nativeObj);
	}
}
