using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Imgproc
	{
		private const int IPL_BORDER_CONSTANT = 0;

		private const int IPL_BORDER_REPLICATE = 1;

		private const int IPL_BORDER_REFLECT = 2;

		private const int IPL_BORDER_WRAP = 3;

		private const int IPL_BORDER_REFLECT_101 = 4;

		private const int IPL_BORDER_TRANSPARENT = 5;

		private const int CV_INTER_NN = 0;

		private const int CV_INTER_LINEAR = 1;

		private const int CV_INTER_CUBIC = 2;

		private const int CV_INTER_AREA = 3;

		private const int CV_INTER_LANCZOS4 = 4;

		private const int CV_MOP_ERODE = 0;

		private const int CV_MOP_DILATE = 1;

		private const int CV_MOP_OPEN = 2;

		private const int CV_MOP_CLOSE = 3;

		private const int CV_MOP_GRADIENT = 4;

		private const int CV_MOP_TOPHAT = 5;

		private const int CV_MOP_BLACKHAT = 6;

		private const int CV_RETR_EXTERNAL = 0;

		private const int CV_RETR_LIST = 1;

		private const int CV_RETR_CCOMP = 2;

		private const int CV_RETR_TREE = 3;

		private const int CV_RETR_FLOODFILL = 4;

		private const int CV_CHAIN_APPROX_NONE = 1;

		private const int CV_CHAIN_APPROX_SIMPLE = 2;

		private const int CV_CHAIN_APPROX_TC89_L1 = 3;

		private const int CV_CHAIN_APPROX_TC89_KCOS = 4;

		private const int CV_THRESH_BINARY = 0;

		private const int CV_THRESH_BINARY_INV = 1;

		private const int CV_THRESH_TRUNC = 2;

		private const int CV_THRESH_TOZERO = 3;

		private const int CV_THRESH_TOZERO_INV = 4;

		private const int CV_THRESH_MASK = 7;

		private const int CV_THRESH_OTSU = 8;

		private const int CV_THRESH_TRIANGLE = 16;

		public const int LINE_AA = 16;

		public const int LINE_8 = 8;

		public const int LINE_4 = 4;

		public const int CV_BLUR_NO_SCALE = 0;

		public const int CV_BLUR = 1;

		public const int CV_GAUSSIAN = 2;

		public const int CV_MEDIAN = 3;

		public const int CV_BILATERAL = 4;

		public const int CV_GAUSSIAN_5x5 = 7;

		public const int CV_SCHARR = -1;

		public const int CV_MAX_SOBEL_KSIZE = 7;

		public const int CV_RGBA2mRGBA = 125;

		public const int CV_mRGBA2RGBA = 126;

		public const int CV_WARP_FILL_OUTLIERS = 8;

		public const int CV_WARP_INVERSE_MAP = 16;

		public const int CV_SHAPE_RECT = 0;

		public const int CV_SHAPE_CROSS = 1;

		public const int CV_SHAPE_ELLIPSE = 2;

		public const int CV_SHAPE_CUSTOM = 100;

		public const int CV_CHAIN_CODE = 0;

		public const int CV_LINK_RUNS = 5;

		public const int CV_POLY_APPROX_DP = 0;

		public const int CV_CONTOURS_MATCH_I1 = 1;

		public const int CV_CONTOURS_MATCH_I2 = 2;

		public const int CV_CONTOURS_MATCH_I3 = 3;

		public const int CV_CLOCKWISE = 1;

		public const int CV_COUNTER_CLOCKWISE = 2;

		public const int CV_COMP_CORREL = 0;

		public const int CV_COMP_CHISQR = 1;

		public const int CV_COMP_INTERSECT = 2;

		public const int CV_COMP_BHATTACHARYYA = 3;

		public const int CV_COMP_HELLINGER = 3;

		public const int CV_COMP_CHISQR_ALT = 4;

		public const int CV_COMP_KL_DIV = 5;

		public const int CV_DIST_MASK_3 = 3;

		public const int CV_DIST_MASK_5 = 5;

		public const int CV_DIST_MASK_PRECISE = 0;

		public const int CV_DIST_LABEL_CCOMP = 0;

		public const int CV_DIST_LABEL_PIXEL = 1;

		public const int CV_DIST_USER = -1;

		public const int CV_DIST_L1 = 1;

		public const int CV_DIST_L2 = 2;

		public const int CV_DIST_C = 3;

		public const int CV_DIST_L12 = 4;

		public const int CV_DIST_FAIR = 5;

		public const int CV_DIST_WELSCH = 6;

		public const int CV_DIST_HUBER = 7;

		public const int CV_CANNY_L2_GRADIENT = int.MinValue;

		public const int CV_HOUGH_STANDARD = 0;

		public const int CV_HOUGH_PROBABILISTIC = 1;

		public const int CV_HOUGH_MULTI_SCALE = 2;

		public const int CV_HOUGH_GRADIENT = 3;

		public const int MORPH_ERODE = 0;

		public const int MORPH_DILATE = 1;

		public const int MORPH_OPEN = 2;

		public const int MORPH_CLOSE = 3;

		public const int MORPH_GRADIENT = 4;

		public const int MORPH_TOPHAT = 5;

		public const int MORPH_BLACKHAT = 6;

		public const int MORPH_HITMISS = 7;

		public const int MORPH_RECT = 0;

		public const int MORPH_CROSS = 1;

		public const int MORPH_ELLIPSE = 2;

		public const int INTER_NEAREST = 0;

		public const int INTER_LINEAR = 1;

		public const int INTER_CUBIC = 2;

		public const int INTER_AREA = 3;

		public const int INTER_LANCZOS4 = 4;

		public const int INTER_MAX = 7;

		public const int WARP_FILL_OUTLIERS = 8;

		public const int WARP_INVERSE_MAP = 16;

		public const int INTER_BITS = 5;

		public const int INTER_BITS2 = 10;

		public const int INTER_TAB_SIZE = 32;

		public const int INTER_TAB_SIZE2 = 1024;

		public const int DIST_USER = -1;

		public const int DIST_L1 = 1;

		public const int DIST_L2 = 2;

		public const int DIST_C = 3;

		public const int DIST_L12 = 4;

		public const int DIST_FAIR = 5;

		public const int DIST_WELSCH = 6;

		public const int DIST_HUBER = 7;

		public const int DIST_MASK_3 = 3;

		public const int DIST_MASK_5 = 5;

		public const int DIST_MASK_PRECISE = 0;

		public const int THRESH_BINARY = 0;

		public const int THRESH_BINARY_INV = 1;

		public const int THRESH_TRUNC = 2;

		public const int THRESH_TOZERO = 3;

		public const int THRESH_TOZERO_INV = 4;

		public const int THRESH_MASK = 7;

		public const int THRESH_OTSU = 8;

		public const int THRESH_TRIANGLE = 16;

		public const int ADAPTIVE_THRESH_MEAN_C = 0;

		public const int ADAPTIVE_THRESH_GAUSSIAN_C = 1;

		public const int PROJ_SPHERICAL_ORTHO = 0;

		public const int PROJ_SPHERICAL_EQRECT = 1;

		public const int GC_BGD = 0;

		public const int GC_FGD = 1;

		public const int GC_PR_BGD = 2;

		public const int GC_PR_FGD = 3;

		public const int GC_INIT_WITH_RECT = 0;

		public const int GC_INIT_WITH_MASK = 1;

		public const int GC_EVAL = 2;

		public const int DIST_LABEL_CCOMP = 0;

		public const int DIST_LABEL_PIXEL = 1;

		public const int FLOODFILL_FIXED_RANGE = 65536;

		public const int FLOODFILL_MASK_ONLY = 131072;

		public const int CC_STAT_LEFT = 0;

		public const int CC_STAT_TOP = 1;

		public const int CC_STAT_WIDTH = 2;

		public const int CC_STAT_HEIGHT = 3;

		public const int CC_STAT_AREA = 4;

		public const int CC_STAT_MAX = 5;

		public const int RETR_EXTERNAL = 0;

		public const int RETR_LIST = 1;

		public const int RETR_CCOMP = 2;

		public const int RETR_TREE = 3;

		public const int RETR_FLOODFILL = 4;

		public const int CHAIN_APPROX_NONE = 1;

		public const int CHAIN_APPROX_SIMPLE = 2;

		public const int CHAIN_APPROX_TC89_L1 = 3;

		public const int CHAIN_APPROX_TC89_KCOS = 4;

		public const int HOUGH_STANDARD = 0;

		public const int HOUGH_PROBABILISTIC = 1;

		public const int HOUGH_MULTI_SCALE = 2;

		public const int HOUGH_GRADIENT = 3;

		public const int LSD_REFINE_NONE = 0;

		public const int LSD_REFINE_STD = 1;

		public const int LSD_REFINE_ADV = 2;

		public const int HISTCMP_CORREL = 0;

		public const int HISTCMP_CHISQR = 1;

		public const int HISTCMP_INTERSECT = 2;

		public const int HISTCMP_BHATTACHARYYA = 3;

		public const int HISTCMP_HELLINGER = 3;

		public const int HISTCMP_CHISQR_ALT = 4;

		public const int HISTCMP_KL_DIV = 5;

		public const int COLOR_BGR2BGRA = 0;

		public const int COLOR_RGB2RGBA = 0;

		public const int COLOR_BGRA2BGR = 1;

		public const int COLOR_RGBA2RGB = 1;

		public const int COLOR_BGR2RGBA = 2;

		public const int COLOR_RGB2BGRA = 2;

		public const int COLOR_RGBA2BGR = 3;

		public const int COLOR_BGRA2RGB = 3;

		public const int COLOR_BGR2RGB = 4;

		public const int COLOR_RGB2BGR = 4;

		public const int COLOR_BGRA2RGBA = 5;

		public const int COLOR_RGBA2BGRA = 5;

		public const int COLOR_BGR2GRAY = 6;

		public const int COLOR_RGB2GRAY = 7;

		public const int COLOR_GRAY2BGR = 8;

		public const int COLOR_GRAY2RGB = 8;

		public const int COLOR_GRAY2BGRA = 9;

		public const int COLOR_GRAY2RGBA = 9;

		public const int COLOR_BGRA2GRAY = 10;

		public const int COLOR_RGBA2GRAY = 11;

		public const int COLOR_BGR2BGR565 = 12;

		public const int COLOR_RGB2BGR565 = 13;

		public const int COLOR_BGR5652BGR = 14;

		public const int COLOR_BGR5652RGB = 15;

		public const int COLOR_BGRA2BGR565 = 16;

		public const int COLOR_RGBA2BGR565 = 17;

		public const int COLOR_BGR5652BGRA = 18;

		public const int COLOR_BGR5652RGBA = 19;

		public const int COLOR_GRAY2BGR565 = 20;

		public const int COLOR_BGR5652GRAY = 21;

		public const int COLOR_BGR2BGR555 = 22;

		public const int COLOR_RGB2BGR555 = 23;

		public const int COLOR_BGR5552BGR = 24;

		public const int COLOR_BGR5552RGB = 25;

		public const int COLOR_BGRA2BGR555 = 26;

		public const int COLOR_RGBA2BGR555 = 27;

		public const int COLOR_BGR5552BGRA = 28;

		public const int COLOR_BGR5552RGBA = 29;

		public const int COLOR_GRAY2BGR555 = 30;

		public const int COLOR_BGR5552GRAY = 31;

		public const int COLOR_BGR2XYZ = 32;

		public const int COLOR_RGB2XYZ = 33;

		public const int COLOR_XYZ2BGR = 34;

		public const int COLOR_XYZ2RGB = 35;

		public const int COLOR_BGR2YCrCb = 36;

		public const int COLOR_RGB2YCrCb = 37;

		public const int COLOR_YCrCb2BGR = 38;

		public const int COLOR_YCrCb2RGB = 39;

		public const int COLOR_BGR2HSV = 40;

		public const int COLOR_RGB2HSV = 41;

		public const int COLOR_BGR2Lab = 44;

		public const int COLOR_RGB2Lab = 45;

		public const int COLOR_BGR2Luv = 50;

		public const int COLOR_RGB2Luv = 51;

		public const int COLOR_BGR2HLS = 52;

		public const int COLOR_RGB2HLS = 53;

		public const int COLOR_HSV2BGR = 54;

		public const int COLOR_HSV2RGB = 55;

		public const int COLOR_Lab2BGR = 56;

		public const int COLOR_Lab2RGB = 57;

		public const int COLOR_Luv2BGR = 58;

		public const int COLOR_Luv2RGB = 59;

		public const int COLOR_HLS2BGR = 60;

		public const int COLOR_HLS2RGB = 61;

		public const int COLOR_BGR2HSV_FULL = 66;

		public const int COLOR_RGB2HSV_FULL = 67;

		public const int COLOR_BGR2HLS_FULL = 68;

		public const int COLOR_RGB2HLS_FULL = 69;

		public const int COLOR_HSV2BGR_FULL = 70;

		public const int COLOR_HSV2RGB_FULL = 71;

		public const int COLOR_HLS2BGR_FULL = 72;

		public const int COLOR_HLS2RGB_FULL = 73;

		public const int COLOR_LBGR2Lab = 74;

		public const int COLOR_LRGB2Lab = 75;

		public const int COLOR_LBGR2Luv = 76;

		public const int COLOR_LRGB2Luv = 77;

		public const int COLOR_Lab2LBGR = 78;

		public const int COLOR_Lab2LRGB = 79;

		public const int COLOR_Luv2LBGR = 80;

		public const int COLOR_Luv2LRGB = 81;

		public const int COLOR_BGR2YUV = 82;

		public const int COLOR_RGB2YUV = 83;

		public const int COLOR_YUV2BGR = 84;

		public const int COLOR_YUV2RGB = 85;

		public const int COLOR_YUV2RGB_NV12 = 90;

		public const int COLOR_YUV2BGR_NV12 = 91;

		public const int COLOR_YUV2RGB_NV21 = 92;

		public const int COLOR_YUV2BGR_NV21 = 93;

		public const int COLOR_YUV420sp2RGB = 92;

		public const int COLOR_YUV420sp2BGR = 93;

		public const int COLOR_YUV2RGBA_NV12 = 94;

		public const int COLOR_YUV2BGRA_NV12 = 95;

		public const int COLOR_YUV2RGBA_NV21 = 96;

		public const int COLOR_YUV2BGRA_NV21 = 97;

		public const int COLOR_YUV420sp2RGBA = 96;

		public const int COLOR_YUV420sp2BGRA = 97;

		public const int COLOR_YUV2RGB_YV12 = 98;

		public const int COLOR_YUV2BGR_YV12 = 99;

		public const int COLOR_YUV2RGB_IYUV = 100;

		public const int COLOR_YUV2BGR_IYUV = 101;

		public const int COLOR_YUV2RGB_I420 = 100;

		public const int COLOR_YUV2BGR_I420 = 101;

		public const int COLOR_YUV420p2RGB = 98;

		public const int COLOR_YUV420p2BGR = 99;

		public const int COLOR_YUV2RGBA_YV12 = 102;

		public const int COLOR_YUV2BGRA_YV12 = 103;

		public const int COLOR_YUV2RGBA_IYUV = 104;

		public const int COLOR_YUV2BGRA_IYUV = 105;

		public const int COLOR_YUV2RGBA_I420 = 104;

		public const int COLOR_YUV2BGRA_I420 = 105;

		public const int COLOR_YUV420p2RGBA = 102;

		public const int COLOR_YUV420p2BGRA = 103;

		public const int COLOR_YUV2GRAY_420 = 106;

		public const int COLOR_YUV2GRAY_NV21 = 106;

		public const int COLOR_YUV2GRAY_NV12 = 106;

		public const int COLOR_YUV2GRAY_YV12 = 106;

		public const int COLOR_YUV2GRAY_IYUV = 106;

		public const int COLOR_YUV2GRAY_I420 = 106;

		public const int COLOR_YUV420sp2GRAY = 106;

		public const int COLOR_YUV420p2GRAY = 106;

		public const int COLOR_YUV2RGB_UYVY = 107;

		public const int COLOR_YUV2BGR_UYVY = 108;

		public const int COLOR_YUV2RGB_Y422 = 107;

		public const int COLOR_YUV2BGR_Y422 = 108;

		public const int COLOR_YUV2RGB_UYNV = 107;

		public const int COLOR_YUV2BGR_UYNV = 108;

		public const int COLOR_YUV2RGBA_UYVY = 111;

		public const int COLOR_YUV2BGRA_UYVY = 112;

		public const int COLOR_YUV2RGBA_Y422 = 111;

		public const int COLOR_YUV2BGRA_Y422 = 112;

		public const int COLOR_YUV2RGBA_UYNV = 111;

		public const int COLOR_YUV2BGRA_UYNV = 112;

		public const int COLOR_YUV2RGB_YUY2 = 115;

		public const int COLOR_YUV2BGR_YUY2 = 116;

		public const int COLOR_YUV2RGB_YVYU = 117;

		public const int COLOR_YUV2BGR_YVYU = 118;

		public const int COLOR_YUV2RGB_YUYV = 115;

		public const int COLOR_YUV2BGR_YUYV = 116;

		public const int COLOR_YUV2RGB_YUNV = 115;

		public const int COLOR_YUV2BGR_YUNV = 116;

		public const int COLOR_YUV2RGBA_YUY2 = 119;

		public const int COLOR_YUV2BGRA_YUY2 = 120;

		public const int COLOR_YUV2RGBA_YVYU = 121;

		public const int COLOR_YUV2BGRA_YVYU = 122;

		public const int COLOR_YUV2RGBA_YUYV = 119;

		public const int COLOR_YUV2BGRA_YUYV = 120;

		public const int COLOR_YUV2RGBA_YUNV = 119;

		public const int COLOR_YUV2BGRA_YUNV = 120;

		public const int COLOR_YUV2GRAY_UYVY = 123;

		public const int COLOR_YUV2GRAY_YUY2 = 124;

		public const int COLOR_YUV2GRAY_Y422 = 123;

		public const int COLOR_YUV2GRAY_UYNV = 123;

		public const int COLOR_YUV2GRAY_YVYU = 124;

		public const int COLOR_YUV2GRAY_YUYV = 124;

		public const int COLOR_YUV2GRAY_YUNV = 124;

		public const int COLOR_RGBA2mRGBA = 125;

		public const int COLOR_mRGBA2RGBA = 126;

		public const int COLOR_RGB2YUV_I420 = 127;

		public const int COLOR_BGR2YUV_I420 = 128;

		public const int COLOR_RGB2YUV_IYUV = 127;

		public const int COLOR_BGR2YUV_IYUV = 128;

		public const int COLOR_RGBA2YUV_I420 = 129;

		public const int COLOR_BGRA2YUV_I420 = 130;

		public const int COLOR_RGBA2YUV_IYUV = 129;

		public const int COLOR_BGRA2YUV_IYUV = 130;

		public const int COLOR_RGB2YUV_YV12 = 131;

		public const int COLOR_BGR2YUV_YV12 = 132;

		public const int COLOR_RGBA2YUV_YV12 = 133;

		public const int COLOR_BGRA2YUV_YV12 = 134;

		public const int COLOR_BayerBG2BGR = 46;

		public const int COLOR_BayerGB2BGR = 47;

		public const int COLOR_BayerRG2BGR = 48;

		public const int COLOR_BayerGR2BGR = 49;

		public const int COLOR_BayerBG2RGB = 48;

		public const int COLOR_BayerGB2RGB = 49;

		public const int COLOR_BayerRG2RGB = 46;

		public const int COLOR_BayerGR2RGB = 47;

		public const int COLOR_BayerBG2GRAY = 86;

		public const int COLOR_BayerGB2GRAY = 87;

		public const int COLOR_BayerRG2GRAY = 88;

		public const int COLOR_BayerGR2GRAY = 89;

		public const int COLOR_BayerBG2BGR_VNG = 62;

		public const int COLOR_BayerGB2BGR_VNG = 63;

		public const int COLOR_BayerRG2BGR_VNG = 64;

		public const int COLOR_BayerGR2BGR_VNG = 65;

		public const int COLOR_BayerBG2RGB_VNG = 64;

		public const int COLOR_BayerGB2RGB_VNG = 65;

		public const int COLOR_BayerRG2RGB_VNG = 62;

		public const int COLOR_BayerGR2RGB_VNG = 63;

		public const int COLOR_BayerBG2BGR_EA = 135;

		public const int COLOR_BayerGB2BGR_EA = 136;

		public const int COLOR_BayerRG2BGR_EA = 137;

		public const int COLOR_BayerGR2BGR_EA = 138;

		public const int COLOR_BayerBG2RGB_EA = 137;

		public const int COLOR_BayerGB2RGB_EA = 138;

		public const int COLOR_BayerRG2RGB_EA = 135;

		public const int COLOR_BayerGR2RGB_EA = 136;

		public const int COLOR_COLORCVT_MAX = 139;

		public const int INTERSECT_NONE = 0;

		public const int INTERSECT_PARTIAL = 1;

		public const int INTERSECT_FULL = 2;

		public const int TM_SQDIFF = 0;

		public const int TM_SQDIFF_NORMED = 1;

		public const int TM_CCORR = 2;

		public const int TM_CCORR_NORMED = 3;

		public const int TM_CCOEFF = 4;

		public const int TM_CCOEFF_NORMED = 5;

		public const int COLORMAP_AUTUMN = 0;

		public const int COLORMAP_BONE = 1;

		public const int COLORMAP_JET = 2;

		public const int COLORMAP_WINTER = 3;

		public const int COLORMAP_RAINBOW = 4;

		public const int COLORMAP_OCEAN = 5;

		public const int COLORMAP_SUMMER = 6;

		public const int COLORMAP_SPRING = 7;

		public const int COLORMAP_COOL = 8;

		public const int COLORMAP_HSV = 9;

		public const int COLORMAP_PINK = 10;

		public const int COLORMAP_HOT = 11;

		public const int COLORMAP_PARULA = 12;

		public const int MARKER_CROSS = 0;

		public const int MARKER_TILTED_CROSS = 1;

		public const int MARKER_STAR = 2;

		public const int MARKER_DIAMOND = 3;

		public const int MARKER_SQUARE = 4;

		public const int MARKER_TRIANGLE_UP = 5;

		public const int MARKER_TRIANGLE_DOWN = 6;

		private const string LIBNAME = "opencvforunity";

		public static Mat getAffineTransform(MatOfPoint2f src, MatOfPoint2f dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return new Mat(imgproc_Imgproc_getAffineTransform_10(src.nativeObj, dst.nativeObj));
		}

		public static Mat getDefaultNewCameraMatrix(Mat cameraMatrix, Size imgsize, bool centerPrincipalPoint)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			return new Mat(imgproc_Imgproc_getDefaultNewCameraMatrix_10(cameraMatrix.nativeObj, imgsize.width, imgsize.height, centerPrincipalPoint));
		}

		public static Mat getDefaultNewCameraMatrix(Mat cameraMatrix)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			return new Mat(imgproc_Imgproc_getDefaultNewCameraMatrix_11(cameraMatrix.nativeObj));
		}

		public static Mat getGaborKernel(Size ksize, double sigma, double theta, double lambd, double gamma, double psi, int ktype)
		{
			return new Mat(imgproc_Imgproc_getGaborKernel_10(ksize.width, ksize.height, sigma, theta, lambd, gamma, psi, ktype));
		}

		public static Mat getGaborKernel(Size ksize, double sigma, double theta, double lambd, double gamma)
		{
			return new Mat(imgproc_Imgproc_getGaborKernel_11(ksize.width, ksize.height, sigma, theta, lambd, gamma));
		}

		public static Mat getGaussianKernel(int ksize, double sigma, int ktype)
		{
			return new Mat(imgproc_Imgproc_getGaussianKernel_10(ksize, sigma, ktype));
		}

		public static Mat getGaussianKernel(int ksize, double sigma)
		{
			return new Mat(imgproc_Imgproc_getGaussianKernel_11(ksize, sigma));
		}

		public static Mat getPerspectiveTransform(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return new Mat(imgproc_Imgproc_getPerspectiveTransform_10(src.nativeObj, dst.nativeObj));
		}

		public static Mat getRotationMatrix2D(Point center, double angle, double scale)
		{
			return new Mat(imgproc_Imgproc_getRotationMatrix2D_10(center.x, center.y, angle, scale));
		}

		public static Mat getStructuringElement(int shape, Size ksize, Point anchor)
		{
			return new Mat(imgproc_Imgproc_getStructuringElement_10(shape, ksize.width, ksize.height, anchor.x, anchor.y));
		}

		public static Mat getStructuringElement(int shape, Size ksize)
		{
			return new Mat(imgproc_Imgproc_getStructuringElement_11(shape, ksize.width, ksize.height));
		}

		public static Moments moments(Mat array, bool binaryImage)
		{
			if (array != null)
			{
				array.ThrowIfDisposed();
			}
			double[] vals = new double[10];
			imgproc_Imgproc_moments_10(array.nativeObj, binaryImage, vals);
			return new Moments(vals);
		}

		public static Moments moments(Mat array)
		{
			if (array != null)
			{
				array.ThrowIfDisposed();
			}
			double[] vals = new double[10];
			imgproc_Imgproc_moments_11(array.nativeObj, vals);
			return new Moments(vals);
		}

		public static Point phaseCorrelate(Mat src1, Mat src2, Mat window, double[] response)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (window != null)
			{
				window.ThrowIfDisposed();
			}
			double[] array = new double[1];
			double[] vals = new double[2];
			imgproc_Imgproc_phaseCorrelate_10(src1.nativeObj, src2.nativeObj, window.nativeObj, array, vals);
			Point result = new Point(vals);
			if (response != null)
			{
				response[0] = array[0];
			}
			return result;
		}

		public static Point phaseCorrelate(Mat src1, Mat src2)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			double[] vals = new double[2];
			imgproc_Imgproc_phaseCorrelate_11(src1.nativeObj, src2.nativeObj, vals);
			return new Point(vals);
		}

		public static CLAHE createCLAHE(double clipLimit, Size tileGridSize)
		{
			return new CLAHE(imgproc_Imgproc_createCLAHE_10(clipLimit, tileGridSize.width, tileGridSize.height));
		}

		public static CLAHE createCLAHE()
		{
			return new CLAHE(imgproc_Imgproc_createCLAHE_11());
		}

		public static LineSegmentDetector createLineSegmentDetector(int _refine, double _scale, double _sigma_scale, double _quant, double _ang_th, double _log_eps, double _density_th, int _n_bins)
		{
			return new LineSegmentDetector(imgproc_Imgproc_createLineSegmentDetector_10(_refine, _scale, _sigma_scale, _quant, _ang_th, _log_eps, _density_th, _n_bins));
		}

		public static LineSegmentDetector createLineSegmentDetector()
		{
			return new LineSegmentDetector(imgproc_Imgproc_createLineSegmentDetector_11());
		}

		public static Rect boundingRect(MatOfPoint points)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			double[] vals = new double[4];
			imgproc_Imgproc_boundingRect_10(points.nativeObj, vals);
			return new Rect(vals);
		}

		public static RotatedRect fitEllipse(MatOfPoint2f points)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			double[] vals = new double[5];
			imgproc_Imgproc_fitEllipse_10(points.nativeObj, vals);
			return new RotatedRect(vals);
		}

		public static RotatedRect minAreaRect(MatOfPoint2f points)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			double[] vals = new double[5];
			imgproc_Imgproc_minAreaRect_10(points.nativeObj, vals);
			return new RotatedRect(vals);
		}

		public static bool clipLine(Rect imgRect, Point pt1, Point pt2)
		{
			double[] array = new double[2];
			double[] array2 = new double[2];
			bool result = imgproc_Imgproc_clipLine_10(imgRect.x, imgRect.y, imgRect.width, imgRect.height, pt1.x, pt1.y, array, pt2.x, pt2.y, array2);
			if (pt1 != null)
			{
				pt1.x = array[0];
				pt1.y = array[1];
			}
			if (pt2 != null)
			{
				pt2.x = array2[0];
				pt2.y = array2[1];
			}
			return result;
		}

		public static bool isContourConvex(MatOfPoint contour)
		{
			if (contour != null)
			{
				contour.ThrowIfDisposed();
			}
			return imgproc_Imgproc_isContourConvex_10(contour.nativeObj);
		}

		public static double arcLength(MatOfPoint2f curve, bool closed)
		{
			if (curve != null)
			{
				curve.ThrowIfDisposed();
			}
			return imgproc_Imgproc_arcLength_10(curve.nativeObj, closed);
		}

		public static double compareHist(Mat H1, Mat H2, int method)
		{
			if (H1 != null)
			{
				H1.ThrowIfDisposed();
			}
			if (H2 != null)
			{
				H2.ThrowIfDisposed();
			}
			return imgproc_Imgproc_compareHist_10(H1.nativeObj, H2.nativeObj, method);
		}

		public static double contourArea(Mat contour, bool oriented)
		{
			if (contour != null)
			{
				contour.ThrowIfDisposed();
			}
			return imgproc_Imgproc_contourArea_10(contour.nativeObj, oriented);
		}

		public static double contourArea(Mat contour)
		{
			if (contour != null)
			{
				contour.ThrowIfDisposed();
			}
			return imgproc_Imgproc_contourArea_11(contour.nativeObj);
		}

		public static double matchShapes(Mat contour1, Mat contour2, int method, double parameter)
		{
			if (contour1 != null)
			{
				contour1.ThrowIfDisposed();
			}
			if (contour2 != null)
			{
				contour2.ThrowIfDisposed();
			}
			return imgproc_Imgproc_matchShapes_10(contour1.nativeObj, contour2.nativeObj, method, parameter);
		}

		public static double minEnclosingTriangle(Mat points, Mat triangle)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			if (triangle != null)
			{
				triangle.ThrowIfDisposed();
			}
			return imgproc_Imgproc_minEnclosingTriangle_10(points.nativeObj, triangle.nativeObj);
		}

		public static double pointPolygonTest(MatOfPoint2f contour, Point pt, bool measureDist)
		{
			if (contour != null)
			{
				contour.ThrowIfDisposed();
			}
			return imgproc_Imgproc_pointPolygonTest_10(contour.nativeObj, pt.x, pt.y, measureDist);
		}

		public static double threshold(Mat src, Mat dst, double thresh, double maxval, int type)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return imgproc_Imgproc_threshold_10(src.nativeObj, dst.nativeObj, thresh, maxval, type);
		}

		public static float initWideAngleProjMap(Mat cameraMatrix, Mat distCoeffs, Size imageSize, int destImageWidth, int m1type, Mat map1, Mat map2, int projType, double alpha)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			return imgproc_Imgproc_initWideAngleProjMap_10(cameraMatrix.nativeObj, distCoeffs.nativeObj, imageSize.width, imageSize.height, destImageWidth, m1type, map1.nativeObj, map2.nativeObj, projType, alpha);
		}

		public static float initWideAngleProjMap(Mat cameraMatrix, Mat distCoeffs, Size imageSize, int destImageWidth, int m1type, Mat map1, Mat map2)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			return imgproc_Imgproc_initWideAngleProjMap_11(cameraMatrix.nativeObj, distCoeffs.nativeObj, imageSize.width, imageSize.height, destImageWidth, m1type, map1.nativeObj, map2.nativeObj);
		}

		public static float intersectConvexConvex(Mat _p1, Mat _p2, Mat _p12, bool handleNested)
		{
			if (_p1 != null)
			{
				_p1.ThrowIfDisposed();
			}
			if (_p2 != null)
			{
				_p2.ThrowIfDisposed();
			}
			if (_p12 != null)
			{
				_p12.ThrowIfDisposed();
			}
			return imgproc_Imgproc_intersectConvexConvex_10(_p1.nativeObj, _p2.nativeObj, _p12.nativeObj, handleNested);
		}

		public static float intersectConvexConvex(Mat _p1, Mat _p2, Mat _p12)
		{
			if (_p1 != null)
			{
				_p1.ThrowIfDisposed();
			}
			if (_p2 != null)
			{
				_p2.ThrowIfDisposed();
			}
			if (_p12 != null)
			{
				_p12.ThrowIfDisposed();
			}
			return imgproc_Imgproc_intersectConvexConvex_11(_p1.nativeObj, _p2.nativeObj, _p12.nativeObj);
		}

		public static int connectedComponents(Mat image, Mat labels, int connectivity, int ltype)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			return imgproc_Imgproc_connectedComponents_10(image.nativeObj, labels.nativeObj, connectivity, ltype);
		}

		public static int connectedComponents(Mat image, Mat labels)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			return imgproc_Imgproc_connectedComponents_11(image.nativeObj, labels.nativeObj);
		}

		public static int connectedComponentsWithStats(Mat image, Mat labels, Mat stats, Mat centroids, int connectivity, int ltype)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			if (stats != null)
			{
				stats.ThrowIfDisposed();
			}
			if (centroids != null)
			{
				centroids.ThrowIfDisposed();
			}
			return imgproc_Imgproc_connectedComponentsWithStats_10(image.nativeObj, labels.nativeObj, stats.nativeObj, centroids.nativeObj, connectivity, ltype);
		}

		public static int connectedComponentsWithStats(Mat image, Mat labels, Mat stats, Mat centroids)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			if (stats != null)
			{
				stats.ThrowIfDisposed();
			}
			if (centroids != null)
			{
				centroids.ThrowIfDisposed();
			}
			return imgproc_Imgproc_connectedComponentsWithStats_11(image.nativeObj, labels.nativeObj, stats.nativeObj, centroids.nativeObj);
		}

		public static int floodFill(Mat image, Mat mask, Point seedPoint, Scalar newVal, Rect rect, Scalar loDiff, Scalar upDiff, int flags)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			double[] array = new double[4];
			int result = imgproc_Imgproc_floodFill_10(image.nativeObj, mask.nativeObj, seedPoint.x, seedPoint.y, newVal.val[0], newVal.val[1], newVal.val[2], newVal.val[3], array, loDiff.val[0], loDiff.val[1], loDiff.val[2], loDiff.val[3], upDiff.val[0], upDiff.val[1], upDiff.val[2], upDiff.val[3], flags);
			if (rect != null)
			{
				rect.x = (int)array[0];
				rect.y = (int)array[1];
				rect.width = (int)array[2];
				rect.height = (int)array[3];
			}
			return result;
		}

		public static int floodFill(Mat image, Mat mask, Point seedPoint, Scalar newVal)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			return imgproc_Imgproc_floodFill_11(image.nativeObj, mask.nativeObj, seedPoint.x, seedPoint.y, newVal.val[0], newVal.val[1], newVal.val[2], newVal.val[3]);
		}

		public static int rotatedRectangleIntersection(RotatedRect rect1, RotatedRect rect2, Mat intersectingRegion)
		{
			if (intersectingRegion != null)
			{
				intersectingRegion.ThrowIfDisposed();
			}
			return imgproc_Imgproc_rotatedRectangleIntersection_10(rect1.center.x, rect1.center.y, rect1.size.width, rect1.size.height, rect1.angle, rect2.center.x, rect2.center.y, rect2.size.width, rect2.size.height, rect2.angle, intersectingRegion.nativeObj);
		}

		public static void Canny(Mat image, Mat edges, double threshold1, double threshold2, int apertureSize, bool L2gradient)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (edges != null)
			{
				edges.ThrowIfDisposed();
			}
			imgproc_Imgproc_Canny_10(image.nativeObj, edges.nativeObj, threshold1, threshold2, apertureSize, L2gradient);
		}

		public static void Canny(Mat image, Mat edges, double threshold1, double threshold2)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (edges != null)
			{
				edges.ThrowIfDisposed();
			}
			imgproc_Imgproc_Canny_11(image.nativeObj, edges.nativeObj, threshold1, threshold2);
		}

		public static void GaussianBlur(Mat src, Mat dst, Size ksize, double sigmaX, double sigmaY, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_GaussianBlur_10(src.nativeObj, dst.nativeObj, ksize.width, ksize.height, sigmaX, sigmaY, borderType);
		}

		public static void GaussianBlur(Mat src, Mat dst, Size ksize, double sigmaX, double sigmaY)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_GaussianBlur_11(src.nativeObj, dst.nativeObj, ksize.width, ksize.height, sigmaX, sigmaY);
		}

		public static void GaussianBlur(Mat src, Mat dst, Size ksize, double sigmaX)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_GaussianBlur_12(src.nativeObj, dst.nativeObj, ksize.width, ksize.height, sigmaX);
		}

		public static void HoughCircles(Mat image, Mat circles, int method, double dp, double minDist, double param1, double param2, int minRadius, int maxRadius)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (circles != null)
			{
				circles.ThrowIfDisposed();
			}
			imgproc_Imgproc_HoughCircles_10(image.nativeObj, circles.nativeObj, method, dp, minDist, param1, param2, minRadius, maxRadius);
		}

		public static void HoughCircles(Mat image, Mat circles, int method, double dp, double minDist)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (circles != null)
			{
				circles.ThrowIfDisposed();
			}
			imgproc_Imgproc_HoughCircles_11(image.nativeObj, circles.nativeObj, method, dp, minDist);
		}

		public static void HoughLines(Mat image, Mat lines, double rho, double theta, int threshold, double srn, double stn, double min_theta, double max_theta)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (lines != null)
			{
				lines.ThrowIfDisposed();
			}
			imgproc_Imgproc_HoughLines_10(image.nativeObj, lines.nativeObj, rho, theta, threshold, srn, stn, min_theta, max_theta);
		}

		public static void HoughLines(Mat image, Mat lines, double rho, double theta, int threshold)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (lines != null)
			{
				lines.ThrowIfDisposed();
			}
			imgproc_Imgproc_HoughLines_11(image.nativeObj, lines.nativeObj, rho, theta, threshold);
		}

		public static void HoughLinesP(Mat image, Mat lines, double rho, double theta, int threshold, double minLineLength, double maxLineGap)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (lines != null)
			{
				lines.ThrowIfDisposed();
			}
			imgproc_Imgproc_HoughLinesP_10(image.nativeObj, lines.nativeObj, rho, theta, threshold, minLineLength, maxLineGap);
		}

		public static void HoughLinesP(Mat image, Mat lines, double rho, double theta, int threshold)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (lines != null)
			{
				lines.ThrowIfDisposed();
			}
			imgproc_Imgproc_HoughLinesP_11(image.nativeObj, lines.nativeObj, rho, theta, threshold);
		}

		public static void HuMoments(Moments m, Mat hu)
		{
			if (hu != null)
			{
				hu.ThrowIfDisposed();
			}
			imgproc_Imgproc_HuMoments_10(m.m00, m.m10, m.m01, m.m20, m.m11, m.m02, m.m30, m.m21, m.m12, m.m03, hu.nativeObj);
		}

		public static void Laplacian(Mat src, Mat dst, int ddepth, int ksize, double scale, double delta, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Laplacian_10(src.nativeObj, dst.nativeObj, ddepth, ksize, scale, delta, borderType);
		}

		public static void Laplacian(Mat src, Mat dst, int ddepth, int ksize, double scale, double delta)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Laplacian_11(src.nativeObj, dst.nativeObj, ddepth, ksize, scale, delta);
		}

		public static void Laplacian(Mat src, Mat dst, int ddepth)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Laplacian_12(src.nativeObj, dst.nativeObj, ddepth);
		}

		public static void Scharr(Mat src, Mat dst, int ddepth, int dx, int dy, double scale, double delta, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Scharr_10(src.nativeObj, dst.nativeObj, ddepth, dx, dy, scale, delta, borderType);
		}

		public static void Scharr(Mat src, Mat dst, int ddepth, int dx, int dy, double scale, double delta)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Scharr_11(src.nativeObj, dst.nativeObj, ddepth, dx, dy, scale, delta);
		}

		public static void Scharr(Mat src, Mat dst, int ddepth, int dx, int dy)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Scharr_12(src.nativeObj, dst.nativeObj, ddepth, dx, dy);
		}

		public static void Sobel(Mat src, Mat dst, int ddepth, int dx, int dy, int ksize, double scale, double delta, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Sobel_10(src.nativeObj, dst.nativeObj, ddepth, dx, dy, ksize, scale, delta, borderType);
		}

		public static void Sobel(Mat src, Mat dst, int ddepth, int dx, int dy, int ksize, double scale, double delta)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Sobel_11(src.nativeObj, dst.nativeObj, ddepth, dx, dy, ksize, scale, delta);
		}

		public static void Sobel(Mat src, Mat dst, int ddepth, int dx, int dy)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_Sobel_12(src.nativeObj, dst.nativeObj, ddepth, dx, dy);
		}

		public static void accumulate(Mat src, Mat dst, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulate_10(src.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void accumulate(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulate_11(src.nativeObj, dst.nativeObj);
		}

		public static void accumulateProduct(Mat src1, Mat src2, Mat dst, Mat mask)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulateProduct_10(src1.nativeObj, src2.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void accumulateProduct(Mat src1, Mat src2, Mat dst)
		{
			if (src1 != null)
			{
				src1.ThrowIfDisposed();
			}
			if (src2 != null)
			{
				src2.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulateProduct_11(src1.nativeObj, src2.nativeObj, dst.nativeObj);
		}

		public static void accumulateSquare(Mat src, Mat dst, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulateSquare_10(src.nativeObj, dst.nativeObj, mask.nativeObj);
		}

		public static void accumulateSquare(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulateSquare_11(src.nativeObj, dst.nativeObj);
		}

		public static void accumulateWeighted(Mat src, Mat dst, double alpha, Mat mask)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulateWeighted_10(src.nativeObj, dst.nativeObj, alpha, mask.nativeObj);
		}

		public static void accumulateWeighted(Mat src, Mat dst, double alpha)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_accumulateWeighted_11(src.nativeObj, dst.nativeObj, alpha);
		}

		public static void adaptiveThreshold(Mat src, Mat dst, double maxValue, int adaptiveMethod, int thresholdType, int blockSize, double C)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_adaptiveThreshold_10(src.nativeObj, dst.nativeObj, maxValue, adaptiveMethod, thresholdType, blockSize, C);
		}

		public static void applyColorMap(Mat src, Mat dst, int colormap)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_applyColorMap_10(src.nativeObj, dst.nativeObj, colormap);
		}

		public static void approxPolyDP(MatOfPoint2f curve, MatOfPoint2f approxCurve, double epsilon, bool closed)
		{
			if (curve != null)
			{
				curve.ThrowIfDisposed();
			}
			if (approxCurve != null)
			{
				approxCurve.ThrowIfDisposed();
			}
			imgproc_Imgproc_approxPolyDP_10(curve.nativeObj, approxCurve.nativeObj, epsilon, closed);
		}

		public static void arrowedLine(Mat img, Point pt1, Point pt2, Scalar color, int thickness, int line_type, int shift, double tipLength)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_arrowedLine_10(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3], thickness, line_type, shift, tipLength);
		}

		public static void arrowedLine(Mat img, Point pt1, Point pt2, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_arrowedLine_11(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void bilateralFilter(Mat src, Mat dst, int d, double sigmaColor, double sigmaSpace, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_bilateralFilter_10(src.nativeObj, dst.nativeObj, d, sigmaColor, sigmaSpace, borderType);
		}

		public static void bilateralFilter(Mat src, Mat dst, int d, double sigmaColor, double sigmaSpace)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_bilateralFilter_11(src.nativeObj, dst.nativeObj, d, sigmaColor, sigmaSpace);
		}

		public static void blur(Mat src, Mat dst, Size ksize, Point anchor, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_blur_10(src.nativeObj, dst.nativeObj, ksize.width, ksize.height, anchor.x, anchor.y, borderType);
		}

		public static void blur(Mat src, Mat dst, Size ksize, Point anchor)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_blur_11(src.nativeObj, dst.nativeObj, ksize.width, ksize.height, anchor.x, anchor.y);
		}

		public static void blur(Mat src, Mat dst, Size ksize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_blur_12(src.nativeObj, dst.nativeObj, ksize.width, ksize.height);
		}

		public static void boxFilter(Mat src, Mat dst, int ddepth, Size ksize, Point anchor, bool normalize, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_boxFilter_10(src.nativeObj, dst.nativeObj, ddepth, ksize.width, ksize.height, anchor.x, anchor.y, normalize, borderType);
		}

		public static void boxFilter(Mat src, Mat dst, int ddepth, Size ksize, Point anchor, bool normalize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_boxFilter_11(src.nativeObj, dst.nativeObj, ddepth, ksize.width, ksize.height, anchor.x, anchor.y, normalize);
		}

		public static void boxFilter(Mat src, Mat dst, int ddepth, Size ksize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_boxFilter_12(src.nativeObj, dst.nativeObj, ddepth, ksize.width, ksize.height);
		}

		public static void boxPoints(RotatedRect box, Mat points)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			imgproc_Imgproc_boxPoints_10(box.center.x, box.center.y, box.size.width, box.size.height, box.angle, points.nativeObj);
		}

		public static void calcBackProject(List<Mat> images, MatOfInt channels, Mat hist, Mat dst, MatOfFloat ranges, double scale)
		{
			if (channels != null)
			{
				channels.ThrowIfDisposed();
			}
			if (hist != null)
			{
				hist.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (ranges != null)
			{
				ranges.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(images);
			imgproc_Imgproc_calcBackProject_10(mat.nativeObj, channels.nativeObj, hist.nativeObj, dst.nativeObj, ranges.nativeObj, scale);
		}

		public static void calcHist(List<Mat> images, MatOfInt channels, Mat mask, Mat hist, MatOfInt histSize, MatOfFloat ranges, bool accumulate)
		{
			if (channels != null)
			{
				channels.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (hist != null)
			{
				hist.ThrowIfDisposed();
			}
			if (histSize != null)
			{
				histSize.ThrowIfDisposed();
			}
			if (ranges != null)
			{
				ranges.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(images);
			imgproc_Imgproc_calcHist_10(mat.nativeObj, channels.nativeObj, mask.nativeObj, hist.nativeObj, histSize.nativeObj, ranges.nativeObj, accumulate);
		}

		public static void calcHist(List<Mat> images, MatOfInt channels, Mat mask, Mat hist, MatOfInt histSize, MatOfFloat ranges)
		{
			if (channels != null)
			{
				channels.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (hist != null)
			{
				hist.ThrowIfDisposed();
			}
			if (histSize != null)
			{
				histSize.ThrowIfDisposed();
			}
			if (ranges != null)
			{
				ranges.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(images);
			imgproc_Imgproc_calcHist_11(mat.nativeObj, channels.nativeObj, mask.nativeObj, hist.nativeObj, histSize.nativeObj, ranges.nativeObj);
		}

		public static void circle(Mat img, Point center, int radius, Scalar color, int thickness, int lineType, int shift)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_circle_10(img.nativeObj, center.x, center.y, radius, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType, shift);
		}

		public static void circle(Mat img, Point center, int radius, Scalar color, int thickness)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_circle_11(img.nativeObj, center.x, center.y, radius, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void circle(Mat img, Point center, int radius, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_circle_12(img.nativeObj, center.x, center.y, radius, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void convertMaps(Mat map1, Mat map2, Mat dstmap1, Mat dstmap2, int dstmap1type, bool nninterpolation)
		{
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			if (dstmap1 != null)
			{
				dstmap1.ThrowIfDisposed();
			}
			if (dstmap2 != null)
			{
				dstmap2.ThrowIfDisposed();
			}
			imgproc_Imgproc_convertMaps_10(map1.nativeObj, map2.nativeObj, dstmap1.nativeObj, dstmap2.nativeObj, dstmap1type, nninterpolation);
		}

		public static void convertMaps(Mat map1, Mat map2, Mat dstmap1, Mat dstmap2, int dstmap1type)
		{
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			if (dstmap1 != null)
			{
				dstmap1.ThrowIfDisposed();
			}
			if (dstmap2 != null)
			{
				dstmap2.ThrowIfDisposed();
			}
			imgproc_Imgproc_convertMaps_11(map1.nativeObj, map2.nativeObj, dstmap1.nativeObj, dstmap2.nativeObj, dstmap1type);
		}

		public static void convexHull(MatOfPoint points, MatOfInt hull, bool clockwise)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			if (hull != null)
			{
				hull.ThrowIfDisposed();
			}
			imgproc_Imgproc_convexHull_10(points.nativeObj, hull.nativeObj, clockwise);
		}

		public static void convexHull(MatOfPoint points, MatOfInt hull)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			if (hull != null)
			{
				hull.ThrowIfDisposed();
			}
			imgproc_Imgproc_convexHull_11(points.nativeObj, hull.nativeObj);
		}

		public static void convexityDefects(MatOfPoint contour, MatOfInt convexhull, MatOfInt4 convexityDefects)
		{
			if (contour != null)
			{
				contour.ThrowIfDisposed();
			}
			if (convexhull != null)
			{
				convexhull.ThrowIfDisposed();
			}
			if (convexityDefects != null)
			{
				convexityDefects.ThrowIfDisposed();
			}
			imgproc_Imgproc_convexityDefects_10(contour.nativeObj, convexhull.nativeObj, convexityDefects.nativeObj);
		}

		public static void cornerEigenValsAndVecs(Mat src, Mat dst, int blockSize, int ksize, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerEigenValsAndVecs_10(src.nativeObj, dst.nativeObj, blockSize, ksize, borderType);
		}

		public static void cornerEigenValsAndVecs(Mat src, Mat dst, int blockSize, int ksize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerEigenValsAndVecs_11(src.nativeObj, dst.nativeObj, blockSize, ksize);
		}

		public static void cornerHarris(Mat src, Mat dst, int blockSize, int ksize, double k, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerHarris_10(src.nativeObj, dst.nativeObj, blockSize, ksize, k, borderType);
		}

		public static void cornerHarris(Mat src, Mat dst, int blockSize, int ksize, double k)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerHarris_11(src.nativeObj, dst.nativeObj, blockSize, ksize, k);
		}

		public static void cornerMinEigenVal(Mat src, Mat dst, int blockSize, int ksize, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerMinEigenVal_10(src.nativeObj, dst.nativeObj, blockSize, ksize, borderType);
		}

		public static void cornerMinEigenVal(Mat src, Mat dst, int blockSize, int ksize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerMinEigenVal_11(src.nativeObj, dst.nativeObj, blockSize, ksize);
		}

		public static void cornerMinEigenVal(Mat src, Mat dst, int blockSize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerMinEigenVal_12(src.nativeObj, dst.nativeObj, blockSize);
		}

		public static void cornerSubPix(Mat image, MatOfPoint2f corners, Size winSize, Size zeroZone, TermCriteria criteria)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (corners != null)
			{
				corners.ThrowIfDisposed();
			}
			imgproc_Imgproc_cornerSubPix_10(image.nativeObj, corners.nativeObj, winSize.width, winSize.height, zeroZone.width, zeroZone.height, criteria.type, criteria.maxCount, criteria.epsilon);
		}

		public static void createHanningWindow(Mat dst, Size winSize, int type)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_createHanningWindow_10(dst.nativeObj, winSize.width, winSize.height, type);
		}

		public static void cvtColor(Mat src, Mat dst, int code, int dstCn)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cvtColor_10(src.nativeObj, dst.nativeObj, code, dstCn);
		}

		public static void cvtColor(Mat src, Mat dst, int code)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_cvtColor_11(src.nativeObj, dst.nativeObj, code);
		}

		public static void demosaicing(Mat _src, Mat _dst, int code, int dcn)
		{
			if (_src != null)
			{
				_src.ThrowIfDisposed();
			}
			if (_dst != null)
			{
				_dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_demosaicing_10(_src.nativeObj, _dst.nativeObj, code, dcn);
		}

		public static void demosaicing(Mat _src, Mat _dst, int code)
		{
			if (_src != null)
			{
				_src.ThrowIfDisposed();
			}
			if (_dst != null)
			{
				_dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_demosaicing_11(_src.nativeObj, _dst.nativeObj, code);
		}

		public static void dilate(Mat src, Mat dst, Mat kernel, Point anchor, int iterations, int borderType, Scalar borderValue)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_dilate_10(src.nativeObj, dst.nativeObj, kernel.nativeObj, anchor.x, anchor.y, iterations, borderType, borderValue.val[0], borderValue.val[1], borderValue.val[2], borderValue.val[3]);
		}

		public static void dilate(Mat src, Mat dst, Mat kernel, Point anchor, int iterations)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_dilate_11(src.nativeObj, dst.nativeObj, kernel.nativeObj, anchor.x, anchor.y, iterations);
		}

		public static void dilate(Mat src, Mat dst, Mat kernel)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_dilate_12(src.nativeObj, dst.nativeObj, kernel.nativeObj);
		}

		public static void distanceTransformWithLabels(Mat src, Mat dst, Mat labels, int distanceType, int maskSize, int labelType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			imgproc_Imgproc_distanceTransformWithLabels_10(src.nativeObj, dst.nativeObj, labels.nativeObj, distanceType, maskSize, labelType);
		}

		public static void distanceTransformWithLabels(Mat src, Mat dst, Mat labels, int distanceType, int maskSize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (labels != null)
			{
				labels.ThrowIfDisposed();
			}
			imgproc_Imgproc_distanceTransformWithLabels_11(src.nativeObj, dst.nativeObj, labels.nativeObj, distanceType, maskSize);
		}

		public static void distanceTransform(Mat src, Mat dst, int distanceType, int maskSize, int dstType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_distanceTransform_10(src.nativeObj, dst.nativeObj, distanceType, maskSize, dstType);
		}

		public static void distanceTransform(Mat src, Mat dst, int distanceType, int maskSize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_distanceTransform_11(src.nativeObj, dst.nativeObj, distanceType, maskSize);
		}

		public static void drawContours(Mat image, List<MatOfPoint> contours, int contourIdx, Scalar color, int thickness, int lineType, Mat hierarchy, int maxLevel, Point offset)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (hierarchy != null)
			{
				hierarchy.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((contours != null) ? contours.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(contours, mats);
			imgproc_Imgproc_drawContours_10(image.nativeObj, mat.nativeObj, contourIdx, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType, hierarchy.nativeObj, maxLevel, offset.x, offset.y);
		}

		public static void drawContours(Mat image, List<MatOfPoint> contours, int contourIdx, Scalar color, int thickness)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((contours != null) ? contours.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(contours, mats);
			imgproc_Imgproc_drawContours_11(image.nativeObj, mat.nativeObj, contourIdx, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void drawContours(Mat image, List<MatOfPoint> contours, int contourIdx, Scalar color)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((contours != null) ? contours.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(contours, mats);
			imgproc_Imgproc_drawContours_12(image.nativeObj, mat.nativeObj, contourIdx, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void drawMarker(Mat img, Point position, Scalar color, int markerType, int markerSize, int thickness, int line_type)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_drawMarker_10(img.nativeObj, position.x, position.y, color.val[0], color.val[1], color.val[2], color.val[3], markerType, markerSize, thickness, line_type);
		}

		public static void drawMarker(Mat img, Point position, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_drawMarker_11(img.nativeObj, position.x, position.y, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void ellipse(Mat img, Point center, Size axes, double angle, double startAngle, double endAngle, Scalar color, int thickness, int lineType, int shift)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_ellipse_10(img.nativeObj, center.x, center.y, axes.width, axes.height, angle, startAngle, endAngle, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType, shift);
		}

		public static void ellipse(Mat img, Point center, Size axes, double angle, double startAngle, double endAngle, Scalar color, int thickness)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_ellipse_11(img.nativeObj, center.x, center.y, axes.width, axes.height, angle, startAngle, endAngle, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void ellipse(Mat img, Point center, Size axes, double angle, double startAngle, double endAngle, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_ellipse_12(img.nativeObj, center.x, center.y, axes.width, axes.height, angle, startAngle, endAngle, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void ellipse(Mat img, RotatedRect box, Scalar color, int thickness, int lineType)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_ellipse_13(img.nativeObj, box.center.x, box.center.y, box.size.width, box.size.height, box.angle, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType);
		}

		public static void ellipse(Mat img, RotatedRect box, Scalar color, int thickness)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_ellipse_14(img.nativeObj, box.center.x, box.center.y, box.size.width, box.size.height, box.angle, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void ellipse(Mat img, RotatedRect box, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_ellipse_15(img.nativeObj, box.center.x, box.center.y, box.size.width, box.size.height, box.angle, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void ellipse2Poly(Point center, Size axes, int angle, int arcStart, int arcEnd, int delta, MatOfPoint pts)
		{
			if (pts != null)
			{
				pts.ThrowIfDisposed();
			}
			imgproc_Imgproc_ellipse2Poly_10(center.x, center.y, axes.width, axes.height, angle, arcStart, arcEnd, delta, pts.nativeObj);
		}

		public static void equalizeHist(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_equalizeHist_10(src.nativeObj, dst.nativeObj);
		}

		public static void erode(Mat src, Mat dst, Mat kernel, Point anchor, int iterations, int borderType, Scalar borderValue)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_erode_10(src.nativeObj, dst.nativeObj, kernel.nativeObj, anchor.x, anchor.y, iterations, borderType, borderValue.val[0], borderValue.val[1], borderValue.val[2], borderValue.val[3]);
		}

		public static void erode(Mat src, Mat dst, Mat kernel, Point anchor, int iterations)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_erode_11(src.nativeObj, dst.nativeObj, kernel.nativeObj, anchor.x, anchor.y, iterations);
		}

		public static void erode(Mat src, Mat dst, Mat kernel)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_erode_12(src.nativeObj, dst.nativeObj, kernel.nativeObj);
		}

		public static void fillConvexPoly(Mat img, MatOfPoint points, Scalar color, int lineType, int shift)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			imgproc_Imgproc_fillConvexPoly_10(img.nativeObj, points.nativeObj, color.val[0], color.val[1], color.val[2], color.val[3], lineType, shift);
		}

		public static void fillConvexPoly(Mat img, MatOfPoint points, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			imgproc_Imgproc_fillConvexPoly_11(img.nativeObj, points.nativeObj, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void fillPoly(Mat img, List<MatOfPoint> pts, Scalar color, int lineType, int shift, Point offset)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((pts != null) ? pts.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(pts, mats);
			imgproc_Imgproc_fillPoly_10(img.nativeObj, mat.nativeObj, color.val[0], color.val[1], color.val[2], color.val[3], lineType, shift, offset.x, offset.y);
		}

		public static void fillPoly(Mat img, List<MatOfPoint> pts, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((pts != null) ? pts.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(pts, mats);
			imgproc_Imgproc_fillPoly_11(img.nativeObj, mat.nativeObj, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void filter2D(Mat src, Mat dst, int ddepth, Mat kernel, Point anchor, double delta, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_filter2D_10(src.nativeObj, dst.nativeObj, ddepth, kernel.nativeObj, anchor.x, anchor.y, delta, borderType);
		}

		public static void filter2D(Mat src, Mat dst, int ddepth, Mat kernel, Point anchor, double delta)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_filter2D_11(src.nativeObj, dst.nativeObj, ddepth, kernel.nativeObj, anchor.x, anchor.y, delta);
		}

		public static void filter2D(Mat src, Mat dst, int ddepth, Mat kernel)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_filter2D_12(src.nativeObj, dst.nativeObj, ddepth, kernel.nativeObj);
		}

		public static void findContours(Mat image, List<MatOfPoint> contours, Mat hierarchy, int mode, int method, Point offset)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (hierarchy != null)
			{
				hierarchy.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			imgproc_Imgproc_findContours_10(image.nativeObj, mat.nativeObj, hierarchy.nativeObj, mode, method, offset.x, offset.y);
			Converters.Mat_to_vector_vector_Point(mat, contours);
			mat.release();
		}

		public static void findContours(Mat image, List<MatOfPoint> contours, Mat hierarchy, int mode, int method)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (hierarchy != null)
			{
				hierarchy.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			imgproc_Imgproc_findContours_11(image.nativeObj, mat.nativeObj, hierarchy.nativeObj, mode, method);
			Converters.Mat_to_vector_vector_Point(mat, contours);
			mat.release();
		}

		public static void fitLine(Mat points, Mat line, int distType, double param, double reps, double aeps)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			if (line != null)
			{
				line.ThrowIfDisposed();
			}
			imgproc_Imgproc_fitLine_10(points.nativeObj, line.nativeObj, distType, param, reps, aeps);
		}

		public static void getDerivKernels(Mat kx, Mat ky, int dx, int dy, int ksize, bool normalize, int ktype)
		{
			if (kx != null)
			{
				kx.ThrowIfDisposed();
			}
			if (ky != null)
			{
				ky.ThrowIfDisposed();
			}
			imgproc_Imgproc_getDerivKernels_10(kx.nativeObj, ky.nativeObj, dx, dy, ksize, normalize, ktype);
		}

		public static void getDerivKernels(Mat kx, Mat ky, int dx, int dy, int ksize)
		{
			if (kx != null)
			{
				kx.ThrowIfDisposed();
			}
			if (ky != null)
			{
				ky.ThrowIfDisposed();
			}
			imgproc_Imgproc_getDerivKernels_11(kx.nativeObj, ky.nativeObj, dx, dy, ksize);
		}

		public static void getRectSubPix(Mat image, Size patchSize, Point center, Mat patch, int patchType)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (patch != null)
			{
				patch.ThrowIfDisposed();
			}
			imgproc_Imgproc_getRectSubPix_10(image.nativeObj, patchSize.width, patchSize.height, center.x, center.y, patch.nativeObj, patchType);
		}

		public static void getRectSubPix(Mat image, Size patchSize, Point center, Mat patch)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (patch != null)
			{
				patch.ThrowIfDisposed();
			}
			imgproc_Imgproc_getRectSubPix_11(image.nativeObj, patchSize.width, patchSize.height, center.x, center.y, patch.nativeObj);
		}

		public static void goodFeaturesToTrack(Mat image, MatOfPoint corners, int maxCorners, double qualityLevel, double minDistance, Mat mask, int blockSize, bool useHarrisDetector, double k)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (corners != null)
			{
				corners.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			imgproc_Imgproc_goodFeaturesToTrack_10(image.nativeObj, corners.nativeObj, maxCorners, qualityLevel, minDistance, mask.nativeObj, blockSize, useHarrisDetector, k);
		}

		public static void goodFeaturesToTrack(Mat image, MatOfPoint corners, int maxCorners, double qualityLevel, double minDistance)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (corners != null)
			{
				corners.ThrowIfDisposed();
			}
			imgproc_Imgproc_goodFeaturesToTrack_11(image.nativeObj, corners.nativeObj, maxCorners, qualityLevel, minDistance);
		}

		public static void grabCut(Mat img, Mat mask, Rect rect, Mat bgdModel, Mat fgdModel, int iterCount, int mode)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (bgdModel != null)
			{
				bgdModel.ThrowIfDisposed();
			}
			if (fgdModel != null)
			{
				fgdModel.ThrowIfDisposed();
			}
			imgproc_Imgproc_grabCut_10(img.nativeObj, mask.nativeObj, rect.x, rect.y, rect.width, rect.height, bgdModel.nativeObj, fgdModel.nativeObj, iterCount, mode);
		}

		public static void grabCut(Mat img, Mat mask, Rect rect, Mat bgdModel, Mat fgdModel, int iterCount)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (bgdModel != null)
			{
				bgdModel.ThrowIfDisposed();
			}
			if (fgdModel != null)
			{
				fgdModel.ThrowIfDisposed();
			}
			imgproc_Imgproc_grabCut_11(img.nativeObj, mask.nativeObj, rect.x, rect.y, rect.width, rect.height, bgdModel.nativeObj, fgdModel.nativeObj, iterCount);
		}

		public static void initUndistortRectifyMap(Mat cameraMatrix, Mat distCoeffs, Mat R, Mat newCameraMatrix, Size size, int m1type, Mat map1, Mat map2)
		{
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (newCameraMatrix != null)
			{
				newCameraMatrix.ThrowIfDisposed();
			}
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			imgproc_Imgproc_initUndistortRectifyMap_10(cameraMatrix.nativeObj, distCoeffs.nativeObj, R.nativeObj, newCameraMatrix.nativeObj, size.width, size.height, m1type, map1.nativeObj, map2.nativeObj);
		}

		public static void integral3(Mat src, Mat sum, Mat sqsum, Mat tilted, int sdepth, int sqdepth)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (sum != null)
			{
				sum.ThrowIfDisposed();
			}
			if (sqsum != null)
			{
				sqsum.ThrowIfDisposed();
			}
			if (tilted != null)
			{
				tilted.ThrowIfDisposed();
			}
			imgproc_Imgproc_integral3_10(src.nativeObj, sum.nativeObj, sqsum.nativeObj, tilted.nativeObj, sdepth, sqdepth);
		}

		public static void integral3(Mat src, Mat sum, Mat sqsum, Mat tilted)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (sum != null)
			{
				sum.ThrowIfDisposed();
			}
			if (sqsum != null)
			{
				sqsum.ThrowIfDisposed();
			}
			if (tilted != null)
			{
				tilted.ThrowIfDisposed();
			}
			imgproc_Imgproc_integral3_11(src.nativeObj, sum.nativeObj, sqsum.nativeObj, tilted.nativeObj);
		}

		public static void integral2(Mat src, Mat sum, Mat sqsum, int sdepth, int sqdepth)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (sum != null)
			{
				sum.ThrowIfDisposed();
			}
			if (sqsum != null)
			{
				sqsum.ThrowIfDisposed();
			}
			imgproc_Imgproc_integral2_10(src.nativeObj, sum.nativeObj, sqsum.nativeObj, sdepth, sqdepth);
		}

		public static void integral2(Mat src, Mat sum, Mat sqsum)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (sum != null)
			{
				sum.ThrowIfDisposed();
			}
			if (sqsum != null)
			{
				sqsum.ThrowIfDisposed();
			}
			imgproc_Imgproc_integral2_11(src.nativeObj, sum.nativeObj, sqsum.nativeObj);
		}

		public static void integral(Mat src, Mat sum, int sdepth)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (sum != null)
			{
				sum.ThrowIfDisposed();
			}
			imgproc_Imgproc_integral_10(src.nativeObj, sum.nativeObj, sdepth);
		}

		public static void integral(Mat src, Mat sum)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (sum != null)
			{
				sum.ThrowIfDisposed();
			}
			imgproc_Imgproc_integral_11(src.nativeObj, sum.nativeObj);
		}

		public static void invertAffineTransform(Mat M, Mat iM)
		{
			if (M != null)
			{
				M.ThrowIfDisposed();
			}
			if (iM != null)
			{
				iM.ThrowIfDisposed();
			}
			imgproc_Imgproc_invertAffineTransform_10(M.nativeObj, iM.nativeObj);
		}

		public static void line(Mat img, Point pt1, Point pt2, Scalar color, int thickness, int lineType, int shift)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_line_10(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType, shift);
		}

		public static void line(Mat img, Point pt1, Point pt2, Scalar color, int thickness)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_line_11(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void line(Mat img, Point pt1, Point pt2, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_line_12(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void linearPolar(Mat src, Mat dst, Point center, double maxRadius, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_linearPolar_10(src.nativeObj, dst.nativeObj, center.x, center.y, maxRadius, flags);
		}

		public static void logPolar(Mat src, Mat dst, Point center, double M, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_logPolar_10(src.nativeObj, dst.nativeObj, center.x, center.y, M, flags);
		}

		public static void matchTemplate(Mat image, Mat templ, Mat result, int method, Mat mask)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (templ != null)
			{
				templ.ThrowIfDisposed();
			}
			if (result != null)
			{
				result.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			imgproc_Imgproc_matchTemplate_10(image.nativeObj, templ.nativeObj, result.nativeObj, method, mask.nativeObj);
		}

		public static void matchTemplate(Mat image, Mat templ, Mat result, int method)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (templ != null)
			{
				templ.ThrowIfDisposed();
			}
			if (result != null)
			{
				result.ThrowIfDisposed();
			}
			imgproc_Imgproc_matchTemplate_11(image.nativeObj, templ.nativeObj, result.nativeObj, method);
		}

		public static void medianBlur(Mat src, Mat dst, int ksize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_medianBlur_10(src.nativeObj, dst.nativeObj, ksize);
		}

		public static void minEnclosingCircle(MatOfPoint2f points, Point center, float[] radius)
		{
			if (points != null)
			{
				points.ThrowIfDisposed();
			}
			double[] array = new double[2];
			double[] array2 = new double[1];
			imgproc_Imgproc_minEnclosingCircle_10(points.nativeObj, array, array2);
			if (center != null)
			{
				center.x = array[0];
				center.y = array[1];
			}
			if (radius != null)
			{
				radius[0] = (float)array2[0];
			}
		}

		public static void morphologyEx(Mat src, Mat dst, int op, Mat kernel, Point anchor, int iterations, int borderType, Scalar borderValue)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_morphologyEx_10(src.nativeObj, dst.nativeObj, op, kernel.nativeObj, anchor.x, anchor.y, iterations, borderType, borderValue.val[0], borderValue.val[1], borderValue.val[2], borderValue.val[3]);
		}

		public static void morphologyEx(Mat src, Mat dst, int op, Mat kernel, Point anchor, int iterations)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_morphologyEx_11(src.nativeObj, dst.nativeObj, op, kernel.nativeObj, anchor.x, anchor.y, iterations);
		}

		public static void morphologyEx(Mat src, Mat dst, int op, Mat kernel)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernel != null)
			{
				kernel.ThrowIfDisposed();
			}
			imgproc_Imgproc_morphologyEx_12(src.nativeObj, dst.nativeObj, op, kernel.nativeObj);
		}

		public static void polylines(Mat img, List<MatOfPoint> pts, bool isClosed, Scalar color, int thickness, int lineType, int shift)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((pts != null) ? pts.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(pts, mats);
			imgproc_Imgproc_polylines_10(img.nativeObj, mat.nativeObj, isClosed, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType, shift);
		}

		public static void polylines(Mat img, List<MatOfPoint> pts, bool isClosed, Scalar color, int thickness)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((pts != null) ? pts.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(pts, mats);
			imgproc_Imgproc_polylines_11(img.nativeObj, mat.nativeObj, isClosed, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void polylines(Mat img, List<MatOfPoint> pts, bool isClosed, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			List<Mat> mats = new List<Mat>((pts != null) ? pts.Count : 0);
			Mat mat = Converters.vector_vector_Point_to_Mat(pts, mats);
			imgproc_Imgproc_polylines_12(img.nativeObj, mat.nativeObj, isClosed, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void preCornerDetect(Mat src, Mat dst, int ksize, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_preCornerDetect_10(src.nativeObj, dst.nativeObj, ksize, borderType);
		}

		public static void preCornerDetect(Mat src, Mat dst, int ksize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_preCornerDetect_11(src.nativeObj, dst.nativeObj, ksize);
		}

		public static void putText(Mat img, string text, Point org, int fontFace, double fontScale, Scalar color, int thickness, int lineType, bool bottomLeftOrigin)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_putText_10(img.nativeObj, text, org.x, org.y, fontFace, fontScale, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType, bottomLeftOrigin);
		}

		public static void putText(Mat img, string text, Point org, int fontFace, double fontScale, Scalar color, int thickness)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_putText_11(img.nativeObj, text, org.x, org.y, fontFace, fontScale, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void putText(Mat img, string text, Point org, int fontFace, double fontScale, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_putText_12(img.nativeObj, text, org.x, org.y, fontFace, fontScale, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void pyrDown(Mat src, Mat dst, Size dstsize, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrDown_10(src.nativeObj, dst.nativeObj, dstsize.width, dstsize.height, borderType);
		}

		public static void pyrDown(Mat src, Mat dst, Size dstsize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrDown_11(src.nativeObj, dst.nativeObj, dstsize.width, dstsize.height);
		}

		public static void pyrDown(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrDown_12(src.nativeObj, dst.nativeObj);
		}

		public static void pyrMeanShiftFiltering(Mat src, Mat dst, double sp, double sr, int maxLevel, TermCriteria termcrit)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrMeanShiftFiltering_10(src.nativeObj, dst.nativeObj, sp, sr, maxLevel, termcrit.type, termcrit.maxCount, termcrit.epsilon);
		}

		public static void pyrMeanShiftFiltering(Mat src, Mat dst, double sp, double sr)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrMeanShiftFiltering_11(src.nativeObj, dst.nativeObj, sp, sr);
		}

		public static void pyrUp(Mat src, Mat dst, Size dstsize, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrUp_10(src.nativeObj, dst.nativeObj, dstsize.width, dstsize.height, borderType);
		}

		public static void pyrUp(Mat src, Mat dst, Size dstsize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrUp_11(src.nativeObj, dst.nativeObj, dstsize.width, dstsize.height);
		}

		public static void pyrUp(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_pyrUp_12(src.nativeObj, dst.nativeObj);
		}

		public static void rectangle(Mat img, Point pt1, Point pt2, Scalar color, int thickness, int lineType, int shift)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_rectangle_10(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3], thickness, lineType, shift);
		}

		public static void rectangle(Mat img, Point pt1, Point pt2, Scalar color, int thickness)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_rectangle_11(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3], thickness);
		}

		public static void rectangle(Mat img, Point pt1, Point pt2, Scalar color)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			imgproc_Imgproc_rectangle_12(img.nativeObj, pt1.x, pt1.y, pt2.x, pt2.y, color.val[0], color.val[1], color.val[2], color.val[3]);
		}

		public static void remap(Mat src, Mat dst, Mat map1, Mat map2, int interpolation, int borderMode, Scalar borderValue)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			imgproc_Imgproc_remap_10(src.nativeObj, dst.nativeObj, map1.nativeObj, map2.nativeObj, interpolation, borderMode, borderValue.val[0], borderValue.val[1], borderValue.val[2], borderValue.val[3]);
		}

		public static void remap(Mat src, Mat dst, Mat map1, Mat map2, int interpolation)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (map1 != null)
			{
				map1.ThrowIfDisposed();
			}
			if (map2 != null)
			{
				map2.ThrowIfDisposed();
			}
			imgproc_Imgproc_remap_11(src.nativeObj, dst.nativeObj, map1.nativeObj, map2.nativeObj, interpolation);
		}

		public static void resize(Mat src, Mat dst, Size dsize, double fx, double fy, int interpolation)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_resize_10(src.nativeObj, dst.nativeObj, dsize.width, dsize.height, fx, fy, interpolation);
		}

		public static void resize(Mat src, Mat dst, Size dsize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_resize_11(src.nativeObj, dst.nativeObj, dsize.width, dsize.height);
		}

		public static void sepFilter2D(Mat src, Mat dst, int ddepth, Mat kernelX, Mat kernelY, Point anchor, double delta, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernelX != null)
			{
				kernelX.ThrowIfDisposed();
			}
			if (kernelY != null)
			{
				kernelY.ThrowIfDisposed();
			}
			imgproc_Imgproc_sepFilter2D_10(src.nativeObj, dst.nativeObj, ddepth, kernelX.nativeObj, kernelY.nativeObj, anchor.x, anchor.y, delta, borderType);
		}

		public static void sepFilter2D(Mat src, Mat dst, int ddepth, Mat kernelX, Mat kernelY, Point anchor, double delta)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernelX != null)
			{
				kernelX.ThrowIfDisposed();
			}
			if (kernelY != null)
			{
				kernelY.ThrowIfDisposed();
			}
			imgproc_Imgproc_sepFilter2D_11(src.nativeObj, dst.nativeObj, ddepth, kernelX.nativeObj, kernelY.nativeObj, anchor.x, anchor.y, delta);
		}

		public static void sepFilter2D(Mat src, Mat dst, int ddepth, Mat kernelX, Mat kernelY)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (kernelX != null)
			{
				kernelX.ThrowIfDisposed();
			}
			if (kernelY != null)
			{
				kernelY.ThrowIfDisposed();
			}
			imgproc_Imgproc_sepFilter2D_12(src.nativeObj, dst.nativeObj, ddepth, kernelX.nativeObj, kernelY.nativeObj);
		}

		public static void spatialGradient(Mat src, Mat dx, Mat dy, int ksize, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dx != null)
			{
				dx.ThrowIfDisposed();
			}
			if (dy != null)
			{
				dy.ThrowIfDisposed();
			}
			imgproc_Imgproc_spatialGradient_10(src.nativeObj, dx.nativeObj, dy.nativeObj, ksize, borderType);
		}

		public static void spatialGradient(Mat src, Mat dx, Mat dy, int ksize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dx != null)
			{
				dx.ThrowIfDisposed();
			}
			if (dy != null)
			{
				dy.ThrowIfDisposed();
			}
			imgproc_Imgproc_spatialGradient_11(src.nativeObj, dx.nativeObj, dy.nativeObj, ksize);
		}

		public static void spatialGradient(Mat src, Mat dx, Mat dy)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dx != null)
			{
				dx.ThrowIfDisposed();
			}
			if (dy != null)
			{
				dy.ThrowIfDisposed();
			}
			imgproc_Imgproc_spatialGradient_12(src.nativeObj, dx.nativeObj, dy.nativeObj);
		}

		public static void sqrBoxFilter(Mat _src, Mat _dst, int ddepth, Size ksize, Point anchor, bool normalize, int borderType)
		{
			if (_src != null)
			{
				_src.ThrowIfDisposed();
			}
			if (_dst != null)
			{
				_dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_sqrBoxFilter_10(_src.nativeObj, _dst.nativeObj, ddepth, ksize.width, ksize.height, anchor.x, anchor.y, normalize, borderType);
		}

		public static void sqrBoxFilter(Mat _src, Mat _dst, int ddepth, Size ksize, Point anchor, bool normalize)
		{
			if (_src != null)
			{
				_src.ThrowIfDisposed();
			}
			if (_dst != null)
			{
				_dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_sqrBoxFilter_11(_src.nativeObj, _dst.nativeObj, ddepth, ksize.width, ksize.height, anchor.x, anchor.y, normalize);
		}

		public static void sqrBoxFilter(Mat _src, Mat _dst, int ddepth, Size ksize)
		{
			if (_src != null)
			{
				_src.ThrowIfDisposed();
			}
			if (_dst != null)
			{
				_dst.ThrowIfDisposed();
			}
			imgproc_Imgproc_sqrBoxFilter_12(_src.nativeObj, _dst.nativeObj, ddepth, ksize.width, ksize.height);
		}

		public static void undistort(Mat src, Mat dst, Mat cameraMatrix, Mat distCoeffs, Mat newCameraMatrix)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (newCameraMatrix != null)
			{
				newCameraMatrix.ThrowIfDisposed();
			}
			imgproc_Imgproc_undistort_10(src.nativeObj, dst.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, newCameraMatrix.nativeObj);
		}

		public static void undistort(Mat src, Mat dst, Mat cameraMatrix, Mat distCoeffs)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			imgproc_Imgproc_undistort_11(src.nativeObj, dst.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj);
		}

		public static void undistortPoints(MatOfPoint2f src, MatOfPoint2f dst, Mat cameraMatrix, Mat distCoeffs, Mat R, Mat P)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			if (R != null)
			{
				R.ThrowIfDisposed();
			}
			if (P != null)
			{
				P.ThrowIfDisposed();
			}
			imgproc_Imgproc_undistortPoints_10(src.nativeObj, dst.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj, R.nativeObj, P.nativeObj);
		}

		public static void undistortPoints(MatOfPoint2f src, MatOfPoint2f dst, Mat cameraMatrix, Mat distCoeffs)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (cameraMatrix != null)
			{
				cameraMatrix.ThrowIfDisposed();
			}
			if (distCoeffs != null)
			{
				distCoeffs.ThrowIfDisposed();
			}
			imgproc_Imgproc_undistortPoints_11(src.nativeObj, dst.nativeObj, cameraMatrix.nativeObj, distCoeffs.nativeObj);
		}

		public static void warpAffine(Mat src, Mat dst, Mat M, Size dsize, int flags, int borderMode, Scalar borderValue)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (M != null)
			{
				M.ThrowIfDisposed();
			}
			imgproc_Imgproc_warpAffine_10(src.nativeObj, dst.nativeObj, M.nativeObj, dsize.width, dsize.height, flags, borderMode, borderValue.val[0], borderValue.val[1], borderValue.val[2], borderValue.val[3]);
		}

		public static void warpAffine(Mat src, Mat dst, Mat M, Size dsize, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (M != null)
			{
				M.ThrowIfDisposed();
			}
			imgproc_Imgproc_warpAffine_11(src.nativeObj, dst.nativeObj, M.nativeObj, dsize.width, dsize.height, flags);
		}

		public static void warpAffine(Mat src, Mat dst, Mat M, Size dsize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (M != null)
			{
				M.ThrowIfDisposed();
			}
			imgproc_Imgproc_warpAffine_12(src.nativeObj, dst.nativeObj, M.nativeObj, dsize.width, dsize.height);
		}

		public static void warpPerspective(Mat src, Mat dst, Mat M, Size dsize, int flags, int borderMode, Scalar borderValue)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (M != null)
			{
				M.ThrowIfDisposed();
			}
			imgproc_Imgproc_warpPerspective_10(src.nativeObj, dst.nativeObj, M.nativeObj, dsize.width, dsize.height, flags, borderMode, borderValue.val[0], borderValue.val[1], borderValue.val[2], borderValue.val[3]);
		}

		public static void warpPerspective(Mat src, Mat dst, Mat M, Size dsize, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (M != null)
			{
				M.ThrowIfDisposed();
			}
			imgproc_Imgproc_warpPerspective_11(src.nativeObj, dst.nativeObj, M.nativeObj, dsize.width, dsize.height, flags);
		}

		public static void warpPerspective(Mat src, Mat dst, Mat M, Size dsize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (M != null)
			{
				M.ThrowIfDisposed();
			}
			imgproc_Imgproc_warpPerspective_12(src.nativeObj, dst.nativeObj, M.nativeObj, dsize.width, dsize.height);
		}

		public static void watershed(Mat image, Mat markers)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			if (markers != null)
			{
				markers.ThrowIfDisposed();
			}
			imgproc_Imgproc_watershed_10(image.nativeObj, markers.nativeObj);
		}

		public static Size getTextSize(string text, int fontFace, double fontScale, int thickness, int[] baseLine)
		{
			if (baseLine != null && baseLine.Length != 1)
			{
				throw new CvException("'baseLine' must be 'int[1]' or 'null'.");
			}
			double[] vals = new double[2];
			imgproc_Imgproc_n_1getTextSize(text, fontFace, fontScale, thickness, baseLine, vals);
			return new Size(vals);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getAffineTransform_10(IntPtr src_mat_nativeObj, IntPtr dst_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getDefaultNewCameraMatrix_10(IntPtr cameraMatrix_nativeObj, double imgsize_width, double imgsize_height, bool centerPrincipalPoint);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getDefaultNewCameraMatrix_11(IntPtr cameraMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getGaborKernel_10(double ksize_width, double ksize_height, double sigma, double theta, double lambd, double gamma, double psi, int ktype);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getGaborKernel_11(double ksize_width, double ksize_height, double sigma, double theta, double lambd, double gamma);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getGaussianKernel_10(int ksize, double sigma, int ktype);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getGaussianKernel_11(int ksize, double sigma);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getPerspectiveTransform_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getRotationMatrix2D_10(double center_x, double center_y, double angle, double scale);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getStructuringElement_10(int shape, double ksize_width, double ksize_height, double anchor_x, double anchor_y);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_getStructuringElement_11(int shape, double ksize_width, double ksize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_moments_10(IntPtr array_nativeObj, bool binaryImage, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_moments_11(IntPtr array_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_phaseCorrelate_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr window_nativeObj, double[] response_out, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_phaseCorrelate_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_createCLAHE_10(double clipLimit, double tileGridSize_width, double tileGridSize_height);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_createCLAHE_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_createLineSegmentDetector_10(int _refine, double _scale, double _sigma_scale, double _quant, double _ang_th, double _log_eps, double _density_th, int _n_bins);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Imgproc_createLineSegmentDetector_11();

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_boundingRect_10(IntPtr points_mat_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_fitEllipse_10(IntPtr points_mat_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_minAreaRect_10(IntPtr points_mat_nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern bool imgproc_Imgproc_clipLine_10(int imgRect_x, int imgRect_y, int imgRect_width, int imgRect_height, double pt1_x, double pt1_y, double[] pt1_out, double pt2_x, double pt2_y, double[] pt2_out);

		[DllImport("opencvforunity")]
		private static extern bool imgproc_Imgproc_isContourConvex_10(IntPtr contour_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_arcLength_10(IntPtr curve_mat_nativeObj, bool closed);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_compareHist_10(IntPtr H1_nativeObj, IntPtr H2_nativeObj, int method);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_contourArea_10(IntPtr contour_nativeObj, bool oriented);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_contourArea_11(IntPtr contour_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_matchShapes_10(IntPtr contour1_nativeObj, IntPtr contour2_nativeObj, int method, double parameter);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_minEnclosingTriangle_10(IntPtr points_nativeObj, IntPtr triangle_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_pointPolygonTest_10(IntPtr contour_mat_nativeObj, double pt_x, double pt_y, bool measureDist);

		[DllImport("opencvforunity")]
		private static extern double imgproc_Imgproc_threshold_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double thresh, double maxval, int type);

		[DllImport("opencvforunity")]
		private static extern float imgproc_Imgproc_initWideAngleProjMap_10(IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, double imageSize_width, double imageSize_height, int destImageWidth, int m1type, IntPtr map1_nativeObj, IntPtr map2_nativeObj, int projType, double alpha);

		[DllImport("opencvforunity")]
		private static extern float imgproc_Imgproc_initWideAngleProjMap_11(IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, double imageSize_width, double imageSize_height, int destImageWidth, int m1type, IntPtr map1_nativeObj, IntPtr map2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern float imgproc_Imgproc_intersectConvexConvex_10(IntPtr _p1_nativeObj, IntPtr _p2_nativeObj, IntPtr _p12_nativeObj, bool handleNested);

		[DllImport("opencvforunity")]
		private static extern float imgproc_Imgproc_intersectConvexConvex_11(IntPtr _p1_nativeObj, IntPtr _p2_nativeObj, IntPtr _p12_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Imgproc_connectedComponents_10(IntPtr image_nativeObj, IntPtr labels_nativeObj, int connectivity, int ltype);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Imgproc_connectedComponents_11(IntPtr image_nativeObj, IntPtr labels_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Imgproc_connectedComponentsWithStats_10(IntPtr image_nativeObj, IntPtr labels_nativeObj, IntPtr stats_nativeObj, IntPtr centroids_nativeObj, int connectivity, int ltype);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Imgproc_connectedComponentsWithStats_11(IntPtr image_nativeObj, IntPtr labels_nativeObj, IntPtr stats_nativeObj, IntPtr centroids_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Imgproc_floodFill_10(IntPtr image_nativeObj, IntPtr mask_nativeObj, double seedPoint_x, double seedPoint_y, double newVal_val0, double newVal_val1, double newVal_val2, double newVal_val3, double[] rect_out, double loDiff_val0, double loDiff_val1, double loDiff_val2, double loDiff_val3, double upDiff_val0, double upDiff_val1, double upDiff_val2, double upDiff_val3, int flags);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Imgproc_floodFill_11(IntPtr image_nativeObj, IntPtr mask_nativeObj, double seedPoint_x, double seedPoint_y, double newVal_val0, double newVal_val1, double newVal_val2, double newVal_val3);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Imgproc_rotatedRectangleIntersection_10(double rect1_center_x, double rect1_center_y, double rect1_size_width, double rect1_size_height, double rect1_angle, double rect2_center_x, double rect2_center_y, double rect2_size_width, double rect2_size_height, double rect2_angle, IntPtr intersectingRegion_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Canny_10(IntPtr image_nativeObj, IntPtr edges_nativeObj, double threshold1, double threshold2, int apertureSize, bool L2gradient);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Canny_11(IntPtr image_nativeObj, IntPtr edges_nativeObj, double threshold1, double threshold2);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_GaussianBlur_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double ksize_width, double ksize_height, double sigmaX, double sigmaY, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_GaussianBlur_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double ksize_width, double ksize_height, double sigmaX, double sigmaY);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_GaussianBlur_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, double ksize_width, double ksize_height, double sigmaX);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_HoughCircles_10(IntPtr image_nativeObj, IntPtr circles_nativeObj, int method, double dp, double minDist, double param1, double param2, int minRadius, int maxRadius);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_HoughCircles_11(IntPtr image_nativeObj, IntPtr circles_nativeObj, int method, double dp, double minDist);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_HoughLines_10(IntPtr image_nativeObj, IntPtr lines_nativeObj, double rho, double theta, int threshold, double srn, double stn, double min_theta, double max_theta);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_HoughLines_11(IntPtr image_nativeObj, IntPtr lines_nativeObj, double rho, double theta, int threshold);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_HoughLinesP_10(IntPtr image_nativeObj, IntPtr lines_nativeObj, double rho, double theta, int threshold, double minLineLength, double maxLineGap);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_HoughLinesP_11(IntPtr image_nativeObj, IntPtr lines_nativeObj, double rho, double theta, int threshold);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_HuMoments_10(double m_m00, double m_m10, double m_m01, double m_m20, double m_m11, double m_m02, double m_m30, double m_m21, double m_m12, double m_m03, IntPtr hu_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Laplacian_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int ksize, double scale, double delta, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Laplacian_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int ksize, double scale, double delta);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Laplacian_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Scharr_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int dx, int dy, double scale, double delta, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Scharr_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int dx, int dy, double scale, double delta);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Scharr_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int dx, int dy);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Sobel_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int dx, int dy, int ksize, double scale, double delta, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Sobel_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int dx, int dy, int ksize, double scale, double delta);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_Sobel_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, int dx, int dy);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulate_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulate_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulateProduct_10(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulateProduct_11(IntPtr src1_nativeObj, IntPtr src2_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulateSquare_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulateSquare_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulateWeighted_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double alpha, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_accumulateWeighted_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double alpha);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_adaptiveThreshold_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double maxValue, int adaptiveMethod, int thresholdType, int blockSize, double C);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_applyColorMap_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int colormap);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_approxPolyDP_10(IntPtr curve_mat_nativeObj, IntPtr approxCurve_mat_nativeObj, double epsilon, bool closed);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_arrowedLine_10(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int line_type, int shift, double tipLength);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_arrowedLine_11(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_bilateralFilter_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int d, double sigmaColor, double sigmaSpace, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_bilateralFilter_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int d, double sigmaColor, double sigmaSpace);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_blur_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double ksize_width, double ksize_height, double anchor_x, double anchor_y, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_blur_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double ksize_width, double ksize_height, double anchor_x, double anchor_y);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_blur_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, double ksize_width, double ksize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_boxFilter_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, double ksize_width, double ksize_height, double anchor_x, double anchor_y, bool normalize, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_boxFilter_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, double ksize_width, double ksize_height, double anchor_x, double anchor_y, bool normalize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_boxFilter_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, double ksize_width, double ksize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_boxPoints_10(double box_center_x, double box_center_y, double box_size_width, double box_size_height, double box_angle, IntPtr points_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_calcBackProject_10(IntPtr images_mat_nativeObj, IntPtr channels_mat_nativeObj, IntPtr hist_nativeObj, IntPtr dst_nativeObj, IntPtr ranges_mat_nativeObj, double scale);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_calcHist_10(IntPtr images_mat_nativeObj, IntPtr channels_mat_nativeObj, IntPtr mask_nativeObj, IntPtr hist_nativeObj, IntPtr histSize_mat_nativeObj, IntPtr ranges_mat_nativeObj, bool accumulate);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_calcHist_11(IntPtr images_mat_nativeObj, IntPtr channels_mat_nativeObj, IntPtr mask_nativeObj, IntPtr hist_nativeObj, IntPtr histSize_mat_nativeObj, IntPtr ranges_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_circle_10(IntPtr img_nativeObj, double center_x, double center_y, int radius, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType, int shift);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_circle_11(IntPtr img_nativeObj, double center_x, double center_y, int radius, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_circle_12(IntPtr img_nativeObj, double center_x, double center_y, int radius, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_convertMaps_10(IntPtr map1_nativeObj, IntPtr map2_nativeObj, IntPtr dstmap1_nativeObj, IntPtr dstmap2_nativeObj, int dstmap1type, bool nninterpolation);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_convertMaps_11(IntPtr map1_nativeObj, IntPtr map2_nativeObj, IntPtr dstmap1_nativeObj, IntPtr dstmap2_nativeObj, int dstmap1type);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_convexHull_10(IntPtr points_mat_nativeObj, IntPtr hull_mat_nativeObj, bool clockwise);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_convexHull_11(IntPtr points_mat_nativeObj, IntPtr hull_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_convexityDefects_10(IntPtr contour_mat_nativeObj, IntPtr convexhull_mat_nativeObj, IntPtr convexityDefects_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerEigenValsAndVecs_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int blockSize, int ksize, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerEigenValsAndVecs_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int blockSize, int ksize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerHarris_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int blockSize, int ksize, double k, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerHarris_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int blockSize, int ksize, double k);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerMinEigenVal_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int blockSize, int ksize, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerMinEigenVal_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int blockSize, int ksize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerMinEigenVal_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int blockSize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cornerSubPix_10(IntPtr image_nativeObj, IntPtr corners_mat_nativeObj, double winSize_width, double winSize_height, double zeroZone_width, double zeroZone_height, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_createHanningWindow_10(IntPtr dst_nativeObj, double winSize_width, double winSize_height, int type);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cvtColor_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int code, int dstCn);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_cvtColor_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int code);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_demosaicing_10(IntPtr _src_nativeObj, IntPtr _dst_nativeObj, int code, int dcn);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_demosaicing_11(IntPtr _src_nativeObj, IntPtr _dst_nativeObj, int code);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_dilate_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, int iterations, int borderType, double borderValue_val0, double borderValue_val1, double borderValue_val2, double borderValue_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_dilate_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, int iterations);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_dilate_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr kernel_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_distanceTransformWithLabels_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr labels_nativeObj, int distanceType, int maskSize, int labelType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_distanceTransformWithLabels_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr labels_nativeObj, int distanceType, int maskSize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_distanceTransform_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int distanceType, int maskSize, int dstType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_distanceTransform_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int distanceType, int maskSize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_drawContours_10(IntPtr image_nativeObj, IntPtr contours_mat_nativeObj, int contourIdx, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType, IntPtr hierarchy_nativeObj, int maxLevel, double offset_x, double offset_y);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_drawContours_11(IntPtr image_nativeObj, IntPtr contours_mat_nativeObj, int contourIdx, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_drawContours_12(IntPtr image_nativeObj, IntPtr contours_mat_nativeObj, int contourIdx, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_drawMarker_10(IntPtr img_nativeObj, double position_x, double position_y, double color_val0, double color_val1, double color_val2, double color_val3, int markerType, int markerSize, int thickness, int line_type);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_drawMarker_11(IntPtr img_nativeObj, double position_x, double position_y, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_ellipse_10(IntPtr img_nativeObj, double center_x, double center_y, double axes_width, double axes_height, double angle, double startAngle, double endAngle, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType, int shift);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_ellipse_11(IntPtr img_nativeObj, double center_x, double center_y, double axes_width, double axes_height, double angle, double startAngle, double endAngle, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_ellipse_12(IntPtr img_nativeObj, double center_x, double center_y, double axes_width, double axes_height, double angle, double startAngle, double endAngle, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_ellipse_13(IntPtr img_nativeObj, double box_center_x, double box_center_y, double box_size_width, double box_size_height, double box_angle, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_ellipse_14(IntPtr img_nativeObj, double box_center_x, double box_center_y, double box_size_width, double box_size_height, double box_angle, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_ellipse_15(IntPtr img_nativeObj, double box_center_x, double box_center_y, double box_size_width, double box_size_height, double box_angle, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_ellipse2Poly_10(double center_x, double center_y, double axes_width, double axes_height, int angle, int arcStart, int arcEnd, int delta, IntPtr pts_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_equalizeHist_10(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_erode_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, int iterations, int borderType, double borderValue_val0, double borderValue_val1, double borderValue_val2, double borderValue_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_erode_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, int iterations);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_erode_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr kernel_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_fillConvexPoly_10(IntPtr img_nativeObj, IntPtr points_mat_nativeObj, double color_val0, double color_val1, double color_val2, double color_val3, int lineType, int shift);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_fillConvexPoly_11(IntPtr img_nativeObj, IntPtr points_mat_nativeObj, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_fillPoly_10(IntPtr img_nativeObj, IntPtr pts_mat_nativeObj, double color_val0, double color_val1, double color_val2, double color_val3, int lineType, int shift, double offset_x, double offset_y);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_fillPoly_11(IntPtr img_nativeObj, IntPtr pts_mat_nativeObj, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_filter2D_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, double delta, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_filter2D_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, double delta);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_filter2D_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, IntPtr kernel_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_findContours_10(IntPtr image_nativeObj, IntPtr contours_mat_nativeObj, IntPtr hierarchy_nativeObj, int mode, int method, double offset_x, double offset_y);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_findContours_11(IntPtr image_nativeObj, IntPtr contours_mat_nativeObj, IntPtr hierarchy_nativeObj, int mode, int method);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_fitLine_10(IntPtr points_nativeObj, IntPtr line_nativeObj, int distType, double param, double reps, double aeps);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_getDerivKernels_10(IntPtr kx_nativeObj, IntPtr ky_nativeObj, int dx, int dy, int ksize, bool normalize, int ktype);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_getDerivKernels_11(IntPtr kx_nativeObj, IntPtr ky_nativeObj, int dx, int dy, int ksize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_getRectSubPix_10(IntPtr image_nativeObj, double patchSize_width, double patchSize_height, double center_x, double center_y, IntPtr patch_nativeObj, int patchType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_getRectSubPix_11(IntPtr image_nativeObj, double patchSize_width, double patchSize_height, double center_x, double center_y, IntPtr patch_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_goodFeaturesToTrack_10(IntPtr image_nativeObj, IntPtr corners_mat_nativeObj, int maxCorners, double qualityLevel, double minDistance, IntPtr mask_nativeObj, int blockSize, bool useHarrisDetector, double k);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_goodFeaturesToTrack_11(IntPtr image_nativeObj, IntPtr corners_mat_nativeObj, int maxCorners, double qualityLevel, double minDistance);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_grabCut_10(IntPtr img_nativeObj, IntPtr mask_nativeObj, int rect_x, int rect_y, int rect_width, int rect_height, IntPtr bgdModel_nativeObj, IntPtr fgdModel_nativeObj, int iterCount, int mode);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_grabCut_11(IntPtr img_nativeObj, IntPtr mask_nativeObj, int rect_x, int rect_y, int rect_width, int rect_height, IntPtr bgdModel_nativeObj, IntPtr fgdModel_nativeObj, int iterCount);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_initUndistortRectifyMap_10(IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, IntPtr R_nativeObj, IntPtr newCameraMatrix_nativeObj, double size_width, double size_height, int m1type, IntPtr map1_nativeObj, IntPtr map2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_integral3_10(IntPtr src_nativeObj, IntPtr sum_nativeObj, IntPtr sqsum_nativeObj, IntPtr tilted_nativeObj, int sdepth, int sqdepth);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_integral3_11(IntPtr src_nativeObj, IntPtr sum_nativeObj, IntPtr sqsum_nativeObj, IntPtr tilted_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_integral2_10(IntPtr src_nativeObj, IntPtr sum_nativeObj, IntPtr sqsum_nativeObj, int sdepth, int sqdepth);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_integral2_11(IntPtr src_nativeObj, IntPtr sum_nativeObj, IntPtr sqsum_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_integral_10(IntPtr src_nativeObj, IntPtr sum_nativeObj, int sdepth);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_integral_11(IntPtr src_nativeObj, IntPtr sum_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_invertAffineTransform_10(IntPtr M_nativeObj, IntPtr iM_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_line_10(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType, int shift);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_line_11(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_line_12(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_linearPolar_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double center_x, double center_y, double maxRadius, int flags);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_logPolar_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double center_x, double center_y, double M, int flags);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_matchTemplate_10(IntPtr image_nativeObj, IntPtr templ_nativeObj, IntPtr result_nativeObj, int method, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_matchTemplate_11(IntPtr image_nativeObj, IntPtr templ_nativeObj, IntPtr result_nativeObj, int method);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_medianBlur_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ksize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_minEnclosingCircle_10(IntPtr points_mat_nativeObj, double[] center_out, double[] radius_out);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_morphologyEx_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int op, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, int iterations, int borderType, double borderValue_val0, double borderValue_val1, double borderValue_val2, double borderValue_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_morphologyEx_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int op, IntPtr kernel_nativeObj, double anchor_x, double anchor_y, int iterations);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_morphologyEx_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int op, IntPtr kernel_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_polylines_10(IntPtr img_nativeObj, IntPtr pts_mat_nativeObj, bool isClosed, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType, int shift);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_polylines_11(IntPtr img_nativeObj, IntPtr pts_mat_nativeObj, bool isClosed, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_polylines_12(IntPtr img_nativeObj, IntPtr pts_mat_nativeObj, bool isClosed, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_preCornerDetect_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ksize, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_preCornerDetect_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ksize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_putText_10(IntPtr img_nativeObj, string text, double org_x, double org_y, int fontFace, double fontScale, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType, bool bottomLeftOrigin);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_putText_11(IntPtr img_nativeObj, string text, double org_x, double org_y, int fontFace, double fontScale, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_putText_12(IntPtr img_nativeObj, string text, double org_x, double org_y, int fontFace, double fontScale, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrDown_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double dstsize_width, double dstsize_height, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrDown_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double dstsize_width, double dstsize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrDown_12(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrMeanShiftFiltering_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double sp, double sr, int maxLevel, int termcrit_type, int termcrit_maxCount, double termcrit_epsilon);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrMeanShiftFiltering_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double sp, double sr);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrUp_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double dstsize_width, double dstsize_height, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrUp_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double dstsize_width, double dstsize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_pyrUp_12(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_rectangle_10(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3, int thickness, int lineType, int shift);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_rectangle_11(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3, int thickness);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_rectangle_12(IntPtr img_nativeObj, double pt1_x, double pt1_y, double pt2_x, double pt2_y, double color_val0, double color_val1, double color_val2, double color_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_remap_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr map1_nativeObj, IntPtr map2_nativeObj, int interpolation, int borderMode, double borderValue_val0, double borderValue_val1, double borderValue_val2, double borderValue_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_remap_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr map1_nativeObj, IntPtr map2_nativeObj, int interpolation);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_resize_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double dsize_width, double dsize_height, double fx, double fy, int interpolation);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_resize_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double dsize_width, double dsize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_sepFilter2D_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, IntPtr kernelX_nativeObj, IntPtr kernelY_nativeObj, double anchor_x, double anchor_y, double delta, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_sepFilter2D_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, IntPtr kernelX_nativeObj, IntPtr kernelY_nativeObj, double anchor_x, double anchor_y, double delta);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_sepFilter2D_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, int ddepth, IntPtr kernelX_nativeObj, IntPtr kernelY_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_spatialGradient_10(IntPtr src_nativeObj, IntPtr dx_nativeObj, IntPtr dy_nativeObj, int ksize, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_spatialGradient_11(IntPtr src_nativeObj, IntPtr dx_nativeObj, IntPtr dy_nativeObj, int ksize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_spatialGradient_12(IntPtr src_nativeObj, IntPtr dx_nativeObj, IntPtr dy_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_sqrBoxFilter_10(IntPtr _src_nativeObj, IntPtr _dst_nativeObj, int ddepth, double ksize_width, double ksize_height, double anchor_x, double anchor_y, bool normalize, int borderType);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_sqrBoxFilter_11(IntPtr _src_nativeObj, IntPtr _dst_nativeObj, int ddepth, double ksize_width, double ksize_height, double anchor_x, double anchor_y, bool normalize);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_sqrBoxFilter_12(IntPtr _src_nativeObj, IntPtr _dst_nativeObj, int ddepth, double ksize_width, double ksize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_undistort_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, IntPtr newCameraMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_undistort_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_undistortPoints_10(IntPtr src_mat_nativeObj, IntPtr dst_mat_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj, IntPtr R_nativeObj, IntPtr P_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_undistortPoints_11(IntPtr src_mat_nativeObj, IntPtr dst_mat_nativeObj, IntPtr cameraMatrix_nativeObj, IntPtr distCoeffs_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_warpAffine_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr M_nativeObj, double dsize_width, double dsize_height, int flags, int borderMode, double borderValue_val0, double borderValue_val1, double borderValue_val2, double borderValue_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_warpAffine_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr M_nativeObj, double dsize_width, double dsize_height, int flags);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_warpAffine_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr M_nativeObj, double dsize_width, double dsize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_warpPerspective_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr M_nativeObj, double dsize_width, double dsize_height, int flags, int borderMode, double borderValue_val0, double borderValue_val1, double borderValue_val2, double borderValue_val3);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_warpPerspective_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr M_nativeObj, double dsize_width, double dsize_height, int flags);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_warpPerspective_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr M_nativeObj, double dsize_width, double dsize_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_watershed_10(IntPtr image_nativeObj, IntPtr markers_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Imgproc_n_1getTextSize(string text, int fontFace, double fontScale, int thickness, int[] baseLine, double[] vals);
	}
}
