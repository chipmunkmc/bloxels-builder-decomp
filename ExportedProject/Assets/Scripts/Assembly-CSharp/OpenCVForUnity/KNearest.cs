using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class KNearest : StatModel
	{
		public const int BRUTE_FORCE = 1;

		public const int KDTREE = 2;

		private const string LIBNAME = "opencvforunity";

		protected KNearest(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_KNearest_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public static KNearest create()
		{
			return new KNearest(ml_KNearest_create_10());
		}

		public bool getIsClassifier()
		{
			ThrowIfDisposed();
			return ml_KNearest_getIsClassifier_10(nativeObj);
		}

		public float findNearest(Mat samples, int k, Mat results, Mat neighborResponses, Mat dist)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (results != null)
			{
				results.ThrowIfDisposed();
			}
			if (neighborResponses != null)
			{
				neighborResponses.ThrowIfDisposed();
			}
			if (dist != null)
			{
				dist.ThrowIfDisposed();
			}
			return ml_KNearest_findNearest_10(nativeObj, samples.nativeObj, k, results.nativeObj, neighborResponses.nativeObj, dist.nativeObj);
		}

		public float findNearest(Mat samples, int k, Mat results)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (results != null)
			{
				results.ThrowIfDisposed();
			}
			return ml_KNearest_findNearest_11(nativeObj, samples.nativeObj, k, results.nativeObj);
		}

		public int getAlgorithmType()
		{
			ThrowIfDisposed();
			return ml_KNearest_getAlgorithmType_10(nativeObj);
		}

		public int getDefaultK()
		{
			ThrowIfDisposed();
			return ml_KNearest_getDefaultK_10(nativeObj);
		}

		public int getEmax()
		{
			ThrowIfDisposed();
			return ml_KNearest_getEmax_10(nativeObj);
		}

		public void setAlgorithmType(int val)
		{
			ThrowIfDisposed();
			ml_KNearest_setAlgorithmType_10(nativeObj, val);
		}

		public void setDefaultK(int val)
		{
			ThrowIfDisposed();
			ml_KNearest_setDefaultK_10(nativeObj, val);
		}

		public void setEmax(int val)
		{
			ThrowIfDisposed();
			ml_KNearest_setEmax_10(nativeObj, val);
		}

		public void setIsClassifier(bool val)
		{
			ThrowIfDisposed();
			ml_KNearest_setIsClassifier_10(nativeObj, val);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_KNearest_create_10();

		[DllImport("opencvforunity")]
		private static extern bool ml_KNearest_getIsClassifier_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ml_KNearest_findNearest_10(IntPtr nativeObj, IntPtr samples_nativeObj, int k, IntPtr results_nativeObj, IntPtr neighborResponses_nativeObj, IntPtr dist_nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ml_KNearest_findNearest_11(IntPtr nativeObj, IntPtr samples_nativeObj, int k, IntPtr results_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_KNearest_getAlgorithmType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_KNearest_getDefaultK_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_KNearest_getEmax_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_KNearest_setAlgorithmType_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_KNearest_setDefaultK_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_KNearest_setEmax_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_KNearest_setIsClassifier_10(IntPtr nativeObj, bool val);

		[DllImport("opencvforunity")]
		private static extern void ml_KNearest_delete(IntPtr nativeObj);
	}
}
