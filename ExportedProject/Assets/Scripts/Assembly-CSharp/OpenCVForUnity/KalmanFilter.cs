using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class KalmanFilter : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		protected KalmanFilter(IntPtr addr)
			: base(addr)
		{
		}

		public KalmanFilter(int dynamParams, int measureParams, int controlParams, int type)
		{
			nativeObj = video_KalmanFilter_KalmanFilter_10(dynamParams, measureParams, controlParams, type);
		}

		public KalmanFilter(int dynamParams, int measureParams)
		{
			nativeObj = video_KalmanFilter_KalmanFilter_11(dynamParams, measureParams);
		}

		public KalmanFilter()
		{
			nativeObj = video_KalmanFilter_KalmanFilter_12();
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						video_KalmanFilter_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat correct(Mat measurement)
		{
			ThrowIfDisposed();
			if (measurement != null)
			{
				measurement.ThrowIfDisposed();
			}
			return new Mat(video_KalmanFilter_correct_10(nativeObj, measurement.nativeObj));
		}

		public Mat predict(Mat control)
		{
			ThrowIfDisposed();
			if (control != null)
			{
				control.ThrowIfDisposed();
			}
			return new Mat(video_KalmanFilter_predict_10(nativeObj, control.nativeObj));
		}

		public Mat predict()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_predict_11(nativeObj));
		}

		public Mat get_statePre()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1statePre_10(nativeObj));
		}

		public void set_statePre(Mat statePre)
		{
			ThrowIfDisposed();
			if (statePre != null)
			{
				statePre.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1statePre_10(nativeObj, statePre.nativeObj);
		}

		public Mat get_statePost()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1statePost_10(nativeObj));
		}

		public void set_statePost(Mat statePost)
		{
			ThrowIfDisposed();
			if (statePost != null)
			{
				statePost.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1statePost_10(nativeObj, statePost.nativeObj);
		}

		public Mat get_transitionMatrix()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1transitionMatrix_10(nativeObj));
		}

		public void set_transitionMatrix(Mat transitionMatrix)
		{
			ThrowIfDisposed();
			if (transitionMatrix != null)
			{
				transitionMatrix.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1transitionMatrix_10(nativeObj, transitionMatrix.nativeObj);
		}

		public Mat get_controlMatrix()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1controlMatrix_10(nativeObj));
		}

		public void set_controlMatrix(Mat controlMatrix)
		{
			ThrowIfDisposed();
			if (controlMatrix != null)
			{
				controlMatrix.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1controlMatrix_10(nativeObj, controlMatrix.nativeObj);
		}

		public Mat get_measurementMatrix()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1measurementMatrix_10(nativeObj));
		}

		public void set_measurementMatrix(Mat measurementMatrix)
		{
			ThrowIfDisposed();
			if (measurementMatrix != null)
			{
				measurementMatrix.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1measurementMatrix_10(nativeObj, measurementMatrix.nativeObj);
		}

		public Mat get_processNoiseCov()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1processNoiseCov_10(nativeObj));
		}

		public void set_processNoiseCov(Mat processNoiseCov)
		{
			ThrowIfDisposed();
			if (processNoiseCov != null)
			{
				processNoiseCov.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1processNoiseCov_10(nativeObj, processNoiseCov.nativeObj);
		}

		public Mat get_measurementNoiseCov()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1measurementNoiseCov_10(nativeObj));
		}

		public void set_measurementNoiseCov(Mat measurementNoiseCov)
		{
			ThrowIfDisposed();
			if (measurementNoiseCov != null)
			{
				measurementNoiseCov.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1measurementNoiseCov_10(nativeObj, measurementNoiseCov.nativeObj);
		}

		public Mat get_errorCovPre()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1errorCovPre_10(nativeObj));
		}

		public void set_errorCovPre(Mat errorCovPre)
		{
			ThrowIfDisposed();
			if (errorCovPre != null)
			{
				errorCovPre.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1errorCovPre_10(nativeObj, errorCovPre.nativeObj);
		}

		public Mat get_gain()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1gain_10(nativeObj));
		}

		public void set_gain(Mat gain)
		{
			ThrowIfDisposed();
			if (gain != null)
			{
				gain.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1gain_10(nativeObj, gain.nativeObj);
		}

		public Mat get_errorCovPost()
		{
			ThrowIfDisposed();
			return new Mat(video_KalmanFilter_get_1errorCovPost_10(nativeObj));
		}

		public void set_errorCovPost(Mat errorCovPost)
		{
			ThrowIfDisposed();
			if (errorCovPost != null)
			{
				errorCovPost.ThrowIfDisposed();
			}
			video_KalmanFilter_set_1errorCovPost_10(nativeObj, errorCovPost.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_KalmanFilter_10(int dynamParams, int measureParams, int controlParams, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_KalmanFilter_11(int dynamParams, int measureParams);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_KalmanFilter_12();

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_correct_10(IntPtr nativeObj, IntPtr measurement_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_predict_10(IntPtr nativeObj, IntPtr control_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_predict_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1statePre_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1statePre_10(IntPtr nativeObj, IntPtr statePre_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1statePost_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1statePost_10(IntPtr nativeObj, IntPtr statePost_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1transitionMatrix_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1transitionMatrix_10(IntPtr nativeObj, IntPtr transitionMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1controlMatrix_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1controlMatrix_10(IntPtr nativeObj, IntPtr controlMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1measurementMatrix_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1measurementMatrix_10(IntPtr nativeObj, IntPtr measurementMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1processNoiseCov_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1processNoiseCov_10(IntPtr nativeObj, IntPtr processNoiseCov_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1measurementNoiseCov_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1measurementNoiseCov_10(IntPtr nativeObj, IntPtr measurementNoiseCov_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1errorCovPre_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1errorCovPre_10(IntPtr nativeObj, IntPtr errorCovPre_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1gain_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1gain_10(IntPtr nativeObj, IntPtr gain_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_KalmanFilter_get_1errorCovPost_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_set_1errorCovPost_10(IntPtr nativeObj, IntPtr errorCovPost_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void video_KalmanFilter_delete(IntPtr nativeObj);
	}
}
