namespace OpenCVForUnity
{
	public class KeyPoint
	{
		public Point pt;

		public float size;

		public float angle;

		public float response;

		public int octave;

		public int class_id;

		public KeyPoint(float x, float y, float _size, float _angle, float _response, int _octave, int _class_id)
		{
			pt = new Point(x, y);
			size = _size;
			angle = _angle;
			response = _response;
			octave = _octave;
			class_id = _class_id;
		}

		public KeyPoint()
			: this(0f, 0f, 0f, -1f, 0f, 0, -1)
		{
		}

		public KeyPoint(float x, float y, float _size, float _angle, float _response, int _octave)
			: this(x, y, _size, _angle, _response, _octave, -1)
		{
		}

		public KeyPoint(float x, float y, float _size, float _angle, float _response)
			: this(x, y, _size, _angle, _response, 0, -1)
		{
		}

		public KeyPoint(float x, float y, float _size, float _angle)
			: this(x, y, _size, _angle, 0f, 0, -1)
		{
		}

		public KeyPoint(float x, float y, float _size)
			: this(x, y, _size, -1f, 0f, 0, -1)
		{
		}

		public override string ToString()
		{
			return string.Concat("KeyPoint [pt=", pt, ", size=", size, ", angle=", angle, ", response=", response, ", octave=", octave, ", class_id=", class_id, "]");
		}
	}
}
