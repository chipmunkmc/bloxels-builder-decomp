using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class LBPHFaceRecognizer : FaceRecognizer
	{
		private const string LIBNAME = "opencvforunity";

		public LBPHFaceRecognizer(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						face_LBPHFaceRecognizer_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getLabels()
		{
			ThrowIfDisposed();
			return new Mat(face_LBPHFaceRecognizer_getLabels_10(nativeObj));
		}

		public double getThreshold()
		{
			ThrowIfDisposed();
			return face_LBPHFaceRecognizer_getThreshold_10(nativeObj);
		}

		public int getGridX()
		{
			ThrowIfDisposed();
			return face_LBPHFaceRecognizer_getGridX_10(nativeObj);
		}

		public int getGridY()
		{
			ThrowIfDisposed();
			return face_LBPHFaceRecognizer_getGridY_10(nativeObj);
		}

		public int getNeighbors()
		{
			ThrowIfDisposed();
			return face_LBPHFaceRecognizer_getNeighbors_10(nativeObj);
		}

		public int getRadius()
		{
			ThrowIfDisposed();
			return face_LBPHFaceRecognizer_getRadius_10(nativeObj);
		}

		public List<Mat> getHistograms()
		{
			ThrowIfDisposed();
			List<Mat> list = new List<Mat>();
			Mat m = new Mat(face_LBPHFaceRecognizer_getHistograms_10(nativeObj));
			Converters.Mat_to_vector_Mat(m, list);
			return list;
		}

		public void setGridX(int val)
		{
			ThrowIfDisposed();
			face_LBPHFaceRecognizer_setGridX_10(nativeObj, val);
		}

		public void setGridY(int val)
		{
			ThrowIfDisposed();
			face_LBPHFaceRecognizer_setGridY_10(nativeObj, val);
		}

		public void setNeighbors(int val)
		{
			ThrowIfDisposed();
			face_LBPHFaceRecognizer_setNeighbors_10(nativeObj, val);
		}

		public void setRadius(int val)
		{
			ThrowIfDisposed();
			face_LBPHFaceRecognizer_setRadius_10(nativeObj, val);
		}

		public void setThreshold(double val)
		{
			ThrowIfDisposed();
			face_LBPHFaceRecognizer_setThreshold_10(nativeObj, val);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr face_LBPHFaceRecognizer_getLabels_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double face_LBPHFaceRecognizer_getThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int face_LBPHFaceRecognizer_getGridX_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int face_LBPHFaceRecognizer_getGridY_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int face_LBPHFaceRecognizer_getNeighbors_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int face_LBPHFaceRecognizer_getRadius_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_LBPHFaceRecognizer_getHistograms_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void face_LBPHFaceRecognizer_setGridX_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void face_LBPHFaceRecognizer_setGridY_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void face_LBPHFaceRecognizer_setNeighbors_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void face_LBPHFaceRecognizer_setRadius_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void face_LBPHFaceRecognizer_setThreshold_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void face_LBPHFaceRecognizer_delete(IntPtr nativeObj);
	}
}
