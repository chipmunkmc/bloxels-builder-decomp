using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class LineSegmentDetector : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public LineSegmentDetector(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						imgproc_LineSegmentDetector_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public int compareSegments(Size size, Mat lines1, Mat lines2, Mat _image)
		{
			ThrowIfDisposed();
			if (lines1 != null)
			{
				lines1.ThrowIfDisposed();
			}
			if (lines2 != null)
			{
				lines2.ThrowIfDisposed();
			}
			if (_image != null)
			{
				_image.ThrowIfDisposed();
			}
			return imgproc_LineSegmentDetector_compareSegments_10(nativeObj, size.width, size.height, lines1.nativeObj, lines2.nativeObj, _image.nativeObj);
		}

		public int compareSegments(Size size, Mat lines1, Mat lines2)
		{
			ThrowIfDisposed();
			if (lines1 != null)
			{
				lines1.ThrowIfDisposed();
			}
			if (lines2 != null)
			{
				lines2.ThrowIfDisposed();
			}
			return imgproc_LineSegmentDetector_compareSegments_11(nativeObj, size.width, size.height, lines1.nativeObj, lines2.nativeObj);
		}

		public void detect(Mat _image, Mat _lines, Mat width, Mat prec, Mat nfa)
		{
			ThrowIfDisposed();
			if (_image != null)
			{
				_image.ThrowIfDisposed();
			}
			if (_lines != null)
			{
				_lines.ThrowIfDisposed();
			}
			if (width != null)
			{
				width.ThrowIfDisposed();
			}
			if (prec != null)
			{
				prec.ThrowIfDisposed();
			}
			if (nfa != null)
			{
				nfa.ThrowIfDisposed();
			}
			imgproc_LineSegmentDetector_detect_10(nativeObj, _image.nativeObj, _lines.nativeObj, width.nativeObj, prec.nativeObj, nfa.nativeObj);
		}

		public void detect(Mat _image, Mat _lines)
		{
			ThrowIfDisposed();
			if (_image != null)
			{
				_image.ThrowIfDisposed();
			}
			if (_lines != null)
			{
				_lines.ThrowIfDisposed();
			}
			imgproc_LineSegmentDetector_detect_11(nativeObj, _image.nativeObj, _lines.nativeObj);
		}

		public void drawSegments(Mat _image, Mat lines)
		{
			ThrowIfDisposed();
			if (_image != null)
			{
				_image.ThrowIfDisposed();
			}
			if (lines != null)
			{
				lines.ThrowIfDisposed();
			}
			imgproc_LineSegmentDetector_drawSegments_10(nativeObj, _image.nativeObj, lines.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern int imgproc_LineSegmentDetector_compareSegments_10(IntPtr nativeObj, double size_width, double size_height, IntPtr lines1_nativeObj, IntPtr lines2_nativeObj, IntPtr _image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int imgproc_LineSegmentDetector_compareSegments_11(IntPtr nativeObj, double size_width, double size_height, IntPtr lines1_nativeObj, IntPtr lines2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_LineSegmentDetector_detect_10(IntPtr nativeObj, IntPtr _image_nativeObj, IntPtr _lines_nativeObj, IntPtr width_nativeObj, IntPtr prec_nativeObj, IntPtr nfa_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_LineSegmentDetector_detect_11(IntPtr nativeObj, IntPtr _image_nativeObj, IntPtr _lines_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_LineSegmentDetector_drawSegments_10(IntPtr nativeObj, IntPtr _image_nativeObj, IntPtr lines_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_LineSegmentDetector_delete(IntPtr nativeObj);
	}
}
