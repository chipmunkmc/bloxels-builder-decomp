using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class LogisticRegression : StatModel
	{
		public const int REG_DISABLE = -1;

		public const int REG_L1 = 0;

		public const int REG_L2 = 1;

		public const int BATCH = 0;

		public const int MINI_BATCH = 1;

		private const string LIBNAME = "opencvforunity";

		protected LogisticRegression(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_LogisticRegression_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat get_learnt_thetas()
		{
			ThrowIfDisposed();
			return new Mat(ml_LogisticRegression_get_1learnt_1thetas_10(nativeObj));
		}

		public static LogisticRegression create()
		{
			return new LogisticRegression(ml_LogisticRegression_create_10());
		}

		public TermCriteria getTermCriteria()
		{
			ThrowIfDisposed();
			double[] vals = new double[3];
			ml_LogisticRegression_getTermCriteria_10(nativeObj, vals);
			return new TermCriteria(vals);
		}

		public double getLearningRate()
		{
			ThrowIfDisposed();
			return ml_LogisticRegression_getLearningRate_10(nativeObj);
		}

		public override float predict(Mat samples, Mat results, int flags)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (results != null)
			{
				results.ThrowIfDisposed();
			}
			return ml_LogisticRegression_predict_10(nativeObj, samples.nativeObj, results.nativeObj, flags);
		}

		public override float predict(Mat samples)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			return ml_LogisticRegression_predict_11(nativeObj, samples.nativeObj);
		}

		public int getIterations()
		{
			ThrowIfDisposed();
			return ml_LogisticRegression_getIterations_10(nativeObj);
		}

		public int getMiniBatchSize()
		{
			ThrowIfDisposed();
			return ml_LogisticRegression_getMiniBatchSize_10(nativeObj);
		}

		public int getRegularization()
		{
			ThrowIfDisposed();
			return ml_LogisticRegression_getRegularization_10(nativeObj);
		}

		public int getTrainMethod()
		{
			ThrowIfDisposed();
			return ml_LogisticRegression_getTrainMethod_10(nativeObj);
		}

		public void setIterations(int val)
		{
			ThrowIfDisposed();
			ml_LogisticRegression_setIterations_10(nativeObj, val);
		}

		public void setLearningRate(double val)
		{
			ThrowIfDisposed();
			ml_LogisticRegression_setLearningRate_10(nativeObj, val);
		}

		public void setMiniBatchSize(int val)
		{
			ThrowIfDisposed();
			ml_LogisticRegression_setMiniBatchSize_10(nativeObj, val);
		}

		public void setRegularization(int val)
		{
			ThrowIfDisposed();
			ml_LogisticRegression_setRegularization_10(nativeObj, val);
		}

		public void setTermCriteria(TermCriteria val)
		{
			ThrowIfDisposed();
			ml_LogisticRegression_setTermCriteria_10(nativeObj, val.type, val.maxCount, val.epsilon);
		}

		public void setTrainMethod(int val)
		{
			ThrowIfDisposed();
			ml_LogisticRegression_setTrainMethod_10(nativeObj, val);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_LogisticRegression_get_1learnt_1thetas_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_LogisticRegression_create_10();

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_getTermCriteria_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern double ml_LogisticRegression_getLearningRate_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ml_LogisticRegression_predict_10(IntPtr nativeObj, IntPtr samples_nativeObj, IntPtr results_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern float ml_LogisticRegression_predict_11(IntPtr nativeObj, IntPtr samples_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_LogisticRegression_getIterations_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_LogisticRegression_getMiniBatchSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_LogisticRegression_getRegularization_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_LogisticRegression_getTrainMethod_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_setIterations_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_setLearningRate_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_setMiniBatchSize_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_setRegularization_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_setTermCriteria_10(IntPtr nativeObj, int val_type, int val_maxCount, double val_epsilon);

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_setTrainMethod_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_LogisticRegression_delete(IntPtr nativeObj);
	}
}
