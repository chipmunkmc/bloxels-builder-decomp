using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Mat : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		public Mat(IntPtr addr)
		{
			if (addr == IntPtr.Zero)
			{
				throw new CvException("Native object address is NULL");
			}
			nativeObj = addr;
		}

		public Mat()
		{
			nativeObj = core_Mat_n_1Mat__();
		}

		public Mat(int rows, int cols, int type)
		{
			nativeObj = core_Mat_n_1Mat__III(rows, cols, type);
		}

		public Mat(Size size, int type)
		{
			nativeObj = core_Mat_n_1Mat__DDI(size.width, size.height, type);
		}

		public Mat(int rows, int cols, int type, Scalar s)
		{
			nativeObj = core_Mat_n_1Mat__IIIDDDD(rows, cols, type, s.val[0], s.val[1], s.val[2], s.val[3]);
		}

		public Mat(Size size, int type, Scalar s)
		{
			nativeObj = core_Mat_n_1Mat__DDIDDDD(size.width, size.height, type, s.val[0], s.val[1], s.val[2], s.val[3]);
		}

		public Mat(Mat m, Range rowRange, Range colRange)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			nativeObj = core_Mat_n_1Mat__JIIII(m.nativeObj, rowRange.start, rowRange.end, colRange.start, colRange.end);
		}

		public Mat(Mat m, Range rowRange)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			nativeObj = core_Mat_n_1Mat__JII(m.nativeObj, rowRange.start, rowRange.end);
		}

		public Mat(Mat m, Rect roi)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			nativeObj = core_Mat_n_1Mat__JIIII(m.nativeObj, roi.y, roi.y + roi.height, roi.x, roi.x + roi.width);
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						core_Mat_n_1delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat adjustROI(int dtop, int dbottom, int dleft, int dright)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1adjustROI(nativeObj, dtop, dbottom, dleft, dright));
		}

		public void assignTo(Mat m, int type)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1assignTo__JJI(nativeObj, m.nativeObj, type);
		}

		public void assignTo(Mat m)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1assignTo__JJ(nativeObj, m.nativeObj);
		}

		public int channels()
		{
			ThrowIfDisposed();
			return core_Mat_n_1channels(nativeObj);
		}

		public int checkVector(int elemChannels, int depth, bool requireContinuous)
		{
			ThrowIfDisposed();
			return core_Mat_n_1checkVector__JIIZ(nativeObj, elemChannels, depth, requireContinuous);
		}

		public int checkVector(int elemChannels, int depth)
		{
			ThrowIfDisposed();
			return core_Mat_n_1checkVector__JII(nativeObj, elemChannels, depth);
		}

		public int checkVector(int elemChannels)
		{
			ThrowIfDisposed();
			return core_Mat_n_1checkVector__JI(nativeObj, elemChannels);
		}

		public Mat clone()
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1clone(nativeObj));
		}

		public Mat col(int x)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1col(nativeObj, x));
		}

		public Mat colRange(int startcol, int endcol)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1colRange(nativeObj, startcol, endcol));
		}

		public Mat colRange(Range r)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1colRange(nativeObj, r.start, r.end));
		}

		public int dims()
		{
			ThrowIfDisposed();
			return core_Mat_n_1dims(nativeObj);
		}

		public int cols()
		{
			ThrowIfDisposed();
			return core_Mat_n_1cols(nativeObj);
		}

		public void convertTo(Mat m, int rtype, double alpha, double beta)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1convertTo__JJIDD(nativeObj, m.nativeObj, rtype, alpha, beta);
		}

		public void convertTo(Mat m, int rtype, double alpha)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1convertTo__JJID(nativeObj, m.nativeObj, rtype, alpha);
		}

		public void convertTo(Mat m, int rtype)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1convertTo__JJI(nativeObj, m.nativeObj, rtype);
		}

		public void copyTo(Mat m)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1copyTo__JJ(nativeObj, m.nativeObj);
		}

		public void copyTo(Mat m, Mat mask)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1copyTo__JJJ(nativeObj, m.nativeObj, mask.nativeObj);
		}

		public void create(int rows, int cols, int type)
		{
			ThrowIfDisposed();
			core_Mat_n_1create__JIII(nativeObj, rows, cols, type);
		}

		public void create(Size size, int type)
		{
			ThrowIfDisposed();
			core_Mat_n_1create__JDDI(nativeObj, size.width, size.height, type);
		}

		public Mat cross(Mat m)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1cross(nativeObj, m.nativeObj));
		}

		public long dataAddr()
		{
			ThrowIfDisposed();
			return core_Mat_n_1dataAddr(nativeObj);
		}

		public int depth()
		{
			ThrowIfDisposed();
			return core_Mat_n_1depth(nativeObj);
		}

		public Mat diag(int d)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1diag__JI(nativeObj, d));
		}

		public Mat diag()
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1diag__JI(nativeObj, 0));
		}

		public static Mat diag(Mat d)
		{
			if (d != null)
			{
				d.ThrowIfDisposed();
			}
			return new Mat(core_Mat_n_1diag__J(d.nativeObj));
		}

		public double dot(Mat m)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			return core_Mat_n_1dot(nativeObj, m.nativeObj);
		}

		public long elemSize()
		{
			ThrowIfDisposed();
			return core_Mat_n_1elemSize(nativeObj);
		}

		public long elemSize1()
		{
			ThrowIfDisposed();
			return core_Mat_n_1elemSize1(nativeObj);
		}

		public bool empty()
		{
			ThrowIfDisposed();
			return core_Mat_n_1empty(nativeObj);
		}

		public static Mat eye(int rows, int cols, int type)
		{
			return new Mat(core_Mat_n_1eye__III(rows, cols, type));
		}

		public static Mat eye(Size size, int type)
		{
			return new Mat(core_Mat_n_1eye__DDI(size.width, size.height, type));
		}

		public Mat inv(int method)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1inv__JI(nativeObj, method));
		}

		public Mat inv()
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1inv__J(nativeObj));
		}

		public bool isContinuous()
		{
			ThrowIfDisposed();
			return core_Mat_n_1isContinuous(nativeObj);
		}

		public bool isSubmatrix()
		{
			ThrowIfDisposed();
			return core_Mat_n_1isSubmatrix(nativeObj);
		}

		public void locateROI(Size wholeSize, Point ofs)
		{
			ThrowIfDisposed();
			double[] array = new double[2];
			double[] array2 = new double[2];
			core_Mat_locateROI_10(nativeObj, array, array2);
			if (wholeSize != null)
			{
				wholeSize.width = array[0];
				wholeSize.height = array[1];
			}
			if (ofs != null)
			{
				ofs.x = array2[0];
				ofs.y = array2[1];
			}
		}

		public Mat mul(Mat m, double scale)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1mul__JJD(nativeObj, m.nativeObj, scale));
		}

		public Mat mul(Mat m)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1mul__JJ(nativeObj, m.nativeObj));
		}

		public static Mat ones(int rows, int cols, int type)
		{
			return new Mat(core_Mat_n_1ones__III(rows, cols, type));
		}

		public static Mat ones(Size size, int type)
		{
			return new Mat(core_Mat_n_1ones__DDI(size.width, size.height, type));
		}

		public void push_back(Mat m)
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			core_Mat_n_1push_1back(nativeObj, m.nativeObj);
		}

		public void release()
		{
			ThrowIfDisposed();
			core_Mat_n_1release(nativeObj);
		}

		public Mat reshape(int cn, int rows)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1reshape__JII(nativeObj, cn, rows));
		}

		public Mat reshape(int cn)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1reshape__JI(nativeObj, cn));
		}

		public Mat row(int y)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1row(nativeObj, y));
		}

		public Mat rowRange(int startrow, int endrow)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1rowRange(nativeObj, startrow, endrow));
		}

		public Mat rowRange(Range r)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1rowRange(nativeObj, r.start, r.end));
		}

		public int rows()
		{
			ThrowIfDisposed();
			return core_Mat_n_1rows(nativeObj);
		}

		public Mat setTo(Scalar s)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1setTo__JDDDD(nativeObj, s.val[0], s.val[1], s.val[2], s.val[3]));
		}

		public Mat setTo(Scalar value, Mat mask)
		{
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1setTo__JDDDDJ(nativeObj, value.val[0], value.val[1], value.val[2], value.val[3], mask.nativeObj));
		}

		public Mat setTo(Mat value, Mat mask)
		{
			if (value != null)
			{
				value.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1setTo__JJJ(nativeObj, value.nativeObj, mask.nativeObj));
		}

		public Mat setTo(Mat value)
		{
			if (value != null)
			{
				value.ThrowIfDisposed();
			}
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1setTo__JJ(nativeObj, value.nativeObj));
		}

		public Size size()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			core_Mat_n_1size(nativeObj, vals);
			return new Size(vals);
		}

		public long step1(int i)
		{
			ThrowIfDisposed();
			return core_Mat_n_1step1__JI(nativeObj, i);
		}

		public long step1()
		{
			ThrowIfDisposed();
			return core_Mat_n_1step1__J(nativeObj);
		}

		public Mat submat(int rowStart, int rowEnd, int colStart, int colEnd)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1submat_1rr(nativeObj, rowStart, rowEnd, colStart, colEnd));
		}

		public Mat submat(Range rowRange, Range colRange)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1submat_1rr(nativeObj, rowRange.start, rowRange.end, colRange.start, colRange.end));
		}

		public Mat submat(Rect roi)
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1submat(nativeObj, roi.x, roi.y, roi.width, roi.height));
		}

		public Mat t()
		{
			ThrowIfDisposed();
			return new Mat(core_Mat_n_1t(nativeObj));
		}

		public long total()
		{
			ThrowIfDisposed();
			return core_Mat_n_1total(nativeObj);
		}

		public int type()
		{
			ThrowIfDisposed();
			return core_Mat_n_1type(nativeObj);
		}

		public static Mat zeros(int rows, int cols, int type)
		{
			return new Mat(core_Mat_n_1zeros__III(rows, cols, type));
		}

		public static Mat zeros(Size size, int type)
		{
			return new Mat(core_Mat_n_1zeros__DDI(size.width, size.height, type));
		}

		public override string ToString()
		{
			return "Mat [ " + rows() + "*" + cols() + "*" + CvType.typeToString(type()) + ", isCont=" + isContinuous() + ", isSubmat=" + isSubmatrix() + ", nativeObj=0x" + Convert.ToString(nativeObj) + ", dataAddr=0x" + Convert.ToString(dataAddr()) + " ]";
		}

		public string dump()
		{
			ThrowIfDisposed();
			return Marshal.PtrToStringAnsi(core_Mat_nDump(nativeObj));
		}

		public int put(int row, int col, params double[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			return core_Mat_nPutD(nativeObj, row, col, data.Length, data);
		}

		public int put(int row, int col, float[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 5)
			{
				return core_Mat_nPutF(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int put(int row, int col, int[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 4)
			{
				return core_Mat_nPutI(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int put(int row, int col, short[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 2 || CvType.depth(num) == 3)
			{
				return core_Mat_nPutS(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int put(int row, int col, byte[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 0 || CvType.depth(num) == 1)
			{
				return core_Mat_nPutB(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int get(int row, int col, byte[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 0 || CvType.depth(num) == 1)
			{
				return core_Mat_nGetB(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int get(int row, int col, short[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 2 || CvType.depth(num) == 3)
			{
				return core_Mat_nGetS(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int get(int row, int col, int[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 4)
			{
				return core_Mat_nGetI(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int get(int row, int col, float[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 5)
			{
				return core_Mat_nGetF(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public int get(int row, int col, double[] data)
		{
			ThrowIfDisposed();
			int num = type();
			if (data == null || data.Length % CvType.channels(num) != 0)
			{
				throw new CvException("Provided data element number (" + ((data != null) ? data.Length : 0) + ") should be multiple of the Mat channels count (" + CvType.channels(num) + ")");
			}
			if (CvType.depth(num) == 6)
			{
				return core_Mat_nGetD(nativeObj, row, col, data.Length, data);
			}
			throw new CvException("Mat data type is not compatible: " + num);
		}

		public double[] get(int row, int col)
		{
			ThrowIfDisposed();
			double[] array = new double[channels()];
			if (core_Mat_nGet(nativeObj, row, col, array.Length, array) == 0)
			{
				return null;
			}
			return array;
		}

		public int height()
		{
			return rows();
		}

		public int width()
		{
			return cols();
		}

		public IntPtr getNativeObjAddr()
		{
			return nativeObj;
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1Mat__();

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1Mat__III(int rows, int cols, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1Mat__DDI(double size_width, double size_height, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1Mat__IIIDDDD(int rows, int cols, int type, double s_val0, double s_val1, double s_val2, double s_val3);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1Mat__DDIDDDD(double size_width, double size_height, int type, double s_val0, double s_val1, double s_val2, double s_val3);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1Mat__JIIII(IntPtr m_nativeObj, int rowRange_start, int rowRange_end, int colRange_start, int colRange_end);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1Mat__JII(IntPtr m_nativeObj, int rowRange_start, int rowRange_end);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1adjustROI(IntPtr nativeObj, int dtop, int dbottom, int dleft, int dright);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1assignTo__JJI(IntPtr nativeObj, IntPtr m_nativeObj, int type);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1assignTo__JJ(IntPtr nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1channels(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1checkVector__JIIZ(IntPtr nativeObj, int elemChannels, int depth, bool requireContinuous);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1checkVector__JII(IntPtr nativeObj, int elemChannels, int depth);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1checkVector__JI(IntPtr nativeObj, int elemChannels);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1clone(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1col(IntPtr nativeObj, int x);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1colRange(IntPtr nativeObj, int startcol, int endcol);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1dims(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1cols(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1convertTo__JJIDD(IntPtr nativeObj, IntPtr m_nativeObj, int rtype, double alpha, double beta);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1convertTo__JJID(IntPtr nativeObj, IntPtr m_nativeObj, int rtype, double alpha);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1convertTo__JJI(IntPtr nativeObj, IntPtr m_nativeObj, int rtype);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1copyTo__JJ(IntPtr nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1copyTo__JJJ(IntPtr nativeObj, IntPtr m_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1create__JIII(IntPtr nativeObj, int rows, int cols, int type);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1create__JDDI(IntPtr nativeObj, double size_width, double size_height, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1cross(IntPtr nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern long core_Mat_n_1dataAddr(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1depth(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1diag__JI(IntPtr nativeObj, int d);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1diag__J(IntPtr d_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double core_Mat_n_1dot(IntPtr nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern long core_Mat_n_1elemSize(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern long core_Mat_n_1elemSize1(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool core_Mat_n_1empty(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1eye__III(int rows, int cols, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1eye__DDI(double size_width, double size_height, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1inv__JI(IntPtr nativeObj, int method);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1inv__J(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool core_Mat_n_1isContinuous(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool core_Mat_n_1isSubmatrix(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_locateROI_10(IntPtr nativeObj, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeConst = 2)] double[] wholeSize_out, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeConst = 2)] double[] ofs_out);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1mul__JJD(IntPtr nativeObj, IntPtr m_nativeObj, double scale);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1mul__JJ(IntPtr nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1ones__III(int rows, int cols, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1ones__DDI(double size_width, double size_height, int type);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1push_1back(IntPtr nativeObj, IntPtr m_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1release(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1reshape__JII(IntPtr nativeObj, int cn, int rows);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1reshape__JI(IntPtr nativeObj, int cn);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1row(IntPtr nativeObj, int y);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1rowRange(IntPtr nativeObj, int startrow, int endrow);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1rows(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1setTo__JDDDD(IntPtr nativeObj, double s_val0, double s_val1, double s_val2, double s_val3);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1setTo__JDDDDJ(IntPtr nativeObj, double s_val0, double s_val1, double s_val2, double s_val3, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1setTo__JJJ(IntPtr nativeObj, IntPtr value_nativeObj, IntPtr mask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1setTo__JJ(IntPtr nativeObj, IntPtr value_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1size(IntPtr nativeObj, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeConst = 2)] double[] vals);

		[DllImport("opencvforunity")]
		private static extern long core_Mat_n_1step1__JI(IntPtr nativeObj, int i);

		[DllImport("opencvforunity")]
		private static extern long core_Mat_n_1step1__J(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1submat_1rr(IntPtr nativeObj, int rowRange_start, int rowRange_end, int colRange_start, int colRange_end);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1submat(IntPtr nativeObj, int roi_x, int roi_y, int roi_width, int roi_height);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1t(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern long core_Mat_n_1total(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_n_1type(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1zeros__III(int rows, int cols, int type);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_n_1zeros__DDI(double size_width, double size_height, int type);

		[DllImport("opencvforunity")]
		private static extern void core_Mat_n_1delete(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nPutD(IntPtr self, int row, int col, int count, [In][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] double[] data);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nPutF(IntPtr self, int row, int col, int count, [In][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] float[] data);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nPutI(IntPtr self, int row, int col, int count, [In][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] int[] data);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nPutS(IntPtr self, int row, int col, int count, [In][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] short[] data);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nPutB(IntPtr self, int row, int col, int count, [In][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] byte[] data);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nGetB(IntPtr self, int row, int col, int count, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] byte[] vals);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nGetS(IntPtr self, int row, int col, int count, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] short[] vals);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nGetI(IntPtr self, int row, int col, int count, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] int[] vals);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nGetF(IntPtr self, int row, int col, int count, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] float[] vals);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nGetD(IntPtr self, int row, int col, int count, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] double[] vals);

		[DllImport("opencvforunity")]
		private static extern int core_Mat_nGet(IntPtr self, int row, int col, int count, [In][Out][MarshalAs(UnmanagedType.LPArray, SizeParamIndex = 3)] double[] vals);

		[DllImport("opencvforunity")]
		private static extern IntPtr core_Mat_nDump(IntPtr self);
	}
}
