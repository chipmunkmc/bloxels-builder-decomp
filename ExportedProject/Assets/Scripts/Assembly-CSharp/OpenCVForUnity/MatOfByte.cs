using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfByte : Mat
	{
		private const int _depth = 0;

		private const int _channels = 1;

		public MatOfByte()
		{
		}

		protected MatOfByte(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(1, 0) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfByte(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(1, 0) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfByte(params byte[] a)
		{
			fromArray(a);
		}

		public static MatOfByte fromNativeAddr(IntPtr addr)
		{
			return new MatOfByte(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(0, 1));
			}
		}

		public void fromArray(params byte[] a)
		{
			if (a != null && a.Length != 0)
			{
				int elemNumber = a.Length;
				alloc(elemNumber);
				put(0, 0, a);
			}
		}

		public byte[] toArray()
		{
			int num = checkVector(1, 0);
			if (num < 0)
			{
				throw new CvException("Native Mat has unexpected type or size: " + ToString());
			}
			byte[] array = new byte[num];
			if (num == 0)
			{
				return array;
			}
			get(0, 0, array);
			return array;
		}

		public void fromList(List<byte> lb)
		{
			if (lb != null && lb.Count != 0)
			{
				byte[] a = lb.ToArray();
				fromArray(a);
			}
		}

		public List<byte> toList()
		{
			byte[] collection = toArray();
			return new List<byte>(collection);
		}
	}
}
