using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfDMatch : Mat
	{
		private const int _depth = 5;

		private const int _channels = 4;

		public MatOfDMatch()
		{
		}

		protected MatOfDMatch(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(4, 5) < 0)
			{
				throw new CvException("Incompatible Mat: " + ToString());
			}
		}

		public MatOfDMatch(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(4, 5) < 0)
			{
				throw new CvException("Incompatible Mat: " + ToString());
			}
		}

		public MatOfDMatch(params DMatch[] ap)
		{
			fromArray(ap);
		}

		public static MatOfDMatch fromNativeAddr(IntPtr addr)
		{
			return new MatOfDMatch(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(5, 4));
			}
		}

		public void fromArray(params DMatch[] a)
		{
			if (a != null && a.Length != 0)
			{
				int num = a.Length;
				alloc(num);
				float[] array = new float[num * 4];
				for (int i = 0; i < num; i++)
				{
					DMatch dMatch = a[i];
					array[4 * i] = dMatch.queryIdx;
					array[4 * i + 1] = dMatch.trainIdx;
					array[4 * i + 2] = dMatch.imgIdx;
					array[4 * i + 3] = dMatch.distance;
				}
				put(0, 0, array);
			}
		}

		public DMatch[] toArray()
		{
			int num = (int)total();
			DMatch[] array = new DMatch[num];
			if (num == 0)
			{
				return array;
			}
			float[] array2 = new float[num * 4];
			get(0, 0, array2);
			for (int i = 0; i < num; i++)
			{
				array[i] = new DMatch((int)array2[4 * i], (int)array2[4 * i + 1], (int)array2[4 * i + 2], array2[4 * i + 3]);
			}
			return array;
		}

		public void fromList(List<DMatch> ldm)
		{
			DMatch[] a = ldm.ToArray();
			fromArray(a);
		}

		public List<DMatch> toList()
		{
			DMatch[] collection = toArray();
			return new List<DMatch>(collection);
		}
	}
}
