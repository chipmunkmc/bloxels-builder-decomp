using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfDouble : Mat
	{
		private const int _depth = 6;

		private const int _channels = 1;

		public MatOfDouble()
		{
		}

		protected MatOfDouble(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(1, 6) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfDouble(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(1, 6) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfDouble(params double[] a)
		{
			fromArray(a);
		}

		public static MatOfDouble fromNativeAddr(IntPtr addr)
		{
			return new MatOfDouble(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(6, 1));
			}
		}

		public void fromArray(params double[] a)
		{
			if (a != null && a.Length != 0)
			{
				int elemNumber = a.Length;
				alloc(elemNumber);
				put(0, 0, a);
			}
		}

		public double[] toArray()
		{
			int num = checkVector(1, 6);
			if (num < 0)
			{
				throw new CvException("Native Mat has unexpected type or size: " + ToString());
			}
			double[] array = new double[num];
			if (num == 0)
			{
				return array;
			}
			get(0, 0, array);
			return array;
		}

		public void fromList(List<double> lb)
		{
			if (lb != null && lb.Count != 0)
			{
				fromArray(lb.ToArray());
			}
		}

		public List<double> toList()
		{
			double[] collection = toArray();
			return new List<double>(collection);
		}
	}
}
