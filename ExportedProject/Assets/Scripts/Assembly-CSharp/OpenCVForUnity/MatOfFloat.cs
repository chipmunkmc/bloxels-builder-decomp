using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfFloat : Mat
	{
		private const int _depth = 5;

		private const int _channels = 1;

		public MatOfFloat()
		{
		}

		protected MatOfFloat(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(1, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfFloat(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(1, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfFloat(params float[] a)
		{
			fromArray(a);
		}

		public static MatOfFloat fromNativeAddr(IntPtr addr)
		{
			return new MatOfFloat(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(5, 1));
			}
		}

		public void fromArray(params float[] a)
		{
			if (a != null && a.Length != 0)
			{
				int elemNumber = a.Length;
				alloc(elemNumber);
				put(0, 0, a);
			}
		}

		public float[] toArray()
		{
			int num = checkVector(1, 5);
			if (num < 0)
			{
				throw new CvException("Native Mat has unexpected type or size: " + ToString());
			}
			float[] array = new float[num];
			if (num == 0)
			{
				return array;
			}
			get(0, 0, array);
			return array;
		}

		public void fromList(List<float> lb)
		{
			if (lb != null && lb.Count != 0)
			{
				fromArray(lb.ToArray());
			}
		}

		public List<float> toList()
		{
			float[] collection = toArray();
			return new List<float>(collection);
		}
	}
}
