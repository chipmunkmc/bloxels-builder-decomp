using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfFloat4 : Mat
	{
		private const int _depth = 5;

		private const int _channels = 4;

		public MatOfFloat4()
		{
		}

		protected MatOfFloat4(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(4, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfFloat4(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(4, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfFloat4(params float[] a)
		{
			fromArray(a);
		}

		public static MatOfFloat4 fromNativeAddr(IntPtr addr)
		{
			return new MatOfFloat4(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(5, 4));
			}
		}

		public void fromArray(params float[] a)
		{
			if (a != null && a.Length != 0)
			{
				int elemNumber = a.Length / 4;
				alloc(elemNumber);
				put(0, 0, a);
			}
		}

		public float[] toArray()
		{
			int num = checkVector(4, 5);
			if (num < 0)
			{
				throw new CvException("Native Mat has unexpected type or size: " + ToString());
			}
			float[] array = new float[num * 4];
			if (num == 0)
			{
				return array;
			}
			get(0, 0, array);
			return array;
		}

		public void fromList(List<float> lb)
		{
			if (lb != null && lb.Count != 0)
			{
				fromArray(lb.ToArray());
			}
		}

		public List<float> toList()
		{
			float[] collection = toArray();
			return new List<float>(collection);
		}
	}
}
