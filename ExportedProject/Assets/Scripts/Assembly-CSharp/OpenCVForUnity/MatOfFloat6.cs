using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfFloat6 : Mat
	{
		private const int _depth = 5;

		private const int _channels = 6;

		public MatOfFloat6()
		{
		}

		protected MatOfFloat6(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(6, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfFloat6(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(6, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfFloat6(params float[] a)
		{
			fromArray(a);
		}

		public static MatOfFloat6 fromNativeAddr(IntPtr addr)
		{
			return new MatOfFloat6(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(5, 6));
			}
		}

		public void fromArray(params float[] a)
		{
			if (a != null && a.Length != 0)
			{
				int elemNumber = a.Length / 6;
				alloc(elemNumber);
				put(0, 0, a);
			}
		}

		public float[] toArray()
		{
			int num = checkVector(6, 5);
			if (num < 0)
			{
				throw new CvException("Native Mat has unexpected type or size: " + ToString());
			}
			float[] array = new float[num * 6];
			if (num == 0)
			{
				return array;
			}
			get(0, 0, array);
			return array;
		}

		public void fromList(List<float> lb)
		{
			if (lb != null && lb.Count != 0)
			{
				float[] array = lb.ToArray();
				float[] array2 = new float[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array2[i] = array[i];
				}
				fromArray(array2);
			}
		}

		public List<float> toList()
		{
			float[] array = toArray();
			float[] array2 = new float[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array2[i] = array[i];
			}
			return new List<float>(array2);
		}
	}
}
