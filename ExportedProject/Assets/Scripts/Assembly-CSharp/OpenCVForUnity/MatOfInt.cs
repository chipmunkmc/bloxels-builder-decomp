using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfInt : Mat
	{
		private const int _depth = 4;

		private const int _channels = 1;

		public MatOfInt()
		{
		}

		protected MatOfInt(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(1, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfInt(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(1, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfInt(params int[] a)
		{
			fromArray(a);
		}

		public static MatOfInt fromNativeAddr(IntPtr addr)
		{
			return new MatOfInt(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(4, 1));
			}
		}

		public void fromArray(params int[] a)
		{
			if (a != null && a.Length != 0)
			{
				int elemNumber = a.Length;
				alloc(elemNumber);
				put(0, 0, a);
			}
		}

		public int[] toArray()
		{
			int num = checkVector(1, 4);
			if (num < 0)
			{
				throw new CvException("Native Mat has unexpected type or size: " + ToString());
			}
			int[] array = new int[num];
			if (num == 0)
			{
				return array;
			}
			get(0, 0, array);
			return array;
		}

		public void fromList(List<int> lb)
		{
			if (lb != null && lb.Count != 0)
			{
				fromArray(lb.ToArray());
			}
		}

		public List<int> toList()
		{
			int[] collection = toArray();
			return new List<int>(collection);
		}
	}
}
