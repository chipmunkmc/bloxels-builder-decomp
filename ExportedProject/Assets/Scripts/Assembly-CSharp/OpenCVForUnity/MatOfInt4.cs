using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfInt4 : Mat
	{
		private const int _depth = 4;

		private const int _channels = 4;

		public MatOfInt4()
		{
		}

		protected MatOfInt4(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(4, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfInt4(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(4, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfInt4(params int[] a)
		{
			fromArray(a);
		}

		public static MatOfInt4 fromNativeAddr(IntPtr addr)
		{
			return new MatOfInt4(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(4, 4));
			}
		}

		public void fromArray(params int[] a)
		{
			if (a != null && a.Length != 0)
			{
				int elemNumber = a.Length / 4;
				alloc(elemNumber);
				put(0, 0, a);
			}
		}

		public int[] toArray()
		{
			int num = checkVector(4, 4);
			if (num < 0)
			{
				throw new CvException("Native Mat has unexpected type or size: " + ToString());
			}
			int[] array = new int[num * 4];
			if (num == 0)
			{
				return array;
			}
			get(0, 0, array);
			return array;
		}

		public void fromList(List<int> lb)
		{
			if (lb != null && lb.Count != 0)
			{
				int[] array = lb.ToArray();
				int[] array2 = new int[array.Length];
				for (int i = 0; i < array.Length; i++)
				{
					array2[i] = array[i];
				}
				fromArray(array2);
			}
		}

		public List<int> toList()
		{
			int[] array = toArray();
			int[] array2 = new int[array.Length];
			for (int i = 0; i < array.Length; i++)
			{
				array2[i] = array[i];
			}
			return new List<int>(array2);
		}
	}
}
