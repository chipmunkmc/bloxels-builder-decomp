using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfKeyPoint : Mat
	{
		private const int _depth = 5;

		private const int _channels = 7;

		public MatOfKeyPoint()
		{
		}

		protected MatOfKeyPoint(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(7, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfKeyPoint(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(7, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfKeyPoint(params KeyPoint[] a)
		{
			fromArray(a);
		}

		public static MatOfKeyPoint fromNativeAddr(IntPtr addr)
		{
			return new MatOfKeyPoint(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(5, 7));
			}
		}

		public void fromArray(params KeyPoint[] a)
		{
			if (a != null && a.Length != 0)
			{
				int num = a.Length;
				alloc(num);
				float[] array = new float[num * 7];
				for (int i = 0; i < num; i++)
				{
					KeyPoint keyPoint = a[i];
					array[7 * i] = (float)keyPoint.pt.x;
					array[7 * i + 1] = (float)keyPoint.pt.y;
					array[7 * i + 2] = keyPoint.size;
					array[7 * i + 3] = keyPoint.angle;
					array[7 * i + 4] = keyPoint.response;
					array[7 * i + 5] = keyPoint.octave;
					array[7 * i + 6] = keyPoint.class_id;
				}
				put(0, 0, array);
			}
		}

		public KeyPoint[] toArray()
		{
			int num = (int)total();
			KeyPoint[] array = new KeyPoint[num];
			if (num == 0)
			{
				return array;
			}
			float[] array2 = new float[num * 7];
			get(0, 0, array2);
			for (int i = 0; i < num; i++)
			{
				array[i] = new KeyPoint(array2[7 * i], array2[7 * i + 1], array2[7 * i + 2], array2[7 * i + 3], array2[7 * i + 4], (int)array2[7 * i + 5], (int)array2[7 * i + 6]);
			}
			return array;
		}

		public void fromList(List<KeyPoint> lkp)
		{
			KeyPoint[] a = lkp.ToArray();
			fromArray(a);
		}

		public List<KeyPoint> toList()
		{
			KeyPoint[] collection = toArray();
			return new List<KeyPoint>(collection);
		}
	}
}
