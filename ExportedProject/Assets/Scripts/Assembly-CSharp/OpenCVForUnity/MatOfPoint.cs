using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfPoint : Mat
	{
		private const int _depth = 4;

		private const int _channels = 2;

		public MatOfPoint()
		{
		}

		protected MatOfPoint(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(2, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(2, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint(params Point[] a)
		{
			fromArray(a);
		}

		public static MatOfPoint fromNativeAddr(IntPtr addr)
		{
			return new MatOfPoint(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(4, 2));
			}
		}

		public void fromArray(params Point[] a)
		{
			if (a != null && a.Length != 0)
			{
				int num = a.Length;
				alloc(num);
				int[] array = new int[num * 2];
				for (int i = 0; i < num; i++)
				{
					Point point = a[i];
					array[2 * i] = (int)point.x;
					array[2 * i + 1] = (int)point.y;
				}
				put(0, 0, array);
			}
		}

		public Point[] toArray()
		{
			int num = (int)total();
			Point[] array = new Point[num];
			if (num == 0)
			{
				return array;
			}
			int[] array2 = new int[num * 2];
			get(0, 0, array2);
			for (int i = 0; i < num; i++)
			{
				array[i] = new Point(array2[i * 2], array2[i * 2 + 1]);
			}
			return array;
		}

		public void fromList(List<Point> lp)
		{
			Point[] a = lp.ToArray();
			fromArray(a);
		}

		public List<Point> toList()
		{
			Point[] collection = toArray();
			return new List<Point>(collection);
		}
	}
}
