using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfPoint2f : Mat
	{
		private const int _depth = 5;

		private const int _channels = 2;

		public MatOfPoint2f()
		{
		}

		protected MatOfPoint2f(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(2, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint2f(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(2, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint2f(params Point[] a)
		{
			fromArray(a);
		}

		public static MatOfPoint2f fromNativeAddr(IntPtr addr)
		{
			return new MatOfPoint2f(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(5, 2));
			}
		}

		public void fromArray(params Point[] a)
		{
			if (a != null && a.Length != 0)
			{
				int num = a.Length;
				alloc(num);
				float[] array = new float[num * 2];
				for (int i = 0; i < num; i++)
				{
					Point point = a[i];
					array[2 * i] = (float)point.x;
					array[2 * i + 1] = (float)point.y;
				}
				put(0, 0, array);
			}
		}

		public Point[] toArray()
		{
			int num = (int)total();
			Point[] array = new Point[num];
			if (num == 0)
			{
				return array;
			}
			float[] array2 = new float[num * 2];
			get(0, 0, array2);
			for (int i = 0; i < num; i++)
			{
				array[i] = new Point(array2[i * 2], array2[i * 2 + 1]);
			}
			return array;
		}

		public void fromList(List<Point> lp)
		{
			Point[] a = lp.ToArray();
			fromArray(a);
		}

		public List<Point> toList()
		{
			Point[] collection = toArray();
			return new List<Point>(collection);
		}
	}
}
