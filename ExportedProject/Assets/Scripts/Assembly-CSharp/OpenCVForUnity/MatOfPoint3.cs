using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfPoint3 : Mat
	{
		private const int _depth = 4;

		private const int _channels = 3;

		public MatOfPoint3()
		{
		}

		protected MatOfPoint3(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(3, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint3(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(3, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint3(params Point3[] a)
		{
			fromArray(a);
		}

		public static MatOfPoint3 fromNativeAddr(IntPtr addr)
		{
			return new MatOfPoint3(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(4, 3));
			}
		}

		public void fromArray(params Point3[] a)
		{
			if (a != null && a.Length != 0)
			{
				int num = a.Length;
				alloc(num);
				int[] array = new int[num * 3];
				for (int i = 0; i < num; i++)
				{
					Point3 point = a[i];
					array[3 * i] = (int)point.x;
					array[3 * i + 1] = (int)point.y;
					array[3 * i + 2] = (int)point.z;
				}
				put(0, 0, array);
			}
		}

		public Point3[] toArray()
		{
			int num = (int)total();
			Point3[] array = new Point3[num];
			if (num == 0)
			{
				return array;
			}
			int[] array2 = new int[num * 3];
			get(0, 0, array2);
			for (int i = 0; i < num; i++)
			{
				array[i] = new Point3(array2[i * 3], array2[i * 3 + 1], array2[i * 3 + 2]);
			}
			return array;
		}

		public void fromList(List<Point3> lp)
		{
			Point3[] a = lp.ToArray();
			fromArray(a);
		}

		public List<Point3> toList()
		{
			Point3[] collection = toArray();
			return new List<Point3>(collection);
		}
	}
}
