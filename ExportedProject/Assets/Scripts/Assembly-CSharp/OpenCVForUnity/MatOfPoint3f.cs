using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfPoint3f : Mat
	{
		private const int _depth = 5;

		private const int _channels = 3;

		public MatOfPoint3f()
		{
		}

		protected MatOfPoint3f(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(3, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint3f(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(3, 5) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfPoint3f(params Point3[] a)
		{
			fromArray(a);
		}

		public static MatOfPoint3f fromNativeAddr(IntPtr addr)
		{
			return new MatOfPoint3f(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(5, 3));
			}
		}

		public void fromArray(params Point3[] a)
		{
			if (a != null && a.Length != 0)
			{
				int num = a.Length;
				alloc(num);
				float[] array = new float[num * 3];
				for (int i = 0; i < num; i++)
				{
					Point3 point = a[i];
					array[3 * i] = (float)point.x;
					array[3 * i + 1] = (float)point.y;
					array[3 * i + 2] = (float)point.z;
				}
				put(0, 0, array);
			}
		}

		public Point3[] toArray()
		{
			int num = (int)total();
			Point3[] array = new Point3[num];
			if (num == 0)
			{
				return array;
			}
			float[] array2 = new float[num * 3];
			get(0, 0, array2);
			for (int i = 0; i < num; i++)
			{
				array[i] = new Point3(array2[i * 3], array2[i * 3 + 1], array2[i * 3 + 2]);
			}
			return array;
		}

		public void fromList(List<Point3> lp)
		{
			Point3[] a = lp.ToArray();
			fromArray(a);
		}

		public List<Point3> toList()
		{
			Point3[] collection = toArray();
			return new List<Point3>(collection);
		}
	}
}
