using System;
using System.Collections.Generic;

namespace OpenCVForUnity
{
	public class MatOfRect : Mat
	{
		private const int _depth = 4;

		private const int _channels = 4;

		public MatOfRect()
		{
		}

		protected MatOfRect(IntPtr addr)
			: base(addr)
		{
			if (!empty() && checkVector(4, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfRect(Mat m)
			: base(m, Range.all())
		{
			if (m != null)
			{
				m.ThrowIfDisposed();
			}
			if (!empty() && checkVector(4, 4) < 0)
			{
				throw new CvException("Incompatible Mat");
			}
		}

		public MatOfRect(params Rect[] a)
		{
			fromArray(a);
		}

		public static MatOfRect fromNativeAddr(IntPtr addr)
		{
			return new MatOfRect(addr);
		}

		public void alloc(int elemNumber)
		{
			if (elemNumber > 0)
			{
				create(elemNumber, 1, CvType.makeType(4, 4));
			}
		}

		public void fromArray(params Rect[] a)
		{
			if (a != null && a.Length != 0)
			{
				int num = a.Length;
				alloc(num);
				int[] array = new int[num * 4];
				for (int i = 0; i < num; i++)
				{
					Rect rect = a[i];
					array[4 * i] = rect.x;
					array[4 * i + 1] = rect.y;
					array[4 * i + 2] = rect.width;
					array[4 * i + 3] = rect.height;
				}
				put(0, 0, array);
			}
		}

		public Rect[] toArray()
		{
			int num = (int)total();
			Rect[] array = new Rect[num];
			if (num == 0)
			{
				return array;
			}
			int[] array2 = new int[num * 4];
			get(0, 0, array2);
			for (int i = 0; i < num; i++)
			{
				array[i] = new Rect(array2[i * 4], array2[i * 4 + 1], array2[i * 4 + 2], array2[i * 4 + 3]);
			}
			return array;
		}

		public void fromList(List<Rect> lr)
		{
			Rect[] a = lr.ToArray();
			fromArray(a);
		}

		public List<Rect> toList()
		{
			Rect[] collection = toArray();
			return new List<Rect>(collection);
		}
	}
}
