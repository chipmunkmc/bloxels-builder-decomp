using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class MergeDebevec : MergeExposures
	{
		private const string LIBNAME = "opencvforunity";

		public MergeDebevec(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_MergeDebevec_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public override void process(List<Mat> src, Mat dst, Mat times, Mat response)
		{
			ThrowIfDisposed();
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (times != null)
			{
				times.ThrowIfDisposed();
			}
			if (response != null)
			{
				response.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			photo_MergeDebevec_process_10(nativeObj, mat.nativeObj, dst.nativeObj, times.nativeObj, response.nativeObj);
		}

		public void process(List<Mat> src, Mat dst, Mat times)
		{
			ThrowIfDisposed();
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (times != null)
			{
				times.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			photo_MergeDebevec_process_11(nativeObj, mat.nativeObj, dst.nativeObj, times.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void photo_MergeDebevec_process_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_nativeObj, IntPtr times_nativeObj, IntPtr response_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeDebevec_process_11(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_nativeObj, IntPtr times_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeDebevec_delete(IntPtr nativeObj);
	}
}
