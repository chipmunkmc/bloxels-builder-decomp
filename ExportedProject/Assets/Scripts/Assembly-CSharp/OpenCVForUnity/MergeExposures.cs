using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class MergeExposures : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected MergeExposures(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_MergeExposures_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public virtual void process(List<Mat> src, Mat dst, Mat times, Mat response)
		{
			ThrowIfDisposed();
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (times != null)
			{
				times.ThrowIfDisposed();
			}
			if (response != null)
			{
				response.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			photo_MergeExposures_process_10(nativeObj, mat.nativeObj, dst.nativeObj, times.nativeObj, response.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void photo_MergeExposures_process_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_nativeObj, IntPtr times_nativeObj, IntPtr response_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeExposures_delete(IntPtr nativeObj);
	}
}
