using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class MergeMertens : MergeExposures
	{
		private const string LIBNAME = "opencvforunity";

		public MergeMertens(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_MergeMertens_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getContrastWeight()
		{
			ThrowIfDisposed();
			return photo_MergeMertens_getContrastWeight_10(nativeObj);
		}

		public float getExposureWeight()
		{
			ThrowIfDisposed();
			return photo_MergeMertens_getExposureWeight_10(nativeObj);
		}

		public float getSaturationWeight()
		{
			ThrowIfDisposed();
			return photo_MergeMertens_getSaturationWeight_10(nativeObj);
		}

		public override void process(List<Mat> src, Mat dst, Mat times, Mat response)
		{
			ThrowIfDisposed();
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (times != null)
			{
				times.ThrowIfDisposed();
			}
			if (response != null)
			{
				response.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			photo_MergeMertens_process_10(nativeObj, mat.nativeObj, dst.nativeObj, times.nativeObj, response.nativeObj);
		}

		public void process(List<Mat> src, Mat dst)
		{
			ThrowIfDisposed();
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(src);
			photo_MergeMertens_process_11(nativeObj, mat.nativeObj, dst.nativeObj);
		}

		public void setContrastWeight(float contrast_weiht)
		{
			ThrowIfDisposed();
			photo_MergeMertens_setContrastWeight_10(nativeObj, contrast_weiht);
		}

		public void setExposureWeight(float exposure_weight)
		{
			ThrowIfDisposed();
			photo_MergeMertens_setExposureWeight_10(nativeObj, exposure_weight);
		}

		public void setSaturationWeight(float saturation_weight)
		{
			ThrowIfDisposed();
			photo_MergeMertens_setSaturationWeight_10(nativeObj, saturation_weight);
		}

		[DllImport("opencvforunity")]
		private static extern float photo_MergeMertens_getContrastWeight_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_MergeMertens_getExposureWeight_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_MergeMertens_getSaturationWeight_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeMertens_process_10(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_nativeObj, IntPtr times_nativeObj, IntPtr response_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeMertens_process_11(IntPtr nativeObj, IntPtr src_mat_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeMertens_setContrastWeight_10(IntPtr nativeObj, float contrast_weiht);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeMertens_setExposureWeight_10(IntPtr nativeObj, float exposure_weight);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeMertens_setSaturationWeight_10(IntPtr nativeObj, float saturation_weight);

		[DllImport("opencvforunity")]
		private static extern void photo_MergeMertens_delete(IntPtr nativeObj);
	}
}
