using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class MinDistancePredictCollector : PredictCollector
	{
		private const string LIBNAME = "opencvforunity";

		protected MinDistancePredictCollector(IntPtr addr)
			: base(addr)
		{
		}

		public MinDistancePredictCollector(double threshold)
			: base(face_MinDistancePredictCollector_MinDistancePredictCollector_10(threshold))
		{
		}

		public MinDistancePredictCollector()
			: base(face_MinDistancePredictCollector_MinDistancePredictCollector_11())
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						face_MinDistancePredictCollector_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public static MinDistancePredictCollector create(double threshold)
		{
			return new MinDistancePredictCollector(face_MinDistancePredictCollector_create_10(threshold));
		}

		public static MinDistancePredictCollector create()
		{
			return new MinDistancePredictCollector(face_MinDistancePredictCollector_create_11());
		}

		public override bool emit(int label, double dist, int state)
		{
			ThrowIfDisposed();
			return face_MinDistancePredictCollector_emit_10(nativeObj, label, dist, state);
		}

		public override bool emit(int label, double dist)
		{
			ThrowIfDisposed();
			return face_MinDistancePredictCollector_emit_11(nativeObj, label, dist);
		}

		public double getDist()
		{
			ThrowIfDisposed();
			return face_MinDistancePredictCollector_getDist_10(nativeObj);
		}

		public int getLabel()
		{
			ThrowIfDisposed();
			return face_MinDistancePredictCollector_getLabel_10(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr face_MinDistancePredictCollector_MinDistancePredictCollector_10(double threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_MinDistancePredictCollector_MinDistancePredictCollector_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr face_MinDistancePredictCollector_create_10(double threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr face_MinDistancePredictCollector_create_11();

		[DllImport("opencvforunity")]
		private static extern bool face_MinDistancePredictCollector_emit_10(IntPtr nativeObj, int label, double dist, int state);

		[DllImport("opencvforunity")]
		private static extern bool face_MinDistancePredictCollector_emit_11(IntPtr nativeObj, int label, double dist);

		[DllImport("opencvforunity")]
		private static extern double face_MinDistancePredictCollector_getDist_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int face_MinDistancePredictCollector_getLabel_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void face_MinDistancePredictCollector_delete(IntPtr nativeObj);
	}
}
