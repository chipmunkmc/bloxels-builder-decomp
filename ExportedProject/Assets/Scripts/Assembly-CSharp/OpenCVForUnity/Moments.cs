using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class Moments
	{
		public double m00;

		public double m10;

		public double m01;

		public double m20;

		public double m11;

		public double m02;

		public double m30;

		public double m21;

		public double m12;

		public double m03;

		public double mu20;

		public double mu11;

		public double mu02;

		public double mu30;

		public double mu21;

		public double mu12;

		public double mu03;

		public double nu20;

		public double nu11;

		public double nu02;

		public double nu30;

		public double nu21;

		public double nu12;

		public double nu03;

		public Moments(double m00, double m10, double m01, double m20, double m11, double m02, double m30, double m21, double m12, double m03)
		{
			this.m00 = m00;
			this.m10 = m10;
			this.m01 = m01;
			this.m20 = m20;
			this.m11 = m11;
			this.m02 = m02;
			this.m30 = m30;
			this.m21 = m21;
			this.m12 = m12;
			this.m03 = m03;
			completeState();
		}

		public Moments()
			: this(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0)
		{
		}

		public Moments(double[] vals)
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				m00 = ((vals.Length > 0) ? ((int)vals[0]) : 0);
				m10 = ((vals.Length > 1) ? ((int)vals[1]) : 0);
				m01 = ((vals.Length > 2) ? ((int)vals[2]) : 0);
				m20 = ((vals.Length > 3) ? ((int)vals[3]) : 0);
				m11 = ((vals.Length > 4) ? ((int)vals[4]) : 0);
				m02 = ((vals.Length > 5) ? ((int)vals[5]) : 0);
				m30 = ((vals.Length > 6) ? ((int)vals[6]) : 0);
				m21 = ((vals.Length > 7) ? ((int)vals[7]) : 0);
				m12 = ((vals.Length > 8) ? ((int)vals[8]) : 0);
				m03 = ((vals.Length > 9) ? ((int)vals[9]) : 0);
				completeState();
				return;
			}
			m00 = 0.0;
			m10 = 0.0;
			m01 = 0.0;
			m20 = 0.0;
			m11 = 0.0;
			m02 = 0.0;
			m30 = 0.0;
			m21 = 0.0;
			m12 = 0.0;
			m03 = 0.0;
			mu20 = 0.0;
			mu11 = 0.0;
			mu02 = 0.0;
			mu30 = 0.0;
			mu21 = 0.0;
			mu12 = 0.0;
			mu03 = 0.0;
			nu20 = 0.0;
			nu11 = 0.0;
			nu02 = 0.0;
			nu30 = 0.0;
			nu21 = 0.0;
			nu12 = 0.0;
			nu03 = 0.0;
		}

		public override string ToString()
		{
			return "Moments [ \nm00=" + m00 + ", \nm10=" + m10 + ", m01=" + m01 + ", \nm20=" + m20 + ", m11=" + m11 + ", m02=" + m02 + ", \nm30=" + m30 + ", m21=" + m21 + ", m12=" + m12 + ", m03=" + m03 + ", \nmu20=" + mu20 + ", mu11=" + mu11 + ", mu02=" + mu02 + ", \nmu30=" + mu30 + ", mu21=" + mu21 + ", mu12=" + mu12 + ", mu03=" + mu03 + ", \nnu20=" + nu20 + ", nu11=" + nu11 + ", nu02=" + nu02 + ", \nnu30=" + nu30 + ", nu21=" + nu21 + ", nu12=" + nu12 + ", nu03=" + nu03 + ", \n]";
		}

		protected void completeState()
		{
			double num = 0.0;
			double num2 = 0.0;
			double num3 = 0.0;
			if (Math.Abs(m00) > 1E-08)
			{
				num3 = 1.0 / m00;
				num = m10 * num3;
				num2 = m01 * num3;
			}
			double num4 = m20 - m10 * num;
			double num5 = m11 - m10 * num2;
			double num6 = m02 - m01 * num2;
			mu20 = num4;
			mu11 = num5;
			mu02 = num6;
			mu30 = m30 - num * (3.0 * num4 + num * m10);
			num5 += num5;
			mu21 = m21 - num * (num5 + num * m01) - num2 * num4;
			mu12 = m12 - num2 * (num5 + num2 * m10) - num * num6;
			mu03 = m03 - num2 * (3.0 * num6 + num2 * m01);
			double num7 = Math.Sqrt(Math.Abs(num3));
			double num8 = num3 * num3;
			double num9 = num8 * num7;
			nu20 = mu20 * num8;
			nu11 = mu11 * num8;
			nu02 = mu02 * num8;
			nu30 = mu30 * num9;
			nu21 = mu21 * num9;
			nu12 = mu12 * num9;
			nu03 = mu03 * num9;
		}

		public double get_m00()
		{
			return m00;
		}

		public double get_m10()
		{
			return m10;
		}

		public double get_m01()
		{
			return m01;
		}

		public double get_m20()
		{
			return m20;
		}

		public double get_m11()
		{
			return m11;
		}

		public double get_m02()
		{
			return m02;
		}

		public double get_m30()
		{
			return m30;
		}

		public double get_m21()
		{
			return m21;
		}

		public double get_m12()
		{
			return m12;
		}

		public double get_m03()
		{
			return m03;
		}

		public double get_mu20()
		{
			return mu20;
		}

		public double get_mu11()
		{
			return mu11;
		}

		public double get_mu02()
		{
			return mu02;
		}

		public double get_mu30()
		{
			return mu30;
		}

		public double get_mu21()
		{
			return mu21;
		}

		public double get_mu12()
		{
			return mu12;
		}

		public double get_mu03()
		{
			return mu03;
		}

		public double get_nu20()
		{
			return nu20;
		}

		public double get_nu11()
		{
			return nu11;
		}

		public double get_nu02()
		{
			return nu02;
		}

		public double get_nu30()
		{
			return nu30;
		}

		public double get_nu21()
		{
			return nu21;
		}

		public double get_nu12()
		{
			return nu12;
		}

		public double get_nu03()
		{
			return nu03;
		}

		public void set_m00(double m00)
		{
			this.m00 = m00;
		}

		public void set_m10(double m10)
		{
			this.m10 = m10;
		}

		public void set_m01(double m01)
		{
			this.m01 = m01;
		}

		public void set_m20(double m20)
		{
			this.m20 = m20;
		}

		public void set_m11(double m11)
		{
			this.m11 = m11;
		}

		public void set_m02(double m02)
		{
			this.m02 = m02;
		}

		public void set_m30(double m30)
		{
			this.m30 = m30;
		}

		public void set_m21(double m21)
		{
			this.m21 = m21;
		}

		public void set_m12(double m12)
		{
			this.m12 = m12;
		}

		public void set_m03(double m03)
		{
			this.m03 = m03;
		}

		public void set_mu20(double mu20)
		{
			this.mu20 = mu20;
		}

		public void set_mu11(double mu11)
		{
			this.mu11 = mu11;
		}

		public void set_mu02(double mu02)
		{
			this.mu02 = mu02;
		}

		public void set_mu30(double mu30)
		{
			this.mu30 = mu30;
		}

		public void set_mu21(double mu21)
		{
			this.mu21 = mu21;
		}

		public void set_mu12(double mu12)
		{
			this.mu12 = mu12;
		}

		public void set_mu03(double mu03)
		{
			this.mu03 = mu03;
		}

		public void set_nu20(double nu20)
		{
			this.nu20 = nu20;
		}

		public void set_nu11(double nu11)
		{
			this.nu11 = nu11;
		}

		public void set_nu02(double nu02)
		{
			this.nu02 = nu02;
		}

		public void set_nu30(double nu30)
		{
			this.nu30 = nu30;
		}

		public void set_nu21(double nu21)
		{
			this.nu21 = nu21;
		}

		public void set_nu12(double nu12)
		{
			this.nu12 = nu12;
		}

		public void set_nu03(double nu03)
		{
			this.nu03 = nu03;
		}
	}
}
