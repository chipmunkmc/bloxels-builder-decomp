using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class NormalBayesClassifier : StatModel
	{
		private const string LIBNAME = "opencvforunity";

		protected NormalBayesClassifier(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_NormalBayesClassifier_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public static NormalBayesClassifier create()
		{
			return new NormalBayesClassifier(ml_NormalBayesClassifier_create_10());
		}

		public float predictProb(Mat inputs, Mat outputs, Mat outputProbs, int flags)
		{
			ThrowIfDisposed();
			if (inputs != null)
			{
				inputs.ThrowIfDisposed();
			}
			if (outputs != null)
			{
				outputs.ThrowIfDisposed();
			}
			if (outputProbs != null)
			{
				outputProbs.ThrowIfDisposed();
			}
			return ml_NormalBayesClassifier_predictProb_10(nativeObj, inputs.nativeObj, outputs.nativeObj, outputProbs.nativeObj, flags);
		}

		public float predictProb(Mat inputs, Mat outputs, Mat outputProbs)
		{
			ThrowIfDisposed();
			if (inputs != null)
			{
				inputs.ThrowIfDisposed();
			}
			if (outputs != null)
			{
				outputs.ThrowIfDisposed();
			}
			if (outputProbs != null)
			{
				outputProbs.ThrowIfDisposed();
			}
			return ml_NormalBayesClassifier_predictProb_11(nativeObj, inputs.nativeObj, outputs.nativeObj, outputProbs.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_NormalBayesClassifier_create_10();

		[DllImport("opencvforunity")]
		private static extern float ml_NormalBayesClassifier_predictProb_10(IntPtr nativeObj, IntPtr inputs_nativeObj, IntPtr outputs_nativeObj, IntPtr outputProbs_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern float ml_NormalBayesClassifier_predictProb_11(IntPtr nativeObj, IntPtr inputs_nativeObj, IntPtr outputs_nativeObj, IntPtr outputProbs_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_NormalBayesClassifier_delete(IntPtr nativeObj);
	}
}
