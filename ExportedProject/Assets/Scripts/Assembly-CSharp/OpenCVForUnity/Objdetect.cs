using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Objdetect
	{
		public const int CASCADE_DO_CANNY_PRUNING = 1;

		public const int CASCADE_SCALE_IMAGE = 2;

		public const int CASCADE_FIND_BIGGEST_OBJECT = 4;

		public const int CASCADE_DO_ROUGH_SEARCH = 8;

		private const string LIBNAME = "opencvforunity";

		public static void groupRectangles(MatOfRect rectList, MatOfInt weights, int groupThreshold, double eps)
		{
			if (rectList != null)
			{
				rectList.ThrowIfDisposed();
			}
			if (weights != null)
			{
				weights.ThrowIfDisposed();
			}
			objdetect_Objdetect_groupRectangles_10(rectList.nativeObj, weights.nativeObj, groupThreshold, eps);
		}

		public static void groupRectangles(MatOfRect rectList, MatOfInt weights, int groupThreshold)
		{
			if (rectList != null)
			{
				rectList.ThrowIfDisposed();
			}
			if (weights != null)
			{
				weights.ThrowIfDisposed();
			}
			objdetect_Objdetect_groupRectangles_11(rectList.nativeObj, weights.nativeObj, groupThreshold);
		}

		[DllImport("opencvforunity")]
		private static extern void objdetect_Objdetect_groupRectangles_10(IntPtr rectList_mat_nativeObj, IntPtr weights_mat_nativeObj, int groupThreshold, double eps);

		[DllImport("opencvforunity")]
		private static extern void objdetect_Objdetect_groupRectangles_11(IntPtr rectList_mat_nativeObj, IntPtr weights_mat_nativeObj, int groupThreshold);
	}
}
