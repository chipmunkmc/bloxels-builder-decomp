using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Optflow
	{
		private const string LIBNAME = "opencvforunity";

		public static Mat readOpticalFlow(string path)
		{
			return new Mat(optflow_Optflow_readOpticalFlow_10(path));
		}

		public static bool writeOpticalFlow(string path, Mat flow)
		{
			if (flow != null)
			{
				flow.ThrowIfDisposed();
			}
			return optflow_Optflow_writeOpticalFlow_10(path, flow.nativeObj);
		}

		public static double calcGlobalOrientation(Mat orientation, Mat mask, Mat mhi, double timestamp, double duration)
		{
			if (orientation != null)
			{
				orientation.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (mhi != null)
			{
				mhi.ThrowIfDisposed();
			}
			return optflow_Optflow_calcGlobalOrientation_10(orientation.nativeObj, mask.nativeObj, mhi.nativeObj, timestamp, duration);
		}

		public static void calcMotionGradient(Mat mhi, Mat mask, Mat orientation, double delta1, double delta2, int apertureSize)
		{
			if (mhi != null)
			{
				mhi.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (orientation != null)
			{
				orientation.ThrowIfDisposed();
			}
			optflow_Optflow_calcMotionGradient_10(mhi.nativeObj, mask.nativeObj, orientation.nativeObj, delta1, delta2, apertureSize);
		}

		public static void calcMotionGradient(Mat mhi, Mat mask, Mat orientation, double delta1, double delta2)
		{
			if (mhi != null)
			{
				mhi.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (orientation != null)
			{
				orientation.ThrowIfDisposed();
			}
			optflow_Optflow_calcMotionGradient_11(mhi.nativeObj, mask.nativeObj, orientation.nativeObj, delta1, delta2);
		}

		public static void segmentMotion(Mat mhi, Mat segmask, MatOfRect boundingRects, double timestamp, double segThresh)
		{
			if (mhi != null)
			{
				mhi.ThrowIfDisposed();
			}
			if (segmask != null)
			{
				segmask.ThrowIfDisposed();
			}
			if (boundingRects != null)
			{
				boundingRects.ThrowIfDisposed();
			}
			optflow_Optflow_segmentMotion_10(mhi.nativeObj, segmask.nativeObj, boundingRects.nativeObj, timestamp, segThresh);
		}

		public static void updateMotionHistory(Mat silhouette, Mat mhi, double timestamp, double duration)
		{
			if (silhouette != null)
			{
				silhouette.ThrowIfDisposed();
			}
			if (mhi != null)
			{
				mhi.ThrowIfDisposed();
			}
			optflow_Optflow_updateMotionHistory_10(silhouette.nativeObj, mhi.nativeObj, timestamp, duration);
		}

		public static void calcOpticalFlowSF(Mat from, Mat to, Mat flow, int layers, int averaging_block_size, int max_flow, double sigma_dist, double sigma_color, int postprocess_window, double sigma_dist_fix, double sigma_color_fix, double occ_thr, int upscale_averaging_radius, double upscale_sigma_dist, double upscale_sigma_color, double speed_up_thr)
		{
			if (from != null)
			{
				from.ThrowIfDisposed();
			}
			if (to != null)
			{
				to.ThrowIfDisposed();
			}
			if (flow != null)
			{
				flow.ThrowIfDisposed();
			}
			optflow_Optflow_calcOpticalFlowSF_10(from.nativeObj, to.nativeObj, flow.nativeObj, layers, averaging_block_size, max_flow, sigma_dist, sigma_color, postprocess_window, sigma_dist_fix, sigma_color_fix, occ_thr, upscale_averaging_radius, upscale_sigma_dist, upscale_sigma_color, speed_up_thr);
		}

		public static void calcOpticalFlowSF(Mat from, Mat to, Mat flow, int layers, int averaging_block_size, int max_flow)
		{
			if (from != null)
			{
				from.ThrowIfDisposed();
			}
			if (to != null)
			{
				to.ThrowIfDisposed();
			}
			if (flow != null)
			{
				flow.ThrowIfDisposed();
			}
			optflow_Optflow_calcOpticalFlowSF_11(from.nativeObj, to.nativeObj, flow.nativeObj, layers, averaging_block_size, max_flow);
		}

		public static void calcOpticalFlowSparseToDense(Mat from, Mat to, Mat flow, int grid_step, int k, float sigma, bool use_post_proc, float fgs_lambda, float fgs_sigma)
		{
			if (from != null)
			{
				from.ThrowIfDisposed();
			}
			if (to != null)
			{
				to.ThrowIfDisposed();
			}
			if (flow != null)
			{
				flow.ThrowIfDisposed();
			}
			optflow_Optflow_calcOpticalFlowSparseToDense_10(from.nativeObj, to.nativeObj, flow.nativeObj, grid_step, k, sigma, use_post_proc, fgs_lambda, fgs_sigma);
		}

		public static void calcOpticalFlowSparseToDense(Mat from, Mat to, Mat flow)
		{
			if (from != null)
			{
				from.ThrowIfDisposed();
			}
			if (to != null)
			{
				to.ThrowIfDisposed();
			}
			if (flow != null)
			{
				flow.ThrowIfDisposed();
			}
			optflow_Optflow_calcOpticalFlowSparseToDense_11(from.nativeObj, to.nativeObj, flow.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr optflow_Optflow_readOpticalFlow_10(string path);

		[DllImport("opencvforunity")]
		private static extern bool optflow_Optflow_writeOpticalFlow_10(string path, IntPtr flow_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double optflow_Optflow_calcGlobalOrientation_10(IntPtr orientation_nativeObj, IntPtr mask_nativeObj, IntPtr mhi_nativeObj, double timestamp, double duration);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_calcMotionGradient_10(IntPtr mhi_nativeObj, IntPtr mask_nativeObj, IntPtr orientation_nativeObj, double delta1, double delta2, int apertureSize);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_calcMotionGradient_11(IntPtr mhi_nativeObj, IntPtr mask_nativeObj, IntPtr orientation_nativeObj, double delta1, double delta2);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_segmentMotion_10(IntPtr mhi_nativeObj, IntPtr segmask_nativeObj, IntPtr boundingRects_mat_nativeObj, double timestamp, double segThresh);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_updateMotionHistory_10(IntPtr silhouette_nativeObj, IntPtr mhi_nativeObj, double timestamp, double duration);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_calcOpticalFlowSF_10(IntPtr from_nativeObj, IntPtr to_nativeObj, IntPtr flow_nativeObj, int layers, int averaging_block_size, int max_flow, double sigma_dist, double sigma_color, int postprocess_window, double sigma_dist_fix, double sigma_color_fix, double occ_thr, int upscale_averaging_radius, double upscale_sigma_dist, double upscale_sigma_color, double speed_up_thr);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_calcOpticalFlowSF_11(IntPtr from_nativeObj, IntPtr to_nativeObj, IntPtr flow_nativeObj, int layers, int averaging_block_size, int max_flow);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_calcOpticalFlowSparseToDense_10(IntPtr from_nativeObj, IntPtr to_nativeObj, IntPtr flow_nativeObj, int grid_step, int k, float sigma, bool use_post_proc, float fgs_lambda, float fgs_sigma);

		[DllImport("opencvforunity")]
		private static extern void optflow_Optflow_calcOpticalFlowSparseToDense_11(IntPtr from_nativeObj, IntPtr to_nativeObj, IntPtr flow_nativeObj);
	}
}
