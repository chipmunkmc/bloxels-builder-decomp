using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Photo
	{
		private const int CV_INPAINT_NS = 0;

		private const int CV_INPAINT_TELEA = 1;

		public const int INPAINT_NS = 0;

		public const int INPAINT_TELEA = 1;

		public const int NORMAL_CLONE = 1;

		public const int MIXED_CLONE = 2;

		public const int MONOCHROME_TRANSFER = 3;

		public const int RECURS_FILTER = 1;

		public const int NORMCONV_FILTER = 2;

		public const int LDR_SIZE = 256;

		private const string LIBNAME = "opencvforunity";

		public static AlignMTB createAlignMTB(int max_bits, int exclude_range, bool cut)
		{
			return new AlignMTB(photo_Photo_createAlignMTB_10(max_bits, exclude_range, cut));
		}

		public static AlignMTB createAlignMTB()
		{
			return new AlignMTB(photo_Photo_createAlignMTB_11());
		}

		public static CalibrateDebevec createCalibrateDebevec(int samples, float lambda, bool random)
		{
			return new CalibrateDebevec(photo_Photo_createCalibrateDebevec_10(samples, lambda, random));
		}

		public static CalibrateDebevec createCalibrateDebevec()
		{
			return new CalibrateDebevec(photo_Photo_createCalibrateDebevec_11());
		}

		public static CalibrateRobertson createCalibrateRobertson(int max_iter, float threshold)
		{
			return new CalibrateRobertson(photo_Photo_createCalibrateRobertson_10(max_iter, threshold));
		}

		public static CalibrateRobertson createCalibrateRobertson()
		{
			return new CalibrateRobertson(photo_Photo_createCalibrateRobertson_11());
		}

		public static MergeDebevec createMergeDebevec()
		{
			return new MergeDebevec(photo_Photo_createMergeDebevec_10());
		}

		public static MergeMertens createMergeMertens(float contrast_weight, float saturation_weight, float exposure_weight)
		{
			return new MergeMertens(photo_Photo_createMergeMertens_10(contrast_weight, saturation_weight, exposure_weight));
		}

		public static MergeMertens createMergeMertens()
		{
			return new MergeMertens(photo_Photo_createMergeMertens_11());
		}

		public static MergeRobertson createMergeRobertson()
		{
			return new MergeRobertson(photo_Photo_createMergeRobertson_10());
		}

		public static Tonemap createTonemap(float gamma)
		{
			return new Tonemap(photo_Photo_createTonemap_10(gamma));
		}

		public static Tonemap createTonemap()
		{
			return new Tonemap(photo_Photo_createTonemap_11());
		}

		public static TonemapDrago createTonemapDrago(float gamma, float saturation, float bias)
		{
			return new TonemapDrago(photo_Photo_createTonemapDrago_10(gamma, saturation, bias));
		}

		public static TonemapDrago createTonemapDrago()
		{
			return new TonemapDrago(photo_Photo_createTonemapDrago_11());
		}

		public static TonemapDurand createTonemapDurand(float gamma, float contrast, float saturation, float sigma_space, float sigma_color)
		{
			return new TonemapDurand(photo_Photo_createTonemapDurand_10(gamma, contrast, saturation, sigma_space, sigma_color));
		}

		public static TonemapDurand createTonemapDurand()
		{
			return new TonemapDurand(photo_Photo_createTonemapDurand_11());
		}

		public static TonemapMantiuk createTonemapMantiuk(float gamma, float scale, float saturation)
		{
			return new TonemapMantiuk(photo_Photo_createTonemapMantiuk_10(gamma, scale, saturation));
		}

		public static TonemapMantiuk createTonemapMantiuk()
		{
			return new TonemapMantiuk(photo_Photo_createTonemapMantiuk_11());
		}

		public static TonemapReinhard createTonemapReinhard(float gamma, float intensity, float light_adapt, float color_adapt)
		{
			return new TonemapReinhard(photo_Photo_createTonemapReinhard_10(gamma, intensity, light_adapt, color_adapt));
		}

		public static TonemapReinhard createTonemapReinhard()
		{
			return new TonemapReinhard(photo_Photo_createTonemapReinhard_11());
		}

		public static void colorChange(Mat src, Mat mask, Mat dst, float red_mul, float green_mul, float blue_mul)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_colorChange_10(src.nativeObj, mask.nativeObj, dst.nativeObj, red_mul, green_mul, blue_mul);
		}

		public static void colorChange(Mat src, Mat mask, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_colorChange_11(src.nativeObj, mask.nativeObj, dst.nativeObj);
		}

		public static void decolor(Mat src, Mat grayscale, Mat color_boost)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (grayscale != null)
			{
				grayscale.ThrowIfDisposed();
			}
			if (color_boost != null)
			{
				color_boost.ThrowIfDisposed();
			}
			photo_Photo_decolor_10(src.nativeObj, grayscale.nativeObj, color_boost.nativeObj);
		}

		public static void denoise_TVL1(List<Mat> observations, Mat result, double lambda, int niters)
		{
			if (result != null)
			{
				result.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(observations);
			photo_Photo_denoise_1TVL1_10(mat.nativeObj, result.nativeObj, lambda, niters);
		}

		public static void denoise_TVL1(List<Mat> observations, Mat result)
		{
			if (result != null)
			{
				result.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(observations);
			photo_Photo_denoise_1TVL1_11(mat.nativeObj, result.nativeObj);
		}

		public static void detailEnhance(Mat src, Mat dst, float sigma_s, float sigma_r)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_detailEnhance_10(src.nativeObj, dst.nativeObj, sigma_s, sigma_r);
		}

		public static void detailEnhance(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_detailEnhance_11(src.nativeObj, dst.nativeObj);
		}

		public static void edgePreservingFilter(Mat src, Mat dst, int flags, float sigma_s, float sigma_r)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_edgePreservingFilter_10(src.nativeObj, dst.nativeObj, flags, sigma_s, sigma_r);
		}

		public static void edgePreservingFilter(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_edgePreservingFilter_11(src.nativeObj, dst.nativeObj);
		}

		public static void fastNlMeansDenoising(Mat src, Mat dst, float h, int templateWindowSize, int searchWindowSize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_fastNlMeansDenoising_10(src.nativeObj, dst.nativeObj, h, templateWindowSize, searchWindowSize);
		}

		public static void fastNlMeansDenoising(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_fastNlMeansDenoising_11(src.nativeObj, dst.nativeObj);
		}

		public static void fastNlMeansDenoising(Mat src, Mat dst, MatOfFloat h, int templateWindowSize, int searchWindowSize, int normType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (h != null)
			{
				h.ThrowIfDisposed();
			}
			photo_Photo_fastNlMeansDenoising_12(src.nativeObj, dst.nativeObj, h.nativeObj, templateWindowSize, searchWindowSize, normType);
		}

		public static void fastNlMeansDenoising(Mat src, Mat dst, MatOfFloat h)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (h != null)
			{
				h.ThrowIfDisposed();
			}
			photo_Photo_fastNlMeansDenoising_13(src.nativeObj, dst.nativeObj, h.nativeObj);
		}

		public static void fastNlMeansDenoisingColored(Mat src, Mat dst, float h, float hColor, int templateWindowSize, int searchWindowSize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_fastNlMeansDenoisingColored_10(src.nativeObj, dst.nativeObj, h, hColor, templateWindowSize, searchWindowSize);
		}

		public static void fastNlMeansDenoisingColored(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_fastNlMeansDenoisingColored_11(src.nativeObj, dst.nativeObj);
		}

		public static void fastNlMeansDenoisingColoredMulti(List<Mat> srcImgs, Mat dst, int imgToDenoiseIndex, int temporalWindowSize, float h, float hColor, int templateWindowSize, int searchWindowSize)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(srcImgs);
			photo_Photo_fastNlMeansDenoisingColoredMulti_10(mat.nativeObj, dst.nativeObj, imgToDenoiseIndex, temporalWindowSize, h, hColor, templateWindowSize, searchWindowSize);
		}

		public static void fastNlMeansDenoisingColoredMulti(List<Mat> srcImgs, Mat dst, int imgToDenoiseIndex, int temporalWindowSize)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(srcImgs);
			photo_Photo_fastNlMeansDenoisingColoredMulti_11(mat.nativeObj, dst.nativeObj, imgToDenoiseIndex, temporalWindowSize);
		}

		public static void fastNlMeansDenoisingMulti(List<Mat> srcImgs, Mat dst, int imgToDenoiseIndex, int temporalWindowSize, float h, int templateWindowSize, int searchWindowSize)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(srcImgs);
			photo_Photo_fastNlMeansDenoisingMulti_10(mat.nativeObj, dst.nativeObj, imgToDenoiseIndex, temporalWindowSize, h, templateWindowSize, searchWindowSize);
		}

		public static void fastNlMeansDenoisingMulti(List<Mat> srcImgs, Mat dst, int imgToDenoiseIndex, int temporalWindowSize)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(srcImgs);
			photo_Photo_fastNlMeansDenoisingMulti_11(mat.nativeObj, dst.nativeObj, imgToDenoiseIndex, temporalWindowSize);
		}

		public static void fastNlMeansDenoisingMulti(List<Mat> srcImgs, Mat dst, int imgToDenoiseIndex, int temporalWindowSize, MatOfFloat h, int templateWindowSize, int searchWindowSize, int normType)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (h != null)
			{
				h.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(srcImgs);
			photo_Photo_fastNlMeansDenoisingMulti_12(mat.nativeObj, dst.nativeObj, imgToDenoiseIndex, temporalWindowSize, h.nativeObj, templateWindowSize, searchWindowSize, normType);
		}

		public static void fastNlMeansDenoisingMulti(List<Mat> srcImgs, Mat dst, int imgToDenoiseIndex, int temporalWindowSize, MatOfFloat h)
		{
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (h != null)
			{
				h.ThrowIfDisposed();
			}
			Mat mat = Converters.vector_Mat_to_Mat(srcImgs);
			photo_Photo_fastNlMeansDenoisingMulti_13(mat.nativeObj, dst.nativeObj, imgToDenoiseIndex, temporalWindowSize, h.nativeObj);
		}

		public static void illuminationChange(Mat src, Mat mask, Mat dst, float alpha, float beta)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_illuminationChange_10(src.nativeObj, mask.nativeObj, dst.nativeObj, alpha, beta);
		}

		public static void illuminationChange(Mat src, Mat mask, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_illuminationChange_11(src.nativeObj, mask.nativeObj, dst.nativeObj);
		}

		public static void inpaint(Mat src, Mat inpaintMask, Mat dst, double inpaintRadius, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (inpaintMask != null)
			{
				inpaintMask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_inpaint_10(src.nativeObj, inpaintMask.nativeObj, dst.nativeObj, inpaintRadius, flags);
		}

		public static void pencilSketch(Mat src, Mat dst1, Mat dst2, float sigma_s, float sigma_r, float shade_factor)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst1 != null)
			{
				dst1.ThrowIfDisposed();
			}
			if (dst2 != null)
			{
				dst2.ThrowIfDisposed();
			}
			photo_Photo_pencilSketch_10(src.nativeObj, dst1.nativeObj, dst2.nativeObj, sigma_s, sigma_r, shade_factor);
		}

		public static void pencilSketch(Mat src, Mat dst1, Mat dst2)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst1 != null)
			{
				dst1.ThrowIfDisposed();
			}
			if (dst2 != null)
			{
				dst2.ThrowIfDisposed();
			}
			photo_Photo_pencilSketch_11(src.nativeObj, dst1.nativeObj, dst2.nativeObj);
		}

		public static void seamlessClone(Mat src, Mat dst, Mat mask, Point p, Mat blend, int flags)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (blend != null)
			{
				blend.ThrowIfDisposed();
			}
			photo_Photo_seamlessClone_10(src.nativeObj, dst.nativeObj, mask.nativeObj, p.x, p.y, blend.nativeObj, flags);
		}

		public static void stylization(Mat src, Mat dst, float sigma_s, float sigma_r)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_stylization_10(src.nativeObj, dst.nativeObj, sigma_s, sigma_r);
		}

		public static void stylization(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_stylization_11(src.nativeObj, dst.nativeObj);
		}

		public static void textureFlattening(Mat src, Mat mask, Mat dst, float low_threshold, float high_threshold, int kernel_size)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_textureFlattening_10(src.nativeObj, mask.nativeObj, dst.nativeObj, low_threshold, high_threshold, kernel_size);
		}

		public static void textureFlattening(Mat src, Mat mask, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Photo_textureFlattening_11(src.nativeObj, mask.nativeObj, dst.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createAlignMTB_10(int max_bits, int exclude_range, bool cut);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createAlignMTB_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createCalibrateDebevec_10(int samples, float lambda, bool random);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createCalibrateDebevec_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createCalibrateRobertson_10(int max_iter, float threshold);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createCalibrateRobertson_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createMergeDebevec_10();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createMergeMertens_10(float contrast_weight, float saturation_weight, float exposure_weight);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createMergeMertens_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createMergeRobertson_10();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemap_10(float gamma);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemap_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapDrago_10(float gamma, float saturation, float bias);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapDrago_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapDurand_10(float gamma, float contrast, float saturation, float sigma_space, float sigma_color);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapDurand_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapMantiuk_10(float gamma, float scale, float saturation);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapMantiuk_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapReinhard_10(float gamma, float intensity, float light_adapt, float color_adapt);

		[DllImport("opencvforunity")]
		private static extern IntPtr photo_Photo_createTonemapReinhard_11();

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_colorChange_10(IntPtr src_nativeObj, IntPtr mask_nativeObj, IntPtr dst_nativeObj, float red_mul, float green_mul, float blue_mul);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_colorChange_11(IntPtr src_nativeObj, IntPtr mask_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_decolor_10(IntPtr src_nativeObj, IntPtr grayscale_nativeObj, IntPtr color_boost_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_denoise_1TVL1_10(IntPtr observations_mat_nativeObj, IntPtr result_nativeObj, double lambda, int niters);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_denoise_1TVL1_11(IntPtr observations_mat_nativeObj, IntPtr result_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_detailEnhance_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, float sigma_s, float sigma_r);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_detailEnhance_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_edgePreservingFilter_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int flags, float sigma_s, float sigma_r);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_edgePreservingFilter_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoising_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, float h, int templateWindowSize, int searchWindowSize);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoising_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoising_12(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr h_mat_nativeObj, int templateWindowSize, int searchWindowSize, int normType);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoising_13(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr h_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingColored_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, float h, float hColor, int templateWindowSize, int searchWindowSize);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingColored_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingColoredMulti_10(IntPtr srcImgs_mat_nativeObj, IntPtr dst_nativeObj, int imgToDenoiseIndex, int temporalWindowSize, float h, float hColor, int templateWindowSize, int searchWindowSize);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingColoredMulti_11(IntPtr srcImgs_mat_nativeObj, IntPtr dst_nativeObj, int imgToDenoiseIndex, int temporalWindowSize);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingMulti_10(IntPtr srcImgs_mat_nativeObj, IntPtr dst_nativeObj, int imgToDenoiseIndex, int temporalWindowSize, float h, int templateWindowSize, int searchWindowSize);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingMulti_11(IntPtr srcImgs_mat_nativeObj, IntPtr dst_nativeObj, int imgToDenoiseIndex, int temporalWindowSize);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingMulti_12(IntPtr srcImgs_mat_nativeObj, IntPtr dst_nativeObj, int imgToDenoiseIndex, int temporalWindowSize, IntPtr h_mat_nativeObj, int templateWindowSize, int searchWindowSize, int normType);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_fastNlMeansDenoisingMulti_13(IntPtr srcImgs_mat_nativeObj, IntPtr dst_nativeObj, int imgToDenoiseIndex, int temporalWindowSize, IntPtr h_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_illuminationChange_10(IntPtr src_nativeObj, IntPtr mask_nativeObj, IntPtr dst_nativeObj, float alpha, float beta);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_illuminationChange_11(IntPtr src_nativeObj, IntPtr mask_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_inpaint_10(IntPtr src_nativeObj, IntPtr inpaintMask_nativeObj, IntPtr dst_nativeObj, double inpaintRadius, int flags);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_pencilSketch_10(IntPtr src_nativeObj, IntPtr dst1_nativeObj, IntPtr dst2_nativeObj, float sigma_s, float sigma_r, float shade_factor);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_pencilSketch_11(IntPtr src_nativeObj, IntPtr dst1_nativeObj, IntPtr dst2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_seamlessClone_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, IntPtr mask_nativeObj, double p_x, double p_y, IntPtr blend_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_stylization_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, float sigma_s, float sigma_r);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_stylization_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_textureFlattening_10(IntPtr src_nativeObj, IntPtr mask_nativeObj, IntPtr dst_nativeObj, float low_threshold, float high_threshold, int kernel_size);

		[DllImport("opencvforunity")]
		private static extern void photo_Photo_textureFlattening_11(IntPtr src_nativeObj, IntPtr mask_nativeObj, IntPtr dst_nativeObj);
	}
}
