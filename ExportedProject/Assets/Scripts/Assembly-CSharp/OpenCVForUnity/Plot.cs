using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Plot
	{
		private const string LIBNAME = "opencvforunity";

		public static Plot2d createPlot2d(Mat data)
		{
			if (data != null)
			{
				data.ThrowIfDisposed();
			}
			return new Plot2d(plot_Plot_createPlot2d_10(data.nativeObj));
		}

		public static Plot2d createPlot2d(Mat dataX, Mat dataY)
		{
			if (dataX != null)
			{
				dataX.ThrowIfDisposed();
			}
			if (dataY != null)
			{
				dataY.ThrowIfDisposed();
			}
			return new Plot2d(plot_Plot_createPlot2d_11(dataX.nativeObj, dataY.nativeObj));
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr plot_Plot_createPlot2d_10(IntPtr data_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr plot_Plot_createPlot2d_11(IntPtr dataX_nativeObj, IntPtr dataY_nativeObj);
	}
}
