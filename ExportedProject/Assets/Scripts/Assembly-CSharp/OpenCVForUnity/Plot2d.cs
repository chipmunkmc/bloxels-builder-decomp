using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Plot2d : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public Plot2d(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						plot_Plot2d_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void render(Mat _plotResult)
		{
			ThrowIfDisposed();
			if (_plotResult != null)
			{
				_plotResult.ThrowIfDisposed();
			}
			plot_Plot2d_render_10(nativeObj, _plotResult.nativeObj);
		}

		public void setMaxX(double _plotMaxX)
		{
			ThrowIfDisposed();
			plot_Plot2d_setMaxX_10(nativeObj, _plotMaxX);
		}

		public void setMaxY(double _plotMaxY)
		{
			ThrowIfDisposed();
			plot_Plot2d_setMaxY_10(nativeObj, _plotMaxY);
		}

		public void setMinX(double _plotMinX)
		{
			ThrowIfDisposed();
			plot_Plot2d_setMinX_10(nativeObj, _plotMinX);
		}

		public void setMinY(double _plotMinY)
		{
			ThrowIfDisposed();
			plot_Plot2d_setMinY_10(nativeObj, _plotMinY);
		}

		public void setPlotAxisColor(Scalar _plotAxisColor)
		{
			ThrowIfDisposed();
			plot_Plot2d_setPlotAxisColor_10(nativeObj, _plotAxisColor.val[0], _plotAxisColor.val[1], _plotAxisColor.val[2], _plotAxisColor.val[3]);
		}

		public void setPlotBackgroundColor(Scalar _plotBackgroundColor)
		{
			ThrowIfDisposed();
			plot_Plot2d_setPlotBackgroundColor_10(nativeObj, _plotBackgroundColor.val[0], _plotBackgroundColor.val[1], _plotBackgroundColor.val[2], _plotBackgroundColor.val[3]);
		}

		public void setPlotGridColor(Scalar _plotGridColor)
		{
			ThrowIfDisposed();
			plot_Plot2d_setPlotGridColor_10(nativeObj, _plotGridColor.val[0], _plotGridColor.val[1], _plotGridColor.val[2], _plotGridColor.val[3]);
		}

		public void setPlotLineColor(Scalar _plotLineColor)
		{
			ThrowIfDisposed();
			plot_Plot2d_setPlotLineColor_10(nativeObj, _plotLineColor.val[0], _plotLineColor.val[1], _plotLineColor.val[2], _plotLineColor.val[3]);
		}

		public void setPlotLineWidth(int _plotLineWidth)
		{
			ThrowIfDisposed();
			plot_Plot2d_setPlotLineWidth_10(nativeObj, _plotLineWidth);
		}

		public void setPlotSize(int _plotSizeWidth, int _plotSizeHeight)
		{
			ThrowIfDisposed();
			plot_Plot2d_setPlotSize_10(nativeObj, _plotSizeWidth, _plotSizeHeight);
		}

		public void setPlotTextColor(Scalar _plotTextColor)
		{
			ThrowIfDisposed();
			plot_Plot2d_setPlotTextColor_10(nativeObj, _plotTextColor.val[0], _plotTextColor.val[1], _plotTextColor.val[2], _plotTextColor.val[3]);
		}

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_render_10(IntPtr nativeObj, IntPtr _plotResult_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setMaxX_10(IntPtr nativeObj, double _plotMaxX);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setMaxY_10(IntPtr nativeObj, double _plotMaxY);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setMinX_10(IntPtr nativeObj, double _plotMinX);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setMinY_10(IntPtr nativeObj, double _plotMinY);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setPlotAxisColor_10(IntPtr nativeObj, double _plotAxisColor_val0, double _plotAxisColor_val1, double _plotAxisColor_val2, double _plotAxisColor_val3);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setPlotBackgroundColor_10(IntPtr nativeObj, double _plotBackgroundColor_val0, double _plotBackgroundColor_val1, double _plotBackgroundColor_val2, double _plotBackgroundColor_val3);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setPlotGridColor_10(IntPtr nativeObj, double _plotGridColor_val0, double _plotGridColor_val1, double _plotGridColor_val2, double _plotGridColor_val3);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setPlotLineColor_10(IntPtr nativeObj, double _plotLineColor_val0, double _plotLineColor_val1, double _plotLineColor_val2, double _plotLineColor_val3);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setPlotLineWidth_10(IntPtr nativeObj, int _plotLineWidth);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setPlotSize_10(IntPtr nativeObj, int _plotSizeWidth, int _plotSizeHeight);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_setPlotTextColor_10(IntPtr nativeObj, double _plotTextColor_val0, double _plotTextColor_val1, double _plotTextColor_val2, double _plotTextColor_val3);

		[DllImport("opencvforunity")]
		private static extern void plot_Plot2d_delete(IntPtr nativeObj);
	}
}
