using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class Point
	{
		public double x;

		public double y;

		public Point(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public Point()
			: this(0.0, 0.0)
		{
		}

		public Point(double[] vals)
			: this()
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				x = ((vals.Length <= 0) ? 0.0 : vals[0]);
				y = ((vals.Length <= 1) ? 0.0 : vals[1]);
			}
			else
			{
				x = 0.0;
				y = 0.0;
			}
		}

		public Point clone()
		{
			return new Point(x, y);
		}

		public double dot(Point p)
		{
			return x * p.x + y * p.y;
		}

		public override int GetHashCode()
		{
			int num = 1;
			long num2 = BitConverter.DoubleToInt64Bits(x);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(y);
			return 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is Point))
			{
				return false;
			}
			Point point = (Point)obj;
			return x == point.x && y == point.y;
		}

		public bool inside(Rect r)
		{
			return r.contains(this);
		}

		public override string ToString()
		{
			return "{" + x + ", " + y + "}";
		}
	}
}
