using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class Point3
	{
		public double x;

		public double y;

		public double z;

		public Point3(double x, double y, double z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public Point3()
			: this(0.0, 0.0, 0.0)
		{
		}

		public Point3(Point p)
		{
			x = p.x;
			y = p.y;
			z = 0.0;
		}

		public Point3(double[] vals)
			: this()
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				x = ((vals.Length <= 0) ? 0.0 : vals[0]);
				y = ((vals.Length <= 1) ? 0.0 : vals[1]);
				z = ((vals.Length <= 2) ? 0.0 : vals[2]);
			}
			else
			{
				x = 0.0;
				y = 0.0;
				z = 0.0;
			}
		}

		public Point3 clone()
		{
			return new Point3(x, y, z);
		}

		public double dot(Point3 p)
		{
			return x * p.x + y * p.y + z * p.z;
		}

		public Point3 cross(Point3 p)
		{
			return new Point3(y * p.z - z * p.y, z * p.x - x * p.z, x * p.y - y * p.x);
		}

		public override int GetHashCode()
		{
			int num = 1;
			long num2 = BitConverter.DoubleToInt64Bits(x);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(y);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(z);
			return 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is Point3))
			{
				return false;
			}
			Point3 point = (Point3)obj;
			return x == point.x && y == point.y && z == point.z;
		}

		public override string ToString()
		{
			return "{" + x + ", " + y + ", " + z + "}";
		}
	}
}
