using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class PredictCollector : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		public PredictCollector(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						face_PredictCollector_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public virtual bool emit(int label, double dist, int state)
		{
			ThrowIfDisposed();
			return face_PredictCollector_emit_10(nativeObj, label, dist, state);
		}

		public virtual bool emit(int label, double dist)
		{
			ThrowIfDisposed();
			return face_PredictCollector_emit_11(nativeObj, label, dist);
		}

		public void init(int size, int state)
		{
			ThrowIfDisposed();
			face_PredictCollector_init_10(nativeObj, size, state);
		}

		public void init(int size)
		{
			ThrowIfDisposed();
			face_PredictCollector_init_11(nativeObj, size);
		}

		[DllImport("opencvforunity")]
		private static extern bool face_PredictCollector_emit_10(IntPtr nativeObj, int label, double dist, int state);

		[DllImport("opencvforunity")]
		private static extern bool face_PredictCollector_emit_11(IntPtr nativeObj, int label, double dist);

		[DllImport("opencvforunity")]
		private static extern void face_PredictCollector_init_10(IntPtr nativeObj, int size, int state);

		[DllImport("opencvforunity")]
		private static extern void face_PredictCollector_init_11(IntPtr nativeObj, int size);

		[DllImport("opencvforunity")]
		private static extern void face_PredictCollector_delete(IntPtr nativeObj);
	}
}
