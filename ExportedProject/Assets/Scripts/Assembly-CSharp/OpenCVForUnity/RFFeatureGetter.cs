using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class RFFeatureGetter : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public RFFeatureGetter(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_RFFeatureGetter_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void getFeatures(Mat src, Mat features, int gnrmRad, int gsmthRad, int shrink, int outNum, int gradNum)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (features != null)
			{
				features.ThrowIfDisposed();
			}
			ximgproc_RFFeatureGetter_getFeatures_10(nativeObj, src.nativeObj, features.nativeObj, gnrmRad, gsmthRad, shrink, outNum, gradNum);
		}

		[DllImport("opencvforunity")]
		private static extern void ximgproc_RFFeatureGetter_getFeatures_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr features_nativeObj, int gnrmRad, int gsmthRad, int shrink, int outNum, int gradNum);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_RFFeatureGetter_delete(IntPtr nativeObj);
	}
}
