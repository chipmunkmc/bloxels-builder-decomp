using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class RTrees : DTrees
	{
		private const string LIBNAME = "opencvforunity";

		protected RTrees(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_RTrees_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getVarImportance()
		{
			ThrowIfDisposed();
			return new Mat(ml_RTrees_getVarImportance_10(nativeObj));
		}

		public new static RTrees create()
		{
			return new RTrees(ml_RTrees_create_10());
		}

		public TermCriteria getTermCriteria()
		{
			ThrowIfDisposed();
			double[] vals = new double[3];
			ml_RTrees_getTermCriteria_10(nativeObj, vals);
			return new TermCriteria(vals);
		}

		public bool getCalculateVarImportance()
		{
			ThrowIfDisposed();
			return ml_RTrees_getCalculateVarImportance_10(nativeObj);
		}

		public int getActiveVarCount()
		{
			ThrowIfDisposed();
			return ml_RTrees_getActiveVarCount_10(nativeObj);
		}

		public void setActiveVarCount(int val)
		{
			ThrowIfDisposed();
			ml_RTrees_setActiveVarCount_10(nativeObj, val);
		}

		public void setCalculateVarImportance(bool val)
		{
			ThrowIfDisposed();
			ml_RTrees_setCalculateVarImportance_10(nativeObj, val);
		}

		public void setTermCriteria(TermCriteria val)
		{
			ThrowIfDisposed();
			ml_RTrees_setTermCriteria_10(nativeObj, val.type, val.maxCount, val.epsilon);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_RTrees_getVarImportance_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_RTrees_create_10();

		[DllImport("opencvforunity")]
		private static extern void ml_RTrees_getTermCriteria_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern bool ml_RTrees_getCalculateVarImportance_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_RTrees_getActiveVarCount_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_RTrees_setActiveVarCount_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_RTrees_setCalculateVarImportance_10(IntPtr nativeObj, bool val);

		[DllImport("opencvforunity")]
		private static extern void ml_RTrees_setTermCriteria_10(IntPtr nativeObj, int val_type, int val_maxCount, double val_epsilon);

		[DllImport("opencvforunity")]
		private static extern void ml_RTrees_delete(IntPtr nativeObj);
	}
}
