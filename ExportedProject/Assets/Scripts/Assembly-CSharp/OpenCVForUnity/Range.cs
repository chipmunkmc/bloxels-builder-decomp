using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class Range
	{
		public int start;

		public int end;

		public Range(int s, int e)
		{
			start = s;
			end = e;
		}

		public Range()
			: this(0, 0)
		{
		}

		public Range(double[] vals)
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				start = ((vals.Length > 0) ? ((int)vals[0]) : 0);
				end = ((vals.Length > 1) ? ((int)vals[1]) : 0);
			}
			else
			{
				start = 0;
				end = 0;
			}
		}

		public int size()
		{
			return (!empty()) ? (end - start) : 0;
		}

		public bool empty()
		{
			return end <= start;
		}

		public static Range all()
		{
			return new Range(int.MinValue, int.MaxValue);
		}

		public Range intersection(Range r1)
		{
			Range range = new Range(Math.Max(r1.start, start), Math.Min(r1.end, end));
			range.end = Math.Max(range.end, range.start);
			return range;
		}

		public Range shift(int delta)
		{
			return new Range(start + delta, end + delta);
		}

		public Range clone()
		{
			return new Range(start, end);
		}

		public override int GetHashCode()
		{
			int num = 1;
			long num2 = BitConverter.DoubleToInt64Bits(start);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(end);
			return 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is Range))
			{
				return false;
			}
			Range range = (Range)obj;
			return start == range.start && end == range.end;
		}

		public override string ToString()
		{
			return "[" + start + ", " + end + ")";
		}
	}
}
