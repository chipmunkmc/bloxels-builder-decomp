using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class Rect
	{
		public int x;

		public int y;

		public int width;

		public int height;

		public Rect(int x, int y, int width, int height)
		{
			this.x = x;
			this.y = y;
			this.width = width;
			this.height = height;
		}

		public Rect()
			: this(0, 0, 0, 0)
		{
		}

		public Rect(Point p1, Point p2)
		{
			x = (int)((!(p1.x < p2.x)) ? p2.x : p1.x);
			y = (int)((!(p1.y < p2.y)) ? p2.y : p1.y);
			width = (int)((!(p1.x > p2.x)) ? p2.x : p1.x) - x;
			height = (int)((!(p1.y > p2.y)) ? p2.y : p1.y) - y;
		}

		public Rect(Point p, Size s)
			: this((int)p.x, (int)p.y, (int)s.width, (int)s.height)
		{
		}

		public Rect(double[] vals)
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				x = ((vals.Length > 0) ? ((int)vals[0]) : 0);
				y = ((vals.Length > 1) ? ((int)vals[1]) : 0);
				width = ((vals.Length > 2) ? ((int)vals[2]) : 0);
				height = ((vals.Length > 3) ? ((int)vals[3]) : 0);
			}
			else
			{
				x = 0;
				y = 0;
				width = 0;
				height = 0;
			}
		}

		public Rect clone()
		{
			return new Rect(x, y, width, height);
		}

		public Point tl()
		{
			return new Point(x, y);
		}

		public Point br()
		{
			return new Point(x + width, y + height);
		}

		public Size size()
		{
			return new Size(width, height);
		}

		public double area()
		{
			return width * height;
		}

		public bool contains(Point p)
		{
			return (double)x <= p.x && p.x < (double)(x + width) && (double)y <= p.y && p.y < (double)(y + height);
		}

		public override int GetHashCode()
		{
			int num = 1;
			long num2 = BitConverter.DoubleToInt64Bits(height);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(width);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(x);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(y);
			return 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is Rect))
			{
				return false;
			}
			Rect rect = (Rect)obj;
			return x == rect.x && y == rect.y && width == rect.width && height == rect.height;
		}

		public override string ToString()
		{
			return "{" + x + ", " + y + ", " + width + "x" + height + "}";
		}
	}
}
