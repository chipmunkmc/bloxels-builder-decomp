using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Retina : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public Retina(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						bioinspired_Retina_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getMagnoRAW()
		{
			ThrowIfDisposed();
			return new Mat(bioinspired_Retina_getMagnoRAW_10(nativeObj));
		}

		public Mat getParvoRAW()
		{
			ThrowIfDisposed();
			return new Mat(bioinspired_Retina_getParvoRAW_10(nativeObj));
		}

		public Size getInputSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			bioinspired_Retina_getInputSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public Size getOutputSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			bioinspired_Retina_getOutputSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public string printSetup()
		{
			ThrowIfDisposed();
			return Marshal.PtrToStringAnsi(bioinspired_Retina_printSetup_10(nativeObj));
		}

		public void activateContoursProcessing(bool activate)
		{
			ThrowIfDisposed();
			bioinspired_Retina_activateContoursProcessing_10(nativeObj, activate);
		}

		public void activateMovingContoursProcessing(bool activate)
		{
			ThrowIfDisposed();
			bioinspired_Retina_activateMovingContoursProcessing_10(nativeObj, activate);
		}

		public void applyFastToneMapping(Mat inputImage, Mat outputToneMappedImage)
		{
			ThrowIfDisposed();
			if (inputImage != null)
			{
				inputImage.ThrowIfDisposed();
			}
			if (outputToneMappedImage != null)
			{
				outputToneMappedImage.ThrowIfDisposed();
			}
			bioinspired_Retina_applyFastToneMapping_10(nativeObj, inputImage.nativeObj, outputToneMappedImage.nativeObj);
		}

		public void clearBuffers()
		{
			ThrowIfDisposed();
			bioinspired_Retina_clearBuffers_10(nativeObj);
		}

		public void getMagno(Mat retinaOutput_magno)
		{
			ThrowIfDisposed();
			if (retinaOutput_magno != null)
			{
				retinaOutput_magno.ThrowIfDisposed();
			}
			bioinspired_Retina_getMagno_10(nativeObj, retinaOutput_magno.nativeObj);
		}

		public void getMagnoRAW(Mat retinaOutput_magno)
		{
			ThrowIfDisposed();
			if (retinaOutput_magno != null)
			{
				retinaOutput_magno.ThrowIfDisposed();
			}
			bioinspired_Retina_getMagnoRAW_11(nativeObj, retinaOutput_magno.nativeObj);
		}

		public void getParvo(Mat retinaOutput_parvo)
		{
			ThrowIfDisposed();
			if (retinaOutput_parvo != null)
			{
				retinaOutput_parvo.ThrowIfDisposed();
			}
			bioinspired_Retina_getParvo_10(nativeObj, retinaOutput_parvo.nativeObj);
		}

		public void getParvoRAW(Mat retinaOutput_parvo)
		{
			ThrowIfDisposed();
			if (retinaOutput_parvo != null)
			{
				retinaOutput_parvo.ThrowIfDisposed();
			}
			bioinspired_Retina_getParvoRAW_11(nativeObj, retinaOutput_parvo.nativeObj);
		}

		public void run(Mat inputImage)
		{
			ThrowIfDisposed();
			if (inputImage != null)
			{
				inputImage.ThrowIfDisposed();
			}
			bioinspired_Retina_run_10(nativeObj, inputImage.nativeObj);
		}

		public void setColorSaturation(bool saturateColors, float colorSaturationValue)
		{
			ThrowIfDisposed();
			bioinspired_Retina_setColorSaturation_10(nativeObj, saturateColors, colorSaturationValue);
		}

		public void setColorSaturation()
		{
			ThrowIfDisposed();
			bioinspired_Retina_setColorSaturation_11(nativeObj);
		}

		public void setup(string retinaParameterFile, bool applyDefaultSetupOnFailure)
		{
			ThrowIfDisposed();
			bioinspired_Retina_setup_10(nativeObj, retinaParameterFile, applyDefaultSetupOnFailure);
		}

		public void setup()
		{
			ThrowIfDisposed();
			bioinspired_Retina_setup_11(nativeObj);
		}

		public void setupIPLMagnoChannel(bool normaliseOutput, float parasolCells_beta, float parasolCells_tau, float parasolCells_k, float amacrinCellsTemporalCutFrequency, float V0CompressionParameter, float localAdaptintegration_tau, float localAdaptintegration_k)
		{
			ThrowIfDisposed();
			bioinspired_Retina_setupIPLMagnoChannel_10(nativeObj, normaliseOutput, parasolCells_beta, parasolCells_tau, parasolCells_k, amacrinCellsTemporalCutFrequency, V0CompressionParameter, localAdaptintegration_tau, localAdaptintegration_k);
		}

		public void setupIPLMagnoChannel()
		{
			ThrowIfDisposed();
			bioinspired_Retina_setupIPLMagnoChannel_11(nativeObj);
		}

		public void setupOPLandIPLParvoChannel(bool colorMode, bool normaliseOutput, float photoreceptorsLocalAdaptationSensitivity, float photoreceptorsTemporalConstant, float photoreceptorsSpatialConstant, float horizontalCellsGain, float HcellsTemporalConstant, float HcellsSpatialConstant, float ganglionCellsSensitivity)
		{
			ThrowIfDisposed();
			bioinspired_Retina_setupOPLandIPLParvoChannel_10(nativeObj, colorMode, normaliseOutput, photoreceptorsLocalAdaptationSensitivity, photoreceptorsTemporalConstant, photoreceptorsSpatialConstant, horizontalCellsGain, HcellsTemporalConstant, HcellsSpatialConstant, ganglionCellsSensitivity);
		}

		public void setupOPLandIPLParvoChannel()
		{
			ThrowIfDisposed();
			bioinspired_Retina_setupOPLandIPLParvoChannel_11(nativeObj);
		}

		public void write(string fs)
		{
			ThrowIfDisposed();
			bioinspired_Retina_write_10(nativeObj, fs);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Retina_getMagnoRAW_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Retina_getParvoRAW_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_getInputSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_getOutputSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_Retina_printSetup_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_activateContoursProcessing_10(IntPtr nativeObj, bool activate);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_activateMovingContoursProcessing_10(IntPtr nativeObj, bool activate);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_applyFastToneMapping_10(IntPtr nativeObj, IntPtr inputImage_nativeObj, IntPtr outputToneMappedImage_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_clearBuffers_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_getMagno_10(IntPtr nativeObj, IntPtr retinaOutput_magno_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_getMagnoRAW_11(IntPtr nativeObj, IntPtr retinaOutput_magno_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_getParvo_10(IntPtr nativeObj, IntPtr retinaOutput_parvo_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_getParvoRAW_11(IntPtr nativeObj, IntPtr retinaOutput_parvo_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_run_10(IntPtr nativeObj, IntPtr inputImage_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setColorSaturation_10(IntPtr nativeObj, bool saturateColors, float colorSaturationValue);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setColorSaturation_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setup_10(IntPtr nativeObj, string retinaParameterFile, bool applyDefaultSetupOnFailure);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setup_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setupIPLMagnoChannel_10(IntPtr nativeObj, bool normaliseOutput, float parasolCells_beta, float parasolCells_tau, float parasolCells_k, float amacrinCellsTemporalCutFrequency, float V0CompressionParameter, float localAdaptintegration_tau, float localAdaptintegration_k);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setupIPLMagnoChannel_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setupOPLandIPLParvoChannel_10(IntPtr nativeObj, bool colorMode, bool normaliseOutput, float photoreceptorsLocalAdaptationSensitivity, float photoreceptorsTemporalConstant, float photoreceptorsSpatialConstant, float horizontalCellsGain, float HcellsTemporalConstant, float HcellsSpatialConstant, float ganglionCellsSensitivity);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_setupOPLandIPLParvoChannel_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_write_10(IntPtr nativeObj, string fs);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_Retina_delete(IntPtr nativeObj);
	}
}
