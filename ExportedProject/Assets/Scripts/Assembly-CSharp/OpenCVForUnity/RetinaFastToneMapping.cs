using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class RetinaFastToneMapping : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public RetinaFastToneMapping(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						bioinspired_RetinaFastToneMapping_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void applyFastToneMapping(Mat inputImage, Mat outputToneMappedImage)
		{
			ThrowIfDisposed();
			if (inputImage != null)
			{
				inputImage.ThrowIfDisposed();
			}
			if (outputToneMappedImage != null)
			{
				outputToneMappedImage.ThrowIfDisposed();
			}
			bioinspired_RetinaFastToneMapping_applyFastToneMapping_10(nativeObj, inputImage.nativeObj, outputToneMappedImage.nativeObj);
		}

		public void setup(float photoreceptorsNeighborhoodRadius, float ganglioncellsNeighborhoodRadius, float meanLuminanceModulatorK)
		{
			ThrowIfDisposed();
			bioinspired_RetinaFastToneMapping_setup_10(nativeObj, photoreceptorsNeighborhoodRadius, ganglioncellsNeighborhoodRadius, meanLuminanceModulatorK);
		}

		public void setup()
		{
			ThrowIfDisposed();
			bioinspired_RetinaFastToneMapping_setup_11(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void bioinspired_RetinaFastToneMapping_applyFastToneMapping_10(IntPtr nativeObj, IntPtr inputImage_nativeObj, IntPtr outputToneMappedImage_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_RetinaFastToneMapping_setup_10(IntPtr nativeObj, float photoreceptorsNeighborhoodRadius, float ganglioncellsNeighborhoodRadius, float meanLuminanceModulatorK);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_RetinaFastToneMapping_setup_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_RetinaFastToneMapping_delete(IntPtr nativeObj);
	}
}
