using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class RotatedRect
	{
		public Point center;

		public Size size;

		public double angle;

		public RotatedRect()
		{
			center = new Point();
			size = new Size();
			angle = 0.0;
		}

		public RotatedRect(Point c, Size s, double a)
		{
			center = c.clone();
			size = s.clone();
			angle = a;
		}

		public RotatedRect(double[] vals)
			: this()
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				center.x = ((vals.Length <= 0) ? 0.0 : vals[0]);
				center.y = ((vals.Length <= 1) ? 0.0 : vals[1]);
				size.width = ((vals.Length <= 2) ? 0.0 : vals[2]);
				size.height = ((vals.Length <= 3) ? 0.0 : vals[3]);
				angle = ((vals.Length <= 4) ? 0.0 : vals[4]);
			}
			else
			{
				center.x = 0.0;
				center.x = 0.0;
				size.width = 0.0;
				size.height = 0.0;
				angle = 0.0;
			}
		}

		public void points(Point[] pt)
		{
			double num = angle * Math.PI / 180.0;
			double num2 = Math.Cos(num) * 0.5;
			double num3 = Math.Sin(num) * 0.5;
			pt[0] = new Point(center.x - num3 * size.height - num2 * size.width, center.y + num2 * size.height - num3 * size.width);
			pt[1] = new Point(center.x + num3 * size.height - num2 * size.width, center.y - num2 * size.height - num3 * size.width);
			pt[2] = new Point(2.0 * center.x - pt[0].x, 2.0 * center.y - pt[0].y);
			pt[3] = new Point(2.0 * center.x - pt[1].x, 2.0 * center.y - pt[1].y);
		}

		public Rect boundingRect()
		{
			Point[] array = new Point[4];
			points(array);
			Rect rect = new Rect((int)Math.Floor(Math.Min(Math.Min(Math.Min(array[0].x, array[1].x), array[2].x), array[3].x)), (int)Math.Floor(Math.Min(Math.Min(Math.Min(array[0].y, array[1].y), array[2].y), array[3].y)), (int)Math.Ceiling(Math.Max(Math.Max(Math.Max(array[0].x, array[1].x), array[2].x), array[3].x)), (int)Math.Ceiling(Math.Max(Math.Max(Math.Max(array[0].y, array[1].y), array[2].y), array[3].y)));
			rect.width -= rect.x - 1;
			rect.height -= rect.y - 1;
			return rect;
		}

		public RotatedRect clone()
		{
			return new RotatedRect(center, size, angle);
		}

		public override int GetHashCode()
		{
			int num = 1;
			long num2 = BitConverter.DoubleToInt64Bits(center.x);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(center.y);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(size.width);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(size.height);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(angle);
			return 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is RotatedRect))
			{
				return false;
			}
			RotatedRect rotatedRect = (RotatedRect)obj;
			return center.Equals(rotatedRect.center) && size.Equals(rotatedRect.size) && angle == rotatedRect.angle;
		}

		public override string ToString()
		{
			return string.Concat("{ ", center, " ", size, " * ", angle, " }");
		}
	}
}
