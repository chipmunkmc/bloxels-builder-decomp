using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class SVM : StatModel
	{
		public const int C_SVC = 100;

		public const int NU_SVC = 101;

		public const int ONE_CLASS = 102;

		public const int EPS_SVR = 103;

		public const int NU_SVR = 104;

		public const int CUSTOM = -1;

		public const int LINEAR = 0;

		public const int POLY = 1;

		public const int RBF = 2;

		public const int SIGMOID = 3;

		public const int CHI2 = 4;

		public const int INTER = 5;

		public const int C = 0;

		public const int GAMMA = 1;

		public const int P = 2;

		public const int NU = 3;

		public const int COEF = 4;

		public const int DEGREE = 5;

		private const string LIBNAME = "opencvforunity";

		protected SVM(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_SVM_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getClassWeights()
		{
			ThrowIfDisposed();
			return new Mat(ml_SVM_getClassWeights_10(nativeObj));
		}

		public Mat getSupportVectors()
		{
			ThrowIfDisposed();
			return new Mat(ml_SVM_getSupportVectors_10(nativeObj));
		}

		public Mat getUncompressedSupportVectors()
		{
			ThrowIfDisposed();
			return new Mat(ml_SVM_getUncompressedSupportVectors_10(nativeObj));
		}

		public static SVM create()
		{
			return new SVM(ml_SVM_create_10());
		}

		public TermCriteria getTermCriteria()
		{
			ThrowIfDisposed();
			double[] vals = new double[3];
			ml_SVM_getTermCriteria_10(nativeObj, vals);
			return new TermCriteria(vals);
		}

		public double getC()
		{
			ThrowIfDisposed();
			return ml_SVM_getC_10(nativeObj);
		}

		public double getCoef0()
		{
			ThrowIfDisposed();
			return ml_SVM_getCoef0_10(nativeObj);
		}

		public double getDecisionFunction(int i, Mat alpha, Mat svidx)
		{
			ThrowIfDisposed();
			if (alpha != null)
			{
				alpha.ThrowIfDisposed();
			}
			if (svidx != null)
			{
				svidx.ThrowIfDisposed();
			}
			return ml_SVM_getDecisionFunction_10(nativeObj, i, alpha.nativeObj, svidx.nativeObj);
		}

		public double getDegree()
		{
			ThrowIfDisposed();
			return ml_SVM_getDegree_10(nativeObj);
		}

		public double getGamma()
		{
			ThrowIfDisposed();
			return ml_SVM_getGamma_10(nativeObj);
		}

		public double getNu()
		{
			ThrowIfDisposed();
			return ml_SVM_getNu_10(nativeObj);
		}

		public double getP()
		{
			ThrowIfDisposed();
			return ml_SVM_getP_10(nativeObj);
		}

		public int getKernelType()
		{
			ThrowIfDisposed();
			return ml_SVM_getKernelType_10(nativeObj);
		}

		public int getType()
		{
			ThrowIfDisposed();
			return ml_SVM_getType_10(nativeObj);
		}

		public void setC(double val)
		{
			ThrowIfDisposed();
			ml_SVM_setC_10(nativeObj, val);
		}

		public void setClassWeights(Mat val)
		{
			ThrowIfDisposed();
			if (val != null)
			{
				val.ThrowIfDisposed();
			}
			ml_SVM_setClassWeights_10(nativeObj, val.nativeObj);
		}

		public void setCoef0(double val)
		{
			ThrowIfDisposed();
			ml_SVM_setCoef0_10(nativeObj, val);
		}

		public void setDegree(double val)
		{
			ThrowIfDisposed();
			ml_SVM_setDegree_10(nativeObj, val);
		}

		public void setGamma(double val)
		{
			ThrowIfDisposed();
			ml_SVM_setGamma_10(nativeObj, val);
		}

		public void setKernel(int kernelType)
		{
			ThrowIfDisposed();
			ml_SVM_setKernel_10(nativeObj, kernelType);
		}

		public void setNu(double val)
		{
			ThrowIfDisposed();
			ml_SVM_setNu_10(nativeObj, val);
		}

		public void setP(double val)
		{
			ThrowIfDisposed();
			ml_SVM_setP_10(nativeObj, val);
		}

		public void setTermCriteria(TermCriteria val)
		{
			ThrowIfDisposed();
			ml_SVM_setTermCriteria_10(nativeObj, val.type, val.maxCount, val.epsilon);
		}

		public void setType(int val)
		{
			ThrowIfDisposed();
			ml_SVM_setType_10(nativeObj, val);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_SVM_getClassWeights_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_SVM_getSupportVectors_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_SVM_getUncompressedSupportVectors_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_SVM_create_10();

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_getTermCriteria_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern double ml_SVM_getC_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_SVM_getCoef0_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_SVM_getDecisionFunction_10(IntPtr nativeObj, int i, IntPtr alpha_nativeObj, IntPtr svidx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_SVM_getDegree_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_SVM_getGamma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_SVM_getNu_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern double ml_SVM_getP_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_SVM_getKernelType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_SVM_getType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setC_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setClassWeights_10(IntPtr nativeObj, IntPtr val_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setCoef0_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setDegree_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setGamma_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setKernel_10(IntPtr nativeObj, int kernelType);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setNu_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setP_10(IntPtr nativeObj, double val);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setTermCriteria_10(IntPtr nativeObj, int val_type, int val_maxCount, double val_epsilon);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_setType_10(IntPtr nativeObj, int val);

		[DllImport("opencvforunity")]
		private static extern void ml_SVM_delete(IntPtr nativeObj);
	}
}
