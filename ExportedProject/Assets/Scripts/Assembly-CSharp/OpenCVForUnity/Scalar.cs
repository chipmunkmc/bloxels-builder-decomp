using System;
using System.Linq;

namespace OpenCVForUnity
{
	[Serializable]
	public class Scalar
	{
		public double[] val;

		public Scalar(double v0, double v1, double v2, double v3)
		{
			val = new double[4] { v0, v1, v2, v3 };
		}

		public Scalar(double v0, double v1, double v2)
		{
			val = new double[4] { v0, v1, v2, 0.0 };
		}

		public Scalar(double v0, double v1)
		{
			val = new double[4] { v0, v1, 0.0, 0.0 };
		}

		public Scalar(double v0)
		{
			val = new double[4] { v0, 0.0, 0.0, 0.0 };
		}

		public Scalar(double[] vals)
		{
			if (vals != null && vals.Length == 4)
			{
				val = (double[])vals.Clone();
				return;
			}
			val = new double[4];
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				val[0] = ((vals.Length <= 0) ? 0.0 : vals[0]);
				val[1] = ((vals.Length <= 1) ? 0.0 : vals[1]);
				val[2] = ((vals.Length <= 2) ? 0.0 : vals[2]);
				val[3] = ((vals.Length <= 3) ? 0.0 : vals[3]);
			}
			else
			{
				val[0] = (val[1] = (val[2] = (val[3] = 0.0)));
			}
		}

		public static Scalar all(double v)
		{
			return new Scalar(v, v, v, v);
		}

		public Scalar clone()
		{
			return new Scalar(val);
		}

		public Scalar mul(Scalar it, double scale)
		{
			return new Scalar(val[0] * it.val[0] * scale, val[1] * it.val[1] * scale, val[2] * it.val[2] * scale, val[3] * it.val[3] * scale);
		}

		public Scalar mul(Scalar it)
		{
			return mul(it, 1.0);
		}

		public Scalar conj()
		{
			return new Scalar(val[0], 0.0 - val[1], 0.0 - val[2], 0.0 - val[3]);
		}

		public bool isReal()
		{
			return val[1] == 0.0 && val[2] == 0.0 && val[3] == 0.0;
		}

		public override int GetHashCode()
		{
			int num = 1;
			return 31 * num + val.HashContents();
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is Scalar))
			{
				return false;
			}
			Scalar scalar = (Scalar)obj;
			if (!val.SequenceEqual(scalar.val))
			{
				return false;
			}
			return true;
		}

		public override string ToString()
		{
			return "[" + val[0] + ", " + val[1] + ", " + val[2] + ", " + val[3] + "]";
		}
	}
}
