using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Shape
	{
		private const string LIBNAME = "opencvforunity";

		public static AffineTransformer createAffineTransformer(bool fullAffine)
		{
			return new AffineTransformer(shape_Shape_createAffineTransformer_10(fullAffine));
		}

		public static HausdorffDistanceExtractor createHausdorffDistanceExtractor(int distanceFlag, float rankProp)
		{
			return new HausdorffDistanceExtractor(shape_Shape_createHausdorffDistanceExtractor_10(distanceFlag, rankProp));
		}

		public static HausdorffDistanceExtractor createHausdorffDistanceExtractor()
		{
			return new HausdorffDistanceExtractor(shape_Shape_createHausdorffDistanceExtractor_11());
		}

		public static HistogramCostExtractor createChiHistogramCostExtractor(int nDummies, float defaultCost)
		{
			return new HistogramCostExtractor(shape_Shape_createChiHistogramCostExtractor_10(nDummies, defaultCost));
		}

		public static HistogramCostExtractor createChiHistogramCostExtractor()
		{
			return new HistogramCostExtractor(shape_Shape_createChiHistogramCostExtractor_11());
		}

		public static HistogramCostExtractor createEMDHistogramCostExtractor(int flag, int nDummies, float defaultCost)
		{
			return new HistogramCostExtractor(shape_Shape_createEMDHistogramCostExtractor_10(flag, nDummies, defaultCost));
		}

		public static HistogramCostExtractor createEMDHistogramCostExtractor()
		{
			return new HistogramCostExtractor(shape_Shape_createEMDHistogramCostExtractor_11());
		}

		public static HistogramCostExtractor createEMDL1HistogramCostExtractor(int nDummies, float defaultCost)
		{
			return new HistogramCostExtractor(shape_Shape_createEMDL1HistogramCostExtractor_10(nDummies, defaultCost));
		}

		public static HistogramCostExtractor createEMDL1HistogramCostExtractor()
		{
			return new HistogramCostExtractor(shape_Shape_createEMDL1HistogramCostExtractor_11());
		}

		public static HistogramCostExtractor createNormHistogramCostExtractor(int flag, int nDummies, float defaultCost)
		{
			return new HistogramCostExtractor(shape_Shape_createNormHistogramCostExtractor_10(flag, nDummies, defaultCost));
		}

		public static HistogramCostExtractor createNormHistogramCostExtractor()
		{
			return new HistogramCostExtractor(shape_Shape_createNormHistogramCostExtractor_11());
		}

		public static ShapeContextDistanceExtractor createShapeContextDistanceExtractor(int nAngularBins, int nRadialBins, float innerRadius, float outerRadius, int iterations, HistogramCostExtractor comparer, ShapeTransformer transformer)
		{
			if (comparer != null)
			{
				comparer.ThrowIfDisposed();
			}
			if (transformer != null)
			{
				transformer.ThrowIfDisposed();
			}
			return new ShapeContextDistanceExtractor(shape_Shape_createShapeContextDistanceExtractor_10(nAngularBins, nRadialBins, innerRadius, outerRadius, iterations, comparer.nativeObj, transformer.nativeObj));
		}

		public static ShapeContextDistanceExtractor createShapeContextDistanceExtractor()
		{
			return new ShapeContextDistanceExtractor(shape_Shape_createShapeContextDistanceExtractor_11());
		}

		public static ThinPlateSplineShapeTransformer createThinPlateSplineShapeTransformer(double regularizationParameter)
		{
			return new ThinPlateSplineShapeTransformer(shape_Shape_createThinPlateSplineShapeTransformer_10(regularizationParameter));
		}

		public static ThinPlateSplineShapeTransformer createThinPlateSplineShapeTransformer()
		{
			return new ThinPlateSplineShapeTransformer(shape_Shape_createThinPlateSplineShapeTransformer_11());
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createAffineTransformer_10(bool fullAffine);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createHausdorffDistanceExtractor_10(int distanceFlag, float rankProp);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createHausdorffDistanceExtractor_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createChiHistogramCostExtractor_10(int nDummies, float defaultCost);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createChiHistogramCostExtractor_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createEMDHistogramCostExtractor_10(int flag, int nDummies, float defaultCost);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createEMDHistogramCostExtractor_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createEMDL1HistogramCostExtractor_10(int nDummies, float defaultCost);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createEMDL1HistogramCostExtractor_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createNormHistogramCostExtractor_10(int flag, int nDummies, float defaultCost);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createNormHistogramCostExtractor_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createShapeContextDistanceExtractor_10(int nAngularBins, int nRadialBins, float innerRadius, float outerRadius, int iterations, IntPtr comparer_nativeObj, IntPtr transformer_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createShapeContextDistanceExtractor_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createThinPlateSplineShapeTransformer_10(double regularizationParameter);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_Shape_createThinPlateSplineShapeTransformer_11();
	}
}
