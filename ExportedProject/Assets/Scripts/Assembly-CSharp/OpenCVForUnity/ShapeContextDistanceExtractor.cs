using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class ShapeContextDistanceExtractor : ShapeDistanceExtractor
	{
		private const string LIBNAME = "opencvforunity";

		public ShapeContextDistanceExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_ShapeContextDistanceExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public HistogramCostExtractor getCostExtractor()
		{
			ThrowIfDisposed();
			return new HistogramCostExtractor(shape_ShapeContextDistanceExtractor_getCostExtractor_10(nativeObj));
		}

		public ShapeTransformer getTransformAlgorithm()
		{
			ThrowIfDisposed();
			return new ShapeTransformer(shape_ShapeContextDistanceExtractor_getTransformAlgorithm_10(nativeObj));
		}

		public bool getRotationInvariant()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getRotationInvariant_10(nativeObj);
		}

		public float getBendingEnergyWeight()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getBendingEnergyWeight_10(nativeObj);
		}

		public float getImageAppearanceWeight()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getImageAppearanceWeight_10(nativeObj);
		}

		public float getInnerRadius()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getInnerRadius_10(nativeObj);
		}

		public float getOuterRadius()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getOuterRadius_10(nativeObj);
		}

		public float getShapeContextWeight()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getShapeContextWeight_10(nativeObj);
		}

		public float getStdDev()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getStdDev_10(nativeObj);
		}

		public int getAngularBins()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getAngularBins_10(nativeObj);
		}

		public int getIterations()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getIterations_10(nativeObj);
		}

		public int getRadialBins()
		{
			ThrowIfDisposed();
			return shape_ShapeContextDistanceExtractor_getRadialBins_10(nativeObj);
		}

		public void getImages(Mat image1, Mat image2)
		{
			ThrowIfDisposed();
			if (image1 != null)
			{
				image1.ThrowIfDisposed();
			}
			if (image2 != null)
			{
				image2.ThrowIfDisposed();
			}
			shape_ShapeContextDistanceExtractor_getImages_10(nativeObj, image1.nativeObj, image2.nativeObj);
		}

		public void setAngularBins(int nAngularBins)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setAngularBins_10(nativeObj, nAngularBins);
		}

		public void setBendingEnergyWeight(float bendingEnergyWeight)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setBendingEnergyWeight_10(nativeObj, bendingEnergyWeight);
		}

		public void setCostExtractor(HistogramCostExtractor comparer)
		{
			ThrowIfDisposed();
			if (comparer != null)
			{
				comparer.ThrowIfDisposed();
			}
			shape_ShapeContextDistanceExtractor_setCostExtractor_10(nativeObj, comparer.nativeObj);
		}

		public void setImageAppearanceWeight(float imageAppearanceWeight)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setImageAppearanceWeight_10(nativeObj, imageAppearanceWeight);
		}

		public void setImages(Mat image1, Mat image2)
		{
			ThrowIfDisposed();
			if (image1 != null)
			{
				image1.ThrowIfDisposed();
			}
			if (image2 != null)
			{
				image2.ThrowIfDisposed();
			}
			shape_ShapeContextDistanceExtractor_setImages_10(nativeObj, image1.nativeObj, image2.nativeObj);
		}

		public void setInnerRadius(float innerRadius)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setInnerRadius_10(nativeObj, innerRadius);
		}

		public void setIterations(int iterations)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setIterations_10(nativeObj, iterations);
		}

		public void setOuterRadius(float outerRadius)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setOuterRadius_10(nativeObj, outerRadius);
		}

		public void setRadialBins(int nRadialBins)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setRadialBins_10(nativeObj, nRadialBins);
		}

		public void setRotationInvariant(bool rotationInvariant)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setRotationInvariant_10(nativeObj, rotationInvariant);
		}

		public void setShapeContextWeight(float shapeContextWeight)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setShapeContextWeight_10(nativeObj, shapeContextWeight);
		}

		public void setStdDev(float sigma)
		{
			ThrowIfDisposed();
			shape_ShapeContextDistanceExtractor_setStdDev_10(nativeObj, sigma);
		}

		public void setTransformAlgorithm(ShapeTransformer transformer)
		{
			ThrowIfDisposed();
			if (transformer != null)
			{
				transformer.ThrowIfDisposed();
			}
			shape_ShapeContextDistanceExtractor_setTransformAlgorithm_10(nativeObj, transformer.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_ShapeContextDistanceExtractor_getCostExtractor_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr shape_ShapeContextDistanceExtractor_getTransformAlgorithm_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool shape_ShapeContextDistanceExtractor_getRotationInvariant_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeContextDistanceExtractor_getBendingEnergyWeight_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeContextDistanceExtractor_getImageAppearanceWeight_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeContextDistanceExtractor_getInnerRadius_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeContextDistanceExtractor_getOuterRadius_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeContextDistanceExtractor_getShapeContextWeight_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeContextDistanceExtractor_getStdDev_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int shape_ShapeContextDistanceExtractor_getAngularBins_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int shape_ShapeContextDistanceExtractor_getIterations_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int shape_ShapeContextDistanceExtractor_getRadialBins_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_getImages_10(IntPtr nativeObj, IntPtr image1_nativeObj, IntPtr image2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setAngularBins_10(IntPtr nativeObj, int nAngularBins);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setBendingEnergyWeight_10(IntPtr nativeObj, float bendingEnergyWeight);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setCostExtractor_10(IntPtr nativeObj, IntPtr comparer_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setImageAppearanceWeight_10(IntPtr nativeObj, float imageAppearanceWeight);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setImages_10(IntPtr nativeObj, IntPtr image1_nativeObj, IntPtr image2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setInnerRadius_10(IntPtr nativeObj, float innerRadius);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setIterations_10(IntPtr nativeObj, int iterations);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setOuterRadius_10(IntPtr nativeObj, float outerRadius);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setRadialBins_10(IntPtr nativeObj, int nRadialBins);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setRotationInvariant_10(IntPtr nativeObj, bool rotationInvariant);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setShapeContextWeight_10(IntPtr nativeObj, float shapeContextWeight);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setStdDev_10(IntPtr nativeObj, float sigma);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_setTransformAlgorithm_10(IntPtr nativeObj, IntPtr transformer_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeContextDistanceExtractor_delete(IntPtr nativeObj);
	}
}
