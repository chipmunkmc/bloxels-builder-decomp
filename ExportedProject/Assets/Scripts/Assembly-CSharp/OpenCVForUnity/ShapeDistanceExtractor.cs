using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class ShapeDistanceExtractor : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected ShapeDistanceExtractor(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_ShapeDistanceExtractor_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float computeDistance(Mat contour1, Mat contour2)
		{
			ThrowIfDisposed();
			if (contour1 != null)
			{
				contour1.ThrowIfDisposed();
			}
			if (contour2 != null)
			{
				contour2.ThrowIfDisposed();
			}
			return shape_ShapeDistanceExtractor_computeDistance_10(nativeObj, contour1.nativeObj, contour2.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeDistanceExtractor_computeDistance_10(IntPtr nativeObj, IntPtr contour1_nativeObj, IntPtr contour2_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeDistanceExtractor_delete(IntPtr nativeObj);
	}
}
