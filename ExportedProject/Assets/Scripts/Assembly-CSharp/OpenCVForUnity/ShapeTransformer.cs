using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class ShapeTransformer : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public ShapeTransformer(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_ShapeTransformer_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float applyTransformation(Mat input, Mat output)
		{
			ThrowIfDisposed();
			if (input != null)
			{
				input.ThrowIfDisposed();
			}
			if (output != null)
			{
				output.ThrowIfDisposed();
			}
			return shape_ShapeTransformer_applyTransformation_10(nativeObj, input.nativeObj, output.nativeObj);
		}

		public float applyTransformation(Mat input)
		{
			ThrowIfDisposed();
			if (input != null)
			{
				input.ThrowIfDisposed();
			}
			return shape_ShapeTransformer_applyTransformation_11(nativeObj, input.nativeObj);
		}

		public void estimateTransformation(Mat transformingShape, Mat targetShape, MatOfDMatch matches)
		{
			ThrowIfDisposed();
			if (transformingShape != null)
			{
				transformingShape.ThrowIfDisposed();
			}
			if (targetShape != null)
			{
				targetShape.ThrowIfDisposed();
			}
			if (matches != null)
			{
				matches.ThrowIfDisposed();
			}
			shape_ShapeTransformer_estimateTransformation_10(nativeObj, transformingShape.nativeObj, targetShape.nativeObj, matches.nativeObj);
		}

		public void warpImage(Mat transformingImage, Mat output, int flags, int borderMode, Scalar borderValue)
		{
			ThrowIfDisposed();
			if (transformingImage != null)
			{
				transformingImage.ThrowIfDisposed();
			}
			if (output != null)
			{
				output.ThrowIfDisposed();
			}
			shape_ShapeTransformer_warpImage_10(nativeObj, transformingImage.nativeObj, output.nativeObj, flags, borderMode, borderValue.val[0], borderValue.val[1], borderValue.val[2], borderValue.val[3]);
		}

		public void warpImage(Mat transformingImage, Mat output, int flags)
		{
			ThrowIfDisposed();
			if (transformingImage != null)
			{
				transformingImage.ThrowIfDisposed();
			}
			if (output != null)
			{
				output.ThrowIfDisposed();
			}
			shape_ShapeTransformer_warpImage_11(nativeObj, transformingImage.nativeObj, output.nativeObj, flags);
		}

		public void warpImage(Mat transformingImage, Mat output)
		{
			ThrowIfDisposed();
			if (transformingImage != null)
			{
				transformingImage.ThrowIfDisposed();
			}
			if (output != null)
			{
				output.ThrowIfDisposed();
			}
			shape_ShapeTransformer_warpImage_12(nativeObj, transformingImage.nativeObj, output.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeTransformer_applyTransformation_10(IntPtr nativeObj, IntPtr input_nativeObj, IntPtr output_nativeObj);

		[DllImport("opencvforunity")]
		private static extern float shape_ShapeTransformer_applyTransformation_11(IntPtr nativeObj, IntPtr input_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeTransformer_estimateTransformation_10(IntPtr nativeObj, IntPtr transformingShape_nativeObj, IntPtr targetShape_nativeObj, IntPtr matches_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeTransformer_warpImage_10(IntPtr nativeObj, IntPtr transformingImage_nativeObj, IntPtr output_nativeObj, int flags, int borderMode, double borderValue_val0, double borderValue_val1, double borderValue_val2, double borderValue_val3);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeTransformer_warpImage_11(IntPtr nativeObj, IntPtr transformingImage_nativeObj, IntPtr output_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeTransformer_warpImage_12(IntPtr nativeObj, IntPtr transformingImage_nativeObj, IntPtr output_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ShapeTransformer_delete(IntPtr nativeObj);
	}
}
