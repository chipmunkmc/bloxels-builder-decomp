using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class Size
	{
		public double width;

		public double height;

		public Size(double width, double height)
		{
			this.width = width;
			this.height = height;
		}

		public Size()
			: this(0.0, 0.0)
		{
		}

		public Size(Point p)
		{
			width = p.x;
			height = p.y;
		}

		public Size(double[] vals)
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				width = ((vals.Length <= 0) ? 0.0 : vals[0]);
				height = ((vals.Length <= 1) ? 0.0 : vals[1]);
			}
			else
			{
				width = 0.0;
				height = 0.0;
			}
		}

		public double area()
		{
			return width * height;
		}

		public Size clone()
		{
			return new Size(width, height);
		}

		public override int GetHashCode()
		{
			int num = 1;
			long num2 = BitConverter.DoubleToInt64Bits(height);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(width);
			return 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is Size))
			{
				return false;
			}
			Size size = (Size)obj;
			return width == size.width && height == size.height;
		}

		public override string ToString()
		{
			return (int)width + "x" + (int)height;
		}
	}
}
