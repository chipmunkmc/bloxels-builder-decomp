using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class SparseMatchInterpolator : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		protected SparseMatchInterpolator(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_SparseMatchInterpolator_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void interpolate(Mat from_image, Mat from_points, Mat to_image, Mat to_points, Mat dense_flow)
		{
			ThrowIfDisposed();
			if (from_image != null)
			{
				from_image.ThrowIfDisposed();
			}
			if (from_points != null)
			{
				from_points.ThrowIfDisposed();
			}
			if (to_image != null)
			{
				to_image.ThrowIfDisposed();
			}
			if (to_points != null)
			{
				to_points.ThrowIfDisposed();
			}
			if (dense_flow != null)
			{
				dense_flow.ThrowIfDisposed();
			}
			ximgproc_SparseMatchInterpolator_interpolate_10(nativeObj, from_image.nativeObj, from_points.nativeObj, to_image.nativeObj, to_points.nativeObj, dense_flow.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SparseMatchInterpolator_interpolate_10(IntPtr nativeObj, IntPtr from_image_nativeObj, IntPtr from_points_nativeObj, IntPtr to_image_nativeObj, IntPtr to_points_nativeObj, IntPtr dense_flow_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SparseMatchInterpolator_delete(IntPtr nativeObj);
	}
}
