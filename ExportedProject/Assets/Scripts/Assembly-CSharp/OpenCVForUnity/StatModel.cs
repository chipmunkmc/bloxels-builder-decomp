using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class StatModel : Algorithm
	{
		public const int UPDATE_MODEL = 1;

		public const int RAW_OUTPUT = 1;

		public const int COMPRESSED_INPUT = 2;

		public const int PREPROCESSED_INPUT = 4;

		private const string LIBNAME = "opencvforunity";

		protected StatModel(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_StatModel_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool empty()
		{
			ThrowIfDisposed();
			return ml_StatModel_empty_10(nativeObj);
		}

		public bool isClassifier()
		{
			ThrowIfDisposed();
			return ml_StatModel_isClassifier_10(nativeObj);
		}

		public bool isTrained()
		{
			ThrowIfDisposed();
			return ml_StatModel_isTrained_10(nativeObj);
		}

		public bool train(Mat samples, int layout, Mat responses)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (responses != null)
			{
				responses.ThrowIfDisposed();
			}
			return ml_StatModel_train_10(nativeObj, samples.nativeObj, layout, responses.nativeObj);
		}

		public virtual float predict(Mat samples, Mat results, int flags)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			if (results != null)
			{
				results.ThrowIfDisposed();
			}
			return ml_StatModel_predict_10(nativeObj, samples.nativeObj, results.nativeObj, flags);
		}

		public virtual float predict(Mat samples)
		{
			ThrowIfDisposed();
			if (samples != null)
			{
				samples.ThrowIfDisposed();
			}
			return ml_StatModel_predict_11(nativeObj, samples.nativeObj);
		}

		public int getVarCount()
		{
			ThrowIfDisposed();
			return ml_StatModel_getVarCount_10(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern bool ml_StatModel_empty_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_StatModel_isClassifier_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_StatModel_isTrained_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool ml_StatModel_train_10(IntPtr nativeObj, IntPtr samples_nativeObj, int layout, IntPtr responses_nativeObj);

		[DllImport("opencvforunity")]
		private static extern float ml_StatModel_predict_10(IntPtr nativeObj, IntPtr samples_nativeObj, IntPtr results_nativeObj, int flags);

		[DllImport("opencvforunity")]
		private static extern float ml_StatModel_predict_11(IntPtr nativeObj, IntPtr samples_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_StatModel_getVarCount_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_StatModel_delete(IntPtr nativeObj);
	}
}
