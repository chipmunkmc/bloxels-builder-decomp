using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class StereoBM : StereoMatcher
	{
		public const int PREFILTER_NORMALIZED_RESPONSE = 0;

		public const int PREFILTER_XSOBEL = 1;

		private const string LIBNAME = "opencvforunity";

		protected StereoBM(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						calib3d_StereoBM_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public static StereoBM create(int numDisparities, int blockSize)
		{
			return new StereoBM(calib3d_StereoBM_create_10(numDisparities, blockSize));
		}

		public static StereoBM create()
		{
			return new StereoBM(calib3d_StereoBM_create_11());
		}

		public Rect getROI1()
		{
			ThrowIfDisposed();
			double[] vals = new double[4];
			calib3d_StereoBM_getROI1_10(nativeObj, vals);
			return new Rect(vals);
		}

		public Rect getROI2()
		{
			ThrowIfDisposed();
			double[] vals = new double[4];
			calib3d_StereoBM_getROI2_10(nativeObj, vals);
			return new Rect(vals);
		}

		public int getPreFilterCap()
		{
			ThrowIfDisposed();
			return calib3d_StereoBM_getPreFilterCap_10(nativeObj);
		}

		public int getPreFilterSize()
		{
			ThrowIfDisposed();
			return calib3d_StereoBM_getPreFilterSize_10(nativeObj);
		}

		public int getPreFilterType()
		{
			ThrowIfDisposed();
			return calib3d_StereoBM_getPreFilterType_10(nativeObj);
		}

		public int getSmallerBlockSize()
		{
			ThrowIfDisposed();
			return calib3d_StereoBM_getSmallerBlockSize_10(nativeObj);
		}

		public int getTextureThreshold()
		{
			ThrowIfDisposed();
			return calib3d_StereoBM_getTextureThreshold_10(nativeObj);
		}

		public int getUniquenessRatio()
		{
			ThrowIfDisposed();
			return calib3d_StereoBM_getUniquenessRatio_10(nativeObj);
		}

		public void setPreFilterCap(int preFilterCap)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setPreFilterCap_10(nativeObj, preFilterCap);
		}

		public void setPreFilterSize(int preFilterSize)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setPreFilterSize_10(nativeObj, preFilterSize);
		}

		public void setPreFilterType(int preFilterType)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setPreFilterType_10(nativeObj, preFilterType);
		}

		public void setROI1(Rect roi1)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setROI1_10(nativeObj, roi1.x, roi1.y, roi1.width, roi1.height);
		}

		public void setROI2(Rect roi2)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setROI2_10(nativeObj, roi2.x, roi2.y, roi2.width, roi2.height);
		}

		public void setSmallerBlockSize(int blockSize)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setSmallerBlockSize_10(nativeObj, blockSize);
		}

		public void setTextureThreshold(int textureThreshold)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setTextureThreshold_10(nativeObj, textureThreshold);
		}

		public void setUniquenessRatio(int uniquenessRatio)
		{
			ThrowIfDisposed();
			calib3d_StereoBM_setUniquenessRatio_10(nativeObj, uniquenessRatio);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_StereoBM_create_10(int numDisparities, int blockSize);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_StereoBM_create_11();

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_getROI1_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_getROI2_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoBM_getPreFilterCap_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoBM_getPreFilterSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoBM_getPreFilterType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoBM_getSmallerBlockSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoBM_getTextureThreshold_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoBM_getUniquenessRatio_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setPreFilterCap_10(IntPtr nativeObj, int preFilterCap);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setPreFilterSize_10(IntPtr nativeObj, int preFilterSize);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setPreFilterType_10(IntPtr nativeObj, int preFilterType);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setROI1_10(IntPtr nativeObj, int roi1_x, int roi1_y, int roi1_width, int roi1_height);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setROI2_10(IntPtr nativeObj, int roi2_x, int roi2_y, int roi2_width, int roi2_height);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setSmallerBlockSize_10(IntPtr nativeObj, int blockSize);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setTextureThreshold_10(IntPtr nativeObj, int textureThreshold);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_setUniquenessRatio_10(IntPtr nativeObj, int uniquenessRatio);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoBM_delete(IntPtr nativeObj);
	}
}
