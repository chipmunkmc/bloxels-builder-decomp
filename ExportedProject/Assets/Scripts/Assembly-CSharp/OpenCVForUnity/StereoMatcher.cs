using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class StereoMatcher : Algorithm
	{
		public const int DISP_SHIFT = 4;

		public const int DISP_SCALE = 16;

		private const string LIBNAME = "opencvforunity";

		protected StereoMatcher(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						calib3d_StereoMatcher_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public int getBlockSize()
		{
			ThrowIfDisposed();
			return calib3d_StereoMatcher_getBlockSize_10(nativeObj);
		}

		public int getDisp12MaxDiff()
		{
			ThrowIfDisposed();
			return calib3d_StereoMatcher_getDisp12MaxDiff_10(nativeObj);
		}

		public int getMinDisparity()
		{
			ThrowIfDisposed();
			return calib3d_StereoMatcher_getMinDisparity_10(nativeObj);
		}

		public int getNumDisparities()
		{
			ThrowIfDisposed();
			return calib3d_StereoMatcher_getNumDisparities_10(nativeObj);
		}

		public int getSpeckleRange()
		{
			ThrowIfDisposed();
			return calib3d_StereoMatcher_getSpeckleRange_10(nativeObj);
		}

		public int getSpeckleWindowSize()
		{
			ThrowIfDisposed();
			return calib3d_StereoMatcher_getSpeckleWindowSize_10(nativeObj);
		}

		public void compute(Mat left, Mat right, Mat disparity)
		{
			ThrowIfDisposed();
			if (left != null)
			{
				left.ThrowIfDisposed();
			}
			if (right != null)
			{
				right.ThrowIfDisposed();
			}
			if (disparity != null)
			{
				disparity.ThrowIfDisposed();
			}
			calib3d_StereoMatcher_compute_10(nativeObj, left.nativeObj, right.nativeObj, disparity.nativeObj);
		}

		public void setBlockSize(int blockSize)
		{
			ThrowIfDisposed();
			calib3d_StereoMatcher_setBlockSize_10(nativeObj, blockSize);
		}

		public void setDisp12MaxDiff(int disp12MaxDiff)
		{
			ThrowIfDisposed();
			calib3d_StereoMatcher_setDisp12MaxDiff_10(nativeObj, disp12MaxDiff);
		}

		public void setMinDisparity(int minDisparity)
		{
			ThrowIfDisposed();
			calib3d_StereoMatcher_setMinDisparity_10(nativeObj, minDisparity);
		}

		public void setNumDisparities(int numDisparities)
		{
			ThrowIfDisposed();
			calib3d_StereoMatcher_setNumDisparities_10(nativeObj, numDisparities);
		}

		public void setSpeckleRange(int speckleRange)
		{
			ThrowIfDisposed();
			calib3d_StereoMatcher_setSpeckleRange_10(nativeObj, speckleRange);
		}

		public void setSpeckleWindowSize(int speckleWindowSize)
		{
			ThrowIfDisposed();
			calib3d_StereoMatcher_setSpeckleWindowSize_10(nativeObj, speckleWindowSize);
		}

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoMatcher_getBlockSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoMatcher_getDisp12MaxDiff_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoMatcher_getMinDisparity_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoMatcher_getNumDisparities_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoMatcher_getSpeckleRange_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoMatcher_getSpeckleWindowSize_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_compute_10(IntPtr nativeObj, IntPtr left_nativeObj, IntPtr right_nativeObj, IntPtr disparity_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_setBlockSize_10(IntPtr nativeObj, int blockSize);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_setDisp12MaxDiff_10(IntPtr nativeObj, int disp12MaxDiff);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_setMinDisparity_10(IntPtr nativeObj, int minDisparity);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_setNumDisparities_10(IntPtr nativeObj, int numDisparities);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_setSpeckleRange_10(IntPtr nativeObj, int speckleRange);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_setSpeckleWindowSize_10(IntPtr nativeObj, int speckleWindowSize);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoMatcher_delete(IntPtr nativeObj);
	}
}
