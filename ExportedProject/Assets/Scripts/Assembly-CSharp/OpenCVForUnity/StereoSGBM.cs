using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class StereoSGBM : StereoMatcher
	{
		public const int MODE_SGBM = 0;

		public const int MODE_HH = 1;

		public const int MODE_SGBM_3WAY = 2;

		private const string LIBNAME = "opencvforunity";

		protected StereoSGBM(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						calib3d_StereoSGBM_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public static StereoSGBM create(int minDisparity, int numDisparities, int blockSize, int P1, int P2, int disp12MaxDiff, int preFilterCap, int uniquenessRatio, int speckleWindowSize, int speckleRange, int mode)
		{
			return new StereoSGBM(calib3d_StereoSGBM_create_10(minDisparity, numDisparities, blockSize, P1, P2, disp12MaxDiff, preFilterCap, uniquenessRatio, speckleWindowSize, speckleRange, mode));
		}

		public static StereoSGBM create(int minDisparity, int numDisparities, int blockSize)
		{
			return new StereoSGBM(calib3d_StereoSGBM_create_11(minDisparity, numDisparities, blockSize));
		}

		public int getMode()
		{
			ThrowIfDisposed();
			return calib3d_StereoSGBM_getMode_10(nativeObj);
		}

		public int getP1()
		{
			ThrowIfDisposed();
			return calib3d_StereoSGBM_getP1_10(nativeObj);
		}

		public int getP2()
		{
			ThrowIfDisposed();
			return calib3d_StereoSGBM_getP2_10(nativeObj);
		}

		public int getPreFilterCap()
		{
			ThrowIfDisposed();
			return calib3d_StereoSGBM_getPreFilterCap_10(nativeObj);
		}

		public int getUniquenessRatio()
		{
			ThrowIfDisposed();
			return calib3d_StereoSGBM_getUniquenessRatio_10(nativeObj);
		}

		public void setMode(int mode)
		{
			ThrowIfDisposed();
			calib3d_StereoSGBM_setMode_10(nativeObj, mode);
		}

		public void setP1(int P1)
		{
			ThrowIfDisposed();
			calib3d_StereoSGBM_setP1_10(nativeObj, P1);
		}

		public void setP2(int P2)
		{
			ThrowIfDisposed();
			calib3d_StereoSGBM_setP2_10(nativeObj, P2);
		}

		public void setPreFilterCap(int preFilterCap)
		{
			ThrowIfDisposed();
			calib3d_StereoSGBM_setPreFilterCap_10(nativeObj, preFilterCap);
		}

		public void setUniquenessRatio(int uniquenessRatio)
		{
			ThrowIfDisposed();
			calib3d_StereoSGBM_setUniquenessRatio_10(nativeObj, uniquenessRatio);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_StereoSGBM_create_10(int minDisparity, int numDisparities, int blockSize, int P1, int P2, int disp12MaxDiff, int preFilterCap, int uniquenessRatio, int speckleWindowSize, int speckleRange, int mode);

		[DllImport("opencvforunity")]
		private static extern IntPtr calib3d_StereoSGBM_create_11(int minDisparity, int numDisparities, int blockSize);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoSGBM_getMode_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoSGBM_getP1_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoSGBM_getP2_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoSGBM_getPreFilterCap_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int calib3d_StereoSGBM_getUniquenessRatio_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoSGBM_setMode_10(IntPtr nativeObj, int mode);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoSGBM_setP1_10(IntPtr nativeObj, int P1);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoSGBM_setP2_10(IntPtr nativeObj, int P2);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoSGBM_setPreFilterCap_10(IntPtr nativeObj, int preFilterCap);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoSGBM_setUniquenessRatio_10(IntPtr nativeObj, int uniquenessRatio);

		[DllImport("opencvforunity")]
		private static extern void calib3d_StereoSGBM_delete(IntPtr nativeObj);
	}
}
