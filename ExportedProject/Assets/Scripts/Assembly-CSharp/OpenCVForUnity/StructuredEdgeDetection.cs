using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class StructuredEdgeDetection : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public StructuredEdgeDetection(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_StructuredEdgeDetection_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public void detectEdges(Mat src, Mat dst)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_StructuredEdgeDetection_detectEdges_10(nativeObj, src.nativeObj, dst.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern void ximgproc_StructuredEdgeDetection_detectEdges_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_StructuredEdgeDetection_delete(IntPtr nativeObj);
	}
}
