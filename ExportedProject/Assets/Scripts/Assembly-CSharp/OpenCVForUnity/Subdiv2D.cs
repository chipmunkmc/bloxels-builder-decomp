using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Subdiv2D : DisposableOpenCVObject
	{
		public const int PTLOC_ERROR = -2;

		public const int PTLOC_OUTSIDE_RECT = -1;

		public const int PTLOC_INSIDE = 0;

		public const int PTLOC_VERTEX = 1;

		public const int PTLOC_ON_EDGE = 2;

		public const int NEXT_AROUND_ORG = 0;

		public const int NEXT_AROUND_DST = 34;

		public const int PREV_AROUND_ORG = 17;

		public const int PREV_AROUND_DST = 51;

		public const int NEXT_AROUND_LEFT = 19;

		public const int NEXT_AROUND_RIGHT = 49;

		public const int PREV_AROUND_LEFT = 32;

		public const int PREV_AROUND_RIGHT = 2;

		private const string LIBNAME = "opencvforunity";

		protected Subdiv2D(IntPtr addr)
			: base(addr)
		{
		}

		public Subdiv2D(Rect rect)
		{
			nativeObj = imgproc_Subdiv2D_Subdiv2D_10(rect.x, rect.y, rect.width, rect.height);
		}

		public Subdiv2D()
		{
			nativeObj = imgproc_Subdiv2D_Subdiv2D_11();
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						imgproc_Subdiv2D_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Point getVertex(int vertex, int[] firstEdge)
		{
			ThrowIfDisposed();
			double[] array = new double[1];
			double[] vals = new double[2];
			imgproc_Subdiv2D_getVertex_10(nativeObj, vertex, array, vals);
			Point result = new Point(vals);
			if (firstEdge != null)
			{
				firstEdge[0] = (int)array[0];
			}
			return result;
		}

		public Point getVertex(int vertex)
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			imgproc_Subdiv2D_getVertex_11(nativeObj, vertex, vals);
			return new Point(vals);
		}

		public int edgeDst(int edge, Point dstpt)
		{
			ThrowIfDisposed();
			double[] array = new double[2];
			int result = imgproc_Subdiv2D_edgeDst_10(nativeObj, edge, array);
			if (dstpt != null)
			{
				dstpt.x = array[0];
				dstpt.y = array[1];
			}
			return result;
		}

		public int edgeDst(int edge)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_edgeDst_11(nativeObj, edge);
		}

		public int edgeOrg(int edge, Point orgpt)
		{
			ThrowIfDisposed();
			double[] array = new double[2];
			int result = imgproc_Subdiv2D_edgeOrg_10(nativeObj, edge, array);
			if (orgpt != null)
			{
				orgpt.x = array[0];
				orgpt.y = array[1];
			}
			return result;
		}

		public int edgeOrg(int edge)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_edgeOrg_11(nativeObj, edge);
		}

		public int findNearest(Point pt, Point nearestPt)
		{
			ThrowIfDisposed();
			double[] array = new double[2];
			int result = imgproc_Subdiv2D_findNearest_10(nativeObj, pt.x, pt.y, array);
			if (nearestPt != null)
			{
				nearestPt.x = array[0];
				nearestPt.y = array[1];
			}
			return result;
		}

		public int findNearest(Point pt)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_findNearest_11(nativeObj, pt.x, pt.y);
		}

		public int getEdge(int edge, int nextEdgeType)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_getEdge_10(nativeObj, edge, nextEdgeType);
		}

		public int insert(Point pt)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_insert_10(nativeObj, pt.x, pt.y);
		}

		public int locate(Point pt, int[] edge, int[] vertex)
		{
			ThrowIfDisposed();
			double[] array = new double[1];
			double[] array2 = new double[1];
			int result = imgproc_Subdiv2D_locate_10(nativeObj, pt.x, pt.y, array, array2);
			if (edge != null)
			{
				edge[0] = (int)array[0];
			}
			if (vertex != null)
			{
				vertex[0] = (int)array2[0];
			}
			return result;
		}

		public int nextEdge(int edge)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_nextEdge_10(nativeObj, edge);
		}

		public int rotateEdge(int edge, int rotate)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_rotateEdge_10(nativeObj, edge, rotate);
		}

		public int symEdge(int edge)
		{
			ThrowIfDisposed();
			return imgproc_Subdiv2D_symEdge_10(nativeObj, edge);
		}

		public void getEdgeList(MatOfFloat4 edgeList)
		{
			ThrowIfDisposed();
			if (edgeList != null)
			{
				edgeList.ThrowIfDisposed();
			}
			imgproc_Subdiv2D_getEdgeList_10(nativeObj, edgeList.nativeObj);
		}

		public void getTriangleList(MatOfFloat6 triangleList)
		{
			ThrowIfDisposed();
			if (triangleList != null)
			{
				triangleList.ThrowIfDisposed();
			}
			imgproc_Subdiv2D_getTriangleList_10(nativeObj, triangleList.nativeObj);
		}

		public void getVoronoiFacetList(MatOfInt idx, List<MatOfPoint2f> facetList, MatOfPoint2f facetCenters)
		{
			ThrowIfDisposed();
			if (idx != null)
			{
				idx.ThrowIfDisposed();
			}
			if (facetCenters != null)
			{
				facetCenters.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			imgproc_Subdiv2D_getVoronoiFacetList_10(nativeObj, idx.nativeObj, mat.nativeObj, facetCenters.nativeObj);
			Converters.Mat_to_vector_vector_Point2f(mat, facetList);
			mat.release();
		}

		public void initDelaunay(Rect rect)
		{
			ThrowIfDisposed();
			imgproc_Subdiv2D_initDelaunay_10(nativeObj, rect.x, rect.y, rect.width, rect.height);
		}

		public void insert(MatOfPoint2f ptvec)
		{
			ThrowIfDisposed();
			if (ptvec != null)
			{
				ptvec.ThrowIfDisposed();
			}
			imgproc_Subdiv2D_insert_11(nativeObj, ptvec.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Subdiv2D_Subdiv2D_10(int rect_x, int rect_y, int rect_width, int rect_height);

		[DllImport("opencvforunity")]
		private static extern IntPtr imgproc_Subdiv2D_Subdiv2D_11();

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_getVertex_10(IntPtr nativeObj, int vertex, double[] firstEdge_out, double[] vals);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_getVertex_11(IntPtr nativeObj, int vertex, double[] vals);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_edgeDst_10(IntPtr nativeObj, int edge, double[] dstpt_out);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_edgeDst_11(IntPtr nativeObj, int edge);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_edgeOrg_10(IntPtr nativeObj, int edge, double[] orgpt_out);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_edgeOrg_11(IntPtr nativeObj, int edge);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_findNearest_10(IntPtr nativeObj, double pt_x, double pt_y, double[] nearestPt_out);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_findNearest_11(IntPtr nativeObj, double pt_x, double pt_y);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_getEdge_10(IntPtr nativeObj, int edge, int nextEdgeType);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_insert_10(IntPtr nativeObj, double pt_x, double pt_y);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_locate_10(IntPtr nativeObj, double pt_x, double pt_y, double[] edge_out, double[] vertex_out);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_nextEdge_10(IntPtr nativeObj, int edge);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_rotateEdge_10(IntPtr nativeObj, int edge, int rotate);

		[DllImport("opencvforunity")]
		private static extern int imgproc_Subdiv2D_symEdge_10(IntPtr nativeObj, int edge);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_getEdgeList_10(IntPtr nativeObj, IntPtr edgeList_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_getTriangleList_10(IntPtr nativeObj, IntPtr triangleList_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_getVoronoiFacetList_10(IntPtr nativeObj, IntPtr idx_mat_nativeObj, IntPtr facetList_mat_nativeObj, IntPtr facetCenters_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_initDelaunay_10(IntPtr nativeObj, int rect_x, int rect_y, int rect_width, int rect_height);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_insert_11(IntPtr nativeObj, IntPtr ptvec_mat_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void imgproc_Subdiv2D_delete(IntPtr nativeObj);
	}
}
