using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class SuperpixelSEEDS : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public SuperpixelSEEDS(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_SuperpixelSEEDS_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public int getNumberOfSuperpixels()
		{
			ThrowIfDisposed();
			return ximgproc_SuperpixelSEEDS_getNumberOfSuperpixels_10(nativeObj);
		}

		public void getLabelContourMask(Mat image, bool thick_line)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSEEDS_getLabelContourMask_10(nativeObj, image.nativeObj, thick_line);
		}

		public void getLabelContourMask(Mat image)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSEEDS_getLabelContourMask_11(nativeObj, image.nativeObj);
		}

		public void getLabels(Mat labels_out)
		{
			ThrowIfDisposed();
			if (labels_out != null)
			{
				labels_out.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSEEDS_getLabels_10(nativeObj, labels_out.nativeObj);
		}

		public void iterate(Mat img, int num_iterations)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSEEDS_iterate_10(nativeObj, img.nativeObj, num_iterations);
		}

		public void iterate(Mat img)
		{
			ThrowIfDisposed();
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSEEDS_iterate_11(nativeObj, img.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern int ximgproc_SuperpixelSEEDS_getNumberOfSuperpixels_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSEEDS_getLabelContourMask_10(IntPtr nativeObj, IntPtr image_nativeObj, bool thick_line);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSEEDS_getLabelContourMask_11(IntPtr nativeObj, IntPtr image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSEEDS_getLabels_10(IntPtr nativeObj, IntPtr labels_out_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSEEDS_iterate_10(IntPtr nativeObj, IntPtr img_nativeObj, int num_iterations);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSEEDS_iterate_11(IntPtr nativeObj, IntPtr img_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSEEDS_delete(IntPtr nativeObj);
	}
}
