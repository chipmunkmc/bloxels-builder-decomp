using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class SuperpixelSLIC : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public SuperpixelSLIC(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ximgproc_SuperpixelSLIC_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public int getNumberOfSuperpixels()
		{
			ThrowIfDisposed();
			return ximgproc_SuperpixelSLIC_getNumberOfSuperpixels_10(nativeObj);
		}

		public void enforceLabelConnectivity(int min_element_size)
		{
			ThrowIfDisposed();
			ximgproc_SuperpixelSLIC_enforceLabelConnectivity_10(nativeObj, min_element_size);
		}

		public void enforceLabelConnectivity()
		{
			ThrowIfDisposed();
			ximgproc_SuperpixelSLIC_enforceLabelConnectivity_11(nativeObj);
		}

		public void getLabelContourMask(Mat image, bool thick_line)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSLIC_getLabelContourMask_10(nativeObj, image.nativeObj, thick_line);
		}

		public void getLabelContourMask(Mat image)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSLIC_getLabelContourMask_11(nativeObj, image.nativeObj);
		}

		public void getLabels(Mat labels_out)
		{
			ThrowIfDisposed();
			if (labels_out != null)
			{
				labels_out.ThrowIfDisposed();
			}
			ximgproc_SuperpixelSLIC_getLabels_10(nativeObj, labels_out.nativeObj);
		}

		public void iterate(int num_iterations)
		{
			ThrowIfDisposed();
			ximgproc_SuperpixelSLIC_iterate_10(nativeObj, num_iterations);
		}

		public void iterate()
		{
			ThrowIfDisposed();
			ximgproc_SuperpixelSLIC_iterate_11(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern int ximgproc_SuperpixelSLIC_getNumberOfSuperpixels_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_enforceLabelConnectivity_10(IntPtr nativeObj, int min_element_size);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_enforceLabelConnectivity_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_getLabelContourMask_10(IntPtr nativeObj, IntPtr image_nativeObj, bool thick_line);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_getLabelContourMask_11(IntPtr nativeObj, IntPtr image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_getLabels_10(IntPtr nativeObj, IntPtr labels_out_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_iterate_10(IntPtr nativeObj, int num_iterations);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_iterate_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_SuperpixelSLIC_delete(IntPtr nativeObj);
	}
}
