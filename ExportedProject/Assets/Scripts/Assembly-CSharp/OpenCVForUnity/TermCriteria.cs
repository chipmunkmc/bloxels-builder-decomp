using System;

namespace OpenCVForUnity
{
	[Serializable]
	public class TermCriteria
	{
		public const int COUNT = 1;

		public const int MAX_ITER = 1;

		public const int EPS = 2;

		public int type;

		public int maxCount;

		public double epsilon;

		public TermCriteria(int type, int maxCount, double epsilon)
		{
			this.type = type;
			this.maxCount = maxCount;
			this.epsilon = epsilon;
		}

		public TermCriteria()
			: this(0, 0, 0.0)
		{
		}

		public TermCriteria(double[] vals)
		{
			set(vals);
		}

		public void set(double[] vals)
		{
			if (vals != null)
			{
				type = ((vals.Length > 0) ? ((int)vals[0]) : 0);
				maxCount = ((vals.Length > 1) ? ((int)vals[1]) : 0);
				epsilon = ((vals.Length <= 2) ? 0.0 : vals[2]);
			}
			else
			{
				type = 0;
				maxCount = 0;
				epsilon = 0.0;
			}
		}

		public TermCriteria clone()
		{
			return new TermCriteria(type, maxCount, epsilon);
		}

		public override int GetHashCode()
		{
			int num = 1;
			long num2 = BitConverter.DoubleToInt64Bits(type);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(maxCount);
			num = 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
			num2 = BitConverter.DoubleToInt64Bits(epsilon);
			return 31 * num + (int)(num2 ^ Utils.URShift(num2, 32));
		}

		public override bool Equals(object obj)
		{
			if (this == obj)
			{
				return true;
			}
			if (!(obj is TermCriteria))
			{
				return false;
			}
			TermCriteria termCriteria = (TermCriteria)obj;
			return type == termCriteria.type && maxCount == termCriteria.maxCount && epsilon == termCriteria.epsilon;
		}

		public override string ToString()
		{
			return "{ type: " + type + ", maxCount: " + maxCount + ", epsilon: " + epsilon + "}";
		}
	}
}
