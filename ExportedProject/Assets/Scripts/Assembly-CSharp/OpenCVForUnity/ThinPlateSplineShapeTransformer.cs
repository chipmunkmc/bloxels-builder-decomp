using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class ThinPlateSplineShapeTransformer : ShapeTransformer
	{
		private const string LIBNAME = "opencvforunity";

		public ThinPlateSplineShapeTransformer(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						shape_ThinPlateSplineShapeTransformer_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public double getRegularizationParameter()
		{
			ThrowIfDisposed();
			return shape_ThinPlateSplineShapeTransformer_getRegularizationParameter_10(nativeObj);
		}

		public void setRegularizationParameter(double beta)
		{
			ThrowIfDisposed();
			shape_ThinPlateSplineShapeTransformer_setRegularizationParameter_10(nativeObj, beta);
		}

		[DllImport("opencvforunity")]
		private static extern double shape_ThinPlateSplineShapeTransformer_getRegularizationParameter_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void shape_ThinPlateSplineShapeTransformer_setRegularizationParameter_10(IntPtr nativeObj, double beta);

		[DllImport("opencvforunity")]
		private static extern void shape_ThinPlateSplineShapeTransformer_delete(IntPtr nativeObj);
	}
}
