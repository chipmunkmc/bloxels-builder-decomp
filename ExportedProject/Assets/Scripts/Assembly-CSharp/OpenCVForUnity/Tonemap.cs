using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Tonemap : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public Tonemap(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_Tonemap_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getGamma()
		{
			ThrowIfDisposed();
			return photo_Tonemap_getGamma_10(nativeObj);
		}

		public void process(Mat src, Mat dst)
		{
			ThrowIfDisposed();
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			photo_Tonemap_process_10(nativeObj, src.nativeObj, dst.nativeObj);
		}

		public void setGamma(float gamma)
		{
			ThrowIfDisposed();
			photo_Tonemap_setGamma_10(nativeObj, gamma);
		}

		[DllImport("opencvforunity")]
		private static extern float photo_Tonemap_getGamma_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Tonemap_process_10(IntPtr nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_Tonemap_setGamma_10(IntPtr nativeObj, float gamma);

		[DllImport("opencvforunity")]
		private static extern void photo_Tonemap_delete(IntPtr nativeObj);
	}
}
