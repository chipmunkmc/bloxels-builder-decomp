using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class TonemapDrago : Tonemap
	{
		private const string LIBNAME = "opencvforunity";

		public TonemapDrago(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_TonemapDrago_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getBias()
		{
			ThrowIfDisposed();
			return photo_TonemapDrago_getBias_10(nativeObj);
		}

		public float getSaturation()
		{
			ThrowIfDisposed();
			return photo_TonemapDrago_getSaturation_10(nativeObj);
		}

		public void setBias(float bias)
		{
			ThrowIfDisposed();
			photo_TonemapDrago_setBias_10(nativeObj, bias);
		}

		public void setSaturation(float saturation)
		{
			ThrowIfDisposed();
			photo_TonemapDrago_setSaturation_10(nativeObj, saturation);
		}

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapDrago_getBias_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapDrago_getSaturation_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDrago_setBias_10(IntPtr nativeObj, float bias);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDrago_setSaturation_10(IntPtr nativeObj, float saturation);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDrago_delete(IntPtr nativeObj);
	}
}
