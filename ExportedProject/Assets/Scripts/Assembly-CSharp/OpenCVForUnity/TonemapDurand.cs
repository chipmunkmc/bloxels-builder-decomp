using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class TonemapDurand : Tonemap
	{
		private const string LIBNAME = "opencvforunity";

		public TonemapDurand(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_TonemapDurand_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getContrast()
		{
			ThrowIfDisposed();
			return photo_TonemapDurand_getContrast_10(nativeObj);
		}

		public float getSaturation()
		{
			ThrowIfDisposed();
			return photo_TonemapDurand_getSaturation_10(nativeObj);
		}

		public float getSigmaColor()
		{
			ThrowIfDisposed();
			return photo_TonemapDurand_getSigmaColor_10(nativeObj);
		}

		public float getSigmaSpace()
		{
			ThrowIfDisposed();
			return photo_TonemapDurand_getSigmaSpace_10(nativeObj);
		}

		public void setContrast(float contrast)
		{
			ThrowIfDisposed();
			photo_TonemapDurand_setContrast_10(nativeObj, contrast);
		}

		public void setSaturation(float saturation)
		{
			ThrowIfDisposed();
			photo_TonemapDurand_setSaturation_10(nativeObj, saturation);
		}

		public void setSigmaColor(float sigma_color)
		{
			ThrowIfDisposed();
			photo_TonemapDurand_setSigmaColor_10(nativeObj, sigma_color);
		}

		public void setSigmaSpace(float sigma_space)
		{
			ThrowIfDisposed();
			photo_TonemapDurand_setSigmaSpace_10(nativeObj, sigma_space);
		}

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapDurand_getContrast_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapDurand_getSaturation_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapDurand_getSigmaColor_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapDurand_getSigmaSpace_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDurand_setContrast_10(IntPtr nativeObj, float contrast);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDurand_setSaturation_10(IntPtr nativeObj, float saturation);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDurand_setSigmaColor_10(IntPtr nativeObj, float sigma_color);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDurand_setSigmaSpace_10(IntPtr nativeObj, float sigma_space);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapDurand_delete(IntPtr nativeObj);
	}
}
