using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class TonemapMantiuk : Tonemap
	{
		private const string LIBNAME = "opencvforunity";

		public TonemapMantiuk(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_TonemapMantiuk_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getSaturation()
		{
			ThrowIfDisposed();
			return photo_TonemapMantiuk_getSaturation_10(nativeObj);
		}

		public float getScale()
		{
			ThrowIfDisposed();
			return photo_TonemapMantiuk_getScale_10(nativeObj);
		}

		public void setSaturation(float saturation)
		{
			ThrowIfDisposed();
			photo_TonemapMantiuk_setSaturation_10(nativeObj, saturation);
		}

		public void setScale(float scale)
		{
			ThrowIfDisposed();
			photo_TonemapMantiuk_setScale_10(nativeObj, scale);
		}

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapMantiuk_getSaturation_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapMantiuk_getScale_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapMantiuk_setSaturation_10(IntPtr nativeObj, float saturation);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapMantiuk_setScale_10(IntPtr nativeObj, float scale);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapMantiuk_delete(IntPtr nativeObj);
	}
}
