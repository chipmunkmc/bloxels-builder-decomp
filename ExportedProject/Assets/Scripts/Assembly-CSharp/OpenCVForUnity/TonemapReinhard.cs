using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class TonemapReinhard : Tonemap
	{
		private const string LIBNAME = "opencvforunity";

		public TonemapReinhard(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						photo_TonemapReinhard_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public float getColorAdaptation()
		{
			ThrowIfDisposed();
			return photo_TonemapReinhard_getColorAdaptation_10(nativeObj);
		}

		public float getIntensity()
		{
			ThrowIfDisposed();
			return photo_TonemapReinhard_getIntensity_10(nativeObj);
		}

		public float getLightAdaptation()
		{
			ThrowIfDisposed();
			return photo_TonemapReinhard_getLightAdaptation_10(nativeObj);
		}

		public void setColorAdaptation(float color_adapt)
		{
			ThrowIfDisposed();
			photo_TonemapReinhard_setColorAdaptation_10(nativeObj, color_adapt);
		}

		public void setIntensity(float intensity)
		{
			ThrowIfDisposed();
			photo_TonemapReinhard_setIntensity_10(nativeObj, intensity);
		}

		public void setLightAdaptation(float light_adapt)
		{
			ThrowIfDisposed();
			photo_TonemapReinhard_setLightAdaptation_10(nativeObj, light_adapt);
		}

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapReinhard_getColorAdaptation_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapReinhard_getIntensity_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern float photo_TonemapReinhard_getLightAdaptation_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapReinhard_setColorAdaptation_10(IntPtr nativeObj, float color_adapt);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapReinhard_setIntensity_10(IntPtr nativeObj, float intensity);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapReinhard_setLightAdaptation_10(IntPtr nativeObj, float light_adapt);

		[DllImport("opencvforunity")]
		private static extern void photo_TonemapReinhard_delete(IntPtr nativeObj);
	}
}
