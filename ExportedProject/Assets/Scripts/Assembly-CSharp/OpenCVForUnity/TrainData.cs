using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class TrainData : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		protected TrainData(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						ml_TrainData_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Mat getCatMap()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getCatMap_10(nativeObj));
		}

		public Mat getCatOfs()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getCatOfs_10(nativeObj));
		}

		public Mat getClassLabels()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getClassLabels_10(nativeObj));
		}

		public Mat getDefaultSubstValues()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getDefaultSubstValues_10(nativeObj));
		}

		public Mat getMissing()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getMissing_10(nativeObj));
		}

		public Mat getNormCatResponses()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getNormCatResponses_10(nativeObj));
		}

		public Mat getResponses()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getResponses_10(nativeObj));
		}

		public Mat getSampleWeights()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getSampleWeights_10(nativeObj));
		}

		public Mat getSamples()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getSamples_10(nativeObj));
		}

		public static Mat getSubVector(Mat vec, Mat idx)
		{
			if (vec != null)
			{
				vec.ThrowIfDisposed();
			}
			if (idx != null)
			{
				idx.ThrowIfDisposed();
			}
			return new Mat(ml_TrainData_getSubVector_10(vec.nativeObj, idx.nativeObj));
		}

		public Mat getTestNormCatResponses()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTestNormCatResponses_10(nativeObj));
		}

		public Mat getTestResponses()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTestResponses_10(nativeObj));
		}

		public Mat getTestSampleIdx()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTestSampleIdx_10(nativeObj));
		}

		public Mat getTestSampleWeights()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTestSampleWeights_10(nativeObj));
		}

		public Mat getTrainNormCatResponses()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTrainNormCatResponses_10(nativeObj));
		}

		public Mat getTrainResponses()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTrainResponses_10(nativeObj));
		}

		public Mat getTrainSampleIdx()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTrainSampleIdx_10(nativeObj));
		}

		public Mat getTrainSampleWeights()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTrainSampleWeights_10(nativeObj));
		}

		public Mat getTrainSamples(int layout, bool compressSamples, bool compressVars)
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTrainSamples_10(nativeObj, layout, compressSamples, compressVars));
		}

		public Mat getTrainSamples()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getTrainSamples_11(nativeObj));
		}

		public Mat getVarIdx()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getVarIdx_10(nativeObj));
		}

		public Mat getVarType()
		{
			ThrowIfDisposed();
			return new Mat(ml_TrainData_getVarType_10(nativeObj));
		}

		public int getCatCount(int vi)
		{
			ThrowIfDisposed();
			return ml_TrainData_getCatCount_10(nativeObj, vi);
		}

		public int getLayout()
		{
			ThrowIfDisposed();
			return ml_TrainData_getLayout_10(nativeObj);
		}

		public int getNAllVars()
		{
			ThrowIfDisposed();
			return ml_TrainData_getNAllVars_10(nativeObj);
		}

		public int getNSamples()
		{
			ThrowIfDisposed();
			return ml_TrainData_getNSamples_10(nativeObj);
		}

		public int getNTestSamples()
		{
			ThrowIfDisposed();
			return ml_TrainData_getNTestSamples_10(nativeObj);
		}

		public int getNTrainSamples()
		{
			ThrowIfDisposed();
			return ml_TrainData_getNTrainSamples_10(nativeObj);
		}

		public int getNVars()
		{
			ThrowIfDisposed();
			return ml_TrainData_getNVars_10(nativeObj);
		}

		public int getResponseType()
		{
			ThrowIfDisposed();
			return ml_TrainData_getResponseType_10(nativeObj);
		}

		public void getSample(Mat varIdx, int sidx, float buf)
		{
			ThrowIfDisposed();
			if (varIdx != null)
			{
				varIdx.ThrowIfDisposed();
			}
			ml_TrainData_getSample_10(nativeObj, varIdx.nativeObj, sidx, buf);
		}

		public void getValues(int vi, Mat sidx, float values)
		{
			ThrowIfDisposed();
			if (sidx != null)
			{
				sidx.ThrowIfDisposed();
			}
			ml_TrainData_getValues_10(nativeObj, vi, sidx.nativeObj, values);
		}

		public void setTrainTestSplit(int count, bool shuffle)
		{
			ThrowIfDisposed();
			ml_TrainData_setTrainTestSplit_10(nativeObj, count, shuffle);
		}

		public void setTrainTestSplit(int count)
		{
			ThrowIfDisposed();
			ml_TrainData_setTrainTestSplit_11(nativeObj, count);
		}

		public void setTrainTestSplitRatio(double ratio, bool shuffle)
		{
			ThrowIfDisposed();
			ml_TrainData_setTrainTestSplitRatio_10(nativeObj, ratio, shuffle);
		}

		public void setTrainTestSplitRatio(double ratio)
		{
			ThrowIfDisposed();
			ml_TrainData_setTrainTestSplitRatio_11(nativeObj, ratio);
		}

		public void shuffleTrainTest()
		{
			ThrowIfDisposed();
			ml_TrainData_shuffleTrainTest_10(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getCatMap_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getCatOfs_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getClassLabels_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getDefaultSubstValues_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getMissing_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getNormCatResponses_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getResponses_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getSampleWeights_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getSamples_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getSubVector_10(IntPtr vec_nativeObj, IntPtr idx_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTestNormCatResponses_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTestResponses_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTestSampleIdx_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTestSampleWeights_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTrainNormCatResponses_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTrainResponses_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTrainSampleIdx_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTrainSampleWeights_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTrainSamples_10(IntPtr nativeObj, int layout, bool compressSamples, bool compressVars);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getTrainSamples_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getVarIdx_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ml_TrainData_getVarType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getCatCount_10(IntPtr nativeObj, int vi);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getLayout_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getNAllVars_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getNSamples_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getNTestSamples_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getNTrainSamples_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getNVars_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern int ml_TrainData_getResponseType_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_getSample_10(IntPtr nativeObj, IntPtr varIdx_nativeObj, int sidx, float buf);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_getValues_10(IntPtr nativeObj, int vi, IntPtr sidx_nativeObj, float values);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_setTrainTestSplit_10(IntPtr nativeObj, int count, bool shuffle);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_setTrainTestSplit_11(IntPtr nativeObj, int count);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_setTrainTestSplitRatio_10(IntPtr nativeObj, double ratio, bool shuffle);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_setTrainTestSplitRatio_11(IntPtr nativeObj, double ratio);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_shuffleTrainTest_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ml_TrainData_delete(IntPtr nativeObj);
	}
}
