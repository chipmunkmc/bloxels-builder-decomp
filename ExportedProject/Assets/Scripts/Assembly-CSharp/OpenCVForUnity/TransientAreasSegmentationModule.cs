using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class TransientAreasSegmentationModule : Algorithm
	{
		private const string LIBNAME = "opencvforunity";

		public TransientAreasSegmentationModule(IntPtr addr)
			: base(addr)
		{
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						bioinspired_TransientAreasSegmentationModule_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public Size getSize()
		{
			ThrowIfDisposed();
			double[] vals = new double[2];
			bioinspired_TransientAreasSegmentationModule_getSize_10(nativeObj, vals);
			return new Size(vals);
		}

		public string printSetup()
		{
			ThrowIfDisposed();
			return Marshal.PtrToStringAnsi(bioinspired_TransientAreasSegmentationModule_printSetup_10(nativeObj));
		}

		public void clearAllBuffers()
		{
			ThrowIfDisposed();
			bioinspired_TransientAreasSegmentationModule_clearAllBuffers_10(nativeObj);
		}

		public void getSegmentationPicture(Mat transientAreas)
		{
			ThrowIfDisposed();
			if (transientAreas != null)
			{
				transientAreas.ThrowIfDisposed();
			}
			bioinspired_TransientAreasSegmentationModule_getSegmentationPicture_10(nativeObj, transientAreas.nativeObj);
		}

		public void run(Mat inputToSegment, int channelIndex)
		{
			ThrowIfDisposed();
			if (inputToSegment != null)
			{
				inputToSegment.ThrowIfDisposed();
			}
			bioinspired_TransientAreasSegmentationModule_run_10(nativeObj, inputToSegment.nativeObj, channelIndex);
		}

		public void run(Mat inputToSegment)
		{
			ThrowIfDisposed();
			if (inputToSegment != null)
			{
				inputToSegment.ThrowIfDisposed();
			}
			bioinspired_TransientAreasSegmentationModule_run_11(nativeObj, inputToSegment.nativeObj);
		}

		public void setup(string segmentationParameterFile, bool applyDefaultSetupOnFailure)
		{
			ThrowIfDisposed();
			bioinspired_TransientAreasSegmentationModule_setup_10(nativeObj, segmentationParameterFile, applyDefaultSetupOnFailure);
		}

		public void setup()
		{
			ThrowIfDisposed();
			bioinspired_TransientAreasSegmentationModule_setup_11(nativeObj);
		}

		public void write(string fs)
		{
			ThrowIfDisposed();
			bioinspired_TransientAreasSegmentationModule_write_10(nativeObj, fs);
		}

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_getSize_10(IntPtr nativeObj, double[] vals);

		[DllImport("opencvforunity")]
		private static extern IntPtr bioinspired_TransientAreasSegmentationModule_printSetup_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_clearAllBuffers_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_getSegmentationPicture_10(IntPtr nativeObj, IntPtr transientAreas_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_run_10(IntPtr nativeObj, IntPtr inputToSegment_nativeObj, int channelIndex);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_run_11(IntPtr nativeObj, IntPtr inputToSegment_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_setup_10(IntPtr nativeObj, string segmentationParameterFile, bool applyDefaultSetupOnFailure);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_setup_11(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_write_10(IntPtr nativeObj, string fs);

		[DllImport("opencvforunity")]
		private static extern void bioinspired_TransientAreasSegmentationModule_delete(IntPtr nativeObj);
	}
}
