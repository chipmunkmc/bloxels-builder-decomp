using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using UnityEngine;

namespace OpenCVForUnity
{
	public static class Utils
	{
		public delegate void DebugLogDelegate(string str);

		private static DebugLogDelegate debugLogFunc = delegate
		{
		};

		public static void copyFromMat(Mat mat, IntPtr intPtr)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			if (intPtr == IntPtr.Zero)
			{
				throw new ArgumentNullException("intPtr == IntPtr.Zero");
			}
			OpenCVForUnity_MatDataToByteArray(mat.nativeObj, intPtr);
		}

		public static void copyToMat(IntPtr intPtr, Mat mat)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (intPtr == IntPtr.Zero)
			{
				throw new ArgumentNullException("intPtr == IntPtr.Zero");
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			OpenCVForUnity_ByteArrayToMatData(intPtr, mat.nativeObj);
		}

		public static void copyFromMat<T>(Mat mat, IList<T> array)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			if (array == null)
			{
				throw new ArgumentNullException("array == null");
			}
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			OpenCVForUnity_MatDataToByteArray(mat.nativeObj, gCHandle.AddrOfPinnedObject());
			gCHandle.Free();
		}

		public static void copyToMat<T>(IList<T> array, Mat mat)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (array == null)
			{
				throw new ArgumentNullException("array == null");
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			OpenCVForUnity_ByteArrayToMatData(gCHandle.AddrOfPinnedObject(), mat.nativeObj);
			gCHandle.Free();
		}

		public static void copyFromTexture(Texture texture, IntPtr intPtr, int bytesPerPixel)
		{
			if (texture == null)
			{
				throw new ArgumentNullException("texture == null");
			}
			if (intPtr == IntPtr.Zero)
			{
				throw new ArgumentNullException("intPtr == IntPtr.Zero");
			}
			OpenCVForUnity_TextureDataToByteArray(texture.GetNativeTexturePtr(), texture.width, texture.height, intPtr, bytesPerPixel);
		}

		public static void copyToTexture(IntPtr intPtr, Texture texture, int bytesPerPixel)
		{
			if (intPtr == IntPtr.Zero)
			{
				throw new ArgumentNullException("intPtr == IntPtr.Zero");
			}
			if (texture == null)
			{
				throw new ArgumentNullException("texture == null");
			}
			OpenCVForUnity_ByteArrayToTextureData(intPtr, texture.GetNativeTexturePtr(), texture.width, texture.height, bytesPerPixel);
		}

		public static void copyFromTexture<T>(Texture texture, IList<T> array, int bytesPerPixel)
		{
			if (texture == null)
			{
				throw new ArgumentNullException("texture == null");
			}
			if (array == null)
			{
				throw new ArgumentNullException("array == null");
			}
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			OpenCVForUnity_TextureDataToByteArray(texture.GetNativeTexturePtr(), texture.width, texture.height, gCHandle.AddrOfPinnedObject(), bytesPerPixel);
			gCHandle.Free();
		}

		public static void copyToTexture<T>(IList<T> array, Texture texture, int bytesPerPixel)
		{
			if (array == null)
			{
				throw new ArgumentNullException("array == null");
			}
			if (texture == null)
			{
				throw new ArgumentNullException("texture == null");
			}
			GCHandle gCHandle = GCHandle.Alloc(array, GCHandleType.Pinned);
			OpenCVForUnity_ByteArrayToTextureData(gCHandle.AddrOfPinnedObject(), texture.GetNativeTexturePtr(), texture.width, texture.height, bytesPerPixel);
			gCHandle.Free();
		}

		public static void matToTexture2D(Mat mat, Texture2D texture2D)
		{
			matToTexture2D(mat, texture2D, null);
		}

		public static void matToTexture2D(Mat mat, Texture2D texture2D, Color32[] bufferColors)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat");
			}
			if (texture2D == null)
			{
				throw new ArgumentNullException("texture2D");
			}
			if (mat.cols() != texture2D.width || mat.rows() != texture2D.height)
			{
				throw new ArgumentException("The output Texture2D object has to be of the same size");
			}
			GCHandle gCHandle;
			if (bufferColors == null)
			{
				Color32[] pixels = texture2D.GetPixels32();
				gCHandle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
				OpenCVForUnity_MatToTexture(mat.nativeObj, gCHandle.AddrOfPinnedObject());
				texture2D.SetPixels32(pixels);
			}
			else
			{
				gCHandle = GCHandle.Alloc(bufferColors, GCHandleType.Pinned);
				OpenCVForUnity_MatToTexture(mat.nativeObj, gCHandle.AddrOfPinnedObject());
				texture2D.SetPixels32(bufferColors);
			}
			texture2D.Apply();
			gCHandle.Free();
		}

		public static void fastMatToTexture2D(Mat mat, Texture2D texture2D)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat");
			}
			if (texture2D == null)
			{
				throw new ArgumentNullException("texture2D");
			}
			if (mat.cols() != texture2D.width || mat.rows() != texture2D.height)
			{
				throw new ArgumentException("The output Texture2D object has to be of the same size");
			}
		}

		public static void texture2DToMat(Texture2D texture2D, Mat mat)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (texture2D == null)
			{
				throw new ArgumentNullException("texture2D == null");
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			if (mat.cols() != texture2D.width || mat.rows() != texture2D.height)
			{
				throw new ArgumentException("The output Mat object has to be of the same size");
			}
			Color32[] pixels = texture2D.GetPixels32();
			GCHandle gCHandle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
			OpenCVForUnity_TextureToMat(gCHandle.AddrOfPinnedObject(), mat.nativeObj);
			gCHandle.Free();
		}

		public static void fastTexture2DToMat(Texture2D texture2D, Mat mat)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (texture2D == null)
			{
				throw new ArgumentNullException("texture2D == null");
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			if (mat.cols() != texture2D.width || mat.rows() != texture2D.height)
			{
				throw new ArgumentException("The output Mat object has to be of the same size");
			}
		}

		public static void webCamTextureToMat(WebCamTexture webCamTexture, Mat mat)
		{
			webCamTextureToMat(webCamTexture, mat, null);
		}

		public static void webCamTextureToMat(WebCamTexture webCamTexture, Mat mat, Color32[] bufferColors)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (webCamTexture == null)
			{
				throw new ArgumentNullException("webCamTexture == null");
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			if (mat.cols() != webCamTexture.width || mat.rows() != webCamTexture.height)
			{
				throw new ArgumentException("The output Mat object has to be of the same size");
			}
			GCHandle gCHandle;
			if (bufferColors == null)
			{
				Color32[] pixels = webCamTexture.GetPixels32();
				gCHandle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
			}
			else
			{
				webCamTexture.GetPixels32(bufferColors);
				gCHandle = GCHandle.Alloc(bufferColors, GCHandleType.Pinned);
			}
			OpenCVForUnity_TextureToMat(gCHandle.AddrOfPinnedObject(), mat.nativeObj);
			gCHandle.Free();
		}

		public static void matToTexture(Mat mat, Texture texture)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat");
			}
			if (texture == null)
			{
				throw new ArgumentNullException("texture2D");
			}
			if (mat.cols() != texture.width || mat.rows() != texture.height)
			{
				throw new ArgumentException("The output Texture object has to be of the same size");
			}
			OpenCVForUnity_LowLevelMatToTexture(mat.nativeObj, texture.GetNativeTexturePtr(), texture.width, texture.height);
		}

		public static void textureToMat(Texture texture, Mat mat)
		{
			if (mat != null)
			{
				mat.ThrowIfDisposed();
			}
			if (texture == null)
			{
				throw new ArgumentNullException("texture2D == null");
			}
			if (mat == null)
			{
				throw new ArgumentNullException("mat == null");
			}
			if (mat.cols() != texture.width || mat.rows() != texture.height)
			{
				throw new ArgumentException("The output Mat object has to be of the same size");
			}
			OpenCVForUnity_LowLevelTextureToMat(texture.GetNativeTexturePtr(), texture.width, texture.height, mat.nativeObj);
		}

		public static int getLowLevelGraphicsDeviceType()
		{
			return OpenCVForUnity_GetLowLevelGraphicsDeviceType();
		}

		public static int getLowLevelTextureFormat(Texture texture)
		{
			if (texture == null)
			{
				throw new ArgumentNullException("texture == null");
			}
			return OpenCVForUnity_GetLowLevelTextureFormat(texture.GetNativeTexturePtr());
		}

		public static bool isNewLowLevelNativePluginInterface()
		{
			return OpenCVForUnity_IsNewLowLevelNativePluginInterface();
		}

		public static string getFilePath(string filename)
		{
			using (AndroidJavaClass androidJavaClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
			{
				using (AndroidJavaObject androidJavaObject = androidJavaClass.GetStatic<AndroidJavaObject>("currentActivity"))
				{
					using (AndroidJavaClass androidJavaClass2 = new AndroidJavaClass("com.enoxsoftware.opencvforunity.OpenCVForUnityPlugin"))
					{
						return androidJavaClass2.CallStatic<string>("copyFileFromAssets", new object[3] { androidJavaObject, filename, "opencvforunity" });
					}
				}
			}
		}

		public static void setDebugMode(bool debugMode)
		{
		}

		internal static int URShift(int number, int bits)
		{
			if (number >= 0)
			{
				return number >> bits;
			}
			return (number >> bits) + (2 << ~bits);
		}

		internal static long URShift(long number, int bits)
		{
			if (number >= 0)
			{
				return number >> bits;
			}
			return (number >> bits) + (2 << ~bits);
		}

		internal static int HashContents<T>(this IEnumerable<T> enumerable)
		{
			int num = 562731820;
			foreach (T item in enumerable)
			{
				int hashCode = item.GetHashCode();
				num = hashCode ^ ((num << 5) + num);
			}
			return num;
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr OpenCVForUnity_GetFilePath(string filename);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_MatToTexture(IntPtr mat, IntPtr textureColors);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_TextureToMat(IntPtr textureColors, IntPtr Mat);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_MatDataToByteArray(IntPtr mat, IntPtr byteArray);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_ByteArrayToMatData(IntPtr byteArray, IntPtr Mat);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_TextureDataToByteArray(IntPtr texPtr, int texWidth, int texHeight, IntPtr byteArray, int bytesPerPixel);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_ByteArrayToTextureData(IntPtr byteArray, IntPtr texPtr, int texWidth, int texHeight, int bytesPerPixel);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_LowLevelMatToTexture(IntPtr mat, IntPtr texPtr, int texWidth, int texHeight);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_LowLevelTextureToMat(IntPtr texPtr, int texWidth, int texHeight, IntPtr mat);

		[DllImport("opencvforunity")]
		private static extern int OpenCVForUnity_GetLowLevelGraphicsDeviceType();

		[DllImport("opencvforunity")]
		private static extern int OpenCVForUnity_GetLowLevelTextureFormat(IntPtr texPtr);

		[DllImport("opencvforunity")]
		private static extern bool OpenCVForUnity_IsNewLowLevelNativePluginInterface();

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_SetDebugMode(bool flag);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_SetDebugLogFunc(DebugLogDelegate func);

		[DllImport("opencvforunity")]
		private static extern void OpenCVForUnity_DebugLogTest();
	}
}
