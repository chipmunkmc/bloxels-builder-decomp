using System;
using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Video
	{
		private const int CV_LKFLOW_INITIAL_GUESSES = 4;

		private const int CV_LKFLOW_GET_MIN_EIGENVALS = 8;

		public const int OPTFLOW_USE_INITIAL_FLOW = 4;

		public const int OPTFLOW_LK_GET_MIN_EIGENVALS = 8;

		public const int OPTFLOW_FARNEBACK_GAUSSIAN = 256;

		public const int MOTION_TRANSLATION = 0;

		public const int MOTION_EUCLIDEAN = 1;

		public const int MOTION_AFFINE = 2;

		public const int MOTION_HOMOGRAPHY = 3;

		private const string LIBNAME = "opencvforunity";

		public static Mat estimateRigidTransform(Mat src, Mat dst, bool fullAffine)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			return new Mat(video_Video_estimateRigidTransform_10(src.nativeObj, dst.nativeObj, fullAffine));
		}

		public static BackgroundSubtractorKNN createBackgroundSubtractorKNN(int history, double dist2Threshold, bool detectShadows)
		{
			return new BackgroundSubtractorKNN(video_Video_createBackgroundSubtractorKNN_10(history, dist2Threshold, detectShadows));
		}

		public static BackgroundSubtractorKNN createBackgroundSubtractorKNN()
		{
			return new BackgroundSubtractorKNN(video_Video_createBackgroundSubtractorKNN_11());
		}

		public static BackgroundSubtractorMOG2 createBackgroundSubtractorMOG2(int history, double varThreshold, bool detectShadows)
		{
			return new BackgroundSubtractorMOG2(video_Video_createBackgroundSubtractorMOG2_10(history, varThreshold, detectShadows));
		}

		public static BackgroundSubtractorMOG2 createBackgroundSubtractorMOG2()
		{
			return new BackgroundSubtractorMOG2(video_Video_createBackgroundSubtractorMOG2_11());
		}

		public static DualTVL1OpticalFlow createOptFlow_DualTVL1()
		{
			return new DualTVL1OpticalFlow(video_Video_createOptFlow_1DualTVL1_10());
		}

		public static RotatedRect CamShift(Mat probImage, Rect window, TermCriteria criteria)
		{
			if (probImage != null)
			{
				probImage.ThrowIfDisposed();
			}
			double[] array = new double[4];
			double[] vals = new double[5];
			video_Video_CamShift_10(probImage.nativeObj, window.x, window.y, window.width, window.height, array, criteria.type, criteria.maxCount, criteria.epsilon, vals);
			RotatedRect result = new RotatedRect(vals);
			if (window != null)
			{
				window.x = (int)array[0];
				window.y = (int)array[1];
				window.width = (int)array[2];
				window.height = (int)array[3];
			}
			return result;
		}

		public static double findTransformECC(Mat templateImage, Mat inputImage, Mat warpMatrix, int motionType, TermCriteria criteria, Mat inputMask)
		{
			if (templateImage != null)
			{
				templateImage.ThrowIfDisposed();
			}
			if (inputImage != null)
			{
				inputImage.ThrowIfDisposed();
			}
			if (warpMatrix != null)
			{
				warpMatrix.ThrowIfDisposed();
			}
			if (inputMask != null)
			{
				inputMask.ThrowIfDisposed();
			}
			return video_Video_findTransformECC_10(templateImage.nativeObj, inputImage.nativeObj, warpMatrix.nativeObj, motionType, criteria.type, criteria.maxCount, criteria.epsilon, inputMask.nativeObj);
		}

		public static double findTransformECC(Mat templateImage, Mat inputImage, Mat warpMatrix, int motionType)
		{
			if (templateImage != null)
			{
				templateImage.ThrowIfDisposed();
			}
			if (inputImage != null)
			{
				inputImage.ThrowIfDisposed();
			}
			if (warpMatrix != null)
			{
				warpMatrix.ThrowIfDisposed();
			}
			return video_Video_findTransformECC_11(templateImage.nativeObj, inputImage.nativeObj, warpMatrix.nativeObj, motionType);
		}

		public static double findTransformECC(Mat templateImage, Mat inputImage, Mat warpMatrix)
		{
			if (templateImage != null)
			{
				templateImage.ThrowIfDisposed();
			}
			if (inputImage != null)
			{
				inputImage.ThrowIfDisposed();
			}
			if (warpMatrix != null)
			{
				warpMatrix.ThrowIfDisposed();
			}
			return video_Video_findTransformECC_12(templateImage.nativeObj, inputImage.nativeObj, warpMatrix.nativeObj);
		}

		public static int buildOpticalFlowPyramid(Mat img, List<Mat> pyramid, Size winSize, int maxLevel, bool withDerivatives, int pyrBorder, int derivBorder, bool tryReuseInputImage)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			int result = video_Video_buildOpticalFlowPyramid_10(img.nativeObj, mat.nativeObj, winSize.width, winSize.height, maxLevel, withDerivatives, pyrBorder, derivBorder, tryReuseInputImage);
			Converters.Mat_to_vector_Mat(mat, pyramid);
			mat.release();
			return result;
		}

		public static int buildOpticalFlowPyramid(Mat img, List<Mat> pyramid, Size winSize, int maxLevel)
		{
			if (img != null)
			{
				img.ThrowIfDisposed();
			}
			Mat mat = new Mat();
			int result = video_Video_buildOpticalFlowPyramid_11(img.nativeObj, mat.nativeObj, winSize.width, winSize.height, maxLevel);
			Converters.Mat_to_vector_Mat(mat, pyramid);
			mat.release();
			return result;
		}

		public static int meanShift(Mat probImage, Rect window, TermCriteria criteria)
		{
			if (probImage != null)
			{
				probImage.ThrowIfDisposed();
			}
			double[] array = new double[4];
			int result = video_Video_meanShift_10(probImage.nativeObj, window.x, window.y, window.width, window.height, array, criteria.type, criteria.maxCount, criteria.epsilon);
			if (window != null)
			{
				window.x = (int)array[0];
				window.y = (int)array[1];
				window.width = (int)array[2];
				window.height = (int)array[3];
			}
			return result;
		}

		public static void calcOpticalFlowFarneback(Mat prev, Mat next, Mat flow, double pyr_scale, int levels, int winsize, int iterations, int poly_n, double poly_sigma, int flags)
		{
			if (prev != null)
			{
				prev.ThrowIfDisposed();
			}
			if (next != null)
			{
				next.ThrowIfDisposed();
			}
			if (flow != null)
			{
				flow.ThrowIfDisposed();
			}
			video_Video_calcOpticalFlowFarneback_10(prev.nativeObj, next.nativeObj, flow.nativeObj, pyr_scale, levels, winsize, iterations, poly_n, poly_sigma, flags);
		}

		public static void calcOpticalFlowPyrLK(Mat prevImg, Mat nextImg, MatOfPoint2f prevPts, MatOfPoint2f nextPts, MatOfByte status, MatOfFloat err, Size winSize, int maxLevel, TermCriteria criteria, int flags, double minEigThreshold)
		{
			if (prevImg != null)
			{
				prevImg.ThrowIfDisposed();
			}
			if (nextImg != null)
			{
				nextImg.ThrowIfDisposed();
			}
			if (prevPts != null)
			{
				prevPts.ThrowIfDisposed();
			}
			if (nextPts != null)
			{
				nextPts.ThrowIfDisposed();
			}
			if (status != null)
			{
				status.ThrowIfDisposed();
			}
			if (err != null)
			{
				err.ThrowIfDisposed();
			}
			video_Video_calcOpticalFlowPyrLK_10(prevImg.nativeObj, nextImg.nativeObj, prevPts.nativeObj, nextPts.nativeObj, status.nativeObj, err.nativeObj, winSize.width, winSize.height, maxLevel, criteria.type, criteria.maxCount, criteria.epsilon, flags, minEigThreshold);
		}

		public static void calcOpticalFlowPyrLK(Mat prevImg, Mat nextImg, MatOfPoint2f prevPts, MatOfPoint2f nextPts, MatOfByte status, MatOfFloat err, Size winSize, int maxLevel)
		{
			if (prevImg != null)
			{
				prevImg.ThrowIfDisposed();
			}
			if (nextImg != null)
			{
				nextImg.ThrowIfDisposed();
			}
			if (prevPts != null)
			{
				prevPts.ThrowIfDisposed();
			}
			if (nextPts != null)
			{
				nextPts.ThrowIfDisposed();
			}
			if (status != null)
			{
				status.ThrowIfDisposed();
			}
			if (err != null)
			{
				err.ThrowIfDisposed();
			}
			video_Video_calcOpticalFlowPyrLK_11(prevImg.nativeObj, nextImg.nativeObj, prevPts.nativeObj, nextPts.nativeObj, status.nativeObj, err.nativeObj, winSize.width, winSize.height, maxLevel);
		}

		public static void calcOpticalFlowPyrLK(Mat prevImg, Mat nextImg, MatOfPoint2f prevPts, MatOfPoint2f nextPts, MatOfByte status, MatOfFloat err)
		{
			if (prevImg != null)
			{
				prevImg.ThrowIfDisposed();
			}
			if (nextImg != null)
			{
				nextImg.ThrowIfDisposed();
			}
			if (prevPts != null)
			{
				prevPts.ThrowIfDisposed();
			}
			if (nextPts != null)
			{
				nextPts.ThrowIfDisposed();
			}
			if (status != null)
			{
				status.ThrowIfDisposed();
			}
			if (err != null)
			{
				err.ThrowIfDisposed();
			}
			video_Video_calcOpticalFlowPyrLK_12(prevImg.nativeObj, nextImg.nativeObj, prevPts.nativeObj, nextPts.nativeObj, status.nativeObj, err.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr video_Video_estimateRigidTransform_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, bool fullAffine);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_Video_createBackgroundSubtractorKNN_10(int history, double dist2Threshold, bool detectShadows);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_Video_createBackgroundSubtractorKNN_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr video_Video_createBackgroundSubtractorMOG2_10(int history, double varThreshold, bool detectShadows);

		[DllImport("opencvforunity")]
		private static extern IntPtr video_Video_createBackgroundSubtractorMOG2_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr video_Video_createOptFlow_1DualTVL1_10();

		[DllImport("opencvforunity")]
		private static extern void video_Video_CamShift_10(IntPtr probImage_nativeObj, int window_x, int window_y, int window_width, int window_height, double[] window_out, int criteria_type, int criteria_maxCount, double criteria_epsilon, double[] vals);

		[DllImport("opencvforunity")]
		private static extern double video_Video_findTransformECC_10(IntPtr templateImage_nativeObj, IntPtr inputImage_nativeObj, IntPtr warpMatrix_nativeObj, int motionType, int criteria_type, int criteria_maxCount, double criteria_epsilon, IntPtr inputMask_nativeObj);

		[DllImport("opencvforunity")]
		private static extern double video_Video_findTransformECC_11(IntPtr templateImage_nativeObj, IntPtr inputImage_nativeObj, IntPtr warpMatrix_nativeObj, int motionType);

		[DllImport("opencvforunity")]
		private static extern double video_Video_findTransformECC_12(IntPtr templateImage_nativeObj, IntPtr inputImage_nativeObj, IntPtr warpMatrix_nativeObj);

		[DllImport("opencvforunity")]
		private static extern int video_Video_buildOpticalFlowPyramid_10(IntPtr img_nativeObj, IntPtr pyramid_mat_nativeObj, double winSize_width, double winSize_height, int maxLevel, bool withDerivatives, int pyrBorder, int derivBorder, bool tryReuseInputImage);

		[DllImport("opencvforunity")]
		private static extern int video_Video_buildOpticalFlowPyramid_11(IntPtr img_nativeObj, IntPtr pyramid_mat_nativeObj, double winSize_width, double winSize_height, int maxLevel);

		[DllImport("opencvforunity")]
		private static extern int video_Video_meanShift_10(IntPtr probImage_nativeObj, int window_x, int window_y, int window_width, int window_height, double[] window_out, int criteria_type, int criteria_maxCount, double criteria_epsilon);

		[DllImport("opencvforunity")]
		private static extern void video_Video_calcOpticalFlowFarneback_10(IntPtr prev_nativeObj, IntPtr next_nativeObj, IntPtr flow_nativeObj, double pyr_scale, int levels, int winsize, int iterations, int poly_n, double poly_sigma, int flags);

		[DllImport("opencvforunity")]
		private static extern void video_Video_calcOpticalFlowPyrLK_10(IntPtr prevImg_nativeObj, IntPtr nextImg_nativeObj, IntPtr prevPts_mat_nativeObj, IntPtr nextPts_mat_nativeObj, IntPtr status_mat_nativeObj, IntPtr err_mat_nativeObj, double winSize_width, double winSize_height, int maxLevel, int criteria_type, int criteria_maxCount, double criteria_epsilon, int flags, double minEigThreshold);

		[DllImport("opencvforunity")]
		private static extern void video_Video_calcOpticalFlowPyrLK_11(IntPtr prevImg_nativeObj, IntPtr nextImg_nativeObj, IntPtr prevPts_mat_nativeObj, IntPtr nextPts_mat_nativeObj, IntPtr status_mat_nativeObj, IntPtr err_mat_nativeObj, double winSize_width, double winSize_height, int maxLevel);

		[DllImport("opencvforunity")]
		private static extern void video_Video_calcOpticalFlowPyrLK_12(IntPtr prevImg_nativeObj, IntPtr nextImg_nativeObj, IntPtr prevPts_mat_nativeObj, IntPtr nextPts_mat_nativeObj, IntPtr status_mat_nativeObj, IntPtr err_mat_nativeObj);
	}
}
