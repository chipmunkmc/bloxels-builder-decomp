using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class VideoCapture : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		protected VideoCapture(IntPtr addr)
			: base(addr)
		{
		}

		public VideoCapture(string filename, int apiPreference)
		{
			nativeObj = videoio_VideoCapture_VideoCapture_10(filename, apiPreference);
		}

		public VideoCapture(string filename)
		{
			nativeObj = videoio_VideoCapture_VideoCapture_11(filename);
		}

		public VideoCapture(int index)
		{
			nativeObj = videoio_VideoCapture_VideoCapture_12(index);
		}

		public VideoCapture()
		{
			nativeObj = videoio_VideoCapture_VideoCapture_13();
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						videoio_VideoCapture_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool grab()
		{
			ThrowIfDisposed();
			return videoio_VideoCapture_grab_10(nativeObj);
		}

		public bool isOpened()
		{
			ThrowIfDisposed();
			return videoio_VideoCapture_isOpened_10(nativeObj);
		}

		public bool open(string filename, int apiPreference)
		{
			ThrowIfDisposed();
			return videoio_VideoCapture_open_10(nativeObj, filename, apiPreference);
		}

		public bool open(string filename)
		{
			ThrowIfDisposed();
			return videoio_VideoCapture_open_11(nativeObj, filename);
		}

		public bool open(int index)
		{
			ThrowIfDisposed();
			return videoio_VideoCapture_open_12(nativeObj, index);
		}

		public bool read(Mat image)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			return videoio_VideoCapture_read_10(nativeObj, image.nativeObj);
		}

		public bool retrieve(Mat image, int flag)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			return videoio_VideoCapture_retrieve_10(nativeObj, image.nativeObj, flag);
		}

		public bool retrieve(Mat image)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			return videoio_VideoCapture_retrieve_11(nativeObj, image.nativeObj);
		}

		public bool set(int propId, double value)
		{
			ThrowIfDisposed();
			return videoio_VideoCapture_set_10(nativeObj, propId, value);
		}

		public double get(int propId)
		{
			ThrowIfDisposed();
			return videoio_VideoCapture_get_10(nativeObj, propId);
		}

		public void release()
		{
			ThrowIfDisposed();
			videoio_VideoCapture_release_10(nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr videoio_VideoCapture_VideoCapture_10(string filename, int apiPreference);

		[DllImport("opencvforunity")]
		private static extern IntPtr videoio_VideoCapture_VideoCapture_11(string filename);

		[DllImport("opencvforunity")]
		private static extern IntPtr videoio_VideoCapture_VideoCapture_12(int index);

		[DllImport("opencvforunity")]
		private static extern IntPtr videoio_VideoCapture_VideoCapture_13();

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_grab_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_isOpened_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_open_10(IntPtr nativeObj, string filename, int apiPreference);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_open_11(IntPtr nativeObj, string filename);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_open_12(IntPtr nativeObj, int index);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_read_10(IntPtr nativeObj, IntPtr image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_retrieve_10(IntPtr nativeObj, IntPtr image_nativeObj, int flag);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_retrieve_11(IntPtr nativeObj, IntPtr image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoCapture_set_10(IntPtr nativeObj, int propId, double value);

		[DllImport("opencvforunity")]
		private static extern double videoio_VideoCapture_get_10(IntPtr nativeObj, int propId);

		[DllImport("opencvforunity")]
		private static extern void videoio_VideoCapture_release_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void videoio_VideoCapture_delete(IntPtr nativeObj);
	}
}
