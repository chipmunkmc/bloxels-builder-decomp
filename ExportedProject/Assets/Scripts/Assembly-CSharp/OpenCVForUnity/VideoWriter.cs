using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class VideoWriter : DisposableOpenCVObject
	{
		private const string LIBNAME = "opencvforunity";

		protected VideoWriter(IntPtr addr)
			: base(addr)
		{
		}

		public VideoWriter(string filename, int fourcc, double fps, Size frameSize, bool isColor)
		{
			nativeObj = videoio_VideoWriter_VideoWriter_10(filename, fourcc, fps, frameSize.width, frameSize.height, isColor);
		}

		public VideoWriter(string filename, int fourcc, double fps, Size frameSize)
		{
			nativeObj = videoio_VideoWriter_VideoWriter_11(filename, fourcc, fps, frameSize.width, frameSize.height);
		}

		public VideoWriter()
		{
			nativeObj = videoio_VideoWriter_VideoWriter_12();
		}

		protected override void Dispose(bool disposing)
		{
			try
			{
				if (disposing)
				{
				}
				if (base.IsEnabledDispose)
				{
					if (nativeObj != IntPtr.Zero)
					{
						videoio_VideoWriter_delete(nativeObj);
					}
					nativeObj = IntPtr.Zero;
				}
			}
			finally
			{
				base.Dispose(disposing);
			}
		}

		public bool isOpened()
		{
			ThrowIfDisposed();
			return videoio_VideoWriter_isOpened_10(nativeObj);
		}

		public bool open(string filename, int fourcc, double fps, Size frameSize, bool isColor)
		{
			ThrowIfDisposed();
			return videoio_VideoWriter_open_10(nativeObj, filename, fourcc, fps, frameSize.width, frameSize.height, isColor);
		}

		public bool open(string filename, int fourcc, double fps, Size frameSize)
		{
			ThrowIfDisposed();
			return videoio_VideoWriter_open_11(nativeObj, filename, fourcc, fps, frameSize.width, frameSize.height);
		}

		public bool set(int propId, double value)
		{
			ThrowIfDisposed();
			return videoio_VideoWriter_set_10(nativeObj, propId, value);
		}

		public double get(int propId)
		{
			ThrowIfDisposed();
			return videoio_VideoWriter_get_10(nativeObj, propId);
		}

		public static int fourcc(char c1, char c2, char c3, char c4)
		{
			return videoio_VideoWriter_fourcc_10(c1, c2, c3, c4);
		}

		public void release()
		{
			ThrowIfDisposed();
			videoio_VideoWriter_release_10(nativeObj);
		}

		public void write(Mat image)
		{
			ThrowIfDisposed();
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			videoio_VideoWriter_write_10(nativeObj, image.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr videoio_VideoWriter_VideoWriter_10(string filename, int fourcc, double fps, double frameSize_width, double frameSize_height, bool isColor);

		[DllImport("opencvforunity")]
		private static extern IntPtr videoio_VideoWriter_VideoWriter_11(string filename, int fourcc, double fps, double frameSize_width, double frameSize_height);

		[DllImport("opencvforunity")]
		private static extern IntPtr videoio_VideoWriter_VideoWriter_12();

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoWriter_isOpened_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoWriter_open_10(IntPtr nativeObj, string filename, int fourcc, double fps, double frameSize_width, double frameSize_height, bool isColor);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoWriter_open_11(IntPtr nativeObj, string filename, int fourcc, double fps, double frameSize_width, double frameSize_height);

		[DllImport("opencvforunity")]
		private static extern bool videoio_VideoWriter_set_10(IntPtr nativeObj, int propId, double value);

		[DllImport("opencvforunity")]
		private static extern double videoio_VideoWriter_get_10(IntPtr nativeObj, int propId);

		[DllImport("opencvforunity")]
		private static extern int videoio_VideoWriter_fourcc_10(char c1, char c2, char c3, char c4);

		[DllImport("opencvforunity")]
		private static extern void videoio_VideoWriter_release_10(IntPtr nativeObj);

		[DllImport("opencvforunity")]
		private static extern void videoio_VideoWriter_write_10(IntPtr nativeObj, IntPtr image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void videoio_VideoWriter_delete(IntPtr nativeObj);
	}
}
