using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Ximgproc
	{
		public const int DTF_NC = 0;

		public const int DTF_IC = 1;

		public const int DTF_RF = 2;

		public const int GUIDED_FILTER = 3;

		public const int AM_FILTER = 4;

		public const int ARO_0_45 = 0;

		public const int ARO_45_90 = 1;

		public const int ARO_90_135 = 2;

		public const int ARO_315_0 = 3;

		public const int ARO_315_45 = 4;

		public const int ARO_45_135 = 5;

		public const int ARO_315_135 = 6;

		public const int ARO_CTR_HOR = 7;

		public const int ARO_CTR_VER = 8;

		public const int FHT_MIN = 0;

		public const int FHT_MAX = 1;

		public const int FHT_ADD = 2;

		public const int FHT_AVE = 3;

		public const int HDO_RAW = 0;

		public const int HDO_DESKEW = 1;

		public const int SLIC = 100;

		public const int SLICO = 101;

		private const string LIBNAME = "opencvforunity";

		public static AdaptiveManifoldFilter createAMFilter(double sigma_s, double sigma_r, bool adjust_outliers)
		{
			return new AdaptiveManifoldFilter(ximgproc_Ximgproc_createAMFilter_10(sigma_s, sigma_r, adjust_outliers));
		}

		public static AdaptiveManifoldFilter createAMFilter(double sigma_s, double sigma_r)
		{
			return new AdaptiveManifoldFilter(ximgproc_Ximgproc_createAMFilter_11(sigma_s, sigma_r));
		}

		public static DTFilter createDTFilter(Mat guide, double sigmaSpatial, double sigmaColor, int mode, int numIters)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			return new DTFilter(ximgproc_Ximgproc_createDTFilter_10(guide.nativeObj, sigmaSpatial, sigmaColor, mode, numIters));
		}

		public static DTFilter createDTFilter(Mat guide, double sigmaSpatial, double sigmaColor)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			return new DTFilter(ximgproc_Ximgproc_createDTFilter_11(guide.nativeObj, sigmaSpatial, sigmaColor));
		}

		public static DisparityWLSFilter createDisparityWLSFilterGeneric(bool use_confidence)
		{
			return new DisparityWLSFilter(ximgproc_Ximgproc_createDisparityWLSFilterGeneric_10(use_confidence));
		}

		public static EdgeAwareInterpolator createEdgeAwareInterpolator()
		{
			return new EdgeAwareInterpolator(ximgproc_Ximgproc_createEdgeAwareInterpolator_10());
		}

		public static FastGlobalSmootherFilter createFastGlobalSmootherFilter(Mat guide, double lambda, double sigma_color, double lambda_attenuation, int num_iter)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			return new FastGlobalSmootherFilter(ximgproc_Ximgproc_createFastGlobalSmootherFilter_10(guide.nativeObj, lambda, sigma_color, lambda_attenuation, num_iter));
		}

		public static FastGlobalSmootherFilter createFastGlobalSmootherFilter(Mat guide, double lambda, double sigma_color)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			return new FastGlobalSmootherFilter(ximgproc_Ximgproc_createFastGlobalSmootherFilter_11(guide.nativeObj, lambda, sigma_color));
		}

		public static GraphSegmentation createGraphSegmentation(double sigma, float k, int min_size)
		{
			return new GraphSegmentation(ximgproc_Ximgproc_createGraphSegmentation_10(sigma, k, min_size));
		}

		public static GraphSegmentation createGraphSegmentation()
		{
			return new GraphSegmentation(ximgproc_Ximgproc_createGraphSegmentation_11());
		}

		public static GuidedFilter createGuidedFilter(Mat guide, int radius, double eps)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			return new GuidedFilter(ximgproc_Ximgproc_createGuidedFilter_10(guide.nativeObj, radius, eps));
		}

		public static RFFeatureGetter createRFFeatureGetter()
		{
			return new RFFeatureGetter(ximgproc_Ximgproc_createRFFeatureGetter_10());
		}

		public static StructuredEdgeDetection createStructuredEdgeDetection(string model, RFFeatureGetter howToGetFeatures)
		{
			if (howToGetFeatures != null)
			{
				howToGetFeatures.ThrowIfDisposed();
			}
			return new StructuredEdgeDetection(ximgproc_Ximgproc_createStructuredEdgeDetection_10(model, howToGetFeatures.nativeObj));
		}

		public static StructuredEdgeDetection createStructuredEdgeDetection(string model)
		{
			return new StructuredEdgeDetection(ximgproc_Ximgproc_createStructuredEdgeDetection_11(model));
		}

		public static SuperpixelLSC createSuperpixelLSC(Mat image, int region_size, float ratio)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			return new SuperpixelLSC(ximgproc_Ximgproc_createSuperpixelLSC_10(image.nativeObj, region_size, ratio));
		}

		public static SuperpixelLSC createSuperpixelLSC(Mat image)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			return new SuperpixelLSC(ximgproc_Ximgproc_createSuperpixelLSC_11(image.nativeObj));
		}

		public static SuperpixelSEEDS createSuperpixelSEEDS(int image_width, int image_height, int image_channels, int num_superpixels, int num_levels, int prior, int histogram_bins, bool double_step)
		{
			return new SuperpixelSEEDS(ximgproc_Ximgproc_createSuperpixelSEEDS_10(image_width, image_height, image_channels, num_superpixels, num_levels, prior, histogram_bins, double_step));
		}

		public static SuperpixelSEEDS createSuperpixelSEEDS(int image_width, int image_height, int image_channels, int num_superpixels, int num_levels)
		{
			return new SuperpixelSEEDS(ximgproc_Ximgproc_createSuperpixelSEEDS_11(image_width, image_height, image_channels, num_superpixels, num_levels));
		}

		public static SuperpixelSLIC createSuperpixelSLIC(Mat image, int algorithm, int region_size, float ruler)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			return new SuperpixelSLIC(ximgproc_Ximgproc_createSuperpixelSLIC_10(image.nativeObj, algorithm, region_size, ruler));
		}

		public static SuperpixelSLIC createSuperpixelSLIC(Mat image)
		{
			if (image != null)
			{
				image.ThrowIfDisposed();
			}
			return new SuperpixelSLIC(ximgproc_Ximgproc_createSuperpixelSLIC_11(image.nativeObj));
		}

		public static void amFilter(Mat joint, Mat src, Mat dst, double sigma_s, double sigma_r, bool adjust_outliers)
		{
			if (joint != null)
			{
				joint.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_amFilter_10(joint.nativeObj, src.nativeObj, dst.nativeObj, sigma_s, sigma_r, adjust_outliers);
		}

		public static void amFilter(Mat joint, Mat src, Mat dst, double sigma_s, double sigma_r)
		{
			if (joint != null)
			{
				joint.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_amFilter_11(joint.nativeObj, src.nativeObj, dst.nativeObj, sigma_s, sigma_r);
		}

		public static void covarianceEstimation(Mat src, Mat dst, int windowRows, int windowCols)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_covarianceEstimation_10(src.nativeObj, dst.nativeObj, windowRows, windowCols);
		}

		public static void dtFilter(Mat guide, Mat src, Mat dst, double sigmaSpatial, double sigmaColor, int mode, int numIters)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_dtFilter_10(guide.nativeObj, src.nativeObj, dst.nativeObj, sigmaSpatial, sigmaColor, mode, numIters);
		}

		public static void dtFilter(Mat guide, Mat src, Mat dst, double sigmaSpatial, double sigmaColor)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_dtFilter_11(guide.nativeObj, src.nativeObj, dst.nativeObj, sigmaSpatial, sigmaColor);
		}

		public static void fastGlobalSmootherFilter(Mat guide, Mat src, Mat dst, double lambda, double sigma_color, double lambda_attenuation, int num_iter)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_fastGlobalSmootherFilter_10(guide.nativeObj, src.nativeObj, dst.nativeObj, lambda, sigma_color, lambda_attenuation, num_iter);
		}

		public static void fastGlobalSmootherFilter(Mat guide, Mat src, Mat dst, double lambda, double sigma_color)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_fastGlobalSmootherFilter_11(guide.nativeObj, src.nativeObj, dst.nativeObj, lambda, sigma_color);
		}

		public static void guidedFilter(Mat guide, Mat src, Mat dst, int radius, double eps, int dDepth)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_guidedFilter_10(guide.nativeObj, src.nativeObj, dst.nativeObj, radius, eps, dDepth);
		}

		public static void guidedFilter(Mat guide, Mat src, Mat dst, int radius, double eps)
		{
			if (guide != null)
			{
				guide.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_guidedFilter_11(guide.nativeObj, src.nativeObj, dst.nativeObj, radius, eps);
		}

		public static void jointBilateralFilter(Mat joint, Mat src, Mat dst, int d, double sigmaColor, double sigmaSpace, int borderType)
		{
			if (joint != null)
			{
				joint.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_jointBilateralFilter_10(joint.nativeObj, src.nativeObj, dst.nativeObj, d, sigmaColor, sigmaSpace, borderType);
		}

		public static void jointBilateralFilter(Mat joint, Mat src, Mat dst, int d, double sigmaColor, double sigmaSpace)
		{
			if (joint != null)
			{
				joint.ThrowIfDisposed();
			}
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_jointBilateralFilter_11(joint.nativeObj, src.nativeObj, dst.nativeObj, d, sigmaColor, sigmaSpace);
		}

		public static void l0Smooth(Mat src, Mat dst, double lambda, double kappa)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_l0Smooth_10(src.nativeObj, dst.nativeObj, lambda, kappa);
		}

		public static void l0Smooth(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_l0Smooth_11(src.nativeObj, dst.nativeObj);
		}

		public static void niBlackThreshold(Mat _src, Mat _dst, double maxValue, int type, int blockSize, double delta)
		{
			if (_src != null)
			{
				_src.ThrowIfDisposed();
			}
			if (_dst != null)
			{
				_dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_niBlackThreshold_10(_src.nativeObj, _dst.nativeObj, maxValue, type, blockSize, delta);
		}

		public static void rollingGuidanceFilter(Mat src, Mat dst, int d, double sigmaColor, double sigmaSpace, int numOfIter, int borderType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_rollingGuidanceFilter_10(src.nativeObj, dst.nativeObj, d, sigmaColor, sigmaSpace, numOfIter, borderType);
		}

		public static void rollingGuidanceFilter(Mat src, Mat dst, int d, double sigmaColor, double sigmaSpace, int numOfIter)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_rollingGuidanceFilter_11(src.nativeObj, dst.nativeObj, d, sigmaColor, sigmaSpace, numOfIter);
		}

		public static void rollingGuidanceFilter(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			ximgproc_Ximgproc_rollingGuidanceFilter_12(src.nativeObj, dst.nativeObj);
		}

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createAMFilter_10(double sigma_s, double sigma_r, bool adjust_outliers);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createAMFilter_11(double sigma_s, double sigma_r);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createDTFilter_10(IntPtr guide_nativeObj, double sigmaSpatial, double sigmaColor, int mode, int numIters);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createDTFilter_11(IntPtr guide_nativeObj, double sigmaSpatial, double sigmaColor);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createDisparityWLSFilterGeneric_10(bool use_confidence);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createEdgeAwareInterpolator_10();

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createFastGlobalSmootherFilter_10(IntPtr guide_nativeObj, double lambda, double sigma_color, double lambda_attenuation, int num_iter);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createFastGlobalSmootherFilter_11(IntPtr guide_nativeObj, double lambda, double sigma_color);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createGraphSegmentation_10(double sigma, float k, int min_size);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createGraphSegmentation_11();

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createGuidedFilter_10(IntPtr guide_nativeObj, int radius, double eps);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createRFFeatureGetter_10();

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createStructuredEdgeDetection_10(string model, IntPtr howToGetFeatures_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createStructuredEdgeDetection_11(string model);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createSuperpixelLSC_10(IntPtr image_nativeObj, int region_size, float ratio);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createSuperpixelLSC_11(IntPtr image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createSuperpixelSEEDS_10(int image_width, int image_height, int image_channels, int num_superpixels, int num_levels, int prior, int histogram_bins, bool double_step);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createSuperpixelSEEDS_11(int image_width, int image_height, int image_channels, int num_superpixels, int num_levels);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createSuperpixelSLIC_10(IntPtr image_nativeObj, int algorithm, int region_size, float ruler);

		[DllImport("opencvforunity")]
		private static extern IntPtr ximgproc_Ximgproc_createSuperpixelSLIC_11(IntPtr image_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_amFilter_10(IntPtr joint_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, double sigma_s, double sigma_r, bool adjust_outliers);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_amFilter_11(IntPtr joint_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, double sigma_s, double sigma_r);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_covarianceEstimation_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int windowRows, int windowCols);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_dtFilter_10(IntPtr guide_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, double sigmaSpatial, double sigmaColor, int mode, int numIters);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_dtFilter_11(IntPtr guide_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, double sigmaSpatial, double sigmaColor);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_fastGlobalSmootherFilter_10(IntPtr guide_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, double lambda, double sigma_color, double lambda_attenuation, int num_iter);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_fastGlobalSmootherFilter_11(IntPtr guide_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, double lambda, double sigma_color);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_guidedFilter_10(IntPtr guide_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, int radius, double eps, int dDepth);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_guidedFilter_11(IntPtr guide_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, int radius, double eps);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_jointBilateralFilter_10(IntPtr joint_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, int d, double sigmaColor, double sigmaSpace, int borderType);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_jointBilateralFilter_11(IntPtr joint_nativeObj, IntPtr src_nativeObj, IntPtr dst_nativeObj, int d, double sigmaColor, double sigmaSpace);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_l0Smooth_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double lambda, double kappa);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_l0Smooth_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_niBlackThreshold_10(IntPtr _src_nativeObj, IntPtr _dst_nativeObj, double maxValue, int type, int blockSize, double delta);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_rollingGuidanceFilter_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int d, double sigmaColor, double sigmaSpace, int numOfIter, int borderType);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_rollingGuidanceFilter_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int d, double sigmaColor, double sigmaSpace, int numOfIter);

		[DllImport("opencvforunity")]
		private static extern void ximgproc_Ximgproc_rollingGuidanceFilter_12(IntPtr src_nativeObj, IntPtr dst_nativeObj);
	}
}
