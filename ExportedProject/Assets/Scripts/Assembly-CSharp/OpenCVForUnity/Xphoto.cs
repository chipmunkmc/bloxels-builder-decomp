using System;
using System.Runtime.InteropServices;

namespace OpenCVForUnity
{
	public class Xphoto
	{
		public const int INPAINT_SHIFTMAP = 0;

		public const int WHITE_BALANCE_SIMPLE = 0;

		public const int WHITE_BALANCE_GRAYWORLD = 1;

		private const string LIBNAME = "opencvforunity";

		public static void autowbGrayworld(Mat src, Mat dst, float thresh)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			xphoto_Xphoto_autowbGrayworld_10(src.nativeObj, dst.nativeObj, thresh);
		}

		public static void autowbGrayworld(Mat src, Mat dst)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			xphoto_Xphoto_autowbGrayworld_11(src.nativeObj, dst.nativeObj);
		}

		public static void balanceWhite(Mat src, Mat dst, int algorithmType, float inputMin, float inputMax, float outputMin, float outputMax)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			xphoto_Xphoto_balanceWhite_10(src.nativeObj, dst.nativeObj, algorithmType, inputMin, inputMax, outputMin, outputMax);
		}

		public static void balanceWhite(Mat src, Mat dst, int algorithmType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			xphoto_Xphoto_balanceWhite_11(src.nativeObj, dst.nativeObj, algorithmType);
		}

		public static void dctDenoising(Mat src, Mat dst, double sigma, int psize)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			xphoto_Xphoto_dctDenoising_10(src.nativeObj, dst.nativeObj, sigma, psize);
		}

		public static void dctDenoising(Mat src, Mat dst, double sigma)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			xphoto_Xphoto_dctDenoising_11(src.nativeObj, dst.nativeObj, sigma);
		}

		public static void inpaint(Mat src, Mat mask, Mat dst, int algorithmType)
		{
			if (src != null)
			{
				src.ThrowIfDisposed();
			}
			if (mask != null)
			{
				mask.ThrowIfDisposed();
			}
			if (dst != null)
			{
				dst.ThrowIfDisposed();
			}
			xphoto_Xphoto_inpaint_10(src.nativeObj, mask.nativeObj, dst.nativeObj, algorithmType);
		}

		[DllImport("opencvforunity")]
		private static extern void xphoto_Xphoto_autowbGrayworld_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, float thresh);

		[DllImport("opencvforunity")]
		private static extern void xphoto_Xphoto_autowbGrayworld_11(IntPtr src_nativeObj, IntPtr dst_nativeObj);

		[DllImport("opencvforunity")]
		private static extern void xphoto_Xphoto_balanceWhite_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, int algorithmType, float inputMin, float inputMax, float outputMin, float outputMax);

		[DllImport("opencvforunity")]
		private static extern void xphoto_Xphoto_balanceWhite_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, int algorithmType);

		[DllImport("opencvforunity")]
		private static extern void xphoto_Xphoto_dctDenoising_10(IntPtr src_nativeObj, IntPtr dst_nativeObj, double sigma, int psize);

		[DllImport("opencvforunity")]
		private static extern void xphoto_Xphoto_dctDenoising_11(IntPtr src_nativeObj, IntPtr dst_nativeObj, double sigma);

		[DllImport("opencvforunity")]
		private static extern void xphoto_Xphoto_inpaint_10(IntPtr src_nativeObj, IntPtr mask_nativeObj, IntPtr dst_nativeObj, int algorithmType);
	}
}
