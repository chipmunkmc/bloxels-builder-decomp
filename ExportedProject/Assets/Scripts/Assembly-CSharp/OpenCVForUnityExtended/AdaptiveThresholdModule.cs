using System;
using OpenCVForUnity;
using UnityEngine;

namespace OpenCVForUnityExtended
{
	[Serializable]
	public class AdaptiveThresholdModule
	{
		public enum AdaptiveMethod
		{
			Mean = 0,
			Gaussian = 1
		}

		public enum ThresholdType
		{
			Binary = 0,
			BinaryInverted = 1
		}

		[Range(0f, 255f)]
		public int maxValue;

		public AdaptiveMethod method;

		public ThresholdType thresholdType;

		[HideInInspector]
		[SerializeField]
		private int _prevBlockSize;

		[Range(3f, 15f)]
		public int blockSize;

		[Range(-10f, 10f)]
		public float C;

		public AdaptiveThresholdModule()
		{
			maxValue = 255;
			method = AdaptiveMethod.Mean;
			thresholdType = ThresholdType.Binary;
			blockSize = 3;
		}

		public void Process(Mat src, Mat dst)
		{
			VerifyBlockSize();
			Imgproc.adaptiveThreshold(src, dst, maxValue, (int)method, (int)thresholdType, blockSize, C);
		}

		public void VerifyBlockSize()
		{
			if (blockSize == _prevBlockSize)
			{
				return;
			}
			if (blockSize < 3)
			{
				blockSize = 3;
				return;
			}
			if (blockSize % 2 == 0)
			{
				blockSize++;
			}
			_prevBlockSize = blockSize;
		}
	}
}
