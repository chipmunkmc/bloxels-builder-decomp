using System;
using OpenCVForUnity;
using UnityEngine;

namespace OpenCVForUnityExtended
{
	[Serializable]
	public class BilateralFilterModule
	{
		public enum BorderType
		{
			Constant = 0,
			Replicate = 1,
			Reflect = 2,
			Wrap = 3,
			Reflect_101 = 4,
			Transparent = 5,
			Relfect101 = 4,
			Default = 4,
			Isolated = 16
		}

		[Range(1f, 15f)]
		public int d;

		[Range(0f, 255f)]
		public float sigmaColor;

		[Range(0f, 255f)]
		public float sigmaSpace;

		public BorderType borderType;

		public BilateralFilterModule()
		{
			d = 15;
			sigmaColor = 80f;
			sigmaSpace = 80f;
			borderType = BorderType.Reflect_101;
		}

		public void Process(Mat src, Mat dst)
		{
			Imgproc.bilateralFilter(src, dst, d, sigmaColor, sigmaSpace, (int)borderType);
		}
	}
}
