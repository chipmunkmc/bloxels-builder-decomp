using System;
using OpenCVForUnity;
using UnityEngine;

namespace OpenCVForUnityExtended
{
	[Serializable]
	public class CLAHEModule
	{
		private int _previousTileSizeX;

		private int _previousTileSizeY;

		[Range(1f, 50f)]
		public int tileGridSizeX;

		[Range(1f, 50f)]
		public int tileGridSizeY;

		private float _previousClipLimit;

		[Range(0f, 255f)]
		public float clipLimit;

		private CLAHE _claheProcessor;

		public CLAHEModule()
		{
			clipLimit = 40f;
			tileGridSizeX = 8;
			tileGridSizeY = 8;
			_previousClipLimit = clipLimit;
			_previousTileSizeX = tileGridSizeX;
			_previousTileSizeY = tileGridSizeY;
			_claheProcessor = Imgproc.createCLAHE(clipLimit, new Size(tileGridSizeX, tileGridSizeY));
		}

		public void Process(Mat src, Mat dst)
		{
			VerifyParameters();
			_claheProcessor.apply(src, dst);
		}

		private void VerifyParameters()
		{
			VerifyTileSize();
			VerifyClipLimit();
		}

		private void VerifyTileSize()
		{
			if (_previousTileSizeX != tileGridSizeX || _previousTileSizeY != tileGridSizeY)
			{
				_previousTileSizeX = tileGridSizeX;
				_previousTileSizeY = tileGridSizeY;
				_claheProcessor.setTilesGridSize(new Size(tileGridSizeX, tileGridSizeY));
			}
		}

		private void VerifyClipLimit()
		{
			if (_previousClipLimit != clipLimit)
			{
				_previousClipLimit = clipLimit;
				_claheProcessor.setClipLimit(clipLimit);
			}
		}
	}
}
