using System;
using OpenCVForUnity;
using UnityEngine;

namespace OpenCVForUnityExtended
{
	[Serializable]
	public class CannyModule
	{
		public double minThreshold;

		public double maxThreshold;

		[HideInInspector]
		[SerializeField]
		private int _prevApertureSize;

		public int sobelApertureSize;

		public bool L2gradient;

		public CannyModule()
		{
			minThreshold = 20.0;
			maxThreshold = 60.0;
			sobelApertureSize = 3;
			L2gradient = false;
		}

		public void Process(Mat src, Mat dst)
		{
			VerifyApertureSize();
			Imgproc.Canny(src, dst, minThreshold, maxThreshold, sobelApertureSize, L2gradient);
		}

		private void VerifyApertureSize()
		{
			if (_prevApertureSize != sobelApertureSize)
			{
				if (sobelApertureSize < 3)
				{
					sobelApertureSize = 3;
				}
				else if (sobelApertureSize % 2 == 0)
				{
					sobelApertureSize++;
				}
				_prevApertureSize = sobelApertureSize;
			}
		}
	}
}
