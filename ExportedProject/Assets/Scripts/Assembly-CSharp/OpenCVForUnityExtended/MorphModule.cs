using System;
using OpenCVForUnity;
using UnityEngine;

namespace OpenCVForUnityExtended
{
	[Serializable]
	public class MorphModule
	{
		public enum MorphType
		{
			Erode = 0,
			Dilate = 1,
			Open = 2,
			Close = 3,
			Gradient = 4,
			TopHat = 5,
			BlackHat = 6,
			HitMiss = 7
		}

		public enum MorphShape
		{
			Ellipse = 2,
			Rect = 0,
			Cross = 1
		}

		public MorphType morphType;

		private MorphShape _previousMorphShape;

		public MorphShape morphShape;

		private Mat _kernel;

		private int _prevKernelWidth = -1;

		private int _prevKernelHeight = -1;

		[Range(1f, 99f)]
		public int kernelWidth;

		[Range(1f, 99f)]
		public int kernelHeight;

		private float _anchorX;

		private float _anchorY;

		[Range(1f, 30f)]
		public int iterations;

		public MorphModule()
		{
			morphType = MorphType.Dilate;
			morphShape = MorphShape.Rect;
			kernelWidth = 3;
			kernelHeight = 3;
			_anchorX = 1.5f;
			_anchorY = 1.5f;
			iterations = 1;
		}

		public void Process(Mat src, Mat dst)
		{
			VerifyKernel();
			Imgproc.morphologyEx(src, dst, (int)morphType, _kernel, new Point(_anchorX, _anchorY), iterations);
		}

		public void VerifyKernel()
		{
			if (morphShape != _previousMorphShape || _prevKernelWidth != kernelWidth || _prevKernelHeight != kernelHeight)
			{
				if (kernelWidth < 1)
				{
					kernelWidth = 1;
				}
				if (kernelWidth % 2 == 0)
				{
					kernelWidth++;
				}
				if (kernelHeight < 1)
				{
					kernelHeight = 1;
				}
				if (kernelHeight % 2 == 0)
				{
					kernelHeight++;
				}
				_previousMorphShape = morphShape;
				_prevKernelWidth = kernelWidth;
				_prevKernelHeight = kernelHeight;
				_kernel = Imgproc.getStructuringElement((int)morphShape, new Size(kernelWidth, kernelHeight));
				_anchorX = (float)kernelWidth * 0.5f;
				_anchorY = (float)kernelHeight * 0.5f;
			}
		}
	}
}
