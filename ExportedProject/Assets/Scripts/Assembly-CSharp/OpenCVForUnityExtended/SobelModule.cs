using System;
using OpenCVForUnity;
using UnityEngine;

namespace OpenCVForUnityExtended
{
	[Serializable]
	public class SobelModule
	{
		public enum KernelSize
		{
			Scharr = -1,
			ThreePixels = 3,
			FivePixels = 5,
			SevenPixels = 7
		}

		public enum Depth
		{
			None = 0,
			NegativeOne = -1,
			Short16 = 3,
			Float32 = 5,
			Float64 = 6
		}

		private Depth _previousDepth;

		public Depth dDepth;

		[Range(0f, 10f)]
		public int dx;

		[Range(0f, 10f)]
		public int dy;

		public KernelSize kernelSize;

		[Range(-255f, 255f)]
		public float delta;

		public SobelModule()
		{
			dDepth = Depth.Short16;
			kernelSize = KernelSize.Scharr;
			dx = 1;
			dy = 1;
			delta = 0f;
		}

		public void Process(Mat src, Mat dst, int dxScale = 1, int dyScale = 1)
		{
			VerifyDepth(dst);
			Imgproc.Sobel(src, dst, (int)dDepth, dx * dxScale, dy * dyScale, (int)kernelSize, 1.0, delta, 1);
		}

		private void VerifyDepth(Mat dst)
		{
			if (dDepth == _previousDepth)
			{
				return;
			}
			switch (dst.depth())
			{
			case 0:
				if (dDepth == Depth.NegativeOne || dDepth == Depth.Short16 || dDepth == Depth.Float32 || dDepth == Depth.Float64)
				{
					_previousDepth = dDepth;
					return;
				}
				break;
			case 2:
			case 3:
				if (dDepth == Depth.NegativeOne || dDepth == Depth.Float32 || dDepth == Depth.Float64)
				{
					_previousDepth = dDepth;
					return;
				}
				break;
			case 5:
				if (dDepth == Depth.NegativeOne || dDepth == Depth.Float32 || dDepth == Depth.Float64)
				{
					_previousDepth = dDepth;
					return;
				}
				break;
			case 6:
				if (dDepth == Depth.NegativeOne || dDepth == Depth.Float64)
				{
					_previousDepth = dDepth;
					return;
				}
				break;
			}
			dDepth = Depth.NegativeOne;
			_previousDepth = dDepth;
		}
	}
}
