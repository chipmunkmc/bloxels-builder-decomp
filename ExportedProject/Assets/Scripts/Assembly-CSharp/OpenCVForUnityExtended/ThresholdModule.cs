using System;
using OpenCVForUnity;
using UnityEngine;

namespace OpenCVForUnityExtended
{
	[Serializable]
	public class ThresholdModule
	{
		public enum ThresholdType
		{
			Binary = 0,
			BinaryInv = 1,
			Mask = 7,
			Otsu = 8,
			ToZero = 3,
			ToZeroInv = 4,
			Triangle = 16,
			Truncate = 2
		}

		public ThresholdType thresholdType;

		[Range(0f, 255f)]
		public int thresholdValue;

		[Range(0f, 255f)]
		public int maxValue;

		public ThresholdModule()
		{
			thresholdType = ThresholdType.Binary;
			thresholdValue = 127;
			maxValue = 255;
		}

		public void Process(Mat src, Mat dst)
		{
			Imgproc.threshold(src, dst, thresholdValue, maxValue, (int)thresholdType);
		}
	}
}
