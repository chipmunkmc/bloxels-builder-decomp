using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PPInputController : MonoBehaviour
{
	public enum ControllerScheme
	{
		None = 0,
		Tap = 1,
		Swipe = 2,
		ArrowsLeftRight = 3,
		ArrowsUpDown = 4,
		ArrowsUpDownLeftRight = 5,
		Slider = 6,
		Buttons1 = 7,
		Buttons2 = 8,
		Buttons3 = 9
	}

	public enum SliderControllerScheme
	{
		Fixed = 0,
		Relative = 1
	}

	public enum InputButton
	{
		None = 0,
		TapLeft = 1,
		TapRight = 2,
		PressingLeft = 3,
		PressingRight = 4,
		Left1 = 5,
		Left2 = 6,
		Left3 = 7,
		Right1 = 8,
		Right2 = 9,
		Right3 = 10,
		SwipeLeftUp = 11,
		SwipeLeftDown = 12,
		SwipeLeftLeft = 13,
		SwipeLeftRight = 14,
		SwipeRightUp = 15,
		SwipeRightDown = 16,
		SwipeRightLeft = 17,
		SwipeRightRight = 18,
		AxisLeft = 19,
		AxisRight = 20,
		LAxisUp = 21,
		LAxisDown = 22,
		LAxisLeft = 23,
		LAxisRight = 24,
		RAxisUp = 25,
		RAxisDown = 26,
		RAxisLeft = 27,
		RAxisRight = 28
	}

	public delegate void KeyboardInput(KeyCode key);

	public delegate void ButtonInput(InputButton button);

	public delegate void AxisInput(Vector2 vector);

	public delegate void SwipeInput(Direction direction);

	public delegate void SchemeChanged();

	public static PPInputController instance;

	public GameObject canvasUIBase1024Object;

	public RectTransform canvasTransfromUIBase1024;

	public Canvas canvasUIBase1024;

	public GameObject[] uiTouchButtonsRight;

	public GameObject[] uiTouchDirectionsLeft;

	private float inputDragThreshold = 37.5f;

	private float inputDragThresholdNext = 48.75f;

	private int numKeycodes;

	public static List<ControllerScheme> schemesWithHorizontal;

	public static List<ControllerScheme> schemesWithVertical;

	public ControllerScheme controllerSchemeLeft;

	public ControllerScheme controllerSchemeRight;

	private Vector2 currentInputCenterPoint;

	private Vector2 currentInputCenterPointRight;

	private bool isPressedLeftUp;

	private bool isPressedLeftDown;

	private bool isPressedLeftLeft;

	private bool isPressedLeftRight;

	private bool isPressedRightUp;

	private bool isPressedRightDown;

	private bool isPressedRightLeft;

	private bool isPressedRightRight;

	private bool isPressedL1;

	private bool isPressedL2;

	private bool isPressedL3;

	private bool isPressedR1;

	private bool isPressedR2;

	private bool isPressedR3;

	private List<KeyCode> keysPressedList;

	public static List<KeyCode> previousInputKeyboard;

	public static List<InputButton> previousInputButton;

	public static List<KeyCode> currentInputKeyboard;

	public static List<InputButton> currentInputButton;

	public static Vector2 currentInputAxisL;

	public static Vector2 currentInputAxisR;

	public static Direction currentInputSwipeL;

	public static Direction currentInputSwipeR;

	public static event KeyboardInput OnKeyboard;

	public static event ButtonInput OnButton;

	public static event ButtonInput OnButtonReleased;

	public static event AxisInput OnAxisL;

	public static event AxisInput OnAxisR;

	public static event SwipeInput OnSwipeL;

	public static event SwipeInput OnSwipeR;

	public static event SchemeChanged OnSchemeChanged;

	private void OnDestroy()
	{
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		previousInputKeyboard = new List<KeyCode>();
		previousInputButton = new List<InputButton>();
		currentInputKeyboard = new List<KeyCode>();
		currentInputButton = new List<InputButton>();
		currentInputAxisL = Vector2.zero;
		currentInputAxisR = Vector2.zero;
		currentInputSwipeL = Direction.None;
		currentInputSwipeR = Direction.None;
		numKeycodes = Enum.GetNames(typeof(KeyCode)).Length;
		if (schemesWithHorizontal == null)
		{
			List<ControllerScheme> list = new List<ControllerScheme>();
			list.Add(ControllerScheme.ArrowsLeftRight);
			list.Add(ControllerScheme.ArrowsUpDownLeftRight);
			list.Add(ControllerScheme.Slider);
			schemesWithHorizontal = list;
		}
		if (schemesWithVertical == null)
		{
			List<ControllerScheme> list = new List<ControllerScheme>();
			list.Add(ControllerScheme.ArrowsUpDown);
			list.Add(ControllerScheme.ArrowsUpDownLeftRight);
			list.Add(ControllerScheme.Slider);
			schemesWithVertical = list;
		}
	}

	private void Start()
	{
		canvasUIBase1024 = canvasUIBase1024Object.GetComponent<Canvas>();
		canvasTransfromUIBase1024 = canvasUIBase1024Object.GetComponent<RectTransform>();
		inputDragThreshold *= canvasTransfromUIBase1024.localScale.x;
		inputDragThresholdNext *= canvasTransfromUIBase1024.localScale.x;
	}

	private void Update()
	{
		previousInputKeyboard.Clear();
		previousInputKeyboard.AddRange(currentInputKeyboard);
		currentInputKeyboard.Clear();
		previousInputButton.Clear();
		previousInputButton.AddRange(currentInputButton);
		currentInputButton.Clear();
		currentInputAxisL = Vector2.zero;
		currentInputAxisR = Vector2.zero;
		currentInputSwipeL = Direction.None;
		currentInputSwipeR = Direction.None;
		List<KeyCode> keyboardInput = GetKeyboardInput();
		for (int i = 0; i < keyboardInput.Count; i++)
		{
			KeyCode keyCode = keyboardInput[i];
			if (PPInputController.OnKeyboard != null)
			{
				PPInputController.OnKeyboard(keyCode);
			}
			currentInputKeyboard.Add(keyCode);
		}
		if (isPressedLeftUp)
		{
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(0f, 1f));
			}
			currentInputAxisL = new Vector2(0f, 1f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.LAxisUp);
			}
			currentInputButton.Add(InputButton.LAxisUp);
		}
		else if (previousInputButton.Contains(InputButton.LAxisUp) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.LAxisUp);
		}
		if (isPressedLeftDown)
		{
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(0f, -1f));
			}
			currentInputAxisL = new Vector2(0f, -1f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.LAxisDown);
			}
			currentInputButton.Add(InputButton.LAxisDown);
		}
		else if (previousInputButton.Contains(InputButton.LAxisDown) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.LAxisDown);
		}
		if (isPressedLeftLeft)
		{
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(-1f, 0f));
			}
			currentInputAxisL = new Vector2(-1f, 0f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.LAxisLeft);
			}
			currentInputButton.Add(InputButton.LAxisLeft);
		}
		else if (previousInputButton.Contains(InputButton.LAxisLeft) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.LAxisLeft);
		}
		if (isPressedLeftRight)
		{
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(1f, 0f));
			}
			currentInputAxisL = new Vector2(1f, 0f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.LAxisRight);
			}
			currentInputButton.Add(InputButton.LAxisRight);
		}
		else if (previousInputButton.Contains(InputButton.LAxisRight) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.LAxisRight);
		}
		if (isPressedRightUp)
		{
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(0f, 1f));
			}
			currentInputAxisR = new Vector2(0f, 1f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.RAxisUp);
			}
			currentInputButton.Add(InputButton.RAxisUp);
		}
		else if (previousInputButton.Contains(InputButton.RAxisUp) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.RAxisUp);
		}
		if (isPressedRightDown)
		{
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(0f, -1f));
			}
			currentInputAxisR = new Vector2(0f, -1f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.RAxisDown);
			}
			currentInputButton.Add(InputButton.RAxisDown);
		}
		else if (previousInputButton.Contains(InputButton.RAxisDown) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.RAxisDown);
		}
		if (isPressedRightLeft)
		{
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(-1f, 0f));
			}
			currentInputAxisR = new Vector2(-1f, 0f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.RAxisLeft);
			}
			currentInputButton.Add(InputButton.RAxisLeft);
		}
		else if (previousInputButton.Contains(InputButton.RAxisLeft) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.RAxisLeft);
		}
		if (isPressedRightRight)
		{
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(1f, 0f));
			}
			currentInputAxisR = new Vector2(1f, 0f);
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.RAxisRight);
			}
			currentInputButton.Add(InputButton.RAxisRight);
		}
		else if (previousInputButton.Contains(InputButton.RAxisRight) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.RAxisRight);
		}
		if (isPressedL1)
		{
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.Left1);
			}
			currentInputButton.Add(InputButton.Left1);
		}
		else if (previousInputButton.Contains(InputButton.Left1) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.Left1);
		}
		if (isPressedL2)
		{
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.Left2);
			}
			currentInputButton.Add(InputButton.Left2);
		}
		else if (previousInputButton.Contains(InputButton.Left2) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.Left2);
		}
		if (isPressedL3)
		{
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.Left3);
			}
			currentInputButton.Add(InputButton.Left3);
		}
		else if (previousInputButton.Contains(InputButton.Left3) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.Left3);
		}
		if (isPressedR1)
		{
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.Right1);
			}
			currentInputButton.Add(InputButton.Right1);
		}
		else if (previousInputButton.Contains(InputButton.Right1) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.Right1);
		}
		if (isPressedR2)
		{
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.Right2);
			}
			currentInputButton.Add(InputButton.Right2);
		}
		else if (previousInputButton.Contains(InputButton.Right2) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.Right2);
		}
		if (isPressedR3)
		{
			if (PPInputController.OnButton != null)
			{
				PPInputController.OnButton(InputButton.Right3);
			}
			currentInputButton.Add(InputButton.Right3);
		}
		else if (previousInputButton.Contains(InputButton.Right3) && PPInputController.OnButtonReleased != null)
		{
			PPInputController.OnButtonReleased(InputButton.Right3);
		}
	}

	private void OnFingerEvent(FingerEvent eventData)
	{
		if (!(eventData is FingerMotionEvent))
		{
			return;
		}
		FingerMotionEvent fingerMotionEvent = (FingerMotionEvent)eventData;
		if (controllerSchemeLeft == ControllerScheme.Tap && fingerMotionEvent.Position.x < (float)Screen.width / 2f)
		{
			if (fingerMotionEvent.Phase != FingerMotionPhase.Ended)
			{
				if (PPInputController.OnButton != null)
				{
					PPInputController.OnButton(InputButton.PressingLeft);
				}
				currentInputButton.Add(InputButton.PressingLeft);
			}
			else if (PPInputController.OnButtonReleased != null)
			{
				PPInputController.OnButtonReleased(InputButton.PressingLeft);
			}
		}
		else
		{
			if (controllerSchemeRight != ControllerScheme.Tap || !(fingerMotionEvent.Position.x > (float)Screen.width / 2f))
			{
				return;
			}
			if (fingerMotionEvent.Phase != FingerMotionPhase.Ended)
			{
				if (PPInputController.OnButton != null)
				{
					PPInputController.OnButton(InputButton.PressingRight);
				}
				currentInputButton.Add(InputButton.PressingRight);
			}
			else if (PPInputController.OnButtonReleased != null)
			{
				PPInputController.OnButtonReleased(InputButton.PressingRight);
			}
		}
	}

	public void ButtonPressed(string buttonId)
	{
		switch (buttonId)
		{
		case "L1":
			ButtonPressed(InputButton.Left1);
			break;
		case "L2":
			ButtonPressed(InputButton.Left2);
			break;
		case "L3":
			ButtonPressed(InputButton.Left3);
			break;
		case "R1":
			ButtonPressed(InputButton.Right1);
			break;
		case "R2":
			ButtonPressed(InputButton.Right2);
			break;
		case "R3":
			ButtonPressed(InputButton.Right3);
			break;
		case "Left-Up":
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(0f, 1f));
			}
			currentInputAxisL = new Vector2(0f, 1f);
			ButtonPressed(InputButton.LAxisUp);
			break;
		case "Left-Down":
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(0f, -1f));
			}
			currentInputAxisL = new Vector2(0f, -1f);
			ButtonPressed(InputButton.LAxisDown);
			break;
		case "Left-Left":
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(-1f, 0f));
			}
			currentInputAxisL = new Vector2(-1f, 0f);
			ButtonPressed(InputButton.LAxisLeft);
			break;
		case "Left-Right":
			if (PPInputController.OnAxisL != null)
			{
				PPInputController.OnAxisL(new Vector2(1f, 0f));
			}
			currentInputAxisL = new Vector2(1f, 0f);
			ButtonPressed(InputButton.LAxisRight);
			break;
		case "Right-Up":
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(0f, 1f));
			}
			currentInputAxisR = new Vector2(0f, 1f);
			ButtonPressed(InputButton.RAxisUp);
			break;
		case "Right-Down":
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(0f, -1f));
			}
			currentInputAxisR = new Vector2(0f, -1f);
			ButtonPressed(InputButton.RAxisDown);
			break;
		case "Right-Left":
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(-1f, 0f));
			}
			currentInputAxisR = new Vector2(-1f, 0f);
			ButtonPressed(InputButton.RAxisLeft);
			break;
		case "Right-Right":
			if (PPInputController.OnAxisR != null)
			{
				PPInputController.OnAxisR(new Vector2(1f, 0f));
			}
			currentInputAxisR = new Vector2(1f, 0f);
			ButtonPressed(InputButton.RAxisRight);
			break;
		}
	}

	public void ButtonPressed(InputButton buttonPressed)
	{
		if (PPInputController.OnButton != null)
		{
			PPInputController.OnButton(buttonPressed);
		}
		currentInputButton.Add(buttonPressed);
	}

	public List<KeyCode> GetKeyboardInput()
	{
		if (keysPressedList == null)
		{
			keysPressedList = new List<KeyCode>();
		}
		else
		{
			keysPressedList.Clear();
		}
		for (int i = 0; i < numKeycodes; i++)
		{
			if (Input.GetKey((KeyCode)i))
			{
				keysPressedList.Add((KeyCode)i);
			}
		}
		return keysPressedList;
	}

	public void LeftUpButtonPressed(bool isPressed)
	{
		isPressedLeftUp = isPressed;
		if (!isPressed)
		{
			return;
		}
		bool flag = false;
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).position.x <= (float)Screen.width / 2f)
			{
				flag = true;
			}
		}
		if (!flag)
		{
			isPressedLeftUp = false;
		}
	}

	public void LeftDownButtonPressed(bool isPressed)
	{
		isPressedLeftDown = isPressed;
		if (!isPressed)
		{
			return;
		}
		bool flag = false;
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).position.x <= (float)Screen.width / 2f)
			{
				flag = true;
			}
		}
		if (!flag)
		{
			isPressedLeftDown = false;
		}
	}

	public void LeftLeftButtonPressed(bool isPressed)
	{
		isPressedLeftLeft = isPressed;
		if (!isPressed)
		{
			return;
		}
		bool flag = false;
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).position.x <= (float)Screen.width / 2f)
			{
				flag = true;
			}
		}
		if (!flag)
		{
			isPressedLeftLeft = false;
		}
	}

	public void LeftRightButtonPressed(bool isPressed)
	{
		isPressedLeftRight = isPressed;
		if (!isPressed)
		{
			return;
		}
		bool flag = false;
		for (int i = 0; i < Input.touchCount; i++)
		{
			if (Input.GetTouch(i).position.x <= (float)Screen.width / 2f)
			{
				flag = true;
			}
		}
		if (!flag)
		{
			isPressedLeftRight = false;
		}
	}

	public void RightUpButtonPressed(bool isPressed)
	{
		isPressedRightUp = isPressed;
	}

	public void RightDownButtonPressed(bool isPressed)
	{
		isPressedRightDown = isPressed;
	}

	public void RightLeftButtonPressed(bool isPressed)
	{
		isPressedRightLeft = isPressed;
	}

	public void RightRightButtonPressed(bool isPressed)
	{
		isPressedRightRight = isPressed;
	}

	public void L1ButtonPressed(bool isPressed)
	{
		isPressedL1 = isPressed;
	}

	public void L2ButtonPressed(bool isPressed)
	{
		isPressedL2 = isPressed;
	}

	public void L3ButtonPressed(bool isPressed)
	{
		isPressedL3 = isPressed;
	}

	public void R1ButtonPressed(bool isPressed)
	{
		isPressedR1 = isPressed;
	}

	public void R2ButtonPressed(bool isPressed)
	{
		isPressedR2 = isPressed;
	}

	public void R3ButtonPressed(bool isPressed)
	{
		isPressedR3 = isPressed;
	}

	public static KeyCode StringToKeyCode(string keyCodeString)
	{
		switch (keyCodeString)
		{
		case "None":
			return KeyCode.None;
		case "Backspace":
			return KeyCode.Backspace;
		case "Delete":
			return KeyCode.Delete;
		case "Tab":
			return KeyCode.Tab;
		case "Clear":
			return KeyCode.Clear;
		case "Return":
			return KeyCode.Return;
		case "Pause":
			return KeyCode.Pause;
		case "Escape":
			return KeyCode.Escape;
		case "Space":
			return KeyCode.Space;
		case "Keypad0":
			return KeyCode.Keypad0;
		case "Keypad1":
			return KeyCode.Keypad1;
		case "Keypad2":
			return KeyCode.Keypad2;
		case "Keypad3":
			return KeyCode.Keypad3;
		case "Keypad4":
			return KeyCode.Keypad4;
		case "Keypad5":
			return KeyCode.Keypad5;
		case "Keypad6":
			return KeyCode.Keypad6;
		case "Keypad7":
			return KeyCode.Keypad7;
		case "Keypad8":
			return KeyCode.Keypad8;
		case "Keypad9":
			return KeyCode.Keypad9;
		case "KeypadPeriod":
			return KeyCode.KeypadPeriod;
		case "KeypadDivide":
			return KeyCode.KeypadDivide;
		case "KeypadMultiply":
			return KeyCode.KeypadMultiply;
		case "KeypadMinus":
			return KeyCode.KeypadMinus;
		case "KeypadPlus":
			return KeyCode.KeypadPlus;
		case "KeypadEnter":
			return KeyCode.KeypadEnter;
		case "KeypadEquals":
			return KeyCode.KeypadEquals;
		case "UpArrow":
			return KeyCode.UpArrow;
		case "DownArrow":
			return KeyCode.DownArrow;
		case "RightArrow":
			return KeyCode.RightArrow;
		case "LeftArrow":
			return KeyCode.LeftArrow;
		case "Insert":
			return KeyCode.Insert;
		case "Home":
			return KeyCode.Home;
		case "End":
			return KeyCode.End;
		case "PageUp":
			return KeyCode.PageUp;
		case "PageDown":
			return KeyCode.PageDown;
		case "F1":
			return KeyCode.F1;
		case "F2":
			return KeyCode.F2;
		case "F3":
			return KeyCode.F3;
		case "F4":
			return KeyCode.F4;
		case "F5":
			return KeyCode.F5;
		case "F6":
			return KeyCode.F6;
		case "F7":
			return KeyCode.F7;
		case "F8":
			return KeyCode.F8;
		case "F9":
			return KeyCode.F9;
		case "F10":
			return KeyCode.F10;
		case "F11":
			return KeyCode.F11;
		case "F12":
			return KeyCode.F12;
		case "F13":
			return KeyCode.F13;
		case "F14":
			return KeyCode.F14;
		case "F15":
			return KeyCode.F15;
		case "Alpha0":
			return KeyCode.Alpha0;
		case "Alpha1":
			return KeyCode.Alpha1;
		case "Alpha2":
			return KeyCode.Alpha2;
		case "Alpha3":
			return KeyCode.Alpha3;
		case "Alpha4":
			return KeyCode.Alpha4;
		case "Alpha5":
			return KeyCode.Alpha5;
		case "Alpha6":
			return KeyCode.Alpha6;
		case "Alpha7":
			return KeyCode.Alpha7;
		case "Alpha8":
			return KeyCode.Alpha8;
		case "Alpha9":
			return KeyCode.Alpha9;
		case "Exclaim":
			return KeyCode.Exclaim;
		case "DoubleQuote":
			return KeyCode.DoubleQuote;
		case "Hash":
			return KeyCode.Hash;
		case "Dollar":
			return KeyCode.Dollar;
		case "Ampersand":
			return KeyCode.Ampersand;
		case "Quote":
			return KeyCode.Quote;
		case "LeftParen":
			return KeyCode.LeftParen;
		case "RightParen":
			return KeyCode.RightParen;
		case "Asterisk":
			return KeyCode.Asterisk;
		case "Plus":
			return KeyCode.Plus;
		case "Comma":
			return KeyCode.Comma;
		case "Minus":
			return KeyCode.Minus;
		case "Period":
			return KeyCode.Period;
		case "Slash":
			return KeyCode.Slash;
		case "Colon":
			return KeyCode.Colon;
		case "Semicolon":
			return KeyCode.Semicolon;
		case "Less":
			return KeyCode.Less;
		case "Equals":
			return KeyCode.Equals;
		case "Greater":
			return KeyCode.Greater;
		case "Question":
			return KeyCode.Question;
		case "At":
			return KeyCode.At;
		case "LeftBracket":
			return KeyCode.LeftBracket;
		case "Backslash":
			return KeyCode.Backslash;
		case "RightBracket":
			return KeyCode.RightBracket;
		case "Caret":
			return KeyCode.Caret;
		case "Underscore":
			return KeyCode.Underscore;
		case "BackQuote":
			return KeyCode.BackQuote;
		case "A":
			return KeyCode.A;
		case "B":
			return KeyCode.B;
		case "C":
			return KeyCode.C;
		case "D":
			return KeyCode.D;
		case "E":
			return KeyCode.E;
		case "F":
			return KeyCode.F;
		case "G":
			return KeyCode.G;
		case "H":
			return KeyCode.H;
		case "I":
			return KeyCode.I;
		case "J":
			return KeyCode.J;
		case "K":
			return KeyCode.K;
		case "L":
			return KeyCode.L;
		case "M":
			return KeyCode.M;
		case "N":
			return KeyCode.N;
		case "O":
			return KeyCode.O;
		case "P":
			return KeyCode.P;
		case "Q":
			return KeyCode.Q;
		case "R":
			return KeyCode.R;
		case "S":
			return KeyCode.S;
		case "T":
			return KeyCode.T;
		case "U":
			return KeyCode.U;
		case "V":
			return KeyCode.V;
		case "W":
			return KeyCode.W;
		case "X":
			return KeyCode.X;
		case "Y":
			return KeyCode.Y;
		case "Z":
			return KeyCode.Z;
		case "Numlock":
			return KeyCode.Numlock;
		case "CapsLock":
			return KeyCode.CapsLock;
		case "ScrollLock":
			return KeyCode.ScrollLock;
		case "RightShift":
			return KeyCode.RightShift;
		case "LeftShift":
			return KeyCode.LeftShift;
		case "RightControl":
			return KeyCode.RightControl;
		case "LeftControl":
			return KeyCode.LeftControl;
		case "RightAlt":
			return KeyCode.RightAlt;
		case "LeftAlt":
			return KeyCode.LeftAlt;
		case "LeftCommand":
			return KeyCode.LeftCommand;
		case "LeftApple":
			return KeyCode.LeftCommand;
		case "LeftWindows":
			return KeyCode.LeftWindows;
		case "RightCommand":
			return KeyCode.RightCommand;
		case "RightApple":
			return KeyCode.RightCommand;
		case "RightWindows":
			return KeyCode.RightWindows;
		case "AltGr":
			return KeyCode.AltGr;
		case "Help":
			return KeyCode.Help;
		case "Print":
			return KeyCode.Print;
		case "SysReq":
			return KeyCode.SysReq;
		case "Break":
			return KeyCode.Break;
		case "Menu":
			return KeyCode.Menu;
		case "Mouse0":
			return KeyCode.Mouse0;
		case "Mouse1":
			return KeyCode.Mouse1;
		case "Mouse2":
			return KeyCode.Mouse2;
		case "Mouse3":
			return KeyCode.Mouse3;
		case "Mouse4":
			return KeyCode.Mouse4;
		case "Mouse5":
			return KeyCode.Mouse5;
		case "Mouse6":
			return KeyCode.Mouse6;
		case "JoystickButton0":
			return KeyCode.JoystickButton0;
		case "JoystickButton1":
			return KeyCode.JoystickButton1;
		case "JoystickButton2":
			return KeyCode.JoystickButton2;
		case "JoystickButton3":
			return KeyCode.JoystickButton3;
		case "JoystickButton4":
			return KeyCode.JoystickButton4;
		case "JoystickButton5":
			return KeyCode.JoystickButton5;
		case "JoystickButton6":
			return KeyCode.JoystickButton6;
		case "JoystickButton7":
			return KeyCode.JoystickButton7;
		case "JoystickButton8":
			return KeyCode.JoystickButton8;
		case "JoystickButton9":
			return KeyCode.JoystickButton9;
		case "JoystickButton10":
			return KeyCode.JoystickButton10;
		case "JoystickButton11":
			return KeyCode.JoystickButton11;
		case "JoystickButton12":
			return KeyCode.JoystickButton12;
		case "JoystickButton13":
			return KeyCode.JoystickButton13;
		case "JoystickButton14":
			return KeyCode.JoystickButton14;
		case "JoystickButton15":
			return KeyCode.JoystickButton15;
		case "JoystickButton16":
			return KeyCode.JoystickButton16;
		case "JoystickButton17":
			return KeyCode.JoystickButton17;
		case "JoystickButton18":
			return KeyCode.JoystickButton18;
		case "JoystickButton19":
			return KeyCode.JoystickButton19;
		case "Joystick1Button0":
			return KeyCode.Joystick1Button0;
		case "Joystick1Button1":
			return KeyCode.Joystick1Button1;
		case "Joystick1Button2":
			return KeyCode.Joystick1Button2;
		case "Joystick1Button3":
			return KeyCode.Joystick1Button3;
		case "Joystick1Button4":
			return KeyCode.Joystick1Button4;
		case "Joystick1Button5":
			return KeyCode.Joystick1Button5;
		case "Joystick1Button6":
			return KeyCode.Joystick1Button6;
		case "Joystick1Button7":
			return KeyCode.Joystick1Button7;
		case "Joystick1Button8":
			return KeyCode.Joystick1Button8;
		case "Joystick1Button9":
			return KeyCode.Joystick1Button9;
		case "Joystick1Button10":
			return KeyCode.Joystick1Button10;
		case "Joystick1Button11":
			return KeyCode.Joystick1Button11;
		case "Joystick1Button12":
			return KeyCode.Joystick1Button12;
		case "Joystick1Button13":
			return KeyCode.Joystick1Button13;
		case "Joystick1Button14":
			return KeyCode.Joystick1Button14;
		case "Joystick1Button15":
			return KeyCode.Joystick1Button15;
		case "Joystick1Button16":
			return KeyCode.Joystick1Button16;
		case "Joystick1Button17":
			return KeyCode.Joystick1Button17;
		case "Joystick1Button18":
			return KeyCode.Joystick1Button18;
		case "Joystick1Button19":
			return KeyCode.Joystick1Button19;
		case "Joystick2Button0":
			return KeyCode.Joystick2Button0;
		case "Joystick2Button1":
			return KeyCode.Joystick2Button1;
		case "Joystick2Button2":
			return KeyCode.Joystick2Button2;
		case "Joystick2Button3":
			return KeyCode.Joystick2Button3;
		case "Joystick2Button4":
			return KeyCode.Joystick2Button4;
		case "Joystick2Button5":
			return KeyCode.Joystick2Button5;
		case "Joystick2Button6":
			return KeyCode.Joystick2Button6;
		case "Joystick2Button7":
			return KeyCode.Joystick2Button7;
		case "Joystick2Button8":
			return KeyCode.Joystick2Button8;
		case "Joystick2Button9":
			return KeyCode.Joystick2Button9;
		case "Joystick2Button10":
			return KeyCode.Joystick2Button10;
		case "Joystick2Button11":
			return KeyCode.Joystick2Button11;
		case "Joystick2Button12":
			return KeyCode.Joystick2Button12;
		case "Joystick2Button13":
			return KeyCode.Joystick2Button13;
		case "Joystick2Button14":
			return KeyCode.Joystick2Button14;
		case "Joystick2Button15":
			return KeyCode.Joystick2Button15;
		case "Joystick2Button16":
			return KeyCode.Joystick2Button16;
		case "Joystick2Button17":
			return KeyCode.Joystick2Button17;
		case "Joystick2Button18":
			return KeyCode.Joystick2Button18;
		case "Joystick2Button19":
			return KeyCode.Joystick2Button19;
		case "Joystick3Button0":
			return KeyCode.Joystick3Button0;
		case "Joystick3Button1":
			return KeyCode.Joystick3Button1;
		case "Joystick3Button2":
			return KeyCode.Joystick3Button2;
		case "Joystick3Button3":
			return KeyCode.Joystick3Button3;
		case "Joystick3Button4":
			return KeyCode.Joystick3Button4;
		case "Joystick3Button5":
			return KeyCode.Joystick3Button5;
		case "Joystick3Button6":
			return KeyCode.Joystick3Button6;
		case "Joystick3Button7":
			return KeyCode.Joystick3Button7;
		case "Joystick3Button8":
			return KeyCode.Joystick3Button8;
		case "Joystick3Button9":
			return KeyCode.Joystick3Button9;
		case "Joystick3Button10":
			return KeyCode.Joystick3Button10;
		case "Joystick3Button11":
			return KeyCode.Joystick3Button11;
		case "Joystick3Button12":
			return KeyCode.Joystick3Button12;
		case "Joystick3Button13":
			return KeyCode.Joystick3Button13;
		case "Joystick3Button14":
			return KeyCode.Joystick3Button14;
		case "Joystick3Button15":
			return KeyCode.Joystick3Button15;
		case "Joystick3Button16":
			return KeyCode.Joystick3Button16;
		case "Joystick3Button17":
			return KeyCode.Joystick3Button17;
		case "Joystick3Button18":
			return KeyCode.Joystick3Button18;
		case "Joystick3Button19":
			return KeyCode.Joystick3Button19;
		case "Joystick4Button0":
			return KeyCode.Joystick4Button0;
		case "Joystick4Button1":
			return KeyCode.Joystick4Button1;
		case "Joystick4Button2":
			return KeyCode.Joystick4Button2;
		case "Joystick4Button3":
			return KeyCode.Joystick4Button3;
		case "Joystick4Button4":
			return KeyCode.Joystick4Button4;
		case "Joystick4Button5":
			return KeyCode.Joystick4Button5;
		case "Joystick4Button6":
			return KeyCode.Joystick4Button6;
		case "Joystick4Button7":
			return KeyCode.Joystick4Button7;
		case "Joystick4Button8":
			return KeyCode.Joystick4Button8;
		case "Joystick4Button9":
			return KeyCode.Joystick4Button9;
		case "Joystick4Button10":
			return KeyCode.Joystick4Button10;
		case "Joystick4Button11":
			return KeyCode.Joystick4Button11;
		case "Joystick4Button12":
			return KeyCode.Joystick4Button12;
		case "Joystick4Button13":
			return KeyCode.Joystick4Button13;
		case "Joystick4Button14":
			return KeyCode.Joystick4Button14;
		case "Joystick4Button15":
			return KeyCode.Joystick4Button15;
		case "Joystick4Button16":
			return KeyCode.Joystick4Button16;
		case "Joystick4Button17":
			return KeyCode.Joystick4Button17;
		case "Joystick4Button18":
			return KeyCode.Joystick4Button18;
		case "Joystick4Button19":
			return KeyCode.Joystick4Button19;
		case "Joystick5Button0":
			return KeyCode.Joystick5Button0;
		case "Joystick5Button1":
			return KeyCode.Joystick5Button1;
		case "Joystick5Button2":
			return KeyCode.Joystick5Button2;
		case "Joystick5Button3":
			return KeyCode.Joystick5Button3;
		case "Joystick5Button4":
			return KeyCode.Joystick5Button4;
		case "Joystick5Button5":
			return KeyCode.Joystick5Button5;
		case "Joystick5Button6":
			return KeyCode.Joystick5Button6;
		case "Joystick5Button7":
			return KeyCode.Joystick5Button7;
		case "Joystick5Button8":
			return KeyCode.Joystick5Button8;
		case "Joystick5Button9":
			return KeyCode.Joystick5Button9;
		case "Joystick5Button10":
			return KeyCode.Joystick5Button10;
		case "Joystick5Button11":
			return KeyCode.Joystick5Button11;
		case "Joystick5Button12":
			return KeyCode.Joystick5Button12;
		case "Joystick5Button13":
			return KeyCode.Joystick5Button13;
		case "Joystick5Button14":
			return KeyCode.Joystick5Button14;
		case "Joystick5Button15":
			return KeyCode.Joystick5Button15;
		case "Joystick5Button16":
			return KeyCode.Joystick5Button16;
		case "Joystick5Button17":
			return KeyCode.Joystick5Button17;
		case "Joystick5Button18":
			return KeyCode.Joystick5Button18;
		case "Joystick5Button19":
			return KeyCode.Joystick5Button19;
		case "Joystick6Button0":
			return KeyCode.Joystick6Button0;
		case "Joystick6Button1":
			return KeyCode.Joystick6Button1;
		case "Joystick6Button2":
			return KeyCode.Joystick6Button2;
		case "Joystick6Button3":
			return KeyCode.Joystick6Button3;
		case "Joystick6Button4":
			return KeyCode.Joystick6Button4;
		case "Joystick6Button5":
			return KeyCode.Joystick6Button5;
		case "Joystick6Button6":
			return KeyCode.Joystick6Button6;
		case "Joystick6Button7":
			return KeyCode.Joystick6Button7;
		case "Joystick6Button8":
			return KeyCode.Joystick6Button8;
		case "Joystick6Button9":
			return KeyCode.Joystick6Button9;
		case "Joystick6Button10":
			return KeyCode.Joystick6Button10;
		case "Joystick6Button11":
			return KeyCode.Joystick6Button11;
		case "Joystick6Button12":
			return KeyCode.Joystick6Button12;
		case "Joystick6Button13":
			return KeyCode.Joystick6Button13;
		case "Joystick6Button14":
			return KeyCode.Joystick6Button14;
		case "Joystick6Button15":
			return KeyCode.Joystick6Button15;
		case "Joystick6Button16":
			return KeyCode.Joystick6Button16;
		case "Joystick6Button17":
			return KeyCode.Joystick6Button17;
		case "Joystick6Button18":
			return KeyCode.Joystick6Button18;
		case "Joystick6Button19":
			return KeyCode.Joystick6Button19;
		case "Joystick7Button0":
			return KeyCode.Joystick7Button0;
		case "Joystick7Button1":
			return KeyCode.Joystick7Button1;
		case "Joystick7Button2":
			return KeyCode.Joystick7Button2;
		case "Joystick7Button3":
			return KeyCode.Joystick7Button3;
		case "Joystick7Button4":
			return KeyCode.Joystick7Button4;
		case "Joystick7Button5":
			return KeyCode.Joystick7Button5;
		case "Joystick7Button6":
			return KeyCode.Joystick7Button6;
		case "Joystick7Button7":
			return KeyCode.Joystick7Button7;
		case "Joystick7Button8":
			return KeyCode.Joystick7Button8;
		case "Joystick7Button9":
			return KeyCode.Joystick7Button9;
		case "Joystick7Button10":
			return KeyCode.Joystick7Button10;
		case "Joystick7Button11":
			return KeyCode.Joystick7Button11;
		case "Joystick7Button12":
			return KeyCode.Joystick7Button12;
		case "Joystick7Button13":
			return KeyCode.Joystick7Button13;
		case "Joystick7Button14":
			return KeyCode.Joystick7Button14;
		case "Joystick7Button15":
			return KeyCode.Joystick7Button15;
		case "Joystick7Button16":
			return KeyCode.Joystick7Button16;
		case "Joystick7Button17":
			return KeyCode.Joystick7Button17;
		case "Joystick7Button18":
			return KeyCode.Joystick7Button18;
		case "Joystick7Button19":
			return KeyCode.Joystick7Button19;
		case "Joystick8Button0":
			return KeyCode.Joystick8Button0;
		case "Joystick8Button1":
			return KeyCode.Joystick8Button1;
		case "Joystick8Button2":
			return KeyCode.Joystick8Button2;
		case "Joystick8Button3":
			return KeyCode.Joystick8Button3;
		case "Joystick8Button4":
			return KeyCode.Joystick8Button4;
		case "Joystick8Button5":
			return KeyCode.Joystick8Button5;
		case "Joystick8Button6":
			return KeyCode.Joystick8Button6;
		case "Joystick8Button7":
			return KeyCode.Joystick8Button7;
		case "Joystick8Button8":
			return KeyCode.Joystick8Button8;
		case "Joystick8Button9":
			return KeyCode.Joystick8Button9;
		case "Joystick8Button10":
			return KeyCode.Joystick8Button10;
		case "Joystick8Button11":
			return KeyCode.Joystick8Button11;
		case "Joystick8Button12":
			return KeyCode.Joystick8Button12;
		case "Joystick8Button13":
			return KeyCode.Joystick8Button13;
		case "Joystick8Button14":
			return KeyCode.Joystick8Button14;
		case "Joystick8Button15":
			return KeyCode.Joystick8Button15;
		case "Joystick8Button16":
			return KeyCode.Joystick8Button16;
		case "Joystick8Button17":
			return KeyCode.Joystick8Button17;
		case "Joystick8Button18":
			return KeyCode.Joystick8Button18;
		case "Joystick8Button19":
			return KeyCode.Joystick8Button19;
		default:
			return KeyCode.None;
		}
	}

	public static InputButton StringToInputButton(string inputButtonString)
	{
		switch (inputButtonString)
		{
		case "None":
			return InputButton.None;
		case "TapLeft":
			return InputButton.TapLeft;
		case "TapRight":
			return InputButton.TapRight;
		case "PressingLeft":
			return InputButton.PressingLeft;
		case "PressingRight":
			return InputButton.PressingRight;
		case "Left1":
			return InputButton.Left1;
		case "Left2":
			return InputButton.Left2;
		case "Left3":
			return InputButton.Left3;
		case "Right1":
			return InputButton.Right1;
		case "Right2":
			return InputButton.Right2;
		case "Right3":
			return InputButton.Right3;
		case "SwipeLeftUp":
			return InputButton.SwipeLeftUp;
		case "SwipeLeftDown":
			return InputButton.SwipeLeftDown;
		case "SwipeLeftLeft":
			return InputButton.SwipeLeftLeft;
		case "SwipeLeftRight":
			return InputButton.SwipeLeftRight;
		case "SwipeRightUp":
			return InputButton.SwipeRightUp;
		case "SwipeRightDown":
			return InputButton.SwipeRightDown;
		case "SwipeRightLeft":
			return InputButton.SwipeRightLeft;
		case "SwipeRightRight":
			return InputButton.SwipeRightRight;
		case "AxisLeft":
			return InputButton.AxisLeft;
		case "AxisRight":
			return InputButton.AxisRight;
		case "LAxisUp":
			return InputButton.LAxisUp;
		case "LAxisDown":
			return InputButton.LAxisDown;
		case "LAxisLeft":
			return InputButton.LAxisLeft;
		case "LAxisRight":
			return InputButton.LAxisRight;
		case "RAxisUp":
			return InputButton.RAxisUp;
		case "RAxisDown":
			return InputButton.RAxisDown;
		case "RAxisLeft":
			return InputButton.RAxisLeft;
		case "RAxisRight":
			return InputButton.RAxisRight;
		default:
			return InputButton.None;
		}
	}
}
