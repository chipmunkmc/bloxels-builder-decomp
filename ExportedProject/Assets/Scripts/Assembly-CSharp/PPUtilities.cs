using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class PPUtilities
{
	public static readonly string Link_allTutorials = "http://www.bloxelsbuilder.com/all-tutorials";

	public static readonly string Link_tutorialBoards = "http://www.bloxelsbuilder.com/tutorials-boards";

	public static readonly string Link_tutorialAnimations = "http://www.bloxelsbuilder.com/tutorials-animations";

	public static readonly string Link_tutorialBackgrounds = "http://www.bloxelsbuilder.com/tutorials-backgrounds";

	public static readonly string Link_tutorialCharacters = "http://www.bloxelsbuilder.com/tutorials-characters";

	public static readonly string Link_tutorialGames = "http://www.bloxelsbuilder.com/tutorials-games";

	public static readonly string Link_tutorialCapture = "http://www.bloxelsbuilder.com/tutorials-capture";

	public static readonly string Link_tutorialCodeboard = "http://www.bloxelsbuilder.com/tutorials-codeboard";

	public static readonly string Link_tutorialiWall = "http://www.bloxelsbuilder.com/tutorials-infinity-wall";

	public static readonly string Link_tutorialAll = "http://www.bloxelsbuilder.com/all-tutorials";

	public static readonly string Link_terms = "http://www.projectpixelpress.com/terms-of-use";

	public static readonly string Link_privacy = "http://www.projectpixelpress.com/privacy-policy";

	public static readonly string Link_support = "http://www.bloxelsbuilder.com/support";

	public static readonly string Link_kickstarter = "http://www.bloxelsbuilder.com/kickstarter";

	public static readonly string Link_legal = "http://www.bloxelsbuilder.com/legal";

	public static readonly string Link_store = "http://store.bloxelsbuilder.com";

	public static readonly string Link_passwordReset = "http://bloxelsbuilder.com/game-password-reset";

	public static readonly string Link_website = "http://bloxelsbuilder.com";

	public static readonly string Link_Brains = "http://kids.bloxelsbuilder.com/tutorials-brain-boards";

	public static readonly string Link_Gems = "http://kids.bloxelsbuilder.com/tutorials-gems";

	public static readonly string Link_AboutBoard = "http://bloxelsbuilder.com/about-the-board";

	public static readonly string Link_StudentAccounts = "http://www.bloxelsbuilder.com/educator-account-creation";

	public static readonly string Link_MigrationFallback = "http://www.projectpixelpress.com/";

	private static int[] blockColorCounts = new int[8];

	private static int[] blockColorBuffer = new int[169];

	public static Vector2[] GetAnchorsFromDirection(Direction dir)
	{
		Vector2[] array = new Vector2[2];
		switch (dir)
		{
		case Direction.UpLeft:
			array[0] = new Vector2(0f, 1f);
			array[1] = new Vector2(0f, 1f);
			break;
		case Direction.Up:
			array[0] = new Vector2(0.5f, 1f);
			array[1] = new Vector2(0.5f, 1f);
			break;
		case Direction.UpRight:
			array[0] = new Vector2(1f, 1f);
			array[1] = new Vector2(1f, 1f);
			break;
		case Direction.Right:
			array[0] = new Vector2(1f, 0.5f);
			array[1] = new Vector2(1f, 0.5f);
			break;
		case Direction.DownRight:
			array[0] = new Vector2(1f, 0f);
			array[1] = new Vector2(1f, 0f);
			break;
		case Direction.Down:
			array[0] = new Vector2(0.5f, 0f);
			array[1] = new Vector2(0.5f, 0f);
			break;
		case Direction.DownLeft:
			array[0] = new Vector2(0f, 0f);
			array[1] = new Vector2(0f, 0f);
			break;
		case Direction.Left:
			array[0] = new Vector2(0f, 0.5f);
			array[1] = new Vector2(0f, 0.5f);
			break;
		case Direction.All:
			array[0] = new Vector2(0.5f, 0.5f);
			array[1] = new Vector2(0.5f, 0.5f);
			break;
		}
		return array;
	}

	public static Vector2 GetPivotFromDirection(Direction dir)
	{
		Vector2 result = Vector2.zero;
		switch (dir)
		{
		case Direction.UpLeft:
			result = new Vector2(0f, 1f);
			break;
		case Direction.Up:
			result = new Vector2(0.5f, 1f);
			break;
		case Direction.UpRight:
			result = new Vector2(1f, 1f);
			break;
		case Direction.Right:
			result = new Vector2(1f, 0.5f);
			break;
		case Direction.DownRight:
			result = new Vector2(1f, 0f);
			break;
		case Direction.Down:
			result = new Vector2(0.5f, 0f);
			break;
		case Direction.DownLeft:
			result = new Vector2(0f, 0f);
			break;
		case Direction.Left:
			result = new Vector2(0f, 0.5f);
			break;
		case Direction.All:
			result = new Vector2(0.5f, 0.5f);
			break;
		}
		return result;
	}

	public static int GetSignFromDirection(Direction dir)
	{
		int result = 0;
		switch (dir)
		{
		case Direction.Up:
		case Direction.Left:
			result = -1;
			break;
		case Direction.Right:
		case Direction.Down:
			result = 1;
			break;
		}
		return result;
	}

	public static Vector3 CalculatePositionFromTransformToRectTransform(Canvas _Canvas, Vector3 _Position, Camera _Cam)
	{
		Vector3 result = Vector3.zero;
		if (_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
		{
			result = _Cam.WorldToScreenPoint(_Position);
		}
		else if (_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
		{
			Vector2 localPoint = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(_Canvas.transform as RectTransform, _Cam.WorldToScreenPoint(_Position), _Cam, out localPoint);
			result = _Canvas.transform.TransformPoint(localPoint);
		}
		return result;
	}

	public static Vector3 GetPositionInCanvasFromMousePosition(Canvas _Canvas, Camera _Cam)
	{
		Vector3 result = Vector3.zero;
		if (_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
		{
			result = Input.mousePosition;
		}
		else if (_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
		{
			Vector2 localPoint = Vector2.zero;
			RectTransformUtility.ScreenPointToLocalPointInRectangle(_Canvas.transform as RectTransform, Input.mousePosition, _Cam, out localPoint);
			result = _Canvas.transform.TransformPoint(localPoint);
		}
		return result;
	}

	public static Vector3 CalculatePositionFromRectTransformToTransform(Canvas _Canvas, Vector3 _Position, Camera _Cam)
	{
		Vector3 worldPoint = Vector3.zero;
		if (_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
		{
			worldPoint = _Cam.ScreenToWorldPoint(_Position);
		}
		else if (_Canvas.renderMode == RenderMode.ScreenSpaceCamera)
		{
			RectTransformUtility.ScreenPointToWorldPointInRectangle(_Canvas.transform as RectTransform, _Cam.WorldToScreenPoint(_Position), _Cam, out worldPoint);
		}
		return worldPoint;
	}

	public static Vector3 GetWorldPositionOnXYPlane(Camera camera, Vector3 screenPosition, float z, bool returnPositionInWorldWrapper = true)
	{
		Ray ray = camera.ScreenPointToRay(screenPosition);
		float enter;
		new Plane(Vector3.forward, new Vector3(0f, 0f, z)).Raycast(ray, out enter);
		if (returnPositionInWorldWrapper)
		{
			return WorldWrapper.instance.selfTransform.InverseTransformPoint(ray.GetPoint(enter));
		}
		return ray.GetPoint(enter);
	}

	public static string Sanitize(string s)
	{
		string text = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890!-_";
		StringBuilder stringBuilder = new StringBuilder(s.Length);
		bool flag = false;
		foreach (char c in s)
		{
			flag = false;
			string text2 = text;
			foreach (char c2 in text2)
			{
				if (c == c2)
				{
					flag = true;
					stringBuilder.Append(c);
				}
			}
		}
		if (!flag)
		{
			return string.Empty;
		}
		return stringBuilder.ToString();
	}

	public static Color BlendColors(Color a, Color b)
	{
		float r = (a.r + b.r) / 2f;
		float g = (a.g + b.g) / 2f;
		float b2 = (a.b + b.b) / 2f;
		float a2 = (a.a + b.a) / 2f;
		return new Color(r, g, b2, a2);
	}

	public static Color BlendColors(Color[] colors)
	{
		int num = colors.Length;
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		float num5 = 0f;
		for (int i = 0; i < num; i++)
		{
			num2 += colors[i].r;
			num3 += colors[i].g;
			num4 += colors[i].b;
			num5 += colors[i].a;
		}
		float num6 = 0f;
		float r = num2 / (float)num;
		float g = num3 / (float)num;
		float b = num4 / (float)num;
		num6 = num5 / (float)num;
		return new Color(r, g, b, num6);
	}

	public static void ScrollRect_FitContent(RectTransform contentRect)
	{
		GridLayoutGroup component = contentRect.GetComponent<GridLayoutGroup>();
		int childCount = contentRect.childCount;
		int num = Mathf.CeilToInt((float)childCount / (float)component.constraintCount);
		float num2 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num * num2);
		contentRect.sizeDelta = new Vector2(contentRect.sizeDelta.x, y);
	}

	public static void ScrollRect_JumpToIndex_Vertical(ScrollRect scrollRect, GridLayoutGroup grid, int index)
	{
		float y = (grid.spacing.y + grid.cellSize.y) * (float)index;
		scrollRect.content.anchoredPosition = new Vector2(scrollRect.content.anchoredPosition.x, y);
	}

	public static bool NearlyEqual(float a, float b)
	{
		float num = Mathf.Abs(a);
		float num2 = Mathf.Abs(b);
		float num3 = Mathf.Abs(a - b);
		if (a == b)
		{
			return true;
		}
		if (a == 0f || b == 0f || num3 < float.MinValue)
		{
			return num3 < -4.7683713E-07f;
		}
		return num3 / (num + num2) < float.Epsilon;
	}

	public static bool IsBetween(int i, int start, int end)
	{
		return i > start && i < end;
	}

	public static string ORStrings(string a, string b)
	{
		if (a.Length > b.Length)
		{
			b = b.PadRight(a.Length, '0');
		}
		else if (a.Length < b.Length)
		{
			a = a.PadRight(b.Length, '0');
		}
		char[] array = a.ToCharArray();
		char[] array2 = b.ToCharArray();
		char[] array3 = new char[a.Length];
		for (int i = 0; i < array3.Length; i++)
		{
			array3[i] = (char)(array[i] | array2[i]);
		}
		return new string(array3);
	}

	public static char[] ORChars(char[] a, char[] b)
	{
		if (a.Length != b.Length)
		{
			return null;
		}
		char[] array = new char[a.Length];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = (char)(a[i] | b[i]);
		}
		return array;
	}

	public static int DifferenceBoolStrings(string a, string b)
	{
		if (a.Length > b.Length)
		{
			b = b.PadRight(a.Length, '0');
		}
		else
		{
			a = a.PadRight(b.Length, '0');
		}
		char[] array = a.ToCharArray();
		char[] array2 = b.ToCharArray();
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] == '1')
			{
				num++;
			}
			if (array2[i] == '1')
			{
				num2++;
			}
		}
		return Math.Abs(num - num2);
	}

	public static void CopyDirectory(string sourceDirectory, string targetDirectory)
	{
		DirectoryInfo source = new DirectoryInfo(sourceDirectory);
		DirectoryInfo target = new DirectoryInfo(targetDirectory);
		CopyDirectoryContents(source, target);
	}

	public static void CopyDirectoryContents(DirectoryInfo source, DirectoryInfo target)
	{
		DirectoryInfo[] directories = source.GetDirectories();
		foreach (DirectoryInfo directoryInfo in directories)
		{
			CopyDirectoryContents(directoryInfo, target.CreateSubdirectory(directoryInfo.Name));
		}
		FileInfo[] files = source.GetFiles();
		foreach (FileInfo fileInfo in files)
		{
			string text = Path.Combine(target.FullName, fileInfo.Name);
			if (!File.Exists(text))
			{
				fileInfo.CopyTo(text);
			}
		}
	}

	public static IEnumerable<string> GetFileList(string rootFolder, string searchPattern)
	{
		Queue<string> pending = new Queue<string>();
		pending.Enqueue(rootFolder);
		while (pending.Count > 0)
		{
			rootFolder = pending.Dequeue();
			string[] tmp2 = Directory.GetFiles(rootFolder, searchPattern);
			for (int i = 0; i < tmp2.Length; i++)
			{
				if (!tmp2[i].Contains(".DS_Store"))
				{
					yield return tmp2[i];
				}
			}
			tmp2 = Directory.GetDirectories(rootFolder);
			for (int j = 0; j < tmp2.Length; j++)
			{
				pending.Enqueue(tmp2[j]);
			}
		}
	}

	public static BlockColor GetBlockColorMode(BlockColor[,] blockColors)
	{
		Array.Clear(blockColorCounts, 0, 8);
		int num = 0;
		BlockColor result = BlockColor.Blank;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = blockColors[j, i];
				if (blockColor != BlockColor.Blank)
				{
					int num2 = ++blockColorCounts[(uint)blockColor];
					if (num2 > num)
					{
						num = num2;
						result = blockColor;
					}
				}
			}
		}
		return result;
	}

	public static BlockColor GetBlockColorMode(BlockColor[] blockColors)
	{
		Array.Clear(blockColorCounts, 0, 8);
		int num = 0;
		BlockColor result = BlockColor.Blank;
		int num2 = blockColors.Length;
		for (int i = 0; i < num2; i++)
		{
			BlockColor blockColor = blockColors[i];
			if (blockColor != BlockColor.Blank)
			{
				int num3 = ++blockColorCounts[(uint)blockColor];
				if (num3 > num)
				{
					num = num3;
					result = blockColor;
				}
			}
		}
		return result;
	}

	public GridLocation GetGridLocationMode(GridLocation[] gridLocations)
	{
		Array.Clear(blockColorBuffer, 0, 169);
		int num = 0;
		int num2 = 0;
		GridLocation result = GridLocation.InvalidLocation;
		int num3 = gridLocations.Length;
		for (int i = 0; i < num3; i++)
		{
			GridLocation gridLocation = gridLocations[i];
			int num4 = gridLocation.r * 13 + gridLocation.c;
			num = ++blockColorBuffer[num4];
			if (num > num2)
			{
				num2 = num;
				result = gridLocation;
			}
		}
		return result;
	}

	public static BlockColor GetMedianBlockColor(BlockColor[,] blockColors)
	{
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = blockColors[j, i];
				if (blockColor != BlockColor.Blank)
				{
					blockColorBuffer[num++] = (int)blockColor;
				}
			}
		}
		if (num == 0)
		{
			return BlockColor.Blank;
		}
		return (BlockColor)GetMedianInt(blockColorBuffer, num);
	}

	public static int GetGridLocationModeInt(int[] values, int length)
	{
		Array.Clear(blockColorBuffer, 0, 169);
		int num = 0;
		int result = -1;
		for (int i = 0; i < length; i++)
		{
			int num2 = values[i];
			if (num2 >= 0)
			{
				int num3 = ++blockColorBuffer[num2];
				if (num3 > num)
				{
					num = num3;
					result = num2;
				}
			}
		}
		return result;
	}

	public static int GetMedianInt(int[] values, int length, int reductionFactor = 1)
	{
		int num = length;
		if (reductionFactor > 1)
		{
			int num2 = 0;
			for (int i = 0; i < length; i++)
			{
				if (i % reductionFactor == 0)
				{
					values[num2] = values[i];
					num2++;
				}
			}
			num = num2;
		}
		Array.Sort(values, 0, num);
		int num3 = num / 2;
		return (num % 2 == 0) ? ((values[num3] + values[num3 - 1]) / 2) : values[num3];
	}

	public static short GetMedianShort(short[] values, int length)
	{
		Array.Sort(values, 0, length);
		int num = length / 2;
		int num2 = ((length % 2 == 0) ? ((values[num] + values[num - 1]) / 2) : values[num]);
		return (short)num2;
	}

	public static int GetMode(short[] hues)
	{
		Dictionary<int, int> dictionary = new Dictionary<int, int>();
		for (int i = 0; i < hues.Length; i++)
		{
			if (dictionary.ContainsKey(hues[i]))
			{
				dictionary[hues[i]] = dictionary[hues[i]] + 1;
			}
			else
			{
				dictionary[hues[i]] = 1;
			}
		}
		int result = int.MinValue;
		int num = int.MinValue;
		foreach (int key in dictionary.Keys)
		{
			if (dictionary[key] > num)
			{
				num = dictionary[key];
				result = key;
			}
		}
		return result;
	}
}
