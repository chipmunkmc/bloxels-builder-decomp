using UnityEngine;
using UnityEngine.EventSystems;

public class PaintBoardTouchEvent : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	public enum TouchArea
	{
		Top = 0,
		Right = 1,
		Bottom = 2,
		Left = 3
	}

	public TouchArea zone;

	private bool isDragging;

	public void OnPointerClick(PointerEventData eData)
	{
		if (PixelEditorPaintBoard.instance.currentBloxelBoard != null)
		{
			switch (zone)
			{
			case TouchArea.Top:
				PixelEditorPaintBoard.instance.ShiftBlocksUp();
				break;
			case TouchArea.Right:
				PixelEditorPaintBoard.instance.ShiftBlocksRight();
				break;
			case TouchArea.Bottom:
				PixelEditorPaintBoard.instance.ShiftBlocksDown();
				break;
			case TouchArea.Left:
				PixelEditorPaintBoard.instance.ShiftBlocksLeft();
				break;
			}
		}
	}

	public void OnBeginDrag(PointerEventData eData)
	{
	}

	public void OnDrag(PointerEventData eData)
	{
	}

	public void OnEndDrag(PointerEventData eData)
	{
	}
}
