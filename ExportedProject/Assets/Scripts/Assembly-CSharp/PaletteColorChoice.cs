using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PaletteColorChoice : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	[HideInInspector]
	[SerializeField]
	private PaletteColor _color;

	public RectTransform rect;

	public Image image;

	public int idx;

	[ExposeProperty]
	public PaletteColor color
	{
		get
		{
			return _color;
		}
		set
		{
			_color = value;
			image.color = ColorUtilities.PaletteColorToUnityColor(value);
		}
	}

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		idx = base.transform.GetSiblingIndex();
		image = GetComponent<Image>();
	}

	public void ChangeActiveToolColor()
	{
		PixelEditorTool[] tools = PixelToolPalette.instance.tools;
		foreach (PixelEditorTool pixelEditorTool in tools)
		{
			if (pixelEditorTool.isActive && pixelEditorTool.toolID != BlockColor.Blank)
			{
				pixelEditorTool.toolColor = ColorUtilities.PaletteColorToUnityColor(color);
				pixelEditorTool.MovePaletteChoiceIndicator();
				SoundManager.PlayEventSound(SoundEvent.ColorSwatchChanged);
				if (PixelEditorPaintBoard.instance.currentBloxelBoard != null)
				{
					PixelEditorPaintBoard.instance.currentBloxelBoard.ChangePaletteChoice(pixelEditorTool.idx, pixelEditorTool.toolColor);
					ColorPalette.instance.SetPaletteTexCoordsForBoard(PixelEditorPaintBoard.instance.currentBloxelBoard);
				}
			}
		}
		PixelToolPalette.instance.isDefault = false;
	}

	public void OnPointerClick(PointerEventData pEvent)
	{
		ChangeActiveToolColor();
		PixelEditorPaintBoard.instance.UpdateBoardReferences();
		if ((PixelEditorController.instance.mode == CanvasMode.Animator || PixelEditorController.instance.mode == CanvasMode.CharacterBuilder) && AnimationTimeline.instance != null && AnimationTimeline.instance.currentFrame != null)
		{
			AnimationTimeline.instance.currentFrame.BuildCover();
		}
	}
}
