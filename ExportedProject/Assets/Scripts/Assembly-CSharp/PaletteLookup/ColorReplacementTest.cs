using System.IO;
using UnityEngine;
using UnityEngine.UI;

namespace PaletteLookup
{
	public class ColorReplacementTest : MonoBehaviour
	{
		public RawImage originalImage;

		public RawImage colorReplacementImage;

		public Texture2D atlasTexture;

		public Texture2D atlasOverlfowTexture;

		private void Awake()
		{
			originalImage.texture = CreateRandomGrayscaleTexture();
			colorReplacementImage.texture = originalImage.texture;
			byte[] data = new byte[atlasOverlfowTexture.width * atlasOverlfowTexture.height];
			atlasOverlfowTexture.LoadRawTextureData(data);
			atlasOverlfowTexture.Apply();
			byte[] data2 = new byte[atlasTexture.width * atlasTexture.height];
			atlasTexture.LoadRawTextureData(data2);
			atlasTexture.Apply();
		}

		private void CreateAtlasTexture(int size, string name)
		{
			Texture2D texture2D = new Texture2D(size, size, TextureFormat.Alpha8, false);
			texture2D.filterMode = FilterMode.Point;
			texture2D.wrapMode = TextureWrapMode.Clamp;
			byte[] bytes = texture2D.EncodeToPNG();
			Object.Destroy(texture2D);
			File.WriteAllBytes(Application.dataPath + "/PaletteLookup/Textures/" + name + ".png", bytes);
		}

		private Texture2D CreateRandomGrayscaleTexture()
		{
			int num = 8;
			int num2 = 8;
			Texture2D texture2D = new Texture2D(num, num2, TextureFormat.Alpha8, false);
			texture2D.filterMode = FilterMode.Point;
			texture2D.wrapMode = TextureWrapMode.Clamp;
			byte[] array = new byte[num * num2];
			for (int i = 0; i < array.Length; i++)
			{
				array[i] = (byte)Random.Range(0, 66);
			}
			texture2D.LoadRawTextureData(array);
			texture2D.Apply();
			return texture2D;
		}
	}
}
