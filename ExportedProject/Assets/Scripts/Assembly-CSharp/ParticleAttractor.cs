using System;
using UnityEngine;

public sealed class ParticleAttractor : PoolableComponent
{
	private Transform _targetTransform;

	private Vector3 _targetOffset;

	private ParticleSystem _particleSystem;

	private ParticleSystem.TextureSheetAnimationModule _animationModule;

	private static ParticleSystem.Particle[] _particles = new ParticleSystem.Particle[100];

	private static float _maxRotation = (float)Math.PI / 30f;

	private float _lifetime = 5f;

	private float _expirationTime;

	private bool _useSoundEffects;

	private AudioClip _currentSoundEffect;

	private int _previousParticleCount;

	public override void EarlyAwake()
	{
		_particleSystem = GetComponent<ParticleSystem>();
		_animationModule = _particleSystem.textureSheetAnimation;
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		selfTransform.parent = WorldWrapper.instance.runtimeObjectContainer;
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		base.gameObject.SetActive(false);
	}

	public override void Despawn()
	{
		GameplayPool.DespawnAttractor(this);
	}

	public void TakeFromPlayer(BloxelEnemyTile enemy, BonusType type, int particleCount, Transform targetTransform, Vector3 targetOffset)
	{
		_useSoundEffects = false;
		_targetTransform = targetTransform;
		_targetOffset = targetOffset;
		_particleSystem.Clear();
		switch (type)
		{
		case BonusType.Health:
			_animationModule.frameOverTime = new ParticleSystem.MinMaxCurve(0f);
			enemy.DisplayHealthIncrease(particleCount);
			break;
		case BonusType.Coin:
			particleCount = GameHUD.instance.RemoveCoins(particleCount);
			_animationModule.frameOverTime = new ParticleSystem.MinMaxCurve(1.5f);
			GameplayController.gameState[enemy.tileInfo.xTileDataIndex, enemy.tileInfo.yTileDataIndex].stolenCoins += particleCount;
			break;
		}
		_particleSystem.startSpeed = Mathf.Clamp(enemy.rigidbodyComponent.velocity.magnitude, 40f, 500f);
		_particleSystem.Emit(particleCount);
		SetParticleLifetime();
		_expirationTime = Time.time + _lifetime;
	}

	public void GiveCoinsToPlayer(BloxelEnemyTile enemy, int particleCount, Transform targetTransform, Vector3 targetOffset)
	{
		_useSoundEffects = true;
		_currentSoundEffect = SoundManager.instance.coinSFX;
		_targetTransform = targetTransform;
		_targetOffset = targetOffset;
		_particleSystem.Clear();
		GameHUD.instance.CollectCoins(particleCount);
		if (particleCount > 100)
		{
			particleCount = 100;
		}
		_animationModule.frameOverTime = new ParticleSystem.MinMaxCurve(1.5f);
		_particleSystem.startSpeed = Mathf.Clamp(enemy.rigidbodyComponent.velocity.magnitude, 40f, 500f);
		_particleSystem.Emit(particleCount);
		SetParticleLifetime();
		_expirationTime = Time.time + _lifetime;
	}

	public void GiveHealthToPlayer(BloxelEnemyTile enemy, int particleCount, Transform targetTransform, Vector3 targetOffset)
	{
		_useSoundEffects = true;
		_currentSoundEffect = SoundManager.instance.powerUpHealthSFX;
		_targetTransform = targetTransform;
		_targetOffset = targetOffset;
		_particleSystem.Clear();
		int num = Mathf.Clamp(particleCount, 0, PixelPlayerController.instance.maxHealth - PixelPlayerController.instance.health);
		PixelPlayerController.instance.health += num;
		if ((bool)GameHUD.instance)
		{
			GameHUD.instance.AddHearts(num);
		}
		if (particleCount > 100)
		{
			particleCount = 100;
		}
		_animationModule.frameOverTime = new ParticleSystem.MinMaxCurve(0f);
		_particleSystem.startSpeed = Mathf.Clamp(enemy.rigidbodyComponent.velocity.magnitude, 40f, 500f);
		_particleSystem.Emit(particleCount);
		SetParticleLifetime();
		_expirationTime = Time.time + _lifetime;
	}

	private void SetParticleLifetime()
	{
		_lifetime = Mathf.Clamp((selfTransform.position - _targetTransform.position).magnitude / _particleSystem.startSpeed, 1f, 10f);
		float lifetime = _lifetime;
		_lifetime *= 1.5f;
		int particles = _particleSystem.GetParticles(_particles);
		for (int i = 0; i < particles; i++)
		{
			_particles[i].startLifetime = UnityEngine.Random.Range(lifetime, _lifetime);
		}
		_particleSystem.SetParticles(_particles, particles);
		_previousParticleCount = particles;
	}

	private void Update()
	{
		if (Time.time > _expirationTime)
		{
			Despawn();
		}
		float maxRadiansDelta = (1f - (_expirationTime - Time.time) / _lifetime) * ((float)Math.PI / 6f) + _maxRotation;
		Vector3 position = _targetTransform.position;
		Vector3 vector = new Vector3(position.x, position.y, WorldWrapper.WorldZ) + _targetOffset * _targetTransform.localScale.y;
		int particles = _particleSystem.GetParticles(_particles);
		for (int i = 0; i < particles; i++)
		{
			ParticleSystem.Particle particle = _particles[i];
			Vector3 target = vector - particle.position;
			particle.velocity = Vector3.RotateTowards(particle.velocity, target, maxRadiansDelta, 0f);
			_particles[i] = particle;
		}
		_particleSystem.SetParticles(_particles, particles);
		if (_useSoundEffects && particles < _previousParticleCount)
		{
			SoundManager.PlayOneShot(_currentSoundEffect);
			_previousParticleCount = particles;
		}
	}
}
