using System.Collections;
using UnityEngine;
using Util;

public class PerlinShake : MonoBehaviour
{
	public float duration = 0.5f;

	public float speed = 1f;

	public float magnitude = 0.1f;

	public float elapsedTime;

	private Coroutine _shakeRoutine;

	private void Start()
	{
		_shakeRoutine = null;
	}

	public void PlayShake()
	{
		elapsedTime = 0f;
		if (_shakeRoutine == null)
		{
			_shakeRoutine = StartCoroutine(Shake());
		}
	}

	private IEnumerator Shake()
	{
		float playerScale = 1f;
		if ((bool)PixelPlayerController.instance)
		{
			playerScale = PixelPlayerController.instance.selfTransform.localScale.z;
		}
		float randomStart = Random.Range(-1000f, 1000f);
		while (elapsedTime < duration)
		{
			if (Time.timeScale == 0f)
			{
				yield return null;
				continue;
			}
			elapsedTime += Time.deltaTime;
			float percentComplete = elapsedTime / duration;
			float damper = 1f - Mathf.Clamp(2f * percentComplete - 1f, 0f, 1f);
			float alpha = randomStart + speed * percentComplete;
			float x2 = Noise.GetNoise(alpha, 0.0, 0.0) * 2f - 1f;
			float y2 = Noise.GetNoise(0.0, alpha, 0.0) * 2f - 1f;
			x2 *= magnitude * playerScale * damper;
			y2 *= magnitude * playerScale * damper;
			BloxelCamera.instance.selfTransform.position += new Vector3(x2, y2, 0f);
			yield return null;
		}
		_shakeRoutine = null;
	}
}
