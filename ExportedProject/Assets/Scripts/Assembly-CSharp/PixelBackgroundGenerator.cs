using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PixelBackgroundGenerator : MonoBehaviour
{
	private RawImage spriteRenderer;

	private Sprite backgroundSprite;

	public int numPixelsX;

	public int numPixelsY;

	private Texture2D pixelTexture;

	private Color[] colorPixels;

	public float colorDeviation;

	private Color gradientTop;

	private Color gradientBottom;

	public Color targetTopColor;

	public Color targetBottomColor;

	public Color[] possibleColors;

	public float transitionDuration;

	public AnimationCurve curve;

	private float elapsed;

	public bool useRandomColors;

	private void Awake()
	{
		spriteRenderer = GetComponent<RawImage>();
		pixelTexture = new Texture2D(numPixelsX, numPixelsY);
		pixelTexture.filterMode = FilterMode.Point;
		colorPixels = new Color[numPixelsX * numPixelsY];
		spriteRenderer.texture = pixelTexture;
	}

	private void Start()
	{
		if (useRandomColors)
		{
			gradientTop = possibleColors[Random.Range(0, possibleColors.Length)];
			gradientBottom = possibleColors[Random.Range(0, possibleColors.Length)];
			SetGradientPixels(gradientTop, gradientBottom);
		}
		else
		{
			SetGradientPixels(targetTopColor, targetBottomColor);
		}
	}

	private void SetGradientPixels(Color topColor, Color bottomColor)
	{
		for (int i = 0; i < pixelTexture.height; i++)
		{
			Color color = Color.Lerp(bottomColor, topColor, Mathf.Clamp((float)i / (float)(pixelTexture.height - 1), 0f, 1f));
			for (int j = 0; j < pixelTexture.width; j++)
			{
				float num = Random.Range(0f - colorDeviation, colorDeviation);
				colorPixels[i * pixelTexture.width + j] = new Color(color.r + num, color.g + num, color.b + num, 1f);
			}
		}
		pixelTexture.SetPixels(colorPixels);
		pixelTexture.Apply();
	}

	private void Update()
	{
		if (useRandomColors && Input.GetKeyDown(KeyCode.Space))
		{
			SwitchColors();
		}
	}

	private void SwitchColors(Color? newActiveColor = null)
	{
		targetTopColor = gradientBottom;
		if (!newActiveColor.HasValue)
		{
			newActiveColor = possibleColors[Random.Range(0, possibleColors.Length)];
		}
		targetBottomColor = newActiveColor.Value;
		DOTween.To(() => elapsed, delegate(float x)
		{
			elapsed = x;
		}, 1f, transitionDuration).SetEase(curve).OnUpdate(delegate
		{
			SetGradientPixels(Color.Lerp(gradientTop, targetTopColor, elapsed), Color.Lerp(gradientBottom, targetBottomColor, elapsed));
		})
			.OnComplete(delegate
			{
				gradientTop = targetTopColor;
				gradientBottom = targetBottomColor;
				elapsed = 0f;
			});
	}
}
