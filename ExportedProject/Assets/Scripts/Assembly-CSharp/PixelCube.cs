using UnityEngine;

public class PixelCube : MonoBehaviour
{
	public MeshRenderer meshRenderer;

	public Rigidbody rigidbody;

	public float explosionIntensity = 100f;

	public float torqueIntensity = 20f;

	private Vector3 explosionForce;

	private Vector3 explosionTorque;

	public float forceMultiplier = 1f;

	private void Awake()
	{
		meshRenderer = GetComponent<MeshRenderer>();
		rigidbody = GetComponent<Rigidbody>();
	}

	public void Explode()
	{
		explosionForce = Random.insideUnitSphere * Random.Range(1f, explosionIntensity);
		explosionTorque = Random.insideUnitSphere * Random.Range(1f, torqueIntensity);
		if (meshRenderer.material.color.a == 0f)
		{
			base.gameObject.SetActive(false);
			return;
		}
		rigidbody.isKinematic = false;
		rigidbody.AddForce(explosionForce * forceMultiplier, ForceMode.Impulse);
		rigidbody.AddTorque(explosionTorque, ForceMode.Impulse);
		Object.Destroy(base.gameObject, Random.Range(1.5f, 4.5f));
	}
}
