using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PixelEditorController : MonoBehaviour
{
	public delegate void HandleEditorModeChange(CanvasMode _mode);

	public static PixelEditorController instance;

	public RectTransform rectGemBank;

	public Transform primaryCanvas;

	public Transform overlayCanvas;

	public PixelEditorTool[] tools;

	public CanvasGroup modeButtonGroup;

	public CanvasGroup primaryCanvasGroup;

	public UIModeToggle[] modeToggles;

	private UnityEngine.Object _popupMessagePrefab;

	private UnityEngine.Object _itemDetailsPrefab;

	public UnityEngine.Object boardActivationPrefab;

	public BloxelBoard copyBoardBuffer;

	public GameObject hoverBoard;

	public CanvasMode mode;

	public Vector2 captureHiddenPosition;

	public Vector2 captureVisiblePosition;

	public bool captureVisible;

	public bool isHoldingCommand;

	public bool isHoldingOption;

	public bool isHoldingControl;

	public bool isHoldingShift;

	public UIButton uiButtonAddItem;

	public Button uiButtonHelp;

	public Button uiButtonChallenges;

	public GameObject tapIndicator;

	public UnityEngine.Object tapIndicatorPrefab;

	public UnityEngine.Object uiTipPrefab;

	public UnityEngine.Object challengePrefab;

	public event HandleEditorModeChange OnEditorModeChange;

	private void Awake()
	{
		Resources.UnloadUnusedAssets();
		if (instance == null)
		{
			instance = this;
		}
		_itemDetailsPrefab = Resources.Load("Prefabs/UIPopupLibraryItem");
		_popupMessagePrefab = Resources.Load("Prefabs/UIPopupMessage");
		boardActivationPrefab = Resources.Load("Prefabs/UIPopupBoardActivation");
		tapIndicatorPrefab = Resources.Load("Prefabs/UI/TapIndicator");
		challengePrefab = Resources.Load("Prefabs/UI/UIPopupChallenges");
		uiTipPrefab = Resources.Load("Prefabs/UITip");
		modeToggles = modeButtonGroup.GetComponentsInChildren<UIModeToggle>();
	}

	private void Start()
	{
		AppStateManager.instance.SetCurrentScene(SceneName.PixelEditor_iPad);
		AnalyticsManager.SendEventString(AnalyticsManager.Event.SceneOpen, AppStateManager.instance.currentScene.ToString());
		SoundManager.instance.PlayMusic(SoundManager.instance.editorMusic);
		tools = PixelToolPalette.instance.primaryTools.GetComponentsInChildren<PixelEditorTool>();
		PixelEditorPaintBoard.instance.OnTilePointerClick += OnPixelEditorPointerClick;
		PixelEditorPaintBoard.instance.OnTileDrag += OnPixelEditorDrag;
		PixelEditorPaintBoard.instance.OnTileBeginDrag += OnPixelEditorBeginDrag;
		PixelEditorPaintBoard.instance.OnTileEndDrag += OnPixelEditorEndDrag;
		PixelEditorPaintBoard.instance.OnTilePointerEnter += OnPixelEditorPointerEnter;
		PixelEditorPaintBoard.instance.OnTilePointerExit += OnPixelEditorPointerExit;
		BloxelMegaBoardLibrary.instance.OnAddButtonPressed += HandleAddMegaBoard;
		PixelToolPalette.instance.OnChange += HandleToolPaletteVisibility;
		uiButtonAddItem.OnClick += AddItem;
		uiButtonChallenges.onClick.AddListener(ShowChallengeMenu);
		CaptureManager.instance.OnCaptureConfirm += CaptureToProject;
		Init();
		mode = CanvasMode.LevelEditor;
		UIHelpLink.instance.location = UIHelpLink.Location.Games;
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.None);
		if (BloxelLibrary.instance.initComplete)
		{
			BloxelLibrary.instance.SwitchTabWindows(0);
		}
		BloxelLibrary.instance.SetPullTabSpriteForEditor();
		ChallengeManager.instance.InitForEditor();
		StartCoroutine(CheckPrefs());
	}

	private void OnDestroy()
	{
		uiButtonAddItem.OnClick -= AddItem;
		PixelEditorPaintBoard.instance.OnTilePointerClick -= OnPixelEditorPointerClick;
		PixelEditorPaintBoard.instance.OnTileDrag -= OnPixelEditorDrag;
		PixelEditorPaintBoard.instance.OnTileBeginDrag -= OnPixelEditorBeginDrag;
		PixelEditorPaintBoard.instance.OnTileEndDrag -= OnPixelEditorEndDrag;
		PixelEditorPaintBoard.instance.OnTilePointerEnter -= OnPixelEditorPointerEnter;
		PixelEditorPaintBoard.instance.OnTilePointerExit -= OnPixelEditorPointerExit;
		BloxelMegaBoardLibrary.instance.OnAddButtonPressed -= HandleAddMegaBoard;
		PixelToolPalette.instance.OnChange -= HandleToolPaletteVisibility;
		CaptureManager.instance.OnCaptureConfirm -= CaptureToProject;
		Unload();
	}

	private void Unload()
	{
		instance = null;
		primaryCanvas = null;
		overlayCanvas = null;
		tools = null;
		modeButtonGroup = null;
		primaryCanvasGroup = null;
		modeToggles = null;
		_popupMessagePrefab = null;
		_itemDetailsPrefab = null;
		boardActivationPrefab = null;
		copyBoardBuffer = null;
		hoverBoard = null;
		uiButtonAddItem = null;
		uiButtonHelp = null;
		uiButtonChallenges = null;
		tapIndicator = null;
		tapIndicatorPrefab = null;
		uiTipPrefab = null;
		challengePrefab = null;
	}

	private IEnumerator CheckPrefs()
	{
		if (!PrefsManager.instance.hasSeenEditor)
		{
			PrefsManager.instance.hasSeenEditor = true;
		}
		yield return new WaitForSeconds(0.5f);
		if (!CurrentUser.instance.IsEduUser() && !AppStateManager.instance.userOwnsBoard)
		{
			UIOverlayCanvas.instance.GameBoardActivation();
		}
		else if (!PrefsManager.instance.hasSeenGemInfo)
		{
			if (!CurrentUser.instance.IsEduUser())
			{
				UIOverlayCanvas.instance.Popup(UIOverlayCanvas.instance.gemInfoPrefab);
			}
			PrefsManager.instance.hasSeenGemInfo = true;
		}
	}

	public void FadeModeButtonGroup(bool visible)
	{
		modeButtonGroup.blocksRaycasts = visible;
		modeButtonGroup.interactable = visible;
		int num = (visible ? 1 : 0);
		modeButtonGroup.DOFade(num, UIAnimationManager.speedMedium);
	}

	public void ShowChallengeMenu()
	{
		ChallengeManager.instance.ShowChallengeMenu();
	}

	public void ShowGemBank(bool shouldShow)
	{
		if (shouldShow)
		{
			rectGemBank.DOScale(1f, UIAnimationManager.speedFast);
		}
		else
		{
			rectGemBank.DOScale(0f, UIAnimationManager.speedFast);
		}
	}

	private void Init()
	{
		UITab[] tabsInGroup = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
		foreach (UITab uITab in tabsInGroup)
		{
			uITab.CheckAvailability(mode);
		}
		PixelToolPalette.instance.locked = true;
		BoardCanvas.instance.Hide();
		WorldWrapper.instance.Hide();
		MegaBoardCanvas.instance.Hide();
		AnimationCanvas.instance.Hide();
	}

	private void HandleToolPaletteVisibility(bool _isOpen)
	{
		FadeModeButtonGroup(!_isOpen);
	}

	private void HandleAddMegaBoard()
	{
		SwitchCanvasMode(CanvasMode.MegaBoard);
	}

	public void AddItem()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		switch (mode)
		{
		case CanvasMode.PaintBoard:
			BloxelBoardLibrary.instance.AddButtonPressed();
			break;
		case CanvasMode.Animator:
			BloxelAnimationLibrary.instance.AddButtonPressed();
			break;
		case CanvasMode.MegaBoard:
			BloxelMegaBoardLibrary.instance.AddButtonPressed();
			break;
		case CanvasMode.CharacterBuilder:
			BloxelCharacterLibrary.instance.AddButtonPressed();
			break;
		case CanvasMode.LevelEditor:
			BloxelGameLibrary.instance.AddButtonPressed();
			break;
		}
	}

	public void CaptureToProject()
	{
		if (CaptureManager.instance.gameboardActivationInProgress)
		{
			return;
		}
		bool flag = false;
		captureVisible = false;
		switch (mode)
		{
		case CanvasMode.PaintBoard:
			flag = true;
			break;
		case CanvasMode.Animator:
			flag = true;
			AnimationCanvas.instance.Move(Direction.Up, Vector2.zero);
			AnimationTimeline.instance.Move(Direction.Up, Vector2.zero);
			break;
		case CanvasMode.CharacterBuilder:
			flag = true;
			CharacterBuilderCanvas.instance.Move(Direction.Up, Vector2.zero);
			AnimationTimeline.instance.Move(Direction.Up, Vector2.zero);
			break;
		case CanvasMode.LevelEditor:
			if (GameBuilderCanvas.instance.currentMode != GameBuilderMode.QuickStart)
			{
				if (GameBuilderCanvas.instance.currentMode != GameBuilderMode.Build && GameBuilderCanvas.instance.currentMode != GameBuilderMode.Wireframe)
				{
					return;
				}
				GameBuilderCanvas.instance.ToggleModeContainerVisibility(true);
				GameBuilderCanvas.instance.currentBloxelLevel.coverBoard.blockColors = CaptureManager.latest;
				GameBuilderCanvas.instance.currentBloxelLevel.coverBoard.BlastUpdate();
				GameplayBuilder.instance.Reset();
				GameBuilderCanvas.instance.mapModeToggle.gameObject.SetActive(true);
				flag = true;
			}
			else
			{
				GameQuickStartMenu.instance.Show();
				GameBuilderCanvas.instance.ToggleModeContainerVisibility(false);
				GameQuickStartMenu.instance.CaptureComplete(CaptureManager.latest);
			}
			break;
		}
		if (flag)
		{
			PixelEditorPaintBoard.instance.Show();
			PixelEditorPaintBoard.instance.currentBloxelBoard.ResetPaletteChoices();
			PixelEditorPaintBoard.instance.UpdateFromBlockColors(CaptureManager.latest);
		}
	}

	private void ActivateModeToggle(CanvasMode mode)
	{
		UIModeToggle[] array = modeToggles;
		foreach (UIModeToggle uIModeToggle in array)
		{
			if (uIModeToggle.mode == mode)
			{
				uIModeToggle.isActive = true;
			}
			else
			{
				uIModeToggle.isActive = false;
			}
		}
	}

	public void SwitchCanvasMode(CanvasMode _mode)
	{
		mode = _mode;
		SoundManager.PlaySoundClip(SoundClip.sweepA);
		ActivateModeToggle(mode);
		EditorBackgroundController.instance.SwitchBackground(_mode);
		BoardZoomManager.instance.StartCanZoomBuffer(1f);
		if (ScrollPickerManager.instance.uiColorPicker.isVisible)
		{
			ScrollPickerManager.instance.uiColorPicker.Hide();
		}
		UITab[] tabsInGroup = BloxelLibrary.instance.uiTabGroup.tabsInGroup;
		foreach (UITab uITab in tabsInGroup)
		{
			uITab.CheckAvailability(mode);
		}
		switch (mode)
		{
		case CanvasMode.LevelEditor:
			if (GameBuilderCanvas.instance.currentGame != null)
			{
				LevelBuilder.instance.SetupGameBackground(GameBuilderCanvas.instance.currentGame);
			}
			UIHelpLink.instance.location = UIHelpLink.Location.Games;
			PixelToolPalette.instance.locked = true;
			BloxelLibrary.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(0);
			BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.GameBuilder);
			BoardCanvas.instance.Hide();
			if (GameBuilderCanvas.instance.currentMode != GameBuilderMode.Map)
			{
				MegaBoardCanvas.instance.Hide();
			}
			CharacterBuilderCanvas.instance.Hide();
			AnimationCanvas.instance.Hide();
			GameBuilderCanvas.instance.Show();
			MegaBoardCanvas.instance.ToggleScrollable(true);
			break;
		case CanvasMode.Animator:
			UIHelpLink.instance.location = UIHelpLink.Location.Animations;
			PixelToolPalette.instance.locked = false;
			BloxelLibrary.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(1);
			CharacterBuilderCanvas.instance.Hide();
			WorldWrapper.instance.Hide();
			GameBuilderCanvas.instance.Hide();
			BoardCanvas.instance.Hide();
			MegaBoardCanvas.instance.Hide();
			AnimationCanvas.instance.Show();
			BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.None);
			break;
		case CanvasMode.MegaBoard:
			UIHelpLink.instance.location = UIHelpLink.Location.Backgrounds;
			PixelToolPalette.instance.locked = false;
			BloxelLibrary.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(2);
			WorldWrapper.instance.Hide();
			GameBuilderCanvas.instance.Hide();
			BoardCanvas.instance.Hide();
			AnimationCanvas.instance.Hide();
			CharacterBuilderCanvas.instance.Hide();
			MegaBoardCanvas.instance.Show();
			MegaBoardCanvas.instance.Init();
			BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.None);
			MegaBoardCanvas.instance.ToggleScrollable(false);
			break;
		case CanvasMode.CharacterBuilder:
			UIHelpLink.instance.location = UIHelpLink.Location.Characters;
			PixelToolPalette.instance.locked = false;
			BloxelLibrary.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(3);
			BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.CharacterConfig);
			BoardCanvas.instance.Hide();
			MegaBoardCanvas.instance.Hide();
			AnimationCanvas.instance.Hide();
			WorldWrapper.instance.Hide();
			GameBuilderCanvas.instance.Hide();
			CharacterBuilderCanvas.instance.Show();
			break;
		case CanvasMode.PaintBoard:
			UIHelpLink.instance.location = UIHelpLink.Location.Boards;
			PixelToolPalette.instance.locked = false;
			BloxelLibrary.instance.Show();
			BloxelLibrary.instance.SwitchTabWindows(4);
			CharacterBuilderCanvas.instance.Hide();
			GameBuilderCanvas.instance.Hide();
			WorldWrapper.instance.Hide();
			MegaBoardCanvas.instance.Hide();
			AnimationCanvas.instance.Hide();
			PixelEditorPaintBoard.instance.Clear();
			PixelToolPalette.instance.Reset();
			BoardCanvas.instance.Show();
			BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.None);
			break;
		}
		if (this.OnEditorModeChange != null)
		{
			this.OnEditorModeChange(mode);
		}
	}

	private void OnPixelEditorPointerClick(PixelEditorTile tile)
	{
		if (tile.currentColor != PixelToolPalette.instance.activeEditorTool.toolID)
		{
			if (instance.mode == CanvasMode.LevelEditor)
			{
				LevelBuilder.instance.UpdateChunkFromWireframe(PixelToolPalette.instance.activeEditorTool.toolID, tile.loc, true);
				PixelEditorPaintBoard.instance.PaintColor(tile, false);
				GameBuilderCanvas.instance.currentBloxelLevel.Save(false);
			}
			else if (PixelEditorPaintBoard.instance.currentBloxelBoard != null)
			{
				PixelEditorPaintBoard.instance.PaintColor(tile, false);
				PixelEditorPaintBoard.instance.currentBloxelBoard.Save();
			}
		}
	}

	private void OnPixelEditorBeginDrag(PixelEditorTile tile)
	{
		if (tile.currentColor != PixelToolPalette.instance.activeEditorTool.toolID)
		{
			if (instance.mode == CanvasMode.LevelEditor)
			{
				LevelBuilder.instance.UpdateChunkFromWireframe(PixelToolPalette.instance.activeEditorTool.toolID, tile.loc, true);
			}
			PixelEditorPaintBoard.instance.PaintColor(tile, false);
		}
	}

	private void OnPixelEditorDrag(PixelEditorTile tile)
	{
	}

	private void OnPixelEditorEndDrag(PixelEditorTile tile)
	{
		if (instance.mode == CanvasMode.LevelEditor)
		{
			GameBuilderCanvas.instance.currentBloxelLevel.Save(false);
		}
		else if (PixelEditorPaintBoard.instance.currentBloxelBoard != null)
		{
			PixelEditorPaintBoard.instance.currentBloxelBoard.Save();
		}
	}

	private void OnPixelEditorPointerEnter(PixelEditorTile tile)
	{
		if (PixelEditorPaintBoard.instance.isPainting)
		{
			if (instance.mode == CanvasMode.LevelEditor)
			{
				LevelBuilder.instance.UpdateChunkFromWireframe(PixelToolPalette.instance.activeEditorTool.toolID, tile.loc, true);
			}
			PixelEditorPaintBoard.instance.PaintColor(tile, false);
		}
	}

	private void OnPixelEditorPointerExit(PixelEditorTile tile)
	{
	}
}
