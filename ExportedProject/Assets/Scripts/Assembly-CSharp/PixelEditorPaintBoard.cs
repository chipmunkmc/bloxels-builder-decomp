using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PixelEditorPaintBoard : UIMoveable
{
	[Serializable]
	public struct BoardState
	{
		public BlockColor[,] blockColors;

		public Color[] palette;

		public static BoardState Empty = new BoardState(null, null);

		public BoardState(BlockColor[,] c, Color[] p)
		{
			blockColors = c;
			palette = p;
		}
	}

	public delegate void HandleBoardChange(BloxelBoard board);

	public delegate void HandleFlipBlocks();

	public delegate void HandleShiftBlocks();

	public delegate void HandleOnPointerClick(PixelEditorTile tile);

	public delegate void HandleOnDrag(PixelEditorTile tile);

	public delegate void HandleOnBeginDrag(PixelEditorTile tile);

	public delegate void HandleOnEndDrag(PixelEditorTile tile);

	public delegate void HandleOnPointerEnter(PixelEditorTile tile);

	public delegate void HandleOnPointerExit(PixelEditorTile tile);

	public delegate void HandleOnPixelTileChange(PixelEditorTile tile);

	public static PixelEditorPaintBoard instance;

	private BloxelBoard _currentBloxelBoard;

	public PixelEditorTile[] tiles;

	public string currentBoardID;

	private History<BoardState> undoStack;

	private History<BoardState> redoStack;

	private int bufferLimit = 100;

	private UnityEngine.Object tilePrefab;

	public GameObject tileOutput;

	public Color[] currentPalette;

	public Vector2 leftPosition;

	public Vector2 startingPosition;

	public Vector3 scaleForCapture;

	public Vector2 anchorPosForCapture;

	public float animationDuration = 0.2f;

	[Header("| ========= State ========= |")]
	public bool isPainting;

	[Header("| ========= Sizing ========= |")]
	public float totalBoardUnits = 862f;

	public float tileGutter;

	public float tileSize = 51f;

	private float boardGutter;

	[Header("| ========= UI ========= |")]
	public Text uiTextCopied;

	public Text uiTextPasted;

	public Button uiButtonShiftUp;

	public Button uiButtonShiftRight;

	public Button uiButtonShiftDown;

	public Button uiButtonShiftLeft;

	public Button uiButtonFlipHorizontal;

	public Button uiButtonFlipVertical;

	public GameObject uiPaintBoardToolsContainer;

	public UIMoveable characterBuilderTools;

	public UILineRenderer boundaryTop;

	public UILineRenderer boundaryRight;

	public UILineRenderer boundaryBottom;

	public UILineRenderer boundaryLeft;

	public BloxelBoard currentBloxelBoard
	{
		get
		{
			return _currentBloxelBoard;
		}
		set
		{
			_currentBloxelBoard = value;
			if (this.OnBoardChange != null && _currentBloxelBoard != null)
			{
				this.OnBoardChange(_currentBloxelBoard);
			}
		}
	}

	public event HandleBoardChange OnBoardChange;

	public event HandleFlipBlocks OnFlip;

	public event HandleShiftBlocks OnShift;

	public event HandleOnPointerClick OnTilePointerClick;

	public event HandleOnDrag OnTileDrag;

	public event HandleOnBeginDrag OnTileBeginDrag;

	public event HandleOnEndDrag OnTileEndDrag;

	public event HandleOnPointerEnter OnTilePointerEnter;

	public event HandleOnPointerExit OnTilePointerExit;

	public event HandleOnPixelTileChange OnPixelTileChange;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		tilePrefab = Resources.Load("Prefabs/PixelEditorTile");
		tiles = new PixelEditorTile[169];
		undoStack = new History<BoardState>();
		redoStack = new History<BoardState>();
		boardGutter = (totalBoardUnits - tileSize * 13f - tileGutter * 12f) / 2f;
		BuildUI();
		uiTextCopied.enabled = false;
		uiTextPasted.enabled = false;
		boundaryTop.DOFade(0f, 0f);
		boundaryRight.DOFade(0f, 0f);
		boundaryBottom.DOFade(0f, 0f);
		boundaryLeft.DOFade(0f, 0f);
	}

	private new void Start()
	{
		BloxelLibrary.instance.OnTabChange += HandleOnTabChange;
		BloxelBoardLibrary.instance.OnProjectSelect += HandleOnBoardChange;
		BloxelBoardLibrary.instance.OnProjectDelete += HandleOnProjectDelete;
		BloxelAnimationLibrary.instance.OnProjectDelete += HandleOnProjectDelete;
		BloxelCharacterLibrary.instance.OnProjectDelete += HandleOnProjectDelete;
		PixelEditorTile[] array = tiles;
		foreach (PixelEditorTile pixelEditorTile in array)
		{
			pixelEditorTile.OnTilePointerClick += OnPointerClick;
			pixelEditorTile.OnTileDrag += OnDrag;
			pixelEditorTile.OnTileBeginDrag += OnBeginDrag;
			pixelEditorTile.OnTileEndDrag += OnEndDrag;
			pixelEditorTile.OnTilePointerEnter += OnPointerEnter;
			pixelEditorTile.OnTilePointerExit += OnPointerExit;
			pixelEditorTile.OnTileChange += OnTileChange;
		}
		uiButtonShiftUp.onClick.AddListener(ShiftBlocksUp);
		uiButtonShiftRight.onClick.AddListener(ShiftBlocksRight);
		uiButtonShiftDown.onClick.AddListener(ShiftBlocksDown);
		uiButtonShiftLeft.onClick.AddListener(ShiftBlocksLeft);
		uiButtonFlipHorizontal.onClick.AddListener(FlipBoardHorizontally);
		uiButtonFlipVertical.onClick.AddListener(FlipBoardVertically);
	}

	private void OnDestroy()
	{
		BloxelLibrary.instance.OnTabChange -= HandleOnTabChange;
		BloxelBoardLibrary.instance.OnProjectSelect -= HandleOnBoardChange;
		BloxelBoardLibrary.instance.OnProjectDelete -= HandleOnProjectDelete;
		BloxelAnimationLibrary.instance.OnProjectDelete -= HandleOnProjectDelete;
		BloxelCharacterLibrary.instance.OnProjectDelete -= HandleOnProjectDelete;
		PixelEditorTile[] array = tiles;
		foreach (PixelEditorTile pixelEditorTile in array)
		{
			pixelEditorTile.OnTilePointerClick -= OnPointerClick;
			pixelEditorTile.OnTileDrag -= OnDrag;
			pixelEditorTile.OnTileBeginDrag -= OnBeginDrag;
			pixelEditorTile.OnTileEndDrag -= OnEndDrag;
			pixelEditorTile.OnTilePointerEnter -= OnPointerEnter;
			pixelEditorTile.OnTilePointerExit -= OnPointerExit;
			pixelEditorTile.OnTileChange -= OnTileChange;
		}
	}

	public new Tweener Show()
	{
		if (PixelEditorController.instance.mode != CanvasMode.LevelEditor)
		{
			uiPaintBoardToolsContainer.SetActive(true);
		}
		else
		{
			uiPaintBoardToolsContainer.SetActive(false);
		}
		return base.Show();
	}

	public new Tweener Hide()
	{
		uiTextCopied.enabled = false;
		uiTextPasted.enabled = false;
		return base.Hide();
	}

	private void HandleOnProjectDelete(SavedProject item)
	{
		if (item.isActive)
		{
			Clear();
		}
	}

	private void OnTileChange(PixelEditorTile tile)
	{
		if (this.OnPixelTileChange != null && isVisible)
		{
			this.OnPixelTileChange(tile);
		}
	}

	private void HandleOnBoardChange(SavedProject item)
	{
		if (PixelEditorController.instance.mode == CanvasMode.PaintBoard)
		{
			SwitchBloxelBoard(item.dataModel.coverBoard);
			undoStack.Clear();
			redoStack.Clear();
		}
	}

	private void HandleOnTabChange(int tabID)
	{
	}

	private BlockColor[,] GetBlankSet()
	{
		BlockColor[,] array = new BlockColor[13, 13];
		for (int i = 0; i < array.GetLength(0); i++)
		{
			for (int j = 0; j < array.GetLength(1); j++)
			{
				array[i, j] = BlockColor.Blank;
			}
		}
		return array;
	}

	private Color[] GetDefaultPalette()
	{
		return new Color[8]
		{
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Yellow),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Pink),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Purple),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.White)
		};
	}

	private void BuildUI()
	{
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				float x = (float)i * (tileSize + tileGutter) + boardGutter;
				float y = (float)j * (tileSize + tileGutter) + boardGutter;
				GameObject gameObject = UnityEngine.Object.Instantiate(tilePrefab) as GameObject;
				RectTransform component = gameObject.GetComponent<RectTransform>();
				PixelEditorTile component2 = gameObject.GetComponent<PixelEditorTile>();
				gameObject.transform.SetParent(tileOutput.transform);
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.localPosition = Vector3.zero;
				component.sizeDelta = new Vector2(tileSize, tileSize);
				component.anchoredPosition = new Vector2(x, y);
				component2.loc = new GridLocation(i, j);
				tiles[num] = component2;
				num++;
			}
		}
	}

	public void Clear(bool shouldChangeBoard = true)
	{
		if (shouldChangeBoard)
		{
			currentBloxelBoard = null;
		}
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				tiles[num].currentColor = BlockColor.Blank;
				tiles[num].ChangeColor(ColorUtilities.blankUnityColor);
				num++;
			}
		}
	}

	public void Create()
	{
		Clear();
		currentBloxelBoard = new BloxelBoard();
		BloxelBoardLibrary.instance.AddBoard(currentBloxelBoard);
	}

	public void UpdateFromBlockColors(BlockColor[,] blockColors)
	{
		if (currentBloxelBoard != null)
		{
			currentBloxelBoard.blockColors = blockColors;
			int num = 0;
			for (int i = 0; i < 13; i++)
			{
				for (int j = 0; j < 13; j++)
				{
					tiles[num].currentColor = currentBloxelBoard.blockColors[i, j];
					tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[i, j]);
					num++;
				}
			}
			for (int k = 0; k < currentBloxelBoard.toolPaletteChoices.Length; k++)
			{
				PixelToolPalette.instance.tools[k].toolColor = currentBloxelBoard.toolPaletteChoices[k];
			}
			currentPalette = currentBloxelBoard.toolPaletteChoices;
			UpdateColorsOnBoard();
		}
		if (PixelEditorController.instance.mode == CanvasMode.Animator)
		{
			AnimationTimeline.instance.UpdateFrames();
		}
	}

	public void AddBlockAtLocation(GridLocation location, BlockColor color, bool updateChunk = true)
	{
		if (currentBloxelBoard != null)
		{
			SoundManager.PlaySoundClip(SoundClip.drawOnBoard);
			int num = location.c * 13 + location.r;
			currentBloxelBoard.SetTileAtLocation(num, color, false);
			tiles[num].currentColor = color;
			tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(color);
			for (int i = 0; i < currentBloxelBoard.toolPaletteChoices.Length; i++)
			{
				PixelToolPalette.instance.tools[i].toolColor = currentBloxelBoard.toolPaletteChoices[i];
			}
			currentPalette = currentBloxelBoard.toolPaletteChoices;
			UpdateColorsOnBoard();
			if (updateChunk)
			{
				LevelBuilder.instance.UpdateChunkFromWireframe(color, location, true);
			}
		}
	}

	public void Copy()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		uiTextCopied.enabled = true;
		PixelEditorController.instance.copyBoardBuffer = currentBloxelBoard.Clone();
		StopCoroutine("RemoveMessages");
		StartCoroutine("RemoveMessages");
		uiTextPasted.enabled = false;
	}

	public void Paste()
	{
		if (PixelEditorController.instance.copyBoardBuffer == null || PixelEditorController.instance.copyBoardBuffer.blockColors == null)
		{
			SoundManager.PlayOneShot(SoundManager.instance.popB);
			return;
		}
		SoundManager.PlaySoundClip(SoundClip.paste);
		uiTextPasted.enabled = true;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				currentBloxelBoard.blockColors[j, i] = PixelEditorController.instance.copyBoardBuffer.blockColors[j, i];
			}
		}
		if (PixelEditorController.instance.mode != CanvasMode.LevelEditor)
		{
			Array.Copy(PixelEditorController.instance.copyBoardBuffer.toolPaletteChoices, currentBloxelBoard.toolPaletteChoices, currentBloxelBoard.toolPaletteChoices.Length);
		}
		SwitchBloxelBoard(_currentBloxelBoard);
		currentBloxelBoard.Save();
		ColorPalette.instance.SetPaletteTexCoordsForBoard(currentBloxelBoard);
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor && GameBuilderCanvas.instance.currentBloxelLevel.location == GameBuilderCanvas.instance.currentGame.heroStartPosition.levelLocationInWorld)
		{
			SavedGame selectedProject = BloxelLibraryScrollController.instance.GetSelectedProject<SavedGame>();
			if (selectedProject != null)
			{
				selectedProject.BuildCover();
			}
		}
		TimelineFrame currentFrame = AnimationTimeline.instance.currentFrame;
		if (AnimationCanvas.instance.isVisible && (bool)currentFrame)
		{
			for (int k = 0; k < 13; k++)
			{
				for (int l = 0; l < 13; l++)
				{
					currentFrame.dataModel.blockColors[l, k] = PixelEditorController.instance.copyBoardBuffer.blockColors[l, k];
				}
			}
			Array.Copy(PixelEditorController.instance.copyBoardBuffer.toolPaletteChoices, currentFrame.dataModel.toolPaletteChoices, currentFrame.dataModel.toolPaletteChoices.Length);
			currentFrame.dataModel.BlastUpdate();
			ColorPalette.instance.SetPaletteTexCoordsForBoard(currentFrame.dataModel);
			currentFrame.Select();
		}
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			GameplayBuilder.instance.ResetLevelAtLocation(GameBuilderCanvas.instance.currentBloxelLevel.location);
		}
		StopCoroutine("RemoveMessages");
		StartCoroutine("RemoveMessages");
		uiTextCopied.enabled = false;
	}

	private IEnumerator RemoveMessages()
	{
		yield return new WaitForSeconds(1.5f);
		uiTextCopied.enabled = false;
		uiTextPasted.enabled = false;
	}

	public void BoardUndo()
	{
		if (undoStack.Count() > 1)
		{
			BoardState boardState = undoStack.Pop();
			SetBoardFromState(boardState);
			redoStack.Push(boardState);
			PushToRedoStack(boardState);
		}
	}

	public void BoardRedo()
	{
		if (redoStack.Count() > 1)
		{
			BoardState boardState = redoStack.Pop();
			SetBoardFromState(boardState);
			PushToUndoStack(boardState);
		}
	}

	private void PushToRedoStack(BoardState state)
	{
		if (redoStack.Count() < bufferLimit)
		{
			redoStack.Push(state);
			return;
		}
		redoStack.RemoveAt(1);
		redoStack.Push(state);
	}

	private void PushToUndoStack(BoardState state)
	{
		if (undoStack.Count() < bufferLimit)
		{
			undoStack.Push(state);
			return;
		}
		undoStack.RemoveAt(1);
		undoStack.Push(state);
	}

	private void SetBoardFromState(BoardState state)
	{
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				tiles[num].currentColor = state.blockColors[i, j];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(state.blockColors[i, j]);
				currentBloxelBoard.blockColors[i, j] = state.blockColors[i, j];
				num++;
			}
		}
		int num2 = 0;
		PixelEditorTool[] tools = PixelToolPalette.instance.tools;
		foreach (PixelEditorTool pixelEditorTool in tools)
		{
			if (pixelEditorTool.toolID != BlockColor.Blank)
			{
				state.palette[num2] = pixelEditorTool.toolColor;
				currentBloxelBoard.toolPaletteChoices[num2] = pixelEditorTool.toolColor;
			}
			num2++;
		}
	}

	public void FlashBoundary(Direction direction)
	{
		UILineRenderer line = null;
		switch (direction)
		{
		case Direction.Up:
			line = boundaryTop;
			break;
		case Direction.Right:
			line = boundaryRight;
			break;
		case Direction.Down:
			line = boundaryBottom;
			break;
		case Direction.Left:
			line = boundaryLeft;
			break;
		}
		line.DOFade(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			line.DOFade(0f, UIAnimationManager.speedMedium);
		});
	}

	public void ShiftBlocksRight()
	{
		for (int i = 0; i < 13; i++)
		{
			if (currentBloxelBoard.blockColors[12, i] != BlockColor.Blank)
			{
				SoundManager.PlayOneShot(SoundManager.instance.popB);
				FlashBoundary(Direction.Right);
				return;
			}
		}
		SoundManager.PlayOneShot(SoundManager.instance.sweepA);
		BlockColor[,] blockColors = currentBloxelBoard.blockColors;
		BlockColor[,] array = new BlockColor[13, 13];
		for (int j = 0; j < 13; j++)
		{
			for (int k = 0; k < 13; k++)
			{
				if (j == 0)
				{
					array[j, k] = BlockColor.Blank;
				}
				else
				{
					array[j, k] = blockColors[j - 1, k];
				}
			}
		}
		currentBloxelBoard.blockColors = array;
		int num = 0;
		for (int l = 0; l < 13; l++)
		{
			for (int m = 0; m < 13; m++)
			{
				tiles[num].currentColor = currentBloxelBoard.blockColors[l, m];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[l, m]);
				num++;
			}
		}
		UpdateColorsOnBoard();
		if (this.OnShift != null)
		{
			this.OnShift();
		}
	}

	public void ShiftBlocksLeft()
	{
		for (int i = 0; i < 13; i++)
		{
			if (currentBloxelBoard.blockColors[0, i] != BlockColor.Blank)
			{
				SoundManager.PlayOneShot(SoundManager.instance.popB);
				FlashBoundary(Direction.Left);
				return;
			}
		}
		SoundManager.PlayOneShot(SoundManager.instance.sweepA);
		BlockColor[,] blockColors = currentBloxelBoard.blockColors;
		BlockColor[,] array = new BlockColor[13, 13];
		for (int j = 0; j < 13; j++)
		{
			for (int k = 0; k < 13; k++)
			{
				if (j == 12)
				{
					array[j, k] = BlockColor.Blank;
				}
				else
				{
					array[j, k] = blockColors[j + 1, k];
				}
			}
		}
		currentBloxelBoard.blockColors = array;
		int num = 0;
		for (int l = 0; l < 13; l++)
		{
			for (int m = 0; m < 13; m++)
			{
				tiles[num].currentColor = currentBloxelBoard.blockColors[l, m];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[l, m]);
				num++;
			}
		}
		UpdateColorsOnBoard();
		if (this.OnShift != null)
		{
			this.OnShift();
		}
	}

	public void ShiftBlocksUp()
	{
		for (int i = 0; i < 13; i++)
		{
			if (currentBloxelBoard.blockColors[i, 12] != BlockColor.Blank)
			{
				SoundManager.PlayOneShot(SoundManager.instance.popB);
				FlashBoundary(Direction.Up);
				return;
			}
		}
		SoundManager.PlayOneShot(SoundManager.instance.sweepA);
		BlockColor[,] blockColors = currentBloxelBoard.blockColors;
		BlockColor[,] array = new BlockColor[13, 13];
		for (int j = 0; j < 13; j++)
		{
			for (int k = 0; k < 13; k++)
			{
				if (k == 0)
				{
					array[j, k] = BlockColor.Blank;
				}
				else
				{
					array[j, k] = blockColors[j, k - 1];
				}
			}
		}
		currentBloxelBoard.blockColors = array;
		int num = 0;
		for (int l = 0; l < 13; l++)
		{
			for (int m = 0; m < 13; m++)
			{
				tiles[num].currentColor = currentBloxelBoard.blockColors[l, m];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[l, m]);
				num++;
			}
		}
		UpdateColorsOnBoard();
		if (this.OnShift != null)
		{
			this.OnShift();
		}
	}

	public void ShiftBlocksDown()
	{
		for (int i = 0; i < 13; i++)
		{
			if (currentBloxelBoard.blockColors[i, 0] != BlockColor.Blank)
			{
				SoundManager.PlayOneShot(SoundManager.instance.popB);
				FlashBoundary(Direction.Down);
				return;
			}
		}
		SoundManager.PlayOneShot(SoundManager.instance.sweepA);
		BlockColor[,] blockColors = currentBloxelBoard.blockColors;
		BlockColor[,] array = new BlockColor[13, 13];
		for (int j = 0; j < 13; j++)
		{
			for (int k = 0; k < 13; k++)
			{
				if (k == 12)
				{
					array[j, k] = BlockColor.Blank;
				}
				else
				{
					array[j, k] = blockColors[j, k + 1];
				}
			}
		}
		currentBloxelBoard.blockColors = array;
		int num = 0;
		for (int l = 0; l < 13; l++)
		{
			for (int m = 0; m < 13; m++)
			{
				tiles[num].currentColor = currentBloxelBoard.blockColors[l, m];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[l, m]);
				num++;
			}
		}
		UpdateColorsOnBoard();
		if (this.OnShift != null)
		{
			this.OnShift();
		}
	}

	public void FlipBoardVertically()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.flip);
		currentBloxelBoard.FlipVertically();
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				tiles[num].currentColor = currentBloxelBoard.blockColors[i, j];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[i, j]);
				num++;
			}
		}
		for (int k = 0; k < PixelToolPalette.instance.tools.Length; k++)
		{
			for (int l = 0; l < tiles.Length; l++)
			{
				if (PixelToolPalette.instance.tools[k].toolID == tiles[l].currentColor)
				{
					tiles[l].ChangeColor(PixelToolPalette.instance.tools[k].toolColor);
				}
			}
		}
		if (this.OnFlip != null)
		{
			this.OnFlip();
		}
	}

	public void FlipBoardHorizontally()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.flip);
		currentBloxelBoard.FlipHorizontally();
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				tiles[num].currentColor = currentBloxelBoard.blockColors[i, j];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[i, j]);
				num++;
			}
		}
		for (int k = 0; k < PixelToolPalette.instance.tools.Length; k++)
		{
			for (int l = 0; l < tiles.Length; l++)
			{
				if (PixelToolPalette.instance.tools[k].toolID == tiles[l].currentColor)
				{
					tiles[l].ChangeColor(PixelToolPalette.instance.tools[k].toolColor);
				}
			}
		}
		if (this.OnFlip != null)
		{
			this.OnFlip();
		}
	}

	public void UpdateBoardReferences()
	{
		PixelEditorTile[] array = tiles;
		foreach (PixelEditorTile pixelEditorTile in array)
		{
			if (pixelEditorTile.currentColor == PixelToolPalette.instance.activeEditorTool.toolID)
			{
				pixelEditorTile.image.color = PixelToolPalette.instance.activeEditorTool.toolColor;
			}
		}
	}

	public void UpdateColorsOnBoard(bool shouldSave = true)
	{
		for (int i = 0; i < PixelToolPalette.instance.tools.Length; i++)
		{
			for (int j = 0; j < tiles.Length; j++)
			{
				if (PixelToolPalette.instance.tools[i].toolID == tiles[j].currentColor)
				{
					tiles[j].ChangeColor(PixelToolPalette.instance.tools[i].toolColor);
				}
			}
		}
		if (shouldSave)
		{
			currentBloxelBoard.Save();
		}
		currentBloxelBoard.BlastUpdate();
	}

	public void SwitchBloxelBoard(BloxelBoard b)
	{
		currentBloxelBoard = b;
		currentBoardID = currentBloxelBoard.ID();
		int num = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				tiles[num].currentColor = currentBloxelBoard.blockColors[i, j];
				tiles[num].image.color = ColorUtilities.GetUnityColorFromBlockColor(currentBloxelBoard.blockColors[i, j]);
				num++;
			}
		}
		for (int k = 0; k < b.toolPaletteChoices.Length; k++)
		{
			PixelToolPalette.instance.tools[k].toolColor = b.toolPaletteChoices[k];
		}
		currentPalette = b.toolPaletteChoices;
		UpdateColorsOnBoard(false);
	}

	public void ResizeForCapture()
	{
		rect.DOScale(scaleForCapture, animationDuration).OnComplete(delegate
		{
		});
		rect.DOAnchorPos(anchorPosForCapture, animationDuration);
	}

	public void ResizePostCapture()
	{
		rect.DOScale(Vector3.one, animationDuration);
		rect.DOAnchorPos(startingPosition, animationDuration).OnComplete(delegate
		{
		});
	}

	public void PaintColor(PixelEditorTile tile, bool shouldSave = true)
	{
		if (tile.currentColor != PixelToolPalette.instance.activeEditorTool.toolID)
		{
			tile.currentColor = PixelToolPalette.instance.activeEditorTool.toolID;
			if (PixelToolPalette.instance.activeEditorTool.toolID != BlockColor.Blank)
			{
				SoundManager.PlaySoundClip(SoundClip.drawOnBoard);
				tile.ChangeColor(PixelToolPalette.instance.activeEditorTool.toolColor);
			}
			else
			{
				SoundManager.PlayEventSound(SoundEvent.Erase);
				tile.ChangeColor(ColorUtilities.blockRGB[BlockColor.Blank]);
			}
			if (currentBloxelBoard != null)
			{
				currentBloxelBoard.SetTileAtLocation(tile.idx, tile.currentColor, shouldSave);
			}
		}
	}

	private void OnPointerClick(PixelEditorTile tile)
	{
		if (Input.touchCount <= 1)
		{
			if (currentBloxelBoard != null)
			{
				PushToUndoStack(new BoardState((BlockColor[,])currentBloxelBoard.blockColors.Clone(), (Color[])currentBloxelBoard.toolPaletteChoices.Clone()));
			}
			else
			{
				PushToUndoStack(new BoardState(GetBlankSet(), GetDefaultPalette()));
			}
			if (this.OnTilePointerClick != null)
			{
				this.OnTilePointerClick(tile);
			}
		}
	}

	private void OnDrag(PixelEditorTile tile)
	{
		if (Input.touchCount <= 1 && this.OnTileDrag != null)
		{
			this.OnTileDrag(tile);
		}
	}

	private void OnBeginDrag(PixelEditorTile tile)
	{
		if (Input.touchCount <= 1)
		{
			isPainting = true;
			if (currentBloxelBoard != null)
			{
				PushToUndoStack(new BoardState((BlockColor[,])currentBloxelBoard.blockColors.Clone(), (Color[])currentBloxelBoard.toolPaletteChoices.Clone()));
			}
			if (this.OnTileBeginDrag != null)
			{
				this.OnTileBeginDrag(tile);
			}
		}
	}

	private void OnEndDrag(PixelEditorTile tile)
	{
		if (Input.touchCount <= 1)
		{
			isPainting = false;
			if (this.OnTileEndDrag != null)
			{
				this.OnTileEndDrag(tile);
			}
		}
	}

	private void OnPointerEnter(PixelEditorTile tile)
	{
		if (Input.touchCount <= 1 && this.OnTilePointerEnter != null)
		{
			this.OnTilePointerEnter(tile);
		}
	}

	private void OnPointerExit(PixelEditorTile tile)
	{
		if (Input.touchCount <= 1 && this.OnTilePointerExit != null)
		{
			this.OnTilePointerExit(tile);
		}
	}
}
