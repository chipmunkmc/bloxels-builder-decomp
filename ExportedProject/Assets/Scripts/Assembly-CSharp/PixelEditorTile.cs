using System;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PixelEditorTile : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IDragHandler, IBeginDragHandler, IEndDragHandler, IEventSystemHandler
{
	public delegate void HandleOnPointerClick(PixelEditorTile tile);

	public delegate void HandleOnDrag(PixelEditorTile tile);

	public delegate void HandleOnBeginDrag(PixelEditorTile tile);

	public delegate void HandleOnEndDrag(PixelEditorTile tile);

	public delegate void HandleOnPointerEnter(PixelEditorTile tile);

	public delegate void HandleOnPointerExit(PixelEditorTile tile);

	public delegate void HandleTileChange(PixelEditorTile tile);

	[HideInInspector]
	[SerializeField]
	private BlockColor _currentColor;

	public int idx;

	public GridLocation loc;

	public Image image;

	public Sprite blank;

	public Sprite solid;

	[ExposeProperty]
	public BlockColor currentColor
	{
		get
		{
			return _currentColor;
		}
		set
		{
			_currentColor = value;
			if (this.OnTileChange != null)
			{
				this.OnTileChange(this);
			}
		}
	}

	public event HandleOnPointerClick OnTilePointerClick;

	public event HandleOnDrag OnTileDrag;

	public event HandleOnBeginDrag OnTileBeginDrag;

	public event HandleOnEndDrag OnTileEndDrag;

	public event HandleOnPointerEnter OnTilePointerEnter;

	public event HandleOnPointerExit OnTilePointerExit;

	public event HandleTileChange OnTileChange;

	private void Awake()
	{
		currentColor = BlockColor.Blank;
		if (image == null)
		{
			image = base.transform.GetChild(0).GetComponent<Image>();
		}
	}

	private void Start()
	{
		idx = base.transform.GetSiblingIndex();
	}

	public void ChangeColor(Color color)
	{
		if (color == ColorUtilities.blankUnityColor)
		{
			image.sprite = blank;
			image.color = Color.white;
		}
		else
		{
			image.sprite = solid;
			image.color = color;
		}
	}

	public void OnPointerClick(PointerEventData pEvent)
	{
		if (Input.touchCount <= 1 && this.OnTilePointerClick != null)
		{
			this.OnTilePointerClick(this);
		}
	}

	public void OnDrag(PointerEventData pEvent)
	{
		if (Input.touchCount <= 1 && this.OnTileDrag != null)
		{
			this.OnTileDrag(this);
		}
	}

	public void OnBeginDrag(PointerEventData pEvent)
	{
		if (Input.touchCount <= 1 && this.OnTileBeginDrag != null)
		{
			this.OnTileBeginDrag(this);
		}
	}

	public void OnEndDrag(PointerEventData pEvent)
	{
		if (Input.touchCount <= 1 && this.OnTileEndDrag != null)
		{
			this.OnTileEndDrag(this);
		}
	}

	public void OnPointerEnter(PointerEventData pEvent)
	{
		if (Input.touchCount <= 1 && this.OnTilePointerEnter != null)
		{
			this.OnTilePointerEnter(this);
		}
	}

	public void OnPointerExit(PointerEventData pEvent)
	{
		if (Input.touchCount <= 1 && this.OnTilePointerExit != null)
		{
			this.OnTilePointerExit(this);
		}
	}
}
