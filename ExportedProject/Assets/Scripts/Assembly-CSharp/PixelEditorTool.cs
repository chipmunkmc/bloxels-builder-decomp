using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class PixelEditorTool : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
{
	private Color _toolColor;

	public BlockColor toolID;

	private bool _isActive;

	public RectTransform tooltipIndicator;

	public Vector2 iconActivePos;

	public Vector2 iconInactivePos;

	public GameObject indicator;

	public RectTransform icon;

	private Image toolImage;

	public int idx;

	public Color toolColor
	{
		get
		{
			return _toolColor;
		}
		set
		{
			_toolColor = value;
			toolImage.color = _toolColor;
		}
	}

	public bool isActive
	{
		get
		{
			return _isActive;
		}
		set
		{
			_isActive = value;
			if (_isActive)
			{
				if (toolID == BlockColor.Blank)
				{
					indicator.transform.localScale = Vector3.one;
				}
				toolImage.sprite = AssetManager.instance.iconToggleBtnActive;
				icon.anchoredPosition = iconActivePos;
				if (tooltipIndicator != null)
				{
					tooltipIndicator.localScale = Vector3.one;
				}
			}
			else
			{
				if (toolID == BlockColor.Blank)
				{
					indicator.transform.localScale = Vector3.zero;
				}
				toolImage.sprite = AssetManager.instance.iconToggleBtnInActive;
				icon.anchoredPosition = iconInactivePos;
				if (tooltipIndicator != null)
				{
					tooltipIndicator.localScale = Vector3.zero;
				}
			}
		}
	}

	private void Awake()
	{
		toolImage = GetComponent<Image>();
		idx = base.transform.GetSiblingIndex();
		indicator.transform.localScale = Vector3.zero;
	}

	private void Start()
	{
		SetupTool();
	}

	private void SetupTool()
	{
		SwitchColor(toolID);
		toolImage.sprite = AssetManager.instance.iconToggleBtnInActive;
		toolImage.color = toolColor;
		icon.anchoredPosition = iconInactivePos;
		isActive = false;
		if (toolID != BlockColor.Blank)
		{
			icon.localScale = Vector3.zero;
		}
		if (toolID == BlockColor.Red)
		{
			isActive = true;
			toolImage.sprite = AssetManager.instance.iconToggleBtnActive;
			PixelToolPalette.instance.SwitchActiveTool(this);
		}
	}

	private void SwitchColor(BlockColor c)
	{
		toolColor = ColorUtilities.blockRGB[c];
	}

	public void MovePaletteChoiceIndicator()
	{
		PaletteColorChoice[] paletteChoices = PixelToolPalette.instance.paletteChoices;
		foreach (PaletteColorChoice paletteColorChoice in paletteChoices)
		{
			if (paletteColorChoice.image.color == toolImage.color && (bool)PixelToolPalette.instance)
			{
				PixelToolPalette.instance.currentPaletteChoiceIndicator.SetParent(paletteColorChoice.transform.parent);
				PixelToolPalette.instance.currentPaletteChoiceIndicator.anchoredPosition = paletteColorChoice.rect.anchoredPosition;
			}
		}
	}

	public void Toggle()
	{
		if (!isActive)
		{
			Activate();
		}
		else if (toolID != BlockColor.Blank && !PixelToolPalette.instance.locked)
		{
			if (isActive && PixelToolPalette.instance.currentState != PixelToolPalette.State.Open)
			{
				PixelToolPalette.instance.Open();
			}
			else if (isActive && PixelToolPalette.instance.currentState == PixelToolPalette.State.Open)
			{
				PixelToolPalette.instance.Dock();
			}
		}
	}

	public void Deactivate()
	{
		isActive = false;
	}

	public void Activate()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		isActive = true;
		PixelToolPalette.instance.SwitchActiveTool(this);
		MovePaletteChoiceIndicator();
	}

	public void OnPointerClick(PointerEventData pEvent)
	{
		Toggle();
	}

	public void OnPointerDown(PointerEventData eData)
	{
		if (isActive)
		{
			base.transform.DOScale(0.95f, UIAnimationManager.speedMedium);
		}
	}

	public void OnPointerUp(PointerEventData eData)
	{
		if (isActive)
		{
			base.transform.DOScale(1f, UIAnimationManager.speedMedium);
		}
	}
}
