using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using Prime31;
using UnityEngine;
using VoxelEngine;

public class PixelPlayerController : CharacterController2D
{
	public delegate void HandlePlayerDeath();

	public delegate void HandleOnReady();

	public static PixelPlayerController instance;

	public bool isInvincible;

	public static string collider2DLayer = "Player";

	public int colliderLayerValue;

	public static float defaultPlayerGravity = -350f;

	public static float defaultJumpHeight = 1.8f;

	public static float defaultInertia = 20f;

	public static float defaultRunSpeed = 50f;

	public PlayerMovementController movementController;

	public PlayerWaterDetector waterDetector;

	public Transform selfTransform;

	public Transform centerTransform;

	public Rigidbody2D playerRigidbody2D;

	public CharacterAnimationController animationController;

	private UnityEngine.Object bombPrefab;

	public UnityEngine.Object powerupIndicatorPrefab;

	public Vector3 pivotLocalPosition;

	private bool canFireBullet;

	private float bulletTimer;

	private float currentJetpackTime;

	private float jetpackDuration = 15f;

	public bool usingJetPackTouchControl;

	public bool playerHasJetpack;

	public bool jetpackActive;

	private bool isDead;

	public readonly int maxHealth = 3;

	private int _health;

	private float _waitTime = 1.5f;

	private bool _lock;

	public Material sharedMaterial;

	private Vector2 touchStartPos1;

	private Vector2 touchDragDistance1;

	private Vector2 touchStartPos2;

	private Vector2 touchDragDistance2;

	public float projectileSpeed = 3f;

	public bool button1Pressed;

	public bool button2Pressed;

	public PlayerSquishTrigger squishDetector;

	public float airTime;

	private bool _isPaused;

	public float buttonHoldCount = 0.5f;

	private Vector2 originalColliderSize;

	private Vector2 originalColliderOffset;

	public Vector3 lastHitPosition;

	private Coroutine damageCoroutine;

	private static int[] _TotalsForCoin = new int[8];

	private Vector3 originalScale;

	private float originalSkinWidth;

	public float shrinkMovementMultiplier = 1f;

	public ParticleSystem shrinkParticles;

	public ParticleSystem invincibilityParticles;

	public ParticleSystem invincibilityFadeParticles;

	public InvincibilityParticleBurstController invincibilityBurst;

	public float invincibilityDuration;

	private float invincibilityEndTime;

	private int button1TouchID = -1;

	private bool buttonSlideActionEnabled1;

	private int button2TouchID = -1;

	private bool buttonSlideActionEnabled2;

	private Touch touchButton1;

	private Touch touchButton2;

	public int health
	{
		get
		{
			return _health;
		}
		set
		{
			_health = value;
			if (_health <= 0 && this.OnDeath != null && !isDead)
			{
				isDead = true;
				movementController.LoseJetpack();
				DisableInvincibility();
				this.OnDeath();
			}
		}
	}

	public bool isShrunk { get; private set; }

	public bool isUsingShrinkPotion { get; private set; }

	public event HandlePlayerDeath OnDeath;

	public event HandleOnReady OnReadyForGameplay;

	private new void Awake()
	{
		if (instance != null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		base.Awake();
		movementController = GetComponent<PlayerMovementController>();
		originalScale = transform.localScale;
		originalSkinWidth = base.skinWidth;
		selfTransform = GetComponent<Transform>();
		playerRigidbody2D = GetComponent<Rigidbody2D>();
		health = 3;
		colliderLayerValue = LayerMask.NameToLayer(collider2DLayer);
	}

	private void OnEnable()
	{
		PPInputController.OnButton += VirtualButtonPressed;
		PPInputController.OnButtonReleased += VirtualButtonRelease;
		base.onTriggerEnterEvent += HitTrigger;
		OnDeath += GameplayController.instance.inventoryManager.ClearInventoryHUD;
		button1Pressed = false;
		button2Pressed = false;
	}

	private void OnDisable()
	{
		if (sharedMaterial != null)
		{
			sharedMaterial.SetColor("_Color", Color.white);
		}
		PPInputController.OnButton -= VirtualButtonPressed;
		PPInputController.OnButtonReleased -= VirtualButtonRelease;
		base.onTriggerEnterEvent -= HitTrigger;
		OnDeath -= GameplayController.instance.inventoryManager.ClearInventoryHUD;
	}

	public void Reset()
	{
		isDead = false;
		health = 3;
		button1Pressed = false;
		button2Pressed = false;
		movementController.LoseJetpack();
		Unshrink(true);
		DisableInvincibility(true);
		CancelTakingDamage();
		GameplayController.instance.PlayerExitedWater();
	}

	public void ResetInventoryFromCheckpoint()
	{
		InventoryData inventoryData = GameplayController.instance.inventoryManager.inventory[1];
		currentJetpackTime = ((!inventoryData.available) ? jetpackDuration : (jetpackDuration - jetpackDuration * ((float)inventoryData.remainingQuantity / (float)inventoryData.maxQuantity)));
		if (InventoryController.checkpointMovement != null && InventoryController.checkpointMovement.type == InventoryData.Type.Jetpack)
		{
			movementController.GetJetpack();
			movementController._jetpack.emissionRate = 0f;
			jetpackActive = false;
		}
	}

	public void DisableForEditor()
	{
		Reset();
		boxCollider.enabled = false;
		movementController.enabled = false;
		base.enabled = false;
	}

	public void SetAutomaticColliderBounds()
	{
		animationController.SetCharacterBoxColliderProperties(ref boxCollider);
		originalColliderSize = boxCollider.size;
		originalColliderOffset = boxCollider.offset;
		centerTransform.localPosition = new Vector3(0f, originalColliderOffset.y, 0f);
		recalculateDistanceBetweenRays();
		waterDetector.circleCollider.offset = boxCollider.offset;
		waterDetector.circleCollider.radius = Mathf.Min(boxCollider.size.x, boxCollider.size.y) / 4f * 0.9f;
		waterDetector.radius = waterDetector.circleCollider.radius;
		squishDetector.SetUpColliderSizesAndPositions();
	}

	public void PrepareToDrop()
	{
		boxCollider.enabled = false;
	}

	public void EnableForGameplay()
	{
		SetAutomaticColliderBounds();
		boxCollider.enabled = true;
		movementController.enabled = true;
		base.enabled = true;
		movementController.jumpHeight = GameplayController.instance.currentGame.hero.characterJumpHeight;
		movementController.groundDamping = GameplayController.instance.currentGame.hero.characterInertia;
		movementController.doubleJump = GameplayController.instance.currentGame.hero.doubleJump;
		movementController.Reset();
		transform.localPosition = new Vector3(transform.localPosition.x, transform.localPosition.y, 0f);
		WorldLocation heroStartPosition = GameplayController.instance.currentGame.heroStartPosition;
		WorldPos2D worldPos2D = new WorldPos2D(heroStartPosition.levelLocationInWorld.c * 169 + heroStartPosition.locationInBoard.c * 13, heroStartPosition.levelLocationInWorld.r * 169 + heroStartPosition.locationInBoard.r * 13);
		WorldPos2D worldPos2D2 = worldPos2D;
		int num = heroStartPosition.levelLocationInWorld.c * 13 + heroStartPosition.locationInBoard.c;
		int num2 = heroStartPosition.levelLocationInWorld.r * 13 + heroStartPosition.locationInBoard.r;
		if (Mathf.FloorToInt(transform.localPosition.x / 13f) * 13 == worldPos2D.x && Mathf.FloorToInt(transform.localPosition.y / 13f) * 13 == worldPos2D.y)
		{
			int num3 = heroStartPosition.locationInBoard.r;
			int num4 = num2;
			while (num3 < 13)
			{
				BloxelTile value = null;
				List<TileInfo> list = GameplayBuilder.instance.worldTileData[num, num4];
				if (list != null && list.Count != 0)
				{
					TileInfo tileInfo = list[0];
					if (tileInfo.gameBehavior == GameBehavior.Coin || tileInfo.gameBehavior == GameBehavior.Water || tileInfo.gameBehavior == GameBehavior.NPC || tileInfo.gameBehavior == GameBehavior.Enemy || tileInfo.gameBehavior == GameBehavior.Air)
					{
						worldPos2D = worldPos2D2;
						break;
					}
					worldPos2D2.y += 13;
					if (num3 == 12)
					{
						if (GameplayBuilder.instance.tiles.TryGetValue(worldPos2D, out value))
						{
							GameplayBuilder.instance.UnloadTile(value, true);
						}
						else
						{
							GameplayBuilder.instance.worldTileData[num, num2][0].shouldRespawn = false;
						}
					}
					num3++;
					num4++;
					continue;
				}
				worldPos2D = worldPos2D2;
				break;
			}
			transform.localPosition = new Vector3(transform.localPosition.x, worldPos2D.y, transform.localPosition.z);
			if (GameplayController.instance.currentMode == GameplayController.Mode.Test)
			{
				GridLocation gridLocation = new GridLocation(Mathf.FloorToInt(transform.localPosition.x % 169f / 13f), Mathf.FloorToInt(transform.localPosition.y % 169f / 13f));
				if (gridLocation != heroStartPosition.locationInBoard)
				{
					GameplayController.instance.currentGame.SetHeroAtLocation(GameplayController.instance.currentGame.hero, new WorldLocation(heroStartPosition.id, gridLocation, heroStartPosition.levelLocationInWorld));
				}
			}
		}
		GameplayController.instance.lastCheckpoint = transform.localPosition;
		button1Pressed = false;
		button2Pressed = false;
		if (this.OnReadyForGameplay != null)
		{
			this.OnReadyForGameplay();
		}
	}

	public void KillPlayer()
	{
		GameHUD.instance.RemoveHearts(health);
		health = 0;
	}

	public void CancelTakingDamage()
	{
		if (damageCoroutine != null)
		{
			StopCoroutine(damageCoroutine);
			sharedMaterial.SetColor("_Color", Color.white);
			SetLayerMaskToNormal();
			_lock = false;
		}
	}

	public IEnumerator TakingDamage(int amount)
	{
		SetLayerMaskToDamaged();
		SoundManager.instance.PlaySound(SoundManager.instance.playerHit);
		_lock = true;
		health -= amount;
		if ((bool)GameHUD.instance)
		{
			GameHUD.instance.RemoveHearts(amount);
		}
		float blinkEndTime = Time.time + _waitTime;
		Color currentColor = Color.white;
		WaitForSeconds waitPointZeroFive = new WaitForSeconds(0.05f);
		while (Time.time < blinkEndTime)
		{
			currentColor = ((!(currentColor == Color.white)) ? Color.white : Color.red);
			sharedMaterial.SetColor("_Color", currentColor);
			yield return waitPointZeroFive;
		}
		sharedMaterial.SetColor("_Color", Color.white);
		SetLayerMaskToNormal();
		_lock = false;
	}

	private void CollisionEvent(RaycastHit2D cast)
	{
	}

	public void HitPlayer(int damageAmount)
	{
		if (damageAmount >= health)
		{
			KillPlayer();
		}
		else
		{
			damageCoroutine = StartCoroutine(TakingDamage(damageAmount));
		}
	}

	private void HitTrigger(Collider2D collider)
	{
		if (collider.gameObject.layer == LayerMask.NameToLayer(EnemyProjectile.collider2Dlayer) && !_lock)
		{
			EnemyProjectile component = collider.gameObject.GetComponent<EnemyProjectile>();
			component.Explode();
			if (!isInvincible)
			{
				lastHitPosition = component.selfTransform.position;
				HitPlayer(component.damage);
			}
		}
		else if (collider.gameObject.layer == LayerMask.NameToLayer("ReflectedProjectile") && !_lock)
		{
			ReflectedBullet component2 = collider.GetComponent<ReflectedBullet>();
			component2.Explode();
			if (!isInvincible)
			{
				lastHitPosition = component2.selfTransform.position;
				HitPlayer(1);
			}
		}
		else if (collider.gameObject.layer == LayerMask.NameToLayer("EnemyDamage") && !_lock)
		{
			float z = selfTransform.localScale.z;
			lastHitPosition = selfTransform.position + new Vector3(0f, boxCollider.size.y * z / 2f, 0f);
			TilePhysicsEnabler component3 = collider.attachedRigidbody.GetComponent<TilePhysicsEnabler>();
			if (component3 == null)
			{
				return;
			}
			BloxelEnemyTile bloxelEnemyTile = component3.tileRenderer.bloxelTile as BloxelEnemyTile;
			if (bloxelEnemyTile == null)
			{
				return;
			}
			if (isInvincible)
			{
				bloxelEnemyTile.TakeDamage(lastHitPosition, TileExplosionManager.instance.bombExplosionForce * selfTransform.localScale.y, 1000);
				return;
			}
			bloxelEnemyTile.controller.BurgleThePlayer(boxCollider.bounds.center, boxCollider.size.y / 2f);
			HitPlayer(bloxelEnemyTile.controller.attackDamage);
			float num = Mathf.Sign((selfTransform.position - collider.transform.position).x);
			float startValue2 = 0f;
			float endValue = 1f;
			Vector3 movementOffset2 = new Vector3(num * 6f, 1f, 0f) * z;
			DOTween.To(() => startValue2, delegate(float x)
			{
				startValue2 = x;
			}, endValue, UIAnimationManager.speedFast).OnUpdate(delegate
			{
				move(movementOffset2 * 0.3f);
			});
		}
		else if (collider.gameObject.layer == LayerMask.NameToLayer(BloxelCollectibleTile.collider2DLayer))
		{
			BloxelCoinTile component4 = collider.gameObject.GetComponent<BloxelCoinTile>();
			if (component4 != null)
			{
				if ((bool)GameHUD.instance)
				{
					GameHUD.instance.CollectCoin();
				}
				SoundManager.instance.PlaySound(SoundManager.instance.coinSFX);
				ParticleSystem particleSystem = SpecialEffects.instance.Create(SpecialEffects.instance.coinPoof, WorldWrapper.instance.effectsContainer, component4.selfTransform.position + new Vector3(6f, 6f, 0f));
				particleSystem.startColor = GetParticleColorForCoin(component4);
				particleSystem.Play();
				GameplayBuilder.instance.UnloadTile(component4, true);
			}
			else
			{
				PowerUp component5 = collider.gameObject.GetComponent<PowerUp>();
				if (component5 != null)
				{
					component5.Collect();
				}
			}
		}
		else if (collider.gameObject.layer == LayerMask.NameToLayer(BloxelHazardTile.collider2DLayer))
		{
			float z2 = selfTransform.localScale.z;
			lastHitPosition = selfTransform.position + new Vector3(0f, boxCollider.size.y * z2 / 2f, 0f);
			HitPlayer(1);
			float num2 = Mathf.Sign((selfTransform.position - collider.gameObject.transform.position).x);
			float startValue = 0f;
			float endValue2 = 1f;
			Vector3 movementOffset = new Vector3(num2 * 6f, 1f, 0f) * z2;
			DOTween.To(() => startValue, delegate(float x)
			{
				startValue = x;
			}, endValue2, UIAnimationManager.speedFast).OnStart(delegate
			{
				SetLayerMaskToDamaged();
			}).OnUpdate(delegate
			{
				move(movementOffset * 0.3f);
			});
		}
	}

	private Color GetParticleColorForCoin(BloxelCoinTile coin)
	{
		if (coin == null)
		{
			return Color.gray;
		}
		Array.Clear(_TotalsForCoin, 0, _TotalsForCoin.Length);
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = coin.tileRenderer.currentFrameChunk.blockColors[i, j];
				if (blockColor != BlockColor.Blank)
				{
					_TotalsForCoin[(uint)blockColor]++;
				}
			}
		}
		int num = 0;
		int num2 = 0;
		for (int k = 0; k < _TotalsForCoin.Length; k++)
		{
			if (_TotalsForCoin[k] >= num)
			{
				num = _TotalsForCoin[k];
				num2 = k;
			}
		}
		switch (num2)
		{
		case 0:
			num2 = 0;
			break;
		case 1:
			num2 = 4;
			break;
		case 2:
			num2 = 3;
			break;
		case 3:
			num2 = 2;
			break;
		case 4:
			num2 = 1;
			break;
		case 5:
			num2 = 6;
			break;
		case 6:
			num2 = 5;
			break;
		case 7:
			num2 = 7;
			break;
		}
		Color white = Color.white;
		if (coin.tileRenderer.projectType == ProjectType.Board)
		{
			return coin.tileRenderer.bloxelBoard.toolPaletteChoices[num2];
		}
		return coin.tileRenderer.animationProject.boards[coin.tileRenderer.currentFrameIndex].toolPaletteChoices[num2];
	}

	public void EnterWater()
	{
		if (!GameplayController.instance.inWater)
		{
			GameplayController.instance.PlayerEnteredWater();
		}
	}

	public void ExitWater()
	{
		if (GameplayController.instance.inWater)
		{
			Bounds bounds = boxCollider.bounds;
			float z = WorldWrapper.instance.selfTransform.position.z;
			Collider2D collider2D = Physics2D.OverlapCircle(new Vector2(bounds.center.x, bounds.center.y), waterDetector.circleCollider.radius * selfTransform.localScale.z, 1 << LayerMask.NameToLayer(BloxelWaterTile.collider2DLayer), z - 1f, z + 1f);
			if (!collider2D)
			{
				GameplayController.instance.PlayerExitedWater();
			}
		}
	}

	private void OnDestroy()
	{
		GameplayController.instance.CancelDroppingCharacter();
	}

	private void Update()
	{
		if (collisionState.becameGroundedThisFrame)
		{
			if (airTime > 0.2f)
			{
				if (selfTransform.localScale.y > 0.5f)
				{
					SpecialEffects.instance.Create(SpecialEffects.instance.smokeEffect, selfTransform, selfTransform.position);
				}
				SoundManager.instance.PlaySound(SoundManager.instance.playerLand);
			}
			airTime = 0f;
		}
		if (selfTransform.localPosition.y < GameplayController.instance.currentGame.worldBoundingRect.min.y - 84.5f)
		{
			health = 0;
		}
		else
		{
			if (Input.touchCount == 0)
			{
				return;
			}
			for (int i = 0; i < Input.touchCount; i++)
			{
				Touch touch = Input.GetTouch(i);
				if (touch.fingerId == button1TouchID)
				{
					touchButton1 = touch;
				}
				else if (touch.fingerId == button2TouchID)
				{
					touchButton2 = touch;
				}
			}
			if (GameplayController.instance.inventoryManager.currentAction != null)
			{
				HandleButton2Slide();
			}
			if (GameplayController.instance.inventoryManager.currentMovement != null)
			{
				HandleButton1Slide();
			}
		}
	}

	public void ToggleShrink()
	{
		if (!isShrunk)
		{
			Shrink();
		}
		else
		{
			Unshrink();
		}
	}

	public void Shrink(bool immediate = false)
	{
		if (!immediate && isUsingShrinkPotion)
		{
			return;
		}
		isUsingShrinkPotion = false;
		selfTransform.DOKill();
		float scale = originalScale.z / 13f;
		float startSkinWidth = base.skinWidth;
		bool invincibilityState = isInvincible;
		if (immediate)
		{
			selfTransform.localScale = new Vector3(scale * Mathf.Sign(selfTransform.localScale.x), scale, scale);
			base.skinWidth = originalSkinWidth * scale;
			shrinkParticles.gameObject.SetActive(false);
			movementController.SetToShrunkenMovement();
			squishDetector.gameObject.SetActive(true);
			isShrunk = true;
		}
		else
		{
			isUsingShrinkPotion = true;
			SoundManager.instance.PlaySound(SoundManager.instance.powerUpShrinkSFX);
			movementController.enabled = false;
			velocity = Vector3.zero;
			movementController.StopMoving();
			shrinkParticles.gameObject.SetActive(true);
			shrinkParticles.Play();
			selfTransform.DOScale(new Vector3(scale * Mathf.Sign(selfTransform.localScale.x), scale, scale), UIAnimationManager.playerShrinkSpeed).OnStart(delegate
			{
				if (!invincibilityState)
				{
					isInvincible = true;
				}
				DOTween.To(() => startSkinWidth, delegate(float x)
				{
					base.skinWidth = x;
				}, originalSkinWidth * scale, UIAnimationManager.playerShrinkSpeed);
				DOTween.To(() => 1f, delegate(float x)
				{
					shrinkMovementMultiplier = x;
				}, 4f, UIAnimationManager.playerShrinkSpeed);
			}).OnComplete(delegate
			{
				if (!invincibilityState)
				{
					isInvincible = false;
				}
				movementController.enabled = true;
				shrinkParticles.gameObject.SetActive(false);
				movementController.SetToShrunkenMovement();
				squishDetector.gameObject.SetActive(true);
				isShrunk = true;
				isUsingShrinkPotion = false;
			});
		}
		BloxelCamera.instance.HandleShrinkPowerUp(immediate);
	}

	public void Unshrink(bool immediate = false)
	{
		if (!immediate && !UnshrinkIsSafe())
		{
			animationController.containerTransform.DOKill(true);
			animationController.containerTransform.DOPunchScale(new Vector3(2f, 2f, 2f), 0.5f, 0, 0f).SetEase(Ease.InQuad);
		}
		else
		{
			if (!immediate && isUsingShrinkPotion)
			{
				return;
			}
			isUsingShrinkPotion = false;
			selfTransform.DOKill();
			float z = originalScale.z;
			float startSkinWidth = base.skinWidth;
			bool invincibilityState = isInvincible;
			if (immediate)
			{
				selfTransform.localScale = new Vector3(z * Mathf.Sign(selfTransform.localScale.x), z, z);
				base.skinWidth = originalSkinWidth;
				movementController.SetToStandardMovement();
				squishDetector.gameObject.SetActive(false);
				isShrunk = false;
			}
			else
			{
				isUsingShrinkPotion = true;
				GameplayController.instance.inventoryManager.DecrementInventoryItem(InventoryData.Type.Shrink);
				SoundManager.instance.PlaySound(SoundManager.instance.powerUpUnshrinkSFX);
				movementController.enabled = false;
				velocity = Vector3.zero;
				movementController.StopMoving();
				selfTransform.DOScale(new Vector3(z * Mathf.Sign(selfTransform.localScale.x), z, z), UIAnimationManager.playerShrinkSpeed).OnStart(delegate
				{
					if (!invincibilityState)
					{
						isInvincible = true;
					}
					DOTween.To(() => startSkinWidth, delegate(float x)
					{
						base.skinWidth = x;
					}, originalSkinWidth, UIAnimationManager.playerShrinkSpeed);
					DOTween.To(() => 4f, delegate(float x)
					{
						shrinkMovementMultiplier = x;
					}, 1f, UIAnimationManager.playerShrinkSpeed);
				}).OnComplete(delegate
				{
					if (!invincibilityState)
					{
						isInvincible = false;
					}
					movementController.enabled = true;
					movementController.SetToStandardMovement();
					squishDetector.gameObject.SetActive(false);
					isShrunk = false;
					isUsingShrinkPotion = false;
				});
			}
			BloxelCamera.instance.HandleUnshrink(immediate);
		}
	}

	private bool UnshrinkIsSafe()
	{
		Vector3 position = selfTransform.position;
		Vector2 pointA = new Vector2(position.x + originalColliderOffset.x - originalColliderSize.x / 2f, position.y + originalColliderOffset.y - originalColliderSize.y / 2f + 0.05f);
		Collider2D collider2D = Physics2D.OverlapArea(pointB: new Vector2(pointA.x + originalColliderSize.x, pointA.y + originalColliderSize.y), pointA: pointA, layerMask: currentPlatformMask, minDepth: position.z - 1f, maxDepth: position.z + 1f);
		return collider2D == null;
	}

	public void GetHealth()
	{
		health = maxHealth;
	}

	public void GetJetpack()
	{
		currentJetpackTime = 0f;
		movementController.GetJetpack();
	}

	public void BurnJetFuel()
	{
		currentJetpackTime += Time.deltaTime;
		GameHUD.instance.SetJetpackFuel(1f - currentJetpackTime / jetpackDuration);
		if (currentJetpackTime >= jetpackDuration)
		{
			movementController.LoseJetpack();
		}
	}

	private new void OnTriggerEnter2D(Collider2D collider)
	{
		if (!(GetComponent<FollowMouse>() != null))
		{
			base.OnTriggerEnter2D(collider);
		}
	}

	public void EnableInvincibility()
	{
		SoundManager.instance.AdjustMusicPitch(SoundManager.instance.invincibilityPitchAdjust);
		isInvincible = true;
		invincibilityParticles.gameObject.SetActive(true);
		invincibilityParticles.Play();
		invincibilityParticles.time = UnityEngine.Random.Range(0f, invincibilityParticles.duration);
		invincibilityBurst.BeginInvincibility();
		ParticleSystem.EmissionModule emission = invincibilityParticles.emission;
		emission.enabled = true;
		CancelTakingDamage();
		StopCoroutine("InvincibilityTimer");
		StartCoroutine("InvincibilityTimer");
	}

	private IEnumerator InvincibilityTimer()
	{
		invincibilityEndTime = Time.time + invincibilityDuration;
		while (Time.time < invincibilityEndTime)
		{
			yield return new WaitForEndOfFrame();
		}
		DisableInvincibility();
	}

	public void DisableInvincibility(bool immediate = false)
	{
		if (isInvincible)
		{
			SoundManager.instance.AdjustMusicPitch(1f);
			isInvincible = false;
			invincibilityBurst.particleSystem.time = invincibilityParticles.time;
			invincibilityBurst.EndInvincibility(immediate);
			invincibilityParticles.Stop();
			invincibilityParticles.gameObject.SetActive(false);
			invincibilityFadeParticles.gameObject.SetActive(false);
		}
	}

	private void VirtualButtonPressed(PPInputController.InputButton button)
	{
		switch (button)
		{
		case PPInputController.InputButton.Right1:
			Button1Pressed();
			break;
		case PPInputController.InputButton.Right2:
			Button2Pressed();
			break;
		}
	}

	private void VirtualButtonRelease(PPInputController.InputButton button)
	{
		switch (button)
		{
		case PPInputController.InputButton.Right1:
			Button1Released();
			break;
		case PPInputController.InputButton.Right2:
			Button2Released();
			break;
		}
	}

	private void Button1Pressed()
	{
		if (button1Pressed)
		{
			return;
		}
		button1Pressed = true;
		Vector2 vector = RectTransformUtility.WorldToScreenPoint(UIOverlayCanvas.instance.overlayCamera, GameHUD.instance.actionButton1.GetComponent<RectTransform>().position);
		float num = float.PositiveInfinity;
		for (int i = 0; i < Input.touchCount; i++)
		{
			Touch touch = Input.touches[i];
			float sqrMagnitude = (touch.position - vector).sqrMagnitude;
			if (sqrMagnitude < num)
			{
				num = sqrMagnitude;
				button1TouchID = touch.fingerId;
			}
		}
	}

	private void Button1Released()
	{
		if (button1Pressed)
		{
			button1Pressed = false;
			GameHUD.instance.ResetMovementButton();
			if (!usingJetPackTouchControl)
			{
				movementController.DeactivateJetpack();
			}
			usingJetPackTouchControl = false;
			button1TouchID = -1;
		}
	}

	private void Button2Pressed()
	{
		if (button2Pressed)
		{
			return;
		}
		button2Pressed = true;
		Vector2 vector = RectTransformUtility.WorldToScreenPoint(UIOverlayCanvas.instance.overlayCamera, GameHUD.instance.actionButton2.GetComponent<RectTransform>().position);
		float num = float.PositiveInfinity;
		for (int i = 0; i < Input.touchCount; i++)
		{
			Touch touch = Input.touches[i];
			float sqrMagnitude = (touch.position - vector).sqrMagnitude;
			if (sqrMagnitude < num)
			{
				num = sqrMagnitude;
				button2TouchID = touch.fingerId;
			}
		}
		FireBullet();
	}

	private void Button2Released()
	{
		if (button2Pressed)
		{
			button2Pressed = false;
			if (GameHUD.instance.npcActive)
			{
				TalkToClosestAvailableNPC();
			}
			if (buttonSlideActionEnabled2)
			{
				ButtonSlideAction2();
			}
			GameHUD.instance.ResetActionButton();
			button2TouchID = -1;
		}
	}

	private void HandleButton2Slide()
	{
		if (button2TouchID < 0)
		{
			return;
		}
		if (touchButton2.phase == TouchPhase.Began)
		{
			touchStartPos2 = touchButton2.position;
		}
		else if (touchButton2.phase == TouchPhase.Moved)
		{
			touchDragDistance2 = touchButton2.position - touchStartPos2;
			if (Mathf.Abs(touchDragDistance2.y) > Mathf.Abs(touchDragDistance2.x) && touchDragDistance2.y <= 0f)
			{
				buttonSlideActionEnabled2 = GameHUD.instance.MoveActionButtonYAxis(touchDragDistance2.y);
			}
		}
	}

	private void HandleButton1Slide()
	{
		if (button1TouchID < 0)
		{
			return;
		}
		if (touchButton1.phase == TouchPhase.Began)
		{
			touchStartPos1 = touchButton1.position;
		}
		else if (touchButton1.phase == TouchPhase.Moved)
		{
			touchDragDistance1 = touchButton1.position - touchStartPos1;
			if (Mathf.Abs(touchDragDistance1.y) > Mathf.Abs(touchDragDistance1.x) && touchDragDistance1.y <= 0f)
			{
				buttonSlideActionEnabled1 = true;
				usingJetPackTouchControl = true;
				movementController.ActivateJetpack();
				GameHUD.instance.MoveMovementButtonYAxis(touchDragDistance1.y);
			}
		}
	}

	private void DetermineEscapeButtonHoldDownage()
	{
		if (Input.GetKey(KeyCode.Escape))
		{
			buttonHoldCount -= Time.unscaledDeltaTime;
			if (buttonHoldCount < 0f)
			{
				UIPopupPause.instance.BackButtonPressed();
				buttonHoldCount = 0.5f;
			}
		}
		else
		{
			buttonHoldCount = 0.5f;
		}
	}

	private void ResetButtonAndActionFlags()
	{
		buttonSlideActionEnabled1 = false;
		buttonSlideActionEnabled2 = false;
	}

	private void ButtonSlideAction1()
	{
		throw new NotImplementedException();
	}

	private void ButtonSlideAction2()
	{
		buttonSlideActionEnabled2 = false;
		InventoryData currentAction = GameplayController.instance.inventoryManager.currentAction;
		if (currentAction != null && currentAction.available)
		{
			switch (currentAction.type)
			{
			case InventoryData.Type.Bomb:
				ThrowBomb();
				break;
			case InventoryData.Type.Shrink:
				ToggleShrink();
				break;
			}
		}
	}

	public void FireBullet()
	{
		Vector3 position = new Vector3(selfTransform.position.x + selfTransform.localScale.x * Mathf.Clamp(boxCollider.size.x * 0.7f, 0.5f, 13f), selfTransform.position.y + selfTransform.localScale.y * Mathf.Clamp(boxCollider.offset.y, 0.5f, 13f), WorldWrapper.WorldZ - 0.5f * selfTransform.localScale.z);
		Bullet bullet = GameplayPool.SpawnBullet();
		bullet.selfTransform.position = position;
		bullet.rigidbodyComponent.AddForce(new Vector2(selfTransform.localScale.x * projectileSpeed * 100f, 0f), ForceMode2D.Impulse);
		SoundManager.PlayOneShot(SoundManager.instance.laser);
	}

	private void ThrowBomb()
	{
		SoundManager.PlayOneShot(SoundManager.instance.bombThrow);
		GameplayController.instance.inventoryManager.DecrementInventoryItem(InventoryData.Type.Bomb);
		float x = selfTransform.localPosition.x + selfTransform.localScale.x * (pivotLocalPosition.x - 3f);
		float y = selfTransform.localPosition.y + 5.5f * selfTransform.localScale.y;
		Bomb bomb = GameplayPool.SpawnBomb();
		bomb.InitWithScale(selfTransform.localScale.y);
		bomb.selfTransform.localPosition = new Vector3(x, y, 0f);
		bomb.selfTransform.localRotation = Quaternion.identity;
		bomb.transform.SetParent(WorldWrapper.instance.runtimeObjectContainer);
		bomb.Throw((int)Mathf.Sign(selfTransform.localScale.x));
	}

	private void TalkToClosestAvailableNPC()
	{
		Vector3 position = selfTransform.position;
		position += new Vector3(0f, boxCollider.size.y / 2f, 0f);
		NPCIndicator nPCIndicator = null;
		float num = float.PositiveInfinity;
		for (int i = 0; i < BloxelNPCTile.activeIndicators.Count; i++)
		{
			NPCIndicator nPCIndicator2 = BloxelNPCTile.activeIndicators[i];
			float sqrMagnitude = (position - (nPCIndicator2.tile.selfTransform.position + new Vector3(6f, 6f, 0f))).sqrMagnitude;
			if (sqrMagnitude < num)
			{
				nPCIndicator = nPCIndicator2;
			}
		}
		if (nPCIndicator != null)
		{
			nPCIndicator.tile.AttachSpeechBubble();
		}
	}

	public float GetSquaredDistanceToEnemy(BloxelEnemyTile enemy)
	{
		return Vector3.SqrMagnitude(selfTransform.localPosition + new Vector3(0f, boxCollider.size.y * selfTransform.localScale.y * 0.5f, 0f) - (enemy.selfTransform.localPosition + new Vector3(0f, enemy.originalBoundsSize.y * enemy.selfTransform.localScale.y * 0.5f, 0f)));
	}
}
