using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class PixelToolPalette : MonoBehaviour
{
	public enum State
	{
		Docked = 0,
		Open = 1,
		Hidden = 2,
		Locked = 3
	}

	public delegate void HandleOnChange(bool _isOpen);

	public static PixelToolPalette instance;

	public State currentState;

	public PixelEditorTool activeEditorTool;

	[Header("Groups")]
	public GameObject primaryTools;

	public GameObject secondaryTools;

	public PixelEditorTool[] tools;

	public PaletteColorChoice[] paletteChoices;

	public Color[] bloxelColorPalette;

	[Header("UI")]
	public RectTransform caret;

	public RectTransform rect;

	public Button uiButtonUndo;

	public Button uiButtonRedo;

	public RectTransform currentPaletteChoiceIndicator;

	public UIBoardPreview boardPreview;

	public Button uiButtonPaletteToggle;

	public Button uiButtonCopy;

	public Button uiButtonPaste;

	public UIUserBadge userBadge;

	[Header("Positioning")]
	public Vector2 startingPosition;

	public Vector2 openPosition;

	public Vector2 hiddenPosition;

	private UnityEngine.Object paletteChoicePrefab;

	private int numColumns = 8;

	private int gutter;

	private int itemWidth = 40;

	private int itemHeight = 40;

	private int yDelta = -20;

	private int xDelta = 20;

	[Header("State")]
	public bool isDefault;

	public bool isOpen;

	private bool _locked;

	public bool locked
	{
		get
		{
			return _locked;
		}
		set
		{
			_locked = value;
			if (_locked)
			{
				TurnOnIcons();
			}
			else
			{
				TurnOffIcons();
			}
		}
	}

	public event HandleOnChange OnChange;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		bloxelColorPalette = new Color[8]
		{
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Yellow),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Pink),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Purple),
			ColorUtilities.GetUnityColorFromBlockColor(BlockColor.White)
		};
		isDefault = true;
		tools = primaryTools.GetComponentsInChildren<PixelEditorTool>();
		paletteChoices = GetComponentsInChildren<PaletteColorChoice>();
	}

	private void Start()
	{
		PixelEditorPaintBoard.instance.OnVisibilityChange += HandleOnVisibilityChange;
		BloxelBoardLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
		BloxelAnimationLibrary.instance.OnProjectSelect += HandleOnProjectSelect;
		isOpen = false;
		rect = GetComponent<RectTransform>();
		paletteChoicePrefab = Resources.Load("Prefabs/PaletteColor");
		uiButtonCopy.onClick.AddListener(PixelEditorPaintBoard.instance.Copy);
		uiButtonPaste.onClick.AddListener(PixelEditorPaintBoard.instance.Paste);
		uiButtonPaletteToggle.onClick.AddListener(Toggle);
	}

	private void HandleOnProjectSelect(SavedProject item)
	{
		for (int i = 0; i < tools.Length; i++)
		{
			if (tools[i].isActive)
			{
				tools[i].MovePaletteChoiceIndicator();
			}
		}
	}

	private void OnDestroy()
	{
		PixelEditorPaintBoard.instance.OnVisibilityChange -= HandleOnVisibilityChange;
	}

	private void HandleOnVisibilityChange(bool isVisible)
	{
		if (isVisible)
		{
			rect.DOAnchorPos(startingPosition, UIAnimationManager.speedMedium);
		}
		else
		{
			rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedMedium);
		}
	}

	public void TurnOffIcons()
	{
		PixelEditorTool[] array = tools;
		foreach (PixelEditorTool pixelEditorTool in array)
		{
			if (pixelEditorTool.toolID != BlockColor.Blank)
			{
				pixelEditorTool.icon.transform.localScale = Vector3.zero;
			}
		}
		uiButtonPaletteToggle.gameObject.SetActive(true);
	}

	public void TurnOnIcons()
	{
		PixelEditorTool[] array = tools;
		foreach (PixelEditorTool pixelEditorTool in array)
		{
			if (pixelEditorTool.toolID != BlockColor.Blank)
			{
				pixelEditorTool.icon.transform.localScale = Vector3.one;
			}
		}
		uiButtonPaletteToggle.gameObject.SetActive(false);
	}

	public void SwitchActiveTool(PixelEditorTool tool)
	{
		GestureTutorialManager.instance.DetermineIfUserIsReadyToSeePinchZoomInTutorial();
		activeEditorTool = tool;
		for (int i = 0; i < tools.Length; i++)
		{
			PixelEditorTool pixelEditorTool = tools[i];
			if (pixelEditorTool != tool && pixelEditorTool.isActive)
			{
				pixelEditorTool.Deactivate();
				break;
			}
		}
	}

	public void UpdatePalette(Color[] paletteChoices)
	{
		int num = 0;
		foreach (Color toolColor in paletteChoices)
		{
			if (tools[num].toolID != BlockColor.Blank)
			{
				tools[num].toolColor = toolColor;
			}
			num++;
		}
		isDefault = false;
	}

	public void Reset()
	{
		int num = 0;
		Color[] array = bloxelColorPalette;
		foreach (Color toolColor in array)
		{
			if (tools[num].toolID != BlockColor.Blank)
			{
				tools[num].toolColor = toolColor;
			}
			num++;
		}
		isDefault = true;
	}

	public void Toggle()
	{
		if (currentState == State.Docked)
		{
			Open();
		}
		else
		{
			Dock();
		}
	}

	public void Open()
	{
		if (locked)
		{
			return;
		}
		SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
		float x = rect.sizeDelta.x - primaryTools.GetComponent<RectTransform>().sizeDelta.x - 140f;
		if (!BloxelLibrary.instance.libraryToggle.isClosed)
		{
			BloxelLibrary.instance.libraryToggle.Hide();
		}
		if (!BoardZoomManager.instance.isZoomed)
		{
			UICaptureToggleTool.instance.Hide();
			PixelEditorPaintBoard.instance.Move(Direction.Left, new Vector2(x, 0f));
			userBadge.Hide();
		}
		else
		{
			BoardZoomManager.instance.MoveToolsBlocksUnderTheColorBlocks();
			BoardZoomManager.instance.paintBoardTools.transform.DOScale(Vector3.zero, UIAnimationManager.speedFast);
		}
		rect.DOAnchorPos(openPosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isOpen = true;
			currentState = State.Open;
			caret.DORotate(new Vector3(0f, 0f, -90f), UIAnimationManager.speedFast);
			if (this.OnChange != null)
			{
				this.OnChange(isOpen);
			}
		}).OnComplete(delegate
		{
		});
	}

	public void Dock()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
		rect.DOAnchorPos(startingPosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isOpen = false;
			currentState = State.Docked;
			caret.DORotate(new Vector3(0f, 0f, 90f), UIAnimationManager.speedFast);
			if (this.OnChange != null)
			{
				this.OnChange(isOpen);
			}
		}).OnComplete(delegate
		{
		});
		if (!BoardZoomManager.instance.isZoomed)
		{
			UICaptureToggleTool.instance.Show();
			PixelEditorPaintBoard.instance.Move(Direction.Right, Vector2.zero);
			userBadge.Show();
		}
		else
		{
			BoardZoomManager.instance.MoveToolsBlocksToTheLeftOfTheColorBlocks();
			BoardZoomManager.instance.paintBoardTools.transform.DOScale(BoardZoomManager.instance.boardToolsSize, UIAnimationManager.speedMedium);
		}
	}

	public void Hide()
	{
		rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isOpen = false;
			currentState = State.Hidden;
		});
	}
}
