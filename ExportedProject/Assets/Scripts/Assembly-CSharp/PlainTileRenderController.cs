using UnityEngine;
using VoxelEngine;

public sealed class PlainTileRenderController : TileRenderController
{
	public override bool shouldGenerateColliders { get; set; }

	protected override void Update()
	{
		if (!(timeOfLastFrameChange + frameDuration < Time.time))
		{
			return;
		}
		if (projectType == ProjectType.Board)
		{
			if (!currentFrameChunk.initialized)
			{
				currentFrameChunk.InitWithBoard(base.bloxelBoard);
			}
			currentFrameChunk.UpdateChunk(base.bloxelBoard.localID, ref localMeshFilter, ref localCollider);
			timeOfLastFrameChange = Time.time;
			return;
		}
		currentFrameIndex = (currentFrameIndex + 1) % numberOfFrames;
		currentFrameChunk = frameChunks[currentFrameIndex];
		BloxelBoard bloxelBoard = animationProject.boards[currentFrameIndex];
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(bloxelBoard);
		}
		currentFrameChunk.UpdateChunk(bloxelBoard.localID, ref localMeshFilter, ref localCollider);
		timeOfLastFrameChange = Time.time;
	}

	public override void RenderTile(BloxelProject projectData)
	{
		switch (projectData.type)
		{
		case ProjectType.Board:
			RenderBoard(projectData as BloxelBoard);
			break;
		case ProjectType.Animation:
			RenderAnimation(projectData as BloxelAnimation);
			break;
		}
	}

	public override void RenderBoard(BloxelBoard board)
	{
		bloxelBoard = board;
		projectType = ProjectType.Board;
		currentFrameChunk = MovableChunk2DPool.instance.Spawn();
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(bloxelBoard);
		}
		currentFrameChunk.UpdateChunk(bloxelBoard.localID, ref localMeshFilter, ref localCollider, false);
		SetUpFrameProperties(currentFrameChunk);
		numberOfFrames = 1;
		base.framesPerSecond = 1f / 3f;
		base.enabled = true;
	}

	public override void RenderAnimation(BloxelAnimation animation)
	{
		animationProject = animation;
		projectType = ProjectType.Animation;
		numberOfFrames = animationProject.boards.Count;
		if (numberOfFrames != 0)
		{
			frameChunks = new MovableChunk2D[numberOfFrames];
			for (int i = 0; i < numberOfFrames; i++)
			{
				MovableChunk2D movableChunk2D = MovableChunk2DPool.instance.Spawn();
				SetUpFrameProperties(movableChunk2D);
				frameChunks[i] = movableChunk2D;
			}
			currentFrameIndex = 0;
			currentFrameChunk = frameChunks[currentFrameIndex];
			if (!currentFrameChunk.initialized)
			{
				currentFrameChunk.InitWithBoard(animationProject.boards[currentFrameIndex]);
			}
			currentFrameChunk.UpdateChunk(animationProject.boards[currentFrameIndex].localID, ref localMeshFilter, ref localCollider);
			base.framesPerSecond = animationProject.GetFrameRate();
			base.enabled = true;
		}
	}

	public override void ReleaseCurrentChunks()
	{
		base.enabled = false;
		if (currentFrameChunk.spawned)
		{
			currentFrameChunk.Despawn();
		}
		if (frameChunks != null && frameChunks != null)
		{
			for (int i = 0; i < frameChunks.Length; i++)
			{
				MovableChunk2D movableChunk2D = frameChunks[i];
				if (movableChunk2D.spawned)
				{
					movableChunk2D.Despawn();
				}
			}
			frameChunks = null;
		}
		localMeshFilter.sharedMesh = null;
	}
}
