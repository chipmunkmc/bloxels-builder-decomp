using System;
using System.Threading;
using UnityEngine;

public class PlayerBankManager : MonoBehaviour
{
	public delegate void HandleNewCoinCount(int _coins);

	public delegate void HandleGemCount(int _gems);

	public static PlayerBankManager instance;

	[SerializeField]
	private int _coinCount;

	private int _lastEmittedCount;

	private int _gemCount;

	private int _lastEmittedGemCount;

	public bool shouldUpdateServer = true;

	public event HandleNewCoinCount OnCoinUpdate;

	public event HandleGemCount OnGemUpdate;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Start()
	{
		GemManager.instance.AddGemsForType(GemEarnable.Type.AppInit);
		SetCoinCount(PrefsManager.instance.playerCoins);
		SetGemCount(CalculateGemCount());
		BloxelServerInterface.instance.OnStopSendingCoins += HandleStopSendingCoins;
		BloxelServerInterface.instance.OnFeaturedGems += HandleFeaturedGems;
	}

	private void OnDestroy()
	{
		SaveCoins();
		CancelInvoke("UpdateServer");
		BloxelServerInterface.instance.OnStopSendingCoins -= HandleStopSendingCoins;
		BloxelServerInterface.instance.OnFeaturedGems -= HandleFeaturedGems;
	}

	private void OnApplicationQuit()
	{
		SaveCoins();
	}

	public void SyncServerAccount()
	{
		shouldUpdateServer = false;
		CancelInvoke("UpdateServer");
		if (!(CurrentUser.instance == null) && CurrentUser.instance.active)
		{
			SetCoinCount(CurrentUser.instance.account.coins);
			shouldUpdateServer = true;
			InvokeRepeating("UpdateServer", 0.5f, 10f);
			SyncGems();
		}
	}

	private void HandleFeaturedGems(string serverFeaturedBool)
	{
		string featuredBool = PPUtilities.ORStrings(PrefsManager.instance.featuredBooleanString, serverFeaturedBool);
		PrefsManager.instance.SetFeaturedBool(featuredBool);
		SetGemCount(CalculateGemCount());
		UpdateGemsOnServer();
	}

	private void HandleStopSendingCoins()
	{
		shouldUpdateServer = false;
	}

	public int GetCoinCount()
	{
		return _coinCount;
	}

	public int AddCoins(int coins)
	{
		if (!CurrentUser.instance.active)
		{
			return 0;
		}
		SetCoinCount(_coinCount + coins);
		return _coinCount;
	}

	public int AddCoin()
	{
		if (!CurrentUser.instance.active)
		{
			return 0;
		}
		SetCoinCount(_coinCount + 1);
		return _coinCount;
	}

	public int RemoveCoins(int coins)
	{
		SetCoinCount(_coinCount - coins);
		return _coinCount;
	}

	public void SetCoinCount(int coins)
	{
		if (CurrentUser.instance.active)
		{
			if (coins < 0)
			{
				coins = 0;
			}
			_coinCount = coins;
			if (this.OnCoinUpdate != null)
			{
				this.OnCoinUpdate(_coinCount);
			}
		}
	}

	public void SaveCoins()
	{
		if (CurrentUser.instance.active)
		{
			PrefsManager.instance.playerCoins = _coinCount;
		}
	}

	public void SyncGems()
	{
		if (!(CurrentUser.instance == null) && CurrentUser.instance.active)
		{
			string gemBool = PPUtilities.ORStrings(PrefsManager.instance.gemBooleanString, CurrentUser.instance.account.gemBool);
			string featuredBool = PPUtilities.ORStrings(PrefsManager.instance.featuredBooleanString, CurrentUser.instance.account.featuredBool);
			string spendBool = PPUtilities.ORStrings(PrefsManager.instance.spendBooleanStringv2, CurrentUser.instance.account.spendBoolv2);
			PrefsManager.instance.SetGemBool(gemBool);
			PrefsManager.instance.SetFeaturedBool(featuredBool);
			PrefsManager.instance.SetSpendBool(spendBool);
			SetGemCount(CalculateGemCount());
			PrefsManager.instance.SyncUnlocks();
		}
	}

	private int CalculateGemCount()
	{
		int num = GemManager.GemValueFromGemBoolString(PrefsManager.instance.gemBooleanString);
		int num2 = GemManager.GemValueFromFeaturedBoolString(PrefsManager.instance.featuredBooleanString);
		int spendFromSpendBool = GemManager.GetSpendFromSpendBool(PrefsManager.instance.spendBooleanStringv2);
		return num + num2 - spendFromSpendBool;
	}

	public bool AddGemForFeaturedGame(string squareId)
	{
		if (BloxelServerInterface.featuredGameIds.Count != PrefsManager.instance.featuredBooleanString.Length)
		{
			return false;
		}
		char[] array = PrefsManager.instance.featuredBooleanString.ToCharArray();
		int num = -1;
		for (int i = 0; i < BloxelServerInterface.featuredGameIds.Count; i++)
		{
			if (BloxelServerInterface.featuredGameIds[i] == squareId)
			{
				num = i;
			}
		}
		if (num == -1)
		{
			return false;
		}
		bool flag = array[num] == '1';
		if (!flag)
		{
			array[num] = '1';
			string featuredBool = new string(array);
			PrefsManager.instance.SetFeaturedBool(featuredBool);
			SetGemCount(CalculateGemCount());
		}
		return flag;
	}

	public bool HasPlayedFeaturedGame(string squareId)
	{
		if (BloxelServerInterface.featuredGameIds.Count != PrefsManager.instance.featuredBooleanString.Length)
		{
			return false;
		}
		char[] array = PrefsManager.instance.featuredBooleanString.ToCharArray();
		int num = -1;
		for (int i = 0; i < BloxelServerInterface.featuredGameIds.Count; i++)
		{
			if (BloxelServerInterface.featuredGameIds[i] == squareId)
			{
				num = i;
			}
		}
		if (num == -1)
		{
			return false;
		}
		return array[num] == '1';
	}

	public int GetGemCount()
	{
		return _gemCount;
	}

	public int AddGems(int gems)
	{
		SetGemCount(_gemCount + gems);
		return _gemCount;
	}

	public int AddGem()
	{
		SetGemCount(_gemCount + 1);
		return _gemCount;
	}

	public int RemoveGems(int amount)
	{
		SetGemCount(_gemCount - amount, false);
		return _gemCount;
	}

	public void SetGemCount(int amount, bool shouldUpdateServer = true)
	{
		if (amount < 0)
		{
			amount = 0;
		}
		_gemCount = amount;
		if (shouldUpdateServer)
		{
			UpdateGemsOnServer();
		}
		if (this.OnGemUpdate != null)
		{
			this.OnGemUpdate(_gemCount);
		}
	}

	public void UpdateGemsOnServer()
	{
		if (CurrentUser.initialServerSynced && CurrentUser.instance.active)
		{
			StartCoroutine(CurrentUser.instance.UpdateServer_Gems(delegate
			{
			}));
		}
	}

	public void CancelUpdate()
	{
		CancelInvoke("UpdateServer");
	}

	public void UpdateServer()
	{
		if (shouldUpdateServer && (bool)CurrentUser.instance && CurrentUser.instance.active && BloxelServerInterface.instance.socket.IsConnected && _lastEmittedCount != _coinCount && AppStateManager.instance.currentScene != SceneName.Gameplay && AppStateManager.instance.currentScene != SceneName.Loading)
		{
			BloxelServerInterface.instance.Emit_UpdateCoins(CurrentUser.instance.account.serverID, _coinCount);
			_lastEmittedCount = _coinCount;
		}
	}
}
