using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlayerHUD : MonoBehaviour
{
	public static PlayerHUD instance;

	public RectTransform rect;

	public RectTransform rectGems;

	public TextMeshProUGUI uiTextCoinCount;

	public TextMeshProUGUI uiTextGemCount;

	public Button uiButtonFeatured;

	public UIButton uiButtonUser;

	public TextMeshProUGUI uiTextUserName;

	public RawImage uiRawImageUserAvatar;

	public Image uiImageDefaultAvatar;

	public CoverBoard userAvatarCover;

	public RectTransform rectIncompleteIndicator;

	public RectTransform rectParentApprovedIndicator;

	private Color coinCountTextBase;

	private Color cointCountTextInfufficient;

	private Vector3 startingCoinScale;

	public bool debug;

	private bool quickChange;

	public bool infinityViewOn { get; private set; }

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		rect = GetComponent<RectTransform>();
		coinCountTextBase = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Yellow);
		cointCountTextInfufficient = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
		startingCoinScale = uiTextCoinCount.transform.localScale;
		userAvatarCover = new CoverBoard(Color.clear);
	}

	private void Start()
	{
		uiTextCoinCount.color = coinCountTextBase;
		UpdateCoins(PlayerBankManager.instance.GetCoinCount());
		uiButtonFeatured.onClick.AddListener(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
			FeaturedSquaresController.instance.Show();
			if (CanvasTileInfo.instance.isVisible)
			{
				CanvasTileInfo.instance.Hide(true);
			}
		});
		uiButtonUser.OnClick += UserPressed;
		PlayerBankManager.instance.OnCoinUpdate += UpdateCoins;
		PlayerBankManager.instance.OnGemUpdate += UpdateGems;
		CurrentUser.instance.OnUpdate += UpdateAccount;
		UpdateAccount(CurrentUser.instance.account);
		uiTextGemCount.SetText(PlayerBankManager.instance.GetGemCount().ToString());
	}

	private void OnDestroy()
	{
		PlayerBankManager.instance.OnCoinUpdate -= UpdateCoins;
		CurrentUser.instance.OnUpdate -= UpdateAccount;
		PlayerBankManager.instance.OnGemUpdate -= UpdateGems;
		uiButtonUser.OnClick -= UserPressed;
	}

	public void UpdateGems(int gems)
	{
		uiTextGemCount.SetText(gems.ToString());
	}

	public void UpdateAccount(UserAccount acct)
	{
		if (acct != null)
		{
			uiTextUserName.SetText(acct.userName);
			if (acct.avatar != null)
			{
				uiRawImageUserAvatar.enabled = true;
				uiImageDefaultAvatar.enabled = false;
				uiRawImageUserAvatar.texture = CurrentUser.instance.account.avatar.UpdateTexture2D(ref userAvatarCover.colors, ref userAvatarCover.texture, userAvatarCover.style);
			}
			else
			{
				uiRawImageUserAvatar.enabled = false;
				uiImageDefaultAvatar.enabled = true;
			}
			if (acct.status == UserAccount.Status.unverified)
			{
				rectIncompleteIndicator.localScale = new Vector3(0.2f, 0.2f, 0.2f);
				rectParentApprovedIndicator.localScale = Vector3.zero;
			}
			else if (acct.status == UserAccount.Status.active && !acct.completed)
			{
				rectIncompleteIndicator.localScale = Vector3.zero;
				rectParentApprovedIndicator.localScale = Vector3.one;
			}
			else
			{
				rectIncompleteIndicator.localScale = Vector3.zero;
				rectParentApprovedIndicator.localScale = Vector3.zero;
			}
		}
		else
		{
			uiTextUserName.SetText("Guest");
			uiRawImageUserAvatar.enabled = false;
			uiImageDefaultAvatar.enabled = true;
			rectIncompleteIndicator.localScale = Vector3.zero;
			rectParentApprovedIndicator.localScale = Vector3.zero;
		}
	}

	public void UpdateCoins(int count)
	{
		PulseCoinCount();
		uiTextCoinCount.SetText(count.ToString("N0"));
	}

	public void ToggleInfinityView()
	{
		if (!infinityViewOn)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.iViewShow);
			CanvasStreamingInterface.instance.TransitionSquaresScale(1f, 0f, CanvasStreamingInterface.instance.originalSquareScale, CanvasStreamingInterface.instance.enlargedSquareScale, UIAnimationManager.speedMedium);
		}
		else
		{
			SoundManager.instance.PlaySound(SoundManager.instance.iViewHide);
			CanvasStreamingInterface.instance.TransitionSquaresScale(0f, 1f, CanvasStreamingInterface.instance.enlargedSquareScale, CanvasStreamingInterface.instance.originalSquareScale, UIAnimationManager.speedMedium);
		}
		infinityViewOn = !infinityViewOn;
		if (!PrefsManager.instance.hasSeenPinchInfinityViewTutorial)
		{
			PrefsManager.instance.hasSeenPinchInfinityViewTutorial = true;
		}
	}

	public IEnumerator ForceTurnOffInfinityView()
	{
		if (infinityViewOn)
		{
			CanvasStreamingInterface.instance.TransitionSquaresScale(0f, 1f, CanvasStreamingInterface.instance.enlargedSquareScale, CanvasStreamingInterface.instance.originalSquareScale, UIAnimationManager.speedMedium);
			infinityViewOn = !infinityViewOn;
			yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		}
	}

	public void InsufficientFunds()
	{
		uiTextCoinCount.transform.DOScale(1.5f, UIAnimationManager.speedFast).OnStart(delegate
		{
			uiTextCoinCount.color = cointCountTextInfufficient;
		}).OnComplete(delegate
		{
			uiTextCoinCount.transform.DOScale(startingCoinScale, UIAnimationManager.speedFast).OnComplete(delegate
			{
				uiTextCoinCount.color = coinCountTextBase;
			});
		});
	}

	public void PulseCoinCount()
	{
		uiTextCoinCount.transform.DOScale(1.5f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			uiTextCoinCount.transform.DOScale(startingCoinScale, UIAnimationManager.speedFast);
		});
	}

	public void UserPressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		if (CurrentUser.instance.account != null)
		{
			UIUserProfile.instance.InitWithAccount(CurrentUser.instance.account);
		}
		else
		{
			UINavMenu.instance.UserAction();
		}
		if (CanvasTileInfo.instance.isVisible)
		{
			CanvasTileInfo.instance.Hide(true);
		}
	}
}
