using System;
using Prime31;
using UnityEngine;

public class PlayerMovementController : MonoBehaviour
{
	[Serializable]
	public class MovementProperties
	{
		public float runSpeed;

		public float gravity;

		public float jumpHeight;

		public float maxFallSpeed;

		public float jetpackSpeed;

		public float jetpackThrust;
	}

	public GameObject jetpackPrefab;

	public ParticleSystem _jetpack;

	private ParticleSystem.Particle[] jetpackParticles;

	public float jetpackFallDampeningFactor = 0.25f;

	public bool doubleJump = true;

	public float groundDamping = 20f;

	public float inAirDamping = 5f;

	public float jumpHeight = 1.5f;

	public bool infiniteJumps;

	private float blockMultiplier = 13f;

	[HideInInspector]
	private float normalizedHorizontalSpeed;

	[HideInInspector]
	private float normalizedVerticalSpeed;

	private CharacterController2D _controller;

	private RaycastHit2D _lastControllerColliderHit;

	private Vector3 _velocity;

	private bool inWater;

	private bool _shouldMoveRight;

	private bool _shouldMoveLeft;

	private bool _shouldMoveUp;

	private bool _shouldMoveDown;

	private bool _shouldJump;

	private int _remainingJumps = 2;

	private Transform _selfTransform;

	public MovementProperties normalMovement;

	public MovementProperties normalWaterMovement;

	public MovementProperties shrunkenMovement;

	public MovementProperties shrunkenWaterMovement;

	public MovementProperties currentMovement;

	private bool isHoldingJump;

	private bool _canJump = true;

	public void Reset()
	{
		SetToStandardMovement();
		_velocity = Vector3.zero;
		_controller.velocity = Vector3.zero;
		_remainingJumps = 2;
	}

	public void GetJetpack()
	{
		if (_jetpack == null)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate(jetpackPrefab, _selfTransform.position, Quaternion.identity);
			_jetpack = gameObject.GetComponent<ParticleSystem>();
			jetpackParticles = new ParticleSystem.Particle[_jetpack.maxParticles];
			_jetpack.transform.SetParent(_selfTransform);
			_jetpack.transform.localPosition = new Vector3(0f, 0f, 0.001f);
			_jetpack.transform.localScale = Vector3.one;
		}
		_jetpack.gameObject.SetActive(true);
		PixelPlayerController.instance.playerHasJetpack = true;
	}

	public void LoseJetpack()
	{
		if (PixelPlayerController.instance.playerHasJetpack)
		{
			_shouldMoveUp = false;
			_shouldMoveDown = false;
			DeactivateJetpack();
			_jetpack.gameObject.SetActive(false);
			PixelPlayerController.instance.playerHasJetpack = false;
		}
	}

	public void ActivateJetpack()
	{
		if (PixelPlayerController.instance.playerHasJetpack && !PixelPlayerController.instance.jetpackActive)
		{
			SoundManager.instance.PlayLoopingSound(SoundManager.instance.jetpackThrustSFX);
			PixelPlayerController.instance.jetpackActive = true;
			if (!_controller.isGrounded)
			{
				_jetpack.emissionRate = 50f;
			}
		}
	}

	public void DeactivateJetpack()
	{
		if (PixelPlayerController.instance.jetpackActive)
		{
			SoundManager.instance.StopLoopingSound();
		}
		if (PixelPlayerController.instance.playerHasJetpack && PixelPlayerController.instance.jetpackActive)
		{
			PixelPlayerController.instance.jetpackActive = false;
			_jetpack.emissionRate = 0f;
			if (inWater)
			{
				_remainingJumps = 1;
			}
		}
	}

	private void OnEnable()
	{
		GetComponent<Rigidbody2D>().velocity = Vector2.zero;
		GetComponent<Rigidbody2D>().angularVelocity = 0f;
		PPInputController.OnButton += HandleOnButton;
		PPInputController.OnButtonReleased += HandleOnButtonReleased;
		PPInputController.OnAxisL += HandleOnAxisL;
	}

	private void Awake()
	{
		_selfTransform = base.transform;
		_controller = GetComponent<CharacterController2D>();
		_controller.onControllerCollidedEvent += onControllerCollider;
		SetToStandardMovement();
	}

	private void OnDisable()
	{
		PPInputController.OnButton -= HandleOnButton;
		PPInputController.OnButtonReleased -= HandleOnButtonReleased;
		PPInputController.OnAxisL -= HandleOnAxisL;
	}

	private void HandleOnAxisR(Vector2 vector)
	{
		if (PixelPlayerController.instance.jetpackActive)
		{
			if (vector.y < 0f)
			{
				_shouldMoveDown = true;
				_shouldMoveUp = false;
			}
			else if (vector.y > 0f)
			{
				_shouldMoveUp = true;
				_shouldMoveDown = false;
			}
		}
	}

	private void HandleOnAxisL(Vector2 vector)
	{
		if (vector.x < 0f)
		{
			_shouldMoveLeft = true;
			_shouldMoveRight = false;
		}
		else if (vector.x > 0f)
		{
			_shouldMoveRight = true;
			_shouldMoveLeft = false;
		}
		else
		{
			_shouldMoveLeft = false;
			_shouldMoveRight = false;
		}
		if (PixelPlayerController.instance.playerHasJetpack)
		{
			if (vector.y > 0f)
			{
				_shouldMoveUp = true;
				_shouldMoveDown = false;
			}
			else if (vector.y < 0f)
			{
				_shouldMoveDown = true;
				_shouldMoveUp = false;
			}
			else
			{
				_shouldMoveDown = false;
				_shouldMoveUp = false;
			}
		}
		else
		{
			_shouldMoveUp = false;
			_shouldMoveDown = false;
		}
	}

	private void HandleOnButton(PPInputController.InputButton button)
	{
		if (button.Equals(PPInputController.InputButton.Right1) && !isHoldingJump)
		{
			isHoldingJump = true;
			_shouldJump = true;
		}
	}

	private void HandleOnButtonReleased(PPInputController.InputButton button)
	{
		if (button.Equals(PPInputController.InputButton.Right1))
		{
			isHoldingJump = false;
		}
	}

	private void onControllerCollider(RaycastHit2D hit)
	{
		if (hit.normal.y != 1f)
		{
		}
	}

	public void EnableWaterProperties()
	{
		inWater = true;
		_remainingJumps = 1;
		if (PixelPlayerController.instance.selfTransform.localScale.y < 0.5f)
		{
			currentMovement = shrunkenWaterMovement;
		}
		else
		{
			currentMovement = normalWaterMovement;
		}
		_controller.velocity = new Vector3(_controller.velocity.x * 0.2f, _controller.velocity.y * 0.2f, _controller.velocity.z * 0.2f);
	}

	public void DisableWaterProperties()
	{
		inWater = false;
		if (PixelPlayerController.instance.selfTransform.localScale.y < 0.5f)
		{
			currentMovement = shrunkenMovement;
		}
		else
		{
			currentMovement = normalMovement;
		}
	}

	public void SetToShrunkenMovement()
	{
		if (PixelPlayerController.instance.waterDetector.CheckForWater())
		{
			currentMovement = shrunkenWaterMovement;
		}
		else
		{
			currentMovement = shrunkenMovement;
		}
	}

	public void SetToStandardMovement()
	{
		if (PixelPlayerController.instance.waterDetector.CheckForWater())
		{
			currentMovement = normalWaterMovement;
		}
		else
		{
			currentMovement = normalMovement;
		}
	}

	private void Update()
	{
		if (_controller.isGrounded)
		{
			_velocity.y = 0f;
			if (doubleJump)
			{
				_remainingJumps = 2;
			}
			else
			{
				_remainingJumps = 1;
			}
			if (PixelPlayerController.instance.jetpackActive)
			{
				DeactivateJetpack();
			}
		}
		else if (_controller.collisionState.wasGroundedLastFrame)
		{
			_remainingJumps = 1;
		}
		else
		{
			PixelPlayerController.instance.airTime += Time.deltaTime;
		}
		_canJump = _remainingJumps > 0;
		if (!_canJump)
		{
			_shouldJump = false;
		}
		if (_shouldMoveRight)
		{
			if (_selfTransform.localScale.x < 0f)
			{
				_selfTransform.localScale = new Vector3(0f - _selfTransform.localScale.x, _selfTransform.localScale.y, _selfTransform.localScale.z);
			}
			normalizedHorizontalSpeed = 1f;
			_shouldMoveRight = false;
		}
		else if (_shouldMoveLeft)
		{
			if (_selfTransform.localScale.x > 0f)
			{
				_selfTransform.localScale = new Vector3(0f - _selfTransform.localScale.x, _selfTransform.localScale.y, _selfTransform.localScale.z);
			}
			normalizedHorizontalSpeed = -1f;
			_shouldMoveLeft = false;
		}
		else
		{
			normalizedHorizontalSpeed = 0f;
		}
		if (_shouldJump)
		{
			_velocity.y = Mathf.Sqrt(2f * (currentMovement.jumpHeight * blockMultiplier) * (0f - currentMovement.gravity));
			if (!inWater && !infiniteJumps)
			{
				_remainingJumps--;
			}
			SoundManager.instance.PlaySound(SoundManager.instance.playerJump);
		}
		if (PixelPlayerController.instance.playerHasJetpack && PixelPlayerController.instance.jetpackActive)
		{
			if (_shouldMoveUp)
			{
				normalizedVerticalSpeed = 1f;
			}
			else if (_shouldMoveDown)
			{
				normalizedVerticalSpeed = -1f;
			}
			else
			{
				normalizedVerticalSpeed = 0f;
			}
			float num = 0f;
			if (PixelPlayerController.instance.usingJetPackTouchControl || _shouldMoveUp || _shouldMoveDown || _shouldMoveLeft || _shouldMoveRight)
			{
				if (PixelPlayerController.instance.usingJetPackTouchControl)
				{
					normalizedVerticalSpeed = 1f;
					if (normalizedHorizontalSpeed != 0f)
					{
						normalizedHorizontalSpeed = Mathf.Sign(normalizedHorizontalSpeed);
					}
				}
				PixelPlayerController.instance.BurnJetFuel();
				num = currentMovement.jetpackThrust;
				_remainingJumps = 0;
			}
			float num2 = inAirDamping;
			_velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * currentMovement.jetpackSpeed, Time.deltaTime * num2);
			_velocity.y = Mathf.Lerp(_velocity.y, normalizedVerticalSpeed * (currentMovement.jetpackSpeed + num), Time.deltaTime * num2);
			_velocity.y += currentMovement.gravity * Time.deltaTime * jetpackFallDampeningFactor;
			Vector3 velocity = -_velocity * _selfTransform.localScale.y;
			if (normalizedHorizontalSpeed != 0f)
			{
				velocity = new Vector3(velocity.x * 2f, velocity.y, 0f);
			}
			if (normalizedVerticalSpeed > 0f)
			{
				velocity = new Vector3(velocity.x, velocity.y * 2f, 0f);
			}
			if (_controller.isGrounded)
			{
				velocity = new Vector3(velocity.x, 0f, 0f);
			}
			if (normalizedVerticalSpeed == 0f && normalizedHorizontalSpeed == 0f)
			{
				if (_jetpack.emissionRate != 50f)
				{
					_jetpack.emissionRate = 50f;
				}
			}
			else if (_jetpack.emissionRate != 175f)
			{
				_jetpack.emissionRate = 175f;
			}
			int particles = _jetpack.GetParticles(jetpackParticles);
			for (int i = 0; i < particles; i++)
			{
				jetpackParticles[i].velocity = velocity;
			}
			_jetpack.SetParticles(jetpackParticles, particles);
		}
		else
		{
			float num3 = ((!_controller.isGrounded) ? inAirDamping : groundDamping);
			_velocity.x = Mathf.Lerp(_velocity.x, normalizedHorizontalSpeed * currentMovement.runSpeed, Time.deltaTime * num3);
			if (!_controller.isGrounded && _velocity.y > 0f && !isHoldingJump)
			{
				_velocity.y += 3f * currentMovement.gravity * Time.deltaTime;
				if (_velocity.y < 0f)
				{
					_velocity.y = 0f;
				}
			}
			_velocity.y += currentMovement.gravity * Time.deltaTime;
		}
		if (PixelPlayerController.instance.isInvincible)
		{
			float gravityModifier = PixelPlayerController.instance.invincibilityParticles.gravityModifier;
			float num4 = 0.2f * PixelPlayerController.instance.selfTransform.localScale.y;
			if (normalizedHorizontalSpeed == 0f && normalizedVerticalSpeed == 0f && gravityModifier != num4)
			{
				PixelPlayerController.instance.invincibilityParticles.gravityModifier = num4;
			}
			else if (gravityModifier != 0f)
			{
				PixelPlayerController.instance.invincibilityParticles.gravityModifier = 0f;
			}
		}
		_controller.move(_velocity * Time.deltaTime);
		_velocity = _controller.velocity;
		if (!_controller.isGrounded)
		{
			_shouldJump = false;
		}
		if (_controller.isGrounded)
		{
			if (normalizedHorizontalSpeed != 0f)
			{
				PixelPlayerController.instance.animationController.SetAnimationState(AnimationType.Walk);
			}
			else
			{
				PixelPlayerController.instance.animationController.SetAnimationState(AnimationType.Idle);
			}
		}
		else
		{
			PixelPlayerController.instance.animationController.SetAnimationState(AnimationType.Jump);
		}
		if (_velocity.y < currentMovement.maxFallSpeed)
		{
			_velocity.y = currentMovement.maxFallSpeed;
		}
		_shouldMoveLeft = false;
		_shouldMoveRight = false;
		_shouldMoveUp = false;
		_shouldMoveDown = false;
	}

	public void StopMoving()
	{
		_velocity = Vector3.zero;
	}
}
