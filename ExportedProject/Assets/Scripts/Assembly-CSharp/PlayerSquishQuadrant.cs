using UnityEngine;

public class PlayerSquishQuadrant : MonoBehaviour
{
	private PlayerSquishTrigger squishTrigger;

	public BoxCollider2D boxCollider;

	public Direction direction;

	public Vector3 tilePosition;

	public BlockColor[,] blocksLastHit;

	private void Awake()
	{
		squishTrigger = base.transform.parent.GetComponent<PlayerSquishTrigger>();
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		BloxelTile component = other.attachedRigidbody.gameObject.GetComponent<BloxelTile>();
		if (!(component == null))
		{
			HandleHit(component);
		}
	}

	private void OnTriggerStay2D(Collider2D other)
	{
		BloxelTile component = other.attachedRigidbody.gameObject.GetComponent<BloxelTile>();
		if (!(component == null))
		{
			HandleHit(component);
		}
	}

	private void HandleHit(BloxelTile tile)
	{
		squishTrigger.hitCount++;
		tilePosition = tile.selfTransform.localPosition - new Vector3(0.5f, 0.5f, 0f);
		blocksLastHit = tile.tileRenderer.currentFrameChunk.blockColors;
		switch (direction)
		{
		case Direction.UpLeft:
			squishTrigger.tlHit = true;
			break;
		case Direction.UpRight:
			squishTrigger.trHit = true;
			break;
		case Direction.DownRight:
			squishTrigger.brHit = true;
			break;
		case Direction.DownLeft:
			squishTrigger.blHit = true;
			break;
		}
	}
}
