using UnityEngine;

public class PlayerSquishTrigger : MonoBehaviour
{
	public PlayerSquishQuadrant topLeft;

	public PlayerSquishQuadrant topRight;

	public PlayerSquishQuadrant bottomRight;

	public PlayerSquishQuadrant bottomLeft;

	private BoxCollider2D characterCollider;

	private float halfCharacterWidth;

	private float halfCharacterHeight;

	private Vector2 squishSize;

	[HideInInspector]
	public int hitCount;

	[HideInInspector]
	public bool tlHit;

	[HideInInspector]
	public bool trHit;

	[HideInInspector]
	public bool brHit;

	[HideInInspector]
	public bool blHit;

	private float scaleReductionFactor = 0.95f;

	private Vector3 currentQuadrantOrigin;

	public void SetUpColliderSizesAndPositions()
	{
		characterCollider = PixelPlayerController.instance.boxCollider;
		halfCharacterWidth = characterCollider.size.x / 2f;
		halfCharacterHeight = characterCollider.size.y / 2f;
		squishSize = new Vector2(halfCharacterWidth, halfCharacterHeight) * scaleReductionFactor;
		float num = squishSize.x / 2f;
		float num2 = squishSize.y / 2f;
		SetColliderProperties(topLeft.boxCollider, 0f - num, num2);
		SetColliderProperties(topRight.boxCollider, num, num2);
		SetColliderProperties(bottomRight.boxCollider, num, 0f - num2);
		SetColliderProperties(bottomLeft.boxCollider, 0f - num, 0f - num2);
	}

	private void SetColliderProperties(BoxCollider2D collider, float xOffset, float yOffset)
	{
		collider.size = squishSize;
		collider.offset = new Vector2(characterCollider.offset.x + xOffset, characterCollider.offset.y + yOffset);
	}

	private void FixedUpdate()
	{
		if (hitCount != 0)
		{
			if (hitCount > 3)
			{
				PixelPlayerController.instance.health = 0;
				ResetQuadrants();
			}
			else
			{
				ResetQuadrants();
			}
		}
	}

	private void ResetQuadrants()
	{
		hitCount = 0;
		tlHit = false;
		trHit = false;
		brHit = false;
		blHit = false;
		topLeft.blocksLastHit = null;
		topRight.blocksLastHit = null;
		bottomRight.blocksLastHit = null;
		bottomLeft.blocksLastHit = null;
	}

	private void MovePlayerLeft()
	{
		if (bottomRight.blocksLastHit == null)
		{
			return;
		}
		float num = 0f;
		int num2 = Mathf.Clamp(Mathf.CeilToInt(currentQuadrantOrigin.x + characterCollider.size.x / 2f - bottomRight.tilePosition.x), 0, 12);
		int num3 = Mathf.Clamp(Mathf.FloorToInt(currentQuadrantOrigin.x - bottomRight.tilePosition.x), 0, 12);
		int num4 = num2;
		int num5 = Mathf.Clamp(Mathf.CeilToInt(currentQuadrantOrigin.y - bottomRight.tilePosition.y), 0, 12);
		int num6 = Mathf.Clamp(Mathf.FloorToInt(currentQuadrantOrigin.y - characterCollider.size.y / 2f - bottomRight.tilePosition.y), 0, 12);
		for (int i = num3; i <= num2; i++)
		{
			bool flag = false;
			for (int j = num6; j <= num5; j++)
			{
				BlockColor blockColor = bottomRight.blocksLastHit[i, j];
				if (blockColor != BlockColor.Blank)
				{
					flag = true;
					num = i;
					break;
				}
			}
			if (flag)
			{
				break;
			}
		}
		float x = bottomRight.tilePosition.x + num - characterCollider.size.x / 2f;
		PixelPlayerController.instance.selfTransform.localPosition = new Vector3(x, PixelPlayerController.instance.selfTransform.localPosition.y, PixelPlayerController.instance.selfTransform.localPosition.z);
		currentQuadrantOrigin = PixelPlayerController.instance.selfTransform.localPosition + new Vector3(characterCollider.offset.x, characterCollider.offset.y, 0f);
	}

	private void MovePlayerUp()
	{
		if (bottomRight.blocksLastHit == null)
		{
			return;
		}
		float y = 0f;
		int value = Mathf.CeilToInt(currentQuadrantOrigin.y - characterCollider.size.y / 2f - bottomRight.tilePosition.y);
		value = Mathf.Clamp(value, 0, 12);
		int num = value;
		int value2 = Mathf.CeilToInt(currentQuadrantOrigin.x + characterCollider.size.x / 2f - bottomRight.tilePosition.x);
		value2 = Mathf.Clamp(value2, 0, 13);
		for (int i = value; i < 13; i++)
		{
			bool flag = false;
			for (int j = 0; j < value2; j++)
			{
				BlockColor blockColor = bottomRight.blocksLastHit[j, i];
				if (blockColor != BlockColor.Blank)
				{
					flag = true;
					num = i;
					break;
				}
			}
			if (!flag)
			{
				y = bottomRight.tilePosition.y + (float)num + 1f;
				break;
			}
		}
		PixelPlayerController.instance.selfTransform.localPosition = new Vector3(PixelPlayerController.instance.selfTransform.localPosition.x, y, PixelPlayerController.instance.selfTransform.localPosition.z);
		PixelPlayerController.instance.warpToGrounded();
		currentQuadrantOrigin = PixelPlayerController.instance.selfTransform.localPosition + new Vector3(characterCollider.offset.x, characterCollider.offset.y, 0f);
	}
}
