using UnityEngine;

public class PlayerWaterDetector : MonoBehaviour
{
	public PixelPlayerController playerController;

	public CircleCollider2D circleCollider;

	public float radius;

	public bool CheckForWater()
	{
		Vector3 position = base.transform.position;
		float y = playerController.selfTransform.localScale.y;
		Collider2D collider2D = Physics2D.OverlapCircle(new Vector2(position.x + circleCollider.offset.x * y, position.y + circleCollider.offset.y * y), circleCollider.radius * y, 1 << LayerMask.NameToLayer("Water"), position.z - 1f, position.z + 1f);
		if ((bool)collider2D)
		{
			return true;
		}
		return false;
	}

	private void OnTriggerEnter2D(Collider2D other)
	{
		if (other.gameObject.layer == 4)
		{
			playerController.EnterWater();
		}
	}

	private void OnTriggerExit2D(Collider2D other)
	{
		playerController.ExitWater();
	}
}
