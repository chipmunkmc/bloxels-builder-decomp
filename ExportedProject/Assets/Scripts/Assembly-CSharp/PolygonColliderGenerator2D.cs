using System;
using System.Collections.Generic;
using UnityEngine;

public static class PolygonColliderGenerator2D
{
	private struct CoordinatePair
	{
		public int x;

		public int y;

		public static CoordinatePair InvalidLocation = new CoordinatePair(-1, -1);

		public CoordinatePair(int x, int y)
		{
			this.x = x;
			this.y = y;
		}
	}

	private static CoordinatePair[] vertexOffsetsForDirection = new CoordinatePair[4]
	{
		new CoordinatePair(-1, 0),
		new CoordinatePair(0, 1),
		new CoordinatePair(1, 0),
		new CoordinatePair(0, -1)
	};

	private static CoordinatePair[] leftBlockOffsetsForDirection = new CoordinatePair[4]
	{
		new CoordinatePair(-1, -1),
		new CoordinatePair(-1, 0),
		new CoordinatePair(0, 0),
		new CoordinatePair(0, -1)
	};

	private static CoordinatePair[] rightBlockOffsetsForDirection = new CoordinatePair[4]
	{
		new CoordinatePair(-1, 0),
		new CoordinatePair(0, 0),
		new CoordinatePair(0, -1),
		new CoordinatePair(-1, -1)
	};

	public static int gridSize = 13;

	public static int paddedGridSize = gridSize + 2;

	private static int stackDepth = 7;

	private static int currentStackIndex;

	private static bool[][,] checkedSpacesStack = new bool[7][,]
	{
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize]
	};

	private static bool[,] currentCheckedSpaces;

	private static bool[][,] occupiedSpacesStack = new bool[7][,]
	{
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize],
		new bool[paddedGridSize, paddedGridSize]
	};

	private static bool[,] currentOccupiedSpaces;

	private static bool[,] checkedRowStartPoints = new bool[paddedGridSize, paddedGridSize];

	private static bool[,] checkedRowEndPoints = new bool[paddedGridSize, paddedGridSize];

	private static bool tracingBoardEdge;

	private static List<Vector2[]> currentPathList;

	private static Vector2[] path = new Vector2[196];

	private static int pathVertexCount;

	private static CoordinatePair startingVertex;

	private static CoordinatePair currentVertex;

	private static CoordinatePair nextVertex;

	private static int originalDirection;

	private static int currentDirection;

	public static void GenerateColliderForCache(BlockColor[,] blockColors, List<Vector2[]> pathList)
	{
		currentPathList = pathList;
		currentPathList.Clear();
		currentStackIndex = 0;
		currentOccupiedSpaces = occupiedSpacesStack[currentStackIndex];
		for (int i = 0; i < gridSize; i++)
		{
			for (int j = 0; j < gridSize; j++)
			{
				currentOccupiedSpaces[j + 1, i + 1] = blockColors[j, i] != BlockColor.Blank;
			}
		}
		FindNextIsland();
	}

	private static void FindNextIsland()
	{
		currentCheckedSpaces = checkedSpacesStack[currentStackIndex];
		currentOccupiedSpaces = occupiedSpacesStack[currentStackIndex];
		Array.Clear(currentCheckedSpaces, 0, paddedGridSize * paddedGridSize);
		Array.Clear(checkedRowStartPoints, 0, paddedGridSize * paddedGridSize);
		Array.Clear(checkedRowEndPoints, 0, paddedGridSize * paddedGridSize);
		for (int i = 1; i < paddedGridSize - 1; i++)
		{
			for (int j = 1; j < paddedGridSize - 1; j++)
			{
				if (!currentOccupiedSpaces[j, i] || currentCheckedSpaces[j, i])
				{
					continue;
				}
				startingVertex.x = j;
				startingVertex.y = i;
				if (!FindVertexPath())
				{
					continue;
				}
				Vector2[] array = new Vector2[pathVertexCount];
				Array.Copy(path, array, pathVertexCount);
				currentPathList.Add(array);
				if (currentStackIndex >= stackDepth - 1)
				{
					continue;
				}
				bool flag = false;
				bool flag2 = false;
				bool[,] array2 = occupiedSpacesStack[currentStackIndex + 1];
				for (int k = 1; k < paddedGridSize - 1; k++)
				{
					for (int l = 1; l < paddedGridSize - 1; l++)
					{
						if (checkedRowStartPoints[l, k])
						{
							for (; !checkedRowEndPoints[l, k]; l++)
							{
								currentCheckedSpaces[l, k] = true;
								flag |= (array2[l, k] = !currentOccupiedSpaces[l, k]);
							}
							currentCheckedSpaces[l, k] = true;
							flag |= (array2[l, k] = !currentOccupiedSpaces[l, k]);
						}
						else
						{
							array2[l, k] = false;
						}
					}
				}
				if (flag)
				{
					currentStackIndex++;
					FindNextIsland();
					currentCheckedSpaces = checkedSpacesStack[currentStackIndex];
					currentOccupiedSpaces = occupiedSpacesStack[currentStackIndex];
				}
				else
				{
					Array.Clear(checkedRowStartPoints, 0, paddedGridSize * paddedGridSize);
					Array.Clear(checkedRowEndPoints, 0, paddedGridSize * paddedGridSize);
				}
			}
		}
		currentStackIndex--;
	}

	private static bool FindVertexPath()
	{
		pathVertexCount = 0;
		currentVertex.x = startingVertex.x;
		currentVertex.y = startingVertex.y;
		originalDirection = 0;
		currentDirection = 0;
		while (true)
		{
			currentDirection = (originalDirection + 4 - 1) % 4;
			for (int i = 0; i < 3; i++)
			{
				if (NextVertexIsValid())
				{
					if (currentDirection != originalDirection)
					{
						path[pathVertexCount++] = new Vector2(currentVertex.x - 1, currentVertex.y - 1);
						originalDirection = currentDirection;
					}
					currentVertex.x = nextVertex.x;
					currentVertex.y = nextVertex.y;
					if (startingVertex.x == currentVertex.x && startingVertex.y == currentVertex.y)
					{
						return true;
					}
					break;
				}
				currentDirection = (currentDirection + 1) % 4;
			}
		}
	}

	private static bool NextVertexIsValid()
	{
		CoordinatePair coordinatePair = leftBlockOffsetsForDirection[currentDirection];
		CoordinatePair coordinatePair2 = new CoordinatePair(currentVertex.x + coordinatePair.x, currentVertex.y + coordinatePair.y);
		CoordinatePair coordinatePair3 = rightBlockOffsetsForDirection[currentDirection];
		CoordinatePair coordinatePair4 = new CoordinatePair(currentVertex.x + coordinatePair3.x, currentVertex.y + coordinatePair3.y);
		if (!currentOccupiedSpaces[coordinatePair2.x, coordinatePair2.y] && currentOccupiedSpaces[coordinatePair4.x, coordinatePair4.y])
		{
			if (currentDirection == 1)
			{
				checkedRowStartPoints[coordinatePair4.x, coordinatePair4.y] = true;
			}
			else if (currentDirection == 3)
			{
				checkedRowEndPoints[coordinatePair4.x, coordinatePair4.y] = true;
			}
			CoordinatePair coordinatePair5 = vertexOffsetsForDirection[currentDirection];
			nextVertex.x = currentVertex.x + coordinatePair5.x;
			nextVertex.y = currentVertex.y + coordinatePair5.y;
			return true;
		}
		return false;
	}
}
