using UnityEngine;

public abstract class PoolableComponent : MonoBehaviour
{
	public bool spawned;

	public Transform selfTransform;

	public abstract void EarlyAwake();

	public abstract void PrepareSpawn();

	public abstract void PrepareDespawn();

	public abstract void Despawn();
}
