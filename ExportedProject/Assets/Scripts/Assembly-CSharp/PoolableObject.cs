public abstract class PoolableObject
{
	public bool spawned;

	public abstract void EarlyAwake();

	public abstract void PrepareSpawn();

	public abstract void PrepareDespawn();

	public abstract void Despawn();
}
