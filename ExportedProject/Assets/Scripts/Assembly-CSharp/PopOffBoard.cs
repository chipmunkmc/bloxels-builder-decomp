using UnityEngine;
using UnityEngine.UI;

public class PopOffBoard : MonoBehaviour
{
	public Color uiBoardColor;

	public RawImage image;

	public Canvas primaryCanvas;

	public BloxelProject project;

	public Color boardColor;

	public RectTransform rect;

	public Vector3 scale = new Vector3(0.3136f, 0.3136f, 1f);

	private CoverBoard _cover;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		image = GetComponent<RawImage>();
		_cover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	private void Start()
	{
		BuildCover();
	}

	private void Update()
	{
		base.transform.position = PPUtilities.GetPositionInCanvasFromMousePosition(primaryCanvas, primaryCanvas.worldCamera);
	}

	public void BuildCover()
	{
		image.texture = project.coverBoard.UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
	}
}
