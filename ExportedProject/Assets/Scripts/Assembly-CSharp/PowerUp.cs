using DG.Tweening;
using UnityEngine;

public abstract class PowerUp : MonoBehaviour
{
	public Vector3 basePos;

	public PowerUpType powerUpType;

	public void Start()
	{
		basePos = base.transform.position;
		base.transform.localScale = new Vector3(0.3f, 0.3f, 1f);
	}

	public void Bounce()
	{
		base.transform.DOMove(new Vector3(basePos.x, basePos.y + 2f, basePos.z), UIAnimationManager.speedMedium).SetEase(Ease.InOutSine).SetLoops(-1, LoopType.Yoyo);
	}

	public virtual void Collect()
	{
		SoundManager.PlayOneShot(SoundManager.instance.powerUpSFX);
		CreatePlayerPowerUpIndicator(powerUpType);
		if (this is InventoryItem)
		{
			((InventoryItem)this).AddItemToInventory();
		}
	}

	protected void CreatePlayerPowerUpIndicator(PowerUpType type)
	{
		PowerupIndicator powerupIndicator = GameplayPool.SpawnPowerupIndicator();
		powerupIndicator.Init(type, PixelPlayerController.instance.selfTransform);
	}
}
