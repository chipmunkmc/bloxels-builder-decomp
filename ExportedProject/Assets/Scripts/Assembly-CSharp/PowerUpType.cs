public enum PowerUpType : byte
{
	Bomb = 0,
	Health = 1,
	Dash = 2,
	BigBro = 3,
	Jetpack = 4,
	Shrink = 5,
	Invincibility = 6,
	Map = 7
}
