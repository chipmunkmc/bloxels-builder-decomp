using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public sealed class PowerupIndicator : PoolableComponent
{
	private static List<PowerupIndicator> _currentIndicators = new List<PowerupIndicator>(4);

	private static Color _clearColor = new Color(1f, 1f, 1f, 0f);

	private static Color _startColor = new Color(1f, 1f, 1f, 1f);

	private static float _lerpFactorSlow = 5f;

	private static float _lerpFactorFast = 7.5f;

	public PowerUpType type;

	private Transform _targetTransform;

	public SpriteRenderer renderer;

	private static float _initialOffset = 16f;

	private float _yOffsetTarget = 16f;

	private float _yOffset = 16f;

	private static Vector3 _originalScale;

	private float _scaleFactor;

	private Tweener _pulseTween;

	private Action _updateAction;

	private float _expirationTime;

	public static void InitStaticValues(PowerupIndicator template)
	{
		_originalScale = template.selfTransform.localScale;
	}

	public override void EarlyAwake()
	{
	}

	public override void PrepareSpawn()
	{
		spawned = true;
		selfTransform.parent = WorldWrapper.instance.runtimeObjectContainer;
		_yOffsetTarget = _initialOffset;
		_yOffset = _initialOffset;
		for (int i = 0; i < _currentIndicators.Count; i++)
		{
			PowerupIndicator powerupIndicator = _currentIndicators[i];
			powerupIndicator._yOffsetTarget += 5.5f;
		}
		_currentIndicators.Add(this);
		_pulseTween = DOTween.To(() => 1f, delegate(float x)
		{
			_scaleFactor = x;
		}, 1.3f, UIAnimationManager.speedMedium).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.InOutSine);
		base.gameObject.SetActive(true);
	}

	public override void PrepareDespawn()
	{
		spawned = false;
		_currentIndicators.Remove(this);
		base.gameObject.SetActive(false);
	}

	public override void Despawn()
	{
		GameplayPool.DespawnPowerupIndicator(this);
	}

	private void Update()
	{
		_updateAction();
	}

	private void HoverOverPlayer()
	{
		if (Time.time > _expirationTime)
		{
			_expirationTime += 1.75f;
			if (type == PowerUpType.Health || type == PowerUpType.Invincibility)
			{
				_updateAction = RemoveIndicator;
			}
			else
			{
				_updateAction = MoveToInventory;
			}
			_pulseTween.Kill();
		}
		else
		{
			_yOffset = Mathf.Lerp(_yOffset, _yOffsetTarget * _targetTransform.localScale.y, Time.deltaTime * _lerpFactorFast);
			selfTransform.position = _targetTransform.position + new Vector3(0f, _yOffset, 0f);
			selfTransform.localScale = _originalScale * _scaleFactor * _targetTransform.localScale.y;
		}
	}

	private void MoveToInventory()
	{
		if (Time.time > _expirationTime)
		{
			Despawn();
			return;
		}
		Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.activeController.cam, new Vector3(-10f, Screen.height + 10, 0f), WorldWrapper.WorldZ, false);
		selfTransform.position = Vector3.Lerp(selfTransform.position, worldPositionOnXYPlane, Time.deltaTime * _lerpFactorSlow);
		renderer.color = Color.Lerp(renderer.color, _clearColor, Time.deltaTime * _lerpFactorSlow);
	}

	private void RemoveIndicator()
	{
		if (Time.time > _expirationTime)
		{
			Despawn();
			return;
		}
		selfTransform.localScale = Vector3.Lerp(selfTransform.localScale, Vector3.zero, Time.deltaTime * _lerpFactorFast);
		_yOffset = Mathf.Lerp(_yOffset, _yOffsetTarget * _targetTransform.localScale.y, Time.deltaTime * _lerpFactorFast);
		selfTransform.position = _targetTransform.position + new Vector3(0f, _yOffset, 0f);
	}

	public void Init(PowerUpType _type, Transform targetTransform)
	{
		type = _type;
		_targetTransform = targetTransform;
		renderer.sprite = GameplayController.powerupSprites[type];
		renderer.color = _startColor;
		_updateAction = HoverOverPlayer;
		_expirationTime = Time.time + 1.75f;
		_yOffset = _yOffsetTarget * targetTransform.localScale.y;
	}
}
