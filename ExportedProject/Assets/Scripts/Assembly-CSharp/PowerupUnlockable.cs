public class PowerupUnlockable : GemUnlockable
{
	public PowerUpType powerupType;

	public PowerupUnlockable(PowerUpType _powerupType)
	{
		powerupType = _powerupType;
		cost = GemUnlockable.PowerupCost;
		type = Type.PowerupType;
		uiBuyable = UIBuyable.CreatePowerup(powerupType);
	}

	public override bool IsLocked()
	{
		return GemManager.instance.IsPowerupLocked(powerupType);
	}

	public override void Unlock()
	{
		GemManager.instance.UnlockPowerup(powerupType);
		EventUnlock(this);
	}
}
