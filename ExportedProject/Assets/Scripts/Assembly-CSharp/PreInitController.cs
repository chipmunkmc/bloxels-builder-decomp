using UnityEngine;

public class PreInitController : MonoBehaviour
{
	private void Awake()
	{
		if (SystemInfo.systemMemorySize < 750)
		{
			DeviceManager.isPoopDevice = true;
			QualitySettings.masterTextureLimit = 1;
			QualitySettings.DecreaseLevel();
		}
		else
		{
			QualitySettings.masterTextureLimit = 0;
		}
	}

	private void Start()
	{
		Application.LoadLevel("GameInit");
	}
}
