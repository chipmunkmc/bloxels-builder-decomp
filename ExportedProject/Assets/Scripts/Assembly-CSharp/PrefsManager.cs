using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class PrefsManager : MonoBehaviour
{
	public delegate void HandleUpdateNews();

	public static PrefsManager instance;

	private const string prefsPassword = "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g";

	private const string keyFor_iWallWarpLocation = "iWallWarpLocation";

	private const string keyFor_shouldActivateAfterWarp = "shouldActivateAfterWarp";

	private const string keyFor_hasOpenedApp = "hasOpenedApp";

	private const string keyFor_hasSeenIWall = "hasSeenIWall";

	private const string keyFor_appInitCoinBonusAwarded = "appInitCoinBonusAwarded";

	private const string keyFor_playerCoins = "playerCoins";

	private const string keyFor_playerGemCount = "dietCoke";

	private const string keyFor_userAccount = "userAccount";

	private const string keyFor_userOwnsBoard = "userOwnsBoard";

	private const string keyFor_hasSeenStartPositionHelp = "hasSeenStartPositionHelp";

	private const string keyFor_cameraCheckDone = "CameraCheckDone";

	private const string keyFor_cameraAccessGranted = "CameraAccessGranted";

	private const string keyFor_hasSeenEditor = "hasSeenEditor";

	private const string keyFor_hasSeenGameMap = "hasSeenGameMap";

	private const string keyFor_hasSeenTags = "hasSeenTags";

	private const string keyFor_hasSeenGameBuilderTour = "hasSeenGameBuilderTour";

	private const string keyFor_hasSeenCaptureTour = "hasSeenCaptureTour";

	private const string keyFor_hasSeenCaptureSuccess = "hasSeenCaptureSuccess";

	private const string keyFor_hasSeenQuickStartTour = "hasSeenQuickStartTour";

	private const string keyFor_hasSeenCharacterTour = "hasSeenCharacterTour";

	private const string keyFor_hasSeenAnimatorTour = "hasSeenAnimatorTour";

	private const string keyFor_hasSeenBoardBuilderTour = "hasSeenBoardBuilderTour";

	private const string keyFor_hasSeeniWallTour = "hasSeeniWallTour";

	private const string keyFor_hasSeenMegaBoardTour = "hasSeenMegaBoardTour";

	private const string keyFor_hasSeenPinchZoomTutorial = "hasSeenPinchZoomTutorial";

	private const string keyFor_hasSeenPinchInfinityViewTutorial = "hasSeenPinchInfinityViewTutorial";

	private const string keyFor_hasInstalledPoltryPanic = "hasInstalledPoultryPanic";

	private const string keyFor_hasInstalledDefaultAssetsV1 = "hasInstalledDefaultAssetsV1";

	private const string keyFor_hasSetupDefaultEnemyTypes = "hasSetupDefaultEnemyTypes";

	private const string keyFor_hasSetupDefaultPowerups = "hasSetupDefaultPowerups";

	private const string keyFor_hasSetupDefaultBrainSlots = "hasSetupDefaultBrainSlots";

	private const string keyFor_hasSetupDefaultMapRooms = "hasSetupDefaultMapRooms";

	private const string keyFor_hasReceivedEarlyAdopterRooms = "hasReceivedEarlyAdopterRooms";

	private const string keyFor_hasSetupDefaultAnimationFrames = "hasSetupDefaultAnimationFrames";

	private const string keyFor_shouldDisableGuide = "shouldDisableGuide";

	private const string keyFor_hasSeenGemInfo = "hasSeenGemInfo";

	private const string keyFor_userToken = "requestToken";

	private const string keyFor_preferredLanguage = "preferredLanguage";

	private const string keyFor_hasSentDeviceInfo = "hasSentDeviceInfo";

	private const string keyFor_hasSeenHomeIntro = "hasSeenHomeIntro";

	private const string keyFor_hasSyncedGuest = "hasSyncedGuest";

	private const string keyFor_useCellularData = "useCellularData";

	private const string keyFor_hasSeenNavMenu = "hasSeenNavMenu";

	private const string keyFor_enemyUnlocks = "enemyUnlocks";

	private const string keyFor_powerupUnlocks = "powerupUnlocks";

	private const string keyFor_challenges = "challenges";

	private const string keyFor_badges = "badges";

	private const string keyFor_brainSlots = "brainSlots";

	private const string keyFor_brainIds = "brainIds";

	private const string keyFor_mapRoomCount = "mapRoomCount";

	private const string keyFor_animationFramesUnlocked = "animationFramesUnlocked";

	private const string keyFor_gemsUnlocked = "gemsUnlocked";

	private const string keyFor_featuredBool = "featuredBool";

	private const string keyFor_spendBool = "spendBool";

	private const string keyFor_spendBoolv2 = "spendBoolv2";

	private const string keyFor_captureVersion = "captureVersion";

	private const string keyFor_hasRunInitialS3Sync = "hasRunInitialS3Sync";

	private const string keyFor_shouldFlipCamera = "shouldFlipCamera";

	public string enemyUnlockBooleanString;

	public string powerupUnlockBooleanString;

	public string challengeBooleanString;

	public string badgeBooleanString;

	public string brainBooleanString;

	public string brainIdString;

	public string gemBooleanString;

	public string featuredBooleanString;

	public string spendBooleanString;

	public string spendBooleanStringv2;

	public bool cameraAccessGranted
	{
		get
		{
			bool result = false;
			if (!PlayerPrefs.HasKey("CameraCheckDone") || !PlayerPrefs.HasKey("CameraAccessGranted"))
			{
				return false;
			}
			int @int = PlayerPrefs.GetInt("CameraAccessGranted");
			if (@int == 1)
			{
				result = true;
			}
			return result;
		}
		set
		{
			if (value)
			{
				PlayerPrefs.SetInt("CameraCheckDone", 1);
				PlayerPrefs.SetInt("CameraAccessGranted", 1);
			}
			else
			{
				PlayerPrefs.SetInt("CameraCheckDone", 0);
				PlayerPrefs.SetInt("CameraAccessGranted", 0);
			}
		}
	}

	public bool hasSyncedGuest
	{
		get
		{
			return GetBooleanStoredAsInt("hasSyncedGuest");
		}
		set
		{
			SetBooleanStoredAsInt("hasSyncedGuest", value);
		}
	}

	public bool useCellualarData
	{
		get
		{
			return GetBooleanStoredAsInt("useCellularData", true);
		}
		set
		{
			SetBooleanStoredAsInt("useCellularData", value);
		}
	}

	public bool hasSeenHomeIntro
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenHomeIntro");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenHomeIntro", value);
		}
	}

	public bool hasSeenNavMenu
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenNavMenu");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenNavMenu", value);
		}
	}

	public bool hasRunInitialS3Sync
	{
		get
		{
			return GetBooleanStoredAsInt("hasRunInitialS3Sync");
		}
		set
		{
			SetBooleanStoredAsInt("hasRunInitialS3Sync", value);
		}
	}

	public bool hasSentDeviceInfo
	{
		get
		{
			return GetBooleanStoredAsInt("hasSentDeviceInfo");
		}
		set
		{
			SetBooleanStoredAsInt("hasSentDeviceInfo", value);
		}
	}

	public bool hasSeenGemInfo
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenGemInfo");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenGemInfo", value);
		}
	}

	public bool shouldDisableGuide
	{
		get
		{
			return GetBooleanStoredAsInt("shouldDisableGuide");
		}
		set
		{
			SetBooleanStoredAsInt("shouldDisableGuide", value);
		}
	}

	public bool hasSeenEditor
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenEditor");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenEditor", value);
		}
	}

	public bool hasSeenCharacterTour
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenCharacterTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenCharacterTour", value);
		}
	}

	public bool hasSeenAnimatorTour
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenAnimatorTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenAnimatorTour", value);
		}
	}

	public bool hasSeenBoardBuilderTour
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenBoardBuilderTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenBoardBuilderTour", value);
		}
	}

	public bool hasSeenQuickStartTour
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenQuickStartTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenQuickStartTour", value);
		}
	}

	public bool hasSeeniWallTour
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeeniWallTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeeniWallTour", value);
		}
	}

	public bool hasSeenCapture
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenCaptureTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenCaptureTour", value);
		}
	}

	public bool hasSeenCaptureSuccess
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenCaptureSuccess");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenCaptureSuccess", value);
		}
	}

	public bool hasSeenGameBuilderTour
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenGameBuilderTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenGameBuilderTour", value);
		}
	}

	public bool hasSeenTags
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenTags");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenTags", value);
		}
	}

	public bool hasSeenMegaBoardTour
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenMegaBoardTour");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenMegaBoardTour", value);
		}
	}

	public bool hasSeenPinchZoomTutorial
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenPinchZoomTutorial");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenPinchZoomTutorial", value);
		}
	}

	public bool hasSeenPinchInfinityViewTutorial
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenPinchInfinityViewTutorial");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenPinchInfinityViewTutorial", value);
		}
	}

	public bool hasInstalledPoultryPanic
	{
		get
		{
			return GetBooleanStoredAsInt("hasInstalledPoultryPanic");
		}
		set
		{
			SetBooleanStoredAsInt("hasInstalledPoultryPanic", value);
		}
	}

	public bool hasInstalledDefaultAssetsV1
	{
		get
		{
			return GetBooleanStoredAsInt("hasInstalledDefaultAssetsV1");
		}
		set
		{
			SetBooleanStoredAsInt("hasInstalledDefaultAssetsV1", value);
		}
	}

	public bool hasSetupDefaultEnemyTypes
	{
		get
		{
			return GetBooleanStoredAsInt("hasSetupDefaultEnemyTypes");
		}
		set
		{
			SetBooleanStoredAsInt("hasSetupDefaultEnemyTypes", value);
		}
	}

	public bool hasSetupDefaultPowerups
	{
		get
		{
			return GetBooleanStoredAsInt("hasSetupDefaultPowerups");
		}
		set
		{
			SetBooleanStoredAsInt("hasSetupDefaultPowerups", value);
		}
	}

	public bool hasSetupDefaultBrainSlots
	{
		get
		{
			return GetBooleanStoredAsInt("hasSetupDefaultBrainSlots");
		}
		set
		{
			SetBooleanStoredAsInt("hasSetupDefaultBrainSlots", value);
		}
	}

	public bool hasSetupDefaultMapRooms
	{
		get
		{
			return GetBooleanStoredAsInt("hasSetupDefaultMapRooms");
		}
		set
		{
			SetBooleanStoredAsInt("hasSetupDefaultMapRooms", value);
		}
	}

	public bool hasSetupDefaultAnimationFrames
	{
		get
		{
			return GetBooleanStoredAsInt("hasSetupDefaultAnimationFrames");
		}
		set
		{
			SetBooleanStoredAsInt("hasSetupDefaultAnimationFrames", value);
		}
	}

	public bool hasSeenStartPositionHelp
	{
		get
		{
			return GetBooleanStoredAsInt("hasSeenStartPositionHelp");
		}
		set
		{
			SetBooleanStoredAsInt("hasSeenStartPositionHelp", value);
		}
	}

	public bool shouldActiveAfterWarp
	{
		get
		{
			return GetBooleanStoredAsInt("shouldActivateAfterWarp");
		}
		set
		{
			SetBooleanStoredAsInt("shouldActivateAfterWarp", value);
		}
	}

	public bool hasUserAccount
	{
		get
		{
			return PlayerPrefs.HasKey("userAccount");
		}
	}

	public UserAccount userAccount
	{
		get
		{
			if (!hasUserAccount)
			{
				return null;
			}
			return new UserAccount(PlayerPrefs.GetString("userAccount"));
		}
		set
		{
			PlayerPrefs.SetString("userAccount", value.ToJSONString());
		}
	}

	public string iWallWarpLocation
	{
		get
		{
			return PlayerPrefs.GetString("iWallWarpLocation");
		}
		set
		{
			PlayerPrefs.SetString("iWallWarpLocation", value);
		}
	}

	public string captureVersion
	{
		get
		{
			return PlayerPrefs.GetString("captureVersion");
		}
		set
		{
			PlayerPrefs.SetString("captureVersion", value);
		}
	}

	public string preferredLanguage
	{
		get
		{
			return PlayerPrefs.GetString("preferredLanguage");
		}
		set
		{
			PlayerPrefs.SetString("preferredLanguage", value);
		}
	}

	public bool shouldFlipCamera
	{
		get
		{
			return GetBooleanStoredAsInt("shouldFlipCamera");
		}
		set
		{
			SetBooleanStoredAsInt("shouldFlipCamera", value);
		}
	}

	public string userToken
	{
		get
		{
			return SecurePlayerPrefs.GetString("requestToken", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		}
		set
		{
			SecurePlayerPrefs.SetString("requestToken", value, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		}
	}

	public bool hasOpenedApp
	{
		get
		{
			return GetSecureBool("hasOpenedApp");
		}
		set
		{
			SetSecureBool("hasOpenedApp", value);
		}
	}

	public bool hasSeeniWall
	{
		get
		{
			return GetSecureBool("hasSeenIWall");
		}
		set
		{
			SetSecureBool("hasSeenIWall", value);
		}
	}

	public bool appInitCoinBonusAwarded
	{
		get
		{
			return GetSecureBool("appInitCoinBonusAwarded");
		}
		set
		{
			SetSecureBool("appInitCoinBonusAwarded", value);
		}
	}

	public bool hasAnimationFrames
	{
		get
		{
			return GetSecureBool("animationFramesUnlocked");
		}
		set
		{
			SetSecureBool("animationFramesUnlocked", value);
		}
	}

	public bool hasReceivedEarlyAdopterRooms
	{
		get
		{
			return GetSecureBool("hasReceivedEarlyAdopterRooms");
		}
		set
		{
			SetSecureBool("hasReceivedEarlyAdopterRooms", value);
		}
	}

	public int playerCoins
	{
		get
		{
			int result = 0;
			if (SecurePlayerPrefs.HasKey("playerCoins"))
			{
				string @string = SecurePlayerPrefs.GetString("playerCoins", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
				int.TryParse(@string, out result);
			}
			return result;
		}
		set
		{
			SecurePlayerPrefs.SetString("playerCoins", value.ToString(), "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		}
	}

	public bool userOwnsBoard
	{
		get
		{
			return GetSecureBool("userOwnsBoard");
		}
		set
		{
			AppStateManager.instance.userOwnsBoard = value;
			SetSecureBool("userOwnsBoard", value);
		}
	}

	public event HandleUpdateNews OnUpdateNews;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null && instance != this)
		{
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		enemyUnlockBooleanString = PlayerPrefs.GetString("enemyUnlocks");
		powerupUnlockBooleanString = PlayerPrefs.GetString("powerupUnlocks");
		challengeBooleanString = PlayerPrefs.GetString("challenges");
		badgeBooleanString = PlayerPrefs.GetString("badges");
		brainIdString = PlayerPrefs.GetString("brainIds");
		brainBooleanString = PlayerPrefs.GetString("brainSlots");
		gemBooleanString = SecurePlayerPrefs.GetString("gemsUnlocked", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		featuredBooleanString = SecurePlayerPrefs.GetString("featuredBool", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		spendBooleanStringv2 = SecurePlayerPrefs.GetString("spendBoolv2", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g", false);
		int length = Enum.GetValues(typeof(EnemyType)).Length;
		if (enemyUnlockBooleanString.Length < length)
		{
			enemyUnlockBooleanString = enemyUnlockBooleanString.PadRight(length, '0');
		}
		int length2 = Enum.GetValues(typeof(PowerUpType)).Length;
		if (powerupUnlockBooleanString.Length < length2)
		{
			powerupUnlockBooleanString = powerupUnlockBooleanString.PadRight(length2, '0');
		}
		int length3 = Enum.GetValues(typeof(Challenge)).Length;
		if (challengeBooleanString.Length < length3)
		{
			challengeBooleanString = challengeBooleanString.PadRight(length3, '0');
		}
		int length4 = Enum.GetValues(typeof(Badge)).Length;
		if (badgeBooleanString.Length < length4)
		{
			badgeBooleanString = badgeBooleanString.PadRight(length4, '0');
		}
		if (brainBooleanString.Length < BloxelBrain.MaxGlobalBrains)
		{
			brainBooleanString = brainBooleanString.PadRight(BloxelBrain.MaxGlobalBrains, '0');
		}
		int length5 = Enum.GetValues(typeof(GemEarnable.Type)).Length;
		if (gemBooleanString.Length < length5)
		{
			gemBooleanString = gemBooleanString.PadRight(length5, '0');
		}
		if (string.IsNullOrEmpty(brainIdString))
		{
			brainIdString = Guid.Empty.ToString() + "|" + Guid.Empty.ToString() + "|" + Guid.Empty.ToString() + "|" + Guid.Empty.ToString() + "|" + Guid.Empty.ToString() + "|" + Guid.Empty.ToString();
			PlayerPrefs.SetString("brainIds", brainIdString);
		}
		else if (GetGlobalBrainIds().Length < BloxelBrain.MaxGlobalBrains)
		{
			brainIdString = brainIdString + "|" + Guid.Empty.ToString();
			PlayerPrefs.SetString("brainIds", brainIdString);
		}
		if (!hasSetupDefaultBrainSlots)
		{
			SetupDefaultBrainSlots();
		}
		if (!hasSetupDefaultMapRooms)
		{
			SetupDefaultRoomCount();
		}
		if (!hasSetupDefaultEnemyTypes)
		{
			SetupDefaultEnemyUnlocks();
		}
		if (!hasSetupDefaultPowerups)
		{
			SetupDefaultPowerups();
		}
		if (!hasSetupDefaultAnimationFrames)
		{
			SetupDefaultAnimationFrames();
		}
	}

	private void Start()
	{
		BloxelServerInterface.instance.OnNewsItems += CheckNews;
		GenerateSpendString();
	}

	private void OnDestroy()
	{
		BloxelServerInterface.instance.OnNewsItems -= CheckNews;
	}

	public void SyncUnlocks()
	{
		if (!CurrentUser.instance.active)
		{
			return;
		}
		char[] array = spendBooleanStringv2.ToCharArray();
		int num = GemManager.MaxRoomsWithoutUnlock;
		for (int i = GemManager.startIndices[GemUnlockable.Type.MapRoom]; i < GemManager.maxUnlock[GemUnlockable.Type.MapRoom]; i++)
		{
			if (array[i] == '1')
			{
				num++;
			}
		}
		if (HasGemsForType(GemEarnable.Type.BoardVerify))
		{
			num = 169;
		}
		string text = PPUtilities.ORStrings(powerupUnlockBooleanString, spendBooleanStringv2.Substring(GemManager.startIndices[GemUnlockable.Type.PowerupType], GemManager.maxUnlock[GemUnlockable.Type.PowerupType]));
		string text2 = PPUtilities.ORStrings(enemyUnlockBooleanString, spendBooleanStringv2.Substring(GemManager.startIndices[GemUnlockable.Type.EnemyType], GemManager.maxUnlock[GemUnlockable.Type.EnemyType]));
		string text3 = PPUtilities.ORStrings(brainBooleanString, spendBooleanStringv2.Substring(GemManager.startIndices[GemUnlockable.Type.BrainBoardSlot], GemManager.maxUnlock[GemUnlockable.Type.BrainBoardSlot]));
		bool flag = false;
		if (array[GemManager.startIndices[GemUnlockable.Type.AnimationFrames]] == '1')
		{
			flag = true;
		}
		powerupUnlockBooleanString = text;
		enemyUnlockBooleanString = text2;
		brainBooleanString = text3;
		if (flag)
		{
			GemManager.animationFramesUnlocked = flag;
			hasAnimationFrames = true;
		}
		if (num > 169)
		{
			num = 169;
		}
		PlayerPrefs.SetInt("mapRoomCount", num);
		GemManager.instance.SetRoomsUnlockedFromDisk();
		userOwnsBoard = GemManager.instance.HasGemsForType(GemEarnable.Type.BoardVerify);
		StartCoroutine(StaggerUnlocks());
	}

	private IEnumerator StaggerUnlocks()
	{
		yield return new WaitForEndOfFrame();
		PlayerPrefs.SetString("powerupUnlocks", powerupUnlockBooleanString);
		GemManager.instance.SetPowerupUnlocksFromDisk();
		yield return new WaitForEndOfFrame();
		PlayerPrefs.SetString("enemyUnlocks", enemyUnlockBooleanString);
		GemManager.instance.SetEnemyTypeUnlocksFromDisk();
		yield return new WaitForEndOfFrame();
		PlayerPrefs.SetString("brainSlots", brainBooleanString);
		GemManager.instance.SetBrainSlotsFromDisk();
	}

	private string GenerateSpendString()
	{
		int num = 0;
		GemUnlockable.Type[] array = Enum.GetValues(typeof(GemUnlockable.Type)) as GemUnlockable.Type[];
		for (int i = 0; i < array.Length; i++)
		{
			num += GemManager.maxUnlock[array[i]];
		}
		if (spendBooleanStringv2.Length < num)
		{
			spendBooleanStringv2 = spendBooleanStringv2.PadRight(num, '0');
		}
		SecurePlayerPrefs.SetString("spendBoolv2", spendBooleanStringv2, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g", false);
		return spendBooleanStringv2;
	}

	public void UpdateSpend(GemUnlockable.Type type, int indexForType = -1)
	{
		char[] array = spendBooleanStringv2.ToCharArray();
		int num = GemManager.startIndices[type];
		int num2 = GemManager.maxUnlock[type];
		int num3 = num + num2;
		if (indexForType == -1)
		{
			for (int i = num; i < num3; i++)
			{
				if (array[i] != '1')
				{
					array[i] = '1';
					break;
				}
			}
		}
		else if (array[indexForType + num] != '1')
		{
			array[indexForType + num] = '1';
		}
		spendBooleanStringv2 = new string(array);
		SecurePlayerPrefs.SetString("spendBoolv2", spendBooleanStringv2, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g", false);
		PlayerBankManager.instance.UpdateGemsOnServer();
	}

	private bool GetSecureBool(string key)
	{
		return "true" == SecurePlayerPrefs.GetString(key, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	private void SetSecureBool(string key, bool value)
	{
		string value2 = ((!value) ? "false" : "true");
		SecurePlayerPrefs.SetString(key, value2, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	private bool GetBooleanStoredAsInt(string key, bool defaultValue = false)
	{
		if (!PlayerPrefs.HasKey(key))
		{
			return defaultValue;
		}
		return PlayerPrefs.GetInt(key) == 1;
	}

	private void SetBooleanStoredAsInt(string key, bool value)
	{
		PlayerPrefs.SetInt(key, value ? 1 : 0);
	}

	public void SetBrainIdAtPosition(int index, string id)
	{
		string[] array = PlayerPrefs.GetString("brainIds").Split('|');
		array[index] = id;
		brainIdString = string.Empty;
		for (int i = 0; i < array.Length; i++)
		{
			if (i < array.Length - 1)
			{
				brainIdString = brainIdString + array[i] + "|";
			}
			else
			{
				brainIdString += array[i];
			}
		}
		PlayerPrefs.SetString("brainIds", brainIdString);
	}

	public void BulkUnlockRooms()
	{
		PlayerPrefs.SetInt("mapRoomCount", 169);
	}

	public int UnlockMapRoom()
	{
		int @int = PlayerPrefs.GetInt("mapRoomCount");
		@int++;
		PlayerPrefs.SetInt("mapRoomCount", @int);
		UpdateSpend(GemUnlockable.Type.MapRoom);
		return @int;
	}

	public int GetMapRoomCount()
	{
		return PlayerPrefs.GetInt("mapRoomCount");
	}

	public string GetBrainIdAtPosition(int index)
	{
		string[] array = PlayerPrefs.GetString("brainIds").Split('|');
		if (index > array.Length)
		{
			return string.Empty;
		}
		return array[index];
	}

	public string[] GetGlobalBrainIds()
	{
		return PlayerPrefs.GetString("brainIds").Split('|');
	}

	public void BrainSlotUnlock(int idx)
	{
		char[] array = brainBooleanString.ToCharArray();
		array[idx] = '1';
		brainBooleanString = new string(array);
		PlayerPrefs.SetString("brainSlots", brainBooleanString);
		UpdateSpend(GemUnlockable.Type.BrainBoardSlot, idx);
	}

	public bool CheckBrainSlot(int idx)
	{
		char c = brainBooleanString[idx];
		return c == '1';
	}

	public void EnemyTypeUnlock(EnemyType type)
	{
		char[] array = enemyUnlockBooleanString.ToCharArray();
		array[(int)type] = '1';
		enemyUnlockBooleanString = new string(array);
		PlayerPrefs.SetString("enemyUnlocks", enemyUnlockBooleanString);
		UpdateSpend(GemUnlockable.Type.EnemyType, (int)type);
	}

	public bool CheckEnemyType(EnemyType type)
	{
		char c = enemyUnlockBooleanString[(int)type];
		return c == '1';
	}

	public void PowerupUnlock(PowerUpType type)
	{
		char[] array = powerupUnlockBooleanString.ToCharArray();
		array[(uint)type] = '1';
		powerupUnlockBooleanString = new string(array);
		PlayerPrefs.SetString("powerupUnlocks", powerupUnlockBooleanString);
		UpdateSpend(GemUnlockable.Type.PowerupType, (int)type);
	}

	public bool CheckPowerup(PowerUpType type)
	{
		char c = powerupUnlockBooleanString[(int)type];
		return c == '1';
	}

	public void ChallengeCompleted(Challenge challenge)
	{
		char[] array = challengeBooleanString.ToCharArray();
		array[(int)challenge] = '1';
		challengeBooleanString = new string(array);
		PlayerPrefs.SetString("challenges", challengeBooleanString);
	}

	public bool CheckChallenge(Challenge challenge)
	{
		char c = challengeBooleanString[(int)challenge];
		return c == '1';
	}

	public void SetFeaturedBool(string featuredBool)
	{
		featuredBooleanString = featuredBool;
		SecurePlayerPrefs.SetString("featuredBool", featuredBooleanString, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	public void SetGemBool(string gemBool)
	{
		gemBooleanString = gemBool;
		SecurePlayerPrefs.SetString("gemsUnlocked", gemBooleanString, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	public void SetSpendBool(string spendBool)
	{
		spendBooleanStringv2 = spendBool;
		SecurePlayerPrefs.SetString("spendBoolv2", spendBooleanStringv2, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	public void AddGemsForType(GemEarnable.Type type)
	{
		char[] array = gemBooleanString.ToCharArray();
		array[(int)type] = '1';
		gemBooleanString = new string(array);
		SecurePlayerPrefs.SetString("gemsUnlocked", gemBooleanString, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	public bool HasGemsForType(GemEarnable.Type type)
	{
		char c = gemBooleanString[(int)type];
		return c == '1';
	}

	public void BadgeEarned(Badge badge)
	{
		char[] array = badgeBooleanString.ToCharArray();
		array[(int)badge] = '1';
		badgeBooleanString = new string(array);
		PlayerPrefs.SetString("badges", badgeBooleanString);
	}

	public bool CheckBadge(Badge badge)
	{
		char c = badgeBooleanString[(int)badge];
		return c == '1';
	}

	private void SetupDefaultRoomCount()
	{
		PlayerPrefs.SetInt("mapRoomCount", GemManager.MaxRoomsWithoutUnlock);
		hasSetupDefaultMapRooms = true;
	}

	private void SetupDefaultAnimationFrames()
	{
		if (hasSeenEditor)
		{
			hasAnimationFrames = true;
		}
		else
		{
			hasAnimationFrames = false;
		}
		hasSetupDefaultAnimationFrames = true;
	}

	private void SetupDefaultBrainSlots()
	{
		char[] array = brainBooleanString.ToCharArray();
		for (int i = 0; i < GemManager.InitialBrainSlots; i++)
		{
			array[i] = '1';
		}
		brainBooleanString = new string(array);
		PlayerPrefs.SetString("brainSlots", brainBooleanString);
		hasSetupDefaultBrainSlots = true;
	}

	private void SetupDefaultEnemyUnlocks()
	{
		if (hasSeenEditor)
		{
			BulkUnlockEnemyTypes();
		}
		else
		{
			char[] array = enemyUnlockBooleanString.ToCharArray();
			array[0] = '1';
			enemyUnlockBooleanString = new string(array);
			PlayerPrefs.SetString("enemyUnlocks", enemyUnlockBooleanString);
		}
		hasSetupDefaultEnemyTypes = true;
	}

	public void BulkUnlockEnemyTypes()
	{
		char[] array = new char[enemyUnlockBooleanString.Length];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = '1';
		}
		enemyUnlockBooleanString = new string(array);
		PlayerPrefs.SetString("enemyUnlocks", enemyUnlockBooleanString);
	}

	private void SetupDefaultPowerups()
	{
		if (hasSeenEditor)
		{
			BulkUnlockPowerups();
		}
		else
		{
			char[] array = new char[powerupUnlockBooleanString.Length];
			for (int i = 0; i < array.Length; i++)
			{
				if (i == 0 || i == 1 || i == 4)
				{
					array[i] = '1';
				}
				else
				{
					array[i] = '0';
				}
			}
			powerupUnlockBooleanString = new string(array);
			PlayerPrefs.SetString("powerupUnlocks", powerupUnlockBooleanString);
		}
		hasSetupDefaultPowerups = true;
	}

	public void BulkUnlockPowerups()
	{
		char[] array = new char[powerupUnlockBooleanString.Length];
		for (int i = 0; i < array.Length; i++)
		{
			array[i] = '1';
		}
		powerupUnlockBooleanString = new string(array);
		PlayerPrefs.SetString("powerupUnlocks", powerupUnlockBooleanString);
	}

	public void RemoveLoggedInUser()
	{
		PlayerPrefs.DeleteKey("userAccount");
		SecurePlayerPrefs.DeleteKey("playerCoins", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		SecurePlayerPrefs.DeleteKey("dietCoke", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g", false);
		gemBooleanString = new string('0', gemBooleanString.Length);
		featuredBooleanString = new string('0', featuredBooleanString.Length);
		spendBooleanString = new string('0', spendBooleanString.Length);
		spendBooleanStringv2 = new string('0', spendBooleanStringv2.Length);
		SecurePlayerPrefs.SetString("gemsUnlocked", gemBooleanString, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		SecurePlayerPrefs.SetString("featuredBool", featuredBooleanString, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		SecurePlayerPrefs.SetString("spendBoolv2", spendBooleanStringv2, "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	public void FullReset()
	{
		SecurePlayerPrefs.DeleteKey("userOwnsBoard", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		PlayerPrefs.DeleteKey("iWallWarpLocation");
		SecurePlayerPrefs.DeleteKey("shouldActivateAfterWarp", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		SecurePlayerPrefs.DeleteKey("hasOpenedApp", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		SecurePlayerPrefs.DeleteKey("appInitCoinBonusAwarded", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		SecurePlayerPrefs.DeleteKey("playerCoins", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
		SecurePlayerPrefs.DeleteKey("dietCoke", "cRE4)&RuB.2LxL`FwDf9z58rV_*4'g");
	}

	public void ResetTuts()
	{
		shouldDisableGuide = false;
		PlayerPrefs.DeleteKey("hasSeenCharacterTour");
		PlayerPrefs.DeleteKey("hasSeenAnimatorTour");
		PlayerPrefs.DeleteKey("hasSeenBoardBuilderTour");
		PlayerPrefs.DeleteKey("hasSeenEditor");
		PlayerPrefs.DeleteKey("hasSeenQuickStartTour");
		PlayerPrefs.DeleteKey("hasSeenCaptureTour");
		PlayerPrefs.DeleteKey("hasSeenCaptureSuccess");
		PlayerPrefs.DeleteKey("hasSeenGameBuilderTour");
		PlayerPrefs.DeleteKey("hasSeenGameMap");
		PlayerPrefs.DeleteKey("hasSeenMegaBoardTour");
		PlayerPrefs.DeleteKey("hasSeenPinchZoomTutorial");
		PlayerPrefs.DeleteKey("hasSeenPinchInfinityViewTutorial");
		PlayerPrefs.DeleteKey("hasSeenTags");
		PlayerPrefs.DeleteKey("hasSeenStartPositionHelp");
		PlayerPrefs.DeleteKey("hasSeeniWallTour");
		PlayerPrefs.DeleteKey("shouldDisableGuide");
	}

	public void SetNewsItem(string id)
	{
		string value = PlayerPrefs.GetString("news") + id;
		PlayerPrefs.SetString("news", value);
		if (this.OnUpdateNews != null)
		{
			this.OnUpdateNews();
		}
	}

	public bool NewsItemExists(string id)
	{
		if (!PlayerPrefs.HasKey("news"))
		{
			return false;
		}
		string[] array = PlayerPrefs.GetString("news").Split('|');
		for (int i = 0; i < array.Length; i++)
		{
			if (array[i] == id)
			{
				return true;
			}
		}
		return false;
	}

	public void CheckNews(List<NewsItem> newsItems)
	{
		CleanNews(newsItems);
		for (int i = 0; i < newsItems.Count; i++)
		{
			string serverID = newsItems[i].serverID;
			if (!NewsItemExists(serverID))
			{
				if (i == 0)
				{
					SetNewsItem(serverID);
				}
				else
				{
					SetNewsItem("|" + serverID);
				}
			}
		}
	}

	private void CleanNews(List<NewsItem> newsItems)
	{
		if (!PlayerPrefs.HasKey("news"))
		{
			return;
		}
		List<string> list = new List<string>(newsItems.Count);
		for (int i = 0; i < newsItems.Count; i++)
		{
			list.Add(newsItems[i].serverID);
		}
		string[] array = PlayerPrefs.GetString("news").Split('|');
		string text = string.Empty;
		for (int j = 0; j < array.Length; j++)
		{
			if (list.Contains(array[j]))
			{
				string empty = string.Empty;
				empty = ((j != array.Length - 1) ? (array[j] + "|") : array[j]);
				text += empty;
			}
		}
		PlayerPrefs.SetString("news", text);
	}
}
