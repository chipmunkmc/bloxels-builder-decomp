#define DEBUG_CC2D_RAYS
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Threading;
using UnityEngine;

namespace Prime31
{
	[RequireComponent(typeof(Rigidbody2D))]
	public class CharacterController2D : MonoBehaviour
	{
		private struct CharacterRaycastOrigins
		{
			public Vector3 topLeft;

			public Vector3 bottomRight;

			public Vector3 bottomLeft;
		}

		public class CharacterCollisionState2D
		{
			public bool right;

			public bool left;

			public bool above;

			public bool below;

			public bool becameGroundedThisFrame;

			public bool wasGroundedLastFrame;

			public bool movingDownSlope;

			public float slopeAngle;

			public bool hasCollision()
			{
				return below || right || left || above;
			}

			public void reset()
			{
				right = (left = (above = (below = (becameGroundedThisFrame = (movingDownSlope = false)))));
				slopeAngle = 0f;
			}

			public override string ToString()
			{
				return string.Format("[CharacterCollisionState2D] r: {0}, l: {1}, a: {2}, b: {3}, movingDownSlope: {4}, angle: {5}, wasGroundedLastFrame: {6}, becameGroundedThisFrame: {7}", right, left, above, below, movingDownSlope, slopeAngle, wasGroundedLastFrame, becameGroundedThisFrame);
			}
		}

		public bool ignoreOneWayPlatformsThisFrame;

		[SerializeField]
		[Range(0.001f, 0.3f)]
		private float _skinWidth = 0.02f;

		public LayerMask platformMask = 0;

		public LayerMask heroPlacementMask = 0;

		public LayerMask damagedPlatformMask = 0;

		public LayerMask currentPlatformMask;

		public LayerMask triggerMask = 0;

		[SerializeField]
		private LayerMask oneWayPlatformMask = 0;

		[Range(0f, 90f)]
		public float slopeLimit = 30f;

		public float jumpingThreshold = 0.07f;

		public AnimationCurve slopeSpeedMultiplier = new AnimationCurve(new Keyframe(-90f, 1.5f), new Keyframe(0f, 1f), new Keyframe(90f, 0f));

		[Range(2f, 20f)]
		public int totalHorizontalRays = 8;

		[Range(2f, 20f)]
		public int totalVerticalRays = 4;

		private float _slopeLimitTangent = Mathf.Tan(1.3089969f);

		[NonSerialized]
		[HideInInspector]
		public new Transform transform;

		[NonSerialized]
		[HideInInspector]
		public BoxCollider2D boxCollider;

		[NonSerialized]
		[HideInInspector]
		public Rigidbody2D rigidBody2D;

		[NonSerialized]
		[HideInInspector]
		public CharacterCollisionState2D collisionState = new CharacterCollisionState2D();

		[NonSerialized]
		[HideInInspector]
		public Vector3 velocity;

		private const float kSkinWidthFloatFudgeFactor = 0.001f;

		private CharacterRaycastOrigins _raycastOrigins;

		private RaycastHit2D _raycastHit;

		private List<RaycastHit2D> _raycastHitsThisFrame = new List<RaycastHit2D>(2);

		private float _verticalDistanceBetweenRays;

		private float _horizontalDistanceBetweenRays;

		private bool _isGoingUpSlope;

		public bool isTakingDamage;

		public float widthCorrectionFactor;

		public float skinWidth
		{
			get
			{
				return _skinWidth;
			}
			set
			{
				_skinWidth = value;
				recalculateDistanceBetweenRays();
			}
		}

		public bool isGrounded
		{
			get
			{
				return collisionState.below;
			}
		}

		public event Action<RaycastHit2D> onControllerCollidedEvent;

		public event Action<Collider2D> onTriggerEnterEvent;

		public event Action<Collider2D> onTriggerExitEvent;

		public void Awake()
		{
			platformMask = (int)platformMask | (int)oneWayPlatformMask;
			currentPlatformMask = platformMask;
			transform = GetComponent<Transform>();
			boxCollider = transform.GetChild(0).GetComponent<BoxCollider2D>();
			rigidBody2D = GetComponent<Rigidbody2D>();
			skinWidth = _skinWidth;
			for (int i = 0; i < 32; i++)
			{
				if ((triggerMask.value & (1 << i)) == 0)
				{
					Physics2D.IgnoreLayerCollision(base.gameObject.layer, i);
				}
			}
		}

		protected void SetLayerMaskToDamaged()
		{
			isTakingDamage = true;
			for (int i = 0; i < 32; i++)
			{
				if ((currentPlatformMask.value & (1 << i)) == 1)
				{
					Physics2D.IgnoreLayerCollision(base.gameObject.layer, i, true);
				}
				if ((damagedPlatformMask.value & (1 << i)) == 0)
				{
					Physics2D.IgnoreLayerCollision(base.gameObject.layer, i, false);
				}
			}
			currentPlatformMask = damagedPlatformMask;
		}

		protected void SetLayerMaskToNormal()
		{
			isTakingDamage = false;
			for (int i = 0; i < 32; i++)
			{
				if ((currentPlatformMask.value & (1 << i)) == 1)
				{
					Physics2D.IgnoreLayerCollision(base.gameObject.layer, i, true);
				}
				if ((platformMask.value & (1 << i)) == 0)
				{
					Physics2D.IgnoreLayerCollision(base.gameObject.layer, i, false);
				}
			}
			currentPlatformMask = platformMask;
		}

		public void OnTriggerEnter2D(Collider2D col)
		{
			if (this.onTriggerEnterEvent != null)
			{
				this.onTriggerEnterEvent(col);
			}
		}

		public void OnTriggerExit2D(Collider2D col)
		{
			if (this.onTriggerExitEvent != null)
			{
				this.onTriggerExitEvent(col);
			}
		}

		[Conditional("DEBUG_CC2D_RAYS")]
		private void DrawRay(Vector3 start, Vector3 dir, Color color)
		{
		}

		public void move(Vector3 deltaMovement)
		{
			collisionState.wasGroundedLastFrame = collisionState.below;
			collisionState.reset();
			_raycastHitsThisFrame.Clear();
			_isGoingUpSlope = false;
			primeRaycastOrigins();
			if (deltaMovement.y < 0f && collisionState.wasGroundedLastFrame)
			{
				handleVerticalSlope(ref deltaMovement);
			}
			if (deltaMovement.x != 0f)
			{
				moveHorizontally(ref deltaMovement);
			}
			if (deltaMovement.y != 0f)
			{
				moveVertically(ref deltaMovement);
			}
			transform.Translate(deltaMovement, Space.World);
			if (Time.deltaTime > 0f)
			{
				velocity = deltaMovement / Time.deltaTime;
			}
			if (!collisionState.wasGroundedLastFrame && collisionState.below)
			{
				collisionState.becameGroundedThisFrame = true;
			}
			if (_isGoingUpSlope)
			{
				velocity.y = 0f;
			}
			if (this.onControllerCollidedEvent != null)
			{
				for (int i = 0; i < _raycastHitsThisFrame.Count; i++)
				{
					this.onControllerCollidedEvent(_raycastHitsThisFrame[i]);
				}
			}
			ignoreOneWayPlatformsThisFrame = false;
		}

		public void warpToGrounded()
		{
			do
			{
				move(new Vector3(0f, -1f, 0f));
			}
			while (!isGrounded);
		}

		public void recalculateDistanceBetweenRays()
		{
			float num = boxCollider.size.y * Mathf.Abs(transform.localScale.y) - 2f * _skinWidth;
			_verticalDistanceBetweenRays = num / (float)(totalHorizontalRays - 1);
			float num2 = boxCollider.size.x * Mathf.Abs(transform.localScale.x) - 2f * _skinWidth;
			_horizontalDistanceBetweenRays = num2 / (float)(totalVerticalRays - 1);
		}

		private void primeRaycastOrigins()
		{
			Bounds bounds = boxCollider.bounds;
			bounds.Expand(-2f * _skinWidth);
			_raycastOrigins.topLeft = new Vector2(bounds.min.x, bounds.max.y);
			_raycastOrigins.bottomRight = new Vector2(bounds.max.x, bounds.min.y);
			_raycastOrigins.bottomLeft = bounds.min;
		}

		private void moveHorizontally(ref Vector3 deltaMovement)
		{
			bool flag = deltaMovement.x > 0f;
			float num = Mathf.Abs(deltaMovement.x) + _skinWidth;
			Vector2 vector = ((!flag) ? (-Vector2.right) : Vector2.right);
			Vector3 vector2 = ((!flag) ? _raycastOrigins.bottomLeft : _raycastOrigins.bottomRight);
			for (int i = 0; i < totalHorizontalRays; i++)
			{
				Vector2 vector3 = new Vector2(vector2.x, vector2.y + (float)i * _verticalDistanceBetweenRays);
				DrawRay(vector3, vector * num, Color.red);
				if (i == 0 && collisionState.wasGroundedLastFrame)
				{
					_raycastHit = Physics2D.Raycast(vector3, vector, num, currentPlatformMask);
				}
				else
				{
					_raycastHit = Physics2D.Raycast(vector3, vector, num, (int)currentPlatformMask & ~(int)oneWayPlatformMask);
				}
				if ((bool)_raycastHit)
				{
					if (i == 0 && handleHorizontalSlope(ref deltaMovement, Vector2.Angle(_raycastHit.normal, Vector2.up)))
					{
						_raycastHitsThisFrame.Add(_raycastHit);
						break;
					}
					deltaMovement.x = _raycastHit.point.x - vector3.x;
					num = Mathf.Abs(deltaMovement.x);
					if (flag)
					{
						deltaMovement.x -= _skinWidth;
						collisionState.right = true;
					}
					else
					{
						deltaMovement.x += _skinWidth;
						collisionState.left = true;
					}
					_raycastHitsThisFrame.Add(_raycastHit);
					if (num < _skinWidth + 0.001f)
					{
						break;
					}
				}
			}
		}

		private bool handleHorizontalSlope(ref Vector3 deltaMovement, float angle)
		{
			if (Mathf.RoundToInt(angle) == 90)
			{
				return false;
			}
			if (angle < slopeLimit)
			{
				if (deltaMovement.y < jumpingThreshold)
				{
					float num = slopeSpeedMultiplier.Evaluate(angle);
					deltaMovement.x *= num;
					deltaMovement.y = Mathf.Abs(Mathf.Tan(angle * ((float)Math.PI / 180f)) * deltaMovement.x);
					bool flag = deltaMovement.x > 0f;
					Vector3 vector = ((!flag) ? _raycastOrigins.bottomLeft : _raycastOrigins.bottomRight);
					RaycastHit2D raycastHit2D = ((!collisionState.wasGroundedLastFrame) ? Physics2D.Raycast(vector, deltaMovement.normalized, deltaMovement.magnitude, (int)currentPlatformMask & ~(int)oneWayPlatformMask) : Physics2D.Raycast(vector, deltaMovement.normalized, deltaMovement.magnitude, currentPlatformMask));
					if ((bool)raycastHit2D)
					{
						deltaMovement = (Vector3)raycastHit2D.point - vector;
						if (flag)
						{
							deltaMovement.x -= _skinWidth;
						}
						else
						{
							deltaMovement.x += _skinWidth;
						}
					}
					_isGoingUpSlope = true;
					collisionState.below = true;
				}
			}
			else
			{
				deltaMovement.x = 0f;
			}
			return true;
		}

		private void moveVertically(ref Vector3 deltaMovement)
		{
			bool flag = deltaMovement.y > 0f;
			float num = Mathf.Abs(deltaMovement.y) + _skinWidth;
			Vector2 vector = ((!flag) ? (-Vector2.up) : Vector2.up);
			Vector3 vector2 = ((!flag) ? _raycastOrigins.bottomLeft : _raycastOrigins.topLeft);
			vector2.x += deltaMovement.x;
			LayerMask layerMask = currentPlatformMask;
			if ((flag && !collisionState.wasGroundedLastFrame) || ignoreOneWayPlatformsThisFrame)
			{
				layerMask = (int)layerMask & ~(int)oneWayPlatformMask;
			}
			for (int i = 0; i < totalVerticalRays; i++)
			{
				Vector2 vector3 = new Vector2(vector2.x + (float)i * _horizontalDistanceBetweenRays, vector2.y);
				DrawRay(vector3, vector * num, Color.red);
				_raycastHit = Physics2D.Raycast(vector3, vector, num, layerMask);
				if ((bool)_raycastHit)
				{
					deltaMovement.y = _raycastHit.point.y - vector3.y;
					num = Mathf.Abs(deltaMovement.y);
					if (flag)
					{
						deltaMovement.y -= _skinWidth * widthCorrectionFactor;
						collisionState.above = true;
					}
					else
					{
						deltaMovement.y += _skinWidth * widthCorrectionFactor;
						collisionState.below = true;
					}
					_raycastHitsThisFrame.Add(_raycastHit);
					if (!flag && deltaMovement.y > 1E-05f)
					{
						_isGoingUpSlope = true;
					}
					if (num < _skinWidth + 0.001f)
					{
						break;
					}
				}
			}
		}

		private void handleVerticalSlope(ref Vector3 deltaMovement)
		{
			float num = (_raycastOrigins.bottomLeft.x + _raycastOrigins.bottomRight.x) * 0.5f;
			Vector2 vector = -Vector2.up;
			float num2 = _slopeLimitTangent * (_raycastOrigins.bottomRight.x - num);
			Vector2 vector2 = new Vector2(num, _raycastOrigins.bottomLeft.y);
			DrawRay(vector2, vector * num2, Color.yellow);
			_raycastHit = Physics2D.Raycast(vector2, vector, num2, currentPlatformMask);
			if ((bool)_raycastHit)
			{
				float num3 = Vector2.Angle(_raycastHit.normal, Vector2.up);
				if (num3 != 0f && Mathf.Sign(_raycastHit.normal.x) == Mathf.Sign(deltaMovement.x))
				{
					float num4 = slopeSpeedMultiplier.Evaluate(0f - num3);
					deltaMovement.y += _raycastHit.point.y - vector2.y - skinWidth;
					deltaMovement.x *= num4;
					collisionState.movingDownSlope = true;
					collisionState.slopeAngle = num3;
				}
			}
		}
	}
}
