using System;
using System.Collections.Generic;
using DiscoCapture;
using OpenCVForUnity;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class ProcessorDebugWindow : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IEventSystemHandler
{
	public RectTransform selfTransform;

	public static List<string> DropdownOptions;

	public RawImage uiDisplayImage;

	public Dropdown uiDropdown;

	public Button uiButtonAdd;

	public Button uiButtonSubtract;

	private Texture2D _displayTexture;

	private Color32[] _colorBuffer;

	public ProcessOp currentOp;

	private bool _isDragging;

	private void Awake()
	{
		selfTransform = GetComponent<RectTransform>();
	}

	private void Start()
	{
		if (DropdownOptions == null)
		{
			DropdownOptions = new List<string>(Enum.GetNames(typeof(ProcessOp)));
		}
		uiDropdown.options.Clear();
		uiDropdown.AddOptions(DropdownOptions);
		uiDropdown.onValueChanged.AddListener(SetProcessor);
		uiButtonAdd.onClick.AddListener(ScaleUp);
		uiButtonSubtract.onClick.AddListener(ScaleDown);
	}

	private void ScaleUp()
	{
		float x = selfTransform.localScale.x;
		Vector3 localScale = new Vector3(x + 0.1f, x + 0.1f, x + 0.1f);
		selfTransform.localScale = localScale;
	}

	private void ScaleDown()
	{
		float x = selfTransform.localScale.x;
		Vector3 localScale = new Vector3(x - 0.1f, x - 0.1f, x - 0.1f);
		selfTransform.localScale = localScale;
	}

	public void UpdateDisplay(Mat displayMat)
	{
		Size size = displayMat.size();
		int num = (int)size.width;
		int num2 = (int)size.height;
		if (_displayTexture == null || _displayTexture.width != num || _displayTexture.height != num2)
		{
			selfTransform.sizeDelta = new Vector2(num, num2);
			_colorBuffer = new Color32[num * num2];
			_displayTexture = new Texture2D(num, num2, TextureFormat.ARGB32, false);
			_displayTexture.filterMode = FilterMode.Point;
			uiDisplayImage.texture = _displayTexture;
		}
		Utils.matToTexture2D(displayMat, _displayTexture, _colorBuffer);
	}

	public void SetProcessor(int dropdownOption)
	{
	}

	public void Close()
	{
		UnityEngine.Object.Destroy(base.gameObject);
	}

	private void OnDestroy()
	{
		if (uiDropdown != null)
		{
			uiDropdown.onValueChanged.RemoveListener(SetProcessor);
		}
	}

	public void OnBeginDrag(PointerEventData pointerData)
	{
		_isDragging = true;
		selfTransform.SetAsLastSibling();
	}

	public void OnDrag(PointerEventData pointerData)
	{
		selfTransform.anchoredPosition += pointerData.delta;
	}

	public void OnEndDrag(PointerEventData pointerData)
	{
		_isDragging = false;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!_isDragging)
		{
			Close();
		}
	}
}
