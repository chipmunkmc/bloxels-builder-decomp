using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using Crosstales.BWF;
using Crosstales.BWF.Filter;
using Crosstales.BWF.Model;
using TMPro;
using UnityEngine;

public class Profanity : MonoBehaviour
{
	public static List<string> whiteList = new List<string>(new string[47]
	{
		"joker", "queen", "hole", "hose", "lick", "king", "house", "hardcore", "hole", "bars",
		"fat", "jack", "jerk", "lame", "lard", "bag", "bang", "bite", "clown", "crack",
		"face", "goblin", "hat", "head", "jack", "lick", "lol", "monk", "pirate", "hit",
		"wad", "wipe", "ax", "wound", "bam", "blow", "bog", "cracker", "hot", "jack",
		"jerk", "poop", "snatch", "snowball", "spastic", "squirt", "s"
	});

	public TMP_InputField WordInput;

	public TextMeshProUGUI ResultOutput;

	public static List<string> badWords = new List<string>();

	public static ManagerMask BadwordManager = ManagerMask.BadWord;

	private BadWordFilter bwf;

	private void Start()
	{
		bwf = (BadWordFilter)BWFManager.Filter();
	}

	public static bool NaughtyString(string s)
	{
		string text = s.ToLower();
		badWords = BWFManager.GetAll(s, BadwordManager);
		badWords = badWords.Except(whiteList).ToList();
		if (badWords != null && badWords.Count > 0)
		{
			return true;
		}
		return false;
	}

	public static string SanitizeString(string s)
	{
		s = ToFamilyFriendlyString(s);
		s = Regex.Replace(s, "\"", "“");
		return s;
	}

	public static string ToFamilyFriendlyString(string input)
	{
		badWords = BWFManager.GetAll(input, BadwordManager);
		badWords = badWords.Except(whiteList).ToList();
		if (badWords.Count > 0)
		{
			return BWFManager.Filter().Replace(input, badWords);
		}
		return input;
	}
}
