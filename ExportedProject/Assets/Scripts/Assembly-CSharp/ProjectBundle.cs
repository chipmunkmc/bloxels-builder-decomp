public struct ProjectBundle
{
	public byte[] bytes;

	public string json;

	public static ProjectBundle Invalid = new ProjectBundle(null, null);

	public ProjectBundle(byte[] _bytes, string _json)
	{
		bytes = _bytes;
		json = _json;
	}
}
