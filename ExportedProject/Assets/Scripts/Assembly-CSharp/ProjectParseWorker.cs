using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using VoxelEngine;

public class ProjectParseWorker : ThreadedJob
{
	public class Job
	{
		public CanvasSquare square;

		public WorldPos2D worldPos;

		public string json;

		public Job(CanvasSquare square, WorldPos2D worldPos, string json)
		{
			this.square = square;
			this.worldPos = worldPos;
			this.json = json;
		}
	}

	public Queue<Job> parseJobs;

	public AutoResetEvent waitHandle;

	public CanvasSquare currentSquare;

	public WorldPos2D currentCoordinates = new WorldPos2D(0, 0);

	public string currentJSON;

	public BloxelProject currentProject;

	private bool _shouldKeepRunning;

	private object _runHandle = new object();

	public bool shouldKeepRunning
	{
		get
		{
			lock (_runHandle)
			{
				return _shouldKeepRunning;
			}
		}
		set
		{
			lock (_runHandle)
			{
				_shouldKeepRunning = value;
			}
		}
	}

	public void Init()
	{
		parseJobs = new Queue<Job>(100);
		waitHandle = new AutoResetEvent(false);
		shouldKeepRunning = true;
	}

	public override void Interrupt()
	{
		ClearJobs();
		shouldKeepRunning = false;
		waitHandle.Set();
	}

	protected override void ThreadFunction()
	{
		try
		{
			while (shouldKeepRunning)
			{
				if (!base.isDone && parseJobs.Count > 0)
				{
					Job job = null;
					lock (parseJobs)
					{
						job = parseJobs.Dequeue();
					}
					ParseJob(job.square, job.worldPos, job.json);
					base.isDone = true;
				}
				waitHandle.WaitOne();
			}
			UnityEngine.Debug.LogError("Parse worker canceled normally.");
			waitHandle.Close();
		}
		catch (Exception ex)
		{
			UnityEngine.Debug.Log("ProjectParseWorker thread interrupted: " + ex.ToString());
		}
	}

	protected override void Run()
	{
		ThreadFunction();
	}

	public override IEnumerator WaitFor()
	{
		while (true)
		{
			Update();
			yield return null;
		}
	}

	public override bool Update()
	{
		if (base.isDone)
		{
			base.isDone = false;
			if (currentSquare == null || currentProject == null)
			{
				return false;
			}
			CanvasSquare value = null;
			CanvasStreamingInterface.instance.canvasSquaresDict.TryGetValue(currentCoordinates, out value);
			if (value != currentSquare)
			{
				return false;
			}
			if (value.serverSquare == null)
			{
				return false;
			}
			if (string.IsNullOrEmpty(currentJSON))
			{
				return false;
			}
			value.project = currentProject;
			switch (currentProject.type)
			{
			case ProjectType.Animation:
				value.AnimationParseComplete(currentProject as BloxelAnimation);
				break;
			case ProjectType.Character:
				value.CharacterParseComplete(currentProject as BloxelCharacter);
				break;
			}
			currentProject = null;
		}
		if (parseJobs.Count > 0)
		{
			if (!_thread.IsAlive && Gandalf.instance != null)
			{
				UnityEngine.Debug.LogError("Parse worker thread is dead, but Gandalf is still operational. Spinning up a new worker...");
				Start();
			}
			waitHandle.Set();
		}
		return false;
	}

	public void EnqueueParseJob(CanvasSquare square, WorldPos2D originalCoordinates, string json)
	{
		square.SetWaitingState();
		lock (parseJobs)
		{
			parseJobs.Enqueue(new Job(square, originalCoordinates, json));
		}
	}

	public void ClearJobs()
	{
		lock (parseJobs)
		{
			parseJobs.Clear();
		}
	}

	private void ParseJob(CanvasSquare square, WorldPos2D originalCoordinates, string json)
	{
		if (square.serverSquare == null)
		{
			currentProject = null;
			currentSquare = null;
			currentJSON = null;
			return;
		}
		if (!square.iWallCoordinate.Equals(originalCoordinates))
		{
			currentProject = null;
			currentSquare = null;
			currentJSON = null;
			return;
		}
		switch (square.serverSquare.type)
		{
		case ProjectType.Animation:
			currentProject = new BloxelAnimation(json, DataSource.JSONString);
			break;
		case ProjectType.Character:
			currentProject = new BloxelCharacter(json, DataSource.JSONString);
			break;
		}
		currentCoordinates = originalCoordinates;
		currentSquare = square;
		currentJSON = json;
	}
}
