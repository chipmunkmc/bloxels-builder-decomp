using System;

public class ProjectSyncInfo : IEquatable<ProjectSyncInfo>
{
	public Guid guid;

	public ProjectType type;

	public string disk;

	public string s3;

	public static ProjectSyncInfo Invalid = new ProjectSyncInfo(string.Empty, string.Empty, Guid.Empty, ProjectType.None);

	public ProjectSyncInfo(string _local, string _s3, Guid _guid, ProjectType _type)
	{
		disk = _local;
		s3 = _s3;
		type = _type;
		guid = _guid;
	}

	public static ProjectSyncInfo Create(BloxelProject _project)
	{
		string text = string.Empty;
		if (CurrentUser.instance.active)
		{
			text = CurrentUser.instance.account.serverID;
		}
		string absolutePathForType = BloxelProject.GetAbsolutePathForType(_project.type, _project.ID());
		string text2 = text + "/" + BloxelProject.GetRelativePathForType(_project.type, _project.ID());
		return new ProjectSyncInfo(absolutePathForType, text2, _project.id, _project.type);
	}

	public bool Equals(ProjectSyncInfo other)
	{
		return guid.Equals(other.guid);
	}

	public override bool Equals(object obj)
	{
		return Equals(obj as ProjectSyncInfo);
	}

	public override int GetHashCode()
	{
		return guid.GetHashCode();
	}
}
