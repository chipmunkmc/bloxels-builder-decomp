public enum ProjectType
{
	Board = 0,
	Animation = 1,
	MegaBoard = 2,
	Level = 3,
	Character = 4,
	Game = 5,
	Music = 6,
	Weapon = 7,
	SFXPack = 8,
	Program = 9,
	ProgramBlock = 10,
	Brain = 11,
	None = 12
}
