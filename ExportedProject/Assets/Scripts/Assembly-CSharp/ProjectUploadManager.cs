using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectUploadManager : MonoBehaviour
{
	public static ProjectUploadManager instance;

	public CoverBoard uploadCover;

	public string latestS3url;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		uploadCover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	private bool GoodResponse(string response)
	{
		return !string.IsNullOrEmpty(response) && response != "ERROR";
	}

	public void Upload(BloxelProject project, Action<string, string> callback)
	{
		StartCoroutine(BloxelServerRequests.instance.CompressAndUpload(project, delegate(string serverID)
		{
			if (GoodResponse(serverID))
			{
				project._serverID = serverID;
				StartCoroutine(UploadPNG(project, delegate(string S3URL)
				{
					if (GoodResponse(S3URL))
					{
						callback(serverID, S3URL);
					}
					else
					{
						callback(string.Empty, string.Empty);
					}
				}));
			}
			else
			{
				callback(string.Empty, string.Empty);
			}
		}));
	}

	private IEnumerator UploadPNG(BloxelProject bloxelProject, Action<string> callback)
	{
		Texture2D tex = bloxelProject.coverBoard.UpdateTexture2D(ref uploadCover.colors, ref uploadCover.texture, uploadCover.style);
		byte[] bytes = tex.EncodeToPNG();
		WWWForm form = new WWWForm();
		string fileName = bloxelProject._serverID + ".png";
		form.AddBinaryData("file", bytes, fileName, "image/png");
		Dictionary<string, string> headers = form.headers;
		headers["Auth-Token"] = CurrentUser.instance.loginToken;
		headers["Auth-UserID"] = CurrentUser.instance.account.serverID;
		WWW www = new WWW(postData: form.data, url: BloxelServerInterface.instance.uploadURL, headers: headers);
		yield return www;
		if (www.error != null)
		{
			callback("ERROR");
		}
		else
		{
			callback(www.text);
		}
	}
}
