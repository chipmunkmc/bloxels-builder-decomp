using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class QuickStartGroup : MonoBehaviour
{
	public enum Type
	{
		Layout = 0,
		Character = 1,
		Theme = 2
	}

	[Header("Data")]
	public BloxelProject currentChoice;

	public Type templateType;

	public UIQuickStartTemplate[] choices;

	[Header("UI")]
	public GridLayoutGroup grid;

	public RawImage previewImage;

	public CoverBoard previewCover;

	public TextMeshProUGUI uiTextChoiceTitle;

	public UIButton uiButtonCapture;

	public Object uiTemplatePrefab;

	public bool isReady;

	private void Awake()
	{
		previewCover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	private void Start()
	{
		if (AssetManager.instance.templatesLoaded)
		{
			BuildTemplates();
			choices = GetComponentsInChildren<UIQuickStartTemplate>();
			if (templateType != Type.Theme)
			{
				uiButtonCapture.OnClick += Capture;
			}
			previewImage.texture = AssetManager.instance.boardSolidBlank;
		}
	}

	private void OnDestroy()
	{
		if (templateType != Type.Theme)
		{
			uiButtonCapture.OnClick -= Capture;
		}
	}

	private void Capture()
	{
		GameQuickStartMenu.instance.captureType = templateType;
		CaptureManager.instance.Show();
	}

	public void Reset()
	{
		previewImage.texture = AssetManager.instance.boardSolidBlank;
		currentChoice = null;
		uiTextChoiceTitle.text = "---";
		isReady = false;
	}

	private void BuildTemplates()
	{
		switch (templateType)
		{
		case Type.Layout:
		{
			for (int j = 0; j < AssetManager.instance.wireframeTemplates.Length; j++)
			{
				GameObject gameObject2 = Object.Instantiate(uiTemplatePrefab) as GameObject;
				gameObject2.transform.SetParent(grid.transform);
				gameObject2.transform.localScale = Vector3.one;
				gameObject2.transform.localPosition = Vector3.zero;
				UIQuickStartTemplate component2 = gameObject2.GetComponent<UIQuickStartTemplate>();
				component2.menuParent = this;
				component2.dataModel = AssetManager.instance.wireframeTemplates[j];
			}
			break;
		}
		case Type.Character:
		{
			for (int k = 0; k < AssetManager.instance.characterTemplates.Length; k++)
			{
				GameObject gameObject3 = Object.Instantiate(uiTemplatePrefab) as GameObject;
				gameObject3.transform.SetParent(grid.transform);
				gameObject3.transform.localScale = Vector3.one;
				gameObject3.transform.localPosition = Vector3.zero;
				UIQuickStartTemplate component3 = gameObject3.GetComponent<UIQuickStartTemplate>();
				component3.menuParent = this;
				component3.dataModel = AssetManager.instance.characterTemplates[k];
			}
			break;
		}
		case Type.Theme:
		{
			for (int i = 0; i < AssetManager.instance.themes.Length; i++)
			{
				Dictionary<BlockColor, BloxelProject> dictionary = AssetManager.instance.themes[i];
				GameObject gameObject = Object.Instantiate(uiTemplatePrefab) as GameObject;
				gameObject.transform.SetParent(grid.transform);
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.localPosition = Vector3.zero;
				UIQuickStartTemplate component = gameObject.GetComponent<UIQuickStartTemplate>();
				component.menuParent = this;
				component.dataModel = dictionary[BlockColor.White];
				component.theme = (ThemeType)i;
			}
			break;
		}
		}
	}

	public void SetCurrentChoice(BloxelProject p)
	{
		isReady = true;
		currentChoice = p;
		string text = p.title;
		if (p.type != ProjectType.Character)
		{
			text = LocalizationManager.getInstance().getLocalizedText(p.title, p.title);
		}
		uiTextChoiceTitle.text = text;
		previewImage.texture = p.coverBoard.UpdateTexture2D(ref previewCover.colors, ref previewCover.texture, previewCover.style);
	}
}
