using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using UnityEngine;

public class RandomNumbers : MonoBehaviour
{
	private static System.Random random = new System.Random();

	private void Start()
	{
		Stopwatch stopwatch = new Stopwatch();
		stopwatch.Start();
		List<int> list = GenerateRandom(7000, 10000000, 99999999);
		stopwatch.Stop();
		TextWriter textWriter = new StreamWriter(Application.persistentDataPath + "/SavedList.txt");
		foreach (int item in list)
		{
			textWriter.WriteLine(item);
		}
		textWriter.Close();
	}

	public static List<int> GenerateRandom(int count, int min, int max)
	{
		if (max <= min || count < 0 || (count > max - min && max - min > 0))
		{
			throw new ArgumentOutOfRangeException("Range " + min + " to " + max + " (" + ((long)max - (long)min) + " values), or count " + count + " is illegal");
		}
		HashSet<int> hashSet = new HashSet<int>();
		for (int i = max - count; i < max; i++)
		{
			if (!hashSet.Add(random.Next(min, i + 1)))
			{
				hashSet.Add(i);
			}
		}
		List<int> list = hashSet.ToList();
		for (int num = list.Count - 1; num > 0; num--)
		{
			int index = random.Next(num + 1);
			int value = list[index];
			list[index] = list[num];
			list[num] = value;
		}
		return list;
	}
}
