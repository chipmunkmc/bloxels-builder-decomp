using UnityEngine;

public struct RectPosition
{
	public Direction dir;

	public Vector2 pos;

	public Vector2 pivot;

	public RectPosition(Direction _dir, Vector2 _pos)
	{
		dir = _dir;
		pos = _pos;
		pivot = PPUtilities.GetPivotFromDirection(_dir);
	}
}
