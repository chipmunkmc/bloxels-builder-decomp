public class RegisterDOB : RegisterMenu
{
	public UIDropdownGroupDOB dobComponent;

	private void Start()
	{
		if (controller == null)
		{
			controller = GetComponentInParent<RegistrationController>();
		}
		uiButtonNext.OnClick += Next;
	}

	private new void OnDestroy()
	{
		uiButtonNext.OnClick -= Next;
	}

	public override void Next()
	{
		controller.isOfAge = dobComponent.isOfAge;
		controller.dob = dobComponent.dob;
		if (controller.isOfAge)
		{
			menuNext = controller.menuLookup[RegistrationController.Step.EmailPassword];
		}
		else
		{
			menuNext = controller.menuLookup[RegistrationController.Step.ParentEmail];
		}
		base.Next();
	}
}
