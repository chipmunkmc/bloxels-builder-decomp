using System;
using System.Collections;
using TMPro;
using UnityEngine;

public class RegisterEmailPassword : RegisterMenu
{
	public TMP_InputField uiInputRegisterEmail;

	public TMP_InputField uiInputRegisterPassword;

	public TMP_InputField uiInputRegisterConfirmPassword;

	private UIInputValidation _emailValidation;

	[Range(0f, 2f)]
	public float inputTimer = 0.35f;

	private bool _emailValid;

	private bool _passwordValid;

	private bool _confirmPassswordValid;

	public bool emailValid
	{
		get
		{
			return _emailValid;
		}
		set
		{
			_emailValid = value;
			checkProgress();
		}
	}

	public bool passwordValid
	{
		get
		{
			return _passwordValid;
		}
		set
		{
			_passwordValid = value;
			checkProgress();
		}
	}

	public bool confirmPassswordValid
	{
		get
		{
			return _confirmPassswordValid;
		}
		set
		{
			_confirmPassswordValid = value;
			checkProgress();
		}
	}

	private new void Awake()
	{
		base.Awake();
		uiInputRegisterEmail.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckEmailTimer");
			StartCoroutine("CheckEmailTimer");
		});
		uiInputRegisterPassword.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckInputPassword");
			StartCoroutine("CheckInputPassword");
		});
		uiInputRegisterConfirmPassword.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckInputPasswordConfirm");
			StartCoroutine("CheckInputPasswordConfirm");
		});
	}

	private void Start()
	{
		uiMenuPanel.OverrideDismiss();
		UnsubscribeNext();
		uiButtonNext.OnClick += Next;
		uiMenuPanel.uiButtonDismiss.OnClick += base.BackToWelcome;
		canProgress = false;
		ToggleNextButtonState();
		_emailValidation = uiInputRegisterEmail.GetComponent<UIInputValidation>();
	}

	private new void OnDestroy()
	{
		uiButtonNext.OnClick -= Next;
		uiMenuPanel.uiButtonDismiss.OnClick -= base.BackToWelcome;
		base.OnDestroy();
	}

	private IEnumerator CheckEmailTimer()
	{
		yield return new WaitForSeconds(inputTimer);
		CheckEmailInput();
	}

	private IEnumerator CheckInputPassword()
	{
		yield return new WaitForSeconds(inputTimer);
		passwordValid = controller.CheckEmpty(uiInputRegisterPassword);
	}

	private IEnumerator CheckInputPasswordConfirm()
	{
		yield return new WaitForSeconds(inputTimer);
		confirmPassswordValid = controller.CheckMatch(uiInputRegisterPassword, uiInputRegisterConfirmPassword);
	}

	public void CheckEmailInput()
	{
		if (!controller.CheckEmpty(uiInputRegisterEmail) || !controller.CheckEmailFormat(uiInputRegisterEmail))
		{
			emailValid = false;
			return;
		}
		StartCoroutine(CheckServer(delegate(bool exists)
		{
			emailValid = !exists;
			_emailValidation.type = UIInputValidation.Type.AlreadyExists;
			if (emailValid)
			{
				_emailValidation.ForceValid();
			}
		}));
	}

	private IEnumerator CheckServer(Action<bool> callback)
	{
		yield return controller.CheckUserPropertyExists(RegistrationController.string_userEmail, uiInputRegisterEmail, delegate(bool exists)
		{
			callback(exists);
		});
	}

	private void checkProgress()
	{
		if (emailValid && passwordValid && confirmPassswordValid)
		{
			canProgress = true;
		}
		else
		{
			canProgress = false;
		}
		ToggleNextButtonState();
	}

	public override void Next()
	{
		controller.chosenEmail = uiInputRegisterEmail.text;
		controller.chosenPw = uiInputRegisterPassword.text;
		base.Next();
	}
}
