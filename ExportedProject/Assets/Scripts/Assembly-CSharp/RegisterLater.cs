using UnityEngine;
using UnityEngine.UI;

public class RegisterLater : RegisterMenu
{
	public RawImage uiRawImage;

	private RegisterUsername _userNameMenu;

	private void Start()
	{
		_userNameMenu = controller.menuLookup[RegistrationController.Step.Username] as RegisterUsername;
		_userNameMenu.OnAvatarChange += UpdateAvatar;
		uiMenuPanel.OverrideDismiss();
		uiButtonNext.OnClick += Next;
		uiMenuPanel.uiButtonDismiss.OnClick += base.BackToWelcome;
		canProgress = true;
		ToggleNextButtonState();
	}

	private new void OnDestroy()
	{
		uiButtonNext.OnClick -= Next;
		uiMenuPanel.uiButtonDismiss.OnClick -= base.BackToWelcome;
		_userNameMenu.OnAvatarChange -= UpdateAvatar;
	}

	public void UpdateAvatar(Texture texture)
	{
		uiRawImage.texture = texture;
	}

	public override void Next()
	{
		controller.menuController.Dismiss();
	}
}
