using DG.Tweening;

public class RegisterMenu : SequentialMenu
{
	public UIMenuPanel uiMenuPanel;

	public RegistrationController controller;

	public RegistrationController.Step step;

	public new void Awake()
	{
		base.Awake();
		uiMenuPanel = GetComponent<UIMenuPanel>();
	}

	public void BackToWelcome()
	{
		Hide().OnComplete(delegate
		{
			controller.SwitchStep(RegistrationController.Step.Welcome);
		});
	}
}
