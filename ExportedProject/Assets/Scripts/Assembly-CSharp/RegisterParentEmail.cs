using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;

public class RegisterParentEmail : RegisterMenu
{
	public TMP_InputField uiInputField;

	public TextMeshProUGUI uiErrorMessage;

	public string emailingMessage = "Emailing now, please be patient...";

	public string errorMessage = "Something went wrong, please close this window and try again. If the probelm persists, contact support@bloxelsbuilder.com";

	[Range(0f, 2f)]
	public float inputTimer = 0.35f;

	private new void Awake()
	{
		base.Awake();
		emailingMessage = LocalizationManager.getInstance().getLocalizedText("account35", "Emailing now, please be patient...");
		errorMessage = LocalizationManager.getInstance().getLocalizedText("account105", "Something went wrong, please close this window and try again. If the probelm persists, contact support@bloxelsbuilder.com");
		uiInputField.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckInputTimer");
			StartCoroutine("CheckInputTimer");
		});
	}

	private void Start()
	{
		uiMenuPanel.OverrideDismiss();
		UnsubscribeNext();
		uiButtonNext.OnClick += Next;
		uiMenuPanel.uiButtonDismiss.OnClick += base.BackToWelcome;
		ToggleNextButtonState();
		uiErrorMessage.transform.localScale = Vector3.zero;
	}

	private new void OnDestroy()
	{
		uiButtonNext.OnClick -= Next;
		base.OnDestroy();
	}

	private IEnumerator CheckInputTimer()
	{
		yield return new WaitForSeconds(inputTimer);
		canProgress = CheckSimple();
		ToggleNextButtonState();
	}

	private bool CheckSimple()
	{
		if (!controller.CheckEmpty(uiInputField))
		{
			return false;
		}
		if (!controller.CheckEmailFormat(uiInputField))
		{
			return false;
		}
		return true;
	}

	public override void Next()
	{
		uiButtonNext.interactable = false;
		controller.parentEmail = uiInputField.text;
		uiErrorMessage.SetText(emailingMessage);
		uiErrorMessage.transform.localScale = Vector3.one;
		if (!controller.isFormerEdu)
		{
			StartCoroutine(controller.CreateAccount(delegate(UserAccount account)
			{
				if (account == null)
				{
					uiErrorMessage.SetText(errorMessage);
					uiErrorMessage.transform.localScale = Vector3.one;
				}
				else
				{
					uiErrorMessage.transform.localScale = Vector3.zero;
				}
				CurrentUser.instance.Login(account);
				BloxelServerInterface.instance.Emit_TempLogin(account.serverID);
				_003CNext_003E__BaseCallProxy0();
			}));
			return;
		}
		CurrentUser.instance.account.parentEmail = controller.parentEmail;
		StartCoroutine(controller.UpdateAccount(delegate(UserAccount account)
		{
			if (account == null)
			{
				uiErrorMessage.SetText(errorMessage);
				uiErrorMessage.transform.localScale = Vector3.one;
			}
			else
			{
				uiErrorMessage.transform.localScale = Vector3.zero;
			}
			CurrentUser.instance.Login(account);
			BloxelServerInterface.instance.Emit_TempLogin(account.serverID);
			_003CNext_003E__BaseCallProxy0();
		}));
	}

	[CompilerGenerated]
	[DebuggerHidden]
	private void _003CNext_003E__BaseCallProxy0()
	{
		base.Next();
	}
}
