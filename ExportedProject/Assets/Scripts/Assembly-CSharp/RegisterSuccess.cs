using DG.Tweening;

public class RegisterSuccess : RegisterMenu
{
	private void Start()
	{
		uiMenuPanel.OverrideDismiss();
		UnsubscribeNext();
		uiButtonNext.OnClick += Next;
		uiMenuPanel.uiButtonDismiss.OnClick += Finish;
	}

	private new void OnDestroy()
	{
		uiButtonNext.OnClick -= Next;
		uiMenuPanel.uiButtonDismiss.OnClick -= Finish;
	}

	public override void Next()
	{
		Finish();
	}

	public void Finish()
	{
		Hide().OnComplete(delegate
		{
			controller.Complete();
		});
	}
}
