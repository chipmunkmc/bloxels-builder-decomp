using System;
using System.Collections;
using System.Threading;
using TMPro;
using UnityEngine;

public class RegisterUsername : RegisterMenu
{
	public delegate void HandleAvatarChange(Texture texture);

	public TMP_InputField uiInputField;

	private UIAvatarSelector[] uiAvatars;

	public UIAvatarSelector currentAvatar;

	[Range(0f, 2f)]
	public float inputTimer = 1f;

	public event HandleAvatarChange OnAvatarChange;

	private new void Awake()
	{
		base.Awake();
		uiAvatars = GetComponentsInChildren<UIAvatarSelector>();
		uiInputField.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckInputTimer");
			StartCoroutine("CheckInputTimer");
		});
	}

	private void Start()
	{
		uiMenuPanel.OverrideDismiss();
		uiButtonNext.OnClick += Next;
		uiMenuPanel.uiButtonDismiss.OnClick += base.BackToWelcome;
		if (controller == null)
		{
			controller = GetComponentInParent<RegistrationController>();
		}
		PopulateAvatars();
		for (int i = 0; i < uiAvatars.Length; i++)
		{
			uiAvatars[i].OnToggle += ToggleAvatar;
		}
		ToggleNextButtonState();
		ToggleAvatar(uiAvatars[0]);
	}

	private new void OnDestroy()
	{
		uiButtonNext.OnClick -= Next;
		uiMenuPanel.uiButtonDismiss.OnClick -= base.BackToWelcome;
	}

	private IEnumerator CheckInputTimer()
	{
		yield return new WaitForSeconds(inputTimer);
		if (CheckSimple())
		{
			StartCoroutine(CheckServer(delegate(bool exists)
			{
				canProgress = !exists;
				ToggleNextButtonState();
			}));
		}
		else
		{
			canProgress = false;
			ToggleNextButtonState();
		}
	}

	public void ToggleAvatar(UIAvatarSelector avatar)
	{
		for (int i = 0; i < uiAvatars.Length; i++)
		{
			if (uiAvatars[i] != avatar)
			{
				uiAvatars[i].DeActivate();
			}
		}
		currentAvatar = avatar;
		currentAvatar.Activate();
		if (this.OnAvatarChange != null)
		{
			this.OnAvatarChange(avatar.uiRawImage.texture);
		}
	}

	private void PopulateAvatars()
	{
		for (int i = 0; i < BloxelServerInterface.instance.defaultAvatars.Count; i++)
		{
			uiAvatars[i].BuildBoard(BloxelServerInterface.instance.defaultAvatars[i]);
		}
	}

	private bool CheckSimple()
	{
		if (currentAvatar == null)
		{
			return false;
		}
		if (!controller.CheckEmpty(uiInputField))
		{
			return false;
		}
		if (!controller.CheckNaughty(uiInputField))
		{
			return false;
		}
		return true;
	}

	private IEnumerator CheckServer(Action<bool> callback)
	{
		yield return controller.CheckUserPropertyExists(RegistrationController.string_userName, uiInputField, delegate(bool exists)
		{
			callback(exists);
		});
	}

	public override void Next()
	{
		controller.chosenUserName = uiInputField.text;
		controller.avatarBoardId = currentAvatar.serverId;
		base.Next();
	}
}
