using System.Collections;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using TMPro;
using UnityEngine;

public class RegisterVerifyCode : RegisterMenu
{
	public TMP_InputField uiInputField;

	public UIButton uiButtonLater;

	public UIConfirmTip uiButtonRestart;

	public TextMeshProUGUI uiTextReminder;

	private UIMenuPanel _menuPanel;

	[Range(0f, 2f)]
	public float inputTimer = 0.35f;

	public static string ApprovalMessage = "approved";

	private new void Awake()
	{
		base.Awake();
		_menuPanel = GetComponent<UIMenuPanel>();
		uiInputField.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckInputTimer");
			StartCoroutine("CheckInputTimer");
		});
	}

	private void Start()
	{
		uiMenuPanel.OverrideDismiss();
		uiButtonNext.OnClick += Next;
		uiButtonLater.OnClick += VerifyLater;
		uiButtonRestart.Init(RestartProcess);
		uiMenuPanel.uiButtonDismiss.OnClick += base.BackToWelcome;
		ToggleNextButtonState();
		BloxelServerInterface.instance.OnApproved += ParentApproved;
		controller.OnParentEmail += HandleParentEmail;
		if (CurrentUser.instance.account != null && !string.IsNullOrEmpty(CurrentUser.instance.account.parentEmail))
		{
			uiTextReminder.SetText(CurrentUser.instance.account.parentEmail);
		}
	}

	private new void OnDestroy()
	{
		BloxelServerInterface.instance.OnApproved -= ParentApproved;
		controller.OnParentEmail -= HandleParentEmail;
		uiButtonNext.OnClick -= Next;
		uiButtonLater.OnClick -= VerifyLater;
		base.OnDestroy();
	}

	private void HandleParentEmail(string email)
	{
		uiTextReminder.SetText(email);
	}

	private IEnumerator CheckInputTimer()
	{
		yield return new WaitForSeconds(inputTimer);
		canProgress = CheckSimple();
		canProgress = CheckMatch();
		ToggleNextButtonState();
	}

	private void ParentApproved()
	{
		CurrentUser.instance.account.status = UserAccount.Status.active;
		CurrentUser.instance.Save();
		base.Next();
	}

	private void VerifyLater()
	{
		menuNext = controller.menuLookup[RegistrationController.Step.VerifyLater];
		base.Next();
	}

	private bool CheckSimple()
	{
		if (!controller.CheckEmpty(uiInputField))
		{
			return false;
		}
		return true;
	}

	public bool CheckMatch()
	{
		UIInputValidation component = uiInputField.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.Match;
		if (uiInputField.text != CurrentUser.instance.account.verifyCode)
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public void RestartProcess()
	{
		CurrentUser.instance.account = null;
		CurrentUser.instance.loginToken = string.Empty;
		CurrentUser.instance.Logout();
		if (PrefsManager.instance.hasUserAccount)
		{
			PrefsManager.instance.RemoveLoggedInUser();
		}
		_menuPanel.controller.Dismiss();
	}

	public override void Next()
	{
		StartCoroutine(BloxelServerRequests.instance.UpdateUserStatus(CurrentUser.instance.account.serverID, ApprovalMessage, delegate(string response)
		{
			if (BloxelServerRequests.ResponseOk(response))
			{
				CurrentUser.instance.account.status = UserAccount.Status.active;
				CurrentUser.instance.Save();
				_003CNext_003E__BaseCallProxy0();
			}
		}));
	}

	[CompilerGenerated]
	[DebuggerHidden]
	private void _003CNext_003E__BaseCallProxy0()
	{
		base.Next();
	}
}
