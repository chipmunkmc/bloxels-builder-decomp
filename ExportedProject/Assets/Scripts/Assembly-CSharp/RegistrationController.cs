using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class RegistrationController : MonoBehaviour
{
	public enum Step
	{
		Welcome = 0,
		Username = 1,
		DOB = 2,
		ParentEmail = 3,
		VerifyCode = 4,
		VerifyLater = 5,
		EmailPassword = 6,
		AccountAgreements = 7,
		Success = 8,
		LearnMore = 9
	}

	public delegate void HandleParentEmail(string parentEmail);

	public UIPopupAccount menuController;

	public Step previousStep;

	public Step currentStep;

	public RegisterMenu[] menus;

	public Dictionary<Step, RegisterMenu> menuLookup;

	public static string string_userName = "userName";

	public static string string_userEmail = "userEmail";

	public static readonly string tmpPassword = "tmpPasswordFooz";

	[Header("Data")]
	public string avatarBoardId;

	public string chosenUserName;

	public bool isOfAge;

	public string dob;

	public bool isFormerEdu;

	public bool shouldGetMarketingEmails;

	public string chosenEmail;

	public string chosenPw;

	private string _parentEmail;

	public string parentEmail
	{
		get
		{
			return _parentEmail;
		}
		set
		{
			_parentEmail = value;
			if (this.OnParentEmail != null)
			{
				this.OnParentEmail(_parentEmail);
			}
		}
	}

	public event HandleParentEmail OnParentEmail;

	private void Awake()
	{
		menus = GetComponentsInChildren<RegisterMenu>(true);
		menuLookup = new Dictionary<Step, RegisterMenu>();
		for (int i = 0; i < menus.Length; i++)
		{
			menuLookup[menus[i].step] = menus[i];
			menus[i].Init();
		}
	}

	public void SwitchStep(Step theStep)
	{
		previousStep = currentStep;
		menuLookup[theStep].Show();
		currentStep = theStep;
	}

	public bool CheckEmpty(InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.Blank;
		if (string.IsNullOrEmpty(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public bool CheckEmpty(TMP_InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.Blank;
		if (string.IsNullOrEmpty(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public bool CheckNaughty(InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.Naughty;
		if (Profanity.NaughtyString(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public bool CheckNaughty(TMP_InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.Naughty;
		if (Profanity.NaughtyString(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public bool CheckMatch(InputField a, InputField b)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		UIInputValidation component2 = b.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.Match;
		component2.type = UIInputValidation.Type.Match;
		if (a.text != b.text)
		{
			component2.InValid();
			return false;
		}
		component2.Valid();
		return true;
	}

	public bool CheckMatch(TMP_InputField a, TMP_InputField b)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		UIInputValidation component2 = b.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.Match;
		component2.type = UIInputValidation.Type.Match;
		if (a.text != b.text)
		{
			component2.InValid();
			return false;
		}
		component2.Valid();
		return true;
	}

	public bool CheckEmailFormat(InputField a)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.InvalidAccountInfo;
		if (!Regex.IsMatch(a.text, "\\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\\Z", RegexOptions.IgnoreCase))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public bool CheckEmailFormat(TMP_InputField a)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		component.type = UIInputValidation.Type.InvalidAccountInfo;
		if (!Regex.IsMatch(a.text, "\\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\\Z", RegexOptions.IgnoreCase))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public IEnumerator CheckUserPropertyExists(string type, InputField a, Action<bool> cb)
	{
		UIInputValidation validation = a.GetComponent<UIInputValidation>();
		validation.type = UIInputValidation.Type.AlreadyExists;
		yield return StartCoroutine(BloxelServerRequests.instance.AccountPropertyExists(type, a.text, delegate(bool exists)
		{
			if (exists)
			{
				validation.InValid();
			}
			else
			{
				validation.Valid();
			}
			cb(exists);
		}));
	}

	public IEnumerator CheckUserPropertyExists(string type, TMP_InputField a, Action<bool> cb)
	{
		UIInputValidation validation = a.GetComponent<UIInputValidation>();
		validation.type = UIInputValidation.Type.AlreadyExists;
		yield return StartCoroutine(BloxelServerRequests.instance.AccountPropertyExists(type, a.text, delegate(bool exists)
		{
			if (exists)
			{
				validation.InValid();
			}
			else
			{
				validation.Valid();
			}
			cb(exists);
		}));
	}

	public IEnumerator UpdateAccount(Action<UserAccount> callback)
	{
		yield return StartCoroutine(BloxelServerRequests.instance.UpdateFormerEDUAccount(delegate(string response)
		{
			if (!BloxelServerRequests.ResponseOk(response))
			{
				callback(null);
			}
			else
			{
				callback(new UserAccount(response));
			}
		}));
	}

	public IEnumerator CreateAccount(Action<UserAccount> callback)
	{
		yield return StartCoroutine(BloxelServerRequests.instance.RegisterUser(chosenUserName, avatarBoardId, parentEmail, tmpPassword, isOfAge, delegate(string response)
		{
			if (!BloxelServerRequests.ResponseOk(response))
			{
				callback(null);
			}
			else
			{
				callback(new UserAccount(response));
			}
		}));
	}

	public void Complete()
	{
		menuController.RegisterComplete();
	}
}
