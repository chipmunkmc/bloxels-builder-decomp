using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using BestHTTP;
using DiscoData;
using UnityEngine;

public class S3Interface : MonoBehaviour
{
	public static S3Interface instance;

	public static string S3UtilityEndpoint = "http://bloxels-s3-sync.us-west-2.elasticbeanstalk.com/";

	public static readonly string bucketRoot = "https://s3-us-west-2.amazonaws.com/";

	public static readonly string S3ColorDataLatest = "captureData/";

	public static readonly string S3ColorDataSuffix = "production";

	public static readonly int MaxCaptures = 20;

	public static int Captures;

	public Bucket bucket;

	public Dictionary<Bucket, string> bucketMap;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		StartCoroutine(GetCurrentCaptureData(delegate(byte[] stream)
		{
			string persistentDataPath = Application.persistentDataPath;
			if (stream != null)
			{
				Zipper.ExtractFromStream(stream, persistentDataPath, false);
				int num = (int)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
				PrefsManager.instance.captureVersion = num.ToString();
			}
		}));
	}

	public IEnumerator GetCurrentCaptureData(Action<byte[]> callback)
	{
		string device = SystemInfo.deviceModel.Replace(' ', '_');
		WWW www = new WWW(S3UtilityEndpoint + S3ColorDataLatest + device + "/" + S3ColorDataSuffix);
		yield return www;
		if (www.error == null)
		{
			callback(www.bytes);
		}
		else
		{
			callback(null);
		}
	}

	public IEnumerator GetAllCaptureImages(Action<List<string>> callback)
	{
		string url = S3UtilityEndpoint + "capture";
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			string @string = Encoding.UTF8.GetString(www.bytes);
			List<string> list = new List<string>();
			JSONObject jSONObject = new JSONObject(@string);
			for (int i = 0; i < jSONObject.list.Count; i++)
			{
				if (jSONObject.list[i].HasField("Key"))
				{
					list.Add(bucketRoot + "bloxels-capture-results/" + jSONObject.list[i].GetField("Key").str);
				}
			}
			callback(list);
		}
		else
		{
			callback(null);
		}
		yield return null;
	}

	public IEnumerator UploadCaptureScreen(int unixTimestamp, byte[] jpegBuffer, CapturedFrameMetaData metaData)
	{
		string key2 = string.Empty;
		key2 += "Android/";
		Captures++;
		string deviceString = SystemInfo.deviceModel.Replace(' ', '+');
		string text = key2;
		key2 = text + deviceString + "/" + unixTimestamp + ".jpg";
		string url = S3UtilityEndpoint + "capture/postImage";
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			bool flag = ((resp.StatusCode == 201 || resp.StatusCode == 200) ? true : false);
			if (ChoosyCornerController.Instance != null)
			{
				ChoosyCornerController.Instance.savingOverlay.SetActive(false);
			}
		});
		request.AddBinaryData(key2, jpegBuffer, key2, "image/jpeg");
		request.Send();
		yield return StartCoroutine(request);
		if (metaData != null)
		{
			metaData.SaveToS3(key2);
		}
	}

	public void UploadCaptureFrameMetaDataStub(string jsonString, string key)
	{
		StartCoroutine(UploadCaptureFrameMetaData(jsonString, key));
	}

	public IEnumerator UploadCaptureFrameMetaData(string jsonString, string key)
	{
		string reqURL = S3UtilityEndpoint + "capture/postMeta";
		HTTPRequest request = new HTTPRequest(new Uri(reqURL), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			bool flag = ((resp.StatusCode == 201 || resp.StatusCode == 200) ? true : false);
			if (ChoosyCornerController.Instance != null)
			{
				ChoosyCornerController.Instance.savingOverlay.SetActive(false);
			}
		});
		UTF8Encoding enc = new UTF8Encoding();
		byte[] bytes = enc.GetBytes(jsonString);
		request.AddBinaryData(key, bytes, key, "application/json");
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator MarkCaptureScreenComplete(string jpgURL)
	{
		string poo = "bloxels-capture-results/";
		int startIndex = jpgURL.IndexOf(poo) + poo.Length;
		string key = jpgURL.Substring(startIndex);
		string url = S3UtilityEndpoint + "capture/renameImage";
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate(HTTPRequest req, HTTPResponse resp)
		{
			if (resp.StatusCode != 200)
			{
				if (ChoosyCornerController.Instance != null)
				{
					ChoosyCornerController.Instance.savingOverlay.SetActive(false);
				}
			}
			else if (ChoosyCornerController.Instance != null)
			{
				if (ChoosyCornerController.Instance.savingOverlay.activeSelf)
				{
					ChoosyCornerController.Instance.savingOverlay.SetActive(false);
				}
				ChoosyCornerController.Instance.HandleImageDeleted();
			}
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		JSONObject obj = new JSONObject();
		obj.AddField("key", key);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(obj.ToString());
		request.Send();
		yield return StartCoroutine(request);
	}

	public IEnumerator DeleteCaptureData(string jpgURL, string jsonURL)
	{
		string poo = "bloxels-capture-results/";
		int startIndex2 = jpgURL.IndexOf(poo) + poo.Length;
		string jpgKey = jpgURL.Substring(startIndex2);
		string url = S3UtilityEndpoint + "capture/remove";
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate
		{
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		JSONObject obj = new JSONObject();
		obj.AddField("key", jpgKey);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(obj.ToString());
		request.Send();
		yield return StartCoroutine(request);
		startIndex2 = jsonURL.IndexOf(poo) + poo.Length;
		string jsonKey = jsonURL.Substring(startIndex2);
		yield return StartCoroutine(DeleteMetaFile(jsonKey));
	}

	public IEnumerator DeleteMetaFile(string key)
	{
		string url = S3UtilityEndpoint + "capture/remove";
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate
		{
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		JSONObject obj = new JSONObject();
		obj.AddField("key", key);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(obj.ToString());
		request.Send();
		yield return StartCoroutine(request);
	}

	private void Start()
	{
		bucketMap = new Dictionary<Bucket, string>
		{
			{
				Bucket.AccountCloud,
				"bloxels-account-cloud"
			},
			{
				Bucket.GameScreenshots,
				"bloxels-game-screenshots"
			},
			{
				Bucket.CaptureResults,
				"bloxels-capture-results"
			},
			{
				Bucket.News,
				"bloxels-news"
			},
			{
				Bucket.ProjectCovers,
				"bloxels-project-covers"
			},
			{
				Bucket.ProjectCoversOld,
				"pp-bloxels-canvas-canvas-dev"
			}
		};
		switch (S3UtilityEndpoint)
		{
		case "http://localhost:8080/":
			BloxelServerInterface.instance.env.transform.localScale = Vector3.one;
			BloxelServerInterface.instance.envText.SetText("L");
			break;
		case "http://lambdazip-dev.us-west-2.elasticbeanstalk.com/":
			BloxelServerInterface.instance.envText.SetText("S");
			BloxelServerInterface.instance.env.transform.localScale = Vector3.one;
			break;
		case "http://bloxels-s3-sync.us-west-2.elasticbeanstalk.com/":
			BloxelServerInterface.instance.env.transform.localScale = Vector3.zero;
			break;
		}
	}

	public bool CanUploadCapture()
	{
		return CurrentUser.instance != null && CurrentUser.instance.account != null && CurrentUser.instance.account.role >= 5;
	}

	public IEnumerator Upload(ProjectSyncInfo syncInfo, Action<bool, ProjectSyncInfo> callback)
	{
		if (!CurrentUser.instance.active)
		{
			callback(false, syncInfo);
			yield break;
		}
		string url = S3UtilityEndpoint + "users/" + CurrentUser.instance.account.serverID;
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Post, delegate(HTTPRequest req, HTTPResponse resp)
		{
			bool arg = ((resp.StatusCode == 201 || resp.StatusCode == 200) ? true : false);
			callback(arg, syncInfo);
		});
		if (File.Exists(syncInfo.disk))
		{
			byte[] content = File.ReadAllBytes(syncInfo.disk);
			request.AddBinaryData(syncInfo.s3, content, syncInfo.s3, "application/json");
			request.Send();
			yield return StartCoroutine(request);
		}
		else
		{
			callback(false, syncInfo);
		}
	}

	public IEnumerator DeleteFileFromBucket(ProjectSyncInfo syncInfo)
	{
		string url = S3UtilityEndpoint + "users/remove";
		HTTPRequest request = new HTTPRequest(new Uri(url), HTTPMethods.Put, delegate
		{
		});
		request.SetHeader("Content-Type", "application/json; charset=UTF-8");
		JSONObject obj = new JSONObject();
		obj.AddField("key", syncInfo.s3);
		UTF8Encoding enc = new UTF8Encoding();
		request.RawData = enc.GetBytes(obj.ToString());
		request.Send();
		yield return StartCoroutine(request);
	}
}
