using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveManager : MonoBehaviour
{
	public enum Method
	{
		Save = 0,
		Cloud = 1
	}

	public static SaveManager Instance;

	public bool isProcessing;

	public Method currentMethod;

	private float _timeBetweenUpdates = 3f;

	private float _timeOfLastUpdate;

	public Queue<BloxelProject> queue = new Queue<BloxelProject>();

	public HashSet<Guid> hash = new HashSet<Guid>();

	private static bool _initComplete;

	public Coroutine saveRoutine;

	public Coroutine syncRoutine;

	private int _savesPerFrame = 1;

	private void Awake()
	{
		if (Instance == null)
		{
			Instance = this;
		}
	}

	private void Start()
	{
		_initComplete = true;
	}

	private void Update()
	{
		if (_initComplete && !isProcessing && !(Time.time - _timeBetweenUpdates < _timeOfLastUpdate))
		{
			_timeOfLastUpdate = Time.time;
			if (currentMethod == Method.Save)
			{
				saveRoutine = StartCoroutine(save());
			}
			else
			{
				syncRoutine = StartCoroutine(CloudManager.Instance.Sync());
			}
		}
	}

	public void Enqueue(BloxelProject project)
	{
		if (!hash.Contains(project.id))
		{
			queue.Enqueue(project);
		}
	}

	public void Remove()
	{
		if (queue.Count != 0)
		{
			BloxelProject bloxelProject = queue.Dequeue();
			hash.Remove(bloxelProject.id);
			bloxelProject.Save(false, false);
		}
	}

	public IEnumerator ForceRun()
	{
		if (syncRoutine != null)
		{
			StopCoroutine(syncRoutine);
			syncRoutine = null;
			isProcessing = false;
		}
		if (saveRoutine != null)
		{
			_savesPerFrame = queue.Count;
			while (saveRoutine != null)
			{
				yield return new WaitForEndOfFrame();
			}
			_savesPerFrame = 1;
		}
		else
		{
			int count = queue.Count;
			for (int i = 0; i < count; i++)
			{
				Remove();
			}
		}
		if (CloudManager.Instance.queue.Count != 0)
		{
			CloudManager.Instance.savesPerFrame = CloudManager.Instance.queue.Count;
			syncRoutine = StartCoroutine(CloudManager.Instance.Sync());
			while (syncRoutine != null)
			{
				yield return new WaitForEndOfFrame();
			}
			CloudManager.Instance.savesPerFrame = 1;
		}
	}

	private IEnumerator save()
	{
		if (queue.Count == 0)
		{
			currentMethod = Method.Cloud;
			yield break;
		}
		isProcessing = true;
		int count = queue.Count;
		for (int i = 0; i < count; i++)
		{
			Remove();
			if (i % _savesPerFrame == 0)
			{
				yield return new WaitForEndOfFrame();
			}
		}
		isProcessing = false;
		currentMethod = Method.Cloud;
		saveRoutine = null;
	}
}
