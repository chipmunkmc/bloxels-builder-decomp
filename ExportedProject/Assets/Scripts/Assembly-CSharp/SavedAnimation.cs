public class SavedAnimation : SavedProject
{
	public new UIProjectCover cover;

	private new void Awake()
	{
		base.Awake();
		cover = GetComponent<UIProjectCover>();
	}

	private new void Start()
	{
		libraryWindow = BloxelAnimationLibrary.instance;
		base.Start();
		cover.Init(dataModel as BloxelAnimation, new CoverStyle(BloxelBoard.baseUIBoardColor), false);
	}

	private new void OnEnable()
	{
		base.OnEnable();
	}

	public override void SetData(BloxelProject project)
	{
		base.SetData(project);
		cover.ResetData(project as BloxelAnimation);
		if (dataModel == BloxelAnimationLibrary.instance.currentAnimation)
		{
			isActive = true;
			outline.enabled = true;
			cover.Animate();
			return;
		}
		isActive = false;
		outline.enabled = false;
		if (cover.shouldAnimate)
		{
			cover.StopAnimating();
		}
	}

	public new bool Delete()
	{
		bool flag = ((BloxelAnimation)dataModel).Delete();
		if (flag)
		{
			if (AssetManager.instance.animationPool.ContainsKey(dataModel.ID()))
			{
				AssetManager.instance.animationPool.Remove(dataModel.ID());
			}
			base.Delete();
		}
		return flag;
	}
}
