public class SavedBoard : SavedProject
{
	private new void Start()
	{
		libraryWindow = BloxelBoardLibrary.instance;
		base.Start();
	}

	public new bool Delete()
	{
		bool flag = ((BloxelBoard)dataModel).Delete();
		if (flag)
		{
			AssetManager.instance.boardPool.Remove(dataModel.ID());
			base.Delete();
		}
		return flag;
	}

	public override void SetData(BloxelProject project)
	{
		base.SetData(project);
		if (dataModel == BloxelBoardLibrary.instance.currentSavedBoard)
		{
			isActive = true;
			outline.enabled = true;
		}
		else
		{
			isActive = false;
			outline.enabled = false;
		}
	}
}
