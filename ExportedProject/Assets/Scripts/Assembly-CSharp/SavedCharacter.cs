using UnityEngine;

public class SavedCharacter : SavedProject
{
	public Rect boundingRect;

	private new void Start()
	{
		libraryWindow = BloxelCharacterLibrary.instance;
		base.Start();
		base.OnSelect += HandleOnSelect;
	}

	private void HandleOnSelect(SavedProject item, bool SFX = true)
	{
		boundingRect = ((BloxelCharacter)dataModel).boundingRect;
	}

	public override void SetData(BloxelProject project)
	{
		base.SetData(project);
		if (dataModel == BloxelCharacterLibrary.instance.currentCharacter)
		{
			isActive = true;
			outline.enabled = true;
		}
		else
		{
			isActive = false;
			outline.enabled = false;
		}
	}

	public new bool Delete()
	{
		bool flag = ((BloxelCharacter)dataModel).Delete();
		if (flag)
		{
			AssetManager.instance.characterPool.Remove(dataModel.ID());
			base.Delete();
		}
		return flag;
	}
}
