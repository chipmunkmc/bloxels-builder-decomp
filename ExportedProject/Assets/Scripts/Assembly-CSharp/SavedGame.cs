using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class SavedGame : SavedProject
{
	public GameObject indicator;

	public Text count;

	public Image coverBackgroundImage;

	private bool shouldRebuild;

	public RawImage heroImage;

	public Image heroBorder;

	public CoverBoard heroCover;

	public CoverSprite coverSprite { get; private set; }

	private new void Awake()
	{
		base.Awake();
		heroBorder.transform.localScale = Vector3.zero;
		uiTextTitle.text = string.Empty;
		heroImage.color = Color.clear;
		heroImage.rectTransform.anchoredPosition = new Vector2(-10f, 10f);
		base.transform.localScale = Vector3.zero;
		heroCover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	private new void Start()
	{
		libraryWindow = BloxelGameLibrary.instance;
		base.Start();
	}

	private void SetTitle()
	{
		uiTextTitle.text = dataModel.title;
	}

	public void SetHeroImage()
	{
		heroBorder.transform.localScale = Vector3.one;
		heroImage.color = Color.white;
		heroImage.rectTransform.anchoredPosition = new Vector2(-10f, -10f);
		heroImage.texture = ((BloxelGame)dataModel).hero.coverBoard.UpdateTexture2D(ref heroCover.colors, ref heroCover.texture, heroCover.style);
	}

	private void Init(BloxelProject data)
	{
		dataModel = data;
		((BloxelGame)dataModel).OnUpdate += SetFrameCount;
		dataModel.OnTitleChange += SetTitle;
	}

	public void SimpleInit(BloxelProject data)
	{
		Init(data);
		SetTitle();
		SetFrameCount();
		SetHeroImage();
	}

	public void AnimatedInit(BloxelProject data)
	{
		base.transform.localScale = Vector3.zero;
		heroBorder.transform.localScale = Vector3.zero;
		base.transform.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			uiTextTitle.DOText(dataModel.title, UIAnimationManager.speedSlowAsCrap);
			heroImage.texture = ((BloxelGame)dataModel).hero.coverBoard.UpdateTexture2D(ref heroCover.colors, ref heroCover.texture, heroCover.style);
			heroImage.DOColor(Color.white, UIAnimationManager.speedMedium);
			heroImage.rectTransform.DOAnchorPos(new Vector2(-10f, -10f), UIAnimationManager.speedSlow).OnComplete(delegate
			{
				heroBorder.transform.DOScale(1f, UIAnimationManager.speedMedium);
			});
			SetFrameCount();
		});
	}

	public new void BuildCover()
	{
		RefreshCoverSprite();
	}

	private new void OnDisable()
	{
		ReleaseCoverSprite();
		if (dataModel != null)
		{
			((BloxelGame)dataModel).OnUpdate -= SetFrameCount;
			dataModel.OnTitleChange -= SetTitle;
		}
	}

	public void SetFrameCount()
	{
		if (dataModel != null && !(count == null))
		{
			count.text = ((BloxelGame)dataModel).levels.Count.ToString();
		}
	}

	public override void SetData(BloxelProject project)
	{
		base.SetData(project);
		if (dataModel == BloxelGameLibrary.instance.currentSavedGame)
		{
			isActive = true;
			outline.enabled = true;
		}
		else
		{
			isActive = false;
			outline.enabled = false;
		}
		SetCoverSprite();
		SimpleInit(dataModel);
	}

	public void RefreshCoverSprite()
	{
		if (coverSprite != null && coverSprite.inUse)
		{
			((BloxelGame)dataModel).SetCoverTexturePixels(coverSprite.sprite, AssetManager.clearColor);
		}
	}

	public void SetCoverSprite()
	{
		ReleaseCoverSprite();
		coverSprite = BloxelLibraryScrollController.instance.GetUnusedCoverSprite();
		((BloxelGame)dataModel).SetCoverTexturePixels(coverSprite.sprite, AssetManager.clearColor);
		coverBackgroundImage.sprite = coverSprite.sprite;
	}

	public void ReleaseCoverSprite()
	{
		if (coverSprite != null)
		{
			coverSprite.inUse = false;
		}
		coverSprite = null;
	}

	public new bool Delete()
	{
		if (dataModel.ID().Equals(AssetManager.poultryPanicID))
		{
			return false;
		}
		bool flag = ((BloxelGame)dataModel).Delete();
		if (flag)
		{
			AssetManager.instance.gamePool.Remove(dataModel.ID());
			base.Delete();
		}
		return flag;
	}
}
