using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SavedLevel : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IEventSystemHandler
{
	public BloxelLevel project;

	public Color boardColor;

	public UIBoard uiBoard;

	public Image outline;

	public RawImage image;

	public BloxelBoard boardWireframe;

	private bool _isActive;

	private bool isDragging;

	private LongPressEventTrigger lpComponent;

	public bool isActive
	{
		get
		{
			return _isActive;
		}
		set
		{
			_isActive = value;
			if (_isActive)
			{
				outline.enabled = true;
				BloxelLevelLibrary.instance.currentLevel = project;
				BloxelLevelLibrary.instance.currentSavedLevel = this;
			}
			else
			{
				outline.enabled = false;
			}
		}
	}

	private void Awake()
	{
		base.gameObject.AddComponent<LongPressEventTrigger>();
		lpComponent = GetComponent<LongPressEventTrigger>();
		isActive = false;
		image = GetComponent<RawImage>();
		image.CrossFadeAlpha(0f, 0f, true);
	}

	private void Start()
	{
		if (project != null && project.coverBoard != null)
		{
			boardWireframe = project.coverBoard;
		}
	}

	private void OnEnable()
	{
		image.CrossFadeAlpha(1f, 0.5f, true);
	}

	private void OnDisable()
	{
		image.CrossFadeAlpha(0f, 0f, true);
	}

	public void BuildCoverBoard()
	{
	}

	public void Delete()
	{
		if (project.Delete())
		{
			BloxelLevelLibrary.instance.savedLevels.Remove(this);
			image.CrossFadeAlpha(0f, 0.2f, true);
			Object.Destroy(base.gameObject, 0.2f);
		}
	}

	public void Select()
	{
		GameBuilderCanvas.instance.gameStart.SetActive(false);
		PixelEditorController.instance.SwitchCanvasMode(CanvasMode.LevelEditor);
		isActive = true;
		foreach (SavedLevel savedLevel in BloxelLevelLibrary.instance.savedLevels)
		{
			if (savedLevel != this)
			{
				savedLevel.isActive = false;
			}
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!isDragging)
		{
			Select();
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		GetComponent<RawImage>().color = new Color(1f, 1f, 1f, 0.5f);
		isDragging = true;
		ExecuteEvents.beginDragHandler(BloxelLibrary.instance.scrollRect, eventData);
	}

	public void OnDrag(PointerEventData eventData)
	{
		ExecuteEvents.dragHandler(BloxelLibrary.instance.scrollRect, eventData);
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		GetComponent<RawImage>().color = new Color(1f, 1f, 1f, 1f);
		isDragging = false;
		ExecuteEvents.endDragHandler(BloxelLibrary.instance.scrollRect, eventData);
	}
}
