using UnityEngine.UI;

public class SavedMegaBoard : SavedProject
{
	public Image coverImage;

	public CoverSprite coverSprite { get; private set; }

	private new void Start()
	{
		libraryWindow = BloxelMegaBoardLibrary.instance;
		base.Start();
		((BloxelMegaBoard)dataModel).OnUpdate += HandleOnUpdate;
	}

	private void HandleOnUpdate()
	{
		BuildCover();
	}

	public new void BuildCover()
	{
		RefreshCoverSprite();
	}

	private new void OnDisable()
	{
		ReleaseCoverSprite();
	}

	public override void SetData(BloxelProject project)
	{
		base.SetData(project);
		if (dataModel == BloxelMegaBoardLibrary.instance.currentMega)
		{
			isActive = true;
			outline.enabled = true;
		}
		else
		{
			isActive = false;
			outline.enabled = false;
		}
		SetCoverSprite();
	}

	public void RefreshCoverSprite()
	{
		if (coverSprite != null && coverSprite.inUse)
		{
			((BloxelMegaBoard)dataModel).SetCoverTexturePixels(coverSprite.sprite, AssetManager.clearColor);
		}
	}

	public void SetCoverSprite()
	{
		ReleaseCoverSprite();
		coverSprite = BloxelLibraryScrollController.instance.GetUnusedCoverSprite();
		((BloxelMegaBoard)dataModel).SetCoverTexturePixels(coverSprite.sprite, AssetManager.clearColor);
		coverImage.sprite = coverSprite.sprite;
	}

	public void ReleaseCoverSprite()
	{
		if (coverSprite != null)
		{
			coverSprite.inUse = false;
		}
		coverSprite = null;
	}

	public new bool Delete()
	{
		bool flag = ((BloxelMegaBoard)dataModel).Delete();
		if (flag)
		{
			AssetManager.instance.megaBoardPool.Remove(dataModel.ID());
			base.Delete();
		}
		return flag;
	}
}
