using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class SavedProject : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleProjectSelect(SavedProject item, bool sfx = true);

	public delegate void HandleDoubleTap(SavedProject item);

	public delegate void HandleDelete(SavedProject item);

	public delegate void HandleBeginDrag(SavedProject item, bool scrolling);

	public delegate void HandleEndDrag(SavedProject item, bool scrolling);

	public delegate void HandleDrag(SavedProject item);

	public BloxelLibraryWindow libraryWindow;

	public BloxelProject dataModel;

	public RawImage image;

	public CoverBoard cover;

	public Image outline;

	public Button uiButtonInfo;

	public Text uiTextTitle;

	public bool isActive;

	private bool isDragging;

	private bool isScrolling;

	private bool initComplete;

	public UnityEngine.Object libraryItemPrefab;

	public event HandleProjectSelect OnSelect;

	public event HandleDoubleTap OnDoubleTap;

	public event HandleDelete OnDelete;

	public event HandleBeginDrag OnDragStart;

	public event HandleEndDrag OnDragEnd;

	public event HandleDrag OnDragging;

	public void Awake()
	{
		if (image == null)
		{
			image = GetComponent<RawImage>();
		}
		outline.enabled = false;
		cover = new CoverBoard(BloxelBoard.baseUIBoardColor);
		OnDelete += RemoveFromScroller;
	}

	public void Start()
	{
		BuildCover();
		if (dataModel != null && dataModel.coverBoard != null)
		{
			dataModel.coverBoard.OnUpdate += HandleOnUpdate;
		}
		uiButtonInfo.onClick.AddListener(InfoPressed);
		initComplete = true;
		SetupEventSubscriptions();
	}

	private void OnDestroy()
	{
		if (dataModel != null && dataModel.coverBoard != null)
		{
			dataModel.coverBoard.OnUpdate -= HandleOnUpdate;
		}
		OnDelete -= RemoveFromScroller;
	}

	public void OnEnable()
	{
		if (initComplete && dataModel != null && dataModel.coverBoard != null)
		{
			dataModel.coverBoard.OnUpdate += HandleOnUpdate;
		}
	}

	public void OnDisable()
	{
		if (dataModel != null && dataModel.coverBoard != null)
		{
			dataModel.coverBoard.OnUpdate -= HandleOnUpdate;
		}
	}

	private void HandleOnUpdate()
	{
		BuildCover();
	}

	public void SetupEventSubscriptions()
	{
		libraryWindow.SetupItem(this);
	}

	public void BuildCover()
	{
		if (dataModel.type != ProjectType.MegaBoard && dataModel.type != ProjectType.Game)
		{
			if (dataModel.coverBoard != null)
			{
				image.texture = dataModel.coverBoard.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
			}
			else
			{
				image.texture = AssetManager.instance.boardSolidBlank;
			}
		}
	}

	public void DeSelect()
	{
		outline.enabled = false;
		isActive = false;
	}

	public void Delete()
	{
		if (this.OnDelete != null)
		{
			this.OnDelete(this);
		}
		image.DOFade(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			BloxelLibraryScrollController.instance.RemoveItem(dataModel);
			image.color = new Color(1f, 1f, 1f, 1f);
		});
	}

	private IEnumerator DelayedLibraryRemoval()
	{
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
	}

	public void GenerateDeleteNotification()
	{
		GameObject gameObject = UnityEngine.Object.Instantiate(Resources.Load("Prefabs/DeleteNotification")) as GameObject;
		gameObject.transform.SetParent(PixelEditorController.instance.primaryCanvas);
		gameObject.transform.localScale = Vector3.one;
		RectTransform component = gameObject.GetComponent<RectTransform>();
		component.anchoredPosition = Vector2.zero;
		component.sizeDelta = Vector2.zero;
	}

	public void Select(bool sfx = true)
	{
		if (sfx)
		{
			SoundManager.PlayOneShot(SoundManager.instance.popA);
		}
		isActive = true;
		outline.enabled = true;
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!isDragging && this.OnSelect != null)
		{
			this.OnSelect(this);
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		isDragging = true;
		if (Mathf.Abs(eventData.delta.x) > Mathf.Abs(eventData.delta.y))
		{
			isScrolling = false;
		}
		else
		{
			isScrolling = true;
			ExecuteEvents.beginDragHandler(BloxelLibraryScrollController.instance.scroller.ScrollRect, eventData);
		}
		if (this.OnDragStart != null)
		{
			this.OnDragStart(this, isScrolling);
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (this.OnDragging != null)
		{
			this.OnDragging(this);
		}
		if (isScrolling)
		{
			ExecuteEvents.dragHandler(BloxelLibraryScrollController.instance.scroller.ScrollRect, eventData);
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (this.OnDragEnd != null)
		{
			this.OnDragEnd(this, isScrolling);
		}
		isDragging = false;
		if (isScrolling)
		{
			ExecuteEvents.endDragHandler(BloxelLibraryScrollController.instance.scroller.ScrollRect, eventData);
		}
	}

	public void InfoPressed()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		GameObject gameObject = UIOverlayCanvas.instance.Popup(libraryItemPrefab);
		UIPopupLibraryItem component = gameObject.GetComponent<UIPopupLibraryItem>();
		component.dataModel = dataModel;
		component.focusedProject = this;
	}

	public virtual void SetData(BloxelProject project)
	{
		dataModel = project;
		if (dataModel == null)
		{
			base.gameObject.SetActive(false);
			return;
		}
		if (!base.gameObject.activeSelf)
		{
			base.gameObject.SetActive(true);
		}
		BuildCover();
	}

	public void RemoveFromScroller(SavedProject project)
	{
		if ((bool)BloxelLibraryScrollController.instance && project.dataModel != null)
		{
			BloxelLibraryScrollController.instance.RemoveItem(project.dataModel);
		}
	}
}
