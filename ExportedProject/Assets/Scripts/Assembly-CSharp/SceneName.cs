public enum SceneName
{
	GameInit = 0,
	Home = 1,
	Viewer = 2,
	PixelEditor_iPad = 3,
	Gameplay = 4,
	Loading = 5,
	WebGLPlayer = 6,
	AccountTransition = 7,
	LanguageSelect = 8
}
