using System.Collections;
using UnityEngine;

public class ScrollPickerManager : MonoBehaviour
{
	public static ScrollPickerManager instance;

	public UIBoardPicker uiBoardPicker;

	public UIAnimationPicker uiAnimationPicker;

	public UIColorPicker uiColorPicker;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		Object.DontDestroyOnLoad(base.gameObject);
		StartCoroutine("WaitForAssetManager");
	}

	private IEnumerator WaitForAssetManager()
	{
		while (!AssetManager.instance.templatesLoaded)
		{
			yield return new WaitForEndOfFrame();
		}
		uiColorPicker.Init();
		uiBoardPicker.Init();
		uiAnimationPicker.Init();
	}
}
