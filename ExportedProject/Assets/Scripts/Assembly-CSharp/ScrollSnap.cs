using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class ScrollSnap : MonoBehaviour
{
	public delegate void HandleSnap(float pos);

	private float[] points;

	[Tooltip("how many screens or pages are there within the content (steps)")]
	public int screens = 1;

	private float stepSize;

	private ScrollRect scroll;

	private bool LerpH;

	private float targetH;

	[Tooltip("Snap horizontally")]
	public bool snapInH = true;

	private bool LerpV;

	private float targetV;

	[Tooltip("Snap vertically")]
	public bool snapInV = true;

	public event HandleSnap OnSnap;

	private void Start()
	{
		scroll = base.gameObject.GetComponent<ScrollRect>();
		scroll.inertia = false;
		if (screens > 0)
		{
			points = new float[screens];
			stepSize = 1f / (float)(screens - 1);
			for (int i = 0; i < screens; i++)
			{
				points[i] = (float)i * stepSize;
			}
		}
		else
		{
			points[0] = 0f;
		}
	}

	private void Update()
	{
		if (LerpH)
		{
			scroll.horizontalNormalizedPosition = Mathf.Lerp(scroll.horizontalNormalizedPosition, targetH, 10f * scroll.elasticity * Time.unscaledDeltaTime);
			if (Mathf.Approximately(scroll.horizontalNormalizedPosition, targetH))
			{
				LerpH = false;
			}
		}
	}

	public void DragEnd()
	{
		if (scroll.horizontal && snapInH)
		{
			targetH = points[FindNearest(scroll.horizontalNormalizedPosition, points)];
			LerpH = true;
		}
		if (this.OnSnap != null)
		{
			this.OnSnap(targetH);
		}
	}

	public void OnDrag()
	{
		LerpH = false;
	}

	private int FindNearest(float f, float[] array)
	{
		float num = float.PositiveInfinity;
		int result = 0;
		for (int i = 0; i < array.Length; i++)
		{
			if (Mathf.Abs(array[i] - f) < num)
			{
				num = Mathf.Abs(array[i] - f);
				result = i;
			}
		}
		return result;
	}
}
