using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;

public class SequentialMenu : MonoBehaviour
{
	public delegate void HandleAnimation();

	[Tooltip("Not Required")]
	public UIButton uiButtonBack;

	[Tooltip("Not Required")]
	public UIButton uiButtonNext;

	[Tooltip("Not Required")]
	public SequentialMenu menuPrev;

	[Tooltip("Not Required")]
	public SequentialMenu menuNext;

	public bool isAnimating;

	public bool canProgress;

	public event HandleAnimation OnAnimationEnd;

	public void Awake()
	{
		isAnimating = false;
		if (uiButtonBack != null)
		{
			uiButtonBack.OnClick += Back;
		}
		if (uiButtonNext != null)
		{
			uiButtonNext.OnClick += Next;
		}
	}

	public void OnDestroy()
	{
		if (uiButtonBack != null)
		{
			uiButtonBack.OnClick -= Back;
		}
		if (uiButtonNext != null)
		{
			uiButtonNext.OnClick -= Next;
		}
	}

	public virtual void Init()
	{
		base.gameObject.SetActive(true);
		base.transform.localScale = Vector3.zero;
	}

	public void UnsubscribeNext()
	{
		if (uiButtonNext != null)
		{
			uiButtonNext.OnClick -= Next;
		}
	}

	public void UnsubscribeBack()
	{
		if (uiButtonBack != null)
		{
			uiButtonBack.OnClick -= Back;
		}
	}

	public virtual void Back()
	{
		isAnimating = true;
		Hide().OnComplete(delegate
		{
			menuPrev.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnComplete(delegate
			{
				isAnimating = false;
				if (this.OnAnimationEnd != null)
				{
					this.OnAnimationEnd();
				}
			});
		});
	}

	public virtual void Next()
	{
		isAnimating = true;
		Hide().OnComplete(delegate
		{
			menuNext.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnComplete(delegate
			{
				isAnimating = false;
				if (this.OnAnimationEnd != null)
				{
					this.OnAnimationEnd();
				}
			});
		});
	}

	public Tweener Show()
	{
		return base.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo);
	}

	public Tweener Hide()
	{
		return base.transform.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo);
	}

	public void ToggleNextButtonState()
	{
		uiButtonNext.interactable = canProgress;
	}
}
