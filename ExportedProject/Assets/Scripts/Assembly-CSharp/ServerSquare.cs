using System;
using System.Collections.Generic;
using UnityEngine;
using VoxelEngine;

[Serializable]
public class ServerSquare
{
	public string _id;

	public string id;

	public string owner;

	public string squareId;

	public string texture;

	public string squareClass;

	public string projectID;

	public string publisher;

	public string createdAt;

	public string moderator;

	public bool moderated;

	public AssociatedSquareData associatedData;

	public string[] downloads;

	public int views;

	public string[] likes;

	public int status;

	public int earnings;

	public ProjectType type;

	public WorldPos2D coordinate;

	public List<string> likeList;

	public ServerSquare(string s)
	{
		JsonUtility.FromJsonOverwrite(s, this);
		type = BloxelProject.GetProjectTypeFromString(squareClass);
		if (squareId == null)
		{
			squareId = id;
		}
		string[] array = squareId.Split('|');
		coordinate = new WorldPos2D(int.Parse(array[0]), int.Parse(array[1]));
		likeList = new List<string>();
		if (likes != null && likes.Length > 0)
		{
			likeList.AddRange(likes);
		}
	}
}
