using UnityEngine;

public class SettingsManager : MonoBehaviour
{
	private string firstTimeBuild = "firstTimeBuild";

	private string firstTimeConfig = "firstTimeConfig";

	private string firstTimeDecorate = "firstTimeDecorate";

	private string firstTimeLevelBG = "firstTimeLevelBG";

	private string firstTimeGameBG = "firstTimeGameBG";

	public bool helpersOn;

	public void ResetFirstTimeWalkThroughs()
	{
	}

	public void TurnOnHelpers()
	{
		helpersOn = true;
	}

	public void TurnOffHelpers()
	{
		helpersOn = false;
	}
}
