using DG.Tweening;
using UnityEngine;

public class ShrinkPowerUp : PowerUp, InventoryItem
{
	private new void Start()
	{
		base.Start();
		powerUpType = PowerUpType.Shrink;
		Bounce();
	}

	private void OnDestroy()
	{
		base.transform.DOKill();
	}

	public override void Collect()
	{
		base.Collect();
		if (PixelPlayerController.instance.isShrunk)
		{
			GameplayController.instance.inventoryManager.DecrementInventoryItem(InventoryData.Type.Shrink);
		}
		Object.Destroy(base.gameObject);
	}

	public void AddItemToInventory()
	{
		GameplayController.instance.inventoryManager.AddToInventory(InventoryData.Type.Shrink);
	}
}
