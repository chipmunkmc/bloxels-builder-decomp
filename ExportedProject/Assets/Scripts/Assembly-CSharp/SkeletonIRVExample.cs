using System.Collections;
using UnityEngine;

[RequireComponent(typeof(InternetReachabilityVerifier))]
public class SkeletonIRVExample : MonoBehaviour
{
	private InternetReachabilityVerifier internetReachabilityVerifier;

	private bool isNetVerified()
	{
		return internetReachabilityVerifier.status == InternetReachabilityVerifier.Status.NetVerified;
	}

	private void forceReverification()
	{
		internetReachabilityVerifier.forceReverification();
	}

	private void netStatusChanged(InternetReachabilityVerifier.Status newStatus)
	{
	}

	private IEnumerator waitForNetwork()
	{
		yield return new WaitForEndOfFrame();
		yield return StartCoroutine(internetReachabilityVerifier.waitForNetVerifiedStatus());
	}

	private void Start()
	{
		internetReachabilityVerifier = GetComponent<InternetReachabilityVerifier>();
		internetReachabilityVerifier.statusChangedDelegate += netStatusChanged;
		StartCoroutine(waitForNetwork());
	}
}
