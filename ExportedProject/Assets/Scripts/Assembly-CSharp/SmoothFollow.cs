using Prime31;
using UnityEngine;

public class SmoothFollow : MonoBehaviour
{
	public Transform target;

	public float smoothDampTime = 0.2f;

	[HideInInspector]
	public new Transform transform;

	public Vector3 cameraOffset;

	public bool useFixedUpdate;

	private CharacterController2D _playerController;

	private Vector3 _smoothDampVelocity;

	private void Awake()
	{
		transform = base.gameObject.transform;
		_playerController = target.GetComponent<CharacterController2D>();
	}

	private void LateUpdate()
	{
		if (!useFixedUpdate)
		{
			updateCameraPosition();
		}
	}

	private void FixedUpdate()
	{
		if (useFixedUpdate)
		{
			updateCameraPosition();
		}
	}

	private void updateCameraPosition()
	{
		if (_playerController == null)
		{
			transform.position = Vector3.SmoothDamp(transform.position, target.position - cameraOffset, ref _smoothDampVelocity, smoothDampTime);
			return;
		}
		if (_playerController.velocity.x > 0f)
		{
			transform.position = Vector3.SmoothDamp(transform.position, target.position - cameraOffset, ref _smoothDampVelocity, smoothDampTime);
			return;
		}
		Vector3 vector = cameraOffset;
		vector.x *= -1f;
		transform.position = Vector3.SmoothDamp(transform.position, target.position - vector, ref _smoothDampVelocity, smoothDampTime);
	}
}
