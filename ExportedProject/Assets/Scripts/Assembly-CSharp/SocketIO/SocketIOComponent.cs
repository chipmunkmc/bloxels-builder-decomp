using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;
using WebSocketSharp;

namespace SocketIO
{
	public class SocketIOComponent : MonoBehaviour
	{
		public string url = "ws://127.0.0.1:4567/socket.io/?EIO=3&transport=websocket";

		public bool autoConnect;

		public int reconnectDelay = 5;

		public float ackExpirationTime = 30f;

		public float pingInterval = 25f;

		public float pingTimeout = 60f;

		private volatile bool connected;

		private volatile bool thPinging;

		private volatile bool thPong;

		private volatile bool wsConnected;

		private Thread socketThread;

		private Thread pingThread;

		private WebSocket ws;

		private Encoder encoder;

		private Decoder decoder;

		private Parser parser;

		private Dictionary<string, List<Action<SocketIOEvent>>> handlers;

		private List<Ack> ackList;

		private int packetId;

		private object eventQueueLock;

		private Queue<SocketIOEvent> eventQueue;

		private object ackQueueLock;

		private Queue<Packet> ackQueue;

		public WebSocket socket
		{
			get
			{
				return ws;
			}
		}

		public string sid { get; set; }

		public bool IsConnected
		{
			get
			{
				return connected;
			}
		}

		public void Awake()
		{
			encoder = new Encoder();
			decoder = new Decoder();
			parser = new Parser();
			handlers = new Dictionary<string, List<Action<SocketIOEvent>>>();
			ackList = new List<Ack>();
			sid = null;
			packetId = 0;
			ws = new WebSocket(url);
			ws.OnOpen += OnOpen;
			ws.OnMessage += OnMessage;
			ws.OnError += OnError;
			ws.OnClose += OnClose;
			wsConnected = false;
			eventQueueLock = new object();
			eventQueue = new Queue<SocketIOEvent>();
			ackQueueLock = new object();
			ackQueue = new Queue<Packet>();
			connected = false;
		}

		public void Start()
		{
			if (autoConnect)
			{
				Connect();
			}
		}

		public void Update()
		{
			lock (eventQueueLock)
			{
				while (eventQueue.Count > 0)
				{
					EmitEvent(eventQueue.Dequeue());
				}
			}
			lock (ackQueueLock)
			{
				while (ackQueue.Count > 0)
				{
					InvokeAck(ackQueue.Dequeue());
				}
			}
			if (wsConnected != ws.IsConnected)
			{
				wsConnected = ws.IsConnected;
				if (wsConnected)
				{
					EmitEvent("connect");
				}
				else
				{
					EmitEvent("disconnect");
				}
			}
			if (ackList.Count != 0 && !(DateTime.Now.Subtract(ackList[0].time).TotalSeconds < (double)ackExpirationTime))
			{
				ackList.RemoveAt(0);
			}
		}

		public void OnDestroy()
		{
			if (socketThread != null)
			{
				socketThread.Abort();
			}
			if (pingThread != null)
			{
				pingThread.Abort();
			}
		}

		public void OnApplicationQuit()
		{
			Close();
		}

		public void SetHeader(string header, string value)
		{
			ws.SetHeader(header, value);
		}

		public void Connect()
		{
			connected = true;
			socketThread = new Thread(RunSocketThread);
			socketThread.Start(ws);
			pingThread = new Thread(RunPingThread);
			pingThread.Start(ws);
		}

		public void Close()
		{
			EmitClose();
			connected = false;
		}

		public void On(string ev, Action<SocketIOEvent> callback)
		{
			if (!handlers.ContainsKey(ev))
			{
				handlers[ev] = new List<Action<SocketIOEvent>>();
			}
			handlers[ev].Add(callback);
		}

		public void Off(string ev, Action<SocketIOEvent> callback)
		{
			if (!handlers.ContainsKey(ev))
			{
				return;
			}
			List<Action<SocketIOEvent>> list = handlers[ev];
			if (list.Contains(callback))
			{
				list.Remove(callback);
				if (list.Count == 0)
				{
					handlers.Remove(ev);
				}
			}
		}

		public void Emit(string ev)
		{
			EmitMessage(-1, string.Format("[\"{0}\"]", ev));
		}

		public void Emit(string ev, string data)
		{
			EmitMessage(-1, string.Format("[\"{0}\",\"{1}\"]", ev, data));
		}

		public void Emit(string ev, Action<JSONObject> action)
		{
			EmitMessage(++packetId, string.Format("[\"{0}\"]", ev));
			ackList.Add(new Ack(packetId, action));
		}

		public void Emit(string ev, JSONObject data)
		{
			EmitMessage(-1, string.Format("[\"{0}\",{1}]", ev, data));
		}

		public void Emit(string ev, JSONObject data, Action<JSONObject> action)
		{
			EmitMessage(++packetId, string.Format("[\"{0}\",{1}]", ev, data));
			ackList.Add(new Ack(packetId, action));
		}

		private void RunSocketThread(object obj)
		{
			WebSocket webSocket = (WebSocket)obj;
			while (connected)
			{
				if (webSocket.IsConnected)
				{
					Thread.Sleep(reconnectDelay);
				}
				else
				{
					webSocket.Connect();
				}
			}
			webSocket.Close();
		}

		private void RunPingThread(object obj)
		{
			WebSocket webSocket = (WebSocket)obj;
			int num = Mathf.FloorToInt(pingTimeout * 1000f);
			int millisecondsTimeout = Mathf.FloorToInt(pingInterval * 1000f);
			while (connected)
			{
				if (!wsConnected)
				{
					Thread.Sleep(reconnectDelay);
					continue;
				}
				thPinging = true;
				thPong = false;
				EmitPacket(new Packet(EnginePacketType.PING));
				DateTime now = DateTime.Now;
				while (webSocket.IsConnected && thPinging && DateTime.Now.Subtract(now).TotalSeconds < (double)num)
				{
					Thread.Sleep(200);
				}
				if (!thPong)
				{
					webSocket.Close();
				}
				Thread.Sleep(millisecondsTimeout);
			}
		}

		private void EmitMessage(int id, string raw)
		{
			EmitPacket(new Packet(EnginePacketType.MESSAGE, SocketPacketType.EVENT, 0, "/", id, new JSONObject(raw)));
		}

		private void EmitClose()
		{
			EmitPacket(new Packet(EnginePacketType.MESSAGE, SocketPacketType.DISCONNECT, 0, "/", -1, new JSONObject(string.Empty)));
			EmitPacket(new Packet(EnginePacketType.CLOSE));
		}

		private void EmitPacket(Packet packet)
		{
			try
			{
				if (ws != null)
				{
					ws.Send(encoder.Encode(packet));
				}
			}
			catch (SocketIOException)
			{
			}
		}

		private void OnOpen(object sender, EventArgs e)
		{
			EmitEvent("open");
		}

		private void OnMessage(object sender, MessageEventArgs e)
		{
			Packet packet = decoder.Decode(e);
			switch (packet.enginePacketType)
			{
			case EnginePacketType.OPEN:
				HandleOpen(packet);
				break;
			case EnginePacketType.CLOSE:
				EmitEvent("close");
				break;
			case EnginePacketType.PING:
				HandlePing();
				break;
			case EnginePacketType.PONG:
				HandlePong();
				break;
			case EnginePacketType.MESSAGE:
				HandleMessage(packet);
				break;
			}
		}

		private void HandleOpen(Packet packet)
		{
			sid = packet.json["sid"].str;
			EmitEvent("open");
		}

		private void HandlePing()
		{
			EmitPacket(new Packet(EnginePacketType.PONG));
		}

		private void HandlePong()
		{
			thPong = true;
			thPinging = false;
		}

		private void HandleMessage(Packet packet)
		{
			if (packet.json == null)
			{
				return;
			}
			if (packet.socketPacketType == SocketPacketType.ACK)
			{
				for (int i = 0; i < ackList.Count; i++)
				{
					if (ackList[i].packetId == packet.id)
					{
						lock (ackQueueLock)
						{
							ackQueue.Enqueue(packet);
							return;
						}
					}
				}
			}
			if (packet.socketPacketType != SocketPacketType.EVENT)
			{
				return;
			}
			SocketIOEvent item = parser.Parse(packet.json);
			lock (eventQueueLock)
			{
				eventQueue.Enqueue(item);
			}
		}

		private void OnError(object sender, ErrorEventArgs e)
		{
			EmitEvent(new SocketIOEvent("error", JSONObject.CreateStringObject(e.Message)));
		}

		private void OnClose(object sender, CloseEventArgs e)
		{
			EmitEvent("close");
		}

		private void EmitEvent(string type)
		{
			EmitEvent(new SocketIOEvent(type));
		}

		private void EmitEvent(SocketIOEvent ev)
		{
			if (!handlers.ContainsKey(ev.name))
			{
				return;
			}
			foreach (Action<SocketIOEvent> item in handlers[ev.name])
			{
				try
				{
					item(ev);
				}
				catch (Exception)
				{
				}
			}
		}

		private void InvokeAck(Packet packet)
		{
			for (int i = 0; i < ackList.Count; i++)
			{
				if (ackList[i].packetId == packet.id)
				{
					Ack ack = ackList[i];
					ackList.RemoveAt(i);
					ack.Invoke(packet.json);
					break;
				}
			}
		}
	}
}
