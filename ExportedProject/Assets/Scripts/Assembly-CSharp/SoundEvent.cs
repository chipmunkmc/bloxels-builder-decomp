public enum SoundEvent
{
	None = 0,
	ButtonsUniversalA = 1,
	ButtonsUniversalB = 2,
	ButtonsUniversalC = 3,
	PopUniversalA = 4,
	PopUniversalB = 5,
	PopUniversalC = 6,
	SliderPressed = 7,
	EditorSwitchSubMode = 8,
	DrawerSlide = 9,
	Erase = 10,
	Trash = 11,
	EditorPlay = 12,
	ColorSwatchChanged = 13
}
