using UnityEngine;
using UnityEngine.EventSystems;

public class SoundEvents : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
{
	public SoundEvent soundPointerEnter;

	public SoundEvent soundPointerExit;

	public SoundEvent soundPointerClick;

	public SoundEvent soundPointerUp;

	public SoundEvent soundPointerDown;

	public void OnPointerEnter(PointerEventData pEvent)
	{
		SoundManager.PlayEventSound(soundPointerEnter);
	}

	public void OnPointerExit(PointerEventData pEvent)
	{
		SoundManager.PlayEventSound(soundPointerExit);
	}

	public void OnPointerClick(PointerEventData pEvent)
	{
		SoundManager.PlayEventSound(soundPointerClick);
	}

	public void OnPointerUp(PointerEventData pEvent)
	{
		SoundManager.PlayEventSound(soundPointerUp);
	}

	public void OnPointerDown(PointerEventData pEvent)
	{
		SoundManager.PlayEventSound(soundPointerDown);
	}
}
