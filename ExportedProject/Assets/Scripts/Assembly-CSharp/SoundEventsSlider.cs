using UnityEngine;
using UnityEngine.UI;

public class SoundEventsSlider : MonoBehaviour
{
	public AudioClip sliderSound;

	public int sliderDivisions = 20;

	private Slider sliderComponent;

	private float previousValue;

	private float accumulatedValue;

	private void Start()
	{
		sliderComponent = GetComponent<Slider>();
		sliderComponent.onValueChanged.AddListener(OnValueChanged);
	}

	private void OnValueChanged(float value)
	{
		float num = (sliderComponent.maxValue - sliderComponent.minValue) / (float)sliderDivisions;
		accumulatedValue += Mathf.Abs(value - previousValue);
		previousValue = value;
		if (accumulatedValue >= num && sliderSound != null)
		{
			accumulatedValue = 0f;
			SoundManager.PlayOneShot(sliderSound);
		}
	}
}
