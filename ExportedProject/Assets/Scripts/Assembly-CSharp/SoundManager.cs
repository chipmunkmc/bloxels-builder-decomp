using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class SoundManager : MonoBehaviour
{
	public static SoundManager instance;

	[Header("Audio Sources")]
	public AudioSource sfxPlayer;

	public AudioSource sfxPlayerBackup1;

	public AudioSource sfxPlayerBackup2;

	public AudioSource progressBarPlayer;

	public AudioSource musicPlayer;

	public AudioSource loopingSFXPlayer;

	[Header("Events")]
	public SoundClip eventButtonUniversalA;

	public SoundClip eventButtonUniversalB;

	public SoundClip eventButtonUniversalC;

	public SoundClip eventPopUniversalA;

	public SoundClip eventPopUniversalB;

	public SoundClip eventPopUniversalC;

	public SoundClip eventSliderPressed;

	public SoundClip eventEditorSwitchSubMode;

	public SoundClip eventDrawerSlide;

	public SoundClip eventErase;

	public SoundClip eventTrash;

	public SoundClip eventEditorPlay;

	public SoundClip eventColorSwatchChanged;

	[Header("SFX - UI")]
	public AudioClip boardFound;

	public AudioClip buyA;

	public AudioClip cameraOpen;

	public AudioClip cameraSuccess;

	public AudioClip cameraTrack;

	public AudioClip cameraShutter;

	public AudioClip cancelA;

	public AudioClip confirmA;

	public AudioClip confirmB;

	public AudioClip confirmC;

	public AudioClip drawOnBoard;

	public AudioClip editorPlay;

	public AudioClip eraser;

	public AudioClip flip;

	public AudioClip paste;

	public AudioClip pause;

	public AudioClip popA;

	public AudioClip popB;

	public AudioClip popC;

	public AudioClip publish;

	public AudioClip slideA;

	public AudioClip sweepA;

	public AudioClip sweepB;

	public AudioClip iViewShow;

	public AudioClip iViewHide;

	public AudioClip trash;

	public AudioClip bell0;

	public AudioClip bell1;

	public AudioClip bell2;

	public AudioClip bell3;

	public AudioClip bell4;

	public AudioClip bell5;

	public AudioClip bell6;

	public AudioClip[] captureTones;

	[Header("SFX - Challenges")]
	public AudioClip boardRefresh;

	public AudioClip challengeUnlock;

	public AudioClip boardWobble;

	public AudioClip challengeComplete;

	public AudioClip coinIncrement;

	public AudioClip coinReward;

	public AudioClip gameboardUnlock;

	public AudioClip menuPopup;

	public AudioClip verifySuccess;

	public AudioClip megaTileUnlock;

	public AudioClip whip;

	public AudioClip magicAppear;

	public AudioClip appearance;

	[Header("SFX - Gameplay")]
	public AudioClip destructSFX;

	public AudioClip enemyHitSFX;

	public AudioClip enemyProjectile;

	public AudioClip enemyDeath;

	public AudioClip npcBubbleSFX;

	public AudioClip laser;

	public AudioClip playerJump;

	public AudioClip playerDoubleJump;

	public AudioClip playerLand;

	public AudioClip playerHit;

	public AudioClip playerHit2;

	public AudioClip powerUpSFX;

	public AudioClip powerUpHealthSFX;

	public AudioClip powerUpShrinkSFX;

	public AudioClip powerUpUnshrinkSFX;

	public AudioClip endInvincibilitySFX;

	public AudioClip coinSFX;

	public AudioClip bombThrow;

	public AudioClip bombSFX;

	public AudioClip jetpackThrustSFX;

	public AudioClip jetpackIdleSFX;

	public AudioClip gameWin;

	public AudioClip gameLose;

	public AudioClip playerDeath;

	[Header("Music - UI")]
	public AudioClip editorMusic;

	public AudioClip captureMusic;

	public AudioClip captureShutter;

	public AudioClip canvasMusic;

	[Header("Music - Gameplay")]
	public AudioClip storyModeMusic;

	public AudioClip choice0;

	public AudioClip choice1;

	public AudioClip choice2;

	public AudioClip choice3;

	public AudioClip choice4;

	public AudioClip choice5;

	public Dictionary<MusicChoice, AudioClip> musicChoiceLookup;

	public Dictionary<SFX, AudioClip> sfx;

	[Header("Settings")]
	public AudioMixerGroup sfxGroup;

	public AudioMixerGroup musicGroup;

	public bool shouldPlayEditorMusic = true;

	private float _musicVolume;

	private float _sfxVolume;

	private bool initComplete;

	public static float defaultSFXVolume = 0.8f;

	public static float defaultMusicVolume = 0.6f;

	public float invincibilityPitchAdjust = 1.25f;

	public AudioClip currentActiveMusic;

	public float musicVolume
	{
		get
		{
			return _musicVolume;
		}
		set
		{
			_musicVolume = value;
			musicPlayer.volume = _musicVolume;
		}
	}

	public float sfxVolume
	{
		get
		{
			return _sfxVolume;
		}
		set
		{
			_sfxVolume = value;
			sfxPlayer.volume = _sfxVolume;
			sfxPlayerBackup1.volume = _sfxVolume;
			sfxPlayerBackup2.volume = _sfxVolume;
		}
	}

	private void Awake()
	{
		if (!initComplete)
		{
			if (instance == null)
			{
				instance = this;
			}
			else if (instance != this)
			{
				Object.Destroy(base.gameObject);
				return;
			}
			Object.DontDestroyOnLoad(base.gameObject);
			musicChoiceLookup = new Dictionary<MusicChoice, AudioClip>
			{
				{
					MusicChoice.Choice0,
					choice0
				},
				{
					MusicChoice.Choice1,
					choice1
				},
				{
					MusicChoice.Choice2,
					choice2
				},
				{
					MusicChoice.Choice3,
					choice3
				},
				{
					MusicChoice.Choice4,
					choice4
				},
				{
					MusicChoice.Choice5,
					choice5
				}
			};
			initComplete = true;
			musicVolume = defaultMusicVolume;
			sfxVolume = defaultSFXVolume;
		}
	}

	public void PlayLoopingSound(AudioClip _clip)
	{
		loopingSFXPlayer.clip = _clip;
		loopingSFXPlayer.Play();
	}

	public void StopLoopingSound()
	{
		if (loopingSFXPlayer.isPlaying)
		{
			loopingSFXPlayer.Stop();
		}
	}

	public void PlaySound(AudioClip _clip)
	{
		if (!sfxPlayer.isPlaying)
		{
			sfxPlayer.clip = _clip;
			sfxPlayer.Play();
		}
		else if (!sfxPlayerBackup1.isPlaying)
		{
			sfxPlayerBackup1.clip = _clip;
			sfxPlayerBackup1.Play();
		}
		else
		{
			sfxPlayerBackup2.clip = _clip;
			sfxPlayerBackup2.Play();
		}
	}

	public void PlayDelayedSound(float delay, AudioClip clip)
	{
		StartCoroutine(DelayedSoundRoutine(delay, clip));
	}

	private IEnumerator DelayedSoundRoutine(float delay, AudioClip clip)
	{
		yield return new WaitForSeconds(delay);
		PlaySound(clip);
	}

	public void PlayProgressSound(AudioClip _clip)
	{
		progressBarPlayer.clip = _clip;
		progressBarPlayer.Play();
	}

	public void PlayMusic(AudioClip _clip)
	{
		if (!(_clip == editorMusic) || shouldPlayEditorMusic)
		{
			currentActiveMusic = _clip;
			musicPlayer.clip = _clip;
			musicPlayer.Play();
		}
	}

	public void AdjustMusicPitch(float pitch)
	{
		musicPlayer.pitch = pitch;
	}

	public void StopMusic()
	{
		if (musicPlayer.isPlaying)
		{
			musicPlayer.Stop();
		}
	}

	public static void PlayOneShot(AudioClip _clip)
	{
		if (!(instance == null))
		{
			instance.sfxPlayer.PlayOneShot(_clip);
		}
	}

	public static void PlaySoundClip(SoundClip soundClip)
	{
		if (!(instance == null))
		{
			AudioClip audioClip = null;
			switch (soundClip)
			{
			case SoundClip.boardFound:
				audioClip = instance.boardFound;
				break;
			case SoundClip.buyA:
				audioClip = instance.buyA;
				break;
			case SoundClip.cameraOpen:
				audioClip = instance.cameraOpen;
				break;
			case SoundClip.cameraSuccess:
				audioClip = instance.cameraSuccess;
				break;
			case SoundClip.cameraTrack:
				audioClip = instance.cameraTrack;
				break;
			case SoundClip.cancelA:
				audioClip = instance.cancelA;
				break;
			case SoundClip.confirmA:
				audioClip = instance.confirmA;
				break;
			case SoundClip.confirmB:
				audioClip = instance.confirmB;
				break;
			case SoundClip.confirmC:
				audioClip = instance.confirmC;
				break;
			case SoundClip.drawOnBoard:
				audioClip = instance.drawOnBoard;
				break;
			case SoundClip.editorPlay:
				audioClip = instance.editorPlay;
				break;
			case SoundClip.eraser:
				audioClip = instance.eraser;
				break;
			case SoundClip.flip:
				audioClip = instance.flip;
				break;
			case SoundClip.paste:
				audioClip = instance.paste;
				break;
			case SoundClip.pause:
				audioClip = instance.pause;
				break;
			case SoundClip.popA:
				audioClip = instance.popA;
				break;
			case SoundClip.popB:
				audioClip = instance.popB;
				break;
			case SoundClip.popC:
				audioClip = instance.popC;
				break;
			case SoundClip.publish:
				audioClip = instance.publish;
				break;
			case SoundClip.slideA:
				audioClip = instance.slideA;
				break;
			case SoundClip.sweepA:
				audioClip = instance.sweepA;
				break;
			case SoundClip.sweepB:
				audioClip = instance.sweepB;
				break;
			case SoundClip.trash:
				audioClip = instance.trash;
				break;
			}
			if (audioClip != null)
			{
				PlayOneShot(audioClip);
			}
		}
	}

	public static void PlayEventSound(SoundEvent soundEvent)
	{
		PlaySoundClip(GetSoundClipForEvent(soundEvent));
	}

	private static SoundClip GetSoundClipForEvent(SoundEvent soundEvent)
	{
		SoundClip result = SoundClip.none;
		switch (soundEvent)
		{
		case SoundEvent.ButtonsUniversalA:
			result = instance.eventButtonUniversalA;
			break;
		case SoundEvent.ButtonsUniversalB:
			result = instance.eventButtonUniversalB;
			break;
		case SoundEvent.ButtonsUniversalC:
			result = instance.eventButtonUniversalC;
			break;
		case SoundEvent.PopUniversalA:
			result = instance.eventPopUniversalA;
			break;
		case SoundEvent.PopUniversalB:
			result = instance.eventPopUniversalB;
			break;
		case SoundEvent.PopUniversalC:
			result = instance.eventPopUniversalC;
			break;
		case SoundEvent.SliderPressed:
			result = instance.eventSliderPressed;
			break;
		case SoundEvent.EditorSwitchSubMode:
			result = instance.eventEditorSwitchSubMode;
			break;
		case SoundEvent.DrawerSlide:
			result = instance.eventDrawerSlide;
			break;
		case SoundEvent.Erase:
			result = instance.eventErase;
			break;
		case SoundEvent.Trash:
			result = instance.eventTrash;
			break;
		case SoundEvent.EditorPlay:
			result = instance.eventEditorPlay;
			break;
		case SoundEvent.ColorSwatchChanged:
			result = instance.eventColorSwatchChanged;
			break;
		}
		return result;
	}
}
