using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public sealed class SpeakBubble : PoolableComponent
{
	public BloxelTile tile;

	public Text uiTextMessage;

	private string npcText;

	private string _textDefaultKey = "home6";

	private string _textDefault = "Hi There!";

	private string _textInModerationKey = "gameplay32";

	private string _textInModeration = "I'm still reviewing this! Check back soon. -- The PixelKing";

	public ScrollRect scrollRect;

	private Vector3 startingScale = new Vector3(0.2f, 0f, 1.1f);

	private Vector3 targetScale = new Vector3(1f, 1f, 1.1f);

	public override void EarlyAwake()
	{
		if (!(selfTransform != null))
		{
			selfTransform = base.transform;
			scrollRect = GetComponentInChildren<ScrollRect>();
			uiTextMessage.text = string.Empty;
		}
	}

	public override void PrepareSpawn()
	{
		uiTextMessage.text = string.Empty;
		spawned = true;
		base.gameObject.SetActive(true);
		if (SoundManager.instance == null)
		{
			return;
		}
		SoundManager.instance.PlaySound(SoundManager.instance.npcBubbleSFX);
		base.transform.DOScaleY(1f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			base.transform.DOScaleX(1f, UIAnimationManager.speedFast).OnStart(delegate
			{
				bool flag = tile.bloxelLevel.npcTextBlocks.TryGetValue(tile.tileInfo.locationInLevel, out npcText);
				if (GameplayController.instance.iWallGame && !GameplayController.instance.currentGame.ugcApproved)
				{
					uiTextMessage.text = LocalizationManager.getInstance().getLocalizedText(_textInModerationKey, _textInModeration);
				}
				else if (flag)
				{
					uiTextMessage.text = npcText;
				}
				else
				{
					uiTextMessage.text = LocalizationManager.getInstance().getLocalizedText(_textDefaultKey, _textDefault);
				}
				scrollRect.content.sizeDelta = new Vector2(scrollRect.content.sizeDelta.x, uiTextMessage.preferredHeight + 12f);
				scrollRect.verticalNormalizedPosition = 1f;
			});
		});
	}

	public override void PrepareDespawn()
	{
		BloxelNPCTile.speakBubble = null;
		tile = null;
		base.transform.DOScaleY(0f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			base.transform.DOScaleX(0.2f, UIAnimationManager.speedFast);
		}).OnComplete(delegate
		{
			spawned = false;
			base.gameObject.SetActive(false);
			selfTransform.SetParent(SpeakBubblePool.instance.selfTransform);
			SpeakBubblePool.instance.pool.Push(this);
		});
	}

	public override void Despawn()
	{
		PrepareDespawn();
	}
}
