using UnityEngine;
using UnityEngine.UI;

public class SpecialEffects : MonoBehaviour
{
	public static SpecialEffects instance;

	public ParticleSystem smokeEffect;

	public ParticleSystem fireEffect;

	public ParticleSystem coinPoof;

	public ParticleSystem barrelPoof;

	public ParticleSystem barrelExplode;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	public void TextPoofAtLocation(Transform parent, Vector3 position, string text)
	{
		GameObject original = Resources.Load("Prefabs/SpecialEffects/TextPoofAtLocation") as GameObject;
		GameObject gameObject = Object.Instantiate(original);
		gameObject.transform.SetParent(parent);
		gameObject.transform.localScale = new Vector3(0.02f, 0.02f, 0.02f);
		gameObject.transform.position = position;
		gameObject.GetComponentInChildren<Text>().text = text;
	}

	public ParticleSystem Create(ParticleSystem prefab, Transform parent, Vector3 position, int siblingIndex = -1, bool play = false)
	{
		ParticleSystem particleSystem = Object.Instantiate(prefab);
		particleSystem.transform.SetParent(parent);
		particleSystem.transform.position = position;
		particleSystem.transform.localScale = Vector3.one;
		if (siblingIndex > -1)
		{
			particleSystem.transform.SetSiblingIndex(siblingIndex);
		}
		else
		{
			particleSystem.transform.SetAsLastSibling();
		}
		ParticleSystem component = particleSystem.GetComponent<ParticleSystem>();
		component.GetComponent<Renderer>().sortingOrder = 2;
		Object.Destroy(particleSystem.gameObject, particleSystem.startLifetime);
		if (play && !particleSystem.playOnAwake)
		{
			particleSystem.Play();
		}
		return particleSystem;
	}
}
