using UnityEngine;

public class SpinningObject : MonoBehaviour
{
	public Light cubeLight;

	private Quaternion localRotation;

	private float xRot;

	private float yRot;

	private float zRot;

	public float ySpeed;

	private void Start()
	{
		localRotation = base.transform.localRotation;
	}

	private void Update()
	{
		rotate();
	}

	private void rotate()
	{
		base.transform.localRotation = Quaternion.Euler(xRot, yRot, zRot);
		yRot = (yRot + ySpeed) % 360f;
	}

	private void OnDisable()
	{
		CancelInvoke("rotate");
	}
}
