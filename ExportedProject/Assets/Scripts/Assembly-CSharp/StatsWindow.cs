using UnityEngine;

public class StatsWindow : MonoBehaviour
{
	private int frameCount;

	private float dt;

	private float fps;

	private float updateRate = 4f;

	private void Update()
	{
		frameCount++;
		dt += Time.deltaTime;
		if (dt > 1f / updateRate)
		{
			fps = (float)frameCount / dt;
			frameCount = 0;
			dt -= 1f / updateRate;
		}
	}
}
