public struct StepText
{
	public string title;

	public string body;

	public StepText(string _title, string _body)
	{
		title = _title;
		body = _body;
	}
}
