using System;
using System.IO;
using System.IO.Compression;

public class StreamHelper
{
	public static void Compress(DirectoryInfo directorySelected, string directoryPath)
	{
		FileInfo[] files = directorySelected.GetFiles();
		foreach (FileInfo fileInfo in files)
		{
			using (FileStream original = fileInfo.OpenRead())
			{
				if (!(((File.GetAttributes(fileInfo.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden) & (fileInfo.Extension != ".gz")))
				{
					continue;
				}
				using (FileStream compressedStream = File.Create(fileInfo.FullName + ".gz"))
				{
					using (GZipStream destination = new GZipStream(compressedStream, CompressionMode.Compress))
					{
						CopyStreamTo(original, destination);
					}
				}
				FileInfo fileInfo2 = new FileInfo(directoryPath + "\\" + fileInfo.Name + ".gz");
				Console.WriteLine("Compressed {0} from {1} to {2} bytes.", fileInfo.Name, fileInfo.Length.ToString(), fileInfo2.Length.ToString());
			}
		}
	}

	public static void CopyStreamTo(Stream original, Stream destination)
	{
		if (destination == null)
		{
			throw new ArgumentNullException("destination");
		}
		if (!original.CanRead && !original.CanWrite)
		{
			throw new ObjectDisposedException("ObjectDisposedException");
		}
		if (!destination.CanRead && !destination.CanWrite)
		{
			throw new ObjectDisposedException("ObjectDisposedException");
		}
		if (!original.CanRead)
		{
			throw new NotSupportedException("NotSupportedException source");
		}
		if (!destination.CanWrite)
		{
			throw new NotSupportedException("NotSupportedException destination");
		}
		byte[] array = new byte[4096];
		int count;
		while ((count = original.Read(array, 0, array.Length)) != 0)
		{
			destination.Write(array, 0, count);
		}
	}
}
