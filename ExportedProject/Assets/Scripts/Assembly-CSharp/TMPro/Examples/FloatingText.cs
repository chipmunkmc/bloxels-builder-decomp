using System.Collections;
using UnityEngine;
using UnityEngine.AI;

namespace TMPro.Examples
{
	public class FloatingText : MonoBehaviour
	{
		private GameObject m_floatingText;

		private TextMeshPro m_textMeshPro;

		private TextMesh m_textMesh;

		private NavMeshAgent m_navAgent;

		private Transform m_transform;

		private Transform m_floatingText_Transform;

		private Transform m_cameraTransform;

		private Vector3 lastPOS = Vector3.zero;

		private Quaternion lastRotation = Quaternion.identity;

		public int SpawnType;

		private void Awake()
		{
			m_transform = base.transform;
			m_navAgent = GetComponent<NavMeshAgent>();
			m_floatingText = new GameObject(m_transform.name + " floating text");
			m_floatingText_Transform = m_floatingText.transform;
			m_floatingText_Transform.parent = m_transform;
			m_floatingText_Transform.localPosition = new Vector3(0f, 1f, 0f);
			m_cameraTransform = Camera.main.transform;
		}

		private void Start()
		{
			if (SpawnType == 0)
			{
				m_textMeshPro = m_floatingText.AddComponent<TextMeshPro>();
				m_textMeshPro.color = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), byte.MaxValue);
				m_textMeshPro.fontSize = 16f;
				m_textMeshPro.text = string.Empty;
				StartCoroutine(DisplayTextMeshProFloatingText());
			}
			else
			{
				m_textMesh = m_floatingText.AddComponent<TextMesh>();
				m_textMesh.font = Resources.Load("Fonts/ARIAL", typeof(Font)) as Font;
				m_textMesh.GetComponent<Renderer>().sharedMaterial = m_textMesh.font.material;
				m_textMesh.color = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), byte.MaxValue);
				m_textMesh.anchor = TextAnchor.LowerCenter;
				m_textMesh.fontSize = 16;
				StartCoroutine(DisplayTextMeshFloatingText());
			}
		}

		public IEnumerator DisplayTextMeshProFloatingText()
		{
			while (true)
			{
				m_textMeshPro.text = m_navAgent.remainingDistance.ToString("f2");
				if (!lastPOS.Compare(m_cameraTransform.position, 1000) || !lastRotation.Compare(m_cameraTransform.rotation, 1000))
				{
					lastPOS = m_cameraTransform.position;
					lastRotation = m_cameraTransform.rotation;
					m_floatingText_Transform.rotation = lastRotation;
				}
				yield return new WaitForEndOfFrame();
			}
		}

		public IEnumerator DisplayTextMeshFloatingText()
		{
			while (true)
			{
				m_textMesh.text = m_navAgent.remainingDistance.ToString("f2");
				if (!lastPOS.Compare(m_cameraTransform.position, 1000) || !lastRotation.Compare(m_cameraTransform.rotation, 1000))
				{
					lastPOS = m_cameraTransform.position;
					lastRotation = m_cameraTransform.rotation;
					m_floatingText_Transform.rotation = lastRotation;
				}
				yield return new WaitForEndOfFrame();
			}
		}
	}
}
