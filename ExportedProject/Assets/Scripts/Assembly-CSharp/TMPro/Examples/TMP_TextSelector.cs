using UnityEngine;
using UnityEngine.EventSystems;

namespace TMPro.Examples
{
	public class TMP_TextSelector : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler, IPointerUpHandler, IEventSystemHandler
	{
		public RectTransform TextPopup_Prefab_01;

		private RectTransform m_TextPopup_RectTransform;

		private TextMeshProUGUI m_TextPopup_TMPComponent;

		private const string k_LinkText = "You have selected link <#ffff00>";

		private const string k_WordText = "Word Index: <#ffff00>";

		private TextMeshProUGUI m_TextMeshPro;

		private Canvas m_Canvas;

		private Camera m_Camera;

		private bool isHoveringObject;

		private int m_selectedWord = -1;

		private int m_selectedLink = -1;

		private int m_lastIndex = -1;

		private void Awake()
		{
			m_TextMeshPro = base.gameObject.GetComponent<TextMeshProUGUI>();
			m_Canvas = base.gameObject.GetComponentInParent<Canvas>();
			if (m_Canvas.renderMode == RenderMode.ScreenSpaceOverlay)
			{
				m_Camera = null;
			}
			else
			{
				m_Camera = m_Canvas.worldCamera;
			}
			m_TextPopup_RectTransform = Object.Instantiate(TextPopup_Prefab_01);
			m_TextPopup_RectTransform.SetParent(m_Canvas.transform, false);
			m_TextPopup_TMPComponent = m_TextPopup_RectTransform.GetComponentInChildren<TextMeshProUGUI>();
			m_TextPopup_RectTransform.gameObject.SetActive(false);
		}

		private void LateUpdate()
		{
			if (!isHoveringObject)
			{
				return;
			}
			int num = TMP_TextUtilities.FindIntersectingCharacter(m_TextMeshPro, Input.mousePosition, m_Camera, true);
			if (num != -1 && num != m_lastIndex && (Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift)))
			{
				m_lastIndex = num;
				Color32 color = new Color32((byte)Random.Range(0, 255), (byte)Random.Range(0, 255), (byte)Random.Range(0, 255), byte.MaxValue);
				int materialReferenceIndex = m_TextMeshPro.textInfo.characterInfo[num].materialReferenceIndex;
				int vertexIndex = m_TextMeshPro.textInfo.characterInfo[num].vertexIndex;
				Color32[] colors = m_TextMeshPro.textInfo.meshInfo[materialReferenceIndex].colors32;
				colors[vertexIndex] = color;
				colors[vertexIndex + 1] = color;
				colors[vertexIndex + 2] = color;
				colors[vertexIndex + 3] = color;
				m_TextMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
			}
			int num2 = TMP_TextUtilities.FindIntersectingWord(m_TextMeshPro, Input.mousePosition, m_Camera);
			if (m_TextPopup_RectTransform != null && m_selectedWord != -1 && (num2 == -1 || num2 != m_selectedWord))
			{
				TMP_WordInfo tMP_WordInfo = m_TextMeshPro.textInfo.wordInfo[m_selectedWord];
				for (int i = 0; i < tMP_WordInfo.characterCount; i++)
				{
					int num3 = tMP_WordInfo.firstCharacterIndex + i;
					int materialReferenceIndex2 = m_TextMeshPro.textInfo.characterInfo[num3].materialReferenceIndex;
					int vertexIndex2 = m_TextMeshPro.textInfo.characterInfo[num3].vertexIndex;
					Color32[] colors2 = m_TextMeshPro.textInfo.meshInfo[materialReferenceIndex2].colors32;
					Color32 color2 = colors2[vertexIndex2].Tint(1.33333f);
					colors2[vertexIndex2] = color2;
					colors2[vertexIndex2 + 1] = color2;
					colors2[vertexIndex2 + 2] = color2;
					colors2[vertexIndex2 + 3] = color2;
				}
				m_TextMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
				m_selectedWord = -1;
			}
			if (num2 != -1 && num2 != m_selectedWord && !Input.GetKey(KeyCode.LeftShift) && !Input.GetKey(KeyCode.RightShift))
			{
				m_selectedWord = num2;
				TMP_WordInfo tMP_WordInfo2 = m_TextMeshPro.textInfo.wordInfo[num2];
				for (int j = 0; j < tMP_WordInfo2.characterCount; j++)
				{
					int num4 = tMP_WordInfo2.firstCharacterIndex + j;
					int materialReferenceIndex3 = m_TextMeshPro.textInfo.characterInfo[num4].materialReferenceIndex;
					int vertexIndex3 = m_TextMeshPro.textInfo.characterInfo[num4].vertexIndex;
					Color32[] colors3 = m_TextMeshPro.textInfo.meshInfo[materialReferenceIndex3].colors32;
					Color32 color3 = colors3[vertexIndex3].Tint(0.75f);
					colors3[vertexIndex3] = color3;
					colors3[vertexIndex3 + 1] = color3;
					colors3[vertexIndex3 + 2] = color3;
					colors3[vertexIndex3 + 3] = color3;
				}
				m_TextMeshPro.UpdateVertexData(TMP_VertexDataUpdateFlags.All);
			}
			int num5 = TMP_TextUtilities.FindIntersectingLink(m_TextMeshPro, Input.mousePosition, m_Camera);
			if ((num5 == -1 && m_selectedLink != -1) || num5 != m_selectedLink)
			{
				m_TextPopup_RectTransform.gameObject.SetActive(false);
				m_selectedLink = -1;
			}
			if (num5 != -1 && num5 != m_selectedLink)
			{
				m_selectedLink = num5;
				TMP_LinkInfo tMP_LinkInfo = m_TextMeshPro.textInfo.linkInfo[num5];
				Vector3 worldPoint = Vector3.zero;
				RectTransformUtility.ScreenPointToWorldPointInRectangle(m_TextMeshPro.rectTransform, Input.mousePosition, m_Camera, out worldPoint);
				switch (tMP_LinkInfo.GetLinkID())
				{
				case "id_01":
					m_TextPopup_RectTransform.position = worldPoint;
					m_TextPopup_RectTransform.gameObject.SetActive(true);
					m_TextPopup_TMPComponent.text = "You have selected link <#ffff00> ID 01";
					break;
				case "id_02":
					m_TextPopup_RectTransform.position = worldPoint;
					m_TextPopup_RectTransform.gameObject.SetActive(true);
					m_TextPopup_TMPComponent.text = "You have selected link <#ffff00> ID 02";
					break;
				}
			}
		}

		public void OnPointerEnter(PointerEventData eventData)
		{
			isHoveringObject = true;
		}

		public void OnPointerExit(PointerEventData eventData)
		{
			isHoveringObject = false;
		}

		public void OnPointerClick(PointerEventData eventData)
		{
		}

		public void OnPointerUp(PointerEventData eventData)
		{
		}
	}
}
