using System.Collections;
using UnityEngine;

namespace TMPro.Examples
{
	public class TextConsoleSimulator : MonoBehaviour
	{
		private enum objectType
		{
			None = 0,
			TextMeshPro = 1,
			TextMeshProUI = 2
		}

		private objectType m_textObjectType;

		private Object m_TextComponent;

		private void Awake()
		{
			m_TextComponent = base.gameObject.GetComponent<TextMeshPro>();
			if (m_TextComponent == null)
			{
				m_TextComponent = base.gameObject.GetComponent<TextMeshProUGUI>();
			}
			if (m_TextComponent as TextMeshPro != null)
			{
				m_textObjectType = objectType.TextMeshPro;
			}
			else if (m_TextComponent as TextMeshProUGUI != null)
			{
				m_textObjectType = objectType.TextMeshProUI;
			}
			else
			{
				m_textObjectType = objectType.None;
			}
		}

		private void Start()
		{
			switch (m_textObjectType)
			{
			case objectType.TextMeshPro:
				StartCoroutine(RevealCharacters(m_TextComponent as TextMeshPro));
				break;
			case objectType.TextMeshProUI:
				StartCoroutine(RevealCharacters(m_TextComponent as TextMeshProUGUI));
				break;
			}
		}

		private IEnumerator RevealCharacters(TextMeshPro textComponent)
		{
			textComponent.ForceMeshUpdate();
			TMP_TextInfo textInfo = textComponent.textInfo;
			int totalVisibleCharacters = textInfo.characterCount;
			int counter = 0;
			int visibleCount2 = 0;
			while (true)
			{
				visibleCount2 = (textComponent.maxVisibleCharacters = counter % (totalVisibleCharacters + 1));
				if (visibleCount2 >= totalVisibleCharacters)
				{
					yield return new WaitForSeconds(1f);
				}
				counter++;
				yield return new WaitForSeconds(0f);
			}
		}

		private IEnumerator RevealCharacters(TextMeshProUGUI textComponent)
		{
			textComponent.ForceMeshUpdate();
			TMP_TextInfo textInfo = textComponent.textInfo;
			int totalVisibleCharacters = textInfo.characterCount;
			int counter = 0;
			int visibleCount2 = 0;
			while (true)
			{
				visibleCount2 = (textComponent.maxVisibleCharacters = counter % (totalVisibleCharacters + 1));
				if (visibleCount2 >= totalVisibleCharacters)
				{
					yield return new WaitForSeconds(1f);
				}
				counter++;
				yield return new WaitForSeconds(0f);
			}
		}

		private IEnumerator RevealWords(TextMeshPro textComponent)
		{
			textComponent.ForceMeshUpdate();
			int totalWordCount = textComponent.textInfo.wordCount;
			int totalVisibleCharacters = textComponent.textInfo.characterCount;
			int counter = 0;
			int currentWord2 = 0;
			int visibleCount = 0;
			while (true)
			{
				currentWord2 = counter % (totalWordCount + 1);
				if (currentWord2 == 0)
				{
					visibleCount = 0;
				}
				else if (currentWord2 < totalWordCount)
				{
					visibleCount = textComponent.textInfo.wordInfo[currentWord2 - 1].lastCharacterIndex + 1;
				}
				else if (currentWord2 == totalWordCount)
				{
					visibleCount = totalVisibleCharacters;
				}
				textComponent.maxVisibleCharacters = visibleCount;
				if (visibleCount >= totalVisibleCharacters)
				{
					yield return new WaitForSeconds(1f);
				}
				counter++;
				yield return new WaitForSeconds(0.1f);
			}
		}

		private IEnumerator RevealWords(TextMeshProUGUI textComponent)
		{
			textComponent.ForceMeshUpdate();
			int totalWordCount = textComponent.textInfo.wordCount;
			int totalVisibleCharacters = textComponent.textInfo.characterCount;
			int counter = 0;
			int currentWord2 = 0;
			int visibleCount = 0;
			while (true)
			{
				currentWord2 = counter % (totalWordCount + 1);
				if (currentWord2 == 0)
				{
					visibleCount = 0;
				}
				else if (currentWord2 < totalWordCount)
				{
					visibleCount = textComponent.textInfo.wordInfo[currentWord2 - 1].lastCharacterIndex + 1;
				}
				else if (currentWord2 == totalWordCount)
				{
					visibleCount = totalVisibleCharacters;
				}
				textComponent.maxVisibleCharacters = visibleCount;
				if (visibleCount >= totalVisibleCharacters)
				{
					yield return new WaitForSeconds(1f);
				}
				counter++;
				yield return new WaitForSeconds(0.1f);
			}
		}
	}
}
