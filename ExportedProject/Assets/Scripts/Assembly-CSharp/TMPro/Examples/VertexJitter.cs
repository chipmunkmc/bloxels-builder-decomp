using System.Collections;
using UnityEngine;

namespace TMPro.Examples
{
	public class VertexJitter : MonoBehaviour
	{
		private struct VertexAnim
		{
			public float angleRange;

			public float angle;

			public float speed;
		}

		public float AngleMultiplier = 1f;

		public float SpeedMultiplier = 1f;

		public float CurveScale = 1f;

		private TMP_Text m_TextComponent;

		private bool hasTextChanged;

		private void Awake()
		{
			m_TextComponent = GetComponent<TMP_Text>();
		}

		private void OnEnable()
		{
			TMPro_EventManager.TEXT_CHANGED_EVENT.Add(ON_TEXT_CHANGED);
		}

		private void OnDisable()
		{
			TMPro_EventManager.TEXT_CHANGED_EVENT.Remove(ON_TEXT_CHANGED);
		}

		private void Start()
		{
			StartCoroutine(AnimateVertexColors());
		}

		private void ON_TEXT_CHANGED(Object obj)
		{
			hasTextChanged = true;
		}

		private IEnumerator AnimateVertexColors()
		{
			m_TextComponent.ForceMeshUpdate();
			TMP_TextInfo textInfo = m_TextComponent.textInfo;
			Vector3[][] copyOfVertices = new Vector3[0][];
			int loopCount = 0;
			hasTextChanged = true;
			VertexAnim[] vertexAnim = new VertexAnim[1024];
			for (int i = 0; i < 1024; i++)
			{
				vertexAnim[i].angleRange = Random.Range(10f, 25f);
				vertexAnim[i].speed = Random.Range(1f, 3f);
			}
			while (true)
			{
				if (hasTextChanged)
				{
					if (copyOfVertices.Length < textInfo.meshInfo.Length)
					{
						copyOfVertices = new Vector3[textInfo.meshInfo.Length][];
					}
					for (int j = 0; j < textInfo.meshInfo.Length; j++)
					{
						int num = textInfo.meshInfo[j].vertices.Length;
						copyOfVertices[j] = new Vector3[num];
					}
					hasTextChanged = false;
				}
				int characterCount = textInfo.characterCount;
				if (characterCount == 0)
				{
					yield return new WaitForSeconds(0.25f);
					continue;
				}
				for (int k = 0; k < characterCount; k++)
				{
					TMP_CharacterInfo tMP_CharacterInfo = textInfo.characterInfo[k];
					if (tMP_CharacterInfo.isVisible)
					{
						VertexAnim vertexAnim2 = vertexAnim[k];
						int materialReferenceIndex = textInfo.characterInfo[k].materialReferenceIndex;
						int vertexIndex = textInfo.characterInfo[k].vertexIndex;
						Vector3[] vertices = textInfo.meshInfo[materialReferenceIndex].vertices;
						Vector2 vector = (vertices[vertexIndex] + vertices[vertexIndex + 2]) / 2f;
						Vector3 vector2 = vector;
						copyOfVertices[materialReferenceIndex][vertexIndex] = vertices[vertexIndex] - vector2;
						copyOfVertices[materialReferenceIndex][vertexIndex + 1] = vertices[vertexIndex + 1] - vector2;
						copyOfVertices[materialReferenceIndex][vertexIndex + 2] = vertices[vertexIndex + 2] - vector2;
						copyOfVertices[materialReferenceIndex][vertexIndex + 3] = vertices[vertexIndex + 3] - vector2;
						vertexAnim2.angle = Mathf.SmoothStep(0f - vertexAnim2.angleRange, vertexAnim2.angleRange, Mathf.PingPong((float)loopCount / 25f * vertexAnim2.speed, 1f));
						Vector3 vector3 = new Vector3(Random.Range(-0.25f, 0.25f), Random.Range(-0.25f, 0.25f), 0f);
						Matrix4x4 matrix = Matrix4x4.TRS(vector3 * CurveScale, Quaternion.Euler(0f, 0f, Random.Range(-5f, 5f) * AngleMultiplier), Vector3.one);
						copyOfVertices[materialReferenceIndex][vertexIndex] = matrix.MultiplyPoint3x4(copyOfVertices[materialReferenceIndex][vertexIndex]);
						copyOfVertices[materialReferenceIndex][vertexIndex + 1] = matrix.MultiplyPoint3x4(copyOfVertices[materialReferenceIndex][vertexIndex + 1]);
						copyOfVertices[materialReferenceIndex][vertexIndex + 2] = matrix.MultiplyPoint3x4(copyOfVertices[materialReferenceIndex][vertexIndex + 2]);
						copyOfVertices[materialReferenceIndex][vertexIndex + 3] = matrix.MultiplyPoint3x4(copyOfVertices[materialReferenceIndex][vertexIndex + 3]);
						copyOfVertices[materialReferenceIndex][vertexIndex] += vector2;
						copyOfVertices[materialReferenceIndex][vertexIndex + 1] += vector2;
						copyOfVertices[materialReferenceIndex][vertexIndex + 2] += vector2;
						copyOfVertices[materialReferenceIndex][vertexIndex + 3] += vector2;
						vertexAnim[k] = vertexAnim2;
					}
				}
				for (int l = 0; l < textInfo.meshInfo.Length; l++)
				{
					textInfo.meshInfo[l].mesh.vertices = copyOfVertices[l];
					m_TextComponent.UpdateGeometry(textInfo.meshInfo[l].mesh, l);
				}
				loopCount++;
				yield return new WaitForSeconds(0.1f);
			}
		}
	}
}
