namespace TMPro
{
	public enum VertexSortingOrder
	{
		Normal = 0,
		Reverse = 1,
		Depth = 2
	}
}
