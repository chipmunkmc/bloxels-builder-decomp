using System.Collections.Generic;
using UnityEngine;

public class Teach : MonoBehaviour
{
	public enum Item
	{
		None = 0,
		Editor = 1,
		GameMap = 2,
		MegaBoards = 3,
		Saving = 4,
		Tags = 5
	}

	public string howTo_Editor_Title = "Bloxels Editor";

	public string howTo_Saving_Title = "Auto Save";

	public string howTo_GameMap_Title = "Game Map";

	public string howTo_MegaBoards_Title = "Backgrounds";

	public string howTo_Editor_Description = "Welcome to the Bloxels Editor!  We'll walk you through a few areas first. If at any time you turn off the tips, you can turn them back on from the help menu!";

	public string howTo_Saving_Description = "Every change you make is being saved automatically, no need to hit save ever! All items are saved to your library for you!";

	public string howTo_GameMap_Description = "Use the map to add rooms to your game and navigate between them. Remember when you change rooms you're also changing the hero start position.";

	public string howTo_MegaBoards_Description = "Select a board from the Boards library and paint in the canvas to create backgrounds!";

	public static Dictionary<Item, RectPosition> indicatorPositionByItem = new Dictionary<Item, RectPosition>
	{
		{
			Item.None,
			new RectPosition(Direction.All, Vector2.zero)
		},
		{
			Item.Editor,
			new RectPosition(Direction.UpRight, new Vector2(-220f, 20f))
		},
		{
			Item.GameMap,
			new RectPosition(Direction.DownLeft, new Vector2(10f, 220f))
		},
		{
			Item.MegaBoards,
			new RectPosition(Direction.UpLeft, new Vector2(-40f, -360f))
		},
		{
			Item.Saving,
			new RectPosition(Direction.UpLeft, new Vector2(40f, -150f))
		},
		{
			Item.Tags,
			new RectPosition(Direction.UpLeft, new Vector2(0f, 40f))
		}
	};

	private void Awake()
	{
		howTo_Editor_Title = LocalizationManager.getInstance().getLocalizedText("gamebuilder202", "Bloxels Editor");
		howTo_Saving_Title = LocalizationManager.getInstance().getLocalizedText("gamebuilder203", "Auto Save");
		howTo_GameMap_Title = LocalizationManager.getInstance().getLocalizedText("gameplay15", "Game Map");
		howTo_MegaBoards_Title = LocalizationManager.getInstance().getLocalizedText("gamebuilder4", "Backgrounds");
		howTo_Editor_Description = LocalizationManager.getInstance().getLocalizedText("gamebuilder204", "Welcome to the Bloxels Editor!  We'll walk you through a few areas first. If at any time you turn off the tips, you can turn them back on from the help menu!");
		howTo_Saving_Description = LocalizationManager.getInstance().getLocalizedText("gamebuilder205", "Every change you make is being saved automatically, no need to hit save ever! All items are saved to your library for you!");
		howTo_GameMap_Description = LocalizationManager.getInstance().getLocalizedText("gamebuilder206", "Use the map to add rooms to your game and navigate between them. Remember when you change rooms you're also changing the hero start position.");
		howTo_MegaBoards_Description = LocalizationManager.getInstance().getLocalizedText("gamebuilder207", "Select a board from the Boards library and paint in the canvas to create backgrounds!");
	}
}
