using System.Collections;
using SocketIO;
using UnityEngine;
using UnityEngine.UI;

public class TestSocketIO : MonoBehaviour
{
	private SocketIOComponent socket;

	public Button webButton;

	public void Start()
	{
		string deviceName = WebCamTexture.devices[0].name;
		WebCamTexture webCamTexture = new WebCamTexture(deviceName, 1280, 720, 30);
		webCamTexture.Play();
		RawImage componentInChildren = GetComponentInChildren<RawImage>();
		componentInChildren.texture = webCamTexture;
		GameObject gameObject = GameObject.Find("SocketIO");
		socket = gameObject.GetComponent<SocketIOComponent>();
		socket.On("open", TestOpen);
		socket.On("boop", TestBoop);
		socket.On("error", TestError);
		socket.On("close", TestClose);
	}

	public void doWeb()
	{
		string url = "http://192.168.1.151/bar.html";
		WWW wWW = new WWW(url);
		while (!wWW.isDone)
		{
		}
		webButton.GetComponentInChildren<Text>().text = wWW.text;
	}

	public void goConnect()
	{
		socket.Connect();
		StartCoroutine("BeepBoop");
	}

	private IEnumerator BeepBoop()
	{
		yield return new WaitForSeconds(1f);
		socket.Emit("beep");
		yield return new WaitForSeconds(3f);
		socket.Emit("beep");
		yield return new WaitForSeconds(2f);
		socket.Emit("beep");
		yield return null;
		socket.Emit("beep");
		socket.Emit("beep");
	}

	public void TestOpen(SocketIOEvent e)
	{
	}

	public void TestBoop(SocketIOEvent e)
	{
		if (e.data != null)
		{
		}
	}

	public void TestError(SocketIOEvent e)
	{
	}

	public void TestClose(SocketIOEvent e)
	{
	}
}
