using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class TextLocalizer : MonoBehaviour, Localizable
{
	public string unityKey = string.Empty;

	private void Awake()
	{
		LocalizationManager.getInstance().addSubscriber(this);
		localizeText();
	}

	private void OnDestroy()
	{
		LocalizationManager.getInstance().removeSubscriber(this);
	}

	public void localizeText()
	{
		if (unityKey != string.Empty)
		{
			localizeTMPro(unityKey);
			localizeBuiltinUnityText(unityKey);
		}
	}

	private void localizeTMPro(string unityKey)
	{
		TextMeshProUGUI component = GetComponent<TextMeshProUGUI>();
		if (component != null)
		{
			component.text = LocalizationManager.getInstance().getLocalizedText(unityKey, component.text);
		}
	}

	private void localizeBuiltinUnityText(string unityKey)
	{
		Text component = GetComponent<Text>();
		if (component != null)
		{
			component.text = LocalizationManager.getInstance().getLocalizedText(unityKey, component.text);
		}
	}
}
