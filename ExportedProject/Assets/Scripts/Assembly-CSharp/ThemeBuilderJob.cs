using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;

public class ThemeBuilderJob : ThreadedJob
{
	public delegate void HandleParseComplete(Dictionary<BlockColor, BloxelProject>[] _themes);

	public bool isRunning;

	public Dictionary<ThemeType, string[]> templates;

	public Dictionary<ThemeType, List<string>> jsonLookup;

	public Dictionary<BlockColor, BloxelProject>[] themes;

	public event HandleParseComplete OnComplete;

	protected override void ThreadFunction()
	{
		isRunning = true;
		SetupThemeTemplates();
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete(themes);
		}
	}

	private void SetupThemeTemplates()
	{
		ThemeType[] array = Enum.GetValues(typeof(ThemeType)) as ThemeType[];
		themes = new Dictionary<BlockColor, BloxelProject>[array.Length];
		for (int i = 0; i < array.Length; i++)
		{
			themes[i] = ThemeDictionaryInit(array[i]);
		}
	}

	private Dictionary<BlockColor, BloxelProject> ThemeDictionaryInit(ThemeType type)
	{
		Dictionary<BlockColor, BloxelProject> dictionary = new Dictionary<BlockColor, BloxelProject>();
		for (int i = 0; i < templates[type].Length; i++)
		{
			string s = jsonLookup[type][i];
			string[] array = templates[type][i].Split('/');
			string text = array[array.Length - 1];
			string[] array2 = text.Split('_');
			BloxelProject value = null;
			switch (array2[0])
			{
			case "board":
			{
				BloxelBoard bloxelBoard = new BloxelBoard(s, DataSource.JSONString);
				value = bloxelBoard;
				break;
			}
			case "anim":
			{
				BloxelAnimation bloxelAnimation = new BloxelAnimation(s, DataSource.JSONString);
				value = bloxelAnimation;
				break;
			}
			}
			switch (array2[1])
			{
			case "red.json":
				dictionary[BlockColor.Red] = value;
				break;
			case "orange.json":
				dictionary[BlockColor.Orange] = value;
				break;
			case "yellow.json":
				dictionary[BlockColor.Yellow] = value;
				break;
			case "green.json":
				dictionary[BlockColor.Green] = value;
				break;
			case "blue.json":
				dictionary[BlockColor.Blue] = value;
				break;
			case "purple.json":
				dictionary[BlockColor.Purple] = value;
				break;
			case "pink.json":
				dictionary[BlockColor.Pink] = value;
				break;
			case "white.json":
				dictionary[BlockColor.White] = value;
				break;
			}
		}
		return dictionary;
	}
}
