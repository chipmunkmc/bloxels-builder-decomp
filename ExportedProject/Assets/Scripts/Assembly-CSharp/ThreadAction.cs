using System;
using System.Threading;

public class ThreadAction : ThreadedJob
{
	public delegate void HandleComplete();

	public bool isRunning;

	public Action action;

	public event HandleComplete OnComplete;

	public ThreadAction(Action _action)
	{
		action = _action;
	}

	protected override void ThreadFunction()
	{
		isRunning = true;
		if (action != null)
		{
			action();
		}
	}

	protected override void OnFinished()
	{
		isRunning = false;
		if (this.OnComplete != null)
		{
			this.OnComplete();
		}
	}
}
