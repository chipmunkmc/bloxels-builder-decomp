using DG.Tweening;
using UnityEngine;

public class ThreadTest : MonoBehaviour
{
	private Job myJob;

	public GameObject sphere;

	private void Start()
	{
		myJob = new Job();
		myJob.inData = new Vector3[10];
		myJob.Start();
		DoSomeAnimationStuff();
	}

	private void Update()
	{
		if (myJob != null && myJob.Update())
		{
			myJob = null;
		}
	}

	private void DoSomeAnimationStuff()
	{
		sphere.transform.DOScale(6f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			sphere.transform.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				DoSomeAnimationStuff();
			});
		});
	}
}
