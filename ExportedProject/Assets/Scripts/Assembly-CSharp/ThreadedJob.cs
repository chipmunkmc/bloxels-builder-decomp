using System.Collections;
using System.Threading;

public class ThreadedJob
{
	private bool _isDone;

	private object _handle = new object();

	protected Thread _thread;

	public bool isDone
	{
		get
		{
			lock (_handle)
			{
				return _isDone;
			}
		}
		set
		{
			lock (_handle)
			{
				_isDone = value;
			}
		}
	}

	public virtual void Start()
	{
		_thread = new Thread(Run);
		_thread.Start();
	}

	public virtual void Start(string _name)
	{
		_thread = new Thread(Run);
		_thread.Name = _name;
		_thread.Start();
	}

	public virtual void Abort()
	{
		_thread.Abort();
	}

	public virtual void Interrupt()
	{
		_thread.Interrupt();
	}

	protected virtual void ThreadFunction()
	{
	}

	protected virtual void OnFinished()
	{
	}

	public virtual bool Update()
	{
		if (isDone)
		{
			OnFinished();
			return true;
		}
		return false;
	}

	public virtual IEnumerator WaitFor()
	{
		while (!Update())
		{
			yield return null;
		}
	}

	protected virtual void Run()
	{
		ThreadFunction();
		isDone = true;
	}
}
