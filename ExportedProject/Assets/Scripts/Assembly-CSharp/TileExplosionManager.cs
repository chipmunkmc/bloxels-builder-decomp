using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VoxelEngine;

public class TileExplosionManager : MonoBehaviour
{
	public static TileExplosionManager instance;

	public GameObject cubePrefab;

	[Header("Bullet Explosions")]
	public float bulletExplosionForce;

	public float bulletExplosionRadius;

	[Header("Bomb Explosions")]
	public float bombExplosionForce;

	public float bombExplosionRadius;

	public float bombUpwardsModifier;

	public Vector3 bombExplosionOffset;

	[Header("Enemy Explosions")]
	public float enemyExplosionForce;

	public float enemyExplosionRadius;

	public float enemyExplodeUpwardsModifier;

	[Range(0f, 1f)]
	public float enemyExplosionTilePosOffset;

	public Vector3 enemyExplosionOffset;

	public float tileExplosionForce;

	public float tileExplosionRadius;

	public LinkedList<ExplosionController> currentExplosions;

	private ExplosionController[] _explosionPool;

	private bool _initComplete;

	private int[] colIndices = new int[13]
	{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
		10, 11, 12
	};

	private int[] rowIndices = new int[13]
	{
		0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
		10, 11, 12
	};

	private System.Random rnd = new System.Random();

	private void Shuffle(ref int[] array)
	{
		int num = array.Length;
		while (num > 1)
		{
			int num2 = rnd.Next(num--);
			int num3 = array[num];
			array[num] = array[num2];
			array[num2] = num3;
		}
	}

	private void Awake()
	{
		if (_initComplete)
		{
			return;
		}
		if (instance == null)
		{
			instance = this;
			currentExplosions = new LinkedList<ExplosionController>();
			_explosionPool = new ExplosionController[40];
			for (int i = 0; i < _explosionPool.Length; i++)
			{
				_explosionPool[i] = new ExplosionController();
			}
			_initComplete = true;
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	public void ExplodeChunk(MovableChunk2D chunk, Transform chunkMeshTransform, Rigidbody2D attachedRigidbody, Vector3 explosionPosition, float explosionForce, float explosionRadius, float upwardsModifier, Vector3 scale, bool playSound = true)
	{
		ExplosionController explosionController = null;
		for (int i = 0; i < _explosionPool.Length; i++)
		{
			if (!currentExplosions.Contains(_explosionPool[i]))
			{
				explosionController = _explosionPool[i];
				break;
			}
		}
		if (explosionController != null)
		{
			Shuffle(ref colIndices);
			Shuffle(ref rowIndices);
			if (playSound)
			{
				SoundManager.PlayOneShot(SoundManager.instance.destructSFX);
			}
			explosionController.ExplodeBetter(chunk, chunkMeshTransform, attachedRigidbody, explosionPosition, ref colIndices, ref rowIndices, explosionForce, explosionRadius, upwardsModifier, scale);
			GameHUD.instance.blocksDestroyed += chunk.blocks.Length;
			StartCoroutine(RecycleAllExplosionCubes(explosionController));
		}
	}

	public void PrepareCubesForExplosion(ExplosionController explosionController)
	{
		int count = CubePool.instance.pool.Count;
		if (count < explosionController.totalNumberOfCubes)
		{
			int num = explosionController.totalNumberOfCubes - count;
			int num2 = num % currentExplosions.Count;
			int num3 = Mathf.FloorToInt((float)num / (float)currentExplosions.Count);
			for (LinkedListNode<ExplosionController> linkedListNode = currentExplosions.First; linkedListNode != null; linkedListNode = linkedListNode.Next)
			{
				num2 = linkedListNode.Value.RecycleSomeExplosionCubes(num3 + num2);
			}
			if (num2 <= 0 && CubePool.instance.pool.Count >= explosionController.totalNumberOfCubes)
			{
			}
		}
	}

	public bool AddActiveExplosion(ExplosionController explosionController)
	{
		currentExplosions.AddLast(explosionController);
		return true;
	}

	public bool RemoveExpiredExplosion(ExplosionController explosionController)
	{
		return currentExplosions.Remove(explosionController);
	}

	private IEnumerator RecycleAllExplosionCubes(ExplosionController ex)
	{
		ex.explosionClock = Time.time;
		ex.isExploding = true;
		bool shouldContinue = true;
		ex.nextCubeIndex = 0;
		while (ex.nextCubeIndex < ex.totalNumberOfCubes)
		{
			while (ex.explosionClock < ex.cubesToRecycle[ex.nextCubeIndex].expirationTime)
			{
				yield return new WaitForEndOfFrame();
				ex.explosionClock += Time.deltaTime;
				if (ex.nextCubeIndex >= ex.totalNumberOfCubes)
				{
					shouldContinue = false;
					break;
				}
			}
			if (!shouldContinue)
			{
				break;
			}
			ExplosionCube cube = ex.cubesToRecycle[ex.nextCubeIndex].cube;
			cube.Despawn();
			ex.nextCubeIndex++;
		}
		ex.isExploding = false;
		RemoveExpiredExplosion(ex);
	}
}
