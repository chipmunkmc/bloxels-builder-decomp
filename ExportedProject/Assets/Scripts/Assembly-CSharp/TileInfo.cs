using UnityEngine;
using VoxelEngine;

public class TileInfo
{
	public int xTileDataIndex;

	public int yTileDataIndex;

	public BloxelProject data;

	public BloxelLevel bloxelLevel;

	public WorldPos2D worldLocation;

	public GridLocation locationInLevel;

	public GridLocation levelLocationInWorld;

	public BlockColor wireframeColor;

	public GameBehavior gameBehavior;

	public Vector3 localPositionWhenUnloaded;

	public bool isLoaded;

	public bool queuedForLoading;

	public bool shouldRespawn;

	public TileInfo(BloxelProject data, BloxelLevel bloxelLevel, GameBehavior gameBehavior, GridLocation locationInLevel, GridLocation levelLocationInWorld)
	{
		this.data = data;
		this.bloxelLevel = bloxelLevel;
		this.gameBehavior = gameBehavior;
		wireframeColor = (BlockColor)gameBehavior;
		this.locationInLevel = locationInLevel;
		this.levelLocationInWorld = levelLocationInWorld;
		xTileDataIndex = levelLocationInWorld.c * 13 + locationInLevel.c;
		yTileDataIndex = levelLocationInWorld.r * 13 + locationInLevel.r;
		worldLocation = new WorldPos2D(xTileDataIndex * 13, yTileDataIndex * 13);
		localPositionWhenUnloaded = new Vector3(worldLocation.x, worldLocation.y, 0f);
		isLoaded = false;
		shouldRespawn = true;
		queuedForLoading = false;
	}

	public void Initialize(BloxelProject data, BloxelLevel bloxelLevel, GameBehavior gameBehavior, GridLocation locationInLevel, GridLocation levelLocationInWorld)
	{
		this.data = data;
		this.bloxelLevel = bloxelLevel;
		this.gameBehavior = gameBehavior;
		wireframeColor = (BlockColor)gameBehavior;
		this.locationInLevel = locationInLevel;
		this.levelLocationInWorld = levelLocationInWorld;
		xTileDataIndex = levelLocationInWorld.c * 13 + locationInLevel.c;
		yTileDataIndex = levelLocationInWorld.r * 13 + locationInLevel.r;
		worldLocation = new WorldPos2D(xTileDataIndex * 13, yTileDataIndex * 13);
		localPositionWhenUnloaded = new Vector3(worldLocation.x, worldLocation.y, 0f);
		isLoaded = false;
		shouldRespawn = true;
		queuedForLoading = false;
	}

	public void ResetLocation(GridLocation locationInLevel, GridLocation levelLocationInWorld)
	{
		this.locationInLevel = locationInLevel;
		this.levelLocationInWorld = levelLocationInWorld;
		xTileDataIndex = levelLocationInWorld.c * 13 + locationInLevel.c;
		yTileDataIndex = levelLocationInWorld.r * 13 + locationInLevel.r;
		worldLocation = new WorldPos2D(xTileDataIndex * 13, yTileDataIndex * 13);
		localPositionWhenUnloaded = new Vector3(worldLocation.x, worldLocation.y, 0f);
	}
}
