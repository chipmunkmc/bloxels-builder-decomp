using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(CircleCollider2D))]
public class TilePhysicsEnabler : MonoBehaviour
{
	public TileRenderController tileRenderer;

	public CircleCollider2D circleCollider;

	private void OnTriggerEnter2D(Collider2D coll)
	{
		if ((tileRenderer.physicsTriggerMaskValue & (1 << coll.gameObject.layer)) > 0)
		{
			circleCollider.enabled = false;
			tileRenderer.shouldGenerateColliders = true;
		}
	}
}
