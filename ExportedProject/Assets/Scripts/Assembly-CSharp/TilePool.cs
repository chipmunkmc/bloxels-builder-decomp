using System.Collections.Generic;
using UnityEngine;

public class TilePool : MonoBehaviour
{
	public static TilePool instance;

	public Transform selfTransform;

	public int poolExpansionSize;

	public int coinPoolSize;

	public int destructiblePoolSize;

	public int enemyPoolSize;

	public int npcPoolSize;

	public int hazardPoolSize;

	public int powerupPoolSize;

	public int waterPoolSize;

	public int platformPoolSize;

	public GameObject coinPrefab;

	public GameObject destructiblePrefab;

	public GameObject enemyPrefab;

	public GameObject npcPrefab;

	public GameObject hazardPrefab;

	public GameObject powerUpPrefab;

	public GameObject waterPrefab;

	public GameObject platformPrefab;

	private static bool _InitComplete;

	public Stack<BloxelCoinTile> coinPool { get; private set; }

	public Stack<BloxelDestructibleTile> destructiblePool { get; private set; }

	public Stack<BloxelEnemyTile> enemyPool { get; private set; }

	public Stack<BloxelNPCTile> npcPool { get; private set; }

	public Stack<BloxelHazardTile> hazardPool { get; private set; }

	public Stack<BloxelPowerUpTile> powerUpPool { get; private set; }

	public Stack<BloxelWaterTile> waterPool { get; private set; }

	public Stack<BloxelPlatformTile> platformPool { get; private set; }

	private void Awake()
	{
		if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		else if (!_InitComplete)
		{
			instance = this;
			_InitComplete = true;
			Object.DontDestroyOnLoad(base.gameObject);
			selfTransform = base.transform;
			Initialize();
		}
	}

	private void Initialize()
	{
		if (SystemInfo.systemMemorySize < 750)
		{
			coinPoolSize = 40;
			destructiblePoolSize = 60;
			enemyPoolSize = 60;
			npcPoolSize = 20;
			hazardPoolSize = 40;
			waterPoolSize = 60;
			platformPoolSize = 180;
		}
		coinPool = new Stack<BloxelCoinTile>(coinPoolSize);
		destructiblePool = new Stack<BloxelDestructibleTile>(destructiblePoolSize);
		enemyPool = new Stack<BloxelEnemyTile>(enemyPoolSize);
		npcPool = new Stack<BloxelNPCTile>(npcPoolSize);
		hazardPool = new Stack<BloxelHazardTile>(hazardPoolSize);
		powerUpPool = new Stack<BloxelPowerUpTile>(powerupPoolSize);
		waterPool = new Stack<BloxelWaterTile>(waterPoolSize);
		platformPool = new Stack<BloxelPlatformTile>(platformPoolSize);
		AllocatePoolObjects();
	}

	private void AllocatePoolObjects()
	{
		for (int i = 0; i < coinPoolSize; i++)
		{
			GameObject gameObject = Object.Instantiate(coinPrefab);
			gameObject.name = coinPrefab.name;
			BloxelCoinTile component = gameObject.GetComponent<BloxelCoinTile>();
			component.EarlyAwake();
			component.selfTransform.SetParent(selfTransform);
			gameObject.SetActive(false);
			coinPool.Push(component);
		}
		for (int j = 0; j < destructiblePoolSize; j++)
		{
			GameObject gameObject2 = Object.Instantiate(destructiblePrefab);
			gameObject2.name = destructiblePrefab.name;
			BloxelDestructibleTile component2 = gameObject2.GetComponent<BloxelDestructibleTile>();
			component2.EarlyAwake();
			component2.selfTransform.SetParent(selfTransform);
			gameObject2.SetActive(false);
			destructiblePool.Push(component2);
		}
		for (int k = 0; k < enemyPoolSize; k++)
		{
			GameObject gameObject3 = Object.Instantiate(enemyPrefab);
			gameObject3.name = enemyPrefab.name;
			BloxelEnemyTile component3 = gameObject3.GetComponent<BloxelEnemyTile>();
			component3.EarlyAwake();
			component3.selfTransform.SetParent(selfTransform);
			gameObject3.SetActive(false);
			enemyPool.Push(component3);
		}
		for (int l = 0; l < npcPoolSize; l++)
		{
			GameObject gameObject4 = Object.Instantiate(npcPrefab);
			gameObject4.name = npcPrefab.name;
			BloxelNPCTile component4 = gameObject4.GetComponent<BloxelNPCTile>();
			component4.EarlyAwake();
			component4.selfTransform.SetParent(selfTransform);
			gameObject4.SetActive(false);
			npcPool.Push(component4);
		}
		for (int m = 0; m < hazardPoolSize; m++)
		{
			GameObject gameObject5 = Object.Instantiate(hazardPrefab);
			gameObject5.name = hazardPrefab.name;
			BloxelHazardTile component5 = gameObject5.GetComponent<BloxelHazardTile>();
			component5.EarlyAwake();
			component5.selfTransform.SetParent(selfTransform);
			gameObject5.SetActive(false);
			hazardPool.Push(component5);
		}
		for (int n = 0; n < powerupPoolSize; n++)
		{
			GameObject gameObject6 = Object.Instantiate(powerUpPrefab);
			gameObject6.name = powerUpPrefab.name;
			BloxelPowerUpTile component6 = gameObject6.GetComponent<BloxelPowerUpTile>();
			component6.EarlyAwake();
			component6.selfTransform.SetParent(selfTransform);
			gameObject6.SetActive(false);
			powerUpPool.Push(component6);
		}
		for (int num = 0; num < waterPoolSize; num++)
		{
			GameObject gameObject7 = Object.Instantiate(waterPrefab);
			gameObject7.name = waterPrefab.name;
			BloxelWaterTile component7 = gameObject7.GetComponent<BloxelWaterTile>();
			component7.EarlyAwake();
			component7.selfTransform.SetParent(selfTransform);
			gameObject7.SetActive(false);
			waterPool.Push(component7);
		}
		for (int num2 = 0; num2 < platformPoolSize; num2++)
		{
			GameObject gameObject8 = Object.Instantiate(platformPrefab);
			gameObject8.name = platformPrefab.name;
			BloxelPlatformTile component8 = gameObject8.GetComponent<BloxelPlatformTile>();
			component8.EarlyAwake();
			component8.selfTransform.SetParent(selfTransform);
			gameObject8.SetActive(false);
			platformPool.Push(component8);
		}
	}

	public BloxelCoinTile SpawnCoinTile()
	{
		if (coinPool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(coinPrefab);
				gameObject.name = coinPrefab.name;
				BloxelCoinTile component = gameObject.GetComponent<BloxelCoinTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				coinPool.Push(component);
			}
		}
		BloxelCoinTile bloxelCoinTile = coinPool.Pop();
		bloxelCoinTile.PrepareSpawn();
		return bloxelCoinTile;
	}

	public void DespawnCoinTile(ref BloxelCoinTile coinComponent)
	{
		coinComponent.PrepareDespawn();
		coinPool.Push(coinComponent);
	}

	public BloxelDestructibleTile SpawnDestructibleTile()
	{
		if (destructiblePool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(destructiblePrefab);
				gameObject.name = destructiblePrefab.name;
				BloxelDestructibleTile component = gameObject.GetComponent<BloxelDestructibleTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				destructiblePool.Push(component);
			}
		}
		BloxelDestructibleTile bloxelDestructibleTile = destructiblePool.Pop();
		bloxelDestructibleTile.PrepareSpawn();
		return bloxelDestructibleTile;
	}

	public void DespawnDestructibleTile(ref BloxelDestructibleTile destructibleComponent)
	{
		destructibleComponent.PrepareDespawn();
		destructiblePool.Push(destructibleComponent);
	}

	public BloxelEnemyTile SpawnEnemyTile()
	{
		if (enemyPool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(enemyPrefab);
				gameObject.name = enemyPrefab.name;
				BloxelEnemyTile component = gameObject.GetComponent<BloxelEnemyTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				enemyPool.Push(component);
			}
		}
		BloxelEnemyTile bloxelEnemyTile = enemyPool.Pop();
		bloxelEnemyTile.PrepareSpawn();
		return bloxelEnemyTile;
	}

	public void DespawnEnemyTile(ref BloxelEnemyTile enemyComponent)
	{
		enemyComponent.PrepareDespawn();
		enemyPool.Push(enemyComponent);
	}

	public BloxelNPCTile SpawnNPCTile()
	{
		if (npcPool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(npcPrefab);
				gameObject.name = npcPrefab.name;
				BloxelNPCTile component = gameObject.GetComponent<BloxelNPCTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				npcPool.Push(component);
			}
		}
		BloxelNPCTile bloxelNPCTile = npcPool.Pop();
		bloxelNPCTile.PrepareSpawn();
		return bloxelNPCTile;
	}

	public void DespawnNPCTile(ref BloxelNPCTile npcComponent)
	{
		npcComponent.PrepareDespawn();
		npcPool.Push(npcComponent);
	}

	public BloxelHazardTile SpawnHazardTile()
	{
		if (hazardPool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(hazardPrefab);
				gameObject.name = hazardPrefab.name;
				BloxelHazardTile component = gameObject.GetComponent<BloxelHazardTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				hazardPool.Push(component);
			}
		}
		BloxelHazardTile bloxelHazardTile = hazardPool.Pop();
		bloxelHazardTile.PrepareSpawn();
		return bloxelHazardTile;
	}

	public void DespawnHazardTile(ref BloxelHazardTile hazardComponent)
	{
		hazardComponent.PrepareDespawn();
		hazardPool.Push(hazardComponent);
	}

	public BloxelPowerUpTile SpawnPowerUpTile()
	{
		if (powerUpPool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(powerUpPrefab);
				gameObject.name = powerUpPrefab.name;
				BloxelPowerUpTile component = gameObject.GetComponent<BloxelPowerUpTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				powerUpPool.Push(component);
			}
		}
		BloxelPowerUpTile bloxelPowerUpTile = powerUpPool.Pop();
		bloxelPowerUpTile.PrepareSpawn();
		return bloxelPowerUpTile;
	}

	public void DespawnPowerUpTile(ref BloxelPowerUpTile powerUpComponent)
	{
		powerUpComponent.PrepareDespawn();
		powerUpPool.Push(powerUpComponent);
	}

	public BloxelWaterTile SpawnWaterTile()
	{
		if (waterPool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(waterPrefab);
				gameObject.name = waterPrefab.name;
				BloxelWaterTile component = gameObject.GetComponent<BloxelWaterTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				waterPool.Push(component);
			}
		}
		BloxelWaterTile bloxelWaterTile = waterPool.Pop();
		bloxelWaterTile.PrepareSpawn();
		return bloxelWaterTile;
	}

	public void DespawnWaterTile(ref BloxelWaterTile waterComponent)
	{
		waterComponent.PrepareDespawn();
		waterPool.Push(waterComponent);
	}

	public BloxelPlatformTile SpawnPlatformTile()
	{
		if (platformPool.Count == 0)
		{
			for (int i = 0; i < poolExpansionSize; i++)
			{
				GameObject gameObject = Object.Instantiate(platformPrefab);
				gameObject.name = platformPrefab.name;
				BloxelPlatformTile component = gameObject.GetComponent<BloxelPlatformTile>();
				component.EarlyAwake();
				component.selfTransform.SetParent(selfTransform);
				gameObject.SetActive(false);
				platformPool.Push(component);
			}
		}
		BloxelPlatformTile bloxelPlatformTile = platformPool.Pop();
		bloxelPlatformTile.PrepareSpawn();
		return bloxelPlatformTile;
	}

	public void DespawnPlatformTile(ref BloxelPlatformTile platformComponent)
	{
		platformComponent.PrepareDespawn();
		platformPool.Push(platformComponent);
	}

	public void PrepareForSceneChange()
	{
		Stack<BloxelCoinTile>.Enumerator enumerator = coinPool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator.Current.gameObject);
		}
		Stack<BloxelDestructibleTile>.Enumerator enumerator2 = destructiblePool.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator2.Current.gameObject);
		}
		Stack<BloxelEnemyTile>.Enumerator enumerator3 = enemyPool.GetEnumerator();
		while (enumerator3.MoveNext())
		{
			enumerator3.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator3.Current.gameObject);
		}
		Stack<BloxelNPCTile>.Enumerator enumerator4 = npcPool.GetEnumerator();
		while (enumerator4.MoveNext())
		{
			enumerator4.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator4.Current.gameObject);
		}
		Stack<BloxelHazardTile>.Enumerator enumerator5 = hazardPool.GetEnumerator();
		while (enumerator5.MoveNext())
		{
			enumerator5.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator5.Current.gameObject);
		}
		Stack<BloxelPowerUpTile>.Enumerator enumerator6 = powerUpPool.GetEnumerator();
		while (enumerator6.MoveNext())
		{
			enumerator6.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator6.Current.gameObject);
		}
		Stack<BloxelWaterTile>.Enumerator enumerator7 = waterPool.GetEnumerator();
		while (enumerator7.MoveNext())
		{
			enumerator7.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator7.Current.gameObject);
		}
		Stack<BloxelPlatformTile>.Enumerator enumerator8 = platformPool.GetEnumerator();
		while (enumerator8.MoveNext())
		{
			enumerator8.Current.selfTransform.SetParent(instance.selfTransform);
			Object.DontDestroyOnLoad(enumerator8.Current.gameObject);
		}
	}
}
