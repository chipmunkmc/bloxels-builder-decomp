using UnityEngine;
using VoxelEngine;

public class TileRenderController : MonoBehaviour
{
	protected Transform selfTransform;

	public BloxelTile bloxelTile;

	public Material tileMaterial;

	public int collisionLayer;

	public bool colliderIsTrigger;

	public BloxelBoard bloxelBoard;

	public BloxelAnimation animationProject;

	public ProjectType projectType;

	public MovableChunk2D[] frameChunks;

	public MovableChunk2D currentFrameChunk;

	private float _framesPerSecond;

	public float frameDuration;

	public int currentFrameIndex;

	public int numberOfFrames;

	public float timeOfLastFrameChange;

	private bool initComplete;

	public Vector3 tileScale;

	public MeshFilter localMeshFilter;

	public MeshRenderer localMeshRenderer;

	public PolygonCollider2D localCollider;

	public TilePhysicsEnabler physicsEnabler;

	protected bool _shouldGenerateColliders;

	protected float collisionTimer;

	protected float collisionResetInterval = 0.5f;

	public LayerMask physicsTriggerMask;

	public float framesPerSecond
	{
		get
		{
			return _framesPerSecond;
		}
		set
		{
			_framesPerSecond = value;
			frameDuration = 1f / _framesPerSecond;
		}
	}

	public virtual bool shouldGenerateColliders
	{
		get
		{
			return _shouldGenerateColliders;
		}
		set
		{
			if (value == _shouldGenerateColliders)
			{
				return;
			}
			_shouldGenerateColliders = value;
			if (_shouldGenerateColliders)
			{
				if (projectType == ProjectType.Board)
				{
					currentFrameChunk.UpdateChunk(this.bloxelBoard.localID, ref localMeshFilter, ref localCollider);
				}
				else
				{
					BloxelBoard bloxelBoard = animationProject.boards[currentFrameIndex];
					currentFrameChunk.UpdateChunk(bloxelBoard.localID, ref localMeshFilter, ref localCollider);
				}
				localCollider.enabled = true;
				collisionTimer = 0f;
			}
			else
			{
				localCollider.enabled = false;
				physicsEnabler.circleCollider.enabled = true;
			}
		}
	}

	public int physicsTriggerMaskValue { get; protected set; }

	protected virtual void Awake()
	{
		selfTransform = GetComponent<Transform>();
		bloxelTile = GetComponent<BloxelTile>();
		localCollider = GetComponent<PolygonCollider2D>();
		localMeshRenderer = GetComponent<MeshRenderer>();
		localMeshFilter = GetComponent<MeshFilter>();
		physicsTriggerMaskValue = physicsTriggerMask.value;
	}

	protected virtual void Update()
	{
		if (shouldGenerateColliders)
		{
			collisionTimer += Time.deltaTime;
			if (collisionTimer > collisionResetInterval)
			{
				Collider2D collider2D = Physics2D.OverlapCircle(new Vector2(selfTransform.position.x + 6f, selfTransform.position.y + 6f), 19.25f, physicsTriggerMask);
				if (collider2D == null)
				{
					shouldGenerateColliders = false;
				}
				else
				{
					collisionTimer = 0f;
				}
			}
		}
		if (!(timeOfLastFrameChange + frameDuration < Time.time))
		{
			return;
		}
		if (projectType == ProjectType.Board)
		{
			if (!currentFrameChunk.initialized)
			{
				currentFrameChunk.InitWithBoard(this.bloxelBoard);
			}
			currentFrameChunk.UpdateChunk(this.bloxelBoard.localID, ref localMeshFilter, ref localCollider, shouldGenerateColliders);
			timeOfLastFrameChange = Time.time;
			return;
		}
		currentFrameIndex = (currentFrameIndex + 1) % numberOfFrames;
		currentFrameChunk = frameChunks[currentFrameIndex];
		BloxelBoard bloxelBoard = animationProject.boards[currentFrameIndex];
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(bloxelBoard);
		}
		currentFrameChunk.UpdateChunk(bloxelBoard.localID, ref localMeshFilter, ref localCollider);
		timeOfLastFrameChange += frameDuration;
	}

	public virtual void RenderTile(BloxelProject projectData)
	{
		localCollider.pathCount = 0;
		switch (projectData.type)
		{
		case ProjectType.Board:
			RenderBoard(projectData as BloxelBoard);
			break;
		case ProjectType.Animation:
			RenderAnimation(projectData as BloxelAnimation);
			break;
		}
	}

	public virtual void RenderBoard(BloxelBoard board)
	{
		bloxelBoard = board;
		projectType = ProjectType.Board;
		currentFrameChunk = MovableChunk2DPool.instance.Spawn();
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(bloxelBoard);
		}
		currentFrameChunk.UpdateChunk(bloxelBoard.localID, ref localMeshFilter, ref localCollider, false);
		SetUpFrameProperties(currentFrameChunk);
		numberOfFrames = 1;
		framesPerSecond = 1f / 3f;
		base.enabled = true;
	}

	public virtual void RenderAnimation(BloxelAnimation animation)
	{
		animationProject = animation;
		projectType = ProjectType.Animation;
		numberOfFrames = animationProject.boards.Count;
		if (numberOfFrames == 0)
		{
			base.enabled = false;
			return;
		}
		frameChunks = new MovableChunk2D[numberOfFrames];
		for (int i = 0; i < numberOfFrames; i++)
		{
			MovableChunk2D movableChunk2D = MovableChunk2DPool.instance.Spawn();
			SetUpFrameProperties(movableChunk2D);
			frameChunks[i] = movableChunk2D;
		}
		framesPerSecond = animationProject.GetFrameRate();
		int num = Mathf.FloorToInt(Time.time * framesPerSecond);
		timeOfLastFrameChange = (float)num * frameDuration;
		currentFrameIndex = num % numberOfFrames;
		currentFrameChunk = frameChunks[currentFrameIndex];
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(animationProject.boards[currentFrameIndex]);
		}
		currentFrameChunk.UpdateChunk(animationProject.boards[currentFrameIndex].localID, ref localMeshFilter, ref localCollider, false);
		base.enabled = true;
	}

	public void ResetAnimation()
	{
		int num = Mathf.FloorToInt(Time.time * framesPerSecond);
		int num2 = num % numberOfFrames;
		timeOfLastFrameChange = (float)num * frameDuration;
		currentFrameIndex = num2;
		currentFrameChunk = frameChunks[currentFrameIndex];
		if (!currentFrameChunk.initialized)
		{
			currentFrameChunk.InitWithBoard(animationProject.boards[currentFrameIndex]);
		}
		currentFrameChunk.UpdateChunk(animationProject.boards[currentFrameIndex].localID, ref localMeshFilter, ref localCollider, false);
	}

	protected void SetUpFrameProperties(MovableChunk2D chunk)
	{
		localMeshRenderer.sharedMaterial = tileMaterial;
		base.gameObject.layer = collisionLayer;
		localCollider.isTrigger = colliderIsTrigger;
		selfTransform.localScale = tileScale;
	}

	public virtual void ReleaseCurrentChunks()
	{
		base.enabled = false;
		if (currentFrameChunk != null && currentFrameChunk.spawned)
		{
			currentFrameChunk.Despawn();
		}
		if (frameChunks != null && frameChunks != null)
		{
			for (int i = 0; i < frameChunks.Length; i++)
			{
				MovableChunk2D movableChunk2D = frameChunks[i];
				if (movableChunk2D != null && movableChunk2D.spawned)
				{
					movableChunk2D.Despawn();
				}
			}
			frameChunks = null;
		}
		localMeshFilter.sharedMesh = null;
		localCollider.pathCount = 0;
	}
}
