using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class TimelineFrame : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, IEventSystemHandler
{
	[Header("Data")]
	public BloxelAnimation animationParent;

	public BloxelBoard dataModel;

	private TimelineFrame _frameToSelectAfterDelete;

	[Header("UI")]
	public RectTransform rect;

	public Image uiImageOutline;

	public RawImage uiRawImage;

	public UIButton uiButtonDelete;

	public UIButton uiButtonAddBefore;

	public UIButton uiButtonAddAfter;

	private CoverBoard _cover;

	[Header("State Tracking")]
	public int idx;

	public bool isActive;

	[HideInInspector]
	public bool isDragging;

	[HideInInspector]
	public bool initComplete;

	private void Awake()
	{
		isActive = false;
		uiRawImage.color = new Color(1f, 1f, 1f, 0f);
		_cover = new CoverBoard(Color.clear);
		uiButtonDelete.transform.localScale = Vector3.zero;
		uiButtonAddAfter.transform.localScale = Vector3.zero;
		uiButtonAddBefore.transform.localScale = Vector3.zero;
	}

	private void Start()
	{
		BuildCover();
		dataModel.coverBoard.OnUpdate += HandleModelUpdate;
		uiButtonDelete.OnClick += DeleteButtonPressed;
		uiButtonAddBefore.OnClick += AddBefore;
		uiButtonAddAfter.OnClick += AddAfter;
		initComplete = true;
	}

	private void OnDestroy()
	{
		uiButtonDelete.OnClick -= DeleteButtonPressed;
		uiButtonAddBefore.OnClick -= AddBefore;
		uiButtonAddAfter.OnClick -= AddAfter;
		if (_frameToSelectAfterDelete != null)
		{
			_frameToSelectAfterDelete.Select();
		}
	}

	private void OnEnable()
	{
		uiRawImage.DOFade(1f, UIAnimationManager.speedSlow);
		if (initComplete)
		{
			dataModel.coverBoard.OnUpdate += HandleModelUpdate;
			uiButtonDelete.OnClick += DeleteButtonPressed;
			uiButtonAddBefore.OnClick += AddBefore;
			uiButtonAddAfter.OnClick += AddAfter;
		}
	}

	private void OnDisable()
	{
		uiRawImage.DOFade(0f, 0f);
		if (initComplete)
		{
			dataModel.coverBoard.OnUpdate -= HandleModelUpdate;
			uiButtonDelete.OnClick -= DeleteButtonPressed;
			uiButtonAddBefore.OnClick -= AddBefore;
			uiButtonAddAfter.OnClick -= AddAfter;
		}
	}

	private void HandleModelUpdate()
	{
		BuildCover();
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		rect.DOScale(0.95f, UIAnimationManager.speedFast);
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		rect.DOScale(1f, UIAnimationManager.speedFast);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!isDragging)
		{
			SoundManager.PlayOneShot(SoundManager.instance.confirmC);
			Select();
		}
	}

	public void BuildCover()
	{
		uiRawImage.texture = dataModel.coverBoard.UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
	}

	public void Activate()
	{
		isActive = true;
		uiImageOutline.transform.localScale = Vector3.one;
		ToggleContextButtons();
	}

	public void DeActivate()
	{
		isActive = false;
		uiImageOutline.transform.localScale = Vector3.zero;
		ToggleContextButtons();
	}

	public void ToggleContextButtons()
	{
		if (isActive)
		{
			uiButtonAddBefore.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic);
			uiButtonAddAfter.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic);
			uiButtonDelete.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic);
		}
		else
		{
			uiButtonAddBefore.transform.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic);
			uiButtonAddAfter.transform.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic);
			uiButtonDelete.transform.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic);
		}
	}

	public void Select()
	{
		PixelEditorPaintBoard.instance.SwitchBloxelBoard(dataModel);
		AnimationTimeline.instance.SetCurrentFrame(this);
		Activate();
		foreach (TimelineFrame savedFrame in AnimationTimeline.instance.savedFrames)
		{
			if (savedFrame != this)
			{
				savedFrame.DeActivate();
			}
		}
	}

	private void AddBefore()
	{
		if (AnimationTimeline.instance.savedFrames.Count < BloxelAnimation.MaxFrames)
		{
			SoundManager.PlayOneShot(SoundManager.instance.popB);
			if (!FramesLocked())
			{
				AnimationTimeline.instance.AddBeforeActiveFrame();
				AnimatedFrame.instance.InitContextualInfo();
			}
		}
	}

	private void AddAfter()
	{
		if (AnimationTimeline.instance.savedFrames.Count < BloxelAnimation.MaxFrames)
		{
			SoundManager.PlayOneShot(SoundManager.instance.popB);
			if (!FramesLocked())
			{
				AnimationTimeline.instance.AddAfterActiveFrame();
				AnimatedFrame.instance.InitContextualInfo();
			}
		}
	}

	private bool FramesLocked()
	{
		bool isLocked = AnimatedFrame.instance.uiGemLock.isLocked;
		if (isLocked)
		{
			AnimatedFrame.instance.ShowGemPurchase();
		}
		return isLocked;
	}

	private void DeleteButtonPressed()
	{
		SoundManager.PlayEventSound(SoundEvent.Trash);
		Delete();
	}

	public bool Delete()
	{
		if (AnimationTimeline.instance.savedFrames.Count <= 1)
		{
			return false;
		}
		bool flag = AnimationTimeline.instance.currentBloxelAnimation.RemoveBoard(dataModel);
		if (flag)
		{
			AnimationTimeline.instance.savedFrames.Remove(this);
			AssetManager.instance.boardPool.Remove(dataModel.ID());
			AnimatedFrame.instance.InitContextualInfo();
			if (AnimationTimeline.instance.currentFrame == this)
			{
				int index = Mathf.Clamp(rect.GetSiblingIndex(), 0, AnimationTimeline.instance.savedFrames.Count - 1);
				_frameToSelectAfterDelete = AnimationTimeline.instance.savedFrames[index];
			}
			uiRawImage.CrossFadeAlpha(0f, 0.2f, true);
			Object.Destroy(base.gameObject, 0.2f);
		}
		return flag;
	}
}
