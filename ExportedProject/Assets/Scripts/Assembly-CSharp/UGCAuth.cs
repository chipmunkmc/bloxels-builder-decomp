using System.Collections.Generic;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class UGCAuth : MonoBehaviour
{
	public UGCType type;

	public TextMeshProUGUI uiText;

	private BloxelProject _dataModel;

	public static Dictionary<UGCType, KeyValuePair<string, string>> stringLookup = new Dictionary<UGCType, KeyValuePair<string, string>>
	{
		{
			UGCType.NPC,
			new KeyValuePair<string, string>("iWall113", "I’m still reviewing this! Check back soon. -The PixelKing")
		},
		{
			UGCType.ProjectTitle,
			new KeyValuePair<string, string>("iWall21", "I'm reviewing this title -The PixelKing")
		},
		{
			UGCType.UserName,
			new KeyValuePair<string, string>("iWall112", "In Review...")
		}
	};

	private void Awake()
	{
		uiText = GetComponent<TextMeshProUGUI>();
	}

	public void Init(BloxelProject project)
	{
		_dataModel = project;
		if (_dataModel == null)
		{
			uiText.SetText("Error");
		}
		else if (_dataModel.type == ProjectType.Game && !_dataModel.ugcApproved)
		{
			KeyValuePair<string, string> keyValuePair = stringLookup[type];
			string localizedText = LocalizationManager.getInstance().getLocalizedText(keyValuePair.Key, keyValuePair.Value);
			uiText.SetText(localizedText);
		}
		else
		{
			uiText.SetText(_dataModel.title);
		}
	}
}
