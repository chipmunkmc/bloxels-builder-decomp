using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIAddBrainDropdown : UIToggleDropdown
{
	public UIPopupEnemyConfig controller;

	public UIButton uiButtonCapture;

	public Image[] uiButtonImages;

	private new void Awake()
	{
		base.Awake();
		ResetUI();
	}

	private void Start()
	{
		uiButtonCapture.OnClick += CaptureBrain;
	}

	private void OnDestroy()
	{
		uiButtonCapture.OnClick -= CaptureBrain;
	}

	public void ResetUI()
	{
		base.transform.localScale = new Vector3(1f, 0f, 1f);
	}

	public Tweener Init()
	{
		return base.transform.DOScaleY(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
	}

	public void CaptureBrain()
	{
		controller.transform.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			CaptureManager.instance.CaptureBrain(controller.tile as BloxelEnemyTile);
		});
	}
}
