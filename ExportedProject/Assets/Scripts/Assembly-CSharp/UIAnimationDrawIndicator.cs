using System.Collections;
using DG.Tweening;
using UnityEngine;

public class UIAnimationDrawIndicator : MonoBehaviour
{
	public RectTransform rect;

	public float drawScrubDistance = 580f;

	public Vector2 position;

	private void Start()
	{
		rect.DOAnchorPos(position, UIAnimationManager.speedSlow);
		StartCoroutine("DrawAnimation");
	}

	private void OnDestroy()
	{
		StopCoroutine("DrawAnimation");
		rect.DOKill();
	}

	private IEnumerator DrawAnimation()
	{
		yield return new WaitForSeconds(2f);
		rect.DOAnchorPos(new Vector2(rect.anchoredPosition.x + drawScrubDistance, rect.anchoredPosition.y), UIAnimationManager.speedSlow);
		yield return new WaitForSeconds(2f);
		rect.DOAnchorPos(new Vector2(rect.anchoredPosition.x - drawScrubDistance, rect.anchoredPosition.y), UIAnimationManager.speedSlow);
		StartCoroutine("DrawAnimation");
	}
}
