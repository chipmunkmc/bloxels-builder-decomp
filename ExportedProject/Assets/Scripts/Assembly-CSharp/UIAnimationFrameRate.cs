using System;
using System.Collections;
using System.Threading;
using UnityEngine;

public class UIAnimationFrameRate : UIValueAdjust
{
	public delegate void HandleRateChange(float _val);

	public BloxelAnimation bloxelAnimation;

	private static string _saveDelayName = "FrameRateSaveDelay";

	public bool hasSetFrameRate;

	public event HandleRateChange OnChange;

	private new void OnEnable()
	{
		base.OnEnable();
		uiButtonIncrement.OnClick += Increment;
		uiButtonDecrement.OnClick += Decrement;
		minValue = BloxelAnimation.minFrameRate;
		maxValue = BloxelAnimation.maxFrameRate;
		if (!hasSetFrameRate)
		{
			val = BloxelAnimation.defaultFrameRate;
			hasSetFrameRate = true;
		}
		StopCoroutine(_saveDelayName);
	}

	private new void OnDisable()
	{
		base.OnDisable();
		uiButtonIncrement.OnClick -= Increment;
		uiButtonDecrement.OnClick -= Decrement;
	}

	public void UpdateModel(BloxelAnimation animation)
	{
		bloxelAnimation = animation;
		SetValue(animation.GetFrameRate(), false, false);
	}

	public void SetValue(float _val, bool shouldSave = true, bool shouldNotify = true)
	{
		StopCoroutine(_saveDelayName);
		base.SetValue(_val);
		if (this.OnChange != null && shouldNotify)
		{
			this.OnChange(val);
		}
		if (shouldSave)
		{
			StartCoroutine(_saveDelayName);
		}
	}

	private new void Increment()
	{
		StopCoroutine(_saveDelayName);
		base.Increment();
		if (this.OnChange != null)
		{
			this.OnChange(val);
		}
		StartCoroutine(_saveDelayName);
	}

	private new void Decrement()
	{
		StopCoroutine(_saveDelayName);
		base.Decrement();
		if (this.OnChange != null)
		{
			this.OnChange(val);
		}
		StartCoroutine(_saveDelayName);
	}

	public IEnumerator FrameRateSaveDelay()
	{
		yield return new WaitForSeconds(1.5f);
		if (val <= 0f)
		{
			val = minValue;
		}
		bloxelAnimation.SetFrameRate(val);
	}
}
