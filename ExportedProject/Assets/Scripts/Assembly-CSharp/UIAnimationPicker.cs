using System.Collections.Generic;

public class UIAnimationPicker : UIProjectPicker
{
	public override void Init()
	{
		enhancedScroller.Delegate = this;
		projectData.Clear();
		List<string> list = new List<string>();
		foreach (BloxelAnimation value in AssetManager.instance.animationPool.Values)
		{
			if (value.tags.Contains("PixelTutz") && !list.Contains(value.ID()))
			{
				list.Add(value.ID());
			}
		}
		foreach (BloxelCharacter value2 in AssetManager.instance.characterPool.Values)
		{
			foreach (BloxelAnimation value3 in value2.animations.Values)
			{
				if (!list.Contains(value3.ID()))
				{
					list.Add(value3.ID());
				}
			}
		}
		foreach (BloxelAnimation value4 in AssetManager.instance.defaultCharacterAnimations.Values)
		{
			if (!list.Contains(value4.ID()))
			{
				list.Add(value4.ID());
			}
		}
		foreach (BloxelAnimation value5 in AssetManager.instance.animationPool.Values)
		{
			if (!list.Contains(value5.ID()))
			{
				projectData.Add(value5);
			}
		}
	}
}
