using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIAnimationTap : MonoBehaviour
{
	public RectTransform rect;

	public Image finger;

	public Image radar;

	public Animator animator;

	public void Init(RectPosition rectPos)
	{
		Vector2[] anchorsFromDirection = PPUtilities.GetAnchorsFromDirection(rectPos.dir);
		rect.anchorMin = anchorsFromDirection[0];
		rect.anchorMax = anchorsFromDirection[1];
		rect.pivot = rectPos.pivot;
		rect.DOAnchorPos(rectPos.pos, UIAnimationManager.speedSlow);
	}

	public void DestroyMe()
	{
		Object.Destroy(base.gameObject);
	}
}
