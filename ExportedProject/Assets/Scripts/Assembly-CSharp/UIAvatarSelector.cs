using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UIAvatarSelector : MonoBehaviour
{
	public delegate void HandleToggle(UIAvatarSelector avatar);

	private Button _uiButton;

	public RectTransform indicator;

	public RawImage uiRawImage;

	public bool isActive;

	public ServerSquare serverSquare;

	public string serverId;

	private Graphic[] graphics;

	private CoverBoard _cover;

	public event HandleToggle OnToggle;

	private void Awake()
	{
		_uiButton = GetComponent<Button>();
		uiRawImage = GetComponentInChildren<RawImage>();
		_uiButton.onClick.AddListener(Toggle);
		isActive = false;
		indicator.localScale = Vector3.zero;
		graphics = GetComponentsInChildren<Graphic>();
		_cover = new CoverBoard(new CoverStyle(Color.clear, 0));
	}

	public void Toggle()
	{
		if (isActive)
		{
			DeActivate();
		}
		else
		{
			Activate();
		}
		if (this.OnToggle != null)
		{
			this.OnToggle(this);
		}
	}

	public void DeActivate()
	{
		isActive = false;
		for (int i = 0; i < graphics.Length; i++)
		{
			graphics[i].DOFade(0.2f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo);
		}
		indicator.DOScale(0f, UIAnimationManager.speedMedium);
	}

	public void Activate()
	{
		isActive = true;
		for (int i = 0; i < graphics.Length; i++)
		{
			graphics[i].DOFade(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo);
		}
		indicator.DOScale(1f, UIAnimationManager.speedMedium);
	}

	public void BuildBoard(ServerSquare square)
	{
		serverSquare = square;
		StartCoroutine(DownloadTexture(serverSquare.texture, delegate(Texture2D texture)
		{
			if (texture != null)
			{
				uiRawImage.texture = texture;
				uiRawImage.texture.filterMode = FilterMode.Point;
			}
		}));
	}

	public void BuildBoard(BloxelBoard board)
	{
		serverId = board._serverID;
		uiRawImage.texture = board.UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
	}

	private IEnumerator DownloadTexture(string url, Action<Texture2D> callback)
	{
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			callback(www.texture);
		}
		else
		{
			callback(null);
		}
	}
}
