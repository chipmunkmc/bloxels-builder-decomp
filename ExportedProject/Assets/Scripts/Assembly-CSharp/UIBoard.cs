using UnityEngine;
using UnityEngine.UI;

public class UIBoard : UIInteractable
{
	[Header("Data")]
	public BloxelBoard bloxelBoard;

	[Header("UI")]
	public RawImage uiRawImage;

	public CoverBoard cover;

	private new void Awake()
	{
		base.Awake();
		if (uiRawImage == null)
		{
			uiRawImage = GetComponent<RawImage>();
		}
		cover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	public void Init(BloxelBoard _board)
	{
		uiRawImage.color = Color.white;
		bloxelBoard = _board;
		uiRawImage.texture = bloxelBoard.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
	}
}
