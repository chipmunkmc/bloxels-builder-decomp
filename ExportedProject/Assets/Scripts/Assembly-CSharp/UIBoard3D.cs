using UnityEngine;
using UnityEngine.UI;

public class UIBoard3D : MonoBehaviour
{
	private RectTransform _rect;

	public RawImage uiRawImage;

	private CoverBoard _cover;

	private BloxelBoard _bloxelBoard;

	private void Awake()
	{
		_rect = GetComponent<RectTransform>();
		uiRawImage = GetComponent<RawImage>();
		_cover = new CoverBoard(new CoverStyle(BloxelBoard.baseUIBoardColor, 5, 2, 1, true));
	}

	public void Init(BloxelBoard board)
	{
		_bloxelBoard = board;
		uiRawImage.texture = _bloxelBoard.UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
		uiRawImage.color = Color.white;
	}

	private void Update()
	{
		if (Input.GetKeyDown(KeyCode.B))
		{
			Init(AssetManager.instance.wireframeTemplates[Random.Range(0, AssetManager.instance.wireframeTemplates.Length - 1)]);
		}
	}
}
