using System.Collections.Generic;

public class UIBoardPicker : UIProjectPicker
{
	public void AddBoard(BloxelBoard b)
	{
		string item = "PixelTutz";
		string item2 = "PPHide";
		if (b.tags.Contains(item) || b.tags.Contains(item2) || !projectData.Contains(b))
		{
			projectData.Add(b);
		}
	}

	public override void Init()
	{
		enhancedScroller.Delegate = this;
		projectData.Clear();
		string item = "PixelTutz";
		string item2 = "PPHide";
		HashSet<string> hashSet = new HashSet<string>();
		foreach (BloxelBoard value3 in AssetManager.instance.boardPool.Values)
		{
			if (value3.tags.Contains(item) || value3.tags.Contains(item2))
			{
				hashSet.Add(value3.ID());
			}
		}
		foreach (BloxelMegaBoard value4 in AssetManager.instance.megaBoardPool.Values)
		{
			Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator3 = value4.boards.GetEnumerator();
			if (value4.tags.Contains(item) || value4.tags.Contains(item2))
			{
				while (enumerator3.MoveNext())
				{
					BloxelBoard value = enumerator3.Current.Value;
					hashSet.Add(value.ID());
				}
				continue;
			}
			while (enumerator3.MoveNext())
			{
				BloxelBoard value2 = enumerator3.Current.Value;
				if (value2.tags.Contains(item) || value2.tags.Contains(item2))
				{
					hashSet.Add(value2.ID());
				}
			}
		}
		foreach (BloxelAnimation value5 in AssetManager.instance.animationPool.Values)
		{
			for (int i = 0; i < value5.boards.Count; i++)
			{
				hashSet.Add(value5.boards[i].ID());
			}
		}
		foreach (BloxelLevel value6 in AssetManager.instance.levelPool.Values)
		{
			hashSet.Add(value6.coverBoard.ID());
		}
		foreach (BloxelGame value7 in AssetManager.instance.gamePool.Values)
		{
			hashSet.Add(value7.coverBoard.ID());
		}
		foreach (BloxelBoard value8 in AssetManager.instance.boardPool.Values)
		{
			if (!hashSet.Contains(value8.ID()))
			{
				projectData.Add(value8);
			}
		}
	}
}
