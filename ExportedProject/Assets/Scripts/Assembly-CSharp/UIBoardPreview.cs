using UnityEngine;
using UnityEngine.UI;

public class UIBoardPreview : MonoBehaviour
{
	public BloxelBoard dataModel;

	public RectTransform rect;

	public RawImage image;

	public CoverBoard cover;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		image = GetComponent<RawImage>();
		cover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	private void Start()
	{
		image.texture = AssetManager.instance.boardSolidBlank;
		PixelEditorPaintBoard.instance.OnBoardChange += HandleBoardChange;
	}

	private void HandleBoardChange(BloxelBoard board)
	{
		dataModel = board;
		dataModel.OnUpdate += Build;
		Build();
	}

	private void OnDestroy()
	{
		PixelEditorPaintBoard.instance.OnBoardChange -= HandleBoardChange;
	}

	public void Build()
	{
		image.texture = dataModel.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
	}
}
