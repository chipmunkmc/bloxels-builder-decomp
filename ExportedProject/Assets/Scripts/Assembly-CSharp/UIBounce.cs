using DG.Tweening;
using UnityEngine;

public class UIBounce : MonoBehaviour
{
	public RectTransform rect;

	public Vector2 position;

	private bool initComplete;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		position = rect.anchoredPosition;
	}

	private void Start()
	{
		Bounce();
		initComplete = true;
	}

	private void OnEnable()
	{
		if (initComplete)
		{
			Bounce();
		}
	}

	private void OnDisable()
	{
		base.transform.DOKill();
	}

	private void Bounce()
	{
		rect.DOAnchorPos(new Vector2(position.x, position.y + 5f), UIAnimationManager.speedSlow).OnComplete(delegate
		{
			rect.DOAnchorPos(position, UIAnimationManager.speedSlow).OnComplete(delegate
			{
				Bounce();
			});
		});
	}
}
