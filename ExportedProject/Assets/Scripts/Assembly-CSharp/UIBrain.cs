using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;

public class UIBrain : UIDraggable
{
	public delegate void HandleBrainDragStart(UIBrain brain);

	public delegate void HandleBrainDragEnd(UIBrain brain);

	public delegate void HandleBrainDragDrop(UIBrain brain);

	[Header("Data")]
	public BloxelBrain bloxelBrain;

	[Header("UI")]
	public UIBoard3D uiBoard;

	[Header("State Tracking")]
	public bool isDraggable = true;

	public bool isVisible;

	public event HandleBrainDragStart OnBrainDragStart;

	public event HandleBrainDragEnd OnBrainDragEnd;

	public event HandleBrainDragDrop OnBrainDragDrop;

	private new void Awake()
	{
		base.Awake();
		if (!isVisible)
		{
			base.transform.localScale = Vector3.zero;
		}
		if (isDraggable)
		{
			base.OnDragStart += DragStart;
			base.OnDragEnd += DragEnd;
			base.OnDropped += DragDrop;
		}
	}

	private void OnDestroy()
	{
		if (isDraggable)
		{
			base.OnDragStart -= DragStart;
			base.OnDragEnd -= DragEnd;
			base.OnDropped -= DragDrop;
		}
	}

	public void Init(BloxelBrain _brain)
	{
		bloxelBrain = _brain;
		uiBoard.Init(bloxelBrain.coverBoard);
		Show();
	}

	public void Set(BloxelBrain _brain)
	{
		bloxelBrain = _brain;
		uiBoard.Init(bloxelBrain.coverBoard);
		Show();
	}

	public void Refresh()
	{
		uiBoard.Init(bloxelBrain.coverBoard);
	}

	public void Show()
	{
		if (!isVisible)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.appearance);
			isVisible = true;
			base.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InBounce);
		}
	}

	public void Hide()
	{
		if (isVisible)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.popB);
			isVisible = false;
			base.transform.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
		}
	}

	private void DragStart()
	{
		if (this.OnBrainDragStart != null)
		{
			this.OnBrainDragStart(this);
		}
	}

	private void DragEnd()
	{
		if (this.OnBrainDragEnd != null)
		{
			this.OnBrainDragEnd(this);
		}
	}

	private void DragDrop()
	{
		if (this.OnBrainDragDrop != null)
		{
			this.OnBrainDragDrop(this);
		}
	}
}
