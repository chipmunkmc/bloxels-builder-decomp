using System;
using System.Threading;
using UnityEngine;

public class UIBrainDisplay : MonoBehaviour
{
	public delegate void HandleSelect(UIBrainDisplay _brainDisplay);

	public UIProjectCover uiProjectCover;

	public BloxelBrain bloxelBrain;

	public UIButton uiButton;

	public event HandleSelect OnSelect;

	private void Start()
	{
		uiButton.OnClick += HandleOnClick;
	}

	private void OnDestroy()
	{
		uiButton.OnClick -= HandleOnClick;
	}

	public void Init(BloxelBrain brain)
	{
		bloxelBrain = brain;
		uiProjectCover.Init(bloxelBrain.coverBoard, new CoverStyle(BloxelBoard.baseUIBoardColor, 4, 2, 1, true));
	}

	private void HandleOnClick()
	{
		if (this.OnSelect != null)
		{
			this.OnSelect(this);
		}
	}
}
