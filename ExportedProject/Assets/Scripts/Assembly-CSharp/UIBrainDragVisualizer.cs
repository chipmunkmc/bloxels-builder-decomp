using UnityEngine;

public class UIBrainDragVisualizer : UIDragItemVisualizer
{
	[Header("UI")]
	public UIBrain uiBrain;

	public void Create(UIBrain _uiBrain, Canvas canvasParent)
	{
		uiBrain.uiBoard.uiRawImage.raycastTarget = false;
		uiBrain.Set(_uiBrain.bloxelBrain);
		base.Init(canvasParent);
	}
}
