using UnityEngine;

public class UIBrainStatBar : UIStatBar
{
	[Header("Data")]
	public BlockColor blockColor;

	public int blockCount;

	private new void Awake()
	{
		base.Awake();
		_fillColor = ColorUtilities.blockRGB[blockColor];
		_uiImageFill.color = _fillColor;
		_uiTextLabel.SetText(BloxelBrain.GetTitleFromColor(blockColor));
		_uiTextLabel.color = ColorUtilities.blockRGB[blockColor];
		_maxFillCount = BloxelBrain.MaxBlocks;
	}

	public void GenerateNotch(float percentage)
	{
		if (!(percentage >= 1f))
		{
			GameObject gameObject = Object.Instantiate(UIStatBar.notchPrefab) as GameObject;
			gameObject.transform.SetParent(uiNotchGrid);
			gameObject.transform.localScale = Vector3.one;
			gameObject.transform.localPosition = Vector3.zero;
			UIStatBarNotch component = gameObject.GetComponent<UIStatBarNotch>();
			component.SetPosition(percentage, rectFillContainer.sizeDelta);
		}
	}

	public override void SetData(int count)
	{
		if (count >= BloxelBrain.MaxBlocks)
		{
			count = BloxelBrain.MaxBlocks;
		}
		blockCount = count;
		base.SetData(count);
	}
}
