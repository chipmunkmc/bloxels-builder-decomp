using System.Collections;
using DG.Tweening;
using UnityEngine;

public class UIBrainStats : MonoBehaviour
{
	[Header("Data")]
	public BloxelBrain bloxelBrain;

	public BloxelEnemyTile enemyTile;

	[Header("UI")]
	public RectTransform selfRect;

	public CapturePreviewDisplay capturePreviewDisplay;

	public UIProjectCover animatedCover;

	public RectTransform rectStats;

	public UIButton uiButtonToggle;

	public UICaptureAdjustments uiCaptureAdjustments;

	public RectTransform enemyVisualizer;

	public UIBrainStatBar[] uiStatBars;

	[Header("State Tracking")]
	public bool isVisible;

	public bool canAutoFlip;

	public float waitTimer = 3f;

	private void Awake()
	{
		selfRect = GetComponent<RectTransform>();
		uiStatBars = GetComponentsInChildren<UIBrainStatBar>();
	}

	private void Start()
	{
		uiButtonToggle.OnClick += TogglePressed;
		selfRect.localScale = Vector3.zero;
		canAutoFlip = true;
	}

	private void OnDestroy()
	{
		uiButtonToggle.OnClick -= TogglePressed;
	}

	private void GenerateNotches()
	{
		for (int i = 0; i < uiStatBars.Length; i++)
		{
			int[] array = BloxelBrain.Thresholds[(uint)uiStatBars[i].blockColor];
			if (array != null)
			{
				for (int j = 0; j < array.Length; j++)
				{
					uiStatBars[i].GenerateNotch((float)array[j] / (float)BloxelBrain.MaxBlocks);
				}
			}
		}
	}

	private void ResetUI()
	{
		base.transform.localScale = Vector3.one;
		rectStats.DOLocalRotate(new Vector3(0f, 180f, 0f), 0f);
		capturePreviewDisplay.rect.SetSiblingIndex(capturePreviewDisplay.rect.parent.childCount - 2);
		isVisible = false;
	}

	public void Init(BloxelBrain brain, BloxelEnemyTile tile)
	{
		bloxelBrain = brain;
		enemyTile = tile;
		ResetUI();
		AnimateCharacter();
		UpdateBrainStats(bloxelBrain);
		StartCoroutine("DelayedFlip");
	}

	public void UpdateBrainStats(BloxelBrain brain)
	{
		for (int i = 0; i < uiStatBars.Length; i++)
		{
			uiStatBars[i].SetData(brain.blockTotals[(uint)uiStatBars[i].blockColor]);
		}
	}

	private void AnimateCharacter()
	{
		BloxelProject data = enemyTile.tileInfo.data;
		if (data.type == ProjectType.Board)
		{
			animatedCover.Init(data as BloxelBoard, new CoverStyle(Color.clear));
		}
		else if (data.type == ProjectType.Animation)
		{
			animatedCover.Init(data as BloxelAnimation, new CoverStyle(Color.clear));
		}
		else if (data.type != ProjectType.Character)
		{
		}
	}

	private void TogglePressed()
	{
		Toggle();
	}

	private IEnumerator DelayedFlip()
	{
		if (canAutoFlip)
		{
			uiButtonToggle.transform.localScale = Vector3.zero;
			yield return new WaitForSeconds(waitTimer);
			Toggle();
			selfRect.localScale = Vector3.one;
			uiButtonToggle.transform.DOScale(Vector3.one, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				uiButtonToggle.selfRect.DOShakeScale(UIAnimationManager.speedSlow, 0.5f, 0, 0f).SetLoops(2, LoopType.Restart);
			});
			canAutoFlip = false;
		}
	}

	public Tweener Toggle()
	{
		if (isVisible)
		{
			return Hide();
		}
		return Show();
	}

	public Tweener Show()
	{
		isVisible = true;
		enemyVisualizer.transform.DOScale(Vector3.one, UIAnimationManager.speedMedium);
		capturePreviewDisplay.rect.DOLocalRotate(new Vector3(0f, 180f, 0f), UIAnimationManager.speedMedium).SetEase(Ease.InOutQuad);
		bool shouldSet = true;
		Tweener tween = rectStats.DOLocalRotate(new Vector3(0f, 0f, 0f), UIAnimationManager.speedMedium).SetEase(Ease.InOutQuad);
		uiCaptureAdjustments.Hide();
		tween.OnUpdate(delegate
		{
			if (tween.ElapsedPercentage() >= 0.5f && shouldSet)
			{
				shouldSet = false;
				selfRect.SetSiblingIndex(selfRect.parent.childCount - 2);
			}
		});
		return tween;
	}

	public Tweener Hide()
	{
		isVisible = false;
		enemyVisualizer.transform.DOScale(Vector3.zero, UIAnimationManager.speedMedium);
		rectStats.DOLocalRotate(new Vector3(0f, 180f, 0f), UIAnimationManager.speedMedium).SetEase(Ease.InOutQuad);
		bool shouldSet = true;
		Tweener tween = capturePreviewDisplay.rect.DOLocalRotate(new Vector3(0f, 0f, 0f), UIAnimationManager.speedMedium).SetEase(Ease.InOutQuad);
		uiCaptureAdjustments.Show();
		tween.OnUpdate(delegate
		{
			if (tween.ElapsedPercentage() >= 0.5f && shouldSet)
			{
				shouldSet = false;
				capturePreviewDisplay.rect.SetSiblingIndex(capturePreviewDisplay.rect.parent.childCount - 2);
			}
		});
		return tween;
	}
}
