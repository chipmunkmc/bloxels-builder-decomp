using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using VoxelEngine;

public class UIBrainSwap : MonoBehaviour
{
	[Header("UI")]
	public UIButton uiButtonActivate;

	public UIButton uiButtonCaptureTrigger;

	public RectTransform rectExplainer;

	public CanvasGroup uiGroupIndicator;

	public CanvasGroup uiGroupOptions;

	public UITapOverlay uiOverlay;

	public RectTransform _optionsRect;

	public float indicatorDelay = 2f;

	private RectTransform _selfRect;

	public Vector3 indicatorOffset = new Vector3(6f, 18f, 0f);

	private Vector3 _indicatorPosition;

	private GridLocation _indicatorLoc;

	private GridLocation _room;

	private GridLocation _roomToUpdate;

	public Bounds bounds;

	private UIBoard3D _uiBoard3d;

	private RectTransform _rectBoard3d;

	[Header("State Tracking")]
	public bool playerInZone;

	private bool _cameraIsOffset;

	private bool _menuActive;

	[Header("Materials")]
	public Material blockMaterial;

	public Color colorDark;

	[Header("Menu")]
	public RectTransform[] menuItems;

	private UIBrainDisplay[] _uiBrainDisplays;

	public Vector3 cameraOffset;

	private BloxelTile _enemyTile;

	private bool _artInitialized;

	private void Awake()
	{
		_selfRect = GetComponent<RectTransform>();
		_uiBoard3d = GetComponentInChildren<UIBoard3D>();
		_uiBrainDisplays = GetComponentsInChildren<UIBrainDisplay>();
		_rectBoard3d = _uiBoard3d.GetComponent<RectTransform>();
		for (int i = 0; i < menuItems.Length; i++)
		{
			menuItems[i].anchoredPosition = Vector2.zero;
		}
		_selfRect.localPosition = new Vector3(0f, 0f, 100f);
		_room = new GridLocation(5, 7);
		_indicatorLoc = new GridLocation(8 + 13 * _room.c, 3 + 13 * _room.r);
		_indicatorPosition = new Vector3(_indicatorLoc.c * 13, _indicatorLoc.r * 13);
	}

	private void Start()
	{
		uiGroupOptions.alpha = 0f;
		uiGroupIndicator.alpha = 0f;
		uiGroupIndicator.blocksRaycasts = false;
		uiGroupIndicator.interactable = false;
		uiGroupOptions.blocksRaycasts = false;
		uiGroupOptions.interactable = false;
		uiOverlay.image.raycastTarget = false;
		rectExplainer.localScale = Vector3.zero;
		_rectBoard3d.anchoredPosition = Vector2.zero;
		uiButtonActivate.OnClick += HandleIndicatorPress;
		uiOverlay.OnClick += HandleOverlayClick;
		uiButtonCaptureTrigger.OnClick += HandleCapturePress;
		CaptureManager.instance.OnCaptureConfirm += HandleCapture;
		setupBoards();
	}

	private void OnDestroy()
	{
		uiButtonActivate.OnClick -= HandleIndicatorPress;
		uiOverlay.OnClick -= HandleOverlayClick;
		uiButtonCaptureTrigger.OnClick -= HandleCapturePress;
		CaptureManager.instance.OnCaptureConfirm -= HandleCapture;
		for (int i = 0; i < _uiBrainDisplays.Length; i++)
		{
			_uiBrainDisplays[i].OnSelect -= HandleBrainSelect;
		}
	}

	private void LateUpdate()
	{
		SetPosition();
		if (!(PixelPlayerController.instance == null) && !PixelPlayerController.instance.isShrunk && !PixelPlayerController.instance.isUsingShrinkPotion && !(_enemyTile == null) && _enemyTile.tileInfo.shouldRespawn)
		{
			tryIndicator();
		}
	}

	public void SetPosition()
	{
		if (!_menuActive)
		{
			if (GameplayBuilder.instance.tiles.TryGetValue(new WorldPos2D(_indicatorLoc.c * 13, _indicatorLoc.r * 13), out _enemyTile))
			{
				Vector3 position = BloxelCamera.instance.mainCamera.WorldToScreenPoint(_enemyTile.selfTransform.position + indicatorOffset);
				_selfRect.position = UIOverlayCanvas.instance.overlayCamera.ScreenToWorldPoint(position);
			}
		}
		else
		{
			_selfRect.position = Vector3.zero;
		}
	}

	public void ResetMaterials()
	{
		blockMaterial.color = Color.white;
		LevelBuilder.instance.gameBackground.renderer.material.color = Color.white;
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.color = Color.white;
		}
	}

	private void setupBoards()
	{
		_uiBoard3d.Init(new BloxelBoard());
		BlockColor[] array = new BlockColor[4]
		{
			BlockColor.Blank,
			BlockColor.Orange,
			BlockColor.Purple,
			BlockColor.Blue
		};
		int[] array2 = new int[4] { 8, 6, 8, 8 };
		if (array.Length == _uiBrainDisplays.Length)
		{
			for (int i = 0; i < _uiBrainDisplays.Length; i++)
			{
				BloxelBoard bloxelBoard = BloxelBoard.GenerateSymmetricalBoard(new BloxelBoard(), array[i], array2[i]);
				_uiBrainDisplays[i].Init(new BloxelBrain(bloxelBoard.blockColors));
				_uiBrainDisplays[i].OnSelect += HandleBrainSelect;
			}
		}
	}

	private void tryIndicator()
	{
		uiGroupIndicator.DOKill();
		if (playerInZone)
		{
			_selfRect.localScale = Vector3.one;
			if (!_cameraIsOffset)
			{
				BloxelCamera.instance.activeController.SetTempCameraOffset(BloxelCamera.instance.activeController.cameraOffset + cameraOffset);
				_cameraIsOffset = true;
			}
			uiGroupIndicator.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				uiGroupIndicator.blocksRaycasts = true;
				uiGroupIndicator.interactable = true;
				uiGroupOptions.blocksRaycasts = true;
				uiGroupOptions.interactable = true;
			});
		}
		else
		{
			_selfRect.localScale = Vector3.zero;
			_cameraIsOffset = false;
			uiGroupIndicator.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				uiGroupIndicator.blocksRaycasts = false;
				uiGroupIndicator.interactable = false;
				uiGroupOptions.blocksRaycasts = false;
				uiGroupOptions.interactable = false;
			});
			if (_menuActive)
			{
				HandleOverlayClick();
			}
		}
	}

	public void Reset()
	{
		setupBoards();
	}

	private void killAllTweens()
	{
		blockMaterial.DOKill();
		uiGroupOptions.DOKill();
		uiOverlay.image.DOKill();
	}

	private void HandleBrainSelect(UIBrainDisplay brainDisplay)
	{
		if (((BloxelEnemyTile)_enemyTile).controller.laser != null)
		{
			((BloxelEnemyTile)_enemyTile).controller.laser.Despawn();
		}
		_uiBoard3d.Init(brainDisplay.bloxelBrain.coverBoard);
		if (_enemyTile.tileInfo.gameBehavior == GameBehavior.Enemy)
		{
			((BloxelEnemyTile)_enemyTile).controller.InitWithBrain(brainDisplay.bloxelBrain);
			((BloxelEnemyTile)_enemyTile).TryToGrow();
		}
	}

	private void HandleOverlayClick()
	{
		_menuActive = false;
		killAllTweens();
		animateMenuIn();
		uiOverlay.transform.DOScale(0f, UIAnimationManager.speedMedium);
		uiGroupOptions.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroupIndicator.blocksRaycasts = false;
			uiGroupIndicator.interactable = false;
			uiGroupOptions.blocksRaycasts = false;
			uiGroupOptions.interactable = false;
		});
		fadeInMats();
		uiButtonActivate.interactable = true;
		HomeController.instance.touchControls.transform.localScale = Vector3.one;
	}

	public void HandleIndicatorPress()
	{
		_menuActive = true;
		killAllTweens();
		uiButtonActivate.interactable = false;
		uiOverlay.transform.DOScale(1f, UIAnimationManager.speedMedium);
		uiGroupOptions.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroupIndicator.blocksRaycasts = true;
			uiGroupIndicator.interactable = true;
			uiGroupOptions.blocksRaycasts = true;
			uiGroupOptions.interactable = true;
		}).OnComplete(delegate
		{
			animateMenuOut();
		});
		fadeOutMats();
		HomeController.instance.touchControls.transform.localScale = Vector3.zero;
	}

	private void HandleCapturePress()
	{
		CaptureManager.instance.CaptureBrain(_enemyTile as BloxelEnemyTile);
	}

	private void HandleCapture()
	{
		if (_menuActive)
		{
			BloxelBrain bloxelBrain = new BloxelBrain(CaptureManager.latest);
			_uiBoard3d.Init(bloxelBrain.coverBoard);
			if (_enemyTile.tileInfo.gameBehavior == GameBehavior.Enemy)
			{
				((BloxelEnemyTile)_enemyTile).controller.InitWithBrain(bloxelBrain);
			}
			((BloxelEnemyTile)_enemyTile).TryToGrow();
		}
	}

	private void animateMenuOut()
	{
		float num = _optionsRect.sizeDelta.x / 2f;
		float num2 = (float)Math.PI * 2f / (float)menuItems.Length / 2f;
		float x = _optionsRect.sizeDelta.x;
		float y = _optionsRect.sizeDelta.y;
		float num3 = ((menuItems.Length % 2 != 0) ? (num2 / 2f) : 0f);
		for (int i = 0; i < menuItems.Length; i++)
		{
			float x2 = Mathf.Round(num * Mathf.Cos(num3));
			float y2 = Mathf.Round(num * Mathf.Sin(num3));
			menuItems[i].DOAnchorPos(new Vector2(x2, y2), UIAnimationManager.speedMedium);
			num3 += num2;
		}
		rectExplainer.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InBounce);
		uiOverlay.image.raycastTarget = true;
	}

	private void animateMenuIn()
	{
		rectExplainer.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.OutExpo).OnComplete(delegate
		{
			for (int i = 0; i < menuItems.Length; i++)
			{
				menuItems[i].DOAnchorPos(Vector2.zero, UIAnimationManager.speedMedium);
			}
		});
		uiOverlay.image.raycastTarget = false;
	}

	private void fadeOutMats()
	{
		LevelBuilder.instance.gameBackground.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		}
		Dictionary<WorldPos2D, LevelBackground>.Enumerator enumerator2 = GameplayBuilder.instance.backgrounds.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.Value.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		}
		blockMaterial.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
	}

	private void fadeInMats()
	{
		LevelBuilder.instance.gameBackground.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		}
		Dictionary<WorldPos2D, LevelBackground>.Enumerator enumerator2 = GameplayBuilder.instance.backgrounds.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.Value.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		}
		blockMaterial.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
	}
}
