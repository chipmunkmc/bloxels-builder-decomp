using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIBuilderMode : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public UIBuilderModeGroup groupParent;

	public int id;

	private Image image;

	public RectTransform rect;

	public GameBuilderMode mode;

	public bool isActive;

	public bool interactable;

	public Vector2 activePos;

	public Vector2 inActivePos;

	public CanvasGroup subModeGroup;

	public UIGameBuilderSecondaryMode[] subModes;

	public Color activeColor;

	public Color inActiveColor;

	private void Awake()
	{
		image = GetComponent<Image>();
		id = base.transform.GetSiblingIndex();
		inActivePos = rect.anchoredPosition;
	}

	private void Start()
	{
		if (id > 0)
		{
			activePos = new Vector2(base.transform.parent.GetComponent<RectTransform>().rect.width * -1f + (float)id * (rect.sizeDelta.x + 2f) + rect.sizeDelta.x, rect.anchoredPosition.y);
		}
		else
		{
			activePos = Vector2.zero;
		}
		interactable = true;
	}

	public void MoveLeft(bool shouldActivate)
	{
		if (shouldActivate)
		{
			Activate();
		}
		rect.DOAnchorPos(activePos, UIAnimationManager.speedMedium);
	}

	public void MoveRight(bool shouldActivate)
	{
		if (shouldActivate)
		{
			Activate();
		}
		rect.DOAnchorPos(inActivePos, UIAnimationManager.speedMedium);
	}

	public void Activate()
	{
		isActive = true;
		image.DOColor(activeColor, UIAnimationManager.speedFast);
		subModeGroup.gameObject.SetActive(true);
		subModeGroup.DOFade(1f, UIAnimationManager.speedFast).OnComplete(delegate
		{
		});
	}

	public void DeActivate()
	{
		isActive = false;
		image.DOColor(inActiveColor, UIAnimationManager.speedFast);
		subModeGroup.DOFade(0f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			subModeGroup.gameObject.SetActive(false);
		});
	}

	public void Select()
	{
		groupParent.SwitchMode(id);
		GameBuilderCanvas.instance.currentMode = mode;
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (interactable)
		{
			Select();
		}
	}
}
