using UnityEngine;

public class UIBuilderModeGroup : MonoBehaviour
{
	public static UIBuilderModeGroup instance;

	public UIBuilderMode[] modesInGroup;

	public RectTransform rect;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		modesInGroup = GetComponentsInChildren<UIBuilderMode>();
	}

	public void SwitchMode(int id)
	{
		UIBuilderMode[] array = modesInGroup;
		foreach (UIBuilderMode uIBuilderMode in array)
		{
			bool shouldActivate = false;
			if (uIBuilderMode.id == id)
			{
				shouldActivate = true;
			}
			else
			{
				uIBuilderMode.DeActivate();
			}
			if (uIBuilderMode.id <= id)
			{
				uIBuilderMode.MoveLeft(shouldActivate);
			}
			else if (uIBuilderMode.id >= id)
			{
				uIBuilderMode.MoveRight(shouldActivate);
			}
		}
	}
}
