using System;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Graphic))]
[RequireComponent(typeof(CanvasGroup))]
public class UIButton : MonoBehaviour, IPointerDownHandler, IPointerEnterHandler, IPointerExitHandler, IPointerUpHandler, IPointerClickHandler, IEventSystemHandler
{
	public enum Type
	{
		PositiveAction = 0,
		NegativeAction = 1
	}

	public delegate void HandleClick();

	public delegate void HandleEnter();

	public delegate void HandleExit();

	public delegate void HandleDown();

	public delegate void HandleUp();

	public delegate void HandleToggle(UIButton button);

	public delegate void HandleGemLock(UIButton button);

	[Header("UI")]
	public RectTransform selfRect;

	public CanvasGroup uiCanvasGroup;

	private TextMeshProUGUI[] _tmProText;

	[Tooltip("The primary part of the button")]
	public Image uiImageClickReceiver;

	private Color _normalColor;

	[Tooltip("This will be ignored if autoTint is enabled")]
	public Color tint;

	private Color _autoTintColor;

	[Header("State Tracking")]
	public bool shouldAutoTint;

	private bool _interactable;

	[Header("Data")]
	[Range(0f, 1f)]
	public float fadeOutAmount = 0.5f;

	[Range(0f, 1f)]
	public float fadeInAmount = 1f;

	public Type actionType;

	[Header("SFX Overrides")]
	public AudioClip onClickSFX;

	public AudioClip onToggleSFX;

	[Header("=== If Toggle ===")]
	public bool isToggle;

	public RectTransform[] toggleIndicators;

	public bool isOn;

	[Header("Gem Lock")]
	public UIGemLock uiGemLock;

	public bool interactable
	{
		get
		{
			return _interactable;
		}
		set
		{
			_interactable = value;
			if (_interactable)
			{
				FadeGraphics(fadeInAmount);
			}
			else
			{
				FadeGraphics(fadeOutAmount);
			}
			if (uiCanvasGroup != null)
			{
				uiCanvasGroup.blocksRaycasts = _interactable;
			}
		}
	}

	public event HandleClick OnClick;

	public event HandleEnter OnEnter;

	public event HandleExit OnExit;

	public event HandleDown OnDown;

	public event HandleUp OnUp;

	public event HandleToggle OnToggle;

	public event HandleGemLock OnGemLock;

	private void Awake()
	{
		selfRect = GetComponent<RectTransform>();
		_tmProText = GetComponentsInChildren<TextMeshProUGUI>();
		uiCanvasGroup = GetComponent<CanvasGroup>();
		if (uiImageClickReceiver != null)
		{
			_normalColor = uiImageClickReceiver.color;
		}
		else
		{
			_normalColor = Color.white;
		}
		_autoTintColor = new Color(_normalColor.r, _normalColor.g, _normalColor.b, _normalColor.a);
		if (!isToggle || toggleIndicators == null)
		{
		}
		if (isToggle)
		{
			isOn = false;
			uiCanvasGroup.alpha = fadeOutAmount;
			for (int i = 0; i < toggleIndicators.Length; i++)
			{
				toggleIndicators[i].localScale = Vector3.zero;
			}
		}
	}

	private void FadeGraphics(float amount)
	{
		uiCanvasGroup.DOFade(amount, UIAnimationManager.speedFast);
	}

	public void Fade(bool fullAlpha)
	{
		float num = 0f;
		num = ((!fullAlpha) ? fadeOutAmount : fadeInAmount);
		FadeGraphics(num);
	}

	private void MixColors(bool shouldMix)
	{
		if (shouldMix && shouldAutoTint)
		{
			uiImageClickReceiver.DOColor(PPUtilities.BlendColors(_normalColor, _autoTintColor), UIAnimationManager.speedMedium);
		}
		else if (shouldMix)
		{
			uiImageClickReceiver.DOColor(PPUtilities.BlendColors(_normalColor, tint), UIAnimationManager.speedMedium);
		}
		else
		{
			uiImageClickReceiver.DOColor(_normalColor, UIAnimationManager.speedMedium);
		}
	}

	private void FlipGraphic()
	{
		uiImageClickReceiver.transform.localScale = new Vector3(1f, uiImageClickReceiver.transform.localScale.y * -1f, 1f);
	}

	private Tweener ScaleAnimation(float scale)
	{
		return selfRect.DOScale(scale, UIAnimationManager.speedMedium);
	}

	public void Toggle()
	{
		if (isToggle)
		{
			if (!isOn)
			{
				Activate();
			}
			else
			{
				DeActivate();
			}
			if (this.OnToggle != null)
			{
				this.OnToggle(this);
			}
		}
	}

	public void Activate()
	{
		if (isToggle)
		{
			isOn = true;
			Fade(isOn);
			for (int i = 0; i < toggleIndicators.Length; i++)
			{
				toggleIndicators[i].DOScale(1f, UIAnimationManager.speedMedium);
			}
		}
	}

	public void DeActivate()
	{
		if (isToggle)
		{
			isOn = false;
			Fade(isOn);
			for (int i = 0; i < toggleIndicators.Length; i++)
			{
				toggleIndicators[i].DOScale(0f, UIAnimationManager.speedMedium);
			}
		}
	}

	public virtual void OnPointerEnter(PointerEventData eventData)
	{
		if (this.OnEnter != null)
		{
			this.OnEnter();
		}
	}

	public virtual void OnPointerExit(PointerEventData eventData)
	{
		if (this.OnExit != null)
		{
			this.OnExit();
		}
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		if (this.OnDown != null)
		{
			this.OnDown();
		}
		ScaleAnimation(0.95f);
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		if (this.OnUp != null)
		{
			this.OnUp();
		}
		ScaleAnimation(1f);
	}

	public virtual void OnPointerClick(PointerEventData eventData)
	{
		PlaySound();
		if (uiGemLock != null && uiGemLock.isLocked)
		{
			if (this.OnGemLock != null)
			{
				this.OnGemLock(this);
			}
		}
		else if (isToggle)
		{
			Toggle();
		}
		else if (this.OnClick != null)
		{
			this.OnClick();
		}
	}

	public virtual void PlaySound()
	{
		if (onToggleSFX != null)
		{
			SoundManager.instance.PlaySound(onToggleSFX);
		}
		else if (onClickSFX != null)
		{
			SoundManager.instance.PlaySound(onClickSFX);
		}
		else if (isToggle)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.popA);
		}
		else if (actionType == Type.NegativeAction)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.popB);
		}
		else
		{
			SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		}
	}
}
