using System;
using System.Threading;
using UnityEngine;

public class UIButtonGroup : MonoBehaviour
{
	public delegate void HandleChange(UIButton btn);

	public UIButton[] buttons;

	public UIButton activeButton;

	public int startingIndex;

	public event HandleChange OnChange;

	private void Awake()
	{
		buttons = GetComponentsInChildren<UIButton>();
	}

	private void OnDestroy()
	{
		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].OnToggle -= ToggleSelect;
		}
	}

	public void InitToggles()
	{
		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].isToggle = true;
			if (i == startingIndex)
			{
				buttons[i].Activate();
			}
			else
			{
				buttons[i].DeActivate();
			}
			buttons[i].OnToggle += ToggleSelect;
		}
	}

	public void Reset()
	{
		for (int i = 0; i < buttons.Length; i++)
		{
			buttons[i].isToggle = true;
			buttons[i].DeActivate();
			buttons[i].OnToggle += ToggleSelect;
		}
	}

	private void SetActiveButton(UIButton btn)
	{
		activeButton = btn;
		if (this.OnChange != null)
		{
			this.OnChange(btn);
		}
	}

	public void ToggleSelect(UIButton btn)
	{
		for (int i = 0; i < buttons.Length; i++)
		{
			if (buttons[i] != btn)
			{
				buttons[i].DeActivate();
			}
		}
		SetActiveButton(btn);
	}
}
