using UnityEngine;
using UnityEngine.EventSystems;

public class UIButtonSwap : UIButton, IPointerDownHandler, IPointerUpHandler, IEventSystemHandler
{
	[Header("UI Button Swap")]
	public bool isPushButton = true;

	public RectTransform rectGraphic;

	public Sprite iconDownState;

	public Sprite iconUpState;

	public Vector2 graphicUpPosition;

	public Vector2 graphicDownPosition;

	public override void OnPointerDown(PointerEventData eventData)
	{
		uiImageClickReceiver.sprite = iconDownState;
		if (isPushButton)
		{
			rectGraphic.anchoredPosition = graphicDownPosition;
		}
		else
		{
			base.OnPointerDown(eventData);
		}
	}

	public override void OnPointerUp(PointerEventData eventData)
	{
		uiImageClickReceiver.sprite = iconUpState;
		if (isPushButton)
		{
			rectGraphic.anchoredPosition = graphicUpPosition;
		}
		else
		{
			base.OnPointerDown(eventData);
		}
	}
}
