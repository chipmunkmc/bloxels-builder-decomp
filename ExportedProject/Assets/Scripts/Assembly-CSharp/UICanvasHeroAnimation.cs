using DG.Tweening;
using UnityEngine;

public class UICanvasHeroAnimation : MonoBehaviour
{
	public CanvasSquare square;

	public UIHero2D hero2D;

	public RectTransform rectContainer;

	public RectTransform startBoundary;

	public RectTransform endBoundary;

	public RectTransform centerBoundaryRight;

	public RectTransform centerBoundaryLeft;

	public Tweener patrolRightTween1;

	public Tweener patrolRightTween2;

	public Tweener patrolLeftTween1;

	public Tweener patrolLeftTween2;

	public Tweener jumpTween;

	public Vector2 jumpTrajectory;

	public float jumpSpeed;

	private bool hasKilled;

	private void Awake()
	{
		base.gameObject.SetActive(false);
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		BloxelGame bloxelGame = square.project as BloxelGame;
		hero2D.Init(bloxelGame.hero, this);
		hero2D.Look(Direction.Right);
		hero2D.Walk();
		base.gameObject.SetActive(true);
		PatrolRightFirst();
	}

	private void PatrolRightFirst()
	{
		hero2D.Look(Direction.Right);
		patrolRightTween1 = hero2D.rect.DOAnchorPos(centerBoundaryRight.anchoredPosition, UIAnimationManager.heroPatrolSpeed).OnComplete(delegate
		{
			Jump();
			PatrolRightSecond();
		}).SetEase(Ease.Linear);
	}

	private void PatrolRightSecond()
	{
		patrolRightTween2 = hero2D.rect.DOAnchorPos(endBoundary.anchoredPosition, UIAnimationManager.heroPatrolSpeed).OnComplete(delegate
		{
			PatrolLeftFirst();
		}).SetEase(Ease.Linear);
	}

	public void Jump(bool shouldShake = true)
	{
		hero2D.Jump();
		jumpTween = rectContainer.DOAnchorPos(rectContainer.anchoredPosition + jumpTrajectory, jumpSpeed).SetLoops(2, LoopType.Yoyo).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.playerJump);
		})
			.OnComplete(delegate
			{
				hero2D.Walk();
			})
			.SetEase(Ease.OutQuad);
		if (shouldShake)
		{
			Invoke("ShakePlay", jumpSpeed);
		}
	}

	private void ShakePlay()
	{
		CanvasTileInfo.instance.ShakePlay();
	}

	private void PatrolLeftFirst()
	{
		hero2D.Look(Direction.Left);
		patrolLeftTween1 = hero2D.rect.DOAnchorPos(centerBoundaryLeft.anchoredPosition, UIAnimationManager.heroPatrolSpeed).OnComplete(delegate
		{
			Jump();
			PatrolLeftSecond();
		}).SetEase(Ease.Linear);
	}

	private void PatrolLeftSecond()
	{
		patrolLeftTween2 = hero2D.rect.DOAnchorPos(startBoundary.anchoredPosition, UIAnimationManager.heroPatrolSpeed).OnComplete(delegate
		{
			PatrolRightFirst();
		}).SetEase(Ease.Linear);
	}

	public void Reset()
	{
		hero2D.Stop();
		hero2D.Clear();
		patrolRightTween1.Kill();
		patrolLeftTween1.Kill();
		patrolLeftTween2.Kill();
		patrolRightTween2.Kill();
		jumpTween.Kill();
		rectContainer.anchoredPosition = new Vector2(0f, 100f);
		hero2D.rect.anchoredPosition = startBoundary.anchoredPosition;
		base.gameObject.SetActive(false);
	}
}
