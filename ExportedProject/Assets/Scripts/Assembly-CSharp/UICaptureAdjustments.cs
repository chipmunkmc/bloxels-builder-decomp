using System.Collections;
using DG.Tweening;
using DiscoCapture;
using UnityEngine;
using UnityEngine.UI;

public class UICaptureAdjustments : MonoBehaviour
{
	public static UICaptureAdjustments CaptureAdjustments;

	public MenuCaptureResults controller;

	[Header("UI")]
	public RectTransform selfRect;

	public RectTransform rectSettingsInstructions;

	public RectTransform rectInstructionsArrowLeft;

	public RectTransform rectInstructionsArrowRight;

	public RectTransform rectWarmth;

	public RectTransform rectBrightness;

	public UIButton uiButtonWarmthAdd;

	public UIButton uiButtonWarmthSubtract;

	public UIButton uiButtonBrightnessAdd;

	public UIButton uiButtonBrightnessSubtract;

	public Image[] warmthImages;

	public Image[] brightnessImages;

	public RectTransform rectFillWarm;

	public RectTransform rectFillWhite;

	public Color maxWarm;

	[HideInInspector]
	public float minWarmthAmount;

	[HideInInspector]
	public float maxWarmthAmount;

	private float _minBrightness = 1f;

	private float _maxBrightness = 12f;

	private float _currentBrightness;

	private Vector3 _arrowLeftScale;

	private Vector3 _arrowRightScale;

	[Header("Data")]
	public bool isVisible;

	public bool instructionsVisible;

	private bool warmthButtonPressed;

	private void Awake()
	{
		selfRect = GetComponent<RectTransform>();
		uiButtonWarmthAdd.OnClick += handleAddWarmth;
		uiButtonWarmthSubtract.OnClick += handleSubtractWarmth;
		uiButtonBrightnessAdd.OnClick += handleAddBrightness;
		uiButtonBrightnessSubtract.OnClick += handleSubtractBrightness;
		CaptureController.Instance.AddListener(CaptureEvent.ClassificationComplete, handleClassificationComplete);
		CaptureManager.instance.OnCaptureRepeat += handleRepeat;
		_arrowLeftScale = rectInstructionsArrowLeft.localScale;
		_arrowRightScale = rectInstructionsArrowRight.localScale;
		Reset();
	}

	private void OnDestroy()
	{
		uiButtonWarmthAdd.OnClick -= handleAddWarmth;
		uiButtonWarmthSubtract.OnClick -= handleSubtractWarmth;
		uiButtonBrightnessAdd.OnClick -= handleAddBrightness;
		uiButtonBrightnessSubtract.OnClick -= handleSubtractBrightness;
		CaptureManager.instance.OnCaptureRepeat -= handleRepeat;
		CaptureController.Instance.RemoveListener(CaptureEvent.ClassificationComplete, handleClassificationComplete);
	}

	private void handleRepeat()
	{
		warmthButtonPressed = false;
		uiButtonBrightnessAdd.interactable = true;
		uiButtonBrightnessSubtract.interactable = true;
		uiButtonWarmthAdd.interactable = true;
		uiButtonWarmthSubtract.interactable = true;
		ColorClassifierData.Thresholds.currentWhiteSatOffset = 0;
		ColorClassifierData.Thresholds.AdjustWhiteSatThresholds(0);
		CaptureController.Instance.StopClassification();
		Hide(true);
	}

	private void handleClassificationComplete()
	{
		StartCoroutine(Init());
	}

	public void Reset()
	{
		selfRect.localScale = Vector3.zero;
		rectWarmth.localScale = Vector3.zero;
		rectBrightness.localScale = Vector3.zero;
		rectSettingsInstructions.localScale = Vector3.zero;
		rectInstructionsArrowLeft.localScale = Vector3.zero;
		rectInstructionsArrowRight.localScale = Vector3.zero;
	}

	public IEnumerator Init()
	{
		if (!warmthButtonPressed)
		{
			yield return new WaitForSeconds(2f);
			CaptureController.Instance.SetNotchRange(out minWarmthAmount, out maxWarmthAmount);
			_currentBrightness = _maxBrightness / 2f;
			SetWarmth(0f);
			SetBrightness(_currentBrightness);
			selfRect.localScale = Vector3.one;
			if (!((UIPopupCapture)controller.controller).isBrainCapture)
			{
				Show();
			}
		}
	}

	public Sequence Show(bool force = false)
	{
		if (isVisible && !force)
		{
			return null;
		}
		isVisible = true;
		if (!instructionsVisible)
		{
			StartCoroutine("delayedInstructions");
		}
		Sequence sequence = DOTween.Sequence();
		sequence.Append(rectWarmth.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce));
		sequence.Append(rectBrightness.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce));
		sequence.Play();
		return sequence;
	}

	public Sequence Hide(bool force = false)
	{
		if (!isVisible && !force)
		{
			return null;
		}
		isVisible = false;
		if (instructionsVisible)
		{
			StopCoroutine("delayedInstructions");
			rectSettingsInstructions.localScale = Vector3.zero;
			rectInstructionsArrowLeft.localScale = Vector3.zero;
			rectInstructionsArrowRight.localScale = Vector3.zero;
			instructionsVisible = false;
		}
		Sequence sequence = DOTween.Sequence();
		sequence.Append(rectWarmth.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce));
		sequence.Append(rectBrightness.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce));
		sequence.Play();
		return sequence;
	}

	private IEnumerator delayedInstructions()
	{
		instructionsVisible = true;
		yield return new WaitForSeconds(5f);
		rectSettingsInstructions.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
		rectInstructionsArrowLeft.DOScale(_arrowLeftScale, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce).OnComplete(delegate
		{
			rectInstructionsArrowLeft.DOPunchScale(_arrowLeftScale - new Vector3(-0.9f, 0.9f, 0f), UIAnimationManager.speedSlow, 0, 0f).SetLoops(-1, LoopType.Yoyo);
		});
		rectInstructionsArrowRight.DOScale(_arrowRightScale, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce).OnComplete(delegate
		{
			rectInstructionsArrowRight.DOPunchScale(_arrowRightScale - new Vector3(0.9f, 0.9f, 0f), UIAnimationManager.speedSlow, 0, 0f).SetLoops(-1, LoopType.Yoyo);
		});
		yield return new WaitForSeconds(5f);
		rectInstructionsArrowLeft.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
		rectInstructionsArrowRight.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
		rectSettingsInstructions.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
		instructionsVisible = false;
	}

	private void handleAddWarmth()
	{
		warmthButtonPressed = true;
		uiButtonWarmthSubtract.interactable = true;
		controller.previewDisplay.MarkAllUnclassified();
		CaptureController.Instance.IncrementTempOffset();
		adjustWarmthGraphics(CaptureController.Instance.cctNotchOffset);
		if (CaptureController.Instance.cctIncreaseLimitReached)
		{
			uiButtonWarmthAdd.interactable = false;
		}
		AnalyticsManager.SendEventBool(AnalyticsManager.Event.AdjustWarmth, true);
	}

	private void handleSubtractWarmth()
	{
		warmthButtonPressed = true;
		uiButtonWarmthAdd.interactable = true;
		controller.previewDisplay.MarkAllUnclassified();
		CaptureController.Instance.DecrementTempOffset();
		adjustWarmthGraphics(CaptureController.Instance.cctNotchOffset);
		if (CaptureController.Instance.cctDecreaseLimitReached)
		{
			uiButtonWarmthSubtract.interactable = false;
		}
		AnalyticsManager.SendEventBool(AnalyticsManager.Event.AdjustWarmth, true);
	}

	private void handleAddBrightness()
	{
		uiButtonBrightnessSubtract.interactable = true;
		bool flag = CaptureController.Instance.IncrementWhiteSatThresh();
		adjustBrightnessGraphics(_currentBrightness + 1f);
		if (!flag || _currentBrightness + 1f == _maxBrightness)
		{
			adjustBrightnessGraphics(_maxBrightness);
			uiButtonBrightnessAdd.interactable = false;
		}
		else
		{
			_currentBrightness += 1f;
		}
		AnalyticsManager.SendEventBool(AnalyticsManager.Event.AdjustBrightness, true);
	}

	private void handleSubtractBrightness()
	{
		uiButtonBrightnessAdd.interactable = true;
		bool flag = CaptureController.Instance.DecrementWhiteSatThresh();
		adjustBrightnessGraphics(_currentBrightness - 1f);
		if (!flag || _currentBrightness - 1f == 0f)
		{
			adjustBrightnessGraphics(0f);
			uiButtonBrightnessSubtract.interactable = false;
		}
		else
		{
			_currentBrightness -= 1f;
		}
		AnalyticsManager.SendEventBool(AnalyticsManager.Event.AdjustBrightness, true);
	}

	public void SetWarmth(float val)
	{
		adjustWarmthGraphics(val);
	}

	public void SetBrightness(float val)
	{
		adjustBrightnessGraphics(val);
	}

	private Sequence adjustWarmthGraphics(float val)
	{
		float num = (val - minWarmthAmount) / (maxWarmthAmount - minWarmthAmount);
		Color endValue = Color.Lerp(Color.white, maxWarm, num);
		rectFillWarm.localScale = new Vector3(1f, num, 1f);
		Sequence sequence = DOTween.Sequence();
		for (int i = 0; i < warmthImages.Length; i++)
		{
			Image target = warmthImages[i];
			sequence.Append(target.DOColor(endValue, UIAnimationManager.speedFast));
		}
		sequence.Play();
		return sequence;
	}

	private Sequence adjustBrightnessGraphics(float val)
	{
		float num = val / _maxBrightness;
		rectFillWhite.localScale = new Vector3(1f, num, 1f);
		Sequence sequence = DOTween.Sequence();
		for (int i = 0; i < brightnessImages.Length; i++)
		{
			Image target = brightnessImages[i];
			sequence.Append(target.DOFade(num, UIAnimationManager.speedFast));
		}
		sequence.Play();
		return sequence;
	}
}
