using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DiscoCapture;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UICaptureData : MonoBehaviour
{
	public UIPopupCapture capturePopup;

	public UICaptureTemplatePicker templatePicker;

	public GameObject pickerPrefab;

	public RawImage uiRawImageTemplateDisplay;

	public Button uiButtonSelectTemplate;

	public UIButton uiButtonRotateSelection;

	public UIButton uiButtonUploadData;

	public TMP_Text noTemplateText;

	private BloxelBoard _selectedTemplate;

	private Texture2D _displayTexture;

	private BlockColor[,] blockColorBuffer = new BlockColor[13, 13];

	private void Awake()
	{
		if (S3Interface.instance == null)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else if (!S3Interface.instance.CanUploadCapture())
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		_selectedTemplate = null;
		uiButtonSelectTemplate.onClick.AddListener(CreatePicker);
		uiButtonRotateSelection.OnClick += RotateCounterClockwise;
		uiButtonUploadData.OnClick += UploadDataToS3;
		CaptureController.Instance.AddListener(CaptureEvent.ProcessComplete, ShowSubmit);
		CaptureManager.instance.OnCaptureRepeat += HideSubmit;
		InitDisplay();
	}

	private void OnDestroy()
	{
		if (templatePicker != null)
		{
			templatePicker.OnProjectSelect -= HandleTemplateSelected;
		}
		UnityEngine.Object.Destroy(_displayTexture);
		uiButtonRotateSelection.OnClick -= RotateCounterClockwise;
		uiButtonUploadData.OnClick -= UploadDataToS3;
		CaptureController.Instance.RemoveListener(CaptureEvent.ProcessComplete, ShowSubmit);
		CaptureManager.instance.OnCaptureRepeat -= HideSubmit;
	}

	private void InitDisplay()
	{
		_displayTexture = new Texture2D(13, 13, TextureFormat.RGBA32, false);
		_displayTexture.filterMode = FilterMode.Point;
		_displayTexture.wrapMode = TextureWrapMode.Clamp;
		uiRawImageTemplateDisplay.texture = _displayTexture;
		UpdateDisplayTexture();
	}

	private void UpdateDisplayTexture()
	{
		if (_selectedTemplate == null)
		{
			_displayTexture.SetPixels(AssetManager.instance.clearColors13x13);
		}
		else
		{
			_selectedTemplate.UpdateTexture2D(ref AssetManager.instance.colorBuffer13x13, ref _displayTexture, new CoverStyle(Color.black, 0, 1, 0, true));
		}
		_displayTexture.Apply();
	}

	public void CreatePicker()
	{
		if (!(templatePicker != null) || !templatePicker.gameObject.activeSelf)
		{
			if (templatePicker == null)
			{
				GameObject gameObject = UnityEngine.Object.Instantiate(pickerPrefab);
				templatePicker = gameObject.GetComponent<UICaptureTemplatePicker>();
				templatePicker.selfRect.SetParent(base.transform);
				templatePicker.selfRect.anchoredPosition = new Vector2(300f, 0f);
				templatePicker.selfRect.localScale = Vector3.one;
			}
			templatePicker.Init();
			templatePicker.OnProjectSelect += HandleTemplateSelected;
			templatePicker.gameObject.SetActive(true);
		}
	}

	public void ShowSubmit()
	{
		uiButtonUploadData.gameObject.SetActive(true);
	}

	public void HideSubmit()
	{
		uiButtonUploadData.gameObject.SetActive(false);
	}

	public void HandleTemplateSelected(BloxelProject selection)
	{
		_selectedTemplate = selection as BloxelBoard;
		if (templatePicker.projectData[0].id.Equals(_selectedTemplate.id))
		{
			noTemplateText.gameObject.SetActive(true);
			_selectedTemplate = null;
		}
		else if (noTemplateText.gameObject.activeSelf)
		{
			noTemplateText.gameObject.SetActive(false);
		}
		UpdateDisplayTexture();
		templatePicker.OnProjectSelect -= HandleTemplateSelected;
		templatePicker.gameObject.SetActive(false);
		if (!CaptureManager.instance.shouldProcess)
		{
			capturePopup.menuCaptureViewport.uiProgressBar.Reset();
			CaptureManager.instance.ResetForNewTemplate();
		}
	}

	private void RotateCounterClockwise()
	{
		int num = 12;
		int num2 = 0;
		while (num >= 0)
		{
			int num3 = 0;
			int num4 = 0;
			while (num3 < 13)
			{
				blockColorBuffer[num2, num4] = _selectedTemplate.blockColors[num3, num];
				num3++;
				num4++;
			}
			num--;
			num2++;
		}
		Array.Copy(blockColorBuffer, _selectedTemplate.blockColors, 169);
		UpdateDisplayTexture();
	}

	public void UploadDataToS3()
	{
		if (!S3Interface.instance.CanUploadCapture())
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else
		{
			StartCoroutine("UploadRoutine");
		}
	}

	public IEnumerator UploadRoutine()
	{
		uiButtonUploadData.interactable = false;
		uiButtonSelectTemplate.interactable = false;
		CapturedFrameMetaData metaData = new CapturedFrameMetaData();
		if (_selectedTemplate != null)
		{
			metaData.SetBlockColors(_selectedTemplate.blockColors);
		}
		Dictionary<int, byte[]>.Enumerator dataEnumerator = CaptureController.Instance.imageCache.GetEnumerator();
		while (dataEnumerator.MoveNext())
		{
			int timestamp = dataEnumerator.Current.Key;
			byte[] imageData = dataEnumerator.Current.Value;
			yield return StartCoroutine(S3Interface.instance.UploadCaptureScreen(timestamp, imageData, metaData));
		}
		CaptureController.Instance.ClearImageCache();
		RectTransform selfRect = base.transform as RectTransform;
		selfRect.SetAsFirstSibling();
		selfRect.DOAnchorPos(new Vector2(-60f, selfRect.anchoredPosition.y), UIAnimationManager.speedMedium).SetEase(Ease.InSine).OnComplete(delegate
		{
			UnityEngine.Object.Destroy(base.gameObject, 0.1f);
		});
	}
}
