using System;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UICaptureFlipToggle : MonoBehaviour, IPointerDownHandler, IEventSystemHandler
{
	public delegate void HandleToggle(bool flip);

	public RectTransform rectHandle;

	public bool flip;

	public Image theImage;

	public Sprite spriteFlip;

	private UIButton _uiButtonHandle;

	public event HandleToggle OnToggle;

	private void Awake()
	{
		_uiButtonHandle = rectHandle.GetComponent<UIButton>();
	}

	private void Start()
	{
		flip = false;
		theImage.sprite = spriteFlip;
		_uiButtonHandle.OnDown += Toggle;
	}

	private void OnDestroy()
	{
		_uiButtonHandle.OnDown -= Toggle;
	}

	public void Toggle()
	{
		flip = !flip;
		if (this.OnToggle != null)
		{
			this.OnToggle(flip);
		}
	}

	public void OnPointerDown(PointerEventData eData)
	{
		Toggle();
	}
}
