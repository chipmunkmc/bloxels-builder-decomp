using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UICaptureModeToggle : MonoBehaviour, IPointerDownHandler, IEventSystemHandler
{
	public delegate void HandleToggle(bool _isGrid);

	public RectTransform rectHandle;

	public bool isGrid = true;

	public Image theImage;

	public Sprite spriteGrid;

	public Sprite spriteEdge;

	public CanvasGroup textEdge;

	public CanvasGroup textGrid;

	public Vector2 positionEdge;

	public Vector2 positionGrid;

	private UIButton _uiButtonHandle;

	public event HandleToggle OnToggle;

	private void Awake()
	{
		_uiButtonHandle = rectHandle.GetComponent<UIButton>();
	}

	private void Start()
	{
		isGrid = true;
		theImage.sprite = spriteGrid;
		rectHandle.anchoredPosition = positionGrid;
		textGrid.alpha = 1f;
		textEdge.alpha = 0.5f;
		_uiButtonHandle.OnDown += Toggle;
	}

	private void OnDestroy()
	{
		_uiButtonHandle.OnDown -= Toggle;
	}

	public void Toggle()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		if (isGrid)
		{
			Edges();
		}
		else
		{
			Grid();
		}
		if (this.OnToggle != null)
		{
			this.OnToggle(isGrid);
		}
	}

	public void Grid()
	{
		isGrid = true;
		theImage.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			theImage.sprite = spriteGrid;
		}).OnComplete(delegate
		{
			theImage.DOFade(1f, UIAnimationManager.speedMedium);
		});
		textGrid.DOFade(1f, UIAnimationManager.speedMedium);
		textEdge.DOFade(0.5f, UIAnimationManager.speedMedium);
		rectHandle.DOAnchorPos(positionGrid, UIAnimationManager.speedMedium);
	}

	public void Edges()
	{
		isGrid = false;
		theImage.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			theImage.sprite = spriteEdge;
		}).OnComplete(delegate
		{
			theImage.DOFade(1f, UIAnimationManager.speedMedium);
		});
		textGrid.DOFade(0.5f, UIAnimationManager.speedMedium);
		textEdge.DOFade(1f, UIAnimationManager.speedMedium);
		rectHandle.DOAnchorPos(positionEdge, UIAnimationManager.speedMedium);
	}

	public void OnPointerDown(PointerEventData eData)
	{
		Toggle();
	}
}
