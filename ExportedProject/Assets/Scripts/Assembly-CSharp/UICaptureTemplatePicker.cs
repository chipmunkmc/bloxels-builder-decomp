using UnityEngine;

public class UICaptureTemplatePicker : UIProjectPicker
{
	private const string CaptureTemplatePath = "CaptureTemplates";

	public override void Init()
	{
		enhancedScroller.Delegate = this;
		projectData.Clear();
		BlockColorArray[] array = Resources.LoadAll<BlockColorArray>("CaptureTemplates");
		for (int i = 0; i < array.Length; i++)
		{
			BlockColor[,] array2 = new BlockColor[13, 13];
			array[i].CopyBlockColorsFromCurrentData(array2);
			projectData.Add(new BloxelBoard(array2, false));
		}
	}

	public void OnEnable()
	{
		CaptureManager.instance.shouldProcess = false;
	}

	public void OnDisable()
	{
		CaptureManager.instance.shouldProcess = true;
	}
}
