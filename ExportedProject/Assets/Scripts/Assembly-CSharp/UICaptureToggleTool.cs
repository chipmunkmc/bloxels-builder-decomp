using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class UICaptureToggleTool : UIButton
{
	public static UICaptureToggleTool instance;

	public RectTransform rect;

	public bool isVisible;

	public Vector2 positionVisible = new Vector2(-166f, -816f);

	public Vector2 positionHidden = new Vector2(0f, -816f);

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
	}

	private void Start()
	{
		if ((bool)PixelEditorPaintBoard.instance)
		{
			PixelEditorPaintBoard.instance.OnVisibilityChange += HandleOnVisibilityChange;
		}
	}

	private void OnDestroy()
	{
		if ((bool)PixelEditorPaintBoard.instance)
		{
			PixelEditorPaintBoard.instance.OnVisibilityChange -= HandleOnVisibilityChange;
		}
	}

	public void HandleOnVisibilityChange(bool boardVisible)
	{
		if (boardVisible)
		{
			Show();
		}
		else
		{
			Hide();
		}
	}

	public void Hide()
	{
		if (isVisible)
		{
			rect.DOAnchorPos(positionHidden, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = false;
			});
		}
	}

	public void Show()
	{
		if (!isVisible)
		{
			rect.DOAnchorPos(positionVisible, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = true;
			});
		}
	}

	public override void OnPointerClick(PointerEventData eventData)
	{
		base.OnPointerClick(eventData);
		CaptureManager.instance.Show();
	}
}
