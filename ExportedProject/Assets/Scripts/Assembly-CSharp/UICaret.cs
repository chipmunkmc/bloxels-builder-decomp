using UnityEngine;
using UnityEngine.UI;

public class UICaret : MonoBehaviour
{
	public CaretDirection caretDirection;

	public Image uiImage;

	public RectTransform rect;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		uiImage = GetComponent<Image>();
		uiImage.enabled = false;
	}

	public void Enable()
	{
		uiImage.enabled = true;
	}

	public void Disable()
	{
		uiImage.enabled = false;
	}
}
