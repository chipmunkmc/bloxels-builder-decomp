using System;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class UIChallengeHeader : MonoBehaviour
{
	public delegate void HandleOnQuit();

	public delegate void HandleInfo();

	[Header("UI")]
	public RectTransform rectText;

	public RectTransform rectButton;

	public RectTransform rectButtonInfo;

	private RectTransform _rect;

	private UIButton _uiButtonDismiss;

	private UIButton _uiButtonInfo;

	private TextMeshProUGUI _uiTextTitle;

	private float _hiddenPosition = 140f;

	private float _visiblePosition = 20f;

	private string _titleString;

	private Sequence _introSequence;

	public ParticleSystem ps;

	public event HandleOnQuit OnQuit;

	public event HandleInfo OnInfo;

	private void Awake()
	{
		_rect = GetComponent<RectTransform>();
		_uiButtonDismiss = rectButton.GetComponent<UIButton>();
		_uiButtonInfo = rectButtonInfo.GetComponent<UIButton>();
		_uiTextTitle = rectText.GetComponent<TextMeshProUGUI>();
		_uiButtonDismiss.OnClick += DismissPressed;
		_uiButtonInfo.OnClick += InfoPressed;
		Setup();
	}

	private void Start()
	{
		PlayIntroSequence();
	}

	private void OnDestroy()
	{
		_uiButtonDismiss.OnClick -= DismissPressed;
		_uiButtonInfo.OnClick -= InfoPressed;
	}

	private void Setup()
	{
		_rect.sizeDelta = new Vector2(52f, 140f);
		_rect.anchoredPosition = new Vector2(0f, _hiddenPosition);
		rectButton.localScale = new Vector3(0f, 1f, 1f);
		rectText.localScale = Vector3.zero;
		_titleString = ChallengeManager.instance.GetHeaderForLatest();
		_uiTextTitle.text = string.Empty;
		rectButtonInfo.localScale = new Vector3(0f, 1f, 1f);
	}

	private void PlayIntroSequence()
	{
		_introSequence = DOTween.Sequence();
		_uiTextTitle.SetText(_titleString);
		_introSequence.Append(_rect.DOSizeDelta(new Vector2(52f, _hiddenPosition), UIAnimationManager.speedFast).SetEase(Ease.OutQuad));
		_introSequence.Append(_rect.DOAnchorPos(new Vector2(0f, _visiblePosition), UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		_introSequence.Append(rectText.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutElastic));
		_introSequence.Append(rectButton.DOScaleX(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		_introSequence.Play();
	}

	public void ShowInfoButton()
	{
		rectButtonInfo.DOScaleX(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}).OnComplete(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
			ps.Play();
			rectButtonInfo.DOShakeScale(UIAnimationManager.speedSlow, 2f);
		});
	}

	public void HideInfoButton()
	{
		rectButtonInfo.DOScaleX(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo);
	}

	private void InfoPressed()
	{
		if (this.OnInfo != null)
		{
			this.OnInfo();
		}
	}

	private void DismissPressed()
	{
		if (this.OnQuit != null)
		{
			this.OnQuit();
		}
		Dismiss();
	}

	public void Dismiss()
	{
		_rect.DOAnchorPos(new Vector2(0f, _hiddenPosition), UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnComplete(delegate
		{
			ChallengeManager.instance.QuitLatest();
			UnityEngine.Object.Destroy(base.gameObject);
		});
	}
}
