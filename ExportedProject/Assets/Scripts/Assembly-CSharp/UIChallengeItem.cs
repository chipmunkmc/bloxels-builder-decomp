using System;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIChallengeItem : MonoBehaviour
{
	public delegate void HandleSelect(UIChallengeItem challenge);

	public Challenge type;

	[Header("UI")]
	public Image uiImage;

	public UIButton uiButtonGo;

	public RectTransform rectLock;

	public RectTransform badgeRect;

	private Image _uiImageBadge;

	[Header("State Tracking")]
	public bool interactable;

	public event HandleSelect OnSelect;

	private void Awake()
	{
		uiButtonGo.OnClick += PlayChallenge;
		_uiImageBadge = badgeRect.GetComponent<Image>();
		badgeRect.transform.localScale = Vector3.zero;
		if (ChallengeManager.instance.completedChallenges.Contains(type))
		{
			Completed();
		}
	}

	private void OnDestroy()
	{
		uiButtonGo.OnClick -= PlayChallenge;
	}

	public void Init()
	{
		interactable = true;
		rectLock.localScale = Vector3.zero;
		uiImage.color = Color.white;
	}

	public Tweener Unlock()
	{
		return rectLock.DOLocalRotate(new Vector3(0f, 0f, 180f), UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnStart(delegate
		{
			interactable = true;
			rectLock.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo);
			SoundManager.instance.PlaySound(SoundManager.instance.challengeUnlock);
		})
			.OnComplete(delegate
			{
				uiButtonGo.GetComponentInChildren<TextMeshProUGUI>().SetText(ChallengeManager.instance.GetNameFromType(type));
				uiImage.DOFade(1f, UIAnimationManager.speedMedium);
			});
	}

	public Tweener Lock()
	{
		return rectLock.DOLocalRotate(new Vector3(0f, 0f, 0f), UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnStart(delegate
		{
			interactable = false;
			rectLock.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo);
		})
			.OnComplete(delegate
			{
				uiButtonGo.GetComponentInChildren<TextMeshProUGUI>().SetText("Locked");
				uiImage.DOFade(0.5f, UIAnimationManager.speedMedium);
			});
	}

	public void PlayChallenge()
	{
		if (!interactable)
		{
			uiButtonGo.uiImageClickReceiver.DOColor(Color.red, UIAnimationManager.speedFast).OnStart(delegate
			{
				SoundManager.instance.PlaySound(SoundManager.instance.popB);
			}).OnComplete(delegate
			{
				uiButtonGo.uiImageClickReceiver.DOColor(Color.white, UIAnimationManager.speedFast);
			});
		}
		else
		{
			ChallengeManager.instance.ActivateChallenge(type);
			if (this.OnSelect != null)
			{
				this.OnSelect(this);
			}
		}
	}

	public void Completed()
	{
		badgeRect.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			badgeRect.DOShakeScale(UIAnimationManager.speedSlow);
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}).SetDelay(UIAnimationManager.speedMedium);
	}
}
