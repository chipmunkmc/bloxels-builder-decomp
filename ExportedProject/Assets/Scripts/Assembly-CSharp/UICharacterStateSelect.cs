using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class UICharacterStateSelect : MonoBehaviour
{
	public delegate void HandleStateSelect(AnimationType _state);

	[Header("| ========= Data ========= |")]
	public AnimationType animationState;

	[Header("| ========= UI ========= |")]
	public Button uiButton;

	public Color activeColor;

	public Color inactiveColor;

	public event HandleStateSelect OnSelect;

	private void Start()
	{
		uiButton.onClick.AddListener(Select);
	}

	public void Select()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.flip);
		if (this.OnSelect != null)
		{
			this.OnSelect(animationState);
		}
	}
}
