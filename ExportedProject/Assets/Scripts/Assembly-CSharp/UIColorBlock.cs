using System;
using System.Threading;
using UnityEngine.UI;

public class UIColorBlock : UIInteractable
{
	public new delegate void HandleClick(UIColorBlock block);

	public Image image;

	public Image logo;

	public Text text;

	public PaletteColor paletteColor;

	public event HandleClick OnPress;

	private new void Awake()
	{
		logo.enabled = false;
	}

	private void Start()
	{
		base.OnClick += HandlePress;
	}

	private void OnDestroy()
	{
		base.OnClick -= HandlePress;
	}

	private void HandlePress()
	{
		if (this.OnPress != null)
		{
			this.OnPress(this);
		}
	}

	public void Init(PaletteColor color)
	{
		paletteColor = color;
		string[] array = color.ToString().Split('_');
		string text = array[1];
		string text2 = array[2];
		string text3 = array[3];
		this.text.text = text + "," + text2 + "," + text3;
		image.color = ColorUtilities.PaletteColorToUnityColor(color);
	}
}
