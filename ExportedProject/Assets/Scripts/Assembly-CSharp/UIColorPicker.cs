using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

public class UIColorPicker : UIScrollTipController, IEnhancedScrollerDelegate
{
	public delegate void HandleColorSelect(UIColorScrollElement _element);

	[Header("Data")]
	public List<PaletteColor> data = new List<PaletteColor>(64);

	[Header("Prefabs")]
	public EnhancedScrollerCellView scrollElementPrefab;

	private UIPopupGemUnlock _popup;

	public event HandleColorSelect OnColorSelect;

	private new void Awake()
	{
		base.Awake();
	}

	public int GetNumberOfCells(EnhancedScroller scroller)
	{
		return data.Count;
	}

	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		return 148f;
	}

	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
	{
		UIColorScrollElement uIColorScrollElement = scroller.GetCellView(scrollElementPrefab) as UIColorScrollElement;
		uIColorScrollElement.controller = this;
		uIColorScrollElement.Init(data[dataIndex]);
		uIColorScrollElement.OnClick -= HandleOnClick;
		uIColorScrollElement.OnClick += HandleOnClick;
		return uIColorScrollElement;
	}

	private void HandleOnClick(UIColorScrollElement element)
	{
		if (this.OnColorSelect != null)
		{
			this.OnColorSelect(element);
		}
	}

	public override void Init()
	{
		enhancedScroller.Delegate = this;
		PaletteColor[] source = Enum.GetValues(typeof(PaletteColor)) as PaletteColor[];
		data = source.ToList();
	}

	public void ScrollToPaletteColor(PaletteColor _paletteColor)
	{
		EnhancedScroller.CellViewPositionEnum insertPosition = EnhancedScroller.CellViewPositionEnum.Before;
		enhancedScroller.ScrollPosition = enhancedScroller.GetScrollPositionForDataIndex((int)data[(int)_paletteColor], insertPosition);
	}
}
