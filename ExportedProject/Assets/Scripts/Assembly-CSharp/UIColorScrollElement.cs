using System;
using System.Threading;
using DG.Tweening;
using EnhancedUI.EnhancedScroller;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
[RequireComponent(typeof(UIInteractable))]
public class UIColorScrollElement : EnhancedScrollerCellView
{
	public delegate void HandleOnClick(UIColorScrollElement uiColorScrollElement);

	public UIColorPicker controller;

	public PaletteColor paletteColor;

	public UIInteractable uiInteractable;

	private Image _uiImage;

	public Image uiImageLogo;

	public event HandleOnClick OnClick;

	private void Awake()
	{
		_uiImage = GetComponent<Image>();
	}

	private void OnEnable()
	{
		uiInteractable.OnClick += ElementClicked;
		uiInteractable.OnDown += ElementPressed;
		uiInteractable.OnUp += ElementReleased;
	}

	private void OnDisable()
	{
		uiInteractable.OnClick -= ElementClicked;
		uiInteractable.OnDown -= ElementPressed;
		uiInteractable.OnUp -= ElementReleased;
	}

	private void ElementClicked()
	{
		if (this.OnClick != null)
		{
			this.OnClick(this);
		}
	}

	private void ElementPressed()
	{
		base.transform.DOScale(0.9f, UIAnimationManager.speedFast);
	}

	private void ElementReleased()
	{
		base.transform.DOScale(1f, UIAnimationManager.speedFast);
	}

	public void Init(PaletteColor _paletteColor)
	{
		paletteColor = _paletteColor;
		_uiImage.color = AssetManager.instance.orderedPalette[(int)paletteColor];
		if (isCoreColor(paletteColor))
		{
			uiImageLogo.transform.localScale = new Vector3(0.2f, 0.2f, 0.2f);
		}
		else
		{
			uiImageLogo.transform.localScale = Vector3.zero;
		}
	}

	private bool isCoreColor(PaletteColor _paletteColor)
	{
		return ColorUtilities.blockColorToPaletteColor.ContainsValue(_paletteColor);
	}
}
