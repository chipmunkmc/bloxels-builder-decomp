using UnityEngine.UI;

public class UIConfigDisclaimer : UIPopup
{
	public Button uiButtonReject;

	public Button uiButtonSubmit;

	public GameBuilderMode targetMode;

	private new void Start()
	{
		base.Start();
		uiButtonDismiss.onClick.RemoveAllListeners();
		uiButtonDismiss.onClick.AddListener(RejectDisclaimer);
		uiButtonReject.onClick.AddListener(RejectDisclaimer);
		uiButtonSubmit.onClick.AddListener(AcceptDisclaimer);
	}

	private void AcceptDisclaimer()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		GameBuilderCanvas.instance.currentMode = targetMode;
		Dismiss(false);
		if (GameplayBuilder.instance != null)
		{
			GameplayBuilder.instance.StartStreaming();
		}
	}

	private void RejectDisclaimer()
	{
		SoundManager.PlayOneShot(SoundManager.instance.cancelA);
		Dismiss(false);
	}
}
