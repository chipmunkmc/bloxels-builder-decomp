using System;
using UnityEngine;

public class UIConfirmTip : UITooltip
{
	[Header("UI")]
	public UIButton uiButtonNo;

	public UIButton uiButtonGo;

	[Header("Data")]
	public Action action;

	private new void Awake()
	{
		base.Awake();
	}

	private void Start()
	{
		uiButtonNo.OnClick += HandleNoPressed;
		uiButtonGo.OnClick += HandleGoPressed;
	}

	private new void OnDestroy()
	{
		uiButtonNo.OnClick -= HandleNoPressed;
		uiButtonGo.OnClick -= HandleGoPressed;
	}

	public override void Activate()
	{
		base.Activate();
	}

	public void Init(Action _action)
	{
		action = _action;
	}

	private void HandleNoPressed()
	{
		DeActivate();
	}

	private void HandleGoPressed()
	{
		action();
		DeActivate();
	}
}
