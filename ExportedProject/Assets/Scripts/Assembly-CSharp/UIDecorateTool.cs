using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIDecorateTool : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public enum Types
	{
		Eraser = 0
	}

	public UIDecorateTools parentController;

	public RectTransform rect;

	public Image icon;

	public Image indicator;

	public bool isActive;

	public Types type;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
	}

	private void OnEnable()
	{
		indicator.enabled = false;
	}

	public void Toggle()
	{
		if (isActive)
		{
			SoundManager.PlaySoundClip(SoundClip.cancelA);
			DeActivate();
		}
		else
		{
			SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
			Activate();
		}
	}

	public void Activate()
	{
		indicator.enabled = true;
		isActive = true;
		parentController.currentTool = this;
	}

	public void DeActivate()
	{
		indicator.enabled = false;
		isActive = false;
		parentController.currentTool = null;
	}

	public void OnPointerClick(PointerEventData pEvent)
	{
		Toggle();
	}
}
