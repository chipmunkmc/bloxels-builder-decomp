using UnityEngine;

public class UIDecorateTools : MonoBehaviour
{
	public RectTransform rect;

	public UIDecorateTool[] tools;

	public UIDecorateTool currentTool;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
	}

	private void Start()
	{
		tools = GetComponentsInChildren<UIDecorateTool>();
	}
}
