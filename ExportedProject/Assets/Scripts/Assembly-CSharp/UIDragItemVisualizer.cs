using UnityEngine;

public class UIDragItemVisualizer : MonoBehaviour
{
	public Canvas primaryCanvas;

	private bool _initComplete;

	private bool _transformScaled;

	public Vector3 offset;

	private void Awake()
	{
		base.transform.localScale = Vector3.zero;
	}

	private void Update()
	{
		if (_initComplete)
		{
			base.transform.position = PPUtilities.GetPositionInCanvasFromMousePosition(primaryCanvas, primaryCanvas.worldCamera) + offset;
			if (!_transformScaled)
			{
				base.transform.localScale = Vector3.one;
				_transformScaled = true;
			}
		}
	}

	public virtual void Init(Canvas canvasParent)
	{
		primaryCanvas = canvasParent;
		_initComplete = true;
	}

	public virtual void DestroyMe()
	{
		Object.DestroyImmediate(base.gameObject);
	}
}
