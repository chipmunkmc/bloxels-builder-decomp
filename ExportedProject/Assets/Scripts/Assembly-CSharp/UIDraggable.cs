using System;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIDraggable : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IDropHandler, IEventSystemHandler
{
	public delegate void HandleBeginDrag();

	public delegate void HandleEndDrag();

	public delegate void HandleDrag();

	public delegate void HandleDrop();

	public RectTransform rectTransform;

	public bool isDragging;

	public event HandleBeginDrag OnDragStart;

	public event HandleEndDrag OnDragEnd;

	public event HandleDrag OnDragging;

	public event HandleDrop OnDropped;

	public void Awake()
	{
		rectTransform = GetComponent<RectTransform>();
	}

	public virtual void OnBeginDrag(PointerEventData eventData)
	{
		isDragging = true;
		if (this.OnDragStart != null)
		{
			this.OnDragStart();
		}
	}

	public virtual void OnEndDrag(PointerEventData eventData)
	{
		isDragging = false;
		if (this.OnDragEnd != null)
		{
			this.OnDragEnd();
		}
	}

	public virtual void OnDrag(PointerEventData eventData)
	{
		isDragging = true;
		if (this.OnDragging != null)
		{
			this.OnDragging();
		}
	}

	public virtual void OnDrop(PointerEventData eventData)
	{
		isDragging = false;
		if (this.OnDropped != null)
		{
			this.OnDropped();
		}
	}
}
