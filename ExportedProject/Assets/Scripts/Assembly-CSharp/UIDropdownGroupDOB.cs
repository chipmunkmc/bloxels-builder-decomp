using System;
using System.Collections.Generic;
using UnityEngine;

public class UIDropdownGroupDOB : MonoBehaviour
{
	public UIDropdownList uiDropdownMonth;

	public UIDropdownList uiDropdownDay;

	public UIDropdownList uiDropdownYear;

	private List<string> _monthNames = new List<string>();

	private List<string> _days = new List<string>();

	private List<string> _years = new List<string>();

	public Dictionary<string, string> monthLookup;

	public string dob;

	public bool isOfAge;

	private static int _requiredAge = 13;

	private void Awake()
	{
		monthLookup = new Dictionary<string, string>
		{
			{ "January", "01" },
			{ "February", "02" },
			{ "March", "03" },
			{ "April", "04" },
			{ "May", "05" },
			{ "June", "06" },
			{ "July", "07" },
			{ "August", "08" },
			{ "September", "09" },
			{ "October", "10" },
			{ "November", "11" },
			{ "December", "12" }
		};
	}

	private void Start()
	{
		uiDropdownMonth.ClearOptions();
		uiDropdownDay.ClearOptions();
		uiDropdownYear.ClearOptions();
		createMonthDropdown();
		createDayDropdown();
		createYearDropdown();
		uiDropdownMonth.OnValueChange += setAge;
		uiDropdownDay.OnValueChange += setAge;
		uiDropdownYear.OnValueChange += setAge;
		setAge(null);
	}

	private void OnDestroy()
	{
		uiDropdownMonth.OnValueChange -= setAge;
		uiDropdownDay.OnValueChange -= setAge;
		uiDropdownYear.OnValueChange -= setAge;
	}

	private void createMonthDropdown()
	{
		foreach (string key in monthLookup.Keys)
		{
			_monthNames.Add(key);
		}
		uiDropdownMonth.AddOptions(_monthNames);
		uiDropdownMonth.Init();
	}

	private void createDayDropdown()
	{
		for (int i = 1; i < 32; i++)
		{
			if (i < 10)
			{
				_days.Add("0" + i);
			}
			else
			{
				_days.Add(i.ToString());
			}
		}
		uiDropdownDay.AddOptions(_days);
		uiDropdownDay.Init();
	}

	private void createYearDropdown()
	{
		int year = DateTime.Now.Year;
		for (int i = 1916; i < year; i++)
		{
			_years.Add(i.ToString());
		}
		uiDropdownYear.AddOptions(_years);
		uiDropdownYear.Init(_years.Count - 12);
	}

	private void setAge(UISelectableItem item)
	{
		dob = monthLookup[_monthNames[uiDropdownMonth.value]] + "/" + _days[uiDropdownDay.value] + "/" + _years[uiDropdownYear.value];
		isOfAge = CheckAge(dob);
	}

	public bool CheckAge(string birthday)
	{
		DateTime dateTime = Convert.ToDateTime(birthday);
		DateTime now = DateTime.Now;
		int num = (int)(now - dateTime).TotalMinutes;
		int num2 = _requiredAge * 60 * 24 * 365;
		if (num >= num2)
		{
			return true;
		}
		return false;
	}
}
