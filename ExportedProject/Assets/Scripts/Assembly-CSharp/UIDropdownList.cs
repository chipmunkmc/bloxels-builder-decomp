using System;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(Image))]
public class UIDropdownList : UIInteractable
{
	public delegate void HandleValueChange(UISelectableItem item);

	[Header("UI")]
	private Image _trigger;

	public TextMeshProUGUI tmProLabel;

	public RectTransform template;

	public Image activeCover;

	public UnityEngine.Object itemPrefab;

	private ScrollRect _scrollRect;

	private GridLayoutGroup _grid;

	[Header("Interactable Colors")]
	public ColorBlock colors;

	[Header("State Tracking")]
	public bool openOnAwake;

	public bool isOpen;

	[Header("Data")]
	public UISelectableItem currentItem;

	public List<UISelectableItem> itemList;

	public int value
	{
		get
		{
			return GetCurrentValue();
		}
		set
		{
			SetValue(value);
		}
	}

	public event HandleValueChange OnValueChange;

	private new void Awake()
	{
		_trigger = GetComponent<Image>();
		itemList = new List<UISelectableItem>();
		_scrollRect = template.GetComponent<ScrollRect>();
		_grid = _scrollRect.content.GetComponent<GridLayoutGroup>();
		if (!openOnAwake)
		{
			activeCover.color = new Color(1f, 1f, 1f, 0f);
			template.localScale = new Vector3(1f, 0f, 1f);
		}
	}

	public void Init(int index = 0)
	{
		for (int i = 0; i < _scrollRect.content.childCount; i++)
		{
			UISelectableItem component = _scrollRect.content.GetChild(i).GetComponent<UISelectableItem>();
			itemList.Add(component);
			component.OnSelect += ItemSelect;
			if (i == index)
			{
				SetValue(i);
			}
		}
	}

	private int GetCurrentValue()
	{
		int result = 0;
		for (int i = 0; i < _scrollRect.content.childCount; i++)
		{
			UISelectableItem component = _scrollRect.content.GetChild(i).GetComponent<UISelectableItem>();
			if (component == currentItem)
			{
				result = i;
			}
		}
		return result;
	}

	private void SetValue(int index)
	{
		currentItem = itemList[index];
		tmProLabel.SetText(currentItem.itemData.text);
	}

	public void AddItem(string text, bool shouldResize = false, Sprite sprite = null)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate(itemPrefab) as GameObject;
		gameObject.transform.SetParent(_scrollRect.content);
		gameObject.transform.localScale = Vector3.one;
		UISelectableItem component = gameObject.GetComponent<UISelectableItem>();
		Dropdown.OptionData itemData = new Dropdown.OptionData(text, sprite);
		itemList.Add(component);
		component.itemData = itemData;
		component.OnSelect += ItemSelect;
		if (shouldResize)
		{
			PPUtilities.ScrollRect_FitContent(_scrollRect.content);
		}
	}

	public void AddOptions(List<string> textOptions)
	{
		for (int i = 0; i < textOptions.Count; i++)
		{
			AddItem(textOptions[i]);
		}
		PPUtilities.ScrollRect_FitContent(_scrollRect.content);
	}

	public void AddOptions(List<Dropdown.OptionData> options)
	{
		for (int i = 0; i < options.Count; i++)
		{
			AddItem(options[i].text, false, options[i].image);
		}
		PPUtilities.ScrollRect_FitContent(_scrollRect.content);
	}

	public void ClearOptions()
	{
		itemList.Clear();
		UISelectableItem[] componentsInChildren = GetComponentsInChildren<UISelectableItem>();
		for (int i = 0; i < componentsInChildren.Length; i++)
		{
			componentsInChildren[i].OnSelect -= ItemSelect;
			UnityEngine.Object.DestroyImmediate(componentsInChildren[i].gameObject);
		}
	}

	public void ItemSelect(UISelectableItem item)
	{
		for (int i = 0; i < itemList.Count; i++)
		{
			if (itemList[i].isActive)
			{
				itemList[i].DeActivate();
			}
		}
		currentItem = item;
		currentItem.Activate();
		tmProLabel.SetText(currentItem.itemData.text);
		if (this.OnValueChange != null)
		{
			this.OnValueChange(item);
		}
		Hide();
	}

	public Tweener Toggle()
	{
		if (isOpen)
		{
			return Hide();
		}
		return Show();
	}

	public Tweener Show()
	{
		if (isOpen)
		{
			return null;
		}
		isOpen = true;
		activeCover.DOFade(1f, UIAnimationManager.speedFast);
		return template.DOScaleY(1f, UIAnimationManager.speedFast).SetEase(Ease.InExpo).OnComplete(delegate
		{
			PPUtilities.ScrollRect_JumpToIndex_Vertical(_scrollRect, _grid, value);
		});
	}

	public Tweener Hide()
	{
		if (!isOpen)
		{
			return null;
		}
		isOpen = false;
		return template.DOScaleY(0f, UIAnimationManager.speedFast).SetEase(Ease.InExpo).OnStart(delegate
		{
			activeCover.DOFade(0f, UIAnimationManager.speedMedium);
		});
	}

	public override void OnPointerEnter(PointerEventData eventData)
	{
		base.OnPointerEnter(eventData);
		_trigger.DOColor(PPUtilities.BlendColors(colors.normalColor, colors.highlightedColor), UIAnimationManager.speedFast);
	}

	public override void OnPointerExit(PointerEventData eventData)
	{
		base.OnPointerExit(eventData);
		_trigger.DOColor(colors.normalColor, UIAnimationManager.speedFast);
	}

	public override void OnPointerDown(PointerEventData eventData)
	{
		base.OnPointerDown(eventData);
		_trigger.DOColor(PPUtilities.BlendColors(colors.normalColor, colors.pressedColor), UIAnimationManager.speedFast);
	}

	public override void OnPointerUp(PointerEventData eventData)
	{
		base.OnPointerUp(eventData);
		_trigger.DOColor(PPUtilities.BlendColors(colors.normalColor, colors.highlightedColor), UIAnimationManager.speedFast);
	}

	public override void OnPointerClick(PointerEventData eventData)
	{
		base.OnPointerClick(eventData);
		Toggle();
	}
}
