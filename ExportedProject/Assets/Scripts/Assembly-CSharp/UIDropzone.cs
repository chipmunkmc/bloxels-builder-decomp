using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIDropzone : UIInteractable
{
	[Header("UI")]
	public Graphic[] placeholderGraphics;

	public Color activeColor;

	public Color inActiveColor;

	public Color unavailableColor;

	[Header("State Tracking")]
	public bool isActive;

	public override void OnPointerEnter(PointerEventData eventData)
	{
		isActive = true;
		base.OnPointerEnter(eventData);
		for (int i = 0; i < placeholderGraphics.Length; i++)
		{
			placeholderGraphics[i].DOColor(activeColor, UIAnimationManager.speedMedium);
		}
	}

	public override void OnPointerExit(PointerEventData eventData)
	{
		isActive = false;
		base.OnPointerExit(eventData);
		for (int i = 0; i < placeholderGraphics.Length; i++)
		{
			placeholderGraphics[i].DOColor(inActiveColor, UIAnimationManager.speedMedium);
		}
	}
}
