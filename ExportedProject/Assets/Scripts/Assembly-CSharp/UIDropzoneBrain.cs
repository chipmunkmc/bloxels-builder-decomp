using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIDropzoneBrain : UIDropzone
{
	[Header("Data")]
	public UIPopupEnemyConfig controller;

	public UIBrain uiBrain;

	public UIGemLock uiGemLock;

	public int idx;

	public bool shouldHideBrain;

	private new void Awake()
	{
		base.Awake();
		if (uiGemLock == null)
		{
			uiGemLock = GetComponentInChildren<UIGemLock>();
		}
	}

	private void Start()
	{
		if (uiGemLock != null)
		{
			uiGemLock.Init(new BrainSlotUnlockable(idx));
		}
	}

	public void SetBrain(UIBrain _uiBrain)
	{
		uiBrain.Set(_uiBrain.bloxelBrain);
		for (int i = 0; i < placeholderGraphics.Length; i++)
		{
			placeholderGraphics[i].enabled = false;
		}
	}

	public void SetBrain(BloxelBrain _bloxelBrain)
	{
		uiBrain.Set(_bloxelBrain);
		for (int i = 0; i < placeholderGraphics.Length; i++)
		{
			placeholderGraphics[i].enabled = false;
		}
	}

	public void RemoveBrainBoard()
	{
		uiBrain.Hide();
		for (int i = 0; i < placeholderGraphics.Length; i++)
		{
			placeholderGraphics[i].enabled = true;
		}
		if (shouldHideBrain)
		{
			shouldHideBrain = false;
		}
	}

	public bool WillAcceptBrain(UIBrain _brain)
	{
		return isActive && uiBrain != _brain && !uiGemLock.isLocked;
	}

	public void MarkForRemoval()
	{
		shouldHideBrain = true;
	}

	public override void OnPointerClick(PointerEventData eventData)
	{
		if (uiGemLock != null && uiGemLock.isLocked)
		{
			controller.ShowUnlockMenu(uiGemLock.model as BrainSlotUnlockable);
		}
		base.OnPointerClick(eventData);
	}

	public override void OnPointerEnter(PointerEventData eventData)
	{
		if (uiGemLock == null)
		{
			base.OnPointerEnter(eventData);
			return;
		}
		if (!uiGemLock.isLocked)
		{
			base.OnPointerEnter(eventData);
			return;
		}
		isActive = true;
		uiGemLock.Wobble();
		for (int i = 0; i < placeholderGraphics.Length; i++)
		{
			placeholderGraphics[i].DOColor(unavailableColor, UIAnimationManager.speedMedium);
		}
	}
}
