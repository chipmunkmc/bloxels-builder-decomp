using System;
using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIFeaturedSquare : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public ServerSquare dataModel;

	public UserAccount userAccount;

	[Header("| ========= UI ========= |")]
	public Button uiButtonTile;

	public UIButton uiButtonUser;

	public TextMeshProUGUI uiTextUserName;

	public RawImage uiRawImageSquare;

	public RawImage uiRawImageUser;

	public Image uiImageDefaultUser;

	public CoverBoard userCover;

	public RectTransform rectGemEarn;

	private void Awake()
	{
		userCover = new CoverBoard(Color.clear);
	}

	private void Start()
	{
		uiRawImageSquare.texture = AssetManager.instance.boardSolidBlank;
		uiRawImageUser.gameObject.SetActive(false);
		uiTextUserName.text = LocalizationManager.getInstance().getLocalizedText("iWall94", "Loading User Name...");
		uiButtonTile.onClick.AddListener(TilePressed);
		uiButtonUser.OnClick += UserPressed;
		if (rectGemEarn != null)
		{
			rectGemEarn.localScale = Vector3.zero;
		}
	}

	private void OnDestroy()
	{
		uiButtonUser.OnClick -= UserPressed;
	}

	public void Init(ServerSquare square)
	{
		dataModel = square;
		BuildBoard();
		BuildUser();
		if (rectGemEarn != null && dataModel.type == ProjectType.Game)
		{
			if (PlayerBankManager.instance.HasPlayedFeaturedGame(dataModel._id))
			{
				rectGemEarn.localScale = Vector3.one;
			}
			else
			{
				rectGemEarn.localScale = Vector3.zero;
			}
		}
	}

	private void BuildBoard()
	{
		StartCoroutine(DownloadTexture(dataModel.texture, delegate(Texture2D texture)
		{
			if (texture != null)
			{
				uiRawImageSquare.texture = texture;
				uiRawImageSquare.texture.filterMode = FilterMode.Point;
			}
		}));
	}

	private void BuildUser()
	{
		StartCoroutine(BloxelServerRequests.instance.FindUserByID(dataModel.owner, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				userAccount = new UserAccount(response);
				uiTextUserName.text = userAccount.userName;
				if (userAccount.avatar != null)
				{
					uiImageDefaultUser.gameObject.SetActive(false);
					uiRawImageUser.gameObject.SetActive(true);
					uiRawImageUser.texture = userAccount.avatar.UpdateTexture2D(ref userCover.colors, ref userCover.texture, userCover.style);
				}
				else
				{
					uiImageDefaultUser.gameObject.SetActive(true);
					uiRawImageUser.gameObject.SetActive(false);
				}
			}
		}));
	}

	private IEnumerator DownloadTexture(string url, Action<Texture2D> callback)
	{
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null)
		{
			callback(www.texture);
		}
		else
		{
			callback(null);
		}
	}

	private void TilePressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		FeaturedSquaresController.instance.GoToTile(dataModel.coordinate);
	}

	private void UserPressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		FeaturedSquaresController.instance.ShowUser(userAccount);
	}
}
