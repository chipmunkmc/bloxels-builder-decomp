using UnityEngine;
using UnityEngine.UI;

public class UIFeaturedSquareGroup : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public ProjectType type;

	[Header("| ========= UI ========= |")]
	public GridLayoutGroup uiGrid;

	public UIFeaturedSquare[] featuredSquares;

	private void Awake()
	{
		if (uiGrid == null)
		{
			uiGrid = GetComponent<GridLayoutGroup>();
		}
		if (featuredSquares == null)
		{
			featuredSquares = GetComponentsInChildren<UIFeaturedSquare>();
		}
	}

	public void Init()
	{
		StartCoroutine(BloxelServerRequests.instance.FindFeaturedSquares(type, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				JSONObject jSONObject = new JSONObject(response);
				for (int i = 0; i < jSONObject.list.Count; i++)
				{
					ServerSquare square = new ServerSquare(jSONObject.list[i].ToString());
					featuredSquares[i].Init(square);
				}
			}
		}));
	}
}
