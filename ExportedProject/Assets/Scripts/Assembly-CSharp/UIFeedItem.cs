using UnityEngine;
using UnityEngine.UI;

public class UIFeedItem : MonoBehaviour
{
	public RectTransform rect;

	public Text uiTextPlayerName;

	public Text uiTextAction;

	public Text uiTextItemTitle;

	public Color bgColor;

	public JSONObject message;

	public Image background;

	public void Awake()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
		background.color = bgColor;
	}

	public Color GetColorFromType(ProjectType type)
	{
		Color result = default(Color);
		switch (type)
		{
		case ProjectType.Board:
			return ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange);
		case ProjectType.Animation:
			return ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue);
		case ProjectType.MegaBoard:
			return ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green);
		case ProjectType.Character:
			return ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
		case ProjectType.Game:
			return ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Purple);
		default:
			return result;
		}
	}

	public string GetDefaultTitleFromType(ProjectType type)
	{
		string result = string.Empty;
		switch (type)
		{
		case ProjectType.Board:
			result = "A Single Board";
			break;
		case ProjectType.Animation:
			result = "An Animation";
			break;
		case ProjectType.MegaBoard:
			result = "A Background";
			break;
		case ProjectType.Character:
			result = "A Character";
			break;
		case ProjectType.Game:
			result = "An Game";
			break;
		}
		return result;
	}
}
