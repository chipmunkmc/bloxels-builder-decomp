using UnityEngine.UI;

public class UIFeedNotification : UIFeedItem
{
	public Button gotoButton;

	public int xPos;

	public int yPos;

	public void Init()
	{
		string str = message["user"]["id"].str;
		string str2 = message["from"]["id"].str;
		ProjectType type = (ProjectType)message["projectInfo"]["type"].n;
		string str3 = message["projectInfo"]["title"].str;
		string str4 = message["action"].str;
		if ((bool)message.GetField("coords"))
		{
			xPos = int.Parse(message["coords"]["c"].str);
			yPos = int.Parse(message["coords"]["r"].str);
			gotoButton.onClick.AddListener(GoToCoordinates);
		}
		uiTextPlayerName.text = str2;
		if (string.IsNullOrEmpty(str4))
		{
			uiTextAction.text = "Downloaded";
		}
		else
		{
			uiTextAction.text = str4;
		}
		if (string.IsNullOrEmpty(str3))
		{
			uiTextItemTitle.text = GetDefaultTitleFromType(type);
		}
		else
		{
			uiTextItemTitle.text = str3;
		}
		uiTextItemTitle.color = GetColorFromType(type);
	}

	private void GoToCoordinates()
	{
		CanvasStreamingInterface.instance.GoToCoordinates(xPos, yPos);
	}
}
