using UnityEngine;
using UnityEngine.UI;

public class UIFeedNotificationReward : UIFeedItem
{
	public Image icon;

	public Sprite spriteCoin;

	public Text uiTextReward;

	public string coinSuffix = "Coins Earned";

	public void Init()
	{
		string str = message["user"]["id"].str;
		int num = (int)message["coins"]["amount"].n;
		string str2 = message["from"]["id"].str;
		ProjectType type = (ProjectType)message["projectInfo"]["type"].n;
		string str3 = message["projectInfo"]["title"].str;
		uiTextReward.text = num.ToString("N0") + " " + coinSuffix;
		uiTextPlayerName.text = str2;
		uiTextAction.text = "Downloaded";
		if (string.IsNullOrEmpty(str3))
		{
			uiTextItemTitle.text = GetDefaultTitleFromType(type);
		}
		else
		{
			uiTextItemTitle.text = str3;
		}
		uiTextItemTitle.color = GetColorFromType(type);
	}
}
