using System;
using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIFilteredGameSquares : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public GameFilter type;

	[Header("| ========= UI ========= |")]
	public GridLayoutGroup uiGrid;

	public UIFeaturedSquare[] featuredSquares;

	private void Awake()
	{
		if (uiGrid == null)
		{
			uiGrid = GetComponent<GridLayoutGroup>();
		}
		if (featuredSquares == null)
		{
			featuredSquares = GetComponentsInChildren<UIFeaturedSquare>();
		}
	}

	public void Init()
	{
		Func<Action<string>, IEnumerator> func = null;
		switch (type)
		{
		case GameFilter.Newest:
			func = BloxelServerRequests.instance.FindNewestGames;
			break;
		case GameFilter.Likes:
			func = BloxelServerRequests.instance.FindMostLikedGames;
			break;
		case GameFilter.Plays:
			func = BloxelServerRequests.instance.FindMostPlayedGames;
			break;
		case GameFilter.FeaturedCreators:
			func = BloxelServerRequests.instance.FindGamesByFeaturedCreators;
			break;
		case GameFilter.BigGames:
			func = BloxelServerRequests.instance.FindBigGames;
			break;
		}
		StartCoroutine(func(delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != BloxelServerRequests.ErrorString)
			{
				JSONObject jSONObject = new JSONObject(response);
				for (int i = 0; i < jSONObject.list.Count; i++)
				{
					ServerSquare square = new ServerSquare(jSONObject.list[i].ToString());
					if (i <= featuredSquares.Length - 1)
					{
						featuredSquares[i].Init(square);
					}
				}
			}
		}));
	}
}
