using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIFollowCardController : MonoBehaviour
{
	public enum Type
	{
		Following = 0,
		Followers = 1
	}

	public Type type;

	[Header("| ========= UI ========= |")]
	public Text uiTextUserNameTitle;

	public Text uiTextCountSubTitle;

	public Transform cardGrid;

	public UserProfileTabController.TabSection section;

	public List<UIUserCard> userCards;

	public Object playerCardPrefab;

	private void Awake()
	{
		userCards = new List<UIUserCard>();
	}

	public void Reset()
	{
		uiTextUserNameTitle.text = "---";
		uiTextCountSubTitle.text = "---";
		userCards.Clear();
		foreach (Transform item in cardGrid)
		{
			Object.Destroy(item.gameObject);
		}
	}

	public void StopLoading()
	{
		StopAllCoroutines();
		AdjustScrollRectHeight(cardGrid);
	}

	public void Init(UserAccount user)
	{
		Reset();
		if (section == UserProfileTabController.TabSection.Following)
		{
			StartCoroutine(InitForFollowing(user));
		}
		else
		{
			StartCoroutine(InitForFollowers(user));
		}
	}

	private IEnumerator InitForFollowing(UserAccount user)
	{
		type = Type.Following;
		uiTextUserNameTitle.text = user.userName + " " + LocalizationManager.getInstance().getLocalizedText("iWall102", "Follows");
		uiTextCountSubTitle.text = user.following.Count + " " + LocalizationManager.getInstance().getLocalizedText("iWall71", "Players");
		for (int i = 0; i < user.following.Count; i++)
		{
			yield return StartCoroutine(BloxelServerRequests.instance.FindUserByID(user.following[i], delegate(string response)
			{
				if (!string.IsNullOrEmpty(response) && response != "ERROR")
				{
					CreateCard(new UserAccount(response));
				}
			}));
		}
		AdjustScrollRectHeight(cardGrid);
	}

	private IEnumerator InitForFollowers(UserAccount user)
	{
		type = Type.Followers;
		uiTextUserNameTitle.text = user.userName + " " + LocalizationManager.getInstance().getLocalizedText("iWall101", "Has");
		uiTextCountSubTitle.text = user.followers.Count + " " + LocalizationManager.getInstance().getLocalizedText("iWall68", "Followers");
		for (int i = 0; i < user.followers.Count; i++)
		{
			yield return StartCoroutine(BloxelServerRequests.instance.FindUserByID(user.followers[i], delegate(string response)
			{
				if (!string.IsNullOrEmpty(response) && response != "ERROR")
				{
					CreateCard(new UserAccount(response));
				}
			}));
		}
		AdjustScrollRectHeight(cardGrid);
	}

	public void CreateCard(UserAccount user, bool adjustRectHeight = false)
	{
		GameObject gameObject = Object.Instantiate(playerCardPrefab) as GameObject;
		gameObject.transform.SetParent(cardGrid);
		gameObject.transform.SetAsFirstSibling();
		gameObject.transform.localScale = Vector3.one;
		UIUserCard component = gameObject.GetComponent<UIUserCard>();
		component.Init(user);
		userCards.Add(component);
		if (adjustRectHeight)
		{
			if (type == Type.Following)
			{
				uiTextCountSubTitle.text = userCards.Count + " " + LocalizationManager.getInstance().getLocalizedText("iWall71", "Players");
			}
			else
			{
				uiTextCountSubTitle.text = userCards.Count + " " + LocalizationManager.getInstance().getLocalizedText("iWall68", "Followers");
			}
			AdjustScrollRectHeight(cardGrid);
		}
	}

	public void RemoveCard(UserAccount user, bool adjustRectHeight = false)
	{
		UIUserCard uIUserCard = null;
		for (int i = 0; i < userCards.Count; i++)
		{
			if (userCards[i].account.serverID == user.serverID)
			{
				uIUserCard = userCards[i];
				break;
			}
		}
		Object.Destroy(uIUserCard.gameObject);
		userCards.Remove(uIUserCard);
		if (adjustRectHeight)
		{
			if (type == Type.Following)
			{
				uiTextCountSubTitle.text = userCards.Count + " " + LocalizationManager.getInstance().getLocalizedText("iWall71", "Players");
			}
			else
			{
				uiTextCountSubTitle.text = userCards.Count + " " + LocalizationManager.getInstance().getLocalizedText("iWall68", "Followers");
			}
			AdjustScrollRectHeight(cardGrid);
		}
	}

	private void AdjustScrollRectHeight(Transform t)
	{
		GridLayoutGroup component = t.GetComponent<GridLayoutGroup>();
		RectTransform component2 = t.GetComponent<RectTransform>();
		int num = 0;
		for (int i = 0; i < component.transform.childCount && !(Mathf.Abs(((RectTransform)component.transform.GetChild(i)).anchoredPosition.y) >= Mathf.Abs(component.cellSize.y + component.spacing.y + (float)component.padding.top)); i++)
		{
			num++;
		}
		if (num == 0)
		{
			num = 1;
		}
		int num2 = Mathf.CeilToInt(t.childCount / num);
		float num3 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num2 * num3) + Mathf.Abs(num3);
		component2.sizeDelta = new Vector2(component2.sizeDelta.x, y);
		component2.anchoredPosition = Vector2.zero;
	}
}
