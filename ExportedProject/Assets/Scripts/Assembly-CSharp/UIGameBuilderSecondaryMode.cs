using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIGameBuilderSecondaryMode : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleOnClick();

	public static UIGameBuilderSecondaryMode MapInstance;

	public GameBuilderMode mode;

	public bool isInteractable;

	public Image indicator;

	public Image background;

	public bool isOn;

	private RectTransform rect;

	public bool isVisible = true;

	private Vector2 positionVisible;

	private Vector2 positionHidden;

	private bool hasDeterminedPosition;

	public event HandleOnClick OnClick;

	private void Awake()
	{
		isInteractable = true;
		background = GetComponent<Image>();
		if (mode != GameBuilderMode.Map)
		{
			background.color = Color.clear;
		}
		else
		{
			MapInstance = this;
		}
		rect = GetComponent<RectTransform>();
	}

	public void Select()
	{
		if (mode == GameBuilderMode.Map && GameBuilderCanvas.instance.currentMode == GameBuilderMode.Map && (GameBuilderCanvas.instance.previousMode == GameBuilderMode.None || GameBuilderCanvas.instance.previousMode == GameBuilderMode.QuickStart))
		{
			GameBuilderCanvas.instance.currentMode = GameBuilderMode.Wireframe;
		}
		else if (mode == GameBuilderMode.Map && GameBuilderCanvas.instance.currentMode == GameBuilderMode.Map && GameBuilderCanvas.instance.previousMode != GameBuilderMode.Test)
		{
			GameBuilderCanvas.instance.currentMode = GameBuilderCanvas.instance.previousMode;
		}
		else
		{
			GameBuilderCanvas.instance.currentMode = mode;
		}
	}

	public void Activate()
	{
		if (mode != GameBuilderMode.Map)
		{
			background.color = new Color(0.20784314f, 0.20784314f, 0.20784314f, 1f);
		}
	}

	public void DeActivate()
	{
		if (mode != GameBuilderMode.Map)
		{
			background.color = Color.clear;
		}
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (mode == GameBuilderMode.Map)
		{
			SoundManager.PlayOneShot(SoundManager.instance.confirmA);
		}
		if (this.OnClick != null)
		{
			this.OnClick();
		}
		if (isInteractable)
		{
			Select();
		}
	}

	public void Hide()
	{
		if (isVisible && mode == GameBuilderMode.Map)
		{
			if (!hasDeterminedPosition)
			{
				positionVisible = rect.anchoredPosition;
				positionHidden = new Vector2(rect.anchoredPosition.x - 600f, rect.anchoredPosition.y);
				hasDeterminedPosition = true;
			}
			rect.DOAnchorPos(positionHidden, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = false;
			});
		}
	}

	public void Show()
	{
		if (!isVisible && mode == GameBuilderMode.Map)
		{
			rect.DOAnchorPos(positionVisible, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = true;
			});
		}
	}
}
