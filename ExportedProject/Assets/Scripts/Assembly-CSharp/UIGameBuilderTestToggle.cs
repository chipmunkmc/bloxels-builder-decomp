using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIGameBuilderTestToggle : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	private Image image;

	public RectTransform rect;

	public GameBuilderMode mode;

	public bool isActive;

	public bool interactable;

	public Color activeColor;

	public Color inActiveColor;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		image = GetComponent<Image>();
	}

	private void Start()
	{
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (GameBuilderCanvas.instance.currentBloxelLevel != null)
		{
			GameBuilderCanvas.instance.currentMode = mode;
		}
	}
}
