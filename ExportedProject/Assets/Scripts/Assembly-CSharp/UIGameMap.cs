using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIGameMap : MonoBehaviour
{
	public GameMap map;

	public RawImage mapImage;

	public RectTransform scrollRectTransform;

	public RectTransform contentTransform;

	public RectTransform playerIndicatorTransform;

	public RectTransform gameStartPosIndicator;

	public RectTransform gameEndPosIndicator;

	public ScrollRect scrollRect;

	public GameObject gridLinePrefab;

	public RectTransform gridLinesOutput;

	private void Awake()
	{
		mapImage.color = Color.white;
		scrollRectTransform = scrollRect.GetComponent<RectTransform>();
	}

	public void Init(GameMap _map, bool activeGame = true)
	{
		scrollRect.inertia = false;
		map = _map;
		mapImage.texture = GameMap.gameMapTexture;
		contentTransform.sizeDelta = new Vector2(map.currentGame.worldBoundingRect.width, map.currentGame.worldBoundingRect.height);
		Vector2 vector = Vector2.zero;
		vector = ((!activeGame) ? new Vector2(map.gameStartPosition.x, map.gameStartPosition.y) : new Vector2(PixelPlayerController.instance.selfTransform.localPosition.x, PixelPlayerController.instance.selfTransform.localPosition.y));
		Vector2 vector2 = map.fullGameCenter - map.currentGame.worldBoundingRect.center;
		mapImage.rectTransform.anchoredPosition = vector2;
		scrollRect.content.anchoredPosition = map.fullGameCenter - vector - vector2;
		scrollRect.StopMovement();
		playerIndicatorTransform.anchoredPosition = new Vector2(vector.x - map.currentGame.worldBoundingRect.xMin, vector.y - map.currentGame.worldBoundingRect.yMin + 26f);
		gameStartPosIndicator.anchoredPosition = new Vector2(map.gameStartPosition.x - map.currentGame.worldBoundingRect.xMin, map.gameStartPosition.y - map.currentGame.worldBoundingRect.yMin + 26f);
		if (!map.currentGame.gameEndFlag.Equals(WorldLocation.InvalidLocation()))
		{
			gameEndPosIndicator.gameObject.SetActive(true);
			gameEndPosIndicator.anchoredPosition = new Vector2(map.gameEndPosition.x - map.currentGame.worldBoundingRect.xMin, map.gameEndPosition.y - map.currentGame.worldBoundingRect.yMin + 26f);
		}
		else
		{
			gameEndPosIndicator.gameObject.SetActive(false);
		}
		AnimatePlayerIndicator();
		BuildLines();
		scrollRect.inertia = true;
	}

	public void AnimatePlayerIndicator()
	{
		playerIndicatorTransform.DOKill();
		Vector2 endValue = playerIndicatorTransform.anchoredPosition + new Vector2(0f, 13f);
		Tween t = playerIndicatorTransform.DOAnchorPos(endValue, UIAnimationManager.speedMedium);
		t.SetLoops(-1, LoopType.Yoyo);
		t.SetEase(Ease.InOutSine);
	}

	private void BuildLines()
	{
		gridLinesOutput.anchoredPosition = mapImage.rectTransform.anchoredPosition;
		int num = (int)(map.currentGame.worldBoundingRect.width / 169f);
		int num2 = (int)(map.currentGame.worldBoundingRect.height / 169f);
		int num3 = num2 + 1;
		int num4 = num + 1;
		int num5 = 169;
		Color color = new Color(0.20784314f, 0.20784314f, 0.20784314f, 1f);
		Vector2 vector = new Vector2(map.currentGame.worldBoundingRect.xMin, map.currentGame.worldBoundingRect.yMin);
		for (int i = 0; i < num3; i++)
		{
			GameObject gameObject = Object.Instantiate(gridLinePrefab);
			RectTransform component = gameObject.GetComponent<RectTransform>();
			UILineRenderer component2 = gameObject.GetComponent<UILineRenderer>();
			component.SetParent(gridLinesOutput);
			component.localScale = Vector3.one;
			component.localPosition = Vector3.zero;
			component.localRotation = Quaternion.identity;
			component.sizeDelta = new Vector2((float)(num * num5) + 1.5f, 3f);
			component.anchoredPosition = vector + new Vector2(0f, (float)(i * num5) - 1.5f);
			component2.color = color;
		}
		for (int j = 0; j < num4; j++)
		{
			GameObject gameObject2 = Object.Instantiate(gridLinePrefab);
			RectTransform component3 = gameObject2.GetComponent<RectTransform>();
			UILineRenderer component4 = gameObject2.GetComponent<UILineRenderer>();
			component3.SetParent(gridLinesOutput);
			component3.localScale = Vector3.one;
			component3.localPosition = Vector3.zero;
			component3.localRotation = Quaternion.identity;
			component3.sizeDelta = new Vector2(3f, (float)(num2 * num5) + 1.5f);
			component3.anchoredPosition = vector + new Vector2((float)(j * num5) - 1.5f, 0f);
			component4.color = color;
		}
	}
}
