using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIGameSettings : UIMoveable
{
	public static UIGameSettings instance;

	public InputField uiInputTitle;

	public UIRadioGroup uiRadioGroupMusic;

	public UIMusicChoice[] musicChoices;

	public UIRadioGroup uiRadioGroupCameraMode;

	public MusicChoice activeMusicChoice;

	public Toggle uiToggleGravity;

	public bool isPreviewingMusic;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		base.Awake();
		if (musicChoices == null)
		{
			musicChoices = GetComponentsInChildren<UIMusicChoice>();
		}
		for (int i = 0; i < musicChoices.Length; i++)
		{
			musicChoices[i].OnPreview += delegate(bool isOn)
			{
				isPreviewingMusic = isOn;
			};
		}
	}

	private new void Start()
	{
		base.Start();
		uiRadioGroupMusic.OnChange += RadioChanged;
		uiInputTitle.onEndEdit.AddListener(TitleChanged);
	}

	public void Init()
	{
		if (!string.IsNullOrEmpty(GameBuilderCanvas.instance.currentGame.title))
		{
			uiInputTitle.text = GameBuilderCanvas.instance.currentGame.title;
		}
		else
		{
			uiInputTitle.text = LocalizationManager.getInstance().getLocalizedText("gamebuilder7", "My Game");
		}
		for (int i = 0; i < musicChoices.Length; i++)
		{
			if (musicChoices[i].choice == GameBuilderCanvas.instance.currentGame.musicChoice)
			{
				musicChoices[i].uiRadioButton.isOn = true;
				activeMusicChoice = musicChoices[i].choice;
			}
			else
			{
				musicChoices[i].uiRadioButton.isOn = false;
			}
		}
	}

	public void SetGame(ref BloxelGame game)
	{
	}

	private void RadioChanged(UIRadioButton btn)
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
	}

	private void TitleChanged(string title)
	{
		Save();
	}

	private void GravityChanged(bool _isOn)
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		Save();
	}

	public void Save()
	{
		string title = Profanity.SanitizeString(uiInputTitle.text);
		GameBuilderCanvas.instance.currentGame.SetTitle(title, false);
		GameBuilderCanvas.instance.currentGame.SetMusicChoice(activeMusicChoice, false);
		bool flag = GameBuilderCanvas.instance.currentGame.Save(false);
	}

	private void ResetDefaults()
	{
		for (int i = 0; i < musicChoices.Length; i++)
		{
			if (i == 0)
			{
				musicChoices[i].uiRadioButton.isOn = true;
				activeMusicChoice = musicChoices[i].choice;
			}
			else
			{
				musicChoices[i].uiRadioButton.isOn = false;
			}
		}
		uiToggleGravity.isOn = true;
		uiRadioGroupCameraMode.activeButton = uiRadioGroupCameraMode.radios[0];
	}

	private IEnumerator ResetTitle()
	{
		yield return new WaitForSeconds(1.5f);
	}
}
