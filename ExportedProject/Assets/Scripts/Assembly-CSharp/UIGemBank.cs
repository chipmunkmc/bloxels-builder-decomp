using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIGemBank : MonoBehaviour
{
	[Header("UI")]
	public RectTransform[] rectGems;

	public RectTransform rectTitle;

	public RectTransform rectCount;

	public RectTransform rectAvatar;

	public RectTransform rectBackground;

	public RectTransform rectInsufficient;

	public Image uiImageAvatarIcon;

	public UIBoard uiBoardAvatar;

	public TextMeshProUGUI uiTextCount;

	private Vector3 _countStartingScale;

	private RectTransform rect;

	public bool isVisible = true;

	private Vector2 positionVisible;

	private Vector2 positionHidden;

	private bool hasDeterminedPosition;

	public void Awake()
	{
		uiBoardAvatar = rectAvatar.GetComponentInChildren<UIBoard>();
		uiTextCount = rectCount.GetComponent<TextMeshProUGUI>();
		if (rectInsufficient != null)
		{
			rectInsufficient.localScale = Vector3.zero;
		}
		_countStartingScale = rectCount.localScale;
		rect = GetComponent<RectTransform>();
	}

	public void Start()
	{
		CheckUser();
		SetFunds();
	}

	public virtual void SetFunds()
	{
		uiTextCount.SetText(PlayerBankManager.instance.GetGemCount().ToString());
	}

	public void CheckUser()
	{
		if (CurrentUser.instance.active && CurrentUser.instance.account.avatar != null)
		{
			ShowAvatar(true);
			uiBoardAvatar.Init(CurrentUser.instance.account.avatar);
		}
		else
		{
			ShowAvatar(false);
		}
	}

	public virtual bool CanUnlock(int cost)
	{
		if (cost > PlayerBankManager.instance.GetGemCount())
		{
			InsufficientFunds();
			return false;
		}
		return true;
	}

	public virtual void UpdateFunds(int amount)
	{
		rectCount.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedSlow, 5).OnStart(delegate
		{
			uiTextCount.SetText(amount.ToString());
		});
	}

	public virtual void DecrementFunds(int cost)
	{
		string updatedCount = PlayerBankManager.instance.RemoveGems(cost).ToString();
		rectCount.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedSlow, 5).OnStart(delegate
		{
			uiTextCount.SetText(updatedCount);
		});
	}

	private void InsufficientFunds()
	{
		rectCount.DOKill();
		rectCount.localScale = _countStartingScale;
		rectCount.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedSlow, 5);
		rectInsufficient.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce).OnComplete(delegate
		{
			StartCoroutine(ResetDelay());
		});
	}

	private IEnumerator ResetDelay()
	{
		yield return new WaitForSeconds(2f);
		rectInsufficient.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce);
	}

	public void ShowAvatar(bool isOn)
	{
		if (isOn)
		{
			uiImageAvatarIcon.transform.localScale = Vector3.zero;
			uiBoardAvatar.transform.localScale = Vector3.one;
		}
		else
		{
			uiImageAvatarIcon.transform.localScale = Vector3.one;
			uiBoardAvatar.transform.localScale = Vector3.zero;
		}
	}

	public void Hide()
	{
		if (isVisible)
		{
			if (!hasDeterminedPosition)
			{
				positionVisible = rect.anchoredPosition;
				positionHidden = new Vector2(rect.anchoredPosition.x - 600f, rect.anchoredPosition.y);
				hasDeterminedPosition = true;
			}
			rect.DOAnchorPos(positionHidden, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = false;
			});
		}
	}

	public void Show()
	{
		if (!isVisible)
		{
			rect.DOAnchorPos(positionVisible, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = true;
			});
		}
	}
}
