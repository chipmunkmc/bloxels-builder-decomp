using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIGemEarn : MonoBehaviour
{
	[Header("UI")]
	public RectTransform selfRect;

	public RectTransform gemRect;

	public RectTransform bgContainer;

	public RectTransform wordRect1;

	public RectTransform wordRect2;

	public RectTransform countRect;

	private Image _uiImageGem;

	private TextMeshProUGUI _uiTextWord1;

	private TextMeshProUGUI _uiTextWord2;

	private TextMeshProUGUI _uiTextCount;

	public int count;

	public float countScale = 1f;

	private void Awake()
	{
		_uiImageGem = gemRect.GetComponent<Image>();
		_uiTextWord1 = wordRect1.GetComponent<TextMeshProUGUI>();
		_uiTextWord2 = wordRect2.GetComponent<TextMeshProUGUI>();
		_uiTextCount = countRect.GetComponent<TextMeshProUGUI>();
	}

	public void Init(int _count)
	{
		count = _count;
		if (count < 100)
		{
			countScale = 1.5f;
		}
		if (count < 10)
		{
			countScale = 2f;
		}
		countRect.localScale = new Vector3(countScale, countScale, countScale);
		_uiTextCount.SetText(count.ToString());
	}
}
