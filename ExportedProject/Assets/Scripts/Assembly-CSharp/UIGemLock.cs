using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ImagePosition))]
public class UIGemLock : MonoBehaviour
{
	public delegate void HandleGemUnlock(GemUnlockable model);

	[HideInInspector]
	public RectTransform selfRect;

	private Image _uiImage;

	public GemUnlockable model;

	public bool isLocked;

	private float _startingScale;

	public event HandleGemUnlock OnUnlock;

	private void Awake()
	{
		selfRect = GetComponent<RectTransform>();
		_uiImage = GetComponent<Image>();
		_startingScale = selfRect.localScale.x;
	}

	public void Init(GemUnlockable data)
	{
		model = data;
		model.OnUnlock += HandleUnlock;
		CheckStatus();
	}

	private void HandleUnlock(GemUnlockable castableModel)
	{
		model.OnUnlock -= HandleUnlock;
		Unlock();
		if (this.OnUnlock != null)
		{
			this.OnUnlock(model);
		}
	}

	private bool CheckStatus()
	{
		if (CurrentUser.instance.active && CurrentUser.instance.account.eduAccount > UserAccount.EduStatus.None)
		{
			Unlock();
			return false;
		}
		if (model.IsLocked())
		{
			Lock();
			return true;
		}
		Unlock();
		return false;
	}

	private void Unlock()
	{
		isLocked = false;
		selfRect.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce);
	}

	private void Lock()
	{
		isLocked = true;
		selfRect.DOScale(_startingScale, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce);
	}

	public void ManualUnlock()
	{
		isLocked = false;
		selfRect.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce);
	}

	public void ManualLock(float scale)
	{
		isLocked = true;
		selfRect.DOScale(scale, UIAnimationManager.speedMedium).SetEase(Ease.InOutBounce);
	}

	public Tweener Pulse()
	{
		return selfRect.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f);
	}

	public Tweener Wobble()
	{
		return selfRect.DOShakeRotation(UIAnimationManager.speedMedium, 10f, 5, 0f);
	}
}
