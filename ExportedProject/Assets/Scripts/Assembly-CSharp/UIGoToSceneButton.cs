using UnityEngine;

public class UIGoToSceneButton : MonoBehaviour
{
	[SerializeField]
	private UIButton attachedButton;

	[SerializeField]
	private SceneName destination;

	private void Awake()
	{
		attachedButton.OnClick += GoToScene;
	}

	private void Destroy()
	{
		attachedButton.OnClick -= GoToScene;
	}

	private void GoToScene()
	{
		BloxelsSceneManager.instance.GoTo(destination);
	}
}
