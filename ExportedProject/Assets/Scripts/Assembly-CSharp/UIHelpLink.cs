using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIHelpLink : ExternalLink
{
	public enum Location
	{
		None = 0,
		Boards = 1,
		Animations = 2,
		Backgrounds = 3,
		Characters = 4,
		Games = 5,
		Capture = 6,
		Codeboard = 7,
		iWall = 8,
		All = 9,
		Terms = 10,
		Privacy = 11,
		Support = 12,
		KickStarter = 13,
		Legal = 14,
		Buy = 15,
		PasswordReset = 16,
		Website = 17,
		Gems = 18,
		Brains = 19
	}

	public static UIHelpLink instance;

	[Header("UI")]
	public Button uiButtonHelp;

	private Location _location;

	public Dictionary<Location, string> urlLookup;

	public Object helpMenuPrefab;

	public Location location
	{
		get
		{
			return _location;
		}
		set
		{
			_location = value;
			url = urlLookup[_location];
		}
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		helpMenuPrefab = Resources.Load("Prefabs/UIPopupHelpMenu");
		urlLookup = new Dictionary<Location, string>
		{
			{
				Location.None,
				"http://www.bloxelsbuilder.com/all-tutorials"
			},
			{
				Location.Boards,
				"http://www.bloxelsbuilder.com/tutorials-boards"
			},
			{
				Location.Animations,
				"http://www.bloxelsbuilder.com/tutorials-animations"
			},
			{
				Location.Backgrounds,
				"http://www.bloxelsbuilder.com/tutorials-backgrounds"
			},
			{
				Location.Characters,
				"http://www.bloxelsbuilder.com/tutorials-characters"
			},
			{
				Location.Games,
				"http://www.bloxelsbuilder.com/tutorials-games"
			},
			{
				Location.Capture,
				"http://www.bloxelsbuilder.com/tutorials-capture"
			},
			{
				Location.Codeboard,
				"http://www.bloxelsbuilder.com/tutorials-codeboard"
			},
			{
				Location.iWall,
				"http://www.bloxelsbuilder.com/tutorials-infinity-wall"
			},
			{
				Location.All,
				"http://www.bloxelsbuilder.com/all-tutorials"
			},
			{
				Location.Terms,
				"http://www.projectpixelpress.com/terms-of-use"
			},
			{
				Location.Privacy,
				"http://www.projectpixelpress.com/privacy-policy"
			},
			{
				Location.Support,
				"http://www.bloxelsbuilder.com/support"
			},
			{
				Location.KickStarter,
				"http://www.bloxelsbuilder.com/kickstarter"
			},
			{
				Location.Legal,
				"http://www.bloxelsbuilder.com/legal"
			},
			{
				Location.Buy,
				"http://store.bloxelsbuilder.com"
			},
			{
				Location.PasswordReset,
				"http://bloxelsbuilder.com/game-password-reset"
			},
			{
				Location.Website,
				"http://bloxelsbuilder.com"
			},
			{
				Location.Gems,
				"http://bloxelsbuilder.com/tutorials-gems"
			},
			{
				Location.Brains,
				"http://bloxelsbuilder.com/tutorials-brain-boards"
			}
		};
		uiButtonHelp.onClick.AddListener(HelpLinkPressed);
	}

	private void HelpLinkPressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		CreateHelpMenu();
		AnalyticsManager.SendEventString(AnalyticsManager.Event.Help, location.ToString());
	}

	private void CreateHelpMenu()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(helpMenuPrefab);
	}
}
