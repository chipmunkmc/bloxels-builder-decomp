using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class UIHero2D : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public UICanvasHeroAnimation controller;

	public RectTransform rect;

	private RawImage _heroImage;

	private BloxelCharacter _bloxelCharacter;

	private List<BloxelBoard> _idleAnimationFrameBoards = new List<BloxelBoard>();

	private List<BloxelBoard> _walkAnimationFrameBoards = new List<BloxelBoard>();

	private List<BloxelBoard> _jumpAnimationFrameBoards = new List<BloxelBoard>();

	private float _idleFrameRate = 6f;

	private float _idleTimerStart;

	private int _idleCurrentFrameIndex;

	private int _idleBoardsUsed;

	private float _walkFrameRate = 6f;

	private float _walkTimerStart;

	private int _walkCurrentFrameIndex;

	private int _walkBoardsUsed;

	private float _jumpFrameRate = 6f;

	private float _jumpTimerStart;

	private int _jumpCurrentFrameIndex;

	private int _jumpBoardsUsed;

	private bool _shouldAnimateIdle;

	private bool _shouldAnimateWalk;

	private bool _shouldAnimateJump;

	public CoverBoard cover;

	private void Awake()
	{
		cover = new CoverBoard(new CoverStyle(Color.clear, 0));
		rect = GetComponent<RectTransform>();
		_heroImage = GetComponent<RawImage>();
	}

	private void Update()
	{
		if (_shouldAnimateIdle && _idleBoardsUsed > 0 && _idleTimerStart + 1f / _idleFrameRate < Time.time)
		{
			_idleCurrentFrameIndex = (_idleCurrentFrameIndex + 1) % _idleBoardsUsed;
			_heroImage.texture = _idleAnimationFrameBoards[_idleCurrentFrameIndex].UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
			_idleTimerStart = Time.time;
		}
		if (_shouldAnimateWalk && _walkBoardsUsed > 0 && _walkTimerStart + 1f / _walkFrameRate < Time.time)
		{
			_walkCurrentFrameIndex = (_walkCurrentFrameIndex + 1) % _walkBoardsUsed;
			_heroImage.texture = _walkAnimationFrameBoards[_walkCurrentFrameIndex].UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
			_walkTimerStart = Time.time;
		}
		if (_shouldAnimateJump && _jumpBoardsUsed > 0 && _jumpTimerStart + 1f / _jumpFrameRate < Time.time)
		{
			_jumpCurrentFrameIndex = (_jumpCurrentFrameIndex + 1) % _jumpBoardsUsed;
			_heroImage.texture = _jumpAnimationFrameBoards[_jumpCurrentFrameIndex].UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
			_jumpTimerStart = Time.time;
		}
	}

	public void Init(BloxelCharacter character, UICanvasHeroAnimation _controller = null)
	{
		controller = _controller;
		_bloxelCharacter = character;
		_idleAnimationFrameBoards = _bloxelCharacter.animations[AnimationType.Idle].boards;
		_idleBoardsUsed = _idleAnimationFrameBoards.Count;
		_idleFrameRate = _bloxelCharacter.animations[AnimationType.Idle].GetFrameRate();
		_idleTimerStart = Time.time;
		_idleCurrentFrameIndex = 0;
		_walkAnimationFrameBoards = _bloxelCharacter.animations[AnimationType.Walk].boards;
		_walkBoardsUsed = _walkAnimationFrameBoards.Count;
		_walkFrameRate = _bloxelCharacter.animations[AnimationType.Walk].GetFrameRate();
		_walkTimerStart = Time.time;
		_walkCurrentFrameIndex = 0;
		_jumpAnimationFrameBoards = _bloxelCharacter.animations[AnimationType.Jump].boards;
		_jumpBoardsUsed = _jumpAnimationFrameBoards.Count;
		_jumpFrameRate = _bloxelCharacter.animations[AnimationType.Jump].GetFrameRate();
		_jumpTimerStart = Time.time;
		_jumpCurrentFrameIndex = 0;
		_shouldAnimateIdle = false;
		_shouldAnimateWalk = false;
		_shouldAnimateJump = false;
	}

	public void Idle()
	{
		_shouldAnimateIdle = true;
		_shouldAnimateWalk = false;
		_shouldAnimateJump = false;
	}

	public void Walk()
	{
		_shouldAnimateIdle = false;
		_shouldAnimateWalk = true;
		_shouldAnimateJump = false;
	}

	public void Jump()
	{
		_shouldAnimateIdle = false;
		_shouldAnimateWalk = false;
		_shouldAnimateJump = true;
	}

	public void Look(Direction direction)
	{
		if (direction == Direction.Left)
		{
			rect.localScale = new Vector3(-1f, 1f, 1f);
		}
		else
		{
			rect.localScale = Vector3.one;
		}
	}

	public void Stop()
	{
		_shouldAnimateIdle = false;
		_shouldAnimateWalk = false;
		_shouldAnimateJump = false;
	}

	public void Clear()
	{
		_heroImage.texture = AssetManager.instance.boardSolidClear;
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (controller != null && !_shouldAnimateJump)
		{
			controller.Jump(false);
		}
		else if (_shouldAnimateWalk)
		{
			Idle();
		}
		else if (_shouldAnimateIdle)
		{
			Walk();
		}
	}
}
