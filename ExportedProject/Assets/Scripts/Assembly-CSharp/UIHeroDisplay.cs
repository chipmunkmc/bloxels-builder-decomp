using System;
using System.Threading;
using UnityEngine;

public class UIHeroDisplay : MonoBehaviour
{
	public delegate void HandleSelect(UIHeroDisplay _heroDisplay);

	public UIProjectCover uiProjectCover;

	public BloxelCharacter bloxelCharacter;

	public UIButton uiButton;

	public event HandleSelect OnSelect;

	private void Start()
	{
		uiButton.OnClick += HandleOnClick;
	}

	private void OnDestroy()
	{
		uiButton.OnClick -= HandleOnClick;
	}

	public void Init(BloxelCharacter character)
	{
		bloxelCharacter = character;
		uiProjectCover.Init(character, AnimationType.Idle, new CoverStyle(Color.clear, 0));
	}

	private void HandleOnClick()
	{
		if (this.OnSelect != null)
		{
			this.OnSelect(this);
		}
	}
}
