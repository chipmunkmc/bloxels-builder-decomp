using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class UIHeroSwap : MonoBehaviour
{
	[Header("UI")]
	public UIButton uiButtonActivate;

	public UIButton uiButtonCaptureTrigger;

	public RectTransform rectExplainer;

	public CanvasGroup uiGroupIndicator;

	public CanvasGroup uiGroupOptions;

	public UITapOverlay uiOverlay;

	public RectTransform _optionsRect;

	public float indicatorDelay = 2f;

	private RectTransform _selfRect;

	private Vector3 _offset = new Vector3(0f, 5f, 0f);

	public Bounds bounds;

	private UIBoard3D _uiBoard3d;

	private RectTransform _rectBoard3d;

	[Header("State Tracking")]
	public bool playerInZone;

	private bool _trackingHero;

	private bool _menuActive;

	private bool _hasSeenIndicator;

	[Header("Materials")]
	public Material blockMaterial;

	public Material backgroundMaterial;

	public Color colorDark;

	[Header("Menu")]
	public RectTransform[] menuItems;

	private UIHeroDisplay[] _uiHeroDisplays;

	private void Awake()
	{
		_selfRect = GetComponent<RectTransform>();
		_uiBoard3d = GetComponentInChildren<UIBoard3D>();
		_uiHeroDisplays = GetComponentsInChildren<UIHeroDisplay>();
		_rectBoard3d = _uiBoard3d.GetComponent<RectTransform>();
		for (int i = 0; i < menuItems.Length; i++)
		{
			menuItems[i].anchoredPosition = Vector2.zero;
		}
		_selfRect.localPosition = new Vector3(0f, 0f, 100f);
	}

	private void Start()
	{
		backgroundMaterial = LevelBuilder.instance.gameBackground.renderer.material;
		uiGroupIndicator.alpha = 0f;
		uiGroupIndicator.blocksRaycasts = false;
		uiGroupIndicator.interactable = false;
		uiGroupOptions.alpha = 0f;
		uiGroupOptions.blocksRaycasts = false;
		uiGroupOptions.interactable = false;
		uiOverlay.image.raycastTarget = false;
		rectExplainer.localScale = Vector3.zero;
		_rectBoard3d.anchoredPosition = Vector2.zero;
		uiButtonActivate.OnClick += HandleIndicatorPress;
		uiOverlay.OnClick += HandleOverlayClick;
		uiButtonCaptureTrigger.OnClick += HandleCapturePress;
		HomeController.instance.menu.OnChange += HandleMovement;
		CaptureManager.instance.OnCaptureConfirm += HandleCapture;
		setupBoards();
		StartCoroutine(startIndicatorTimer());
	}

	private void OnDestroy()
	{
		uiButtonActivate.OnClick -= HandleIndicatorPress;
		uiOverlay.OnClick -= HandleOverlayClick;
		uiButtonCaptureTrigger.OnClick -= HandleCapturePress;
		HomeController.instance.menu.OnChange -= HandleMovement;
		CaptureManager.instance.OnCaptureConfirm -= HandleCapture;
		for (int i = 0; i < _uiHeroDisplays.Length; i++)
		{
			_uiHeroDisplays[i].OnSelect -= HandleHeroSelect;
		}
	}

	private void LateUpdate()
	{
		if (!(PixelPlayerController.instance == null))
		{
			if (!_trackingHero)
			{
				attachHero();
			}
			if (_trackingHero)
			{
				Vector3 position = BloxelCamera.instance.mainCamera.WorldToScreenPoint(PixelPlayerController.instance.selfTransform.position + _offset);
				_selfRect.position = UIOverlayCanvas.instance.overlayCamera.ScreenToWorldPoint(position);
				int num = (int)Mathf.Sign(PixelPlayerController.instance.selfTransform.localScale.x);
				_rectBoard3d.localScale = new Vector3(Mathf.Abs(_rectBoard3d.localScale.x) * (float)num, _rectBoard3d.localScale.y, _rectBoard3d.localScale.z);
			}
		}
	}

	public void ResetMaterials()
	{
		blockMaterial.color = Color.white;
		backgroundMaterial.color = Color.white;
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.color = Color.white;
		}
	}

	private IEnumerator startIndicatorTimer()
	{
		yield return new WaitForSeconds(indicatorDelay);
		if (!_hasSeenIndicator)
		{
			HandleMovement(true);
		}
	}

	private void setupBoards()
	{
		if (AssetManager.instance.characterTemplates.Length >= _uiHeroDisplays.Length)
		{
			for (int i = 0; i < _uiHeroDisplays.Length; i++)
			{
				_uiHeroDisplays[i].Init(AssetManager.instance.characterTemplates[i]);
				_uiHeroDisplays[i].OnSelect += HandleHeroSelect;
			}
		}
	}

	private void killAllTweens()
	{
		blockMaterial.DOKill();
		uiGroupOptions.DOKill();
		uiOverlay.image.DOKill();
	}

	private void HandleHeroSelect(UIHeroDisplay heroDisplay)
	{
		_uiBoard3d.Init(heroDisplay.bloxelCharacter.animations[AnimationType.Idle].coverBoard);
		PixelPlayerController.instance.animationController.InitializeAnimations(heroDisplay.bloxelCharacter);
	}

	private void HandleOverlayClick()
	{
		_menuActive = false;
		killAllTweens();
		animateMenuIn();
		uiOverlay.transform.DOScale(0f, UIAnimationManager.speedMedium);
		uiGroupOptions.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroupIndicator.blocksRaycasts = false;
			uiGroupIndicator.interactable = false;
			uiGroupOptions.blocksRaycasts = false;
			uiGroupOptions.interactable = false;
		});
		fadeInMats();
		uiButtonActivate.interactable = true;
		HandleMovement(false);
		HomeController.instance.heroSwapVisible = false;
		HomeController.instance.touchControls.transform.localScale = Vector3.one;
	}

	public void HandleIndicatorPress()
	{
		HandleIndicatorPress(false);
	}

	public void HandleIndicatorPress(bool pressBypass = false)
	{
		if (!_menuActive)
		{
			_menuActive = true;
			killAllTweens();
			if (pressBypass)
			{
				uiGroupIndicator.alpha = 1f;
			}
			uiButtonActivate.interactable = false;
			uiOverlay.transform.DOScale(1f, UIAnimationManager.speedMedium);
			uiGroupOptions.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				uiGroupIndicator.blocksRaycasts = true;
				uiGroupIndicator.interactable = true;
				uiGroupOptions.blocksRaycasts = true;
				uiGroupOptions.interactable = true;
			}).OnComplete(delegate
			{
				animateMenuOut();
			});
			fadeOutMats();
			HomeController.instance.heroSwapVisible = true;
			HomeController.instance.touchControls.transform.localScale = Vector3.zero;
		}
	}

	public void HandleMovement(bool playerIsStationary)
	{
		if (PixelPlayerController.instance.isShrunk || PixelPlayerController.instance.isUsingShrinkPotion)
		{
			return;
		}
		uiGroupIndicator.DOKill();
		if (playerIsStationary && playerInZone)
		{
			_hasSeenIndicator = true;
			uiGroupIndicator.DOFade(1f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				uiGroupIndicator.blocksRaycasts = true;
				uiGroupIndicator.interactable = true;
				uiGroupOptions.blocksRaycasts = true;
				uiGroupOptions.interactable = true;
			});
			return;
		}
		uiGroupIndicator.DOFade(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			uiGroupIndicator.blocksRaycasts = false;
			uiGroupIndicator.interactable = false;
			uiGroupOptions.blocksRaycasts = false;
			uiGroupOptions.interactable = false;
		});
		if (_menuActive)
		{
			HandleOverlayClick();
		}
	}

	private void HandleCapturePress()
	{
		CaptureManager.instance.Show();
	}

	private void HandleCapture()
	{
		if (_menuActive)
		{
			BloxelCharacter bloxelCharacter = BloxelCharacter.CreateFromCapture(CaptureManager.latest);
			bloxelCharacter.Save();
			AssetManager.instance.characterPool.Add(bloxelCharacter.ID(), bloxelCharacter);
			if (BloxelLibrary.instance != null)
			{
				BloxelLibrary.instance.ReInitAllLibraries();
			}
			_uiBoard3d.Init(bloxelCharacter.animations[AnimationType.Idle].coverBoard);
			PixelPlayerController.instance.animationController.InitializeAnimations(bloxelCharacter);
		}
	}

	public void Reset()
	{
		attachHero();
	}

	private void attachHero()
	{
		_uiBoard3d.Init(PixelPlayerController.instance.animationController.characterProject.animations[AnimationType.Idle].coverBoard);
		_trackingHero = true;
	}

	private void animateMenuOut()
	{
		float num = _optionsRect.sizeDelta.x / 2f;
		float num2 = (float)Math.PI * 2f / (float)menuItems.Length / 2f;
		float x = _optionsRect.sizeDelta.x;
		float y = _optionsRect.sizeDelta.y;
		float num3 = ((menuItems.Length % 2 != 0) ? (num2 / 2f) : 0f);
		for (int i = 0; i < menuItems.Length; i++)
		{
			float x2 = Mathf.Round(num * Mathf.Cos(num3));
			float y2 = Mathf.Round(num * Mathf.Sin(num3));
			menuItems[i].DOAnchorPos(new Vector2(x2, y2), UIAnimationManager.speedMedium);
			num3 += num2;
		}
		_rectBoard3d.DOAnchorPos(new Vector2(0f, -200f), UIAnimationManager.speedSlow).OnComplete(delegate
		{
			rectExplainer.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InBounce);
		});
		uiOverlay.image.raycastTarget = true;
	}

	private void animateMenuIn()
	{
		uiOverlay.image.raycastTarget = false;
		rectExplainer.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.OutExpo).OnComplete(delegate
		{
			for (int i = 0; i < menuItems.Length; i++)
			{
				menuItems[i].DOAnchorPos(Vector2.zero, UIAnimationManager.speedMedium);
			}
			_rectBoard3d.DOAnchorPos(Vector2.zero, UIAnimationManager.speedMedium);
		});
	}

	private void fadeOutMats()
	{
		LevelBuilder.instance.gameBackground.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		}
		Dictionary<VoxelEngine.WorldPos2D, LevelBackground>.Enumerator enumerator2 = GameplayBuilder.instance.backgrounds.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.Value.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		}
		blockMaterial.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
	}

	private void fadeInMats()
	{
		LevelBuilder.instance.gameBackground.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		}
		Dictionary<VoxelEngine.WorldPos2D, LevelBackground>.Enumerator enumerator2 = GameplayBuilder.instance.backgrounds.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.Value.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		}
		blockMaterial.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
	}
}
