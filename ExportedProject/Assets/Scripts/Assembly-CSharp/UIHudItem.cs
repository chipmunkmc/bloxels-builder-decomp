using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public abstract class UIHudItem : MonoBehaviour
{
	public RectTransform selfRect;

	public Image icon;

	public CanvasGroup uiGroup;

	public bool permanent;

	public abstract void Init(int count, bool permanent = false, bool immediate = true);

	public abstract void UpdateDisplay(int count);

	public abstract void Increment(int num = 1, int max = 3);

	public abstract void Decrement(int num = 1);

	public abstract void Reset();

	public void Enable(bool immediate = false)
	{
		uiGroup.DOKill(true);
		base.gameObject.SetActive(true);
		if (immediate)
		{
			uiGroup.alpha = 1f;
		}
		else
		{
			uiGroup.DOFade(1f, UIAnimationManager.speedMedium);
		}
	}

	public void Disable(bool immediate = false)
	{
		uiGroup.DOKill();
		if (immediate)
		{
			base.gameObject.SetActive(false);
			uiGroup.alpha = 0f;
			base.gameObject.Recycle();
		}
		else
		{
			uiGroup.DOFade(0f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				base.gameObject.Recycle();
			});
		}
	}
}
