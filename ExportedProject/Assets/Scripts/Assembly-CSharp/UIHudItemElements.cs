using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIHudItemElements : UIHudItem
{
	public Image[] elements;

	private int lastActiveElement;

	public override void Init(int count, bool permanent = false, bool immediate = true)
	{
		base.permanent = permanent;
		uiGroup.DOKill();
		if (immediate)
		{
			UpdateDisplay(count);
			return;
		}
		uiGroup.DOFade(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			UpdateDisplay(count);
			uiGroup.DOFade(1f, UIAnimationManager.speedMedium);
		});
	}

	public void SetElementSprites(Sprite sprite)
	{
		for (int i = 0; i < elements.Length; i++)
		{
			elements[i].sprite = sprite;
		}
	}

	public override void UpdateDisplay(int count)
	{
		for (int i = 0; i < elements.Length; i++)
		{
			if (i < count)
			{
				elements[i].enabled = true;
			}
			else
			{
				elements[i].enabled = false;
			}
		}
		lastActiveElement = count - 1;
	}

	public override void Increment(int num = 1, int max = 3)
	{
		while (lastActiveElement < elements.Length - 1 && num != 0)
		{
			elements[++lastActiveElement].enabled = true;
			num--;
		}
	}

	public override void Decrement(int num = 1)
	{
		while (lastActiveElement >= 0 && num != 0)
		{
			elements[lastActiveElement--].enabled = false;
			num--;
		}
		if (lastActiveElement == 0 && !permanent)
		{
			Disable();
		}
	}

	public override void Reset()
	{
		lastActiveElement = -1;
		Dump();
	}

	public void Dump()
	{
		for (int i = 0; i < elements.Length; i++)
		{
			elements[i].enabled = false;
		}
	}
}
