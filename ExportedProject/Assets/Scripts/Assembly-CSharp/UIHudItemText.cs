using UnityEngine;
using UnityEngine.UI;

public class UIHudItemText : UIHudItem
{
	[Header("| ========= UI ========= |")]
	public Text uiTextCount;

	public override void Init(int count, bool permanent = false, bool immediate = true)
	{
		base.permanent = permanent;
		uiTextCount.text = count.ToString();
	}

	public override void UpdateDisplay(int count)
	{
		int num = int.Parse(uiTextCount.text);
		if (num != count)
		{
			uiTextCount.text = count.ToString();
		}
	}

	public override void Increment(int num = 1, int max = 3)
	{
		int num2 = int.Parse(uiTextCount.text);
		num2 += num;
		if (num2 > max)
		{
			num2 = max;
		}
		uiTextCount.text = num2.ToString();
	}

	public override void Decrement(int num = 1)
	{
		int num2 = int.Parse(uiTextCount.text);
		num2 -= num;
		if (num2 < 0)
		{
			num2 = 0;
		}
		if (num2 == 0 && !permanent)
		{
			Disable();
		}
		uiTextCount.text = num2.ToString();
	}

	public override void Reset()
	{
		uiTextCount.text = 0.ToString();
	}
}
