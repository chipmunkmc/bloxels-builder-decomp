using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIInfinityWallWelcome : UIMenuPanel
{
	public UIPopupAccount menuController;

	public UIButton uiButtonRegister;

	public Button uiButtonGuest;

	public UIButton uiButtonStudentAccounts;

	public UILoginDropdown uiLoginDropdown;

	public Transform legalTransform;

	public Transform[] clouds;

	public Transform[] features;

	private new void Awake()
	{
		base.Awake();
		ResetUI();
	}

	private new void Start()
	{
		base.Start();
		base.OnInit += ParentInit;
		uiButtonRegister.OnClick += RegisterPressed;
		uiButtonGuest.onClick.AddListener(GuestPressed);
	}

	private new void OnDestroy()
	{
		if (GameHUD.instance != null && GameHUD.instance.touchControls != null)
		{
			GameHUD.instance.touchControls.localScale = Vector3.one;
		}
		base.OnInit -= ParentInit;
		base.OnDestroy();
	}

	public override void ResetUI()
	{
		base.ResetUI();
		uiLoginDropdown.ResetUI();
		uiButtonRegister.transform.localScale = Vector3.zero;
		legalTransform.localScale = Vector3.zero;
		uiButtonGuest.transform.localScale = Vector3.zero;
		uiButtonStudentAccounts.transform.localScale = Vector3.zero;
		for (int i = 0; i < clouds.Length; i++)
		{
			clouds[i].localScale = Vector3.zero;
		}
		for (int j = 0; j < features.Length; j++)
		{
			features[j].localScale = Vector3.zero;
		}
		if (GameHUD.instance != null && GameHUD.instance.touchControls != null)
		{
			GameHUD.instance.touchControls.localScale = Vector3.zero;
		}
	}

	private void ParentInit()
	{
		if (!initComplete)
		{
			IntroSequence();
		}
		else
		{
			FastInit();
		}
	}

	private void FastInit()
	{
		uiLoginDropdown.FastInit();
		uiButtonRegister.transform.localScale = Vector3.one;
		uiButtonGuest.transform.localScale = Vector3.one;
		legalTransform.transform.localScale = Vector3.one;
		uiButtonStudentAccounts.transform.localScale = Vector3.one;
		for (int i = 0; i < features.Length; i++)
		{
			features[i].localScale = Vector3.one;
		}
		for (int j = 0; j < clouds.Length; j++)
		{
			clouds[j].localScale = Vector3.one;
		}
	}

	public Sequence IntroSequence()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(uiLoginDropdown.Init());
		sequence.Append(uiButtonRegister.transform.DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.OutExpo));
		sequence.Append(uiButtonGuest.transform.DOScale(1f, UIAnimationManager.speedFast));
		sequence.Append(legalTransform.DOScale(1f, UIAnimationManager.speedFast));
		sequence.Append(uiButtonStudentAccounts.transform.DOScale(1f, UIAnimationManager.speedFast));
		for (int i = 0; i < features.Length; i++)
		{
			sequence.Append(features[i].DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.OutExpo));
		}
		for (int j = 0; j < clouds.Length; j++)
		{
			sequence.Append(clouds[j].DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.OutExpo));
		}
		sequence.Append(uiLoginDropdown.Show());
		sequence.Play().OnComplete(delegate
		{
			initComplete = true;
		});
		return sequence;
	}

	private void RegisterPressed()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		Hide().OnComplete(delegate
		{
			menuController.registerController.SwitchStep(RegistrationController.Step.Username);
		});
	}

	private void GuestPressed()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		Hide().OnComplete(delegate
		{
			menuController.guestMenu.Show();
		});
	}
}
