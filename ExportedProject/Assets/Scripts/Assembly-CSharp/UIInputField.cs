using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInputField : InputField
{
	public override void OnPointerClick(PointerEventData eventData)
	{
		base.OnPointerClick(eventData);
		if (!string.IsNullOrEmpty(base.text))
		{
			base.text = string.Empty;
		}
	}
}
