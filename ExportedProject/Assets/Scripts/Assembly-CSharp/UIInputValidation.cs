using System;
using System.Collections;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIInputValidation : MonoBehaviour
{
	public enum Type
	{
		Blank = 0,
		InvalidUserName = 1,
		InvalidAccountInfo = 2,
		AlreadyExists = 3,
		Match = 4,
		Naughty = 5
	}

	public Type type;

	public Dictionary<Type, string> validationLookup;

	public Image uiImageInputBackground;

	public Text uiValidationTextField;

	public Color inValidColor;

	public RectTransform validIndicator;

	public RectTransform inValidIndicator;

	private Tweener _validTween;

	private Tweener _inValidTween;

	private void Start()
	{
		ValidationMessage componentInChildren = GetComponentInChildren<ValidationMessage>();
		if (componentInChildren != null)
		{
			uiValidationTextField = componentInChildren.uiTextMessage;
		}
		if (uiImageInputBackground == null)
		{
			uiImageInputBackground = GetComponent<Image>();
		}
		uiValidationTextField.enabled = false;
		if (validIndicator != null)
		{
			validIndicator.localScale = Vector3.zero;
		}
		if (inValidIndicator != null)
		{
			inValidIndicator.localScale = Vector3.zero;
		}
		validationLookup = new Dictionary<Type, string>
		{
			{
				Type.Blank,
				LocalizationManager.getInstance().getLocalizedText("account100", "Cannot Be Blank")
			},
			{
				Type.InvalidUserName,
				LocalizationManager.getInstance().getLocalizedText("account101", "Invalid Username")
			},
			{
				Type.InvalidAccountInfo,
				LocalizationManager.getInstance().getLocalizedText("account18", "Invalid Login Credentials")
			},
			{
				Type.AlreadyExists,
				LocalizationManager.getInstance().getLocalizedText("account81", "Already Exists")
			},
			{
				Type.Match,
				LocalizationManager.getInstance().getLocalizedText("account102", "Must Match")
			},
			{
				Type.Naughty,
				LocalizationManager.getInstance().getLocalizedText("account103", "Restricted For Profanity")
			}
		};
	}

	public void InValid()
	{
		uiValidationTextField.enabled = true;
		uiImageInputBackground.DOColor(inValidColor, UIAnimationManager.speedFast);
		uiValidationTextField.text = validationLookup[type];
		if (validIndicator != null)
		{
			if (_validTween != null)
			{
				_validTween.Kill();
			}
			validIndicator.localScale = Vector3.zero;
		}
		if (inValidIndicator != null)
		{
			if (_inValidTween != null)
			{
				_inValidTween.Kill();
			}
			_inValidTween = inValidIndicator.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
		}
	}

	public void Valid()
	{
		uiValidationTextField.enabled = false;
		uiImageInputBackground.DOColor(Color.white, UIAnimationManager.speedFast);
		if (validIndicator != null)
		{
			if (_validTween != null)
			{
				_validTween.Kill();
			}
			_validTween = validIndicator.DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.OutBounce);
		}
		if (inValidIndicator != null)
		{
			if (_inValidTween != null)
			{
				_inValidTween.Kill();
			}
			inValidIndicator.localScale = Vector3.zero;
		}
	}

	public void ForceValid()
	{
		uiValidationTextField.enabled = false;
		uiImageInputBackground.color = Color.white;
		if (_validTween != null)
		{
			_validTween.Kill();
		}
		if (_inValidTween != null)
		{
			_inValidTween.Kill();
		}
		validIndicator.localScale = Vector3.one;
		inValidIndicator.localScale = Vector3.zero;
	}

	public static bool CheckEmpty(InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = Type.Blank;
		if (string.IsNullOrEmpty(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public static bool CheckEmpty(TMP_InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = Type.Blank;
		if (string.IsNullOrEmpty(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public static bool CheckNaughty(InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = Type.Naughty;
		if (Profanity.NaughtyString(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public static bool CheckNaughty(TMP_InputField input)
	{
		UIInputValidation component = input.GetComponent<UIInputValidation>();
		component.type = Type.Naughty;
		if (Profanity.NaughtyString(input.text))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public static bool CheckMatch(InputField a, InputField b)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		UIInputValidation component2 = b.GetComponent<UIInputValidation>();
		component.type = Type.Match;
		component2.type = Type.Match;
		if (a.text != b.text)
		{
			component2.InValid();
			return false;
		}
		component2.Valid();
		return true;
	}

	public static bool CheckMatch(TMP_InputField a, TMP_InputField b)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		UIInputValidation component2 = b.GetComponent<UIInputValidation>();
		component.type = Type.Match;
		component2.type = Type.Match;
		if (a.text != b.text)
		{
			component2.InValid();
			return false;
		}
		component2.Valid();
		return true;
	}

	public static bool CheckEmailFormat(InputField a)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		component.type = Type.InvalidAccountInfo;
		if (!Regex.IsMatch(a.text, "\\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\\Z", RegexOptions.IgnoreCase))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public static bool CheckEmailFormat(TMP_InputField a)
	{
		UIInputValidation component = a.GetComponent<UIInputValidation>();
		component.type = Type.InvalidAccountInfo;
		if (!Regex.IsMatch(a.text, "\\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\\Z", RegexOptions.IgnoreCase))
		{
			component.InValid();
			return false;
		}
		component.Valid();
		return true;
	}

	public IEnumerator CheckUserPropertyExists(string type, InputField a, Action<bool> cb)
	{
		UIInputValidation validation = a.GetComponent<UIInputValidation>();
		validation.type = Type.AlreadyExists;
		yield return StartCoroutine(BloxelServerRequests.instance.AccountPropertyExists(type, a.text, delegate(bool exists)
		{
			if (exists)
			{
				validation.InValid();
			}
			else
			{
				validation.Valid();
			}
			cb(exists);
		}));
	}

	public IEnumerator CheckUserPropertyExists(string type, TMP_InputField a, Action<bool> cb)
	{
		UIInputValidation validation = a.GetComponent<UIInputValidation>();
		validation.type = Type.AlreadyExists;
		yield return StartCoroutine(BloxelServerRequests.instance.AccountPropertyExists(type, a.text, delegate(bool exists)
		{
			if (exists)
			{
				validation.InValid();
			}
			else
			{
				validation.Valid();
			}
			cb(exists);
		}));
	}
}
