using System;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIInteractable : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler, ISelectHandler, IEventSystemHandler
{
	public delegate void HandleClick();

	public delegate void HandleEnter();

	public delegate void HandleExit();

	public delegate void HandleDown();

	public delegate void HandleUp();

	public delegate void HandleSelect();

	public RectTransform rectTransform;

	public event HandleClick OnClick;

	public event HandleEnter OnEnter;

	public event HandleExit OnExit;

	public event HandleDown OnDown;

	public event HandleUp OnUp;

	public event HandleSelect onSelect;

	public void Awake()
	{
		rectTransform = GetComponent<RectTransform>();
	}

	public virtual void OnSelect(BaseEventData eData)
	{
		if (this.onSelect != null)
		{
			this.onSelect();
		}
	}

	public virtual void OnPointerEnter(PointerEventData eventData)
	{
		if (this.OnEnter != null)
		{
			this.OnEnter();
		}
	}

	public virtual void OnPointerExit(PointerEventData eventData)
	{
		if (this.OnExit != null)
		{
			this.OnExit();
		}
	}

	public virtual void OnPointerDown(PointerEventData eventData)
	{
		if (this.OnDown != null)
		{
			this.OnDown();
		}
	}

	public virtual void OnPointerUp(PointerEventData eventData)
	{
		if (this.OnUp != null)
		{
			this.OnUp();
		}
	}

	public virtual void OnPointerClick(PointerEventData eventData)
	{
		if (this.OnClick != null)
		{
			this.OnClick();
		}
	}
}
