using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIInventoryItem : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	[Header("| ========== DATA ========= |")]
	public UIInventorySelect controller;

	public InventoryData.Type inventoryType;

	[Header("| ========== UI ========= |")]
	public RectTransform selfRect;

	public CanvasGroup indicatorGroup;

	public Image uiImageIcon;

	public Image uiImageShadow;

	public Color gray;

	public Color activeColor;

	public Color inActiveColor = Color.black;

	public static UIInventoryItem currentActionItem;

	public static UIInventoryItem currentMovementItem;

	public InventoryData dataModel { get; private set; }

	public bool isSelected
	{
		get
		{
			return GameplayController.instance.inventoryManager.ItemIsEquipped(inventoryType);
		}
	}

	public bool isActive
	{
		get
		{
			if (dataModel == null)
			{
				return false;
			}
			return dataModel.available;
		}
	}

	private void Awake()
	{
		activeColor = AssetManager.instance.bloxelGreen;
		selfRect = GetComponent<RectTransform>();
	}

	private void OnEnable()
	{
		Init();
	}

	private void Init()
	{
		if (dataModel == null)
		{
			dataModel = GameplayController.instance.inventoryManager.inventory[(int)inventoryType];
		}
		if (isActive)
		{
			uiImageIcon.color = Color.white;
		}
		else
		{
			uiImageIcon.color = Color.black;
		}
		switch (dataModel.behavior)
		{
		case InventoryData.Behavior.Action:
			if (GameplayController.instance.inventoryManager.currentAction != null && inventoryType == GameplayController.instance.inventoryManager.currentAction.type)
			{
				currentActionItem = this;
			}
			break;
		case InventoryData.Behavior.Movement:
			if (GameplayController.instance.inventoryManager.currentMovement != null && inventoryType == GameplayController.instance.inventoryManager.currentMovement.type)
			{
				currentMovementItem = this;
			}
			break;
		}
		if (isSelected)
		{
			UpdateSelectedUI();
		}
		else
		{
			UpdateDeselectedUI(true);
		}
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (dataModel != null && dataModel.available && dataModel.behavior != 0)
		{
			Toggle();
		}
	}

	public void Toggle()
	{
		if (isSelected)
		{
			Deselect();
		}
		else
		{
			Select();
		}
	}

	public void Select()
	{
		if (isSelected)
		{
			return;
		}
		switch (dataModel.behavior)
		{
		case InventoryData.Behavior.Action:
			if (currentActionItem != null && currentActionItem.inventoryType != inventoryType)
			{
				currentActionItem.Deselect(false);
			}
			currentActionItem = this;
			break;
		case InventoryData.Behavior.Movement:
			if (currentMovementItem != null && currentMovementItem.inventoryType != inventoryType)
			{
				currentMovementItem.Deselect(false);
			}
			currentMovementItem = this;
			break;
		}
		GameplayController.instance.inventoryManager.AssignItem(dataModel.type);
		UpdateSelectedUI();
	}

	public void Deselect(bool updateGameplayHUD = true)
	{
		if (isSelected)
		{
			GameplayController.instance.inventoryManager.UnassignItem(dataModel.type, updateGameplayHUD);
			switch (dataModel.behavior)
			{
			case InventoryData.Behavior.Action:
				currentActionItem = null;
				break;
			case InventoryData.Behavior.Movement:
				currentMovementItem = null;
				break;
			}
			UpdateDeselectedUI();
		}
	}

	private void UpdateSelectedUI(bool immediate = false)
	{
		if (immediate)
		{
			indicatorGroup.alpha = 1f;
			uiImageShadow.color = activeColor;
		}
		else
		{
			indicatorGroup.DOFade(1f, UIAnimationManager.speedFast);
			uiImageShadow.DOColor(activeColor, UIAnimationManager.speedFast);
		}
	}

	private void UpdateDeselectedUI(bool immediate = false)
	{
		if (immediate)
		{
			indicatorGroup.alpha = 0f;
			uiImageShadow.color = inActiveColor;
		}
		else
		{
			indicatorGroup.DOFade(0f, UIAnimationManager.speedFast);
			uiImageShadow.DOColor(inActiveColor, UIAnimationManager.speedFast);
		}
	}
}
