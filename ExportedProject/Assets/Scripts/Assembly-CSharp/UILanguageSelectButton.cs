using UnityEngine;

public class UILanguageSelectButton : MonoBehaviour
{
	[SerializeField]
	private string language;

	[SerializeField]
	private UIButton attachedButton;

	private void Awake()
	{
		attachedButton.OnClick += HandleClick;
	}

	private void OnDestroy()
	{
		attachedButton.OnClick -= HandleClick;
	}

	private void HandleClick()
	{
		LocalizationManager.getInstance().loadLocalization(language);
		StartCoroutine(AssetManager.instance.SetupHomeMiniGame());
		StartCoroutine(AssetManager.instance.SetupPoultryPanic());
		PrefsManager.instance.preferredLanguage = language;
		BloxelsSceneManager.instance.GoTo(SceneName.Home);
	}
}
