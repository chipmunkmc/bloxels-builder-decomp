using UnityEngine;
using UnityEngine.UI;

public class UILegalLink : ExternalLink
{
	[Header("UI")]
	public Button uiButtonLegalLink;

	public UIHelpLink.Location location;

	private void Awake()
	{
		uiButtonLegalLink.onClick.AddListener(LegalLinkPressed);
	}

	private void LegalLinkPressed()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		UIPopup.instance.Dismiss(false);
	}
}
