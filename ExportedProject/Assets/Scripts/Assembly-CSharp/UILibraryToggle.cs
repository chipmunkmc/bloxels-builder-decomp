using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class UILibraryToggle : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public RectTransform rect;

	public bool isClosed;

	public RectTransform toggleArrow;

	private void Awake()
	{
		rect = GetComponent<RectTransform>();
		isClosed = true;
	}

	public void Toggle()
	{
		if (!isClosed)
		{
			Hide();
		}
		else
		{
			Show();
		}
	}

	public Tweener Show()
	{
		if (!isClosed)
		{
			return null;
		}
		if (BoardZoomManager.instance != null && BoardZoomManager.instance.is16by10OrWider)
		{
			UIGameBuilderSecondaryMode.MapInstance.Hide();
		}
		SoundManager.PlayEventSound(SoundEvent.DrawerSlide);
		return BloxelLibrary.instance.rect.DOAnchorPos(BloxelLibrary.instance.openPosition, UIAnimationManager.speedMedium).SetEase(Ease.OutQuad).OnStart(delegate
		{
			isClosed = false;
			BloxelLibrary.instance.isVisible = true;
		})
			.OnComplete(delegate
			{
				toggleArrow.DOScaleX(-1f, UIAnimationManager.speedFast).SetEase(Ease.OutBounce);
				if (BloxelLibrary.instance.currentTabID == 0)
				{
					SavedGame selectedProject = BloxelLibraryScrollController.instance.GetSelectedProject<SavedGame>();
					if (selectedProject != null)
					{
						selectedProject.BuildCover();
					}
				}
			});
	}

	public Tweener Hide()
	{
		if (isClosed)
		{
			return null;
		}
		if (BoardZoomManager.instance != null && BoardZoomManager.instance.is16by10OrWider)
		{
			UIGameBuilderSecondaryMode.MapInstance.Show();
		}
		return BloxelLibrary.instance.rect.DOAnchorPos(BloxelLibrary.instance.basePosition, UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnStart(delegate
		{
			isClosed = true;
			BloxelLibrary.instance.isVisible = false;
			SoundManager.instance.PlayDelayedSound(UIAnimationManager.speedFast, SoundManager.instance.slideA);
		})
			.OnComplete(delegate
			{
				toggleArrow.DOScaleX(1f, UIAnimationManager.speedFast).SetEase(Ease.InBounce);
			});
	}

	public IEnumerator DelayedCollapse(float seconds)
	{
		yield return new WaitForSeconds(seconds);
		Hide();
	}

	private IEnumerator DelayedSound()
	{
		yield return new WaitForSeconds(UIAnimationManager.speedFast);
		SoundManager.PlayEventSound(SoundEvent.DrawerSlide);
	}

	public void OnPointerClick(PointerEventData eData)
	{
		Toggle();
	}
}
