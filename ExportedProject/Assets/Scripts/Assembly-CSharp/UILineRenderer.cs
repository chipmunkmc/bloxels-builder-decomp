using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILineRenderer : MaskableGraphic
{
	public float LineThikness = 2f;

	public bool UseMargins;

	public Vector2 Margin;

	public Vector2[] Points;

	protected override void OnFillVBO(List<UIVertex> vbo)
	{
		if (Points == null || Points.Length < 2)
		{
			Points = new Vector2[2]
			{
				new Vector2(0f, 0f),
				new Vector2(1f, 1f)
			};
		}
		float num = base.rectTransform.rect.width;
		float num2 = base.rectTransform.rect.height;
		float num3 = (0f - base.rectTransform.pivot.x) * base.rectTransform.rect.width;
		float num4 = (0f - base.rectTransform.pivot.y) * base.rectTransform.rect.height;
		if (UseMargins)
		{
			num -= Margin.x;
			num2 -= Margin.y;
			num3 += Margin.x / 2f;
			num4 += Margin.y / 2f;
		}
		vbo.Clear();
		Vector2 vector = Vector2.zero;
		Vector2 vector2 = Vector2.zero;
		for (int i = 1; i < Points.Length; i++)
		{
			Vector2 vector3 = Points[i - 1];
			Vector2 vector4 = Points[i];
			vector3 = new Vector2(vector3.x * num + num3, vector3.y * num2 + num4);
			vector4 = new Vector2(vector4.x * num + num3, vector4.y * num2 + num4);
			float z = Mathf.Atan2(vector4.y - vector3.y, vector4.x - vector3.x) * 180f / (float)Math.PI;
			Vector2 vector5 = vector3 + new Vector2(0f, (0f - LineThikness) / 2f);
			Vector2 vector6 = vector3 + new Vector2(0f, LineThikness / 2f);
			Vector2 vector7 = vector4 + new Vector2(0f, LineThikness / 2f);
			Vector2 vector8 = vector4 + new Vector2(0f, (0f - LineThikness) / 2f);
			vector5 = RotatePointAroundPivot(vector5, vector3, new Vector3(0f, 0f, z));
			vector6 = RotatePointAroundPivot(vector6, vector3, new Vector3(0f, 0f, z));
			vector7 = RotatePointAroundPivot(vector7, vector4, new Vector3(0f, 0f, z));
			vector8 = RotatePointAroundPivot(vector8, vector4, new Vector3(0f, 0f, z));
			if (i > 1)
			{
				SetVbo(vbo, new Vector2[4] { vector, vector2, vector5, vector6 });
			}
			SetVbo(vbo, new Vector2[4] { vector5, vector6, vector7, vector8 });
			vector = vector7;
			vector2 = vector8;
		}
	}

	protected void SetVbo(List<UIVertex> vbo, Vector2[] vertices)
	{
		for (int i = 0; i < vertices.Length; i++)
		{
			UIVertex simpleVert = UIVertex.simpleVert;
			simpleVert.color = color;
			simpleVert.position = vertices[i];
			vbo.Add(simpleVert);
		}
	}

	public Vector3 RotatePointAroundPivot(Vector3 point, Vector3 pivot, Vector3 angles)
	{
		Vector3 vector = point - pivot;
		vector = Quaternion.Euler(angles) * vector;
		point = vector + pivot;
		return point;
	}
}
