using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UILoginDropdown : UIToggleDropdown
{
	public UIPopupAccount controller;

	public UIButton uiButtonLogin;

	public TMP_InputField uiInputEmail;

	public TMP_InputField uiInputPassword;

	public Image[] uiButtonImages;

	[Range(0f, 2f)]
	public float inputTimer = 0.35f;

	public bool canProgress;

	private bool _emailValid;

	private bool _passwordValid;

	public bool emailValid
	{
		get
		{
			return _emailValid;
		}
		set
		{
			_emailValid = value;
			checkProgress();
		}
	}

	public bool passwordValid
	{
		get
		{
			return _passwordValid;
		}
		set
		{
			_passwordValid = value;
			checkProgress();
		}
	}

	private void Start()
	{
		uiInputEmail.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckEmailTimer");
			StartCoroutine("CheckEmailTimer");
		});
		uiInputPassword.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckInputPassword");
			StartCoroutine("CheckInputPassword");
		});
		uiButtonLogin.OnClick += Login;
	}

	private void OnDestroy()
	{
		uiButtonLogin.OnClick -= Login;
	}

	public void ResetUI()
	{
		base.transform.localScale = new Vector3(1f, 0f, 1f);
	}

	public void FastInit()
	{
		base.transform.localScale = Vector3.one;
		ToggleButtonState();
		canProgress = true;
	}

	public Tweener Init()
	{
		return base.transform.DOScaleY(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce).OnComplete(delegate
		{
			canProgress = false;
			ToggleButtonState();
		});
	}

	private IEnumerator CheckEmailTimer()
	{
		yield return new WaitForSeconds(inputTimer);
		CheckEmailInput();
	}

	private IEnumerator CheckInputPassword()
	{
		yield return new WaitForSeconds(inputTimer);
		passwordValid = controller.registerController.CheckEmpty(uiInputPassword);
	}

	public void CheckEmailInput()
	{
		if (!controller.registerController.CheckEmpty(uiInputEmail))
		{
			emailValid = false;
		}
		else
		{
			emailValid = true;
		}
	}

	private IEnumerator CheckServer(Action<bool> callback)
	{
		yield return controller.registerController.CheckUserPropertyExists(RegistrationController.string_userEmail, uiInputEmail, delegate(bool exists)
		{
			callback(exists);
		});
	}

	private void checkProgress()
	{
		if (emailValid && passwordValid)
		{
			canProgress = true;
		}
		else
		{
			canProgress = false;
		}
		ToggleButtonState();
	}

	private void Login()
	{
		if (!canProgress)
		{
			return;
		}
		uiButtonLogin.interactable = false;
		uiButtonLogin.GetComponentInChildren<TextMeshProUGUI>().SetText(LocalizationManager.getInstance().getLocalizedText("loadingscreen2", "Loading..."));
		UIInputValidation validation = uiInputEmail.GetComponent<UIInputValidation>();
		UIInputValidation pwValidation = uiInputPassword.GetComponent<UIInputValidation>();
		if (controller.registerController.CheckEmailFormat(uiInputEmail))
		{
			StartCoroutine(BloxelServerRequests.instance.ValidateUserLogin(uiInputEmail.text, uiInputPassword.text, delegate(UserAccount account)
			{
				if (account != null)
				{
					validation.Valid();
					pwValidation.Valid();
					CurrentUser.instance.Login(account);
					controller.LoginComplete();
				}
				else
				{
					validation.type = UIInputValidation.Type.InvalidAccountInfo;
					pwValidation.type = UIInputValidation.Type.InvalidAccountInfo;
					pwValidation.InValid();
					validation.InValid();
					uiButtonLogin.interactable = true;
					uiButtonLogin.GetComponentInChildren<TextMeshProUGUI>().SetText(LocalizationManager.getInstance().getLocalizedText("challenges20", "Try Again"));
				}
			}));
		}
		else
		{
			if (!controller.registerController.CheckEmpty(uiInputEmail))
			{
				return;
			}
			StartCoroutine(BloxelServerRequests.instance.ValidateUserLoginViaUsername(uiInputEmail.text, uiInputPassword.text, delegate(UserAccount account)
			{
				if (account != null)
				{
					validation.Valid();
					pwValidation.Valid();
					CurrentUser.instance.Login(account);
					controller.LoginComplete();
				}
				else
				{
					validation.type = UIInputValidation.Type.InvalidAccountInfo;
					pwValidation.type = UIInputValidation.Type.InvalidAccountInfo;
					pwValidation.InValid();
					validation.InValid();
					uiButtonLogin.interactable = true;
					uiButtonLogin.GetComponentInChildren<TextMeshProUGUI>().SetText(LocalizationManager.getInstance().getLocalizedText("challenges20", "Try Again"));
				}
			}));
		}
	}

	public void ToggleButtonState()
	{
		for (int i = 0; i < uiButtonImages.Length; i++)
		{
			if (canProgress)
			{
				uiButtonImages[i].DOFade(1f, UIAnimationManager.speedFast);
			}
			else
			{
				uiButtonImages[i].DOFade(0f, UIAnimationManager.speedFast);
			}
		}
		uiButtonLogin.interactable = canProgress;
	}
}
