using UnityEngine;
using UnityEngine.UI;

public class UIMapShift : MonoBehaviour
{
	public Direction direction;

	public Button uiButtonShift;

	private void Start()
	{
		uiButtonShift = GetComponent<Button>();
		uiButtonShift.onClick.AddListener(Pressed);
	}

	public void Pressed()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
		GridLocation gridLocation = GameBuilderCanvas.instance.currentBloxelLevel.location;
		switch (direction)
		{
		case Direction.Left:
			if (gridLocation.c == 0)
			{
				return;
			}
			gridLocation = new GridLocation(gridLocation.c - 1, gridLocation.r);
			break;
		case Direction.Up:
			if (gridLocation.r == 12)
			{
				return;
			}
			gridLocation = new GridLocation(gridLocation.c, gridLocation.r + 1);
			break;
		case Direction.Right:
			if (gridLocation.c == 12)
			{
				return;
			}
			gridLocation = new GridLocation(gridLocation.c + 1, gridLocation.r);
			break;
		case Direction.Down:
			if (gridLocation.r == 0)
			{
				return;
			}
			gridLocation = new GridLocation(gridLocation.c, gridLocation.r - 1);
			break;
		}
		if (GameBuilderCanvas.instance.currentGame.levels.ContainsKey(gridLocation))
		{
			GameBuilderCanvas.instance.ChangeLevel(gridLocation);
		}
	}
}
