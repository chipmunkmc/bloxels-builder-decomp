using UnityEngine;

public class UIMapTileDragVisualizer : UIDragItemVisualizer
{
	[Header("UI")]
	public MegaBoardTile tile;

	public UIProjectCover projectCover;

	public void Create(MegaBoardTile _tile, Canvas canvasParent, Vector3 _dragOffset)
	{
		tile = _tile;
		projectCover.Init(tile.project.coverBoard, tile.cover.style);
		projectCover.uiRawImage.color = Color.white;
		offset = _dragOffset;
		base.Init(canvasParent);
	}
}
