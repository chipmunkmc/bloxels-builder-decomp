using System;
using System.Collections;
using System.Text.RegularExpressions;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIMenuByeByeBloxels : MonoBehaviour
{
	[Serializable]
	public class NotificationsResponse
	{
		[Serializable]
		public class Notification
		{
			public bool isV1;

			public string title;

			public string message;

			public string videoUrl;

			public string imageUrl;

			public string migrationLink;

			public string migrationButtonText;
		}

		public Notification data;
	}

	[Serializable]
	public class MigrationUserSubmission
	{
		public string email;

		public int bbUserType;
	}

	[SerializeField]
	private TextMeshProUGUI _titlePrimary;

	[SerializeField]
	private TextMeshProUGUI _body;

	[SerializeField]
	private Dropdown _userTypeDropdown;

	[SerializeField]
	private TMP_InputField _emailInput;

	[SerializeField]
	private UIButton _sendButton;

	[SerializeField]
	private TextMeshProUGUI _warningText;

	[SerializeField]
	private TextMeshProUGUI _sendButtonText;

	[SerializeField]
	private TextMeshProUGUI _migrateButtonText;

	[SerializeField]
	private GameObject _sizzleRealContainer;

	[SerializeField]
	private AgeGateTooltip _migrationAgeGate;

	[SerializeField]
	private AgeGateTooltip _playVideoAgeGate;

	[SerializeField]
	private RawImage _rawImageForMedia;

	[HideInInspector]
	public BloxelsMigrationUserType currentUserType;

	private const string CHOOSE_TYPE_WARNING = "Please choose a user type";

	private const string ENTER_VALID_EMAIL_WARNING = "Please enter a valid email";

	private const string EMAIL_SEND_SUCCESS_TEXT = "Sent!";

	private string _migrationLink;

	private const string NO_INTERNET_PRIMARY_TITLE = "Please connect to WiFi";

	private const string NO_INTERNET_MESSAGE = "Please connect to the internet to read some important news from Bloxels. Thanks!";

	private string _videoUrl;

	private UIMenuSizzleReal _activeSizzleReal;

	[SerializeField]
	private UIMenuSizzleReal _sizzleRealPrefab;

	[SerializeField]
	private GameObject _navButtons;

	private void OnEnable()
	{
		_warningText.text = string.Empty;
		_userTypeDropdown.onValueChanged.AddListener(HandleUserTypeSelected);
		_sendButton.OnClick += HandleSendPressed;
		InternetReachabilityVerifier.Instance.statusChangedDelegate += HandleInternetConnection;
		HandleInternetConnection(InternetReachabilityVerifier.Instance.status);
	}

	private void OnDisable()
	{
		_userTypeDropdown.onValueChanged.RemoveListener(HandleUserTypeSelected);
		_sendButton.OnClick -= HandleSendPressed;
		InternetReachabilityVerifier.Instance.statusChangedDelegate -= HandleInternetConnection;
	}

	private void HandleUserTypeSelected(int userType)
	{
		currentUserType = (BloxelsMigrationUserType)userType;
	}

	private void HandleSendPressed()
	{
		if (currentUserType == BloxelsMigrationUserType.unchosen)
		{
			_warningText.text = "Please choose a user type";
			return;
		}
		if (!CheckEmailValidity(_emailInput))
		{
			_warningText.text = "Please enter a valid email";
			return;
		}
		_warningText.text = string.Empty;
		SendInfoToServer(_emailInput.text, currentUserType);
	}

	private void SendInfoToServer(string email, BloxelsMigrationUserType userType)
	{
		MigrationUserSubmission migrationUserSubmission = new MigrationUserSubmission();
		migrationUserSubmission.email = email;
		migrationUserSubmission.bbUserType = (int)userType;
		MigrationUserSubmission migrationUserSubmission2 = migrationUserSubmission;
		StartCoroutine(BloxelServerRequests.instance.SubmitMigrationRequest(migrationUserSubmission2, delegate
		{
			UnityEngine.Debug.Log("probably worked");
			_sendButtonText.text = "Sent!";
		}));
	}

	private void ShowNoInternetMenu()
	{
		_titlePrimary.text = "Please connect to WiFi";
		_body.text = "Please connect to the internet to read some important news from Bloxels. Thanks!";
		_emailInput.gameObject.SetActive(false);
		_userTypeDropdown.gameObject.SetActive(false);
		_sendButton.gameObject.SetActive(false);
		_migrationAgeGate.gameObject.SetActive(false);
		_sendButton.gameObject.SetActive(false);
		_sizzleRealContainer.SetActive(false);
	}

	private void PopulateMenuConent(NotificationsResponse.Notification notification)
	{
		_titlePrimary.text = notification.title;
		_body.text = notification.message;
		if (!notification.isV1)
		{
			_migrationLink = notification.migrationLink;
			_emailInput.gameObject.SetActive(false);
			_userTypeDropdown.gameObject.SetActive(false);
			_sendButton.gameObject.SetActive(false);
			_migrationAgeGate.gameObject.SetActive(true);
			_migrateButtonText.text = notification.migrationButtonText;
			_migrationAgeGate.SetOverrideLink(notification.migrationLink);
			_playVideoAgeGate.SetOverrideLink(notification.videoUrl);
			_sendButton.gameObject.SetActive(false);
			_sizzleRealContainer.gameObject.SetActive(true);
			StartCoroutine(FetchPNG(notification.imageUrl));
			if (!string.IsNullOrEmpty(notification.videoUrl) || notification.videoUrl == null)
			{
				_videoUrl = notification.videoUrl;
				_playVideoAgeGate.gameObject.SetActive(true);
			}
		}
		else
		{
			_emailInput.gameObject.SetActive(true);
			_userTypeDropdown.gameObject.SetActive(true);
			_sendButton.gameObject.SetActive(true);
			_migrationAgeGate.gameObject.SetActive(false);
			_sendButton.gameObject.SetActive(true);
			_sizzleRealContainer.SetActive(false);
		}
	}

	private void EstablishMenuContents()
	{
		StartCoroutine(BloxelServerRequests.instance.GetGluNotifications(delegate(string response)
		{
			NotificationsResponse notificationsResponse = JsonUtility.FromJson<NotificationsResponse>(response);
			if (!string.IsNullOrEmpty(notificationsResponse.data.title))
			{
				UnityEngine.Debug.Log("Got a notification with title: " + notificationsResponse.data.title);
				PopulateMenuConent(notificationsResponse.data);
			}
		}));
	}

	private void HandleInternetConnection(InternetReachabilityVerifier.Status newStatus)
	{
		if (newStatus == InternetReachabilityVerifier.Status.Offline)
		{
			ShowNoInternetMenu();
		}
		else
		{
			EstablishMenuContents();
		}
	}

	public bool CheckEmailValidity(TMP_InputField emailInput)
	{
		bool result = Regex.IsMatch(emailInput.text, "\\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\\Z", RegexOptions.IgnoreCase);
		if (string.IsNullOrEmpty(emailInput.text))
		{
			return false;
		}
		return result;
	}

	private IEnumerator FetchPNG(string imageURL)
	{
		using (WWW www = new WWW(imageURL))
		{
			yield return www;
			Texture2D pngTexture = www.texture;
			while (!www.isDone)
			{
				yield return null;
			}
			_rawImageForMedia.texture = pngTexture;
		}
	}

	public void ShowSizzleReal()
	{
		base.gameObject.SetActive(false);
		_navButtons.SetActive(false);
		if (_activeSizzleReal == null)
		{
			_activeSizzleReal = UnityEngine.Object.Instantiate(_sizzleRealPrefab);
			_activeSizzleReal.Init(this, _videoUrl);
		}
		else
		{
			_activeSizzleReal.Show();
		}
		UIOverlayNavMenu.instance.uiButtonDismiss.gameObject.SetActive(false);
	}

	public void HandleSizzleRealDismised()
	{
		_activeSizzleReal.Hide();
		base.gameObject.SetActive(true);
		_navButtons.SetActive(true);
		UIOverlayNavMenu.instance.uiButtonDismiss.gameObject.SetActive(true);
	}
}
