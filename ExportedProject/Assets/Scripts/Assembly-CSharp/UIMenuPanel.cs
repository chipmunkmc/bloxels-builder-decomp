using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;

public class UIMenuPanel : MonoBehaviour
{
	public delegate void HandleInit();

	[Header("Data")]
	public UIPopupMenu controller;

	[Header("UI")]
	public UIButton uiButtonDismiss;

	public UIMenuTitle uiMenuTitle;

	public Transform bgTransform;

	public Sequence initSequence;

	[Header("State Tracking")]
	public bool isIndex;

	public bool initComplete;

	private bool _overrideDismiss;

	public event HandleInit OnInit;

	public void Awake()
	{
		if (controller == null)
		{
			controller = GetComponentInParent<UIPopupMenu>();
		}
	}

	public void Start()
	{
		if (isIndex)
		{
			base.transform.localScale = Vector3.one;
			controller.OnInit += ControllerInitComplete;
		}
		uiButtonDismiss.OnClick += Dismiss;
	}

	public void OnDestroy()
	{
		if (isIndex)
		{
			controller.OnInit -= ControllerInitComplete;
		}
		uiButtonDismiss.OnClick -= Dismiss;
	}

	private void ControllerInitComplete()
	{
		Init();
	}

	public void OverrideDismiss()
	{
		uiButtonDismiss.OnClick -= Dismiss;
		_overrideDismiss = true;
	}

	public virtual void ResetUI()
	{
		uiMenuTitle.ResetUI();
		uiButtonDismiss.transform.localScale = Vector3.zero;
		bgTransform.localScale = Vector3.zero;
	}

	public virtual Sequence Init()
	{
		initSequence = DOTween.Sequence();
		initSequence.Append(bgTransform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		initSequence.Append(uiButtonDismiss.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		initSequence.Play().OnComplete(delegate
		{
			uiMenuTitle.Init();
			if (this.OnInit != null)
			{
				this.OnInit();
			}
		});
		return initSequence;
	}

	public virtual Tweener Hide()
	{
		return base.transform.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo);
	}

	public void FastHide()
	{
		base.transform.localScale = Vector3.zero;
	}

	public virtual Tweener Show()
	{
		return base.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo);
	}

	public virtual void Dismiss()
	{
		if (_overrideDismiss)
		{
			return;
		}
		base.transform.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnComplete(delegate
		{
			if (isIndex)
			{
				controller.OnInit -= ControllerInitComplete;
			}
			controller.Dismiss();
		});
	}
}
