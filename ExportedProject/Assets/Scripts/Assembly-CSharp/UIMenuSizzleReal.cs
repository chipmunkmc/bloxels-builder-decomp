using System.Collections;
using UnityEngine;

public class UIMenuSizzleReal : MonoBehaviour
{
	private UIMenuByeByeBloxels _controller;

	[SerializeField]
	private Transform _container;

	[SerializeField]
	private UIButton backButton;

	[SerializeField]
	private UIButton dismissButton;

	[SerializeField]
	private SizzleRealVideoPlayer _sizzleRealVideoPrefab;

	private string _videoURL;

	public void Init(UIMenuByeByeBloxels controller, string url)
	{
		_controller = controller;
		base.transform.SetParent(UIOverlayNavMenu.instance.transform);
		Show();
		backButton.OnClick += _controller.HandleSizzleRealDismised;
		dismissButton.OnClick += _controller.HandleSizzleRealDismised;
		_videoURL = url;
		StartCoroutine(StartVideo());
	}

	public void Show()
	{
		base.transform.localPosition = Vector3.zero;
		base.transform.localScale = Vector3.one;
	}

	public void Hide()
	{
		base.transform.localScale = Vector3.zero;
	}

	private void OnDisable()
	{
		backButton.OnClick -= _controller.HandleSizzleRealDismised;
		dismissButton.OnClick -= _controller.HandleSizzleRealDismised;
	}

	private IEnumerator StartVideo()
	{
		yield return new WaitForSeconds(1f);
		SizzleRealVideoPlayer sizzleRealVideo = Object.Instantiate(_sizzleRealVideoPrefab);
		sizzleRealVideo.transform.SetParent(_container.transform);
		sizzleRealVideo.transform.localPosition = Vector3.zero;
		sizzleRealVideo.transform.localScale = Vector3.one;
		sizzleRealVideo.videoPlayer.url = _videoURL;
		sizzleRealVideo.videoPlayer.Play();
	}
}
