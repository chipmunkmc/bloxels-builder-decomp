using DG.Tweening;
using TMPro;
using UnityEngine;

public class UIMenuTitle : MonoBehaviour
{
	[Tooltip("Not required")]
	public TextMeshProUGUI uiTextEyebrow;

	public TextMeshProUGUI uiTextPrimary;

	public Sequence initSequence;

	public bool shouldRotate;

	public void ResetUI()
	{
		base.transform.localScale = Vector3.zero;
		base.transform.DORotate(Vector3.zero, 0f);
		if (uiTextEyebrow != null)
		{
			uiTextEyebrow.DOFade(0f, 0f);
		}
		uiTextPrimary.DOFade(0f, 0f);
	}

	public void Init()
	{
		initSequence = DOTween.Sequence();
		initSequence.Append(base.transform.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo));
		if (uiTextEyebrow != null)
		{
			initSequence.Append(uiTextEyebrow.DOFade(1f, UIAnimationManager.speedMedium));
		}
		initSequence.Append(uiTextPrimary.DOFade(1f, UIAnimationManager.speedMedium));
		if (shouldRotate)
		{
			initSequence.Append(base.transform.DORotate(new Vector3(0f, 0f, 2f), UIAnimationManager.speedFast).SetEase(Ease.InOutElastic));
		}
		initSequence.Play();
	}
}
