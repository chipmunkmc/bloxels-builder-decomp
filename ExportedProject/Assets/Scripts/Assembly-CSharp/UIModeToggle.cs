using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIModeToggle : MonoBehaviour, IPointerDownHandler, IEventSystemHandler
{
	private Transform parent;

	private bool _isActive;

	public RectTransform rect;

	public CanvasMode mode;

	public Image burst;

	public Color inActiveColor;

	public Color activeColor;

	public Image btnState;

	public TextMeshProUGUI uiTextLabel;

	public Button uiButtonHelp;

	public UIButton uiButtonAddItem;

	public TextMeshProUGUI uiTMPAdd;

	public Image uiImageGamesIcon;

	public UIButtonIcon icon;

	public float activeScale;

	public float inActiveScale;

	public bool isActive
	{
		get
		{
			return _isActive;
		}
		set
		{
			_isActive = value;
			if (_isActive)
			{
				Activate();
			}
			else
			{
				DeActivate();
			}
		}
	}

	private void Awake()
	{
		parent = base.transform.parent;
		rect = GetComponent<RectTransform>();
		burst.DOFade(0f, 0f);
		uiTextLabel.transform.localScale = Vector3.zero;
		btnState.color = inActiveColor;
	}

	private void Start()
	{
		if (mode == CanvasMode.LevelEditor)
		{
			isActive = true;
		}
	}

	private void Activate()
	{
		btnState.DOColor(activeColor, UIAnimationManager.speedFast);
		burst.DOFade(1f, UIAnimationManager.speedMedium);
		uiTextLabel.transform.DOScale(1f, UIAnimationManager.speedMedium);
		icon.transform.DOScale(activeScale, UIAnimationManager.speedFast).OnComplete(delegate
		{
			icon.transform.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f);
		});
		SwitchAddButton();
	}

	private void DeActivate()
	{
		btnState.DOColor(inActiveColor, UIAnimationManager.speedFast);
		burst.DOFade(0f, UIAnimationManager.speedMedium);
		uiTextLabel.transform.DOScale(0f, UIAnimationManager.speedMedium);
		icon.transform.DOScale(inActiveScale, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
	}

	private void SwitchAddButton()
	{
		switch (mode)
		{
		case CanvasMode.LevelEditor:
			uiButtonAddItem.uiImageClickReceiver.color = ColorUtilities.blockRGB[BlockColor.Purple];
			uiImageGamesIcon.transform.localScale = new Vector3(0.25f, 0.25f, 0.25f);
			uiTMPAdd.transform.localScale = Vector3.zero;
			break;
		case CanvasMode.Animator:
			uiButtonAddItem.uiImageClickReceiver.color = ColorUtilities.blockRGB[BlockColor.Blue];
			uiImageGamesIcon.transform.localScale = Vector3.zero;
			uiTMPAdd.transform.localScale = Vector3.one;
			break;
		case CanvasMode.MegaBoard:
			uiButtonAddItem.uiImageClickReceiver.color = ColorUtilities.blockRGB[BlockColor.Green];
			uiImageGamesIcon.transform.localScale = Vector3.zero;
			uiTMPAdd.transform.localScale = Vector3.one;
			break;
		case CanvasMode.CharacterBuilder:
			uiButtonAddItem.uiImageClickReceiver.color = ColorUtilities.blockRGB[BlockColor.Red];
			uiImageGamesIcon.transform.localScale = Vector3.zero;
			uiTMPAdd.transform.localScale = Vector3.one;
			break;
		case CanvasMode.PaintBoard:
			uiButtonAddItem.uiImageClickReceiver.color = ColorUtilities.blockRGB[BlockColor.Orange];
			uiImageGamesIcon.transform.localScale = Vector3.zero;
			uiTMPAdd.transform.localScale = Vector3.one;
			break;
		}
	}

	public void OnPointerDown(PointerEventData eData)
	{
		if (PixelEditorController.instance.mode != mode)
		{
			PixelEditorController.instance.SwitchCanvasMode(mode);
		}
	}
}
