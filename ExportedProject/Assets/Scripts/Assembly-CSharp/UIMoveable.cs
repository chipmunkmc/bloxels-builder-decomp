using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;

public class UIMoveable : MonoBehaviour
{
	public delegate void HandleVisibilityChange(bool _isVisible);

	[Header("| ========= Movable ========= |")]
	public RectTransform rect;

	public Vector2 basePosition;

	public Vector2 hiddenPosition;

	public bool isVisible;

	public event HandleVisibilityChange OnVisibilityChange;

	public void Awake()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
	}

	public void Start()
	{
		base.gameObject.SetActive(false);
		isVisible = false;
	}

	public Tweener Move(Direction dir, Vector2 amountChange)
	{
		Tweener result = null;
		switch (dir)
		{
		case Direction.Up:
			result = rect.DOAnchorPos(new Vector2(basePosition.x, basePosition.y + amountChange.y), UIAnimationManager.speedMedium);
			break;
		case Direction.Right:
			result = rect.DOAnchorPos(new Vector2(basePosition.x + amountChange.x, basePosition.y), UIAnimationManager.speedMedium);
			break;
		case Direction.Down:
			result = rect.DOAnchorPos(new Vector2(basePosition.x, basePosition.y - amountChange.y), UIAnimationManager.speedMedium);
			break;
		case Direction.Left:
			result = rect.DOAnchorPos(new Vector2(basePosition.x - amountChange.x, basePosition.y), UIAnimationManager.speedMedium);
			break;
		}
		return result;
	}

	public void VisiblityChange()
	{
		if (this.OnVisibilityChange != null)
		{
			this.OnVisibilityChange(isVisible);
		}
	}

	public Tweener Show()
	{
		rect.DOKill();
		if (base.gameObject.activeInHierarchy)
		{
			return null;
		}
		Tweener tweener = null;
		base.gameObject.SetActive(true);
		tweener = rect.DOAnchorPos(basePosition, UIAnimationManager.speedMedium).SetEase(Ease.OutBack).OnStart(delegate
		{
			isVisible = true;
			VisiblityChange();
		});
		tweener.SetDelay(UIAnimationManager.speedMedium);
		return tweener;
	}

	public Tweener Hide()
	{
		rect.DOKill();
		if (!base.gameObject.activeInHierarchy)
		{
			return null;
		}
		Tweener tweener = null;
		return rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedMedium).SetEase(Ease.InBack).OnStart(delegate
		{
			isVisible = false;
			VisiblityChange();
		})
			.OnComplete(delegate
			{
				base.gameObject.SetActive(false);
			});
	}
}
