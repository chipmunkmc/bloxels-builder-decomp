using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class UIMusicChoice : MonoBehaviour
{
	public delegate void HandlePreview(bool isOn);

	public Sprite iconPlay;

	public Sprite iconStop;

	public Button uiButtonPreview;

	private bool _isPlaying;

	public UIRadioButton uiRadioButton;

	public MusicChoice choice;

	public bool isPlaying
	{
		get
		{
			return _isPlaying;
		}
		set
		{
			_isPlaying = value;
			if (this.OnPreview != null)
			{
				this.OnPreview(_isPlaying);
			}
		}
	}

	public event HandlePreview OnPreview;

	private void Start()
	{
		uiButtonPreview.onClick.AddListener(TogglePreview);
		uiRadioButton.OnClick += HandleOnClick;
	}

	private void HandleOnClick(UIRadioButton button)
	{
		UIGameSettings.instance.activeMusicChoice = choice;
		UIGameSettings.instance.Save();
	}

	private void TogglePreview()
	{
		if (isPlaying)
		{
			SoundManager.PlaySoundClip(SoundClip.cancelA);
			Stop(true);
		}
		else
		{
			SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
			Play();
		}
	}

	public void Play()
	{
		UIMusicChoice[] musicChoices = UIGameSettings.instance.musicChoices;
		foreach (UIMusicChoice uIMusicChoice in musicChoices)
		{
			if (uIMusicChoice.isPlaying)
			{
				uIMusicChoice.Stop(false);
			}
		}
		SoundManager.instance.PlayMusic(SoundManager.instance.musicChoiceLookup[choice]);
		isPlaying = true;
		uiButtonPreview.image.sprite = iconStop;
	}

	public void Stop(bool startEditorMusic)
	{
		if (startEditorMusic)
		{
			SoundManager.instance.PlayMusic(SoundManager.instance.editorMusic);
		}
		isPlaying = false;
		uiButtonPreview.image.sprite = iconPlay;
	}
}
