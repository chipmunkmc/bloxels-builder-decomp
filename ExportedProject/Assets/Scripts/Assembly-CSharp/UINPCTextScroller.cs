using UnityEngine;
using UnityEngine.UI;

public class UINPCTextScroller : MonoBehaviour
{
	public ScrollRect scrollRect;

	public RectTransform content;

	public Text uiTextNPCContent;

	public Image background;
}
