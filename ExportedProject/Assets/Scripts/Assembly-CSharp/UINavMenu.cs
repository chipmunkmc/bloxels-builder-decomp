using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UINavMenu : MonoBehaviour
{
	public static UINavMenu instance;

	[Header("UI")]
	public Button uiButtonTrigger;

	public GameObject menuNotifier;

	public GameObject menuWheelButtons;

	public RectTransform[] _buttonPositions;

	public bool canShowNavBarWheel;

	public RectTransform wheelParent;

	[Header("Prefabs")]
	public Object userMenuPrefab;

	public Object accountMenuPrefab;

	public Object ageGatePrefab;

	public Object newsPrefab;

	public Object settingsPrefab;

	public Object overlayMenuPrefab;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		userMenuPrefab = Resources.Load("Prefabs/UIPopupUser");
		accountMenuPrefab = Resources.Load("Prefabs/UIPopupAccount");
		newsPrefab = Resources.Load("Prefabs/UIPopupNews");
		ageGatePrefab = Resources.Load("Prefabs/UIPopupAgeGate");
		settingsPrefab = Resources.Load("Prefabs/UIPopupSettings");
		overlayMenuPrefab = Resources.Load("Prefabs/UIOverlayNavMenu");
		uiButtonTrigger.onClick.AddListener(Open);
		menuNotifier.SetActive(false);
	}

	private void Start()
	{
		PrefsManager.instance.OnUpdateNews += NewsUpdated;
		if (menuWheelButtons != null)
		{
			_buttonPositions = menuWheelButtons.GetComponentsInChildren<RectTransform>();
		}
	}

	private void OnDestroy()
	{
		PrefsManager.instance.OnUpdateNews -= NewsUpdated;
	}

	public void Open()
	{
		if (!(UIOverlayNavMenu.instance != null))
		{
			if (menuWheelButtons != null)
			{
				menuWheelButtons.transform.DOScale(0f, UIAnimationManager.speedMedium);
				canShowNavBarWheel = false;
			}
			if (!PrefsManager.instance.hasSeenNavMenu)
			{
				PrefsManager.instance.hasSeenNavMenu = true;
			}
			SoundManager.PlayOneShot(SoundManager.instance.flip);
			GameObject gameObject = UIOverlayCanvas.instance.Popup(overlayMenuPrefab);
		}
	}

	private void NewsUpdated()
	{
		menuNotifier.SetActive(true);
	}

	public void GoToHome()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		if (AppStateManager.instance.currentScene != SceneName.Home)
		{
			if ((bool)BloxelLibrary.instance)
			{
				BloxelLibrary.instance.libraryToggle.Hide();
			}
			BloxelsSceneManager.instance.GoTo(SceneName.Home);
		}
	}

	public void LoadEditor()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		if (AppStateManager.instance.currentScene != SceneName.PixelEditor_iPad)
		{
			if ((bool)BloxelLibrary.instance)
			{
				BloxelLibrary.instance.libraryToggle.Hide();
			}
			BloxelsSceneManager.instance.GoTo(SceneName.PixelEditor_iPad);
		}
	}

	public void LoadViewer()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		if (AppStateManager.instance.currentScene != SceneName.Viewer)
		{
			if ((bool)BloxelLibrary.instance)
			{
				BloxelLibrary.instance.libraryToggle.Hide();
			}
			BloxelsSceneManager.instance.GoTo(SceneName.Viewer);
		}
	}

	public void UserAction()
	{
		if (CurrentUser.instance.active)
		{
			GameObject gameObject = UIOverlayCanvas.instance.Popup(userMenuPrefab);
		}
		else if (InternetReachabilityVerifier.Instance.status != InternetReachabilityVerifier.Status.NetVerified)
		{
			BloxelsSceneManager.instance.ShowInternetMessage();
		}
		else
		{
			OpenAccountMenu();
		}
	}

	public void OpenAccountMenu()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(accountMenuPrefab);
		UIPopupAccount component = gameObject.GetComponent<UIPopupAccount>();
		if (CurrentUser.instance.account != null)
		{
			component.menuIndex.initComplete = true;
			StartCoroutine(DelayedSwitchMenu(component));
		}
	}

	private IEnumerator DelayedSwitchMenu(UIPopupAccount accountMenu)
	{
		while (accountMenu.isAnimating)
		{
			yield return new WaitForEndOfFrame();
		}
		accountMenu.OnInit += delegate
		{
			accountMenu.menuIndex.FastHide();
			if (CurrentUser.instance.account.status == UserAccount.Status.active)
			{
				accountMenu.registerController.SwitchStep(RegistrationController.Step.EmailPassword);
			}
			else
			{
				accountMenu.registerController.SwitchStep(RegistrationController.Step.VerifyCode);
			}
		};
	}

	public void OpenSettings()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(settingsPrefab);
	}

	public void OpenNews()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(newsPrefab);
		UIPopupNews component = gameObject.GetComponent<UIPopupNews>();
		component.Init();
		UIOverlayNavMenu.instance.newsNotifier.localScale = Vector3.zero;
	}

	public IEnumerator RotateMenuItemsWheel()
	{
		yield return new WaitForSeconds(30f);
		canShowNavBarWheel = true;
		menuWheelButtons.transform.DOScale(Vector3.one, UIAnimationManager.speedMedium);
	}
}
