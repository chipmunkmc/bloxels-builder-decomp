using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UINewsItem : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public NewsItem dataModel;

	[Header("| ========= UI ========= |")]
	public RawImage uiRawImage;

	public TextMeshProUGUI uiTextDate;

	public TextMeshProUGUI uiTextTitle;

	public Text uiTextBody;

	public UIButton uiButtonInteractive;

	public AgeGateTooltip tooltip;

	private void Start()
	{
		uiRawImage.color = Color.white;
	}

	private void OnDestroy()
	{
		if (dataModel.interactive != null && dataModel.type != 0)
		{
			uiButtonInteractive.OnClick += HandleWarp;
		}
	}

	public void Init(NewsItem item)
	{
		dataModel = item;
		uiTextDate.text = dataModel.date.ToString();
		uiTextTitle.text = dataModel.title;
		uiTextBody.text = dataModel.body;
		if (dataModel.interactive != null && dataModel.type != 0)
		{
			if (dataModel.type == NewsItem.Type.Link)
			{
				tooltip.SetOverrideLink(dataModel.interactive);
			}
			else
			{
				tooltip.enabled = false;
				uiButtonInteractive.OnClick += HandleWarp;
			}
		}
		else
		{
			uiButtonInteractive.transform.localScale = Vector3.zero;
		}
		StartCoroutine(DownloadTexture(dataModel.imageURL));
	}

	private IEnumerator DownloadTexture(string url)
	{
		WWW www = new WWW(url);
		yield return www;
		if (www.error == null && www.texture != null)
		{
			uiRawImage.texture = www.texture;
		}
	}

	public void HandleWarp()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		string[] array = dataModel.interactive.Split('|');
		int x = int.Parse(array[0]);
		int y = int.Parse(array[1]);
		if (AppStateManager.instance.currentScene == SceneName.Viewer)
		{
			UIWarpDrive.instance.ManualWarp(x, y);
		}
		else
		{
			PrefsManager.instance.iWallWarpLocation = dataModel.interactive;
			PrefsManager.instance.shouldActiveAfterWarp = true;
			BloxelsSceneManager.instance.GoTo(SceneName.Viewer);
		}
		UIPopupNews.instance.Kill();
	}
}
