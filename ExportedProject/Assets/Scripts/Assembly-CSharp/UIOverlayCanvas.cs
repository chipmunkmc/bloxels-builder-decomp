using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIOverlayCanvas : MonoBehaviour
{
	public enum SyncState
	{
		Waiting = 0,
		Syncing = 1,
		Borked = 2
	}

	public static UIOverlayCanvas instance;

	public Canvas canvas;

	public CanvasScaler canvasScaler;

	public RectTransform rect;

	public Camera overlayCamera;

	private Object _uiRectanglePrefab;

	private Object _ageGatePrefab;

	private Object _gameboardActivationPrefab;

	public Object uiPopupDebugPrefab;

	public Object uiPopupBackDoorPrefab;

	public Object gemInfoPrefab;

	private Object _syncGuestPrefab;

	public int staticChildCount;

	private Object _gemUnlockPrefab;

	public Object shareGamePrefab;

	public Object shareCoordinatePrefab;

	public Image uiImageSyncIndicator;

	public bool debugMode;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
			if (canvas == null)
			{
				canvas = GetComponent<Canvas>();
			}
			if (canvasScaler == null)
			{
				canvasScaler = GetComponent<CanvasScaler>();
			}
			if (rect == null)
			{
				rect = GetComponent<RectTransform>();
			}
			if (overlayCamera == null)
			{
				overlayCamera = GetComponentInChildren<Camera>();
			}
			canvas.worldCamera = overlayCamera;
			_uiRectanglePrefab = Resources.Load("Prefabs/UI/UIRectangle");
			_ageGatePrefab = Resources.Load("Prefabs/UIPopupAgeGate");
			_gemUnlockPrefab = Resources.Load("Prefabs/UIPopupGemUnlock");
			_gameboardActivationPrefab = Resources.Load("Prefabs/UIPopupBoardActivation");
			_syncGuestPrefab = Resources.Load("Prefabs/UIPopupSyncGuest");
			gemInfoPrefab = Resources.Load("Prefabs/UIPopupGemInfo");
			staticChildCount = base.transform.childCount;
			if (!debugMode)
			{
				uiImageSyncIndicator.transform.localScale = Vector3.zero;
			}
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
	}

	private void Start()
	{
		BloxelServerInterface.instance.OnBackDoor += HandleBackDoor;
	}

	private void OnDestroy()
	{
		BloxelServerInterface.instance.OnBackDoor -= HandleBackDoor;
	}

	public Tweener TryBlur(Graphic overlay)
	{
		overlay.color = new Color(0f, 0f, 0f, 0f);
		if (BloxelCamera.instance != null && !DeviceManager.isPoopDevice)
		{
			return BloxelCamera.instance.Blur(overlay);
		}
		if (InfinityWallCamera.instance != null && !DeviceManager.isPoopDevice)
		{
			return InfinityWallCamera.instance.Blur(overlay);
		}
		return overlay.DOColor(new Color(0f, 0f, 0f, 0.7f), UIAnimationManager.speedFast);
	}

	public void TryUnBlur(Graphic overlay, GameObject objectToDestroy)
	{
		if (BloxelCamera.instance != null && !DeviceManager.isPoopDevice)
		{
			BloxelCamera.instance.DeBlur(overlay).OnComplete(delegate
			{
				Object.Destroy(objectToDestroy);
			});
		}
		else if (InfinityWallCamera.instance != null && !DeviceManager.isPoopDevice)
		{
			InfinityWallCamera.instance.DeBlur(overlay).OnComplete(delegate
			{
				Object.Destroy(objectToDestroy);
			});
		}
		else
		{
			Object.Destroy(objectToDestroy);
		}
	}

	public void SetSyncState(SyncState state)
	{
		if (debugMode)
		{
			switch (state)
			{
			case SyncState.Waiting:
				uiImageSyncIndicator.color = AssetManager.instance.bloxelYellow;
				break;
			case SyncState.Syncing:
				uiImageSyncIndicator.color = AssetManager.instance.bloxelGreen;
				break;
			case SyncState.Borked:
				uiImageSyncIndicator.color = AssetManager.instance.bloxelRed;
				break;
			}
		}
	}

	private void HandleBackDoor(string message)
	{
		BackdoorMessage(message);
	}

	public UIPopupSyncGuest SyncGuestPopup()
	{
		GameObject gameObject = Popup(_syncGuestPrefab);
		return gameObject.GetComponent<UIPopupSyncGuest>();
	}

	public UIPopupBoardActivation GameBoardActivation()
	{
		GameObject gameObject = Popup(_gameboardActivationPrefab);
		UIPopupBoardActivation component = gameObject.GetComponent<UIPopupBoardActivation>();
		CaptureManager.instance.gameboardActivationInProgress = true;
		return component;
	}

	public GameObject Popup(Object prefab)
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		GameObject gameObject = Object.Instantiate(prefab) as GameObject;
		gameObject.transform.SetParent(base.transform);
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localPosition = Vector3.zero;
		RectTransform component = gameObject.GetComponent<RectTransform>();
		component.anchoredPosition = Vector2.zero;
		component.sizeDelta = Vector2.zero;
		overlayCamera.transform.SetAsFirstSibling();
		return gameObject;
	}

	public UIPopupDebugPanel DLogMessage(string message)
	{
		GameObject gameObject = Popup(uiPopupDebugPrefab);
		UIPopupDebugPanel component = gameObject.GetComponent<UIPopupDebugPanel>();
		component.Message(message);
		return component;
	}

	public UIPopupBackDoor BackdoorMessage(string message)
	{
		GameObject gameObject = Popup(uiPopupBackDoorPrefab);
		UIPopupBackDoor component = gameObject.GetComponent<UIPopupBackDoor>();
		component.Init(message);
		return component;
	}

	public UIPopupGemUnlock GemBuyPopup(GemUnlockable model)
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		GameObject gameObject = Object.Instantiate(_gemUnlockPrefab) as GameObject;
		gameObject.transform.SetParent(base.transform);
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localPosition = Vector3.zero;
		RectTransform component = gameObject.GetComponent<RectTransform>();
		component.anchoredPosition = Vector2.zero;
		component.sizeDelta = Vector2.zero;
		overlayCamera.transform.SetAsFirstSibling();
		UIPopupGemUnlock component2 = gameObject.GetComponent<UIPopupGemUnlock>();
		component2.Init(model);
		return component2;
	}

	public UIRectangle GenerateRectangle(RectPosition rectPos, Vector2 size, Color color, bool animate = false, bool blockInput = true)
	{
		GameObject gameObject = Object.Instantiate(_uiRectanglePrefab) as GameObject;
		gameObject.transform.SetParent(base.transform);
		gameObject.transform.localScale = Vector3.one;
		gameObject.transform.localPosition = Vector3.zero;
		UIRectangle component = gameObject.GetComponent<UIRectangle>();
		component.Init(rectPos, size, color, animate, blockInput);
		overlayCamera.transform.SetAsFirstSibling();
		return component;
	}
}
