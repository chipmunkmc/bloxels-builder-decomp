using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIOverlayNavMenu : MonoBehaviour
{
	public delegate void HandleDestroy();

	public static UIOverlayNavMenu instance;

	public RectTransform menuIndex;

	public UIButton uiButtonDismiss;

	public UIButton uiButtonHome;

	public UIButton uiButtonCanvas;

	public UIButton uiButtonEditor;

	public UIButton uiButtonSettings;

	public UIButton uiButtonNews;

	public UIButton uiButtonPoultryPanic;

	public UIButton uiButtonFeatured;

	public RectTransform[] rectSmallLabels;

	public RectTransform[] rectLargeLabels;

	public RectTransform newsNotifier;

	public RawImage overlay;

	public RectTransform rectButtons;

	public RectTransform rectNavigationText;

	private UIButton[] _menuButtons;

	private Dictionary<UIButton, Vector2> buttonPositions;

	public GameObject earnPoultryPanicGemsLabels;

	public GameObject earnFeaturedGemsLabels;

	public event HandleDestroy onDestroy;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (overlay == null)
		{
			overlay = GetComponent<RawImage>();
		}
		newsNotifier.localScale = Vector3.zero;
		_menuButtons = rectButtons.GetComponentsInChildren<UIButton>();
		buttonPositions = new Dictionary<UIButton, Vector2>(_menuButtons.Length);
		if (GameHUD.instance != null)
		{
			GameHUD.instance.touchControls.localScale = Vector3.zero;
		}
	}

	private void Start()
	{
		uiButtonHome.OnClick += HandleHomePressed;
		uiButtonCanvas.OnClick += HandleIWallPressed;
		uiButtonEditor.OnClick += HandleEditorPressed;
		uiButtonSettings.OnClick += HandleSettingsPressed;
		uiButtonNews.OnClick += HandleNewsPress;
		uiButtonDismiss.OnClick += HandleDismissPressed;
		uiButtonPoultryPanic.OnClick += HandlePoultryPanicPressed;
		uiButtonFeatured.OnClick += HandleFeaturedPressed;
		for (int i = 0; i < _menuButtons.Length; i++)
		{
			buttonPositions.Add(_menuButtons[i], _menuButtons[i].selfRect.anchoredPosition);
			_menuButtons[i].selfRect.anchoredPosition = Vector2.zero;
			_menuButtons[i].uiCanvasGroup.alpha = 0f;
		}
		if (UINavMenu.instance.menuNotifier.activeInHierarchy)
		{
			newsNotifier.localScale = Vector3.one;
		}
		PrefsManager.instance.OnUpdateNews += NewsUpdated;
		UIOverlayCanvas.instance.TryBlur(overlay).OnComplete(delegate
		{
			if (HomeController.instance != null)
			{
				HomeController.instance.HideForOverlay();
			}
			animateIn();
		});
		if (PrefsManager.instance.HasGemsForType(GemEarnable.Type.PoultryPanicV1))
		{
			earnPoultryPanicGemsLabels.transform.localScale = Vector3.zero;
		}
	}

	public void OnDestroy()
	{
		if (HomeController.instance != null)
		{
			HomeController.instance.ShowAfterOverlay();
		}
		PrefsManager.instance.OnUpdateNews -= NewsUpdated;
		uiButtonHome.OnClick -= HandleHomePressed;
		uiButtonCanvas.OnClick -= HandleIWallPressed;
		uiButtonEditor.OnClick -= HandleEditorPressed;
		uiButtonSettings.OnClick -= HandleSettingsPressed;
		uiButtonNews.OnClick -= HandleNewsPress;
		uiButtonDismiss.OnClick -= HandleDismissPressed;
		uiButtonPoultryPanic.OnClick -= HandlePoultryPanicPressed;
		uiButtonFeatured.OnClick -= HandleFeaturedPressed;
		if (this.onDestroy != null)
		{
			this.onDestroy();
		}
	}

	private void animateIn()
	{
		Sequence sequence = DOTween.Sequence();
		Sequence smallLabelSeq = DOTween.Sequence();
		Sequence largeLabelSeq = DOTween.Sequence();
		for (int i = 0; i < _menuButtons.Length; i++)
		{
			UIButton btn = _menuButtons[i];
			sequence.Append(btn.selfRect.DOAnchorPos(buttonPositions[btn], UIAnimationManager.speedFast).OnStart(delegate
			{
				SoundManager.PlayOneShot(SoundManager.instance.boardRefresh);
				btn.uiCanvasGroup.DOFade(1f, UIAnimationManager.speedFast);
			}).SetEase(Ease.InExpo)
				.OnComplete(delegate
				{
					btn.selfRect.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f);
				}));
		}
		smallLabelSeq.SetDelay(UIAnimationManager.speedSlow);
		largeLabelSeq.SetDelay(UIAnimationManager.speedSlow);
		for (int j = 0; j < rectSmallLabels.Length; j++)
		{
			smallLabelSeq.Append(rectSmallLabels[j].DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		}
		for (int k = 0; k < rectLargeLabels.Length; k++)
		{
			largeLabelSeq.Append(rectLargeLabels[k].DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		}
		sequence.Play().OnComplete(delegate
		{
			SoundManager.PlayOneShot(SoundManager.instance.magicAppear);
			StartCoroutine(DelayedGemLabelVanish());
			smallLabelSeq.Play().OnComplete(delegate
			{
				largeLabelSeq.Play();
			});
		});
	}

	private void animateOut()
	{
		for (int i = 0; i < rectSmallLabels.Length; i++)
		{
			rectSmallLabels[i].localScale = Vector3.zero;
		}
		for (int j = 0; j < rectLargeLabels.Length; j++)
		{
			rectLargeLabels[j].localScale = Vector3.zero;
		}
		for (int k = 0; k < _menuButtons.Length; k++)
		{
			UIButton btn = _menuButtons[k];
			btn.selfRect.DOAnchorPos(Vector2.zero, UIAnimationManager.speedFast).SetEase(Ease.OutExpo).OnStart(delegate
			{
				btn.uiCanvasGroup.DOFade(0f, UIAnimationManager.speedFast);
			});
		}
	}

	private void HandlePoultryPanicPressed()
	{
		if ((GameplayController.instance != null && (GameplayController.instance.currentGame == null || GameplayController.instance.currentGame.id != AssetManager.instance.poultryPanicGame.id)) || AppStateManager.instance.currentScene == SceneName.Viewer)
		{
			BloxelsSceneManager.instance.GameplayWithGame(AssetManager.instance.poultryPanicGame);
		}
	}

	private void HandleFeaturedPressed()
	{
		if (AppStateManager.instance.currentScene == SceneName.Viewer)
		{
			FeaturedSquaresController.instance.Show();
			Dismiss();
		}
		else
		{
			AppStateManager.ActivateFeatured = true;
			UINavMenu.instance.LoadViewer();
		}
	}

	private void HandleHomePressed()
	{
		if (AppStateManager.instance.currentScene != SceneName.Home)
		{
			UINavMenu.instance.GoToHome();
		}
	}

	private void HandleIWallPressed()
	{
		if (AppStateManager.instance.currentScene != SceneName.Viewer)
		{
			UINavMenu.instance.LoadViewer();
		}
	}

	private void HandleEditorPressed()
	{
		if (AppStateManager.instance.currentScene != SceneName.PixelEditor_iPad)
		{
			UINavMenu.instance.LoadEditor();
		}
	}

	private void HandleNewsPress()
	{
		UINavMenu.instance.OpenNews();
		Dismiss(false);
	}

	private void HandleDismissPressed()
	{
		Dismiss();
	}

	private void HandleSettingsPressed()
	{
		UINavMenu.instance.OpenSettings();
		Dismiss(false);
	}

	private void NewsUpdated()
	{
		newsNotifier.localScale = Vector3.one;
	}

	public void Dismiss(bool removeBlur = true)
	{
		SoundManager.PlayOneShot(SoundManager.instance.cancelA);
		if (GameHUD.instance != null)
		{
			GameHUD.instance.touchControls.localScale = Vector3.one;
		}
		animateOut();
		StartCoroutine(DelayedDestroy(removeBlur));
	}

	private IEnumerator DelayedDestroy(bool removeBlur)
	{
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		if (!removeBlur)
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		else
		{
			UIOverlayCanvas.instance.TryUnBlur(overlay, base.gameObject);
		}
	}

	private IEnumerator DelayedGemLabelVanish()
	{
		yield return new WaitForSeconds(6f);
		earnPoultryPanicGemsLabels.transform.DOScale(0f, UIAnimationManager.speedMedium);
		earnFeaturedGemsLabels.transform.DOScale(0f, UIAnimationManager.speedMedium);
	}
}
