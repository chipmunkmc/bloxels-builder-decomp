using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIPopup : MonoBehaviour
{
	public delegate void HandleDestroy();

	public static UIPopup instance;

	protected Vector3 menuIndexStartScale;

	protected Vector3 menuIndexTargetScale;

	public Transform menuIndex;

	public Button uiButtonDismiss;

	public Text uiTextTitle;

	public RawImage overlay;

	public event HandleDestroy onDestroy;

	public void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			UnityEngine.Object.Destroy(base.gameObject);
		}
		if (overlay == null)
		{
			overlay = GetComponent<RawImage>();
		}
		menuIndexStartScale = Vector3.zero;
		menuIndexTargetScale = Vector3.one;
		menuIndex.transform.localScale = menuIndexStartScale;
	}

	public void Start()
	{
		if (BloxelCamera.instance != null && !DeviceManager.isPoopDevice)
		{
			BloxelCamera.instance.Blur(overlay).OnComplete(delegate
			{
				menuIndex.DOScale(menuIndexTargetScale, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
			});
		}
		else if (InfinityWallCamera.instance != null && !DeviceManager.isPoopDevice)
		{
			InfinityWallCamera.instance.Blur(overlay).OnComplete(delegate
			{
				menuIndex.DOScale(menuIndexTargetScale, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
			});
		}
		else
		{
			overlay.color = new Color(0f, 0f, 0f, 0f);
			overlay.DOColor(new Color(0f, 0f, 0f, 0.7f), UIAnimationManager.speedFast).OnComplete(delegate
			{
				menuIndex.DOScale(menuIndexTargetScale, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
			});
		}
		uiButtonDismiss.onClick.AddListener(ButtonDismissOnClick);
	}

	public void OnDestroy()
	{
		if (this.onDestroy != null)
		{
			this.onDestroy();
		}
	}

	private void ButtonDismissOnClick()
	{
		Dismiss();
	}

	public virtual void Dismiss(bool playSound = true, bool removeBlur = true)
	{
		if (playSound)
		{
			SoundManager.PlayOneShot(SoundManager.instance.cancelA);
		}
		menuIndex.DOScale(menuIndexStartScale, UIAnimationManager.speedFast).SetEase(Ease.InExpo).OnComplete(delegate
		{
			if (removeBlur)
			{
				if (BloxelCamera.instance != null && !DeviceManager.isPoopDevice)
				{
					BloxelCamera.instance.DeBlur(overlay).OnComplete(delegate
					{
						UnityEngine.Object.Destroy(base.gameObject);
					});
				}
				else if (InfinityWallCamera.instance != null && !DeviceManager.isPoopDevice)
				{
					InfinityWallCamera.instance.DeBlur(overlay).OnComplete(delegate
					{
						UnityEngine.Object.Destroy(base.gameObject);
					});
				}
				else
				{
					UnityEngine.Object.Destroy(base.gameObject);
				}
			}
			else
			{
				UnityEngine.Object.Destroy(base.gameObject);
			}
		});
	}
}
