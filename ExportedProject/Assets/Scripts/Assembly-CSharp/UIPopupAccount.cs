using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;

public class UIPopupAccount : UIPopupMenu
{
	public delegate void HandleRegisterSuccess();

	public delegate void HandleLoginSuccess();

	public delegate void HandleGuestLogin();

	public RegistrationController registerController;

	public UIInfinityWallWelcome menuIndex;

	[Header("Guest")]
	public ContinueAsGuest guestMenu;

	public event HandleRegisterSuccess OnRegisterSuccess;

	public event HandleLoginSuccess OnLoginSuccess;

	public event HandleGuestLogin OnGuestLogin;

	private new void Start()
	{
		base.Start();
		if (HomeController.instance != null)
		{
			HomeController.instance.HideForOverlay();
		}
		guestMenu.Init();
	}

	private void OnDestroy()
	{
		if (HomeController.instance != null)
		{
			HomeController.instance.ShowAfterOverlay();
		}
	}

	public void RegisterComplete()
	{
		if (this.OnRegisterSuccess != null)
		{
			this.OnRegisterSuccess();
		}
		CurrentUser.instance.SwitchAccounts();
		UnityEngine.Object.Destroy(base.gameObject);
	}

	public void LoginComplete()
	{
		if (this.OnLoginSuccess != null)
		{
			this.OnLoginSuccess();
		}
		if (CurrentUser.instance.account.eduAccount != UserAccount.EduStatus.ReleasedInComplete)
		{
			CurrentUser.instance.SwitchAccounts();
			UnityEngine.Object.Destroy(base.gameObject);
			return;
		}
		registerController.chosenUserName = CurrentUser.instance.account.userName;
		registerController.avatarBoardId = CurrentUser.instance.account.avatar._serverID;
		registerController.parentEmail = CurrentUser.instance.account.parentEmail;
		registerController.isOfAge = false;
		registerController.isFormerEdu = true;
		menuIndex.Hide().OnComplete(delegate
		{
			registerController.SwitchStep(RegistrationController.Step.ParentEmail);
		});
	}

	public void GuestLogin()
	{
		if (this.OnGuestLogin != null)
		{
			this.OnGuestLogin();
		}
		Dismiss();
	}
}
