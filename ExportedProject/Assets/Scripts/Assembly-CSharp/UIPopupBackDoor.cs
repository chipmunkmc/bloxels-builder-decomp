using TMPro;

public class UIPopupBackDoor : UIPopupMenu
{
	public TextMeshProUGUI uiTextMessage;

	public void Init(string message)
	{
		uiTextMessage.SetText(message);
	}
}
