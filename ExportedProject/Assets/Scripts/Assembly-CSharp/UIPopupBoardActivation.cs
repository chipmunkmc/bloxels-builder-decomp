using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using DiscoCapture;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupBoardActivation : MonoBehaviour
{
	public delegate void HandleDestroy();

	public static UIPopupBoardActivation instance;

	[Header("Popup")]
	protected Vector3 menuIndexStartScale;

	protected Vector3 menuIndexTargetScale;

	public Transform menuIndex;

	public RawImage overlay;

	public static BloxelBoard boardConstant;

	public RectTransform panelTransform;

	public RectTransform rectBackground;

	public RectTransform rectTitleBar;

	public RectTransform boardContainer;

	public RawImage boardImage;

	public Image boardBorder;

	public UIButton uiButtonDismiss;

	public CoverBoard cover;

	public Vector2 startingBoardPosition;

	public Vector2 currentBoardPosition;

	private static int[] _blockCounts = new int[8];

	private static int _maxNumberOfSameColoredBlocks = 1;

	public float requiredAccuracy;

	public BoardValidationStep currentStep;

	public BoardVerificationStep[] steps;

	public float transitionDelay;

	public bool fromGamebuilder;

	public bool dismissedCapture;

	public event HandleDestroy onDestroy;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (overlay == null)
		{
			overlay = GetComponent<RawImage>();
		}
		menuIndexStartScale = Vector3.zero;
		menuIndexTargetScale = Vector3.one;
		menuIndex.transform.localScale = menuIndexStartScale;
		uiButtonDismiss.OnClick += DismissPressed;
		GenerateBoardFromRandomQuadrant();
		cover = new CoverBoard(new CoverStyle(BloxelBoard.baseUIBoardColor, 4, 2, 1, true));
		boardImage.texture = boardConstant.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
		steps = GetComponentsInChildren<BoardVerificationStep>();
		startingBoardPosition = boardContainer.anchoredPosition;
		boardImage.DOFade(0f, 0f);
		boardBorder.DOFade(0f, 0f);
	}

	private void Start()
	{
		overlay.color = new Color(0f, 0f, 0f, 0f);
		if (!DeviceManager.isPoopDevice)
		{
			BloxelCamera.instance.Blur(overlay).OnComplete(delegate
			{
				menuIndex.DOScale(menuIndexTargetScale, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce).OnStart(delegate
				{
					SoundManager.instance.PlaySound(SoundManager.instance.menuPopup);
				})
					.OnComplete(delegate
					{
						ShowValidationPanelAtStep(BoardValidationStep.Splash);
					});
			});
		}
		else
		{
			overlay.DOColor(new Color(0f, 0f, 0f, 0.7f), UIAnimationManager.speedFast).OnComplete(delegate
			{
				menuIndex.DOScale(menuIndexTargetScale, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce).OnStart(delegate
				{
					SoundManager.instance.PlaySound(SoundManager.instance.menuPopup);
				})
					.OnComplete(delegate
					{
						ShowValidationPanelAtStep(BoardValidationStep.Splash);
					});
			});
		}
		SetupCaptureEvents();
	}

	private void OnDestroy()
	{
		CaptureManager.instance.gameboardActivationInProgress = false;
		uiButtonDismiss.OnClick -= DismissPressed;
		CaptureManager.instance.OnCaptureConfirm -= HandleCaptureConfirm;
		CaptureManager.instance.OnCaptureDismiss -= HandleCaptureDismiss;
		CaptureManager.instance.OnCaptureRepeat -= HandleCaptureRepeat;
		if (this.onDestroy != null)
		{
			this.onDestroy();
		}
	}

	public void SetupCaptureEvents()
	{
		CaptureManager.instance.OnCaptureConfirm += HandleCaptureConfirm;
		CaptureManager.instance.OnCaptureDismiss += HandleCaptureDismiss;
		CaptureManager.instance.OnCaptureRepeat += HandleCaptureRepeat;
	}

	private void HandleCaptureConfirm()
	{
		ReturnFromCapture();
	}

	private void HandleCaptureDismiss()
	{
		StartCoroutine(SmoothTransition());
	}

	private void HandleCaptureRepeat()
	{
	}

	public void RefreshBoard()
	{
		GenerateBoardFromRandomQuadrant();
		boardImage.texture = boardConstant.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
	}

	public void ResetPreviewBoard()
	{
		boardImage.DOFade(0f, 0f);
		boardImage.transform.localScale = Vector3.one;
		boardContainer.anchoredPosition = startingBoardPosition;
	}

	public void ShowValidationPanelAtStep(BoardValidationStep step)
	{
		for (int i = 0; i < steps.Length; i++)
		{
			if (steps[i].step == step)
			{
				steps[i].Activate();
			}
			else
			{
				steps[i].DeActivate();
			}
		}
		currentStep = step;
	}

	public void GenerateBoard()
	{
		boardConstant = new BloxelBoard();
		int num = UnityEngine.Random.Range(1, 4);
		int num2 = 6 - num;
		int num3 = 6 + num;
		int num4 = UnityEngine.Random.Range(0, 5);
		for (int i = num2; i < boardConstant.blockColors.GetLength(0) - num2; i++)
		{
			for (int j = num2; j < boardConstant.blockColors.GetLength(1) - num2; j++)
			{
				if (boardConstant.blockColors[i, j] == BlockColor.Blank && CheckLines(i, j))
				{
					boardConstant.blockColors[i, j] = (BlockColor)num4;
				}
			}
		}
	}

	private bool CheckLines(int x, int y)
	{
		if (y == 6)
		{
			return true;
		}
		if (x == 6)
		{
			return true;
		}
		if (x == y)
		{
			return true;
		}
		if (y == -x + 12)
		{
			return true;
		}
		return false;
	}

	private void GenerateBoardFromRandomQuadrant()
	{
		boardConstant = new BloxelBoard();
		GenerateRandomLowerLeftQuadrant();
		MirrorLeftToRightAxisY();
		MirrorBottomToTopAxisX();
	}

	private void GenerateLowerLeftQuadrant()
	{
		Array.Clear(_blockCounts, 0, _blockCounts.Length);
		float num = 0.9875f;
		int num2 = 0;
		bool flag = false;
		BlockColor blockColor = BlockColor.Blank;
		for (int i = 0; i < 7; i++)
		{
			if (flag)
			{
				break;
			}
			for (int j = 0; j < 7; j++)
			{
				if (flag)
				{
					break;
				}
				if (j == i)
				{
					num *= 0.9875f;
				}
				if (UnityEngine.Random.Range(0f, 1f) < num)
				{
					boardConstant.blockColors[j, i] = BlockColor.Blank;
					continue;
				}
				blockColor = (BlockColor)UnityEngine.Random.Range(0, 7);
				while (_blockCounts[(uint)blockColor] == _maxNumberOfSameColoredBlocks)
				{
					blockColor = (BlockColor)UnityEngine.Random.Range(0, 7);
				}
				boardConstant.blockColors[j, i] = blockColor;
				_blockCounts[(uint)blockColor]++;
				num2++;
				if (num2 == 4)
				{
					flag = true;
				}
			}
		}
		if (!flag)
		{
			GenerateLowerLeftQuadrant();
		}
	}

	private void GenerateRandomLowerLeftQuadrant()
	{
		Array.Clear(_blockCounts, 0, _blockCounts.Length);
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				boardConstant.blockColors[j, i] = BlockColor.Blank;
			}
		}
		int num = UnityEngine.Random.Range(0, 7);
		int num2 = UnityEngine.Random.Range(0, 7);
		for (int k = 0; k < 4; k++)
		{
			while (boardConstant.blockColors[num, num2] != BlockColor.Blank)
			{
				num = UnityEngine.Random.Range(0, 7);
				num2 = UnityEngine.Random.Range(0, 7);
			}
			BlockColor blockColor = (BlockColor)UnityEngine.Random.Range(0, 7);
			while (_blockCounts[(uint)blockColor] >= _maxNumberOfSameColoredBlocks)
			{
				blockColor = (BlockColor)UnityEngine.Random.Range(0, 7);
			}
			_blockCounts[(uint)blockColor]++;
			boardConstant.blockColors[num, num2] = blockColor;
		}
	}

	private void MirrorLeftToRightAxisY()
	{
		for (int i = 0; i < 7; i++)
		{
			for (int j = 0; j < 6; j++)
			{
				boardConstant.blockColors[12 - j, i] = boardConstant.blockColors[j, i];
			}
		}
	}

	private void MirrorBottomToTopAxisX()
	{
		for (int i = 0; i < 6; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				boardConstant.blockColors[j, 12 - i] = boardConstant.blockColors[j, i];
			}
		}
	}

	public bool IsMatch(BlockColor[,] constant, BlockColor[,] attempt)
	{
		if (constant.Length != attempt.Length)
		{
			return false;
		}
		return true;
	}

	public void ReturnFromCapture()
	{
		bool flag = VerifyCapturedBoard(CaptureManager.latest);
		if (flag)
		{
			PrefsManager.instance.userOwnsBoard = flag;
		}
		StartCoroutine(SmoothTransition());
	}

	private IEnumerator SmoothTransition()
	{
		while (CaptureController.Instance.deviceCamera.webcamTexture.isPlaying)
		{
			yield return new WaitForEndOfFrame();
		}
		yield return new WaitForSeconds(0.5f);
		ShowValidationPanelAtStep(BoardValidationStep.Validation);
	}

	public bool VerifyCapturedBoard(BlockColor[,] capturedBlockColors)
	{
		int num = 0;
		int num2 = 0;
		for (int i = 0; i < 13; i++)
		{
			for (int j = 0; j < 13; j++)
			{
				BlockColor blockColor = boardConstant.blockColors[j, i];
				BlockColor blockColor2 = capturedBlockColors[j, i];
				if ((blockColor != BlockColor.Blank && blockColor2 == BlockColor.Blank) || (blockColor == BlockColor.Blank && blockColor2 != BlockColor.Blank))
				{
					return false;
				}
				if (blockColor != BlockColor.Blank)
				{
					num++;
					if (blockColor == blockColor2)
					{
						num2++;
					}
				}
			}
		}
		float num3 = (float)num2 / (float)num;
		return (num3 >= requiredAccuracy) ? true : false;
	}

	private void DismissPressed()
	{
		Dismiss();
	}

	public Tweener Dismiss(bool removeBlur = true)
	{
		SoundManager.PlayOneShot(SoundManager.instance.cancelA);
		return menuIndex.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InExpo).OnComplete(delegate
		{
			if (removeBlur && !DeviceManager.isPoopDevice)
			{
				BloxelCamera.instance.DeBlur(overlay).OnComplete(delegate
				{
					UnityEngine.Object.Destroy(base.gameObject);
				});
			}
			else
			{
				UnityEngine.Object.Destroy(base.gameObject);
			}
		});
	}
}
