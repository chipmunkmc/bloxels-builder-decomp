using System;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupBuildLayout : UIPopupMenu
{
	public delegate void HandleCompete();

	public TextMeshProUGUI uiTextDescription;

	public RectTransform rectButtonComplete;

	public RectTransform rectStepIndicator;

	public RectTransform rectHelperInfo;

	public UIBoard3D uiBoard3D;

	public UIButton _uiButtonComplete;

	private TextMeshProUGUI _uiTextStep;

	private Text _uiTextHelper;

	private string _titleString;

	private string _descriptionString;

	private string _stepString;

	private string _helperString;

	public event HandleCompete OnComplete;

	private new void Awake()
	{
		base.Awake();
		_uiTextStep = rectStepIndicator.GetComponentInChildren<TextMeshProUGUI>();
		_uiTextHelper = rectHelperInfo.GetComponentInChildren<Text>();
		_uiButtonComplete.OnClick += Complete;
		Reset();
	}

	private new void Start()
	{
		base.Start();
		SoundManager.instance.PlaySound(SoundManager.instance.menuPopup);
	}

	private void Reset()
	{
		uiTextDescription.text = string.Empty;
		_uiTextHelper.text = string.Empty;
		uiBoard3D.transform.localScale = new Vector3(0f, 0f, 1f);
		rectButtonComplete.transform.localScale = Vector3.zero;
		rectStepIndicator.transform.localScale = Vector3.zero;
	}

	public void Init(BloxelBoard board, string title, string description, string step, string helper)
	{
		_titleString = title;
		_descriptionString = description;
		_stepString = step;
		_helperString = helper;
		_uiTextStep.text = _stepString;
		IntroSequence().Play().OnComplete(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.magicAppear);
			uiBoard3D.Init(board);
			_uiTextHelper.DOText(_helperString, UIAnimationManager.speedSlow).SetEase(Ease.OutExpo);
		});
	}

	private Sequence IntroSequence()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(uiBoard3D.transform.DOScaleX(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			SoundManager.instance.PlayDelayedSound(UIAnimationManager.speedFast, SoundManager.instance.appearance);
		}));
		sequence.Append(uiBoard3D.transform.DOScaleY(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			SoundManager.instance.PlayDelayedSound(UIAnimationManager.speedFast, SoundManager.instance.appearance);
		}));
		uiTextDescription.text = _descriptionString;
		sequence.Append(rectButtonComplete.transform.DOScale(1f, UIAnimationManager.speedMedium));
		sequence.Append(rectButtonComplete.DOShakeScale(UIAnimationManager.speedSlow).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}));
		sequence.Append(rectStepIndicator.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		sequence.SetDelay(UIAnimationManager.speedMedium);
		return sequence;
	}

	private void Complete()
	{
		if (this.OnComplete != null)
		{
			this.OnComplete();
		}
		Dismiss();
	}
}
