using DiscoCapture;
using UnityEngine;

public class UIPopupCapture : UIPopupMenu
{
	[Header("Controllers")]
	public CaptureController captureController;

	public MenuCaptureViewport menuCaptureViewport;

	public MenuCaptureResults menuCaptureResults;

	public SequentialMenu[] menus;

	[Header("Brain Capture")]
	public bool isBrainCapture;

	public BloxelEnemyTile enemyTile;

	[Header("S3")]
	public GameObject S3Prefab;

	private new void Awake()
	{
		base.Awake();
		menus = GetComponentsInChildren<SequentialMenu>(true);
		for (int i = 0; i < menus.Length; i++)
		{
			menus[i].Init();
		}
	}

	private new void Start()
	{
		base.Start();
		CreateS3Interface();
	}

	private void CreateS3Interface()
	{
		if (!(S3Interface.instance == null) && S3Interface.instance.CanUploadCapture())
		{
			GameObject gameObject = Object.Instantiate(S3Prefab);
			gameObject.GetComponent<UICaptureData>().capturePopup = this;
			RectTransform rectTransform = gameObject.transform as RectTransform;
			rectTransform.SetParent(base.transform);
			rectTransform.SetAsLastSibling();
			rectTransform.anchoredPosition = new Vector2(20f, 20f);
			rectTransform.localScale = Vector3.one;
		}
	}

	public void CaptureRepeat()
	{
		CaptureManager.instance.CaptureRepeat();
	}

	public void CaptureComplete()
	{
		CaptureManager.instance.CaptureConfirm();
	}
}
