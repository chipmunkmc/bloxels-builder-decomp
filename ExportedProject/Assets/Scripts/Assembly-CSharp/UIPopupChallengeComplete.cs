using System;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupChallengeComplete : UIPopupMenu
{
	public delegate void HandleCompete();

	public Challenge challenge;

	public RectTransform rectBadge;

	public RectTransform rectBadgeTitle;

	public RectTransform rectGemContainer;

	public RectTransform rectGemIncrement;

	public RectTransform rectGemTotal;

	public RectTransform rectIconGem;

	public TextMeshProUGUI uiTextDescription;

	public RectTransform rectButtonComplete;

	public TextMeshProUGUI uiTextGemIncrement;

	public TextMeshProUGUI uiGemTextTotal;

	private Image _uiImageBadge;

	public TextMeshProUGUI uiButtonText;

	public UIButton uiButtonComplete;

	private string _descriptionString;

	private int _gemValue;

	private int _currentBankValue;

	public ParticleSystem ps;

	public event HandleCompete OnComplete;

	private new void Awake()
	{
		base.Awake();
		_uiImageBadge = rectBadge.GetComponent<Image>();
		Reset();
	}

	private new void Start()
	{
		base.Start();
		uiButtonComplete.OnClick += Complete;
		SoundManager.instance.PlaySound(SoundManager.instance.menuPopup);
	}

	private void OnDestroy()
	{
		uiButtonComplete.OnClick -= Complete;
	}

	private void Reset()
	{
		uiTextDescription.text = string.Empty;
		uiGemTextTotal.text = string.Empty;
		uiTextGemIncrement.text = string.Empty;
		rectBadge.transform.localScale = Vector3.zero;
		rectBadgeTitle.transform.localScale = Vector3.zero;
		rectIconGem.transform.localScale = Vector3.zero;
		rectGemContainer.transform.localScale = Vector3.zero;
		rectButtonComplete.transform.localScale = Vector3.zero;
	}

	public void Init(Challenge _challenge)
	{
		challenge = _challenge;
		SoundManager.instance.PlaySound(SoundManager.instance.challengeComplete);
		uiButtonText.text = ChallengeManager.instance.acceptanceText;
		_descriptionString = ChallengeManager.instance.descriptionForChallenge[challenge];
		_uiImageBadge.sprite = ChallengeManager.instance.badgeForChallenge[challenge];
		_gemValue = ChallengeManager.instance.rewardForChallenge[challenge];
		_currentBankValue = PlayerBankManager.instance.GetGemCount();
		uiTextDescription.text = _descriptionString;
		IntroSequence().Play().OnComplete(delegate
		{
			LootSequence().Play();
		});
	}

	private Sequence IntroSequence()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(rectBadge.DOScale(1f, UIAnimationManager.speedMedium));
		sequence.Append(rectBadge.DOShakeScale(UIAnimationManager.speedSlow).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}));
		sequence.Append(rectBadgeTitle.DOScale(1f, UIAnimationManager.speedMedium));
		sequence.Append(rectBadgeTitle.DOShakeScale(UIAnimationManager.speedSlow).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}));
		sequence.Append(rectGemContainer.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		sequence.SetDelay(UIAnimationManager.speedMedium);
		return sequence;
	}

	private Sequence LootSequence()
	{
		Sequence sequence = DOTween.Sequence();
		uiGemTextTotal.SetText(_currentBankValue.ToString());
		sequence.Append(rectIconGem.DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.OutBounce).OnStart(delegate
		{
			ps.Play();
			SoundManager.instance.PlaySound(SoundManager.instance.coinReward);
		}));
		int num = _gemValue / 5;
		for (int i = 0; i <= _gemValue; i += num)
		{
			string empty = string.Empty;
			empty = "+" + i;
			uiTextGemIncrement.SetText(empty);
			sequence.Append(rectGemIncrement.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), UIAnimationManager.speedMedium).OnStart(delegate
			{
				SoundManager.instance.PlaySound(SoundManager.instance.coinIncrement);
				uiTextGemIncrement.DOColor(AssetManager.instance.bloxelPurple, UIAnimationManager.speedFast).OnComplete(delegate
				{
					uiTextGemIncrement.DOColor(Color.white, UIAnimationManager.speedFast);
				});
			}));
		}
		sequence.Append(rectGemTotal.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGemTextTotal.SetText((_currentBankValue + _gemValue).ToString());
			ps.Stop();
		}));
		sequence.Append(rectGemTotal.DOPunchScale(new Vector3(1.1f, 1.1f, 1.1f), UIAnimationManager.speedMedium));
		sequence.Append(rectButtonComplete.transform.DOScale(1f, UIAnimationManager.speedMedium));
		sequence.Append(rectButtonComplete.DOShakeScale(UIAnimationManager.speedSlow).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}));
		return sequence;
	}

	private void Complete()
	{
		if (this.OnComplete != null)
		{
			this.OnComplete();
		}
		Dismiss();
	}
}
