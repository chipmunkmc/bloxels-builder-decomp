using System;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupChallengeInfo : UIPopupMenu
{
	public delegate void HandleCompete();

	public TextMeshProUGUI uiTextTitle;

	public Text uiTextDescription;

	public RectTransform rectButtonComplete;

	public TextMeshProUGUI _uiButtonText;

	private UIButton _uiButtonComplete;

	private string _titleString;

	private string _descriptionString;

	public event HandleCompete OnComplete;

	private new void Awake()
	{
		base.Awake();
		_uiButtonComplete = rectButtonComplete.GetComponent<UIButton>();
		_uiButtonComplete.OnClick += Complete;
		Reset();
	}

	private new void Start()
	{
		base.Start();
		SoundManager.instance.PlaySound(SoundManager.instance.menuPopup);
	}

	private void Reset()
	{
		uiTextTitle.text = string.Empty;
		uiTextDescription.text = string.Empty;
		rectButtonComplete.transform.localScale = Vector3.zero;
	}

	public void Init(string title, string description, string buttonText)
	{
		_titleString = title;
		_descriptionString = description;
		_uiButtonText.text = buttonText;
		IntroSequence().Play();
	}

	private Sequence IntroSequence()
	{
		Sequence sequence = DOTween.Sequence();
		uiTextTitle.text = _titleString;
		sequence.Append(uiTextDescription.DOText(_descriptionString, UIAnimationManager.speedMedium));
		sequence.Append(rectButtonComplete.transform.DOScale(1f, UIAnimationManager.speedMedium));
		sequence.Append(rectButtonComplete.DOShakeScale(UIAnimationManager.speedSlow).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}));
		sequence.SetDelay(UIAnimationManager.speedMedium);
		return sequence;
	}

	private void Complete()
	{
		if (this.OnComplete != null)
		{
			this.OnComplete();
		}
		MonoBehaviour.print("COMPLETE");
		Dismiss();
	}
}
