using System.Collections;
using DG.Tweening;
using UnityEngine;

public class UIPopupChallenges : UIPopupMenu
{
	[Header("UI")]
	public RectTransform rectBoardVerification;

	public RectTransform rectChallenges;

	public RectTransform rectChallengeGrid;

	public RectTransform rectButtonActivate;

	public RectTransform rectButtonBuy;

	public RectTransform rectActivatedSuccess;

	private UIButton _uiButtonActivate;

	public UIButton _uiButtonBuy;

	public RectTransform rectImageStatusHolder;

	public ParticleSystem confetti;

	[Header("Data")]
	public UIChallengeItem[] challenges;

	public Object boardActivationPrefab;

	public AgeGateTooltip ageGateTooltip;

	private new void Awake()
	{
		base.Awake();
		challenges = GetComponentsInChildren<UIChallengeItem>();
		_uiButtonActivate = rectButtonActivate.GetComponent<UIButton>();
		_uiButtonBuy = rectButtonBuy.GetComponent<UIButton>();
		_uiButtonActivate.OnClick += ActivateButtonPressed;
		_uiButtonBuy.OnClick += BuyButtonPressed;
		ageGateTooltip.isOverrideLink = true;
	}

	private new void Start()
	{
		base.Start();
		for (int i = 0; i < challenges.Length; i++)
		{
			challenges[i].OnSelect += HandleChallengeOnSelect;
		}
	}

	private void OnDestroy()
	{
		for (int i = 0; i < challenges.Length; i++)
		{
			challenges[i].OnSelect -= HandleChallengeOnSelect;
		}
		_uiButtonActivate.OnClick -= ActivateButtonPressed;
		_uiButtonBuy.OnClick -= BuyButtonPressed;
	}

	public void Init()
	{
		if (AppStateManager.instance.userOwnsBoard)
		{
			rectBoardVerification.localScale = Vector3.zero;
			rectChallenges.anchoredPosition = new Vector2(0f, -120f);
			rectChallengeGrid.sizeDelta = new Vector2(rectChallengeGrid.sizeDelta.x, 512f);
			for (int i = 0; i < challenges.Length; i++)
			{
				challenges[i].Init();
			}
		}
		else
		{
			rectImageStatusHolder.DOScale(1.1f, UIAnimationManager.speedSlow).SetLoops(-1, LoopType.Yoyo);
			StartCoroutine(AnimateButtons());
			for (int j = 0; j < challenges.Length; j++)
			{
				challenges[j].Init();
			}
		}
	}

	private IEnumerator AnimateButtons()
	{
		yield return new WaitForSeconds(1.5f);
		rectButtonActivate.DOShakeScale(UIAnimationManager.speedMedium).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
		}).OnComplete(delegate
		{
			rectButtonBuy.DOShakeScale(UIAnimationManager.speedMedium).OnStart(delegate
			{
				SoundManager.instance.PlaySound(SoundManager.instance.boardWobble);
			});
		});
	}

	public void ActivateButtonPressed()
	{
		UIOverlayCanvas.instance.Popup(boardActivationPrefab);
		Object.Destroy(base.gameObject);
	}

	public void BuyButtonPressed()
	{
		string value = string.Empty;
		UIHelpLink.instance.urlLookup.TryGetValue(UIHelpLink.Location.Buy, out value);
		ageGateTooltip.linkOverride = value;
	}

	public void Unlock()
	{
		BoardVerified().OnComplete(ChallengesAvailable);
	}

	private Sequence BoardVerified()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(rectButtonActivate.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InBounce));
		sequence.Append(rectButtonBuy.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InBounce));
		sequence.Append(rectBoardVerification.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo));
		sequence.Append(rectActivatedSuccess.DOScale(1f, UIAnimationManager.speedSlow).SetEase(Ease.OutExpo));
		sequence.Append(rectActivatedSuccess.DOShakeScale(UIAnimationManager.speedSlow).OnStart(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.gameboardUnlock);
			confetti.Play();
		}));
		sequence.Append(rectActivatedSuccess.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo));
		sequence.Append(rectChallenges.DOAnchorPos(new Vector2(0f, -120f), UIAnimationManager.speedSlow).SetEase(Ease.OutExpo).OnStart(delegate
		{
			rectChallengeGrid.DOSizeDelta(new Vector2(rectChallengeGrid.sizeDelta.x, 512f), UIAnimationManager.speedSlow).SetEase(Ease.OutExpo);
		}));
		sequence.Play();
		return sequence;
	}

	private void ChallengesAvailable()
	{
		Sequence sequence = DOTween.Sequence();
		for (int i = 0; i < challenges.Length; i++)
		{
			sequence.Append(challenges[i].Unlock());
		}
		sequence.Play();
	}

	private void ChallengesLock()
	{
		Sequence sequence = DOTween.Sequence();
		for (int i = 0; i < challenges.Length; i++)
		{
			sequence.Append(challenges[i].Lock());
		}
		sequence.Play();
	}

	private void HandleChallengeOnSelect(UIChallengeItem item)
	{
		Dismiss();
	}
}
