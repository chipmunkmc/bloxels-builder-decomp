using UnityEngine.UI;

public class UIPopupDebugPanel : UIPopupMenu
{
	public Text uiTextField;

	public void Message(string message)
	{
		uiTextField.text = message;
	}
}
