using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupDeleteSquare : UIPopupMenu
{
	private CanvasSquare _square;

	public UIButton uiButtonConfirm;

	private new void Start()
	{
		base.Start();
		uiButtonConfirm.OnClick += Delete;
	}

	public void Init(CanvasSquare canvasSquare)
	{
		_square = canvasSquare;
	}

	private void Delete()
	{
		if (_square.serverSquare.publisher != null && CurrentUser.instance.account != null && CurrentUser.instance.account.serverID != _square.serverSquare.publisher)
		{
			SoundManager.PlayOneShot(SoundManager.instance.popB);
			uiButtonConfirm.GetComponentInChildren<Text>().text = "This is not yours!";
			StartCoroutine("DelayedConfirmRemoval");
			return;
		}
		SoundManager.PlayEventSound(SoundEvent.Trash);
		BloxelServerInterface.instance.Emit_RemoveSquare(_square.serverSquare._id, _square.serverSquare.publisher, _square.serverSquare.owner);
		CanvasTileInfo.instance.Hide(true);
		CanvasStreamingInterface.instance.canvasSquaresDict.Remove(_square.iWallCoordinate);
		_square.Reset();
		Dismiss();
	}

	private IEnumerator DelayedConfirmRemoval()
	{
		yield return new WaitForSeconds(2f);
		Graphic[] graphics = uiButtonConfirm.GetComponentsInChildren<Graphic>();
		Graphic[] array = graphics;
		foreach (Graphic target in array)
		{
			target.DOFade(0.2f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				uiButtonConfirm.interactable = false;
			});
		}
	}
}
