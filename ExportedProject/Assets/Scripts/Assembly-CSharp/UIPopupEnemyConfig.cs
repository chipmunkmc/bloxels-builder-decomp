using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class UIPopupEnemyConfig : UIPopupMenu
{
	[Header("Data")]
	public BloxelTile tile;

	public EnemyType currentType;

	private BloxelBrain _currentBrain;

	[Header("UI")]
	public RectTransform menuIndex;

	public UIPopupGemUnlock menuGemUnlock;

	public UIButton uiButtonDismiss;

	public UIButton uiButtonSubmit;

	public UIButton uiButtonRemoveBrain;

	public RectTransform rectHelp;

	public UIButtonGroup buttonGroup;

	public UIButton stationary;

	public UIButton patroller;

	public UIButton flyer;

	public RectTransform rectEnemyVisualizer;

	private UIProjectCover _animatedEnemy;

	public UIAddBrainDropdown uiBrainDropdown;

	public UIDropzoneBrain brainDropzone;

	public RectTransform rectBrainLibrary;

	private UIDropzoneBrain[] _brainLibraryDropzones;

	public UIBrainDragVisualizer currentDragVisualizer;

	[Header("State Tracking")]
	private bool _hasBrain;

	[Header("Prefabs")]
	public UnityEngine.Object draggablePrefab;

	[Header("Brain Stats")]
	public float visibleOffsetAmount = 600f;

	public RectTransform rectBrainStats;

	public UIButton uiButtonStatToggle;

	public bool statsVisible;

	public RectTransform rectMakeEnemyCooler;

	private Vector2 _indexBasePosition;

	private Vector2 _indexMovedPosition;

	private Vector2 _libraryMovedPosition;

	private Vector2 _libraryBasePosition;

	private UIBrainStatBar[] _uiStatBars;

	public BloxelBrain currentBrain
	{
		get
		{
			return _currentBrain;
		}
		set
		{
			_currentBrain = value;
			UpdateStats();
		}
	}

	private new void Awake()
	{
		base.Awake();
		buttonGroup = GetComponentInChildren<UIButtonGroup>(true);
		_brainLibraryDropzones = rectBrainLibrary.GetComponentsInChildren<UIDropzoneBrain>(true);
		_animatedEnemy = rectEnemyVisualizer.GetComponentInChildren<UIProjectCover>(true);
		_uiStatBars = GetComponentsInChildren<UIBrainStatBar>();
		ResetUI();
	}

	private new void Start()
	{
		base.Start();
		patroller.uiGemLock.Init(new EnemyTypeUnlockable(EnemyType.Patrol));
		flyer.uiGemLock.Init(new EnemyTypeUnlockable(EnemyType.Flyer));
		stationary.uiGemLock.Init(new EnemyTypeUnlockable(EnemyType.Stationary));
		SubscribeEvents();
		buttonGroup.Reset();
		CheckExistingType();
		CheckExistingBrain();
		BuildCharacter();
		SetupExistingBrains();
	}

	private void OnDestroy()
	{
		UnSubscribeEvents();
	}

	private void GenerateNotches()
	{
		for (int i = 0; i < _uiStatBars.Length; i++)
		{
			int[] array = BloxelBrain.Thresholds[(uint)_uiStatBars[i].blockColor];
			if (array != null)
			{
				for (int j = 0; j < array.Length; j++)
				{
					_uiStatBars[i].GenerateNotch((float)array[j] / (float)BloxelBrain.MaxBlocks);
				}
			}
		}
	}

	private void UpdateStats()
	{
		if (currentBrain == null)
		{
			rectMakeEnemyCooler.localScale = Vector3.one;
			for (int i = 0; i < _uiStatBars.Length; i++)
			{
				_uiStatBars[i].ResetUI();
			}
		}
		else
		{
			rectMakeEnemyCooler.localScale = Vector3.zero;
			for (int j = 0; j < _uiStatBars.Length; j++)
			{
				_uiStatBars[j].SetData(currentBrain.blockTotals[(uint)_uiStatBars[j].blockColor]);
			}
		}
	}

	private void SubscribeEvents()
	{
		uiButtonSubmit.OnClick += SaveAndClose;
		uiButtonRemoveBrain.OnClick += RemoveBrain;
		uiButtonDismiss.OnClick += Dismiss;
		patroller.OnGemLock += HandleGemLock;
		flyer.OnGemLock += HandleGemLock;
		stationary.OnGemLock += HandleGemLock;
		CaptureManager.instance.OnCaptureConfirm += HandleConfirmCapture;
		CaptureManager.instance.OnCaptureDismiss += HandleConfirmDismiss;
		base.OnInit += HandleMenuInit;
		brainDropzone.uiBrain.OnBrainDragStart += HandleBrainDragStart;
		brainDropzone.uiBrain.OnBrainDragEnd += HandleBrainDragEnd;
		for (int i = 0; i < _brainLibraryDropzones.Length; i++)
		{
			_brainLibraryDropzones[i].uiBrain.OnBrainDragStart += HandleLibraryBrainDragStart;
			_brainLibraryDropzones[i].uiBrain.OnBrainDragEnd += HandleLibraryBrainDragEnd;
		}
		uiButtonStatToggle.OnClick += ToggleStats;
	}

	private void UnSubscribeEvents()
	{
		uiButtonSubmit.OnClick -= SaveAndClose;
		uiButtonRemoveBrain.OnClick -= RemoveBrain;
		uiButtonDismiss.OnClick -= Dismiss;
		CaptureManager.instance.OnCaptureConfirm -= HandleConfirmCapture;
		CaptureManager.instance.OnCaptureDismiss -= HandleConfirmDismiss;
		base.OnInit -= HandleMenuInit;
		brainDropzone.uiBrain.OnBrainDragStart -= HandleBrainDragStart;
		brainDropzone.uiBrain.OnBrainDragEnd -= HandleBrainDragEnd;
		for (int i = 0; i < _brainLibraryDropzones.Length; i++)
		{
			_brainLibraryDropzones[i].uiBrain.OnBrainDragStart -= HandleLibraryBrainDragStart;
			_brainLibraryDropzones[i].uiBrain.OnBrainDragEnd -= HandleLibraryBrainDragEnd;
		}
		uiButtonStatToggle.OnClick -= ToggleStats;
	}

	private void HandleGemLock(UIButton btn)
	{
		ShowUnlockMenu(btn.uiGemLock.model);
	}

	private void HandleMenuInit()
	{
		InitSequence().OnComplete(delegate
		{
			uiBrainDropdown.Init().OnComplete(delegate
			{
				uiBrainDropdown.Toggle();
			});
		});
	}

	private void HandleConfirmCapture()
	{
		ShowMenu();
		currentBrain = new BloxelBrain(CaptureManager.latest);
		brainDropzone.uiBrain.bloxelBrain = currentBrain;
		brainDropzone.SetBrain(brainDropzone.uiBrain);
	}

	private void HandleConfirmDismiss()
	{
		ShowMenu();
	}

	private void HandleLibraryBrainDragStart(UIBrain brain)
	{
		SoundManager.instance.PlaySound(SoundManager.instance.editorPlay);
		CreateVisualizer(brain);
		brain.GetComponentInParent<UIDropzoneBrain>().shouldHideBrain = true;
	}

	private void HandleLibraryBrainDragEnd(UIBrain brain)
	{
		if (currentDragVisualizer != null)
		{
			currentDragVisualizer.DestroyMe();
		}
		if (brainDropzone.isActive)
		{
			currentBrain = brain.bloxelBrain;
			brainDropzone.SetBrain(brain);
			return;
		}
		bool flag = false;
		for (int i = 0; i < _brainLibraryDropzones.Length; i++)
		{
			if (_brainLibraryDropzones[i].WillAcceptBrain(brain))
			{
				flag = true;
				brain.bloxelBrain.Save();
				_brainLibraryDropzones[i].SetBrain(brain);
				AssetManager.globalBrains[i] = brain.bloxelBrain;
				PrefsManager.instance.SetBrainIdAtPosition(i, brain.bloxelBrain.ID());
			}
		}
		if (!flag)
		{
			return;
		}
		for (int j = 0; j < _brainLibraryDropzones.Length; j++)
		{
			if (_brainLibraryDropzones[j].shouldHideBrain)
			{
				_brainLibraryDropzones[j].RemoveBrainBoard();
				AssetManager.globalBrains[j] = null;
				PrefsManager.instance.SetBrainIdAtPosition(j, Guid.Empty.ToString());
			}
		}
	}

	private void HandleBrainDragStart(UIBrain brain)
	{
		SoundManager.instance.PlaySound(SoundManager.instance.editorPlay);
		CreateVisualizer(brain);
	}

	private void HandleBrainDragEnd(UIBrain brain)
	{
		if (currentDragVisualizer != null)
		{
			currentDragVisualizer.DestroyMe();
		}
		for (int i = 0; i < _brainLibraryDropzones.Length; i++)
		{
			if (_brainLibraryDropzones[i].WillAcceptBrain(brain))
			{
				brain.bloxelBrain.Save();
				_brainLibraryDropzones[i].SetBrain(brain);
				AssetManager.globalBrains[i] = brain.bloxelBrain;
				PrefsManager.instance.SetBrainIdAtPosition(i, brain.bloxelBrain.ID());
				break;
			}
		}
	}

	private void HandleUnlockDismiss()
	{
		menuGemUnlock.OnSiblingDismiss -= HandleUnlockDismiss;
		menuIndex.DOScale(1f, UIAnimationManager.speedMedium);
		uiButtonStatToggle.selfRect.DOScale(1f, UIAnimationManager.speedMedium);
		rectEnemyVisualizer.DOScale(1f, UIAnimationManager.speedMedium);
		rectHelp.DOScale(1f, UIAnimationManager.speedMedium);
	}

	private void CreateVisualizer(UIBrain _brain)
	{
		GameObject gameObject = UnityEngine.Object.Instantiate(draggablePrefab) as GameObject;
		gameObject.transform.SetParent(UIOverlayCanvas.instance.transform);
		gameObject.transform.SetAsLastSibling();
		currentDragVisualizer = gameObject.GetComponent<UIBrainDragVisualizer>();
		currentDragVisualizer.Create(_brain, UIOverlayCanvas.instance.canvas);
	}

	private void ResetUI()
	{
		_indexBasePosition = menuIndex.anchoredPosition;
		_indexMovedPosition = new Vector2(menuIndex.anchoredPosition.x + visibleOffsetAmount, menuIndex.anchoredPosition.y);
		_libraryBasePosition = rectBrainLibrary.anchoredPosition;
		_libraryMovedPosition = new Vector2(rectBrainLibrary.anchoredPosition.x + visibleOffsetAmount, rectBrainLibrary.anchoredPosition.y);
		menuIndex.localScale = Vector3.zero;
		rectBrainLibrary.anchoredPosition = new Vector2(rectBrainLibrary.sizeDelta.x, rectBrainLibrary.anchoredPosition.y);
		rectEnemyVisualizer.localScale = Vector3.zero;
		rectHelp.localScale = Vector3.zero;
	}

	private Sequence InitSequence()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(menuIndex.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		sequence.Append(rectHelp.DOScale(1f, UIAnimationManager.speedMedium));
		sequence.Append(rectEnemyVisualizer.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo));
		sequence.Append(rectEnemyVisualizer.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f));
		sequence.Append(rectBrainLibrary.DOAnchorPos(new Vector2(0f, rectBrainLibrary.anchoredPosition.y), UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		sequence.Play();
		return sequence;
	}

	public void ShowUnlockMenu(GemUnlockable unlockable)
	{
		rectEnemyVisualizer.DOScale(0f, UIAnimationManager.speedMedium);
		rectHelp.DOScale(0f, UIAnimationManager.speedMedium);
		uiButtonStatToggle.selfRect.DOScale(0f, UIAnimationManager.speedMedium);
		menuIndex.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			menuGemUnlock.Init(unlockable);
			menuGemUnlock.IntroSequence();
			menuGemUnlock.OnSiblingDismiss += HandleUnlockDismiss;
		});
	}

	private void ShowMenu()
	{
		base.transform.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			BloxelCamera.instance.Blur(overlay);
		});
	}

	private void SetupExistingBrains()
	{
		for (int i = 0; i < AssetManager.globalBrains.Length; i++)
		{
			if (AssetManager.globalBrains[i] != null)
			{
				_brainLibraryDropzones[i].SetBrain(AssetManager.globalBrains[i]);
			}
		}
	}

	private void BuildCharacter()
	{
		if (!(tile == null))
		{
			BloxelProject projectModelAtLocation = tile.bloxelLevel.GetProjectModelAtLocation(tile.tileInfo.locationInLevel);
			if (projectModelAtLocation == null)
			{
				_animatedEnemy.InitStatic(AssetManager.instance.boardSolidPurple);
			}
			else if (projectModelAtLocation.type == ProjectType.Board)
			{
				_animatedEnemy.Init(new List<BloxelBoard> { projectModelAtLocation.coverBoard }, 1f, new CoverStyle(Color.clear));
			}
			else if (projectModelAtLocation.type == ProjectType.Animation)
			{
				_animatedEnemy.Init(projectModelAtLocation as BloxelAnimation, new CoverStyle(Color.clear));
			}
		}
	}

	private void ToggleStats()
	{
		if (statsVisible)
		{
			HideStats();
		}
		else
		{
			ShowStats();
		}
		statsVisible = !statsVisible;
	}

	private void ShowStats()
	{
		if (!statsVisible)
		{
			rectBrainStats.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
			rectBrainLibrary.DOAnchorPos(_libraryMovedPosition, UIAnimationManager.speedMedium);
			menuIndex.DOAnchorPos(_indexMovedPosition, UIAnimationManager.speedMedium);
		}
	}

	private void HideStats()
	{
		if (statsVisible)
		{
			rectBrainStats.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
			rectBrainLibrary.DOAnchorPos(_libraryBasePosition, UIAnimationManager.speedMedium);
			menuIndex.DOAnchorPos(_indexBasePosition, UIAnimationManager.speedMedium);
		}
	}

	private void CheckExistingBrain()
	{
		if (!(tile == null))
		{
			BloxelBrain value = null;
			_hasBrain = tile.bloxelLevel.brains.TryGetValue(tile.tileInfo.locationInLevel, out value);
			currentBrain = value;
			if (_hasBrain)
			{
				brainDropzone.SetBrain(currentBrain);
			}
		}
	}

	private void CheckExistingType()
	{
		if (tile == null)
		{
			return;
		}
		if (tile.bloxelLevel.enemyTypes.TryGetValue(tile.tileInfo.locationInLevel, out currentType))
		{
			switch (currentType)
			{
			case EnemyType.Patrol:
				patroller.Toggle();
				break;
			case EnemyType.Flyer:
				flyer.Toggle();
				break;
			case EnemyType.Stationary:
				stationary.Toggle();
				break;
			}
		}
		else
		{
			patroller.Toggle();
		}
	}

	private void CreateRandomBrain()
	{
		BloxelBrain bloxelBrain = BloxelBrain.GenerateSingleColorBrain(BlockColor.White, UnityEngine.Random.Range(0.05f, 0.5f));
		bloxelBrain.Save();
		brainDropzone.uiBrain.Init(bloxelBrain);
		tile.bloxelLevel.SetBrainAtLocation(tile.tileInfo.locationInLevel, bloxelBrain);
	}

	private void RemoveBrain()
	{
		currentBrain = null;
		tile.bloxelLevel.RemoveBrainAtLocation(tile.tileInfo.locationInLevel);
		brainDropzone.RemoveBrainBoard();
	}

	private void SaveAndClose()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.coinIncrement);
		EnemyType enemyType = EnemyType.Patrol;
		if (flyer.isOn)
		{
			enemyType = EnemyType.Flyer;
		}
		else if (stationary.isOn)
		{
			enemyType = EnemyType.Stationary;
		}
		if (tile != null)
		{
			if (currentBrain != null)
			{
				currentBrain.Save();
				tile.bloxelLevel.SetBrainAtLocation(tile.tileInfo.locationInLevel, currentBrain);
			}
			tile.bloxelLevel.SetEnemyTypeAtLocation(tile.tileInfo.locationInLevel, enemyType);
			((BloxelEnemyTile)tile).SetEnemyType(enemyType);
			((BloxelEnemyTile)tile).UpdateHudForNewBrain();
			Dismiss();
		}
		else
		{
			Dismiss();
		}
	}
}
