using DG.Tweening;
using TMPro;
using UnityEngine;

public class UIPopupGameEnd : UIPopupMenu
{
	[Header("Data")]
	private BloxelGame _game;

	[Header("UI")]
	public TextMeshProUGUI uiTextCoinsValue;

	public TextMeshProUGUI uiTextEnemiesValue;

	public TextMeshProUGUI uiTextBlocksValue;

	public TextMeshProUGUI uiTextTimeValue;

	public TextMeshProUGUI uiTextHealth;

	public UIButton uiButtonReplay;

	public UIButton uiButtonBack;

	public UIButton uiButtonPlayFromCheckpoint;

	public UIStatBar uiStatBar;

	[Header("Gem Bonus")]
	public RectTransform gemRect;

	public ParticleSystem gemBarf;

	public ParticleSystem confetti;

	private TextMeshProUGUI _gemText;

	[Header("Gold Blocks")]
	public RectTransform goldBlockMiddle;

	public RectTransform goldBlockLeft;

	public RectTransform goldBlockRight;

	public CanvasGroup outlineLeft;

	public CanvasGroup outlineRight;

	public CanvasGroup outlineMiddle;

	private bool doMiddleBlock;

	private bool doLeftBlock;

	private bool doRightBlock;

	private string timeCompleted;

	private bool shouldReset;

	private bool doGoldBlocks;

	private bool _doPoultryGem;

	private bool _doFeaturedGem;

	private new void Awake()
	{
		base.Awake();
		if (goldBlockMiddle != null)
		{
			goldBlockMiddle.transform.localScale = Vector3.zero;
		}
		if (goldBlockLeft != null)
		{
			goldBlockLeft.transform.localScale = Vector3.zero;
		}
		if (goldBlockRight != null)
		{
			goldBlockRight.transform.localScale = Vector3.zero;
		}
		timeCompleted = GameHUD.instance.uiTextTimer.text;
		if (gemRect != null)
		{
			gemRect.localScale = Vector3.zero;
			_gemText = gemRect.GetComponentInChildren<TextMeshProUGUI>();
		}
	}

	private new void Start()
	{
		base.Start();
		Time.timeScale = 0f;
		if (uiButtonBack != null)
		{
			uiButtonBack.OnClick += Back;
		}
		if (uiButtonReplay != null)
		{
			uiButtonReplay.OnClick += Replay;
		}
		if (uiButtonPlayFromCheckpoint != null)
		{
			uiButtonPlayFromCheckpoint.OnClick += RePlayFromCheckpoint;
		}
		if (AppStateManager.instance.currentScene == SceneName.Home)
		{
			uiButtonBack.transform.localScale = Vector3.zero;
		}
	}

	private void OnDestroy()
	{
		if (Time.timeScale < 1f)
		{
			Time.timeScale = 1f;
		}
		if (uiButtonBack != null)
		{
			uiButtonBack.OnClick += Back;
		}
		if (uiButtonReplay != null)
		{
			uiButtonReplay.OnClick += Replay;
		}
		if (uiButtonPlayFromCheckpoint != null)
		{
			uiButtonPlayFromCheckpoint.OnClick += RePlayFromCheckpoint;
		}
	}

	private void Init()
	{
		GameHUD.instance.StopTimer();
		CalculateValues();
	}

	public void InitWin()
	{
		doGoldBlocks = true;
		_doPoultryGem = false;
		_doFeaturedGem = false;
		if (AppStateManager.instance.currentScene == SceneName.PixelEditor_iPad)
		{
			uiButtonReplay.GetComponentInChildren<TextMeshProUGUI>().text = LocalizationManager.getInstance().getLocalizedText("gameplay25", "Keep Testing");
		}
		Init();
	}

	public void InitWinForPoultryPanic()
	{
		doGoldBlocks = true;
		_doPoultryGem = true;
		_gemText.SetText(LocalizationManager.getInstance().getLocalizedText("gameplay33", "Collect 5 Gems for Beating Poultry Panic!"));
		Init();
	}

	public void InitFeaturedWin()
	{
		doGoldBlocks = true;
		_doPoultryGem = false;
		_doFeaturedGem = true;
		_gemText.SetText(LocalizationManager.getInstance().getLocalizedText("gameplay26", "Collect 1 Gem for Beating Featured Game!"));
		Init();
	}

	public void InitLose()
	{
		doGoldBlocks = false;
		_doPoultryGem = false;
		_doFeaturedGem = false;
		Init();
	}

	public void SetGame(ref BloxelGame game)
	{
		_game = game;
	}

	private void Back()
	{
		switch (AppStateManager.instance.currentScene)
		{
		case SceneName.PixelEditor_iPad:
			SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
			Time.timeScale = 1f;
			GameHUD.instance.Reset(GameHUD.ResetMethod.Full);
			GameBuilderCanvas.instance.ComeBackFromTest();
			Dismiss();
			break;
		case SceneName.Gameplay:
			SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
			Time.timeScale = 1f;
			GameHUD.instance.Reset(GameHUD.ResetMethod.Full);
			BloxelsSceneManager.instance.GoTo(AppStateManager.instance.previousScene);
			Object.DestroyImmediate(base.gameObject);
			break;
		}
	}

	private void Replay()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		Time.timeScale = 1f;
		if (AppStateManager.instance.currentScene == SceneName.PixelEditor_iPad)
		{
			Dismiss();
			return;
		}
		GameHUD.instance.Reset(GameHUD.ResetMethod.Full);
		GameplayController.instance.Replay();
		GameplayController.instance.lastCheckpoint = GameplayController.instance.startingPosition;
		GameplayController.instance.heroObject.transform.localPosition = GameplayController.instance.lastCheckpoint;
		PixelPlayerController.instance.Reset();
		GameplayController.instance.StartDroppingCharacter();
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		Dismiss();
	}

	private void RePlayFromCheckpoint()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		Time.timeScale = 1f;
		GameHUD.instance.Reset(GameHUD.ResetMethod.FromCheckpoint);
		GameplayController.instance.ReplayFromCheckpiont();
		PixelPlayerController.instance.Reset();
		GameplayController.instance.heroObject.transform.localPosition = GameplayController.instance.lastCheckpoint;
		GameplayController.instance.StartDroppingCharacter();
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		PixelPlayerController.instance.ResetInventoryFromCheckpoint();
		Dismiss();
	}

	public void DoGoldBlocks()
	{
		if (!doGoldBlocks || !doLeftBlock)
		{
			return;
		}
		goldBlockLeft.transform.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedMedium).OnStart(delegate
		{
			outlineLeft.DOFade(0f, UIAnimationManager.speedMedium);
			SoundManager.instance.PlaySound(SoundManager.instance.publish);
		}).OnComplete(delegate
		{
			goldBlockLeft.transform.DOScale(1f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				if (doRightBlock)
				{
					goldBlockRight.transform.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedMedium).OnStart(delegate
					{
						outlineRight.DOFade(0f, UIAnimationManager.speedMedium);
						SoundManager.instance.PlaySound(SoundManager.instance.publish);
					}).OnComplete(delegate
					{
						goldBlockRight.transform.DOScale(1f, UIAnimationManager.speedFast).OnComplete(delegate
						{
							if (doMiddleBlock)
							{
								goldBlockMiddle.transform.DOPunchScale(new Vector3(1.2f, 1.2f, 1.2f), UIAnimationManager.speedMedium).OnStart(delegate
								{
									outlineMiddle.DOFade(0f, UIAnimationManager.speedMedium);
									SoundManager.instance.PlaySound(SoundManager.instance.publish);
								}).OnStart(delegate
								{
									confetti.Play();
								})
									.OnComplete(delegate
									{
										goldBlockMiddle.transform.DOScale(1f, UIAnimationManager.speedFast).OnComplete(delegate
										{
											SoundManager.instance.PlaySound(SoundManager.instance.buyA);
										});
									});
							}
						});
					});
				}
			});
		});
	}

	public void CalculateValues()
	{
		int num = GameHUD.instance.GetCoinsCollected();
		float num2 = 0f;
		float num3 = 0f;
		float num4 = 0f;
		int totalCoins = GameHUD.instance.GetTotalCoins();
		num2 = ((totalCoins != 0) ? ((float)num / (float)totalCoins * 100f) : 100f);
		int num5 = _game.TotalEnemiesInGame();
		num3 = ((num5 != 0) ? ((float)GameHUD.instance.enemiesDefeated / (float)num5 * 100f) : 100f);
		int num6 = _game.TotalDestructible();
		num4 = ((num6 != 0) ? ((float)GameHUD.instance.blocksDestroyed / (float)(num6 * 169) * 100f) : 100f);
		uiTextEnemiesValue.SetText(GameHUD.instance.enemiesDefeated.ToString());
		int num7 = 0;
		if (num3 > 74f)
		{
			num += Mathf.CeilToInt((float)num * 0.75f);
			num7 = Mathf.CeilToInt((float)GameHUD.instance.enemiesDefeated * 0.75f);
		}
		else if (num3 > 49f)
		{
			num += Mathf.CeilToInt((float)num * 0.5f);
			num7 = Mathf.CeilToInt((float)GameHUD.instance.enemiesDefeated * 0.5f);
		}
		else if (num3 > 24f)
		{
			num += Mathf.CeilToInt((float)num * 0.25f);
			num7 = Mathf.CeilToInt((float)GameHUD.instance.enemiesDefeated * 0.25f);
		}
		uiTextBlocksValue.SetText(GameHUD.instance.blocksDestroyed.ToString());
		int num8 = 0;
		if (num4 > 74f)
		{
			num += Mathf.CeilToInt((float)num * 0.75f);
			num8 = Mathf.CeilToInt((float)GameHUD.instance.blocksDestroyed * 0.75f);
		}
		else if (num4 > 49f)
		{
			num += Mathf.CeilToInt((float)num * 0.5f);
			num8 = Mathf.CeilToInt((float)GameHUD.instance.blocksDestroyed * 0.5f);
		}
		else if (num4 > 24f)
		{
			num += Mathf.CeilToInt((float)num * 0.25f);
			num8 = Mathf.CeilToInt((float)GameHUD.instance.blocksDestroyed * 0.25f);
		}
		uiTextCoinsValue.SetText(GameHUD.instance.GetCoinsCollected().ToString());
		int num9 = 0;
		if (num2 > 74f)
		{
			num += Mathf.CeilToInt((float)num * 0.75f);
			num9 = Mathf.CeilToInt((float)GameHUD.instance.GetCoinsCollected() * 0.75f);
		}
		else if (num2 > 49f)
		{
			num += Mathf.CeilToInt((float)num * 0.5f);
			num9 = Mathf.CeilToInt((float)GameHUD.instance.GetCoinsCollected() * 0.75f);
		}
		else if (num2 > 24f)
		{
			num += Mathf.CeilToInt((float)num * 0.25f);
			num9 = Mathf.CeilToInt((float)GameHUD.instance.GetCoinsCollected() * 0.75f);
		}
		float num10 = 0f;
		int maxHealth = GameHUD.instance.GetMaxHealth();
		num10 = ((maxHealth != 0) ? Mathf.Floor((float)GameHUD.instance.GetRemaingHealth() / (float)maxHealth * 100f) : 0f);
		uiTextHealth.SetText(num10 + "%");
		int num11 = 0;
		if (num10 > 74f)
		{
			num += Mathf.CeilToInt((float)num * 0.75f);
			num11 = 75;
		}
		else if (num10 > 49f)
		{
			num += Mathf.CeilToInt((float)num * 0.5f);
			num11 = 50;
		}
		else if (num10 > 24f)
		{
			num += Mathf.CeilToInt((float)num * 0.25f);
			num11 = 25;
		}
		uiTextTimeValue.SetText(timeCompleted);
		float num12 = (num4 + num2 + num3 + num10) / 400f;
		if (num12 > 1f)
		{
			num12 = 1f;
		}
		float num13 = num12 * 100f;
		uiStatBar.SetData(num12);
		if (doGoldBlocks)
		{
			if (num13 > 99f)
			{
				doLeftBlock = true;
				doMiddleBlock = true;
				doRightBlock = true;
			}
			else if (num13 > 65f)
			{
				doLeftBlock = true;
				doRightBlock = true;
			}
			else if (num13 > 32f)
			{
				doLeftBlock = true;
			}
			DoGoldBlocks();
			PlayerBankManager.instance.AddCoins(GameHUD.instance.GetCoinsCollected());
			if (_doPoultryGem)
			{
				DoGemEarn();
			}
			else if (_doFeaturedGem)
			{
				DoFeaturedGem();
			}
		}
	}

	private void DoGemEarn()
	{
		if (GemManager.instance.HasGemsForType(GemEarnable.Type.PoultryPanicV1))
		{
			return;
		}
		gemRect.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			gemRect.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f).OnStart(delegate
			{
				SoundManager.instance.PlaySound(SoundManager.instance.coinIncrement);
				gemBarf.Play();
				GemManager.instance.AddGemsForType(GemEarnable.Type.PoultryPanicV1);
			});
		});
	}

	private void DoFeaturedGem()
	{
		gemRect.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			gemRect.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f).OnStart(delegate
			{
				SoundManager.instance.PlaySound(SoundManager.instance.coinIncrement);
				gemBarf.Play();
			});
		});
	}
}
