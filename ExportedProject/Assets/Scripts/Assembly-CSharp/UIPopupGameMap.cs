using UnityEngine;
using UnityEngine.UI;

public class UIPopupGameMap : UIPopupMenu
{
	[Header("| ========= UI ========= |")]
	public Image uiImageRoomIcon;

	public Image uiImageEndFlagIcon;

	public Image uiImageCoinIcon;

	public Text uiTextRoomCount;

	public Text uiTextEndFlag;

	public Text uiTextCoinCount;

	private new void Awake()
	{
		base.Awake();
	}

	public void Init(BloxelGame _game)
	{
		uiTextRoomCount.text = _game.levels.Count.ToString();
		uiTextCoinCount.text = _game.TotalCoinsInGame().ToString();
		if (_game.gameEndFlag != WorldLocation.InvalidLocation())
		{
			uiTextEndFlag.text = LocalizationManager.getInstance().getLocalizedText("account66", "Yes");
		}
		else
		{
			uiTextEndFlag.text = LocalizationManager.getInstance().getLocalizedText("account65", "No");
		}
		GetComponent<UIGameMap>().Init(new GameMap(_game, GameMap.Type.iWall), false);
	}
}
