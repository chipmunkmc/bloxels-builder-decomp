using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupGemUnlock : UIPopupMenu
{
	public delegate void HandleSiblingDismiss();

	public delegate void HandleUnlock(GemUnlockable model);

	public delegate void HandleBulkRooms();

	[Header("UI")]
	public RectTransform rectButtonBuy;

	public RectTransform rectProductImage;

	public RectTransform rectBodyCopy;

	public RectTransform rectInfo;

	public RectTransform rectGameboard;

	public TextMeshProUGUI uiTextGemCount;

	public UIMenuPanel uiMenuIndex;

	public UIMenuPanel uiMenuHowToEarnGems;

	public UIGemBank uiGemBank;

	private Image _uiImageProduct;

	private UIButton _uiButtonBuy;

	public UIButton uiButtonHowToEarnGemsTrigger;

	private Button _uiButtonGameboard;

	private TextMeshProUGUI _uiTextBody;

	[Header("Data")]
	public GemUnlockable dataModel;

	private int _gemCount;

	[Header("Extra")]
	public ParticleSystem ps;

	[Tooltip("Treats it as if it's not a popup anymore")]
	public bool isSiblingMenu;

	public GameObject parent;

	public event HandleSiblingDismiss OnSiblingDismiss;

	public event HandleUnlock OnUnlock;

	public event HandleBulkRooms OnBulkRooms;

	private new void Awake()
	{
		if (!isSiblingMenu)
		{
			base.Awake();
		}
		_uiImageProduct = rectProductImage.GetComponent<Image>();
		_uiButtonBuy = rectButtonBuy.GetComponent<UIButton>();
		_uiTextBody = rectBodyCopy.GetComponent<TextMeshProUGUI>();
		_uiButtonGameboard = rectGameboard.GetComponentInChildren<Button>();
		ResetUI();
	}

	private new void Start()
	{
		if (!isSiblingMenu)
		{
			base.Start();
			base.OnInit += HandleMenuInit;
		}
		else
		{
			uiMenuIndex.ResetUI();
			uiMenuIndex.OverrideDismiss();
			uiMenuIndex.uiButtonDismiss.OnClick += HandleButtonDismiss;
		}
		_uiButtonGameboard.onClick.AddListener(HandleGameBoardClick);
		_uiButtonBuy.OnClick += HandleBuy;
	}

	private void OnDestroy()
	{
		if (!isSiblingMenu)
		{
			base.OnInit -= HandleMenuInit;
		}
		else
		{
			uiMenuIndex.uiButtonDismiss.OnClick -= HandleButtonDismiss;
		}
		_uiButtonGameboard.onClick.RemoveListener(HandleGameBoardClick);
		_uiButtonBuy.OnClick -= HandleBuy;
	}

	private void HandleGameBoardClick()
	{
		rectGameboard.DOKill();
		base.transform.DOScale(0f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			UIOverlayCanvas.instance.GameBoardActivation();
			if (isSiblingMenu)
			{
				UnityEngine.Object.DestroyImmediate(parent);
			}
			else
			{
				UnityEngine.Object.DestroyImmediate(base.gameObject);
			}
		});
	}

	private void HandleButtonDismiss()
	{
		SiblingDismiss(false);
	}

	private void HandleMenuInit()
	{
		IntroSequence();
	}

	private void HandleBuy()
	{
		if (uiGemBank.CanUnlock(_gemCount))
		{
			ps.Play();
			SoundManager.PlayOneShot(SoundManager.instance.megaTileUnlock);
			uiGemBank.DecrementFunds(_gemCount);
			dataModel.Unlock();
			StartCoroutine(DelayedDismiss());
		}
	}

	private void HandleHelpButtonPressed()
	{
		uiMenuIndex.Hide();
		uiButtonHowToEarnGemsTrigger.transform.DOScale(Vector3.zero, UIAnimationManager.speedMedium);
		rectGameboard.transform.DOScale(Vector3.zero, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			rectGameboard.DOKill();
		});
		uiMenuHowToEarnGems.Show();
	}

	private void ResetUI()
	{
		uiMenuIndex.bgTransform.localScale = Vector3.zero;
		uiMenuIndex.uiButtonDismiss.transform.localScale = Vector3.zero;
		uiMenuIndex.uiMenuTitle.transform.localScale = Vector3.zero;
		uiButtonHowToEarnGemsTrigger.GetComponent<RectTransform>().localScale = Vector3.zero;
		rectBodyCopy.localScale = Vector3.zero;
		rectInfo.localScale = Vector3.zero;
		rectProductImage.localScale = Vector3.zero;
		rectButtonBuy.localScale = Vector3.zero;
		uiGemBank.transform.localScale = Vector3.zero;
		rectGameboard.localScale = Vector3.zero;
		base.transform.localScale = Vector3.one;
	}

	public void Init(GemUnlockable _dataModel)
	{
		dataModel = _dataModel;
		uiMenuIndex.uiMenuTitle.uiTextPrimary.text = _dataModel.uiBuyable.title;
		_uiTextBody.text = _dataModel.uiBuyable.bodyText;
		_uiImageProduct.sprite = _dataModel.uiBuyable.sprite;
		_gemCount = _dataModel.cost;
		uiTextGemCount.text = _gemCount.ToString();
		_uiImageProduct.color = _dataModel.uiBuyable.productTint;
		uiButtonHowToEarnGemsTrigger.OnClick += HandleHelpButtonPressed;
	}

	public Sequence IntroSequence()
	{
		Sequence sequence = DOTween.Sequence();
		uiMenuIndex.Init();
		sequence.Append(uiButtonHowToEarnGemsTrigger.GetComponent<RectTransform>().DOScale(1f, UIAnimationManager.speedFast));
		sequence.Append(rectProductImage.DOScale(1f, UIAnimationManager.speedFast));
		sequence.Append(rectBodyCopy.DOScale(1f, UIAnimationManager.speedFast));
		sequence.Append(rectInfo.DOScale(1f, UIAnimationManager.speedFast));
		sequence.Append(rectButtonBuy.DOScale(1f, UIAnimationManager.speedFast).OnStart(delegate
		{
			SoundManager.PlayOneShot(SoundManager.instance.coinIncrement);
		}));
		sequence.Append(rectButtonBuy.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 1, 0f));
		sequence.Append(uiGemBank.transform.DOScale(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			SoundManager.PlayOneShot(SoundManager.instance.appearance);
		}));
		sequence.Play();
		if (!AppStateManager.instance.userOwnsBoard)
		{
			StartCoroutine("DelayedGameboardNotification");
		}
		return sequence;
	}

	private IEnumerator DelayedGameboardNotification()
	{
		yield return new WaitForSeconds(1.2f);
		rectGameboard.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnStart(delegate
		{
			SoundManager.PlayOneShot(SoundManager.instance.appearance);
		})
			.OnComplete(delegate
			{
				rectGameboard.DOScale(1.05f, UIAnimationManager.speedSlow).SetLoops(-1, LoopType.Yoyo);
			});
	}

	private IEnumerator DelayedDismiss()
	{
		yield return new WaitForSeconds(0.5f);
		StopCoroutine("DelayedGameboardNotification");
		if (!isSiblingMenu)
		{
			uiMenuIndex.Hide().OnComplete(delegate
			{
				if (this.OnUnlock != null)
				{
					this.OnUnlock(dataModel);
				}
				rectGameboard.DOKill();
				Dismiss();
			});
		}
		else
		{
			SiblingDismiss(true);
		}
	}

	public void SiblingDismiss(bool shouldFireUnlockEvent)
	{
		StopCoroutine("DelayedGameboardNotification");
		uiMenuIndex.Hide().OnComplete(delegate
		{
			ResetUI();
			uiMenuIndex.ResetUI();
			rectGameboard.DOKill();
			if (this.OnSiblingDismiss != null)
			{
				this.OnSiblingDismiss();
			}
			if (shouldFireUnlockEvent && this.OnUnlock != null)
			{
				this.OnUnlock(dataModel);
			}
		});
	}
}
