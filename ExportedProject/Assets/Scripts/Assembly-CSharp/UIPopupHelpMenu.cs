using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupHelpMenu : UIPopupMenu
{
	private struct UrlAndText
	{
		public string unityKey;

		public string label;

		public string link;

		public UrlAndText(string unityKey, string label, string link)
		{
			this.unityKey = unityKey;
			this.label = label;
			this.link = link;
		}
	}

	public delegate void HandleCompete();

	[Header("| ========= State ========= |")]
	public string currentHelpVideo;

	[Header("| ========= UI ========= |")]
	public Dropdown uiDropDownOnlineHelp;

	public UIButton uiButtonWatchOnlineHelp;

	public UIButton uiButtonDismiss;

	public AgeGateTooltip ageGateTooltip;

	private List<UrlAndText> urlLookup = new List<UrlAndText>
	{
		new UrlAndText("account76", "Boards", PPUtilities.Link_tutorialBoards),
		new UrlAndText("gamebuilder3", "Animations", PPUtilities.Link_tutorialAnimations),
		new UrlAndText("gamebuilder4", "Backgrounds", PPUtilities.Link_tutorialBackgrounds),
		new UrlAndText("gamebuilder5", "Characters", PPUtilities.Link_tutorialCharacters),
		new UrlAndText("gamebuilder2", "Games", PPUtilities.Link_tutorialGames),
		new UrlAndText("challenges53", "Capture", PPUtilities.Link_tutorialCapture),
		new UrlAndText("gamebuilder131", "iWall", PPUtilities.Link_tutorialiWall),
		new UrlAndText("gamebuilder132", "Gems", PPUtilities.Link_Gems),
		new UrlAndText("gamebuilder133", "Brains", PPUtilities.Link_Brains),
		new UrlAndText("gamebuilder134", "All", PPUtilities.Link_tutorialAll)
	};

	[Header("| ========= Data ========= |")]
	private List<Dropdown.OptionData> onlineHelpOptions = new List<Dropdown.OptionData>();

	public event HandleCompete OnComplete;

	private new void Awake()
	{
		base.Awake();
		List<string> list = urlLookup.Select((UrlAndText kvp) => LocalizationManager.getInstance().getLocalizedText(kvp.unityKey, kvp.label)).ToList();
		for (int i = 0; i < list.Count; i++)
		{
			onlineHelpOptions.Add(new Dropdown.OptionData(list[i]));
		}
		uiDropDownOnlineHelp.options = onlineHelpOptions;
		ageGateTooltip.isOverrideLink = true;
	}

	private new void Start()
	{
		base.Start();
		uiDropDownOnlineHelp.onValueChanged.AddListener(delegate(int index)
		{
			SoundManager.PlayOneShot(SoundManager.instance.drawOnBoard);
			currentHelpVideo = urlLookup[index].link;
			ageGateTooltip.linkOverride = currentHelpVideo;
		});
		currentHelpVideo = urlLookup[0].link;
		ageGateTooltip.linkOverride = currentHelpVideo;
	}

	private void Complete()
	{
		if (this.OnComplete != null)
		{
			this.OnComplete();
		}
	}
}
