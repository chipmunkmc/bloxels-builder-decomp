using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupLibraryItem : UIPopupMenu
{
	[Header("| ========= Data ========= |")]
	public SavedProject focusedProject;

	public BloxelProject dataModel;

	public bool initComplete;

	[Header("| ========= UI Index ========= |")]
	public Image coverBoardTexture;

	public InputField uiInputFieldTitle;

	public UIButton uiButtonTags;

	public UIButton uiButtonDelete;

	public UIButton uiButtonSaveAndClose;

	public string title;

	[Header("| ========= UI Tags ========= |")]
	public RectTransform menuTags;

	public UIButton uiButtonBack;

	public UIButton uiButtonDismissFromTags;

	public UIButton uiButtonSaveFromTags;

	public UIButton uiButtonAddTag;

	private Text uiTextNoDelete;

	public InputField uiInputFieldTag;

	public List<UITag> activeTags = new List<UITag>();

	public Transform activeTagGrid;

	public Transform globalTagGrid;

	public Object uiTagPrefab;

	public GameObject[] emptySlots;

	public Animator animator;

	public GameObject tooltip;

	public Object tapIndicatorPrefab;

	public GameObject tapIndicator;

	public Transform menuIndex;

	private string tagInputText;

	[Header("| ========= UI Delete ========= |")]
	public RectTransform menuDelete;

	public UIButton uiButtonBackFromDelete;

	public UIButton uiButtonDismissFromDelete;

	public UIButton uiButtonSaveFromDelete;

	private new void Awake()
	{
		base.Awake();
		if (uiTagPrefab == null)
		{
			uiTagPrefab = Resources.Load("Prefabs/UITag");
		}
		emptySlots = new GameObject[activeTagGrid.childCount];
		for (int i = 0; i < activeTagGrid.childCount; i++)
		{
			emptySlots[i] = activeTagGrid.GetChild(i).gameObject;
		}
		menuTags.gameObject.SetActive(true);
		menuTags.localScale = Vector3.zero;
		menuDelete.gameObject.SetActive(true);
		menuDelete.localScale = Vector3.zero;
		uiTextNoDelete = uiButtonDelete.GetComponentInChildren<Text>();
		uiTextNoDelete.text = "Cannot Delete This";
		uiTextNoDelete.enabled = false;
	}

	private new void Start()
	{
		base.Start();
		if (!string.IsNullOrEmpty(dataModel.title))
		{
			uiInputFieldTitle.text = dataModel.title;
		}
		else
		{
			uiInputFieldTitle.text = LocalizationManager.getInstance().getLocalizedText("gamebuilder8", "My Project");
		}
		uiInputFieldTag.onValueChange.AddListener(CheckForComma);
		uiInputFieldTag.onEndEdit.AddListener(delegate(string val)
		{
			if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
			{
				AddTagWithString(val);
			}
		});
		uiButtonTags.OnClick += ShowTagMenu;
		uiButtonBack.OnClick += ShowIndexMenu;
		uiButtonDismissFromTags.OnClick += Dismiss;
		uiButtonSaveFromTags.OnClick += SaveAndClose;
		uiButtonBackFromDelete.OnClick += ShowIndexMenuFromDelete;
		uiButtonDismissFromDelete.OnClick += Dismiss;
		uiButtonSaveFromDelete.OnClick += Delete;
		BuildCover();
		CreateTitle();
	}

	private void OnDestroy()
	{
		if (tapIndicator != null)
		{
			KillTagTeacher();
		}
		uiButtonTags.OnClick -= ShowTagMenu;
		uiButtonBack.OnClick -= ShowIndexMenu;
		uiButtonDismissFromTags.OnClick -= Dismiss;
		uiButtonSaveFromTags.OnClick -= SaveAndClose;
		uiButtonBackFromDelete.OnClick -= ShowIndexMenuFromDelete;
		uiButtonDismissFromDelete.OnClick -= Dismiss;
		uiButtonSaveFromDelete.OnClick -= Delete;
	}

	private void OnEnable()
	{
		uiButtonDelete.OnClick += ShowDeleteMenu;
		uiButtonAddTag.OnClick += AddTagToProject;
		uiButtonSaveAndClose.OnClick += SaveAndClose;
	}

	private void OnDisable()
	{
		uiButtonDelete.OnClick -= ShowDeleteMenu;
		uiButtonAddTag.OnClick -= AddTagToProject;
		uiButtonSaveAndClose.OnClick -= SaveAndClose;
	}

	private void CreateTitle()
	{
		switch (dataModel.type)
		{
		case ProjectType.Board:
			title = "Board Info";
			break;
		case ProjectType.Animation:
			title = "Animation Info";
			break;
		case ProjectType.Character:
			title = "Character Info";
			break;
		case ProjectType.MegaBoard:
			title = "Background Info";
			break;
		case ProjectType.Game:
			title = "Game Info";
			break;
		case ProjectType.Level:
			break;
		}
	}

	private void ShowTagMenu()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		menuIndex.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			menuTags.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
			{
				if (!initComplete)
				{
					BuildActiveTags();
					BuildGlobalTags();
					if (!PrefsManager.instance.hasSeenTags)
					{
						TeachTags();
						PrefsManager.instance.hasSeenTags = true;
					}
					initComplete = true;
				}
			});
		});
	}

	private void ShowDeleteMenu()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		menuIndex.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			menuDelete.DOScale(1f, UIAnimationManager.speedMedium);
		});
	}

	private void ShowIndexMenuFromDelete()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		menuDelete.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			menuIndex.DOScale(1f, UIAnimationManager.speedMedium);
		});
	}

	private void ShowIndexMenu()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		menuTags.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			menuIndex.DOScale(1f, UIAnimationManager.speedMedium);
		});
	}

	private void TeachTags()
	{
		BloxelLibrary.instance.libraryToggle.Show();
		tooltip.gameObject.SetActive(true);
		ShowTapIndicator(Teach.indicatorPositionByItem[Teach.Item.Tags]);
	}

	private void KillTagTeacher()
	{
		tooltip.gameObject.SetActive(false);
		tapIndicator.GetComponent<UIAnimationTap>().DestroyMe();
	}

	private void ShowTapIndicator(RectPosition pos)
	{
		tapIndicator = UIOverlayCanvas.instance.Popup(tapIndicatorPrefab);
		tapIndicator.GetComponent<UIAnimationTap>().Init(pos);
	}

	private void BuildCover()
	{
		if (dataModel.type != ProjectType.MegaBoard && dataModel.type != ProjectType.Game)
		{
			coverBoardTexture.sprite = Sprite.Create(focusedProject.cover.texture, new Rect(0f, 0f, focusedProject.cover.texture.width, focusedProject.cover.texture.height), new Vector2(0.5f, 0.5f));
		}
		else if (dataModel.type == ProjectType.MegaBoard)
		{
			coverBoardTexture.sprite = ((SavedMegaBoard)focusedProject).coverSprite.sprite;
		}
		else if (dataModel.type == ProjectType.Game)
		{
			coverBoardTexture.sprite = ((SavedGame)focusedProject).coverSprite.sprite;
		}
	}

	private void CheckForComma(string val)
	{
		tagInputText = val;
		if (val.Contains(","))
		{
			string[] array = val.Split(',');
			AddTagWithString(array[0]);
		}
	}

	private void BuildActiveTags()
	{
		if (dataModel.tags != null)
		{
			for (int i = 0; i < dataModel.tags.Count && i < BloxelProject.MAX_TAGS; i++)
			{
				emptySlots[i].SetActive(false);
				UITag uITag = CreateUITag(activeTagGrid);
				ActivateTag(uITag);
				uITag.tagText = dataModel.tags[i];
			}
		}
		if (dataModel.tags.Count >= BloxelProject.MAX_TAGS)
		{
			uiInputFieldTag.placeholder.color = ColorUtilities.blockRGB[BlockColor.Red];
			uiInputFieldTag.text = "Maximum Tags (6) Reached";
			uiInputFieldTag.interactable = false;
		}
	}

	private void BuildGlobalTags()
	{
		for (int i = 0; i < AssetManager.availableTags.Count; i++)
		{
			if (!(AssetManager.availableTags[i] == "PPHide") && !(AssetManager.availableTags[i] == "PixelTutz"))
			{
				UITag uITag = CreateUITag(globalTagGrid);
				uITag.tagText = AssetManager.availableTags[i];
				uITag.ShowAdd();
				uITag.OnClick += ActivateTag;
			}
		}
		AdjustScrollRectHeight(globalTagGrid);
	}

	private void ActivateTag(UITag tag)
	{
		if (activeTags.Count >= BloxelProject.MAX_TAGS)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.cancelA);
			return;
		}
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		tag.transform.SetParent(activeTagGrid);
		tag.ShowDelete();
		tag.OnRemove += DeActivateTag;
		if (!activeTags.Contains(tag))
		{
			activeTags.Add(tag);
		}
		int num = activeTags.Count - 1;
		emptySlots[num].SetActive(false);
		tag.transform.SetSiblingIndex(num);
		AdjustScrollRectHeight(globalTagGrid);
	}

	private void DeActivateTag(UITag tag)
	{
		tag.transform.SetParent(globalTagGrid);
		tag.transform.SetAsFirstSibling();
		tag.ShowAdd();
		if (activeTags.Contains(tag))
		{
			activeTags.Remove(tag);
		}
		for (int i = activeTags.Count; i < BloxelProject.MAX_TAGS; i++)
		{
			emptySlots[i].SetActive(true);
		}
		if (tag.generatedByUser)
		{
			tag.DestroyMe();
		}
		else
		{
			tag.OnClick += ActivateTag;
		}
		CheckTagCount();
		AdjustScrollRectHeight(globalTagGrid);
	}

	private UITag CreateUITag(Transform parent)
	{
		GameObject gameObject = Object.Instantiate(uiTagPrefab) as GameObject;
		gameObject.transform.SetParent(parent);
		gameObject.transform.localScale = Vector3.one;
		return gameObject.GetComponent<UITag>();
	}

	public void CheckTagCount()
	{
		if (dataModel.tags.Count >= BloxelProject.MAX_TAGS)
		{
			uiInputFieldTag.placeholder.color = ColorUtilities.blockRGB[BlockColor.Red];
			uiInputFieldTag.text = "Maximum Tags (6) Reached";
			uiInputFieldTag.interactable = false;
		}
		else
		{
			uiInputFieldTag.textComponent.color = new Color(0.46f, 0.46f, 0.46f, 1f);
			uiInputFieldTag.text = string.Empty;
			uiInputFieldTag.interactable = true;
		}
	}

	private void AddTagWithString(string inputText)
	{
		if (tapIndicator != null)
		{
			KillTagTeacher();
		}
		tagInputText = Profanity.SanitizeString(inputText);
		AddTagToProject();
	}

	private void AddTagToProject()
	{
		tagInputText = Profanity.SanitizeString(tagInputText);
		if (tapIndicator != null)
		{
			KillTagTeacher();
		}
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		string text = tagInputText.ToLower();
		if (string.IsNullOrEmpty(text) || dataModel.tags.Contains(text))
		{
			return;
		}
		if (dataModel.tags.Count >= BloxelProject.MAX_TAGS)
		{
			uiInputFieldTag.placeholder.color = ColorUtilities.blockRGB[BlockColor.Red];
			uiInputFieldTag.text = "Maximum Tags (6) Reached";
			uiInputFieldTag.interactable = false;
			return;
		}
		switch (dataModel.type)
		{
		case ProjectType.Board:
			((BloxelBoard)dataModel).AddTag(text);
			break;
		case ProjectType.Animation:
			((BloxelAnimation)dataModel).AddTag(text);
			break;
		case ProjectType.MegaBoard:
			((BloxelMegaBoard)dataModel).AddTag(text);
			break;
		case ProjectType.Character:
			((BloxelCharacter)dataModel).AddTag(text);
			break;
		case ProjectType.Game:
			((BloxelGame)dataModel).AddTag(text);
			break;
		}
		if (!AssetManager.availableTags.Contains(text))
		{
			AssetManager.availableTags.Add(text);
		}
		UITag uITag = CreateUITag(activeTagGrid);
		ActivateTag(uITag);
		uITag.tagText = text;
		uITag.generatedByUser = true;
		uiInputFieldTag.text = string.Empty;
	}

	private void Delete()
	{
		SoundManager.PlayEventSound(SoundEvent.Trash);
		bool flag = false;
		if (CurrentUser.instance.active)
		{
			StartCoroutine(S3Interface.instance.DeleteFileFromBucket(dataModel.syncInfo));
		}
		switch (dataModel.type)
		{
		case ProjectType.Board:
			flag = ((SavedBoard)focusedProject).Delete();
			break;
		case ProjectType.Animation:
			flag = ((SavedAnimation)focusedProject).Delete();
			break;
		case ProjectType.MegaBoard:
			if (focusedProject.dataModel.ID() == AssetManager.demoBackgroundID)
			{
				uiTextNoDelete.enabled = true;
				return;
			}
			flag = ((SavedMegaBoard)focusedProject).Delete();
			break;
		case ProjectType.Game:
			if (focusedProject.dataModel.ID() == AssetManager.poultryPanicID)
			{
				uiTextNoDelete.enabled = true;
				return;
			}
			flag = ((SavedGame)focusedProject).Delete();
			break;
		case ProjectType.Character:
			flag = ((SavedCharacter)focusedProject).Delete();
			break;
		}
		if (flag)
		{
			StartCoroutine(DestroyMe());
		}
	}

	public void AdjustScrollRectHeight(Transform t)
	{
		GridLayoutGroup component = t.GetComponent<GridLayoutGroup>();
		RectTransform component2 = t.GetComponent<RectTransform>();
		int childCount = t.childCount;
		int num = Mathf.CeilToInt((float)childCount / (float)component.constraintCount);
		float num2 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num * num2) + Mathf.Abs(num2);
		component2.sizeDelta = new Vector2(component2.sizeDelta.x, y);
		component2.anchoredPosition = Vector2.zero;
	}

	private IEnumerator DestroyMe()
	{
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		Dismiss();
	}

	private void SaveActiveTags()
	{
		List<string> list = new List<string>(activeTags.Count);
		for (int i = 0; i < activeTags.Count; i++)
		{
			list.Add(activeTags[i].tagText);
		}
		dataModel.OverrideTags(list);
		switch (dataModel.type)
		{
		case ProjectType.Board:
			((BloxelBoard)dataModel).Save();
			break;
		case ProjectType.Animation:
			((BloxelAnimation)dataModel).Save(false);
			break;
		case ProjectType.MegaBoard:
			((BloxelMegaBoard)dataModel).Save(false);
			break;
		case ProjectType.Character:
			((BloxelCharacter)dataModel).Save(false);
			break;
		case ProjectType.Game:
			((BloxelGame)dataModel).Save(false);
			break;
		case ProjectType.Level:
			break;
		}
	}

	private void SaveAndClose()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmA);
		dataModel.SetTitle(uiInputFieldTitle.text);
		if (!string.IsNullOrEmpty(uiInputFieldTag.text))
		{
			AddTagWithString(uiInputFieldTag.text);
		}
		SaveActiveTags();
		StartCoroutine(DestroyMe());
	}
}
