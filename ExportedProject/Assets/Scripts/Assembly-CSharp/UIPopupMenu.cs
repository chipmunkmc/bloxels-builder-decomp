using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupMenu : MonoBehaviour
{
	public delegate void HandleDismiss();

	public delegate void HandleInit();

	public static UIPopupMenu instance;

	[Tooltip("Reference populated programmatically")]
	public RawImage overlay;

	public bool isAnimating;

	public event HandleDismiss OnDismiss;

	public event HandleInit OnInit;

	public void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (overlay == null)
		{
			overlay = GetComponent<RawImage>();
		}
		overlay.color = new Color(0f, 0f, 0f, 0f);
	}

	public void Start()
	{
		isAnimating = true;
		Tweener tweener = null;
		tweener = ((BloxelCamera.instance != null && !DeviceManager.isPoopDevice) ? BloxelCamera.instance.Blur(overlay) : ((!(InfinityWallCamera.instance != null) || DeviceManager.isPoopDevice) ? overlay.DOColor(new Color(0f, 0f, 0f, 0.7f), UIAnimationManager.speedFast) : InfinityWallCamera.instance.Blur(overlay)));
		tweener.OnComplete(delegate
		{
			SoundManager.instance.PlaySound(SoundManager.instance.menuPopup);
			if (this.OnInit != null)
			{
				this.OnInit();
			}
			isAnimating = false;
		});
	}

	public virtual void Dismiss()
	{
		isAnimating = true;
		Tweener tweener = null;
		tweener = ((BloxelCamera.instance != null && !DeviceManager.isPoopDevice) ? BloxelCamera.instance.DeBlur(overlay) : ((!(InfinityWallCamera.instance != null) || DeviceManager.isPoopDevice) ? overlay.DOColor(new Color(0f, 0f, 0f, 0f), UIAnimationManager.speedFast) : InfinityWallCamera.instance.DeBlur(overlay)));
		tweener.OnComplete(delegate
		{
			if (this.OnDismiss != null)
			{
				this.OnDismiss();
			}
			isAnimating = false;
			UnityEngine.Object.Destroy(base.gameObject);
		});
	}
}
