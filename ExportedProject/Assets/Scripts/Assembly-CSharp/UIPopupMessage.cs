using TMPro;
using UnityEngine.UI;

public class UIPopupMessage : UIPopupMenu
{
	public string title;

	public string description;

	public UIButton uiButtonOk;

	public Text uiTextDescription;

	public TextMeshProUGUI uiTextTitle;

	private new void Start()
	{
		base.Start();
		uiButtonOk.OnClick += ButtonDismissOnClick;
		uiTextDescription.text = description;
		uiTextTitle.text = title;
	}

	private void ButtonDismissOnClick()
	{
		Dismiss();
	}
}
