using System;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupNPCConfig : UIConfigItem
{
	public delegate void HandleFlagChange(bool isOn);

	[Header("UI")]
	public RectTransform menuIndex;

	public UIButton uiButtonSave;

	public UIButton uiButtonDismiss;

	public InputField uiInputField;

	public UIButton uiToggleSetFlag;

	private bool showInputField;

	public RectTransform rectVisualizer;

	private UIProjectCover _powerupVisual;

	public event HandleFlagChange OnFlagChange;

	private new void Awake()
	{
		base.Awake();
		_powerupVisual = rectVisualizer.GetComponentInChildren<UIProjectCover>(true);
		ResetUI();
	}

	private new void Start()
	{
		base.Start();
		uiButtonSave.OnClick += Save;
		uiButtonDismiss.OnClick += Dismiss;
		base.OnInit += HandleMenuInit;
		BuildVisualizer();
		WorldLocation worldLocation = new WorldLocation(tile.tileInfo.bloxelLevel.id, tile.tileInfo.locationInLevel, tile.tileInfo.bloxelLevel.location);
		if (GameBuilderCanvas.instance.currentGame.gameEndFlag == worldLocation)
		{
			uiToggleSetFlag.Activate();
		}
		else
		{
			uiToggleSetFlag.DeActivate();
		}
		if (tile.tileInfo.bloxelLevel.npcTextBlocks.ContainsKey(tile.tileInfo.locationInLevel))
		{
			uiInputField.text = tile.tileInfo.bloxelLevel.npcTextBlocks[tile.tileInfo.locationInLevel];
		}
		if (uiToggleSetFlag.isOn)
		{
			uiInputField.interactable = false;
			uiInputField.textComponent.DOFade(0f, UIAnimationManager.speedFast);
		}
		uiToggleSetFlag.OnToggle += CheckFlag;
	}

	private void OnDestroy()
	{
		uiButtonSave.OnClick -= Save;
		uiButtonDismiss.OnClick -= Dismiss;
		base.OnInit -= HandleMenuInit;
		uiToggleSetFlag.OnToggle -= CheckFlag;
	}

	private void HandleMenuInit()
	{
		InitSequence();
	}

	private void ResetUI()
	{
		menuIndex.localScale = Vector3.zero;
	}

	private Sequence InitSequence()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(menuIndex.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		sequence.Play();
		return sequence;
	}

	private void BuildVisualizer()
	{
		if (!(tile == null))
		{
			BloxelProject projectModelAtLocation = tile.bloxelLevel.GetProjectModelAtLocation(tile.tileInfo.locationInLevel);
			if (projectModelAtLocation == null)
			{
				_powerupVisual.InitStatic(AssetManager.instance.boardSolidWhite);
			}
			else if (projectModelAtLocation.type == ProjectType.Board)
			{
				_powerupVisual.Init(new List<BloxelBoard> { projectModelAtLocation.coverBoard }, 1f, new CoverStyle(Color.clear));
			}
			else if (projectModelAtLocation.type == ProjectType.Animation)
			{
				_powerupVisual.Init(projectModelAtLocation as BloxelAnimation, new CoverStyle(Color.clear));
			}
		}
	}

	private void CheckFlag(UIButton btn)
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		if (btn.isOn)
		{
			uiInputField.interactable = false;
			uiInputField.textComponent.DOFade(0f, UIAnimationManager.speedFast);
			uiInputField.placeholder.DOFade(0f, UIAnimationManager.speedFast);
		}
		else
		{
			uiInputField.interactable = true;
			uiInputField.textComponent.DOFade(1f, UIAnimationManager.speedFast);
			uiInputField.placeholder.DOFade(1f, UIAnimationManager.speedFast);
		}
	}

	private void Save()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalA);
		tile.bloxelLevel.SetNPCBlockAtLocation(tile.tileInfo.locationInLevel, uiInputField.text);
		WorldLocation worldLocation = new WorldLocation(tile.bloxelLevel.id, tile.tileInfo.locationInLevel, tile.bloxelLevel.location);
		if (uiToggleSetFlag.isOn)
		{
			GameBuilderCanvas.instance.currentGame.SetGameEndFlagAtLocation(worldLocation);
		}
		else
		{
			GameBuilderCanvas.instance.currentGame.RemoveGameEndFlag(worldLocation);
		}
		if (this.OnFlagChange != null)
		{
			this.OnFlagChange(uiToggleSetFlag.isOn);
		}
		Dismiss();
	}
}
