using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupNews : UIPopupMenu
{
	public new static UIPopupNews instance;

	[Header("| ========= UI ========= |")]
	public ScrollRect uiScrollRect;

	public Transform uiGrid;

	public Object newsItemPrefab;

	private RectTransform _gridTransform;

	private new void Awake()
	{
		base.Awake();
		if (instance == null)
		{
			instance = this;
		}
		_gridTransform = uiGrid.GetComponent<RectTransform>();
	}

	private new void Start()
	{
		AnalyticsManager.SendEventBool(AnalyticsManager.Event.NewsOpen, true);
	}

	public void Init()
	{
		Populate();
	}

	private void Populate()
	{
		if (DeviceManager.isPoopDevice)
		{
			for (int i = 0; i < 2; i++)
			{
				CreateNewsItem(BloxelServerInterface.instance.newsItems[i]);
			}
		}
		else
		{
			for (int j = 0; j < BloxelServerInterface.instance.newsItems.Count; j++)
			{
				CreateNewsItem(BloxelServerInterface.instance.newsItems[j]);
			}
		}
		AdjustScrollRectHeight(uiGrid);
	}

	private IEnumerator ResetScrollPosition(RectTransform rect)
	{
		yield return new WaitForSeconds(0.5f);
		rect.anchoredPosition = Vector2.zero;
	}

	private void CreateNewsItem(NewsItem model)
	{
		GameObject gameObject = Object.Instantiate(newsItemPrefab) as GameObject;
		gameObject.transform.SetParent(uiGrid);
		gameObject.transform.SetAsFirstSibling();
		gameObject.transform.localScale = Vector3.one;
		UINewsItem component = gameObject.GetComponent<UINewsItem>();
		component.Init(model);
	}

	private void AdjustScrollRectHeight(Transform t)
	{
		GridLayoutGroup component = t.GetComponent<GridLayoutGroup>();
		RectTransform component2 = t.GetComponent<RectTransform>();
		int childCount = t.childCount;
		int num = Mathf.CeilToInt((float)childCount / (float)component.constraintCount);
		float num2 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num * num2);
		component2.sizeDelta = new Vector2(component2.sizeDelta.x, y);
		component2.anchoredPosition = Vector2.zero;
		StartCoroutine(ResetScrollPosition(component2));
	}

	public void Kill()
	{
		Object.DestroyImmediate(base.gameObject);
	}
}
