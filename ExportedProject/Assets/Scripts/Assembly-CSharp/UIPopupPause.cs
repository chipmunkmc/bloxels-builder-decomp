using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupPause : UIPopupMenu
{
	public new static UIPopupPause instance;

	[Header("Data")]
	public GameplayController.Mode mode;

	private BloxelGame _game;

	[Header("UI")]
	public Transform menuIndex;

	public Transform menuMap;

	public UIButton uiButtonDismiss;

	public UIButton uiButtonDismissMap;

	public UIButton uiButtonBack;

	public UIButton uiButtonMapBack;

	public UIButton uiButtonMap;

	public UIButton uiButtonRestart;

	public UIButton uiButtonReplayFromCheckpoint;

	public GameMap map;

	public UIGameMap uiGameMap;

	public Slider uiSliderMusicVolume;

	public Slider uiSliderSFXVolume;

	private Image uiImageBack;

	[Header("Prefabs")]
	public Object accountMenuPrefab;

	public Object userMenuPrefab;

	public new void Awake()
	{
		base.Awake();
		if (instance == null)
		{
			instance = this;
		}
		else
		{
			Object.Destroy(base.gameObject);
		}
		if (overlay == null)
		{
			overlay = GetComponent<RawImage>();
		}
		uiImageBack = uiButtonBack.GetComponentInChildren<UIButtonIcon>();
		menuMap.gameObject.SetActive(true);
		menuMap.localScale = Vector3.zero;
	}

	private new void Start()
	{
		base.Start();
		GameHUD.instance.StopTimer();
		base.OnInit += HandleMenuInit;
		mode = GameplayController.instance.currentMode;
		uiButtonDismiss.OnClick += Dismiss;
		uiButtonDismissMap.OnClick += Dismiss;
		uiButtonReplayFromCheckpoint.OnClick += ReplayFromCheckpoint;
		uiButtonBack.OnClick += BackButtonPressed;
		uiButtonMap.OnClick += ShowMap;
		uiButtonRestart.OnClick += Restart;
		uiButtonMapBack.OnClick += ShowIndex;
		uiSliderMusicVolume.DOValue(SoundManager.instance.musicVolume, UIAnimationManager.speedMedium);
		uiSliderSFXVolume.DOValue(SoundManager.instance.sfxVolume, UIAnimationManager.speedMedium);
		uiSliderMusicVolume.onValueChanged.AddListener(AdjustMusicVolume);
		uiSliderSFXVolume.onValueChanged.AddListener(AdjustSFXVolume);
		bool available = GameplayController.instance.inventoryManager.inventory[3].available;
		map = new GameMap(GameplayController.instance.currentGame, GameMap.Type.Gameplay, available);
		uiGameMap.Init(map);
	}

	private void OnDestroy()
	{
		base.OnInit -= HandleMenuInit;
		uiButtonDismiss.OnClick -= Dismiss;
		uiButtonDismissMap.OnClick -= Dismiss;
		uiButtonReplayFromCheckpoint.OnClick -= ReplayFromCheckpoint;
		uiButtonBack.OnClick -= BackButtonPressed;
		uiButtonMap.OnClick -= ShowMap;
		uiButtonRestart.OnClick -= Restart;
		uiButtonMapBack.OnClick -= ShowIndex;
	}

	private void HandleMenuInit()
	{
		menuIndex.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce);
	}

	public void Init()
	{
		Time.timeScale = 0f;
		if (AppStateManager.instance.currentScene == SceneName.PixelEditor_iPad)
		{
			uiImageBack.sprite = AssetManager.instance.iconEditor;
		}
		else if (AppStateManager.instance.previousScene == SceneName.Viewer)
		{
			uiImageBack.sprite = AssetManager.instance.iconInfinityWall;
		}
		else if (AppStateManager.instance.previousScene == SceneName.Home)
		{
			uiImageBack.sprite = AssetManager.instance.iconHome;
		}
		else if (AppStateManager.instance.previousScene == SceneName.PixelEditor_iPad)
		{
			uiImageBack.sprite = AssetManager.instance.iconEditor;
		}
		else
		{
			uiButtonBack.transform.localScale = Vector3.zero;
		}
		if (AppStateManager.instance.currentScene == SceneName.Home)
		{
			uiButtonBack.transform.localScale = Vector3.zero;
		}
	}

	public void SetGame(ref BloxelGame game)
	{
		_game = game;
	}

	private void AdjustMusicVolume(float val)
	{
		if (!(val > 1f) && !(val < 0f))
		{
			SoundManager.instance.musicVolume = val;
		}
	}

	private void AdjustSFXVolume(float val)
	{
		if (!(val > 1f) && !(val < 0f))
		{
			SoundManager.instance.sfxVolume = val;
		}
	}

	public void ShowIndex()
	{
		menuMap.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			menuIndex.DOScale(1f, UIAnimationManager.speedMedium);
		});
	}

	public void ShowMap()
	{
		menuIndex.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			uiGameMap.Init(map);
			menuMap.DOScale(1f, UIAnimationManager.speedMedium);
		});
	}

	public void BackButtonPressed()
	{
		switch (AppStateManager.instance.currentScene)
		{
		case SceneName.PixelEditor_iPad:
			GameHUD.instance.GoToEditor();
			break;
		case SceneName.Gameplay:
			Back();
			break;
		}
		Dismiss();
	}

	private void ReturnToHomeStart()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		Time.timeScale = 1f;
		GameplayController.instance.heroObject.transform.localPosition = GameplayController.instance.startingPosition;
		GameplayController.instance.StartDroppingCharacter();
		Dismiss();
	}

	private void Back()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		Time.timeScale = 1f;
		if (AppStateManager.instance.previousScene == SceneName.Viewer && !BloxelServerInterface.instance.receivingUpdates)
		{
			BloxelServerInterface.instance.Init();
			return;
		}
		BloxelsSceneManager.instance.GoTo(AppStateManager.instance.previousScene);
		Dismiss();
	}

	private void Restart()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		Time.timeScale = 1f;
		GameHUD.instance.Reset(GameHUD.ResetMethod.Full);
		GameplayController.instance.Replay();
		GameplayController.instance.lastCheckpoint = GameplayController.instance.startingPosition;
		GameplayController.instance.heroObject.transform.localPosition = GameplayController.instance.lastCheckpoint;
		PixelPlayerController.instance.Reset();
		GameplayController.instance.StartDroppingCharacter();
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		Dismiss();
	}

	private void ReplayFromCheckpoint()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		Time.timeScale = 1f;
		GameHUD.instance.Reset(GameHUD.ResetMethod.FromCheckpoint);
		GameplayController.instance.ReplayFromCheckpiont();
		PixelPlayerController.instance.Reset();
		GameplayController.instance.heroObject.transform.localPosition = GameplayController.instance.lastCheckpoint;
		GameplayController.instance.StartDroppingCharacter();
		PixelPlayerController.instance.ResetInventoryFromCheckpoint();
		BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
		Dismiss();
	}

	public override void Dismiss()
	{
		Time.timeScale = 1f;
		GameHUD.instance.EnableTimer();
		GameHUD.instance.touchControls.localScale = Vector3.one;
		base.Dismiss();
	}
}
