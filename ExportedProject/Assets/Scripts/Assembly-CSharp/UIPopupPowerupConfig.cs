using System;
using System.Collections.Generic;
using System.Threading;
using DG.Tweening;
using UnityEngine;

public class UIPopupPowerupConfig : UIConfigItem
{
	public delegate void HandleConfigure(PowerUpType type);

	[Header("Data")]
	public PowerUpType currentType;

	[Header("UI")]
	public RectTransform menuIndex;

	public UIPopupGemUnlock menuGemUnlock;

	public UIButton uiButtonDismiss;

	public UIButton uiButtonSubmit;

	public UIButtonGroup buttonGroup;

	public UIButton bomb;

	public UIButton health;

	public UIButton jetpack;

	public UIButton map;

	public UIButton shrink;

	public UIButton invincible;

	public RectTransform rectVisualizer;

	private UIProjectCover _powerupVisual;

	public event HandleConfigure OnConfigure;

	private new void Awake()
	{
		base.Awake();
		buttonGroup = GetComponentInChildren<UIButtonGroup>(true);
		_powerupVisual = rectVisualizer.GetComponentInChildren<UIProjectCover>(true);
		ResetUI();
	}

	private new void Start()
	{
		base.Start();
		bomb.uiGemLock.Init(new PowerupUnlockable(PowerUpType.Bomb));
		health.uiGemLock.Init(new PowerupUnlockable(PowerUpType.Health));
		jetpack.uiGemLock.Init(new PowerupUnlockable(PowerUpType.Jetpack));
		map.uiGemLock.Init(new PowerupUnlockable(PowerUpType.Map));
		shrink.uiGemLock.Init(new PowerupUnlockable(PowerUpType.Shrink));
		invincible.uiGemLock.Init(new PowerupUnlockable(PowerUpType.Invincibility));
		SubscribeEvents();
		buttonGroup.Reset();
		CheckExistingType();
		BuildVisualizer();
	}

	private void OnDestroy()
	{
		UnSubscribeEvents();
	}

	private void SubscribeEvents()
	{
		uiButtonSubmit.OnClick += SaveAndClose;
		uiButtonDismiss.OnClick += Dismiss;
		bomb.OnGemLock += HandleGemLock;
		health.OnGemLock += HandleGemLock;
		jetpack.OnGemLock += HandleGemLock;
		map.OnGemLock += HandleGemLock;
		shrink.OnGemLock += HandleGemLock;
		invincible.OnGemLock += HandleGemLock;
		base.OnInit += HandleMenuInit;
	}

	private void UnSubscribeEvents()
	{
		uiButtonSubmit.OnClick -= SaveAndClose;
		uiButtonDismiss.OnClick -= Dismiss;
		base.OnInit -= HandleMenuInit;
	}

	private void HandleGemLock(UIButton btn)
	{
		ShowUnlockMenu(btn.uiGemLock.model);
	}

	private void HandleMenuInit()
	{
		InitSequence().OnComplete(delegate
		{
		});
	}

	private void HandleUnlockDismiss()
	{
		menuGemUnlock.OnSiblingDismiss -= HandleUnlockDismiss;
		menuIndex.DOScale(1f, UIAnimationManager.speedMedium);
		rectVisualizer.DOScale(1f, UIAnimationManager.speedMedium);
	}

	private void ResetUI()
	{
		menuIndex.localScale = Vector3.zero;
		rectVisualizer.localScale = Vector3.zero;
	}

	private Sequence InitSequence()
	{
		Sequence sequence = DOTween.Sequence();
		sequence.Append(menuIndex.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutBounce));
		sequence.Append(rectVisualizer.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo));
		sequence.Append(rectVisualizer.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 5, 0f));
		sequence.Play();
		return sequence;
	}

	public void ShowUnlockMenu(GemUnlockable unlockable)
	{
		rectVisualizer.DOScale(0f, UIAnimationManager.speedMedium);
		menuIndex.DOScale(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			menuGemUnlock.Init(unlockable);
			menuGemUnlock.IntroSequence();
			menuGemUnlock.OnSiblingDismiss += HandleUnlockDismiss;
		});
	}

	private void ShowMenu()
	{
		base.transform.DOScale(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			BloxelCamera.instance.Blur(overlay);
		});
	}

	private void BuildVisualizer()
	{
		if (!(tile == null))
		{
			BloxelProject projectModelAtLocation = tile.bloxelLevel.GetProjectModelAtLocation(tile.tileInfo.locationInLevel);
			if (projectModelAtLocation == null)
			{
				_powerupVisual.InitStatic(AssetManager.instance.boardSolidPink);
			}
			else if (projectModelAtLocation.type == ProjectType.Board)
			{
				_powerupVisual.Init(new List<BloxelBoard> { projectModelAtLocation.coverBoard }, 1f, new CoverStyle(Color.clear));
			}
			else if (projectModelAtLocation.type == ProjectType.Animation)
			{
				_powerupVisual.Init(projectModelAtLocation as BloxelAnimation, new CoverStyle(Color.clear));
			}
		}
	}

	private void CheckExistingType()
	{
		if (tile == null)
		{
			return;
		}
		if (tile.bloxelLevel.powerups.TryGetValue(tile.tileInfo.locationInLevel, out currentType))
		{
			switch (currentType)
			{
			case PowerUpType.Bomb:
				bomb.Toggle();
				break;
			case PowerUpType.Health:
				health.Toggle();
				break;
			case PowerUpType.Jetpack:
				jetpack.Toggle();
				break;
			case PowerUpType.Map:
				map.Toggle();
				break;
			case PowerUpType.Shrink:
				shrink.Toggle();
				break;
			case PowerUpType.Invincibility:
				invincible.Toggle();
				break;
			case PowerUpType.Dash:
			case PowerUpType.BigBro:
				break;
			}
		}
		else
		{
			bomb.Toggle();
		}
	}

	private void SaveAndClose()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalA);
		PowerUpType powerUpType = PowerUpType.Bomb;
		if (jetpack.isOn)
		{
			powerUpType = PowerUpType.Jetpack;
		}
		else if (health.isOn)
		{
			powerUpType = PowerUpType.Health;
		}
		else if (map.isOn)
		{
			powerUpType = PowerUpType.Map;
		}
		else if (shrink.isOn)
		{
			powerUpType = PowerUpType.Shrink;
		}
		else if (invincible.isOn)
		{
			powerUpType = PowerUpType.Invincibility;
		}
		tile.bloxelLevel.SetPowerUpAtLocation(tile.tileInfo.locationInLevel, powerUpType);
		if (this.OnConfigure != null)
		{
			this.OnConfigure(powerUpType);
		}
		((BloxelPowerUpTile)tile).type = powerUpType;
		Dismiss();
	}
}
