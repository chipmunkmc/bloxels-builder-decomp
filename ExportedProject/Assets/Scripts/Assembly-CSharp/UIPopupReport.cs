using System.Collections;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupReport : UIPopupMenu
{
	public enum Type
	{
		Square = 0,
		User = 1
	}

	public Type type;

	public CanvasSquare square;

	public UserAccount userAccount;

	public UIRadioGroup radioGroup;

	public UIRadioButton offensive;

	public UIRadioButton broken;

	public InputField uiInputField;

	public UIButton uiButtonSubmit;

	private TextMeshProUGUI _uiButtonText;

	private new void Start()
	{
		base.Start();
		uiButtonSubmit.OnClick += Submit;
		_uiButtonText = uiButtonSubmit.GetComponentInChildren<TextMeshProUGUI>();
	}

	public void Init(CanvasSquare _canvasSquare)
	{
		square = _canvasSquare;
		type = Type.Square;
	}

	public void Init(UserAccount _userAccount)
	{
		userAccount = _userAccount;
		type = Type.User;
	}

	private IEnumerator ResetButtonTextDelay()
	{
		yield return new WaitForSeconds(2f);
		_uiButtonText.text = LocalizationManager.getInstance().getLocalizedText("challenges20", "Try Again");
	}

	private void Submit()
	{
		if (radioGroup.activeButton == null || string.IsNullOrEmpty(uiInputField.text))
		{
			_uiButtonText.text = LocalizationManager.getInstance().getLocalizedText("iWall114", "Requires Message");
			SoundManager.instance.PlaySound(SoundManager.instance.popB);
			StartCoroutine(ResetButtonTextDelay());
			return;
		}
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		int val = 0;
		for (int i = 0; i < radioGroup.radios.Length; i++)
		{
			if (radioGroup.radios[i] == radioGroup.activeButton)
			{
				val = i;
			}
		}
		string text = uiInputField.text;
		text = text.Replace("|", " ");
		JSONObject jSONObject = new JSONObject();
		if (type == Type.Square)
		{
			jSONObject.AddField("reason", val);
			jSONObject.AddField("coordinate", square.iWallCoordinate.ToString());
			jSONObject.AddField("details", text);
			StartCoroutine(BloxelServerRequests.instance.ReportSquare(square.serverSquare._id, jSONObject, delegate(string response)
			{
				Complete(response);
			}));
		}
		else if (type == Type.User)
		{
			jSONObject.AddField("reason", val.ToString());
			jSONObject.AddField("details", text);
			StartCoroutine(BloxelServerRequests.instance.ReportUser(userAccount, jSONObject, Complete));
		}
	}

	public void Complete(string res)
	{
		if (!string.IsNullOrEmpty(res) && res != "ERROR")
		{
			uiButtonSubmit.interactable = false;
			_uiButtonText.text = LocalizationManager.getInstance().getLocalizedText("iWall40", "Submitted Report!");
			StartCoroutine("DelayedDismiss");
		}
	}

	private IEnumerator DelayedDismiss()
	{
		yield return new WaitForSeconds(2f);
		Dismiss();
	}
}
