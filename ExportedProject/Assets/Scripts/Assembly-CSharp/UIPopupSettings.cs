using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupSettings : UIPopupMenu
{
	public Text uiTextVersion;

	public Text uiTextCaptureVersion;

	public Slider uiSliderMusicVolume;

	public Slider uiSliderSFXVolume;

	public Toggle uiToggleCellular;

	public UIButton uiButtonCopyGuest;

	public TextMeshProUGUI uiTextSyncAssets;

	private new void Start()
	{
		base.Start();
		uiTextVersion.text = AppStateManager.instance.appVersion;
		if (!string.IsNullOrEmpty(CaptureManager.DataVersion))
		{
			uiTextCaptureVersion.text = CaptureManager.DataVersion;
		}
		else
		{
			uiTextCaptureVersion.text = "0";
		}
		uiSliderMusicVolume.DOValue(SoundManager.instance.musicVolume, UIAnimationManager.speedMedium);
		uiSliderSFXVolume.DOValue(SoundManager.instance.sfxVolume, UIAnimationManager.speedMedium);
		uiSliderMusicVolume.onValueChanged.AddListener(AdjustMusicVolume);
		uiSliderSFXVolume.onValueChanged.AddListener(AdjustSFXVolume);
		uiToggleCellular.isOn = PrefsManager.instance.useCellualarData;
		uiButtonCopyGuest.OnClick += HandleCopyGuestAccount;
		uiToggleCellular.onValueChanged.AddListener(HandleToggleCellular);
		if (!CurrentUser.instance.active)
		{
			Graphic[] componentsInChildren = uiToggleCellular.GetComponentsInChildren<Graphic>();
			foreach (Graphic graphic in componentsInChildren)
			{
				graphic.color = new Color(graphic.color.r, graphic.color.g, graphic.color.b, 0.2f);
			}
			uiButtonCopyGuest.interactable = false;
			uiToggleCellular.interactable = false;
			uiTextSyncAssets.SetText(string.Empty);
			uiButtonCopyGuest.transform.localScale = Vector3.zero;
		}
		else
		{
			int guestAssetCount = BloxelProject.GetGuestAssetCount();
			string text = LocalizationManager.getInstance().getLocalizedText("account5", "Sync") + " " + guestAssetCount + " " + LocalizationManager.getInstance().getLocalizedText("nav15", "Assets to Your Account?");
			uiTextSyncAssets.SetText(text);
			uiButtonCopyGuest.transform.localScale = Vector3.one;
		}
	}

	public void HandleCopyGuestAccount()
	{
		AppStateManager.instance.acctTransitionState = AppStateManager.AccountTransitionState.GuestCopy;
		BloxelsSceneManager.instance.GoTo(SceneName.AccountTransition);
	}

	public void HandleToggleCellular(bool _isOn)
	{
		PrefsManager.instance.useCellualarData = _isOn;
	}

	private void AdjustMusicVolume(float val)
	{
		if (!(val > 1f) && !(val < 0f))
		{
			SoundManager.instance.musicVolume = val;
		}
	}

	private void AdjustSFXVolume(float val)
	{
		if (!(val > 1f) && !(val < 0f))
		{
			SoundManager.instance.sfxVolume = val;
		}
	}
}
