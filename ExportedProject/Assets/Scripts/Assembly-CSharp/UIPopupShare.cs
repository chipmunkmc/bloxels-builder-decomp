using System.Collections;
using TMPro;
using UnityEngine;

public class UIPopupShare : UIPopupMenu
{
	public ProjectType type;

	public CanvasSquare square;

	public UIButton uiButtonCopy;

	public TextMeshProUGUI uiText;

	public TextMeshProUGUI uiTextLink;

	public TextMeshProUGUI uiTextCoordinate;

	private string _copyString;

	public string copiedString = "Copied!";

	public string copyGameString = "Copy\nWebplayer Link";

	public string copyAssetString = "Copy Link";

	private string url;

	private new void Awake()
	{
		copiedString = LocalizationManager.getInstance().getLocalizedText("gamebuilder45", copiedString);
		copyGameString = LocalizationManager.getInstance().getLocalizedText("iWall33", copyGameString);
		copyAssetString = LocalizationManager.getInstance().getLocalizedText("iWall27", copyAssetString);
	}

	public void Init(CanvasSquare _square)
	{
		type = _square.serverSquare.type;
		uiTextCoordinate.text = _square.iWallCoordinate.ToString().Replace('|', ',');
		string empty = string.Empty;
		if (type == ProjectType.Game)
		{
			_copyString = copyGameString;
			empty = "http://bloxels.co/";
		}
		else
		{
			_copyString = copyAssetString;
			empty = "http://bloxelsbuilder.com/share?coordinate=";
		}
		square = _square;
		string text = empty + square.iWallCoordinate.ToString();
		text = empty + square.iWallCoordinate.ToString();
		uiTextLink.SetText(text);
		url = text.Replace("|", "%7C");
		uiButtonCopy.OnClick += Copy;
		uiText.text = _copyString;
	}

	private void Copy()
	{
		uiButtonCopy.OnClick -= Copy;
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		string empty = string.Empty;
		empty = ((type != ProjectType.Game) ? "Copied Link" : "Game Link");
		string[] results = new string[2]
		{
			empty,
			square.iWallCoordinate.ToString()
		};
		AnalyticsManager.SendEventObject(AnalyticsManager.Event.Share, results);
		if (AppStateManager.instance.CopyToClipBoard(url))
		{
			uiButtonCopy.interactable = false;
			uiText.text = copiedString;
			StartCoroutine("DelayedMessageReset");
		}
	}

	private IEnumerator DelayedMessageReset()
	{
		yield return new WaitForSeconds(2.5f);
		uiButtonCopy.interactable = true;
		uiText.text = _copyString;
	}
}
