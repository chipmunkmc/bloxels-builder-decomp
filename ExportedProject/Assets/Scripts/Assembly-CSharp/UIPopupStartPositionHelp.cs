using UnityEngine.UI;

public class UIPopupStartPositionHelp : UIPopup
{
	public Button uiButtonOK;

	private new void Start()
	{
		base.Start();
		uiButtonOK.onClick.AddListener(Dismiss);
	}

	private void Dismiss()
	{
		PrefsManager.instance.hasSeenStartPositionHelp = true;
		base.Dismiss();
	}
}
