using System;
using System.Threading;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupStateChange : UIPopupMenu
{
	public delegate void HandleChoice(BloxelProject _project);

	[Header("| ========= Data ========= |")]
	public BloxelProject project;

	[Header("| ========= UI ========= |")]
	public UIButton uiButtonYes;

	public UIButton uiButtonNo;

	public Text uiTextDescription;

	public event HandleChoice OnChoice;

	private new void Start()
	{
		base.Start();
		uiButtonYes.OnClick += Proceed;
		uiButtonNo.OnClick += Dismiss;
	}

	public void Init(BloxelProject _project, AnimationType state)
	{
		project = _project;
		string localizedText = LocalizationManager.getInstance().getLocalizedText("gamebuilder118", "Are you sure you want to replace the");
		string localizedText2 = LocalizationManager.getInstance().getLocalizedText("gamebuilder119", "Character State with a new");
		string localizedText3 = LocalizationManager.getInstance().getLocalizedText(BloxelProject.projectTypeDisplayKeys[project.type], project.type.ToString());
		string localizedText4 = LocalizationManager.getInstance().getLocalizedText(BloxelAnimation.animStateDisplayText[state], state.ToString());
		string text = localizedText + " " + localizedText4 + " " + localizedText2 + " " + localizedText3 + "?";
		uiTextDescription.text = text;
	}

	private void Proceed()
	{
		if (this.OnChoice != null)
		{
			this.OnChoice(project);
		}
		Dismiss();
	}
}
