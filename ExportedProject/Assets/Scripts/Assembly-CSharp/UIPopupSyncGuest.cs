using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupSyncGuest : UIPopupMenu
{
	public Image uiImageUserIcon;

	public TextMeshProUGUI uiTextUserName;

	public UIProjectCover userAvatar;

	public UIMenuPanel menuPanel;

	public UIButton uiButtonYes;

	public TextMeshProUGUI uiTextDynamicCount;

	private new void Start()
	{
		if (HomeController.instance != null)
		{
			HomeController.instance.HideForOverlay();
		}
		if (CurrentUser.instance.account.avatar != null)
		{
			userAvatar.Init(CurrentUser.instance.account.avatar, new CoverStyle(BloxelBoard.baseUIBoardColor));
			uiImageUserIcon.transform.localScale = Vector3.zero;
		}
		else
		{
			userAvatar.transform.localScale = Vector3.zero;
			uiImageUserIcon.transform.localScale = Vector3.one;
		}
		int guestAssetCount = BloxelProject.GetGuestAssetCount();
		string text = LocalizationManager.getInstance().getLocalizedText("account61", "There are") + " " + guestAssetCount + " " + LocalizationManager.getInstance().getLocalizedText("account62", "Unsynced Assets");
		uiTextDynamicCount.SetText(text);
		uiTextUserName.SetText(CurrentUser.instance.account.userName);
		uiButtonYes.OnClick += handleYes;
	}

	private void OnDestroy()
	{
		uiButtonYes.OnClick -= handleYes;
	}

	private void handleYes()
	{
		CurrentUser.syncWithGuest = true;
		Dismiss();
	}
}
