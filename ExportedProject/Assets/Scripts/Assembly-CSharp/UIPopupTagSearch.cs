using UnityEngine;
using UnityEngine.UI;

public class UIPopupTagSearch : UIPopupMenu
{
	[Header("UI")]
	public RectTransform rect;

	public UIButton uiButtonSubmit;

	public UIButton uiButtonAdd;

	public InputField uiInputFieldTag;

	public Animator animator;

	public GameObject tooltip;

	public Object uiTagPrefab;

	public Transform tagGrid;

	public Transform activeTagGrid;

	private new void Awake()
	{
		base.Awake();
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
		uiTagPrefab = Resources.Load("Prefabs/UITag");
	}

	private new void Start()
	{
		base.Start();
		uiInputFieldTag.onValueChange.AddListener(CheckForComma);
		uiInputFieldTag.onEndEdit.AddListener(delegate(string val)
		{
			if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
			{
				AddTagWithString(val);
			}
		});
		BuildTagCloud();
		CheckActiveTags();
	}

	private void OnEnable()
	{
		uiButtonSubmit.OnClick += OkPressed;
		uiButtonAdd.OnClick += AddButtonPressed;
	}

	private void OnDisable()
	{
		uiButtonSubmit.OnClick -= OkPressed;
		uiButtonAdd.OnClick -= AddButtonPressed;
	}

	private void CheckActiveTags()
	{
		if (UITagSearch.instance.activeTags.Count != 0)
		{
			for (int i = 0; i < UITagSearch.instance.activeTags.Count; i++)
			{
				CreateTag(UITagSearch.instance.activeTags[i]);
			}
		}
	}

	private void CheckForComma(string val)
	{
		if (val.Contains(","))
		{
			string[] array = val.Split(',');
			AddTagWithString(array[0]);
		}
	}

	private void AddButtonPressed()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		AddTagWithString(uiInputFieldTag.text);
	}

	private void AddTagWithString(string inputText)
	{
		if (!string.IsNullOrEmpty(inputText))
		{
			CreateTag(inputText);
		}
	}

	private void BuildTagCloud()
	{
		for (int i = 0; i < AssetManager.availableTags.Count; i++)
		{
			if (!(AssetManager.availableTags[i] == "PPHide") && !(AssetManager.availableTags[i] == "PixelTutz"))
			{
				GameObject gameObject = Object.Instantiate(uiTagPrefab) as GameObject;
				gameObject.transform.SetParent(tagGrid);
				gameObject.transform.localScale = Vector3.one;
				gameObject.transform.localPosition = Vector3.zero;
				gameObject.transform.localRotation = Quaternion.identity;
				UITag component = gameObject.GetComponent<UITag>();
				component.tagText = AssetManager.availableTags[i];
				component.ShowAdd();
				component.OnClick += AddToList;
				component.OnRemove += RemoveFromList;
			}
		}
		AdjustScrollRectHeight(tagGrid);
	}

	public void AdjustScrollRectHeight(Transform t)
	{
		GridLayoutGroup component = t.GetComponent<GridLayoutGroup>();
		RectTransform component2 = t.GetComponent<RectTransform>();
		int childCount = t.childCount;
		int num = Mathf.CeilToInt((float)childCount / (float)component.constraintCount);
		float num2 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num * num2) + Mathf.Abs(num2);
		component2.sizeDelta = new Vector2(component2.sizeDelta.x, y);
	}

	private void CreateTag(string _text)
	{
		GameObject gameObject = Object.Instantiate(uiTagPrefab) as GameObject;
		gameObject.transform.SetParent(activeTagGrid);
		gameObject.transform.SetAsFirstSibling();
		gameObject.transform.localScale = Vector3.one;
		UITag component = gameObject.GetComponent<UITag>();
		component.tagText = _text;
		component.generatedByUser = true;
		component.OnRemove += RemoveFromList;
		if (!UITagSearch.instance.activeTags.Contains(_text))
		{
			UITagSearch.instance.activeTags.Add(_text);
		}
		uiInputFieldTag.text = string.Empty;
		AdjustScrollRectHeight(activeTagGrid);
		FilterLibrary();
	}

	private void AddToList(UITag tag)
	{
		SoundManager.PlayOneShot(SoundManager.instance.drawOnBoard);
		tag.transform.SetParent(activeTagGrid);
		tag.transform.SetAsFirstSibling();
		tag.ShowDelete();
		if (!UITagSearch.instance.activeTags.Contains(tag.tagText))
		{
			UITagSearch.instance.activeTags.Add(tag.tagText);
		}
		AdjustScrollRectHeight(tagGrid);
		AdjustScrollRectHeight(activeTagGrid);
		FilterLibrary();
	}

	private void RemoveFromList(UITag tag)
	{
		tag.transform.SetParent(tagGrid);
		tag.transform.SetAsFirstSibling();
		tag.ShowAdd();
		if (UITagSearch.instance.activeTags.Contains(tag.tagText))
		{
			UITagSearch.instance.activeTags.Remove(tag.tagText);
		}
		if (tag.generatedByUser)
		{
			tag.DestroyMe();
		}
		AdjustScrollRectHeight(tagGrid);
		AdjustScrollRectHeight(activeTagGrid);
		FilterLibrary();
	}

	private void FilterLibrary()
	{
		UITagSearch.instance.FilterResults();
	}

	private void OkPressed()
	{
		Dismiss();
	}
}
