using System;
using System.Threading;
using UnityEngine;

public class UIPopupUploadGame : UIPopupMenu
{
	public delegate void HandleConfirm();

	public UIButton uiButtonConfirm;

	public event HandleConfirm onConfirm;

	public new void Start()
	{
		base.Start();
		uiButtonConfirm.OnClick += buttonPressed;
	}

	private void buttonPressed()
	{
		if (this.onConfirm != null)
		{
			this.onConfirm();
		}
		UnityEngine.Object.DestroyImmediate(base.gameObject);
	}
}
