using UnityEngine;

public class UIPopupUploading : UIPopupMenu
{
	[Header("UI")]
	[Header("Data")]
	public CanvasSquare square;

	public SavedProject projectOnDisk;

	public UIProgressBar progressBar;

	private new void Awake()
	{
		base.Awake();
		if (progressBar == null)
		{
			progressBar = GetComponentInChildren<UIProgressBar>();
		}
		progressBar.auto = true;
	}

	private new void Start()
	{
		base.Start();
	}

	private void ProgressBar_OnComplete()
	{
		Finished();
	}

	public void Finished()
	{
		Dismiss();
	}

	private void CancelUpload()
	{
		progressBar.StopProgress();
		Dismiss();
	}
}
