using System;
using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIPopupUser : UIPopupMenu
{
	[Header("| ========= UI ========= |")]
	public UIMenuPanel menuIndex;

	public UIMenuPanel menuLoggedOut;

	public UIButton uiButtonOk;

	public UIButton uiButtonSave;

	public UIButton uiButtonLogout;

	public TMP_InputField uiInputName;

	private UIInputValidation _validation;

	public TextMeshProUGUI _uiTextGemCount;

	public TextMeshProUGUI _uiTextCoinCount;

	private Texture2D tmpTex;

	public RawImage avatar;

	public UIButton uiButtonEditAvatar;

	public CoverBoard cover;

	public UserAccount userAccount;

	private bool didEditName;

	public float inputTimer = 0.35f;

	[Header("Board Picker")]
	public Direction positionDirection = Direction.Down;

	public Vector2 tipPosition = new Vector2(380f, -210f);

	public CaretDirection tipCaretDirection;

	public Vector2 tipSize = new Vector2(1000f, 200f);

	public UITipVisual tipVisual;

	private bool pickerEnabled;

	private new void Awake()
	{
		base.Awake();
		_validation = uiInputName.GetComponent<UIInputValidation>();
		avatar.texture = AssetManager.instance.boardSolidBlank;
		avatar.color = Color.white;
		menuLoggedOut.gameObject.SetActive(true);
		menuLoggedOut.transform.localScale = Vector3.zero;
		cover = new CoverBoard(Color.clear);
		tipVisual = new UITipVisual(new RectPosition(positionDirection, tipPosition), tipCaretDirection, tipSize);
	}

	private new void Start()
	{
		base.Start();
		uiButtonSave.OnClick += Save;
		uiButtonLogout.OnClick += Logout;
		uiButtonEditAvatar.OnClick += EditAvatar;
		uiButtonOk.OnClick += HandleTransition;
		ScrollPickerManager.instance.uiBoardPicker.OnProjectSelect += HandleProjectSelect;
		uiInputName.onValueChanged.AddListener(delegate
		{
			didEditName = true;
		});
		if (CurrentUser.instance != null && CurrentUser.instance.account != null)
		{
			uiInputName.text = CurrentUser.instance.account.userName;
		}
		uiInputName.onValueChanged.AddListener(delegate
		{
			StopCoroutine("CheckInputTimer");
			StartCoroutine("CheckInputTimer");
		});
		Populate();
	}

	private void OnDestroy()
	{
		ScrollPickerManager.instance.uiBoardPicker.HideImmediate();
		uiButtonSave.OnClick -= Save;
		uiButtonLogout.OnClick -= Logout;
		uiButtonEditAvatar.OnClick -= EditAvatar;
		uiButtonOk.OnClick -= HandleTransition;
		ScrollPickerManager.instance.uiBoardPicker.OnProjectSelect -= HandleProjectSelect;
		StopCoroutine("waitForSyncComplete");
	}

	private IEnumerator CheckInputTimer()
	{
		yield return new WaitForSeconds(inputTimer);
		if (CheckSimple())
		{
			StartCoroutine(CheckServer(delegate(bool exists)
			{
				if (exists)
				{
					uiButtonSave.interactable = false;
				}
				else
				{
					uiButtonSave.interactable = true;
				}
			}));
		}
		else
		{
			uiButtonSave.interactable = false;
		}
	}

	private bool CheckSimple()
	{
		if (!UIInputValidation.CheckEmpty(uiInputName))
		{
			return false;
		}
		if (!UIInputValidation.CheckNaughty(uiInputName))
		{
			return false;
		}
		return true;
	}

	private IEnumerator CheckServer(Action<bool> callback)
	{
		yield return _validation.CheckUserPropertyExists(RegistrationController.string_userName, uiInputName, delegate(bool exists)
		{
			callback(exists);
		});
	}

	private void HandleProjectSelect(BloxelProject project)
	{
		UpdateAvatar(project as BloxelBoard);
		ScrollPickerManager.instance.uiBoardPicker.Hide();
	}

	private void Populate()
	{
		if (CurrentUser.instance != null && CurrentUser.instance.account.avatar != null)
		{
			avatar.texture = CurrentUser.instance.account.avatar.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
		}
		_uiTextGemCount.SetText(PlayerBankManager.instance.GetGemCount().ToString());
		_uiTextCoinCount.SetText(PlayerBankManager.instance.GetCoinCount().ToString());
	}

	private void EditAvatar()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmB);
		if (!pickerEnabled)
		{
			ScrollPickerManager.instance.uiBoardPicker.Show(uiButtonEditAvatar.selfRect, tipVisual);
		}
		else
		{
			ScrollPickerManager.instance.uiBoardPicker.Hide();
		}
		pickerEnabled = !pickerEnabled;
	}

	private void UpdateAvatar(BloxelBoard b)
	{
		StartCoroutine(BloxelServerRequests.instance.SaveBoard(b.ToJSON(), delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				BloxelBoard bloxelBoard = new BloxelBoard(response, DataSource.JSONString);
				CurrentUser.instance.account.avatar = bloxelBoard;
				StartCoroutine(CurrentUser.instance.UpdateServer_Avatar(delegate(string uResponse)
				{
					if (!string.IsNullOrEmpty(uResponse))
					{
						CurrentUser.instance.Save();
						avatar.texture = CurrentUser.instance.account.avatar.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
					}
				}));
			}
		}));
	}

	private void Save()
	{
		SoundManager.PlaySoundClip(SoundClip.confirmA);
		uiButtonSave.interactable = false;
		uiButtonSave.GetComponentInChildren<TextMeshProUGUI>().text = LocalizationManager.getInstance().getLocalizedText("account104", "Saving...");
		if (CurrentUser.instance.account.userName == uiInputName.text)
		{
			Dismiss();
			return;
		}
		CurrentUser.instance.account.userName = uiInputName.text;
		StartCoroutine(CurrentUser.instance.UpdateServer_UserName(uiInputName.text, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				CurrentUser.instance.Save();
				Dismiss();
			}
			else
			{
				uiButtonSave.GetComponentInChildren<TextMeshProUGUI>().text = "Save Failed";
			}
		}));
	}

	private void Logout()
	{
		SoundManager.PlaySoundClip(SoundClip.cancelA);
		if (SaveManager.Instance.isProcessing)
		{
			StartCoroutine("waitForSyncComplete");
			return;
		}
		CurrentUser.instance.Logout();
		menuIndex.Hide().OnComplete(delegate
		{
			menuLoggedOut.Show();
		});
	}

	private IEnumerator waitForSyncComplete()
	{
		uiButtonLogout.GetComponentInChildren<TextMeshProUGUI>().SetText("Waiting...");
		while (SaveManager.Instance.isProcessing)
		{
			yield return new WaitForEndOfFrame();
		}
		CurrentUser.instance.Logout();
		menuIndex.Hide().OnComplete(delegate
		{
			menuLoggedOut.Show();
		});
	}

	public void HandleTransition()
	{
		CurrentUser.instance.SwitchAccounts();
	}
}
