using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIProgressBar : MonoBehaviour
{
	public delegate void HandleOnComplete();

	[Header("UI")]
	public GridLayoutGroup uiGridLayout;

	public Image[] elements;

	private int lastActiveElement = -1;

	private Tweener progressTween;

	private Animator animator;

	public bool auto;

	public event HandleOnComplete OnComplete;

	private void Awake()
	{
		if (animator == null)
		{
			animator = GetComponent<Animator>();
		}
	}

	private void Start()
	{
		if (!auto)
		{
			lastActiveElement = -1;
			for (int i = 0; i < elements.Length; i++)
			{
				elements[i].enabled = false;
			}
		}
	}

	public void Increment()
	{
		if (lastActiveElement < elements.Length - 1)
		{
			elements[++lastActiveElement].enabled = true;
			SoundManager.instance.PlayProgressSound(SoundManager.instance.captureTones[lastActiveElement]);
		}
	}

	public void Reset()
	{
		animator.enabled = false;
		lastActiveElement = -1;
		for (int i = 0; i < elements.Length; i++)
		{
			elements[i].transform.localScale = Vector3.one;
			elements[i].enabled = false;
		}
	}

	public void FakeIncrement()
	{
		for (int i = 0; i < elements.Length; i++)
		{
			elements[i].enabled = true;
		}
		animator.enabled = true;
	}

	public void StopProgress()
	{
		if (progressTween != null)
		{
			progressTween.Kill();
		}
		Reset();
	}

	private void Complete()
	{
		if (this.OnComplete != null)
		{
			this.OnComplete();
		}
	}
}
