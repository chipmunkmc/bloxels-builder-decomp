using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(RawImage))]
public class UIProjectCover : MonoBehaviour
{
	private List<BloxelBoard> _frames = new List<BloxelBoard>();

	private float _frameRate = 6f;

	public RawImage uiRawImage;

	private CoverBoard _cover;

	public bool shouldAnimate;

	private float timerStart;

	private int currentFrameIndex;

	private void Awake()
	{
		uiRawImage = GetComponent<RawImage>();
	}

	private void OnDisable()
	{
		if (shouldAnimate)
		{
			StopAnimating();
		}
	}

	public void InitStatic(Texture2D texture)
	{
		_cover = new CoverBoard(new CoverStyle(Color.clear));
		uiRawImage.texture = texture;
	}

	public void Init(BloxelAnimation bloxelAnimation, CoverStyle coverStyle, bool shouldAnimate = true)
	{
		_cover = new CoverBoard(coverStyle);
		if (bloxelAnimation != null && bloxelAnimation.boards != null)
		{
			_frames = bloxelAnimation.boards;
			_frameRate = bloxelAnimation.GetFrameRate();
			if (shouldAnimate)
			{
				Animate();
			}
		}
	}

	public void Init(BloxelCharacter bloxelCharacter, AnimationType state, CoverStyle coverStyle, bool shouldAnimate = true)
	{
		_cover = new CoverBoard(coverStyle);
		if (bloxelCharacter != null && bloxelCharacter.animations != null && bloxelCharacter.animations[state] != null && bloxelCharacter.animations[state].boards != null)
		{
			_frames = bloxelCharacter.animations[state].boards;
			_frameRate = bloxelCharacter.animations[state].GetFrameRate();
			if (shouldAnimate)
			{
				Animate();
			}
		}
	}

	public void Init(List<BloxelBoard> boards, float frameRate, CoverStyle coverStyle, bool shouldAnimate = true)
	{
		_cover = new CoverBoard(coverStyle);
		_frames = boards;
		_frameRate = frameRate;
		if (shouldAnimate)
		{
			Animate();
		}
	}

	public void Init(BloxelBoard board, CoverStyle coverStyle)
	{
		_cover = new CoverBoard(coverStyle);
		_frames = new List<BloxelBoard> { board };
		_frameRate = 1f;
		uiRawImage.texture = board.coverBoard.UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
	}

	public void Init(BloxelMegaBoard megaBoard, CoverStyle coverStyle)
	{
		_cover = new CoverBoard(coverStyle);
		_frames = new List<BloxelBoard>();
		_frameRate = 1f;
		uiRawImage.texture = megaBoard.GetDetailTexture();
	}

	public void ResetData(BloxelAnimation animation)
	{
		if (animation == null)
		{
			shouldAnimate = false;
			return;
		}
		_frames = animation.boards;
		_frameRate = animation.GetFrameRate();
		if (shouldAnimate)
		{
			Animate();
		}
	}

	private void Update()
	{
		if (base.gameObject.activeInHierarchy && shouldAnimate && _frames.Count > 0 && timerStart + 1f / _frameRate < Time.time)
		{
			currentFrameIndex = (currentFrameIndex + 1) % _frames.Count;
			uiRawImage.texture = _frames[currentFrameIndex].UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
			timerStart = Time.time;
		}
	}

	public void UpdateFrameRate(float rate)
	{
		_frameRate = rate;
	}

	public void Animate()
	{
		shouldAnimate = true;
		timerStart = Time.time;
		currentFrameIndex = 0;
	}

	public void StopAnimating()
	{
		shouldAnimate = false;
		timerStart = Time.time;
		currentFrameIndex = 0;
		if (_frames.Count > 0)
		{
			uiRawImage.texture = _frames[currentFrameIndex].UpdateTexture2D(ref _cover.colors, ref _cover.texture, _cover.style);
		}
	}
}
