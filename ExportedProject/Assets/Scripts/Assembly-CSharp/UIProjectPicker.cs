using System;
using System.Collections.Generic;
using System.Threading;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

public abstract class UIProjectPicker : UIScrollTipController, IEnhancedScrollerDelegate
{
	public delegate void HandleProjectSelect(BloxelProject project);

	[Header("Data")]
	public ProjectType type;

	public List<BloxelProject> projectData = new List<BloxelProject>(64);

	[Header("Prefabs")]
	public EnhancedScrollerCellView scrollElementPrefab;

	public event HandleProjectSelect OnProjectSelect;

	public int GetNumberOfCells(EnhancedScroller scroller)
	{
		return projectData.Count;
	}

	public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
	{
		return 140f;
	}

	public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
	{
		UIProjectScrollElement uIProjectScrollElement = scroller.GetCellView(scrollElementPrefab) as UIProjectScrollElement;
		uIProjectScrollElement.Init(projectData[dataIndex]);
		uIProjectScrollElement.OnClick -= HandleOnClick;
		uIProjectScrollElement.OnClick += HandleOnClick;
		return uIProjectScrollElement;
	}

	private void HandleOnClick(UIProjectScrollElement element)
	{
		if (this.OnProjectSelect != null)
		{
			this.OnProjectSelect(element.bloxelProject);
		}
	}
}
