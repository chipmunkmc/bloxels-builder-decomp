using System;
using System.Threading;
using DG.Tweening;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

[RequireComponent(typeof(UIProjectCover))]
[RequireComponent(typeof(UIInteractable))]
public class UIProjectScrollElement : EnhancedScrollerCellView
{
	public delegate void HandleOnClick(UIProjectScrollElement uiProjectScrollElement);

	public BloxelProject bloxelProject;

	public UIInteractable uiInteractable;

	public UIProjectCover uiProjectCover;

	public CoverStyle style;

	public Color uiFillColor = BloxelBoard.baseUIBoardColor;

	public event HandleOnClick OnClick;

	private void Awake()
	{
		style = new CoverStyle(uiFillColor);
	}

	private void OnEnable()
	{
		uiInteractable.OnClick += ElementClicked;
		uiInteractable.OnDown += ElementPressed;
		uiInteractable.OnUp += ElementReleased;
	}

	private void OnDisable()
	{
		uiInteractable.OnClick -= ElementClicked;
		uiInteractable.OnDown -= ElementPressed;
		uiInteractable.OnUp -= ElementReleased;
	}

	private void ElementClicked()
	{
		if (this.OnClick != null)
		{
			this.OnClick(this);
		}
	}

	private void ElementPressed()
	{
		base.transform.DOScale(0.9f, UIAnimationManager.speedFast);
	}

	private void ElementReleased()
	{
		base.transform.DOScale(1f, UIAnimationManager.speedFast);
	}

	public void Init(BloxelProject project)
	{
		bloxelProject = project;
		switch (project.type)
		{
		case ProjectType.Board:
			uiProjectCover.Init(project as BloxelBoard, style);
			break;
		case ProjectType.Animation:
			uiProjectCover.Init(project as BloxelAnimation, style);
			break;
		case ProjectType.Character:
			uiProjectCover.Init(project as BloxelCharacter, AnimationType.Idle, style);
			break;
		case ProjectType.MegaBoard:
			uiProjectCover.Init(project as BloxelMegaBoard, style);
			break;
		case ProjectType.Game:
			break;
		case ProjectType.Level:
			break;
		}
	}
}
