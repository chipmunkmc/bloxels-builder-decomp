using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIQuickStartTemplate : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public BloxelProject dataModel;

	[Header("Data")]
	public QuickStartGroup menuParent;

	public ThemeType theme;

	[Header("UI")]
	public RawImage rawImage;

	public Image outline;

	public CoverBoard cover;

	[Header("State")]
	public bool isActive;

	private void Awake()
	{
		cover = new CoverBoard(BloxelBoard.baseUIBoardColor);
	}

	private void Start()
	{
		SetupImage();
	}

	private void SetupImage()
	{
		outline.enabled = false;
		rawImage.texture = dataModel.coverBoard.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
	}

	public void Activate()
	{
		isActive = true;
		outline.enabled = true;
	}

	public void DeActivate()
	{
		isActive = false;
		outline.enabled = false;
	}

	public void OnPointerClick(PointerEventData eData)
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmC);
		Activate();
		menuParent.SetCurrentChoice(dataModel);
		for (int i = 0; i < menuParent.choices.Length; i++)
		{
			if (menuParent.choices[i] != this)
			{
				menuParent.choices[i].DeActivate();
			}
		}
		switch (menuParent.templateType)
		{
		case QuickStartGroup.Type.Layout:
			GameQuickStartMenu.instance.templateLevelChoice = dataModel as BloxelBoard;
			break;
		case QuickStartGroup.Type.Character:
			GameQuickStartMenu.instance.templateCharacterChoice = dataModel as BloxelCharacter;
			break;
		case QuickStartGroup.Type.Theme:
			GameQuickStartMenu.instance.templateThemeChoice = theme;
			GameQuickStartMenu.instance.templateBackgroundChoice = AssetManager.instance.themeBackgrounds[(int)theme];
			break;
		}
		GameQuickStartMenu.instance.CheckForReady();
	}
}
