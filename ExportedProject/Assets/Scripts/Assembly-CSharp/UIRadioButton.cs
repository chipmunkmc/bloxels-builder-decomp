using System;
using System.Threading;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIRadioButton : Toggle
{
	public delegate void HandleClick(UIRadioButton button);

	public int groupIndex;

	public event HandleClick OnClick;

	public void SetActive()
	{
		if (this.OnClick != null)
		{
			this.OnClick(this);
		}
	}

	public override void OnPointerClick(PointerEventData eData)
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		if (this.OnClick != null)
		{
			this.OnClick(this);
		}
	}

	public override void OnSubmit(BaseEventData eventData)
	{
		if (this.OnClick != null)
		{
			this.OnClick(this);
		}
	}
}
