using System;
using System.Threading;
using UnityEngine;

public class UIRadioGroup : MonoBehaviour
{
	public delegate void HandleActiveChange(UIRadioButton btn);

	public UIRadioButton[] radios;

	public UIRadioButton activeButton;

	public event HandleActiveChange OnChange;

	private void Start()
	{
		radios = GetComponentsInChildren<UIRadioButton>();
		InitToggleListeners();
	}

	private void InitToggleListeners()
	{
		for (int i = 0; i < radios.Length; i++)
		{
			radios[i].OnClick += RadioClick;
		}
	}

	private void SetActiveButton(UIRadioButton btn)
	{
		activeButton = btn;
		if (this.OnChange != null)
		{
			this.OnChange(btn);
		}
	}

	public void RadioClick(UIRadioButton button)
	{
		for (int i = 0; i < radios.Length; i++)
		{
			radios[i].isOn = false;
		}
		button.isOn = true;
		SetActiveButton(button);
	}
}
