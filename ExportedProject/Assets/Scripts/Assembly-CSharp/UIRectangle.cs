using System;
using System.Collections;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIRectangle : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleOnClick();

	private RectTransform _rect;

	private RectPosition _rectPosition;

	private Vector2 _size;

	private Image _uiImage;

	private Color _color;

	public RectPosition startingPosition;

	public Vector2 startSize;

	public event HandleOnClick OnClick;

	private void Awake()
	{
		_rect = GetComponent<RectTransform>();
		_uiImage = GetComponent<Image>();
	}

	public void Init(Color color, bool blockInput = true)
	{
		_color = color;
		_uiImage.color = _color;
		_uiImage.raycastTarget = blockInput;
	}

	public void Init(RectPosition rectPos, Vector2 size, Color color, bool animate = false, bool blockInput = true)
	{
		_size = size;
		startSize = size;
		_rectPosition = rectPos;
		startingPosition = rectPos;
		_color = color;
		_uiImage.color = _color;
		_uiImage.raycastTarget = blockInput;
		Position(animate);
	}

	public void Position(bool animate)
	{
		Vector2[] anchorsFromDirection = PPUtilities.GetAnchorsFromDirection(_rectPosition.dir);
		_rect.anchorMin = anchorsFromDirection[0];
		_rect.anchorMax = anchorsFromDirection[1];
		_rect.pivot = _rectPosition.pivot;
		if (animate)
		{
			StopCoroutine("PositionRoutine");
			StartCoroutine("PositionRoutine");
		}
		else
		{
			_rect.anchoredPosition = _rectPosition.pos;
			_rect.sizeDelta = _size;
		}
	}

	public void RePosition(RectPosition rectPos, Vector2 size, bool animate = false)
	{
		_size = size;
		_rectPosition = rectPos;
		Position(animate);
	}

	public void ReSize(Vector2 size, bool animate = false)
	{
		_size = size;
		Position(animate);
	}

	public void ChangeColor(Color color, bool animate)
	{
		if (animate)
		{
			_uiImage.DOColor(color, UIAnimationManager.speedMedium);
		}
		else
		{
			_uiImage.color = color;
		}
	}

	public void SetSprite(Sprite sprite)
	{
		_uiImage.sprite = sprite;
	}

	public void SetInputBlocking(bool shouldBlock)
	{
		_uiImage.raycastTarget = shouldBlock;
	}

	private IEnumerator PositionRoutine()
	{
		yield return new WaitForEndOfFrame();
		_rect.DOAnchorPos(_rectPosition.pos, UIAnimationManager.speedMedium);
		_rect.DOSizeDelta(_size, UIAnimationManager.speedMedium);
	}

	public void FillScreen(bool animate)
	{
		RectPosition rectPos = new RectPosition(Direction.All, Vector2.zero);
		Vector2 size = (startSize = new Vector2((float)Screen.width * 2f, (float)Screen.height * 2f));
		startingPosition = rectPos;
		RePosition(rectPos, size, animate);
	}

	private void Top(float height, bool animate = false)
	{
		RectPosition rectPos = new RectPosition(Direction.Up, Vector2.zero);
		Vector2 size = (startSize = new Vector2((float)Screen.width * 2f, height));
		startingPosition = rectPos;
		RePosition(rectPos, size, animate);
	}

	private void Left(float width, bool animate = false)
	{
		RectPosition rectPos = new RectPosition(Direction.Left, Vector2.zero);
		Vector2 size = (startSize = new Vector2(width, (float)Screen.height * 2f));
		startingPosition = rectPos;
		RePosition(rectPos, size, animate);
	}

	private void Right(float width, bool animate = false)
	{
		RectPosition rectPos = new RectPosition(Direction.Right, Vector2.zero);
		Vector2 size = (startSize = new Vector2(width, (float)Screen.height * 2f));
		startingPosition = rectPos;
		RePosition(rectPos, size, animate);
	}

	private void Down(float height, bool animate = false)
	{
		RectPosition rectPos = new RectPosition(Direction.Down, Vector2.zero);
		Vector2 size = (startSize = new Vector2((float)Screen.width * 2f, height));
		startingPosition = rectPos;
		RePosition(rectPos, size, animate);
	}

	public void DestroyMe(float delay = 0f)
	{
		UnityEngine.Object.Destroy(base.gameObject, delay);
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (this.OnClick != null)
		{
			this.OnClick();
		}
	}
}
