using UnityEngine;
using UnityEngine.UI;

public class UIRetinaText : MonoBehaviour
{
	private Text uiText;

	private void Awake()
	{
		uiText = GetComponent<Text>();
		uiText.fontSize *= 2;
		uiText.rectTransform.sizeDelta = uiText.rectTransform.sizeDelta * 2f;
		uiText.transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
	}
}
