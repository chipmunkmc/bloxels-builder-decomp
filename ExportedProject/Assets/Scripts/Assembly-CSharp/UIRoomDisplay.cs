using System;
using System.Threading;
using UnityEngine;

public class UIRoomDisplay : MonoBehaviour
{
	public delegate void HandleSelect(UIRoomDisplay _roomDisplay);

	public UIProjectCover uiProjectCover;

	public BloxelBoard bloxelBoard;

	public UIButton uiButton;

	public event HandleSelect OnSelect;

	private void Start()
	{
		uiButton.OnClick += HandleOnClick;
	}

	private void OnDestroy()
	{
		uiButton.OnClick -= HandleOnClick;
	}

	public void Init(BloxelBoard board)
	{
		bloxelBoard = board;
		uiProjectCover.Init(bloxelBoard, new CoverStyle(BloxelBoard.baseUIBoardColor, 4, 2, 1, true));
	}

	private void HandleOnClick()
	{
		if (this.OnSelect != null)
		{
			this.OnSelect(this);
		}
	}
}
