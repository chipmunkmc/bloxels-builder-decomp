using System;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using VoxelEngine;

public class UIRoomSwap : MonoBehaviour
{
	[Header("UI")]
	public UIButton uiButtonActivate;

	public UIButton uiButtonCaptureTrigger;

	public RectTransform rectExplainer;

	public CanvasGroup uiGroupIndicator;

	public CanvasGroup uiGroupOptions;

	public UITapOverlay uiOverlay;

	public RectTransform _optionsRect;

	public float indicatorDelay = 2f;

	private RectTransform _selfRect;

	private Vector3 _offset = new Vector3(6f, 18f, 0f);

	private Vector3 _indicatorPosition;

	private GridLocation _indicatorLoc;

	private GridLocation _room;

	private GridLocation _roomToUpdate;

	private GridLocation[] _triggers;

	public Bounds bounds;

	private UIBoard3D _uiBoard3d;

	private RectTransform _rectBoard3d;

	[Header("State Tracking")]
	public bool playerInZone;

	private bool _cameraIsOffset;

	private bool _menuActive;

	[Header("Materials")]
	public Material blockMaterial;

	public Color colorDark;

	[Header("Menu")]
	public RectTransform[] menuItems;

	private UIRoomDisplay[] _uiRoomDisplays;

	public Vector3 cameraOffset;

	private void Awake()
	{
		_selfRect = GetComponent<RectTransform>();
		_uiBoard3d = GetComponentInChildren<UIBoard3D>();
		_uiRoomDisplays = GetComponentsInChildren<UIRoomDisplay>();
		_rectBoard3d = _uiBoard3d.GetComponent<RectTransform>();
		for (int i = 0; i < menuItems.Length; i++)
		{
			menuItems[i].anchoredPosition = Vector2.zero;
		}
		_selfRect.localPosition = new Vector3(0f, 0f, 100f);
		_room = new GridLocation(7, 8);
		_roomToUpdate = new GridLocation(8, 8);
		_triggers = new GridLocation[6]
		{
			new GridLocation(6 + 13 * _room.c, 1 + 13 * _room.r),
			new GridLocation(7 + 13 * _room.c, 1 + 13 * _room.r),
			new GridLocation(8 + 13 * _room.c, 1 + 13 * _room.r),
			new GridLocation(9 + 13 * _room.c, 1 + 13 * _room.r),
			new GridLocation(10 + 13 * _room.c, 1 + 13 * _room.r),
			new GridLocation(11 + 13 * _room.c, 1 + 13 * _room.r)
		};
		_indicatorLoc = new GridLocation(10 + 13 * _room.c, 1 + 13 * _room.r);
		_indicatorPosition = new Vector3(_indicatorLoc.c * 13, _indicatorLoc.r * 13);
	}

	private void Start()
	{
		uiGroupOptions.alpha = 0f;
		uiGroupIndicator.alpha = 0f;
		uiGroupIndicator.blocksRaycasts = false;
		uiGroupIndicator.interactable = false;
		uiGroupOptions.blocksRaycasts = false;
		uiGroupOptions.interactable = false;
		uiOverlay.image.raycastTarget = false;
		rectExplainer.localScale = Vector3.zero;
		_rectBoard3d.anchoredPosition = Vector2.zero;
		uiButtonActivate.OnClick += HandleIndicatorPress;
		uiOverlay.OnClick += HandleOverlayClick;
		uiButtonCaptureTrigger.OnClick += HandleCapturePress;
		CaptureManager.instance.OnCaptureConfirm += HandleCapture;
		setupBoards();
	}

	private void OnDestroy()
	{
		uiButtonActivate.OnClick -= HandleIndicatorPress;
		uiOverlay.OnClick -= HandleOverlayClick;
		uiButtonCaptureTrigger.OnClick -= HandleCapturePress;
		CaptureManager.instance.OnCaptureConfirm -= HandleCapture;
		for (int i = 0; i < _uiRoomDisplays.Length; i++)
		{
			_uiRoomDisplays[i].OnSelect -= HandleLayoutSelect;
		}
	}

	private void LateUpdate()
	{
		if (!(PixelPlayerController.instance == null) && !PixelPlayerController.instance.isShrunk && !PixelPlayerController.instance.isUsingShrinkPotion)
		{
			SetPosition();
			tryIndicator();
		}
	}

	public void SetPosition()
	{
		BloxelTile value = null;
		if (GameplayBuilder.instance.tiles.TryGetValue(new WorldPos2D(_indicatorLoc.c * 13, _indicatorLoc.r * 13), out value))
		{
			Vector3 position = BloxelCamera.instance.mainCamera.WorldToScreenPoint(value.selfTransform.position + _offset);
			_selfRect.position = UIOverlayCanvas.instance.overlayCamera.ScreenToWorldPoint(position);
		}
	}

	public void Reset()
	{
		_uiBoard3d.Init(new BloxelBoard());
	}

	public void ResetMaterials()
	{
		blockMaterial.color = Color.white;
		LevelBuilder.instance.gameBackground.renderer.material.color = Color.white;
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.color = Color.white;
		}
	}

	private void setupBoards()
	{
		_uiBoard3d.Init(new BloxelBoard());
		for (int i = 0; i < _uiRoomDisplays.Length; i++)
		{
			_uiRoomDisplays[i].Init(AssetManager.instance.wireframeTemplates[i]);
			_uiRoomDisplays[i].OnSelect += HandleLayoutSelect;
		}
	}

	private void tryIndicator()
	{
		uiGroupIndicator.DOKill();
		if (playerInZone)
		{
			_selfRect.localScale = Vector3.one;
			if (!_cameraIsOffset)
			{
				BloxelCamera.instance.activeController.SetTempCameraOffset(BloxelCamera.instance.activeController.cameraOffset + cameraOffset);
				_cameraIsOffset = true;
			}
			uiGroupIndicator.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				uiGroupIndicator.blocksRaycasts = true;
				uiGroupIndicator.interactable = true;
				uiGroupOptions.blocksRaycasts = true;
				uiGroupOptions.interactable = true;
			});
		}
		else
		{
			_selfRect.localScale = Vector3.zero;
			_cameraIsOffset = false;
			uiGroupIndicator.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
			{
				uiGroupIndicator.blocksRaycasts = false;
				uiGroupIndicator.interactable = false;
				uiGroupOptions.blocksRaycasts = false;
				uiGroupOptions.interactable = false;
			});
			if (_menuActive)
			{
				HandleOverlayClick();
			}
		}
	}

	private void killAllTweens()
	{
		blockMaterial.DOKill();
		uiGroupOptions.DOKill();
		uiOverlay.image.DOKill();
	}

	private void HandleLayoutSelect(UIRoomDisplay layout)
	{
		_uiBoard3d.Init(layout.bloxelBoard);
		GameplayBuilder.instance.ReplaceLevelAtLocation(new BloxelLevel(_roomToUpdate, layout.bloxelBoard));
	}

	private void HandleOverlayClick()
	{
		_menuActive = false;
		killAllTweens();
		animateMenuIn();
		uiOverlay.transform.DOScale(0f, UIAnimationManager.speedMedium);
		uiGroupOptions.DOFade(0f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroupIndicator.blocksRaycasts = false;
			uiGroupIndicator.interactable = false;
			uiGroupOptions.blocksRaycasts = false;
			uiGroupOptions.interactable = false;
		});
		fadeInMats();
		uiButtonActivate.interactable = true;
		HomeController.instance.touchControls.transform.localScale = Vector3.one;
	}

	public void HandleIndicatorPress()
	{
		_menuActive = true;
		killAllTweens();
		uiButtonActivate.interactable = false;
		uiOverlay.transform.DOScale(1f, UIAnimationManager.speedMedium);
		uiGroupOptions.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiGroupIndicator.blocksRaycasts = true;
			uiGroupIndicator.interactable = true;
			uiGroupOptions.blocksRaycasts = true;
			uiGroupOptions.interactable = true;
		}).OnComplete(delegate
		{
			animateMenuOut();
		});
		fadeOutMats();
		HomeController.instance.touchControls.transform.localScale = Vector3.zero;
	}

	private void HandleCapturePress()
	{
		CaptureManager.instance.Show();
	}

	private void HandleCapture()
	{
		if (_menuActive)
		{
			BloxelBoard bloxelBoard = new BloxelBoard(CaptureManager.latest, false);
			_uiBoard3d.Init(bloxelBoard);
			GameplayBuilder.instance.ReplaceLevelAtLocation(new BloxelLevel(_roomToUpdate, bloxelBoard));
		}
	}

	private void animateMenuOut()
	{
		float num = _optionsRect.sizeDelta.x / 2f;
		float num2 = (float)Math.PI * 2f / (float)menuItems.Length / 2f;
		float x = _optionsRect.sizeDelta.x;
		float y = _optionsRect.sizeDelta.y;
		float num3 = ((menuItems.Length % 2 != 0) ? (num2 / 2f) : 0f);
		for (int i = 0; i < menuItems.Length; i++)
		{
			float x2 = Mathf.Round(num * Mathf.Cos(num3));
			float y2 = Mathf.Round(num * Mathf.Sin(num3));
			menuItems[i].DOAnchorPos(new Vector2(x2, y2), UIAnimationManager.speedMedium);
			num3 += num2;
		}
		rectExplainer.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.InBounce);
		uiOverlay.image.raycastTarget = true;
	}

	private void animateMenuIn()
	{
		rectExplainer.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.OutExpo).OnComplete(delegate
		{
			for (int i = 0; i < menuItems.Length; i++)
			{
				menuItems[i].DOAnchorPos(Vector2.zero, UIAnimationManager.speedMedium);
			}
		});
		uiOverlay.image.raycastTarget = false;
	}

	private void fadeOutMats()
	{
		LevelBuilder.instance.gameBackground.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		}
		Dictionary<WorldPos2D, LevelBackground>.Enumerator enumerator2 = GameplayBuilder.instance.backgrounds.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.Value.renderer.material.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
		}
		blockMaterial.DOColor(colorDark, "_Color", UIAnimationManager.speedMedium);
	}

	private void fadeInMats()
	{
		LevelBuilder.instance.gameBackground.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		Stack<LevelBackground>.Enumerator enumerator = LevelBackgroundPool.instance.pool.GetEnumerator();
		while (enumerator.MoveNext())
		{
			enumerator.Current.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		}
		Dictionary<WorldPos2D, LevelBackground>.Enumerator enumerator2 = GameplayBuilder.instance.backgrounds.GetEnumerator();
		while (enumerator2.MoveNext())
		{
			enumerator2.Current.Value.renderer.material.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
		}
		blockMaterial.DOColor(Color.white, "_Color", UIAnimationManager.speedMedium);
	}
}
