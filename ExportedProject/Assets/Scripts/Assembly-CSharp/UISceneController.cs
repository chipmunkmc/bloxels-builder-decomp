using UnityEngine;
using UnityEngine.UI;

public class UISceneController : MonoBehaviour
{
	public Button uiButton;

	private void Start()
	{
		uiButton.onClick.AddListener(DoThing);
	}

	private void DoThing()
	{
		UIOverlayCanvas.instance.Popup(Resources.Load("Prefabs/UIPopupBoardActivation"));
	}
}
