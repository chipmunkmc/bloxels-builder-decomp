using DG.Tweening;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

public abstract class UIScrollTipController : MonoBehaviour
{
	[Header("UI")]
	public EnhancedScroller enhancedScroller;

	public CanvasGroup uiCanvasGroup;

	public RectTransform selfRect;

	public UICaret[] carets;

	private Vector2[] caretPositions;

	public bool isVisible;

	public void Awake()
	{
		carets = GetComponentsInChildren<UICaret>();
		enhancedScroller = GetComponentInChildren<EnhancedScroller>();
		caretPositions = new Vector2[carets.Length];
	}

	public void Start()
	{
		for (int i = 0; i < carets.Length; i++)
		{
			caretPositions[i] = carets[i].rect.anchoredPosition;
		}
	}

	public abstract void Init();

	public virtual Tweener Show(RectTransform parent, UITipVisual tipVisual)
	{
		isVisible = true;
		selfRect.SetParent(parent);
		selfRect.localScale = Vector3.one;
		selfRect.localPosition = Vector3.zero;
		RePosition(tipVisual);
		return uiCanvasGroup.DOFade(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			uiCanvasGroup.blocksRaycasts = true;
			uiCanvasGroup.interactable = true;
		});
	}

	public virtual void MoveCaret(CaretDirection directionalCaret, Vector2 position)
	{
		for (int i = 0; i < carets.Length; i++)
		{
			if (carets[i].caretDirection == directionalCaret)
			{
				carets[i].rect.anchoredPosition = position;
				break;
			}
		}
	}

	public virtual void MoveCaretY(CaretDirection directionalCaret, float yPos)
	{
		for (int i = 0; i < carets.Length; i++)
		{
			if (carets[i].caretDirection == directionalCaret)
			{
				carets[i].rect.anchoredPosition = new Vector2(carets[i].rect.anchoredPosition.x, yPos);
				break;
			}
		}
	}

	public virtual void MoveCaretX(CaretDirection directionalCaret, float xPos)
	{
		for (int i = 0; i < carets.Length; i++)
		{
			if (carets[i].caretDirection == directionalCaret)
			{
				carets[i].rect.anchoredPosition = new Vector2(xPos, carets[i].rect.anchoredPosition.y);
				break;
			}
		}
	}

	public virtual Tweener Hide()
	{
		isVisible = false;
		return uiCanvasGroup.DOFade(0f, UIAnimationManager.speedFast).OnStart(delegate
		{
			uiCanvasGroup.blocksRaycasts = false;
			uiCanvasGroup.interactable = false;
		}).OnComplete(delegate
		{
			selfRect.SetParent(ScrollPickerManager.instance.transform);
			for (int i = 0; i < carets.Length; i++)
			{
				carets[i].rect.anchoredPosition = caretPositions[i];
				carets[i].Disable();
			}
		});
	}

	public void HideImmediate()
	{
		selfRect.SetParent(ScrollPickerManager.instance.transform);
		uiCanvasGroup.alpha = 0f;
		uiCanvasGroup.blocksRaycasts = false;
		uiCanvasGroup.interactable = false;
		for (int i = 0; i < carets.Length; i++)
		{
			carets[i].rect.anchoredPosition = caretPositions[i];
			carets[i].Disable();
		}
	}

	private void EnableCaretFromDirection(CaretDirection dir)
	{
		for (int i = 0; i < carets.Length; i++)
		{
			if (carets[i].caretDirection != dir)
			{
				carets[i].Disable();
			}
			else
			{
				carets[i].Enable();
			}
			if (carets[i].caretDirection == CaretDirection.None)
			{
				carets[i].Disable();
			}
		}
	}

	private void RePosition(UITipVisual visual)
	{
		Vector2[] anchorsFromDirection = PPUtilities.GetAnchorsFromDirection(visual.rectPosition.dir);
		selfRect.anchorMin = anchorsFromDirection[0];
		selfRect.anchorMax = anchorsFromDirection[1];
		selfRect.pivot = visual.rectPosition.pivot;
		selfRect.anchoredPosition = visual.rectPosition.pos;
		selfRect.sizeDelta = visual.size;
		EnableCaretFromDirection(visual.caretDirection);
	}
}
