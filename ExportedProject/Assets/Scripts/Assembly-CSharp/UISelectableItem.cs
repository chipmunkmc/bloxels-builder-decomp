using System;
using System.Threading;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISelectableItem : UIInteractable
{
	public new delegate void HandleSelect(UISelectableItem item);

	public Dropdown.OptionData itemData;

	public TextMeshProUGUI tmProText;

	public RectTransform background;

	public bool isActive;

	public new event HandleSelect OnSelect;

	private new void Awake()
	{
		base.Awake();
		background.localScale = Vector3.zero;
	}

	private void Start()
	{
		if (base.transform.GetSiblingIndex() == 0)
		{
			Activate();
		}
		tmProText.text = itemData.text;
	}

	public void Toggle()
	{
		if (isActive)
		{
			DeActivate();
		}
		else
		{
			Activate();
		}
	}

	public void Activate()
	{
		if (!isActive)
		{
			isActive = true;
			background.localScale = Vector3.one;
		}
	}

	public void DeActivate()
	{
		if (isActive)
		{
			isActive = false;
			background.localScale = Vector3.zero;
		}
	}

	public override void OnPointerClick(PointerEventData eventData)
	{
		if (this.OnSelect != null)
		{
			this.OnSelect(this);
		}
	}
}
