using System;
using System.Collections;
using System.Threading;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISimpleSquare : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleTextureUpdate(Texture2D tex);

	[Header("| ========= Data ========= |")]
	public ServerSquare dataModel;

	[Header("| ========= UI ========= |")]
	public RectTransform rect;

	public RawImage uiRawImageBackground;

	public RawImage uiRawImageForeground;

	public Image uiImageBorder;

	private Texture2D _texture;

	public Texture2D texture
	{
		get
		{
			return _texture;
		}
		private set
		{
			_texture = value;
			uiRawImageForeground.texture = _texture;
			uiRawImageForeground.texture.filterMode = FilterMode.Point;
			if (this.OnTextureUpdate != null)
			{
				this.OnTextureUpdate(_texture);
			}
		}
	}

	public event HandleTextureUpdate OnTextureUpdate;

	private void Awake()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
		uiRawImageForeground.texture = AssetManager.instance.boardSolidBlank;
		uiRawImageForeground.texture.filterMode = FilterMode.Point;
	}

	public void Init(ServerSquare serverSquare)
	{
		dataModel = serverSquare;
		uiImageBorder.color = BloxelProject.GetColorFromType(serverSquare.type);
		StartCoroutine(DownloadTexture(dataModel.texture));
	}

	private IEnumerator DownloadTexture(string texurl)
	{
		WWW www = new WWW(texurl);
		yield return www;
		texture = www.texture;
	}

	public void OnPointerClick(PointerEventData eData)
	{
		SoundManager.PlayOneShot(SoundManager.instance.flip);
		UIUserProfile.instance.Hide();
		if (CanvasTileInfo.instance.isVisible)
		{
			CanvasTileInfo.instance.Hide(true);
		}
		StartCoroutine(DelayedWarp());
	}

	private IEnumerator DelayedWarp()
	{
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		UIWarpDrive.instance.ManualWarp(dataModel.coordinate.x, dataModel.coordinate.y);
	}
}
