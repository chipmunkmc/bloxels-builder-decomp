using UnityEngine;
using UnityEngine.UI;

public class UISliderPanel : MonoBehaviour
{
	public int currentIndex;

	public RectTransform rect;

	public ScrollRect scrollRect;

	public ScrollSnap scrollSnap;

	public GridLayoutGroup gridLayout;

	public Image[] indicators;

	public Button uiButtonPrev;

	public Button uiButtonNext;

	private void OnEnable()
	{
		uiButtonNext.onClick.AddListener(Next);
		uiButtonPrev.onClick.AddListener(Prev);
		scrollSnap.OnSnap += ScrollSnap_OnSnap;
	}

	private void OnDisable()
	{
		uiButtonNext.onClick.RemoveListener(Next);
		uiButtonPrev.onClick.RemoveListener(Prev);
		scrollSnap.OnSnap -= ScrollSnap_OnSnap;
	}

	private void ScrollSnap_OnSnap(float position)
	{
		if (position == 0f)
		{
			currentIndex = 0;
		}
		else if (position < 0.66f)
		{
			currentIndex = 1;
		}
		else if (position < 1f)
		{
			currentIndex = 2;
		}
		else
		{
			currentIndex = 3;
		}
		SwitchIndicator(currentIndex);
	}

	private void SwitchIndicator(int idx)
	{
		for (int i = 0; i < indicators.Length; i++)
		{
			if (idx == i)
			{
				indicators[i].enabled = true;
			}
			else
			{
				indicators[i].enabled = false;
			}
		}
	}

	private void Next()
	{
		if (currentIndex != 3)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
			currentIndex++;
			scrollRect.horizontalNormalizedPosition = (float)((double)currentIndex * 0.3333) / 1f;
			scrollSnap.DragEnd();
		}
	}

	private void Prev()
	{
		if (currentIndex != 0)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
			currentIndex--;
			scrollRect.horizontalNormalizedPosition = (float)((double)currentIndex * 0.3333) / 1f;
			scrollSnap.DragEnd();
		}
	}
}
