using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UISquareFilterToggle : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public FilterableSquares controller;

	public ProjectType type;

	public Text uiTextCount;

	public Image image;

	public Sprite activeSprite;

	public Sprite inActiveSprite;

	public Color activeColor;

	public Color inActiveColor;

	public RectTransform icon;

	public Vector2 iconActivePos;

	public Vector2 iconInactivePos;

	public bool isActive;

	public bool interactable = true;

	private int _count;

	public int count
	{
		get
		{
			return _count;
		}
		set
		{
			_count = value;
			uiTextCount.text = _count.ToString();
		}
	}

	private void Awake()
	{
		uiTextCount = GetComponentInChildren<Text>();
	}

	public bool Toggle()
	{
		if (isActive)
		{
			DeActivate();
			controller.RemoveFilter(type);
		}
		else
		{
			Activate();
			controller.AddFilter(type);
		}
		return isActive;
	}

	public void Activate()
	{
		if (!isActive)
		{
			isActive = true;
			image.sprite = activeSprite;
			image.color = activeColor;
			icon.anchoredPosition = iconActivePos;
		}
	}

	public void DeActivate()
	{
		if (isActive)
		{
			isActive = false;
			image.sprite = inActiveSprite;
			image.color = inActiveColor;
			icon.anchoredPosition = iconInactivePos;
		}
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (interactable)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
			Toggle();
		}
	}
}
