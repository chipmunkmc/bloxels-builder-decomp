using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class UISquareStats : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public ServerSquare dataModel;

	[Header("| ========= UI ========= |")]
	public RectTransform rect;

	public RawImage uiRawImage;

	public Image uiSquareColor;

	public Button uiButtonSquare;

	public CanvasGroup playsGroup;

	public CanvasGroup downloadsGroup;

	public Text uiTextLikeCount;

	public Text uiTextDownloadCount;

	public Text uiTextViewCount;

	public Text uiTextEarningsCount;

	public Text uiTextPlayCount;

	public Text uiTextCoordinate;

	private void Awake()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
		uiRawImage.texture = AssetManager.instance.boardSolidBlank;
		uiRawImage.texture.filterMode = FilterMode.Point;
	}

	private void Start()
	{
		uiButtonSquare.onClick.AddListener(SquareButtonPressed);
	}

	public void Init(UISimpleSquare square)
	{
		dataModel = square.dataModel;
		uiSquareColor.color = BloxelProject.GetColorFromType(dataModel.type);
		square.OnTextureUpdate += TextureUpdate;
		uiTextLikeCount.text = dataModel.likes.Length.ToString();
		uiTextDownloadCount.text = dataModel.downloads.Length.ToString();
		uiTextViewCount.text = dataModel.views.ToString();
		uiTextEarningsCount.text = dataModel.earnings.ToString();
		uiTextCoordinate.text = dataModel.coordinate.x + "," + dataModel.coordinate.y;
		uiTextPlayCount.text = dataModel.associatedData.game.plays.ToString();
		if (dataModel.type == ProjectType.Game)
		{
			playsGroup.alpha = 1f;
			playsGroup.interactable = true;
			playsGroup.blocksRaycasts = true;
			downloadsGroup.alpha = 0.1f;
			downloadsGroup.interactable = false;
			downloadsGroup.blocksRaycasts = false;
		}
		else
		{
			playsGroup.alpha = 0.1f;
			playsGroup.interactable = false;
			playsGroup.blocksRaycasts = false;
			downloadsGroup.alpha = 1f;
			downloadsGroup.interactable = true;
			downloadsGroup.blocksRaycasts = true;
		}
	}

	private void TextureUpdate(Texture2D tex)
	{
		uiRawImage.texture = tex;
		uiRawImage.texture.filterMode = FilterMode.Point;
	}

	public void SquareButtonPressed()
	{
		StartCoroutine(DelayedWarp());
	}

	private IEnumerator DelayedWarp()
	{
		UIUserProfile.instance.Hide();
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		CanvasTileInfo.instance.Hide(true);
		yield return new WaitForSeconds(UIAnimationManager.speedMedium);
		UIWarpDrive.instance.ManualWarp(dataModel.coordinate.x, dataModel.coordinate.y);
	}
}
