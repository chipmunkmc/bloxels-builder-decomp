using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIStatBar : MonoBehaviour
{
	[Header("UI")]
	public RectTransform selfRect;

	public RectTransform rectLabel;

	public RectTransform rectFillContainer;

	public RectTransform rectFill;

	public RectTransform rectCount;

	public RectTransform uiNotchGrid;

	protected Image _uiImageRectFillContainer;

	protected Image _uiImageFill;

	protected TextMeshProUGUI _uiTextLabel;

	protected TextMeshProUGUI _uiTextCount;

	protected Color _fillColor;

	protected float _maxFillWidth;

	protected int _maxFillCount;

	public static Object notchPrefab;

	public void Awake()
	{
		_uiImageRectFillContainer = rectFillContainer.GetComponent<Image>();
		_uiImageFill = rectFill.GetComponent<Image>();
		_uiTextLabel = rectLabel.GetComponent<TextMeshProUGUI>();
		_uiTextCount = rectCount.GetComponent<TextMeshProUGUI>();
		_maxFillWidth = rectFillContainer.sizeDelta.x;
		ResetUI();
		if (notchPrefab == null)
		{
			notchPrefab = Resources.Load("Prefabs/UIStatNotch");
		}
	}

	public virtual void ResetUI()
	{
		rectFill.localScale = new Vector3(0f, 1f, 1f);
		rectCount.anchoredPosition = new Vector2(0f, rectCount.anchoredPosition.y);
		_uiTextCount.SetText("0");
	}

	public virtual void SetData(int count)
	{
		float num = (float)count / (float)_maxFillCount;
		rectFill.localScale = new Vector3(num, 1f, 1f);
		rectCount.anchoredPosition = new Vector2(num * _maxFillWidth, rectCount.anchoredPosition.y);
		_uiTextCount.SetText(count.ToString());
	}

	public virtual void SetData(float percent)
	{
		rectFill.DOScaleX(percent, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo);
		rectCount.DOAnchorPos(new Vector2(percent * _maxFillWidth - rectCount.sizeDelta.x / 2f, rectCount.anchoredPosition.y), UIAnimationManager.speedMedium).SetEase(Ease.OutExpo);
		_uiTextCount.SetText(Mathf.FloorToInt(percent * 100f) + "%");
	}
}
