using UnityEngine;
using UnityEngine.UI;

public class UIStatBarNotch : MonoBehaviour
{
	private Image _uiImage;

	public RectTransform rectTransform;

	public void SetPosition(float percentage, Vector2 containerSize)
	{
		rectTransform.anchoredPosition = new Vector2(percentage * containerSize.x - rectTransform.sizeDelta.x / 2f, 0f);
		rectTransform.sizeDelta = new Vector2(rectTransform.sizeDelta.x, containerSize.y);
	}
}
