using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIStatRoomInventory : MonoBehaviour
{
	[Header("Data")]
	public BloxelGame bloxelGame;

	public int available;

	public int used;

	public int max = 169;

	[Header("UI")]
	public RectTransform selfRect;

	public RectTransform rectTitle;

	public RectTransform rectFillContainer;

	public RectTransform rectFillAvailable;

	public RectTransform rectFillUsed;

	public RectTransform rectCountTotal;

	public RectTransform rectCountUsed;

	public RectTransform rectCountAvailable;

	private Image _uiImageRectFillContainer;

	private Image _uiImageFillAvailable;

	private Image _uiImageFillUsed;

	private TextMeshProUGUI _uiTextTitle;

	private TextMeshProUGUI _uiTextCountUsed;

	private TextMeshProUGUI _uiTextCountAvailable;

	public Color fillColorAvailable;

	public Color fillColorUsed;

	public float offsetTextY = 10f;

	private float _maxFillHeight;

	public bool isVisible;

	private void Awake()
	{
		_uiImageRectFillContainer = rectFillContainer.GetComponent<Image>();
		_uiImageFillAvailable = rectFillAvailable.GetComponent<Image>();
		_uiImageFillUsed = rectFillUsed.GetComponent<Image>();
		_uiTextTitle = rectTitle.GetComponent<TextMeshProUGUI>();
		_uiTextCountUsed = rectCountUsed.GetComponent<TextMeshProUGUI>();
		_uiTextCountAvailable = rectCountAvailable.GetComponent<TextMeshProUGUI>();
		_uiImageFillAvailable.color = fillColorAvailable;
		_uiImageFillUsed.color = fillColorUsed;
		_maxFillHeight = rectFillContainer.sizeDelta.y;
		ResetUI();
		selfRect.localScale = Vector3.zero;
		isVisible = false;
	}

	public void ResetUI()
	{
		rectFillAvailable.localScale = new Vector3(1f, 0f, 1f);
		rectFillUsed.localScale = new Vector3(1f, 0f, 1f);
		rectCountAvailable.anchoredPosition = new Vector2(-90f, 0f);
		rectCountUsed.anchoredPosition = new Vector2(90f, 0f);
		_uiTextCountAvailable.SetText("0");
		_uiTextCountUsed.SetText("0");
	}

	public void IncrementAvailable(int count = 1)
	{
		available += count;
		SetData(available, used);
	}

	public void InitFromGame(BloxelGame _game)
	{
		bloxelGame = _game;
		if (CurrentUser.instance.active && CurrentUser.instance.account.eduAccount > UserAccount.EduStatus.None)
		{
			SetData(max, bloxelGame.levels.Count);
		}
		else
		{
			SetData(GemManager.roomsUnlocked, bloxelGame.levels.Count);
		}
	}

	public void SetData(int _available, int _used)
	{
		available = _available;
		used = _used;
		if (available >= max - 10)
		{
			rectTitle.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
			rectCountTotal.DOScale(0f, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
		}
		else
		{
			rectTitle.DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
			rectCountTotal.DOScale(1f, UIAnimationManager.speedFast).SetEase(Ease.InExpo);
		}
		float num = (float)available / (float)max;
		rectFillAvailable.localScale = new Vector3(1f, num, 1f);
		rectCountAvailable.anchoredPosition = new Vector2(-90f, num * _maxFillHeight + offsetTextY);
		_uiTextCountAvailable.SetText(available.ToString());
		float num2 = (float)used / (float)max;
		rectFillUsed.localScale = new Vector3(1f, num2, 1f);
		rectCountUsed.anchoredPosition = new Vector2(90f, num2 * _maxFillHeight + offsetTextY);
		_uiTextCountUsed.SetText(used.ToString());
	}

	public Tweener Show()
	{
		return selfRect.DOScale(1f, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnStart(delegate
		{
			isVisible = true;
		});
	}

	public Tweener Hide()
	{
		return selfRect.DOScale(0f, UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnStart(delegate
		{
			isVisible = false;
		});
	}
}
