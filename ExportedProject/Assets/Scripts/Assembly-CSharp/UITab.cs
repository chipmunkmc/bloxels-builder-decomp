using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITab : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public UITabGroup groupParent;

	public int id;

	public CanvasMode[] availableModes;

	public bool isActive;

	private Text textField;

	public Image icon;

	private Image background;

	private Color activeColor;

	public RectTransform rect;

	public Color inActiveColor;

	public Vector2 activePos;

	public bool interactable;

	private CanvasGroup buttonGroup;

	private void Awake()
	{
		background = GetComponent<Image>();
		textField = GetComponentInChildren<Text>();
		buttonGroup = GetComponentInChildren<CanvasGroup>();
		activeColor = background.color;
		activePos = new Vector2(rect.anchoredPosition.x, (float)id * rect.sizeDelta.y * -1f);
	}

	public void Activate()
	{
		interactable = true;
		isActive = true;
		textField.DOFade(1f, UIAnimationManager.speedFast);
		icon.DOFade(1f, UIAnimationManager.speedFast);
		background.DOColor(activeColor, UIAnimationManager.speedMedium);
		Color color = new Color(activeColor.r + 0.1f, activeColor.g + 0.1f, activeColor.b + 0.1f);
		ShortcutExtensions46.DOColor(endValue: new Color(activeColor.r + 0.25f, activeColor.g + 0.25f, activeColor.b + 0.25f), target: BloxelLibrary.instance.scrollRect.GetComponent<Image>(), duration: UIAnimationManager.speedMedium);
		BloxelLibrary.instance.scrollRectRectTransform.DOAnchorPos(new Vector2(BloxelLibrary.instance.scrollRectRectTransform.anchoredPosition.x, activePos.y + (0f - (rect.sizeDelta.y + UITagSearch.instance.rect.sizeDelta.y))), UIAnimationManager.speedMedium);
	}

	public void MakeAvailable()
	{
		interactable = true;
		background.DOColor(activeColor, UIAnimationManager.speedMedium);
		textField.DOFade(1f, UIAnimationManager.speedFast);
		icon.DOFade(1f, UIAnimationManager.speedFast);
	}

	public void CheckAvailability(CanvasMode mode)
	{
		for (int i = 0; i < availableModes.Length; i++)
		{
			if (availableModes[i] == mode)
			{
				MakeAvailable();
				return;
			}
		}
		DeActivate();
	}

	public void DeActivate()
	{
		interactable = false;
		isActive = false;
		textField.DOFade(0.2f, UIAnimationManager.speedFast);
		icon.DOFade(0.2f, UIAnimationManager.speedFast);
		background.DOColor(inActiveColor, UIAnimationManager.speedMedium);
	}

	public void MoveUp(bool shouldActivate)
	{
		if (shouldActivate)
		{
			Activate();
		}
		rect.DOAnchorPos(activePos, UIAnimationManager.speedMedium);
	}

	public void MoveDown(bool shouldActivate)
	{
		if (shouldActivate)
		{
			Activate();
		}
		float y = (BloxelLibrary.instance.rect.sizeDelta.y - groupParent.rect.sizeDelta.y + rect.sizeDelta.y * (float)id - UITagSearch.instance.rect.sizeDelta.y) * -1f;
		Vector2 endValue = new Vector2(rect.anchoredPosition.x, y);
		rect.DOAnchorPos(endValue, UIAnimationManager.speedMedium);
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (interactable && BloxelLibrary.instance.currentTabID != id)
		{
			BloxelLibrary.instance.SwitchTabWindows(id);
		}
		else if (interactable && BloxelLibrary.instance.currentTabID == id)
		{
			BloxelLibrary.instance.ScrollToTop();
		}
	}
}
