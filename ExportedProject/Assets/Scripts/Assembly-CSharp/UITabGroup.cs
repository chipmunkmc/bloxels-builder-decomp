using UnityEngine;

public class UITabGroup : MonoBehaviour
{
	public UITab[] tabsInGroup;

	public RectTransform rect;

	private void Awake()
	{
		tabsInGroup = GetComponentsInChildren<UITab>();
	}

	public void SwitchTab(int id)
	{
		UITab[] array = tabsInGroup;
		foreach (UITab uITab in array)
		{
			bool shouldActivate = false;
			if (uITab.id == id)
			{
				shouldActivate = true;
			}
			if (uITab.id <= id)
			{
				uITab.MoveUp(shouldActivate);
			}
			else if (uITab.id >= id)
			{
				uITab.MoveDown(shouldActivate);
			}
		}
	}
}
