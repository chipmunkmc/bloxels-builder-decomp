using UnityEngine;

public class UITabWindow : MonoBehaviour
{
	public int id;

	public BloxelLibraryWindow library;

	public void Activate()
	{
		base.gameObject.SetActive(true);
		library.UpdateScrollerContents();
	}

	public void Disable()
	{
		base.gameObject.SetActive(false);
	}
}
