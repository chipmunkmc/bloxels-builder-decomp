using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITag : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleTagClick(UITag tag);

	public delegate void HandleTagRemove(UITag tag);

	public string tagText;

	public Image background;

	public Button uiButtonDelete;

	public Image uiImageAdd;

	public Text uiText;

	public bool generatedByUser;

	public event HandleTagClick OnClick;

	public event HandleTagRemove OnRemove;

	private void Awake()
	{
		if (background == null)
		{
			background = GetComponent<Image>();
		}
		if (uiButtonDelete == null)
		{
			uiButtonDelete = GetComponentInChildren<Button>();
		}
		background.DOFade(0f, 0f);
		uiButtonDelete.onClick.AddListener(DeleteTag);
	}

	private void Start()
	{
		uiText.text = tagText;
	}

	private void OnEnable()
	{
		background.DOFade(1f, UIAnimationManager.speedMedium);
	}

	public void ShowAdd()
	{
		uiButtonDelete.gameObject.SetActive(false);
		uiImageAdd.gameObject.SetActive(true);
	}

	public void ShowDelete()
	{
		uiButtonDelete.gameObject.SetActive(true);
		uiImageAdd.gameObject.SetActive(false);
	}

	private void DeleteTag()
	{
		if (this.OnRemove != null)
		{
			this.OnRemove(this);
		}
	}

	public void DestroyMe()
	{
		background.DOFade(0f, UIAnimationManager.speedMedium);
		UnityEngine.Object.Destroy(base.gameObject, UIAnimationManager.speedMedium);
	}

	public void OnPointerClick(PointerEventData eData)
	{
		if (this.OnClick != null)
		{
			this.OnClick(this);
		}
	}
}
