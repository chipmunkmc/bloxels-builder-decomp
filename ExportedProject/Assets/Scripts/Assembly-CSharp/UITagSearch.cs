using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UITagSearch : MonoBehaviour
{
	public static UITagSearch instance;

	public Image background;

	public Button uiButtonClear;

	public Button uiButtonSearch;

	public Text uiTextHeading;

	public RectTransform rect;

	public Object tagSearchPrefab;

	[Header("Data")]
	public List<string> activeTags;

	private bool _hasFilter;

	public bool hasFilter
	{
		get
		{
			return _hasFilter;
		}
		set
		{
			_hasFilter = value;
			if (_hasFilter)
			{
				uiButtonClear.gameObject.SetActive(true);
				return;
			}
			uiButtonClear.gameObject.SetActive(false);
			hasBoardFilter = false;
			hasAnimationFilter = false;
			hasCharacterFilter = false;
			hasMegaBoardFilter = false;
			hasGameFilter = false;
		}
	}

	public bool hasBoardFilter { get; private set; }

	public bool hasAnimationFilter { get; private set; }

	public bool hasMegaBoardFilter { get; private set; }

	public bool hasCharacterFilter { get; private set; }

	public bool hasGameFilter { get; private set; }

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
		if (background == null)
		{
			background = GetComponent<Image>();
		}
		if (tagSearchPrefab == null)
		{
			tagSearchPrefab = Resources.Load("Prefabs/UIPopupTagSearch");
		}
		activeTags = new List<string>();
	}

	private void Start()
	{
		uiButtonSearch.onClick.AddListener(ShowTagSearchMenu);
		uiButtonClear.onClick.AddListener(ClearResults);
		hasFilter = false;
	}

	public void MoveY(float y)
	{
		rect.DOAnchorPos(new Vector2(rect.anchoredPosition.x, y), UIAnimationManager.speedMedium);
	}

	public void ShowTagSearchMenu()
	{
		UIOverlayCanvas.instance.Popup(tagSearchPrefab);
	}

	public void ClearResults()
	{
		SoundManager.PlaySoundClip(SoundClip.popC);
		activeTags.Clear();
		hasFilter = false;
		BloxelLibraryScrollController.instance.ClearAllFilters();
		switch (BloxelLibrary.instance.currentTabID)
		{
		case 0:
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Game);
			break;
		case 1:
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Animation);
			break;
		case 2:
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.MegaBoard);
			break;
		case 3:
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Character);
			break;
		case 4:
			BloxelLibraryScrollController.instance.SetCurrentDataForProjectType(ProjectType.Board);
			break;
		}
	}

	public void FilterResults()
	{
		BloxelLibrary.instance.scrollRect.verticalNormalizedPosition = 1f;
		if (activeTags.Count == 0)
		{
			ClearResults();
			return;
		}
		List<string> list = new List<string>(activeTags.Count);
		for (int i = 0; i < activeTags.Count; i++)
		{
			list.Add(activeTags[i].ToLower());
		}
		hasFilter = true;
		switch (BloxelLibrary.instance.currentTabID)
		{
		case 0:
			FilterGames(list);
			break;
		case 1:
			FilterAnimations(list);
			break;
		case 2:
			FilterBackgrounds(list);
			break;
		case 3:
			FilterCharacters(list);
			break;
		case 4:
			FilterBoards(list);
			break;
		}
	}

	private void FilterBoards(List<string> _vals)
	{
		hasBoardFilter = true;
		BloxelLibraryScrollController.instance.filteredBoardData.Clear();
		List<BloxelBoard> unfilteredBoardData = BloxelLibraryScrollController.instance.unfilteredBoardData;
		for (int i = 0; i < unfilteredBoardData.Count; i++)
		{
			bool flag = true;
			BloxelBoard bloxelBoard = unfilteredBoardData[i];
			for (int j = 0; j < _vals.Count; j++)
			{
				if (!bloxelBoard.tags.Contains(_vals[j]))
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				BloxelLibraryScrollController.instance.filteredBoardData.Add(bloxelBoard);
			}
		}
		BloxelBoardLibrary.instance.UpdateScrollerContents();
	}

	private void FilterAnimations(List<string> _vals)
	{
		hasAnimationFilter = true;
		BloxelLibraryScrollController.instance.filteredAnimationData.Clear();
		List<BloxelAnimation> unfilteredAnimationData = BloxelLibraryScrollController.instance.unfilteredAnimationData;
		for (int i = 0; i < unfilteredAnimationData.Count; i++)
		{
			bool flag = true;
			BloxelAnimation bloxelAnimation = unfilteredAnimationData[i];
			for (int j = 0; j < _vals.Count; j++)
			{
				if (!bloxelAnimation.tags.Contains(_vals[j]))
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				BloxelLibraryScrollController.instance.filteredAnimationData.Add(bloxelAnimation);
			}
		}
		BloxelAnimationLibrary.instance.UpdateScrollerContents();
	}

	private void FilterBackgrounds(List<string> _vals)
	{
		hasMegaBoardFilter = true;
		BloxelLibraryScrollController.instance.filteredMegaBoardData.Clear();
		List<BloxelMegaBoard> unfilteredMegaBoardData = BloxelLibraryScrollController.instance.unfilteredMegaBoardData;
		for (int i = 0; i < unfilteredMegaBoardData.Count; i++)
		{
			bool flag = true;
			BloxelMegaBoard bloxelMegaBoard = unfilteredMegaBoardData[i];
			for (int j = 0; j < _vals.Count; j++)
			{
				if (!bloxelMegaBoard.tags.Contains(_vals[j]))
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				BloxelLibraryScrollController.instance.filteredMegaBoardData.Add(bloxelMegaBoard);
			}
		}
		BloxelMegaBoardLibrary.instance.UpdateScrollerContents();
	}

	private void FilterCharacters(List<string> _vals)
	{
		hasCharacterFilter = true;
		BloxelLibraryScrollController.instance.filteredCharacterData.Clear();
		List<BloxelCharacter> unfilteredCharacterData = BloxelLibraryScrollController.instance.unfilteredCharacterData;
		for (int i = 0; i < unfilteredCharacterData.Count; i++)
		{
			bool flag = true;
			BloxelCharacter bloxelCharacter = unfilteredCharacterData[i];
			for (int j = 0; j < _vals.Count; j++)
			{
				if (!bloxelCharacter.tags.Contains(_vals[j]))
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				BloxelLibraryScrollController.instance.filteredCharacterData.Add(bloxelCharacter);
			}
		}
		BloxelCharacterLibrary.instance.UpdateScrollerContents();
	}

	private void FilterGames(List<string> _vals)
	{
		hasGameFilter = true;
		BloxelLibraryScrollController.instance.filteredGameData.Clear();
		List<BloxelGame> unfilteredGameData = BloxelLibraryScrollController.instance.unfilteredGameData;
		for (int i = 0; i < unfilteredGameData.Count; i++)
		{
			bool flag = true;
			BloxelGame bloxelGame = unfilteredGameData[i];
			for (int j = 0; j < _vals.Count; j++)
			{
				if (!bloxelGame.tags.Contains(_vals[j]))
				{
					flag = false;
					break;
				}
			}
			if (flag)
			{
				BloxelLibraryScrollController.instance.filteredGameData.Add(bloxelGame);
			}
		}
		BloxelGameLibrary.instance.UpdateScrollerContents();
	}
}
