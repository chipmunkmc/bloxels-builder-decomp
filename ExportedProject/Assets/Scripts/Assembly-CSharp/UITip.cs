using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UITip : MonoBehaviour
{
	public delegate void HandleComplete();

	[Header("| + ========= UI ========= + |")]
	public RectTransform parentRect;

	public RectTransform rectTooltip;

	public RectTransform rectCaret;

	public Image uiImageOverlay;

	public Image uiImageIcon;

	public Text uiTextTitle;

	public Text uiTextBody;

	public Text uiTextStep;

	public Button uiButtonOk;

	public Toggle uiToggleStop;

	public UICaret[] carets;

	[Header("| + ========= Positioning ========= + |")]
	private Vector3 tooltipStartScale;

	private Vector3 tooltipTargetScale;

	public UITipVisual visual;

	public event HandleComplete OnComplete;

	private void Awake()
	{
		uiImageOverlay.color = new Color(0f, 0f, 0f, 0f);
		tooltipStartScale = Vector3.zero;
		tooltipTargetScale = Vector3.one;
		carets = GetComponentsInChildren<UICaret>();
	}

	public void Start()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmB);
		uiImageOverlay.DOFade(0.35f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			rectTooltip.DOScale(tooltipTargetScale, UIAnimationManager.speedMedium);
		});
		uiButtonOk.onClick.AddListener(CheckPressed);
	}

	public void Init(UITipVisual _visual, StepText stepText, Sprite icon, string step)
	{
		visual = _visual;
		Vector2[] anchorsFromDirection = PPUtilities.GetAnchorsFromDirection(visual.rectPosition.dir);
		rectTooltip.anchorMin = anchorsFromDirection[0];
		rectTooltip.anchorMax = anchorsFromDirection[1];
		rectTooltip.pivot = visual.rectPosition.pivot;
		rectTooltip.anchoredPosition = visual.rectPosition.pos;
		uiImageIcon.sprite = icon;
		uiTextTitle.text = stepText.title;
		uiTextBody.text = stepText.body;
		uiTextStep.text = step;
		EnableCaretFromDirection(visual.caretDirection);
	}

	public void Init(UITipVisual _visual, StepText stepText)
	{
		visual = _visual;
		Vector2[] anchorsFromDirection = PPUtilities.GetAnchorsFromDirection(visual.rectPosition.dir);
		rectTooltip.anchorMin = anchorsFromDirection[0];
		rectTooltip.anchorMax = anchorsFromDirection[1];
		rectTooltip.pivot = visual.rectPosition.pivot;
		rectTooltip.anchoredPosition = visual.rectPosition.pos;
		rectTooltip.sizeDelta = visual.size;
		uiTextTitle.text = stepText.title;
		uiTextBody.text = stepText.body;
		EnableCaretFromDirection(visual.caretDirection);
	}

	private void EnableCaretFromDirection(CaretDirection dir)
	{
		for (int i = 0; i < carets.Length; i++)
		{
			if (carets[i].caretDirection != dir)
			{
				carets[i].Disable();
			}
			else
			{
				carets[i].Enable();
			}
			if (carets[i].caretDirection == CaretDirection.None)
			{
				carets[i].Disable();
			}
		}
	}

	private void CheckPressed()
	{
		SoundManager.PlayOneShot(SoundManager.instance.confirmC);
		Dismiss();
	}

	public void Dismiss()
	{
		rectTooltip.DOScale(tooltipStartScale, UIAnimationManager.speedFast).OnComplete(delegate
		{
			uiImageOverlay.DOFade(0f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				PrefsManager.instance.shouldDisableGuide = uiToggleStop.isOn;
				if (this.OnComplete != null)
				{
					this.OnComplete();
				}
				UnityEngine.Object.Destroy(base.gameObject);
			});
		});
	}
}
