using UnityEngine;

public struct UITipVisual
{
	public CaretDirection caretDirection;

	public RectPosition rectPosition;

	public Vector2 size;

	public UITipVisual(RectPosition _rectPos, CaretDirection _caretDir, Vector2 _size)
	{
		rectPosition = _rectPos;
		caretDirection = _caretDir;
		size = _size;
	}
}
