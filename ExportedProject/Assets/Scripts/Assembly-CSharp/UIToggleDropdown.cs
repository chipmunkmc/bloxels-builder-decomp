using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;

public class UIToggleDropdown : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	public delegate void HandleChange(bool _isOpen);

	[Header("UI")]
	public RectTransform rect;

	public Transform transformArrow;

	public Vector2 positionOpen;

	public Vector2 positionClosed;

	public bool isOpen;

	public event HandleChange OnChange;

	public void Awake()
	{
		isOpen = false;
	}

	public Tweener Toggle()
	{
		SoundManager.instance.PlaySound(SoundManager.instance.sweepA);
		if (isOpen)
		{
			return Hide();
		}
		return Show();
	}

	public Tweener Show()
	{
		if (isOpen)
		{
			return null;
		}
		return rect.DOAnchorPos(positionOpen, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnStart(delegate
		{
			isOpen = true;
			Vector3 endValue = new Vector3(0f, 0f, transformArrow.localRotation.z + 180f);
			transformArrow.DORotate(endValue, UIAnimationManager.speedFast, RotateMode.LocalAxisAdd);
			if (this.OnChange != null)
			{
				this.OnChange(isOpen);
			}
		});
	}

	public Tweener Hide()
	{
		if (!isOpen)
		{
			return null;
		}
		return rect.DOAnchorPos(positionClosed, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnStart(delegate
		{
			isOpen = false;
			Vector3 endValue = new Vector3(0f, 0f, transformArrow.localRotation.z + 180f);
			transformArrow.DORotate(endValue, UIAnimationManager.speedFast, RotateMode.LocalAxisAdd);
			if (this.OnChange != null)
			{
				this.OnChange(isOpen);
			}
		});
	}

	public void OnPointerClick(PointerEventData eData)
	{
		Toggle();
	}
}
