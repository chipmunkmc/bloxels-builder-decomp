using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UITooltip : MonoBehaviour
{
	[Tooltip("Could override with UIButton")]
	public Button uiButtonTrigger;

	public UIButton uiButtonTriggerOverride;

	public RectTransform tooltipTransform;

	public TextMeshProUGUI tmpTextTitle;

	public Text uiTextBody;

	public string title;

	public string body;

	public Direction direction;

	public bool isActive;

	public Tweener animation;

	private Vector3 _startingScale;

	private Vector2 _startingPosition;

	private Vector2 _movedPosition;

	public bool shouldAnimate = true;

	public void Awake()
	{
		if (uiButtonTriggerOverride == null)
		{
			uiButtonTrigger.onClick.AddListener(Toggle);
		}
		else
		{
			uiButtonTriggerOverride.OnClick += Toggle;
		}
		ResetUI();
	}

	public void OnDestroy()
	{
		if (uiButtonTriggerOverride == null)
		{
			uiButtonTrigger.onClick.RemoveListener(Toggle);
		}
		else
		{
			uiButtonTriggerOverride.OnClick -= Toggle;
		}
	}

	public virtual void ResetUI()
	{
		if (tmpTextTitle != null && !string.IsNullOrEmpty(title))
		{
			tmpTextTitle.SetText(title);
		}
		if (uiTextBody != null && !string.IsNullOrEmpty(body))
		{
			uiTextBody.text = body;
		}
		_startingPosition = tooltipTransform.anchoredPosition;
		switch (direction)
		{
		case Direction.Left:
			_startingScale = new Vector3(0f, 1f, 1f);
			_movedPosition = _startingPosition + new Vector2(2f, 0f);
			break;
		case Direction.Right:
			_startingScale = new Vector3(0f, 1f, 1f);
			_movedPosition = _startingPosition - new Vector2(2f, 0f);
			break;
		case Direction.Up:
			_startingScale = new Vector3(1f, 0f, 1f);
			_movedPosition = _startingPosition + new Vector2(0f, 2f);
			break;
		case Direction.Down:
			_startingScale = new Vector3(1f, 0f, 1f);
			_movedPosition = _startingPosition - new Vector2(0f, 2f);
			break;
		default:
			_startingScale = new Vector3(1f, 0f, 1f);
			_movedPosition = _startingPosition - new Vector2(0f, 2f);
			break;
		}
		tooltipTransform.localScale = _startingScale;
	}

	public virtual void Toggle()
	{
		if (isActive)
		{
			DeActivate();
		}
		else
		{
			Activate();
		}
	}

	public virtual void Activate()
	{
		if (isActive)
		{
			return;
		}
		tooltipTransform.DOScale(1f, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isActive = true;
		}).OnComplete(delegate
		{
			if (shouldAnimate)
			{
				animation = tooltipTransform.DOAnchorPos(_movedPosition, UIAnimationManager.speedSlow).SetLoops(-1, LoopType.Yoyo);
			}
		});
	}

	public virtual void DeActivate()
	{
		if (isActive)
		{
			if (shouldAnimate)
			{
				animation.Kill(true);
			}
			tooltipTransform.anchoredPosition = _startingPosition;
			tooltipTransform.DOScale(_startingScale, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isActive = false;
			});
		}
	}
}
