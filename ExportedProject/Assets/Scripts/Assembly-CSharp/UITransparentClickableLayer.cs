using UnityEngine;
using UnityEngine.UI;

public class UITransparentClickableLayer : MonoBehaviour
{
	public bool visualize;

	private Image _image;

	private void Awake()
	{
		_image = GetComponent<Image>();
		_image.color = new Color(1f, 1f, 1f, 0f);
	}
}
