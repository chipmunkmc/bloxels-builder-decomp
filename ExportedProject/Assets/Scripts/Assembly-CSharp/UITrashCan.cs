using System;
using System.Threading;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UITrashCan : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IEventSystemHandler
{
	public delegate void HandleOpen();

	public delegate void HandleClose();

	public delegate void HandleVisible();

	public delegate void HandleHidden();

	public delegate void HandleDelete();

	public RectTransform selfRect;

	public Image uiImage;

	public Sprite spriteOpen;

	public Sprite spriteClosed;

	public bool isVisible;

	public bool isOpen;

	public event HandleOpen OnOpen;

	public event HandleOpen OnClose;

	public event HandleVisible OnVisible;

	public event HandleHidden OnHidden;

	public event HandleDelete OnDelete;

	public void Init(bool visible, bool open)
	{
		if (visible)
		{
			Show();
		}
		else
		{
			Hide();
		}
		if (open)
		{
			Open();
		}
		else
		{
			Close();
		}
	}

	public void ToggleVisibility()
	{
		if (isVisible)
		{
			Hide();
		}
		else
		{
			Show();
		}
	}

	public void Show()
	{
		if (BoardZoomManager.instance.is16by10OrWider)
		{
			selfRect.anchoredPosition = new Vector2(138f, 230f);
		}
		isVisible = true;
		selfRect.DOKill();
		UIGameBuilderSecondaryMode.MapInstance.Hide();
		selfRect.DOScale(Vector3.one, UIAnimationManager.speedMedium).SetEase(Ease.OutExpo).OnComplete(delegate
		{
			selfRect.DOShakeScale(UIAnimationManager.speedMedium, 0.5f, 0, 10f);
			if (this.OnVisible != null)
			{
				this.OnVisible();
			}
		});
	}

	public void Hide()
	{
		isVisible = false;
		selfRect.DOKill();
		UIGameBuilderSecondaryMode.MapInstance.Show();
		selfRect.DOScale(Vector3.zero, UIAnimationManager.speedMedium).SetEase(Ease.InExpo).OnComplete(delegate
		{
			if (this.OnHidden != null)
			{
				this.OnHidden();
			}
		});
	}

	public bool TryDelete()
	{
		if (isOpen)
		{
			Delete();
			return true;
		}
		Hide();
		return false;
	}

	public void Delete()
	{
		uiImage.DOColor(Color.red, 0.1f).SetLoops(4, LoopType.Yoyo).OnStart(delegate
		{
			SoundManager.PlayOneShot(SoundManager.instance.trash);
		})
			.OnComplete(delegate
			{
				if (this.OnDelete != null)
				{
					this.OnDelete();
				}
				uiImage.color = Color.white;
				Hide();
			});
	}

	public void ToggleTop()
	{
		if (isOpen)
		{
			Close();
		}
		else
		{
			Open();
		}
	}

	public void Open()
	{
		isOpen = true;
		uiImage.sprite = spriteOpen;
		if (this.OnOpen != null)
		{
			this.OnOpen();
		}
	}

	public void Close()
	{
		isOpen = false;
		uiImage.sprite = spriteClosed;
		if (this.OnClose != null)
		{
			this.OnClose();
		}
	}

	public void OnPointerEnter(PointerEventData eData)
	{
		Open();
	}

	public void OnPointerExit(PointerEventData eData)
	{
		Close();
	}
}
