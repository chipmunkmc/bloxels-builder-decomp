using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITriggerBuyAsset : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	public BloxelProject project;

	public int bloxelsUsed;

	[Header("| ========= UI ========= |")]
	public UIButton uiButton;

	public UIButtonIcon uiIcon;

	public Image colorBG;

	public Image uiButtonColorBG;

	public Image uiButtonOutlineColorBG;

	public Text uiTextCount;

	private Color OrangeOutline = new Color(0.65f, 0.35f, 0.16f, 1f);

	private Color BlueOutline = new Color(0.27f, 0.5f, 0.69f, 1f);

	private Color RedOutline = new Color(0.72f, 0.23f, 0.2f, 1f);

	private Color GreenOutline = new Color(0.13f, 0.61f, 0.17f, 1f);

	public string string_saved = "Saved";

	public string string_moreCoins = "Not Enough Coins";

	public string string_alreadyExists = "Already Exists";

	private void Awake()
	{
		string_saved = LocalizationManager.getInstance().getLocalizedText("iWall103", "Saved");
		string_moreCoins = LocalizationManager.getInstance().getLocalizedText("iWall55", "Not Enough Coins");
		string_alreadyExists = LocalizationManager.getInstance().getLocalizedText("iWall54", "Already Exists");
		if (uiButton == null)
		{
			uiButton = GetComponent<UIButton>();
		}
		if (uiIcon == null)
		{
			uiIcon = GetComponentInChildren<UIButtonIcon>();
		}
		uiButton.OnClick += Activate;
		uiTextCount.text = LocalizationManager.getInstance().getLocalizedText("loadingscreen1", "Loading");
		uiButton.interactable = false;
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		project = _square.project;
		square.OnStateChange += SetReady;
		SwitchType(square.serverSquare.type, true);
		uiTextCount.text = square.currentValue.ToString("N0");
		uiButton.interactable = true;
	}

	public void Init(BloxelProject _project, CanvasSquare _square)
	{
		square = _square;
		project = _project;
		SwitchType(project.type, false);
		int num = ExchangeManager.instance.CalculateValue(bloxelsUsed);
		uiTextCount.text = num.ToString("N0");
		uiButton.interactable = true;
	}

	public void SetReady(CanvasTileInfo.ReadyState state)
	{
		square.OnStateChange -= SetReady;
		switch (state)
		{
		case CanvasTileInfo.ReadyState.Requested:
			break;
		case CanvasTileInfo.ReadyState.Downloading:
			break;
		case CanvasTileInfo.ReadyState.Waiting:
			break;
		case CanvasTileInfo.ReadyState.Ready:
			if (square.project != null)
			{
				uiTextCount.text = square.currentValue.ToString("N0");
				uiButton.interactable = true;
			}
			break;
		case CanvasTileInfo.ReadyState.Failed:
			uiButton.interactable = false;
			break;
		}
	}

	private void SwitchType(ProjectType type, bool delayed)
	{
		switch (type)
		{
		case ProjectType.Board:
			colorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange);
			uiButtonColorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Orange);
			uiButtonOutlineColorBG.color = OrangeOutline;
			if (!delayed)
			{
				bloxelsUsed = ((BloxelBoard)project).GetBloxelCount();
			}
			break;
		case ProjectType.Animation:
			colorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue);
			uiButtonColorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Blue);
			uiButtonOutlineColorBG.color = BlueOutline;
			if (!delayed)
			{
				bloxelsUsed = ((BloxelAnimation)project).GetBloxelCount();
			}
			break;
		case ProjectType.MegaBoard:
			colorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green);
			uiButtonColorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Green);
			uiButtonOutlineColorBG.color = GreenOutline;
			if (!delayed)
			{
				bloxelsUsed = ((BloxelMegaBoard)project).GetBloxelCount();
			}
			break;
		case ProjectType.Character:
			colorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
			uiButtonColorBG.color = ColorUtilities.GetUnityColorFromBlockColor(BlockColor.Red);
			uiButtonOutlineColorBG.color = RedOutline;
			if (!delayed)
			{
				bloxelsUsed = ((BloxelCharacter)project).GetBloxelCount();
			}
			break;
		case ProjectType.Level:
			break;
		}
	}

	public void Activate()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		DownloadAsset(project);
	}

	public void DownloadAsset(BloxelProject _project)
	{
		StopCoroutine("turnOffCloudSyncOverride");
		int num = 0;
		switch (_project.type)
		{
		case ProjectType.Board:
			num = ExchangeManager.instance.CalculateValue(((BloxelBoard)_project).GetBloxelCount());
			break;
		case ProjectType.Animation:
			num = ExchangeManager.instance.CalculateValue(((BloxelAnimation)_project).GetBloxelCount());
			break;
		case ProjectType.MegaBoard:
			num = ExchangeManager.instance.CalculateValue(((BloxelMegaBoard)_project).GetBloxelCount());
			break;
		case ProjectType.Character:
			num = ExchangeManager.instance.CalculateValue(((BloxelCharacter)_project).GetBloxelCount());
			break;
		}
		PlayerBankManager.instance.shouldUpdateServer = false;
		if (CurrentUser.instance.account == null)
		{
			UINavMenu.instance.UserAction();
			return;
		}
		if (num > PlayerBankManager.instance.GetCoinCount())
		{
			uiTextCount.text = string_moreCoins;
			PlayerHUD.instance.InsufficientFunds();
			return;
		}
		bool flag = false;
		CloudManager.Instance.addToQueueOverride = true;
		switch (_project.type)
		{
		case ProjectType.Board:
			if (!AssetManager.instance.boardPool.ContainsKey(_project.ID()))
			{
				BloxelLibrary.instance.libraryToggle.Show();
				BloxelLibrary.instance.SwitchTabWindows(4);
				BloxelBoardLibrary.instance.AddBoard(_project as BloxelBoard);
				((BloxelBoard)_project).SetOwner(square.serverSquare.owner);
				flag = ((BloxelBoard)_project).Save(true, false);
			}
			else
			{
				uiTextCount.text = string_alreadyExists;
			}
			break;
		case ProjectType.Animation:
			if (!AssetManager.instance.animationPool.ContainsKey(_project.ID()))
			{
				BloxelLibrary.instance.libraryToggle.Show();
				BloxelLibrary.instance.SwitchTabWindows(1);
				BloxelAnimationLibrary.instance.AddProject(_project as BloxelAnimation);
				((BloxelAnimation)_project).SetOwner(square.serverSquare.owner);
				flag = ((BloxelAnimation)_project).Save(true, false);
			}
			else
			{
				uiTextCount.text = string_alreadyExists;
			}
			break;
		case ProjectType.MegaBoard:
			if (!AssetManager.instance.megaBoardPool.ContainsKey(_project.ID()))
			{
				BloxelLibrary.instance.libraryToggle.Show();
				BloxelLibrary.instance.SwitchTabWindows(2);
				BloxelMegaBoardLibrary.instance.AddProject(_project as BloxelMegaBoard);
				((BloxelMegaBoard)_project).SetOwner(square.serverSquare.owner);
				Dictionary<GridLocation, BloxelBoard>.Enumerator enumerator = ((BloxelMegaBoard)_project).boards.GetEnumerator();
				while (enumerator.MoveNext())
				{
					BloxelBoard value = enumerator.Current.Value;
					value.tags.Add("PPHide");
				}
				flag = ((BloxelMegaBoard)_project).Save(true, false);
			}
			else
			{
				uiTextCount.text = string_alreadyExists;
			}
			break;
		case ProjectType.Character:
			if (!AssetManager.instance.characterPool.ContainsKey(_project.ID()))
			{
				BloxelLibrary.instance.libraryToggle.Show();
				BloxelLibrary.instance.SwitchTabWindows(3);
				flag = BloxelCharacterLibrary.instance.AddProject(_project as BloxelCharacter, square.serverSquare.owner, false);
			}
			else
			{
				uiTextCount.text = string_alreadyExists;
				flag = false;
			}
			break;
		}
		StartCoroutine("turnOffCloudSyncOverride");
		if (flag)
		{
			uiTextCount.text = string_saved;
			PlayerBankManager.instance.RemoveCoins(num);
			string empty = string.Empty;
			empty = (string.IsNullOrEmpty(_project.title) ? LocalizationManager.getInstance().getLocalizedText("iWall104", "A Single Board") : _project.title);
			BloxelServerInterface.instance.Emit_SendCoins(num, square.serverSquare.owner, CurrentUser.instance.account.serverID, _project.type, empty);
			BloxelServerInterface.instance.Emit_DownloadSquare(CurrentUser.instance.account.serverID, square.serverSquare._id, num);
			PlayerBankManager.instance.shouldUpdateServer = true;
		}
	}

	private IEnumerator turnOffCloudSyncOverride()
	{
		yield return new WaitForSeconds(10f);
		CloudManager.Instance.addToQueueOverride = false;
	}
}
