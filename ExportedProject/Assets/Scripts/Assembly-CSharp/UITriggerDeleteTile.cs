using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UITriggerDeleteTile : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public Button uiButton;

	public UIButtonIcon uiIcon;

	public Object deletePrefab;

	private void Awake()
	{
		uiButton = GetComponent<Button>();
		uiIcon = GetComponentInChildren<UIButtonIcon>();
		uiButton.onClick.AddListener(Activate);
		deletePrefab = Resources.Load("Prefabs/UIPopupDeleteSquare");
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
	}

	public void Activate()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(deletePrefab);
		UIPopupDeleteSquare component = gameObject.GetComponent<UIPopupDeleteSquare>();
		component.Init(square);
	}
}
