using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UITriggerLikeTile : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public Button uiButton;

	public UIButtonIcon uiIcon;

	public Text uiTextCount;

	private void Awake()
	{
		uiButton = GetComponent<Button>();
		uiIcon = GetComponentInChildren<UIButtonIcon>();
		uiButton.onClick.AddListener(Toggle);
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		uiTextCount.text = square.serverSquare.likes.Length.ToString();
		if (CurrentUser.instance.account != null && CurrentUser.instance.HasLikedSquare(square.serverSquare._id))
		{
			uiIcon.color = Color.white;
		}
		else
		{
			uiIcon.color = Color.gray;
		}
	}

	public void Toggle()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalC);
		if (CurrentUser.instance.account == null)
		{
			UINavMenu.instance.UserAction();
			return;
		}
		bool flag = CurrentUser.instance.ToggleSquareLike(square.serverSquare._id);
		int num = int.Parse(uiTextCount.text);
		if (flag)
		{
			uiIcon.color = Color.gray;
			num--;
			if (square.serverSquare.likeList.Contains(CurrentUser.instance.account.serverID))
			{
				square.serverSquare.likeList.Remove(CurrentUser.instance.account.serverID);
			}
		}
		else
		{
			uiIcon.color = Color.white;
			num++;
			if (!square.serverSquare.likeList.Contains(CurrentUser.instance.account.serverID))
			{
				square.serverSquare.likeList.Add(CurrentUser.instance.account.serverID);
			}
		}
		uiTextCount.text = num.ToString();
	}
}
