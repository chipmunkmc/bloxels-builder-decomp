using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UITriggerMap : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public Button uiButton;

	public UIButtonIcon uiIcon;

	public Object mapPrefab;

	private void Awake()
	{
		uiButton = GetComponent<Button>();
		uiIcon = GetComponentInChildren<UIButtonIcon>();
		uiButton.onClick.AddListener(Activate);
		mapPrefab = Resources.Load("Prefabs/UIPopupGameMap");
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
	}

	public void Activate()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(mapPrefab);
		UIPopupGameMap component = gameObject.GetComponent<UIPopupGameMap>();
		component.Init(square.project as BloxelGame);
	}
}
