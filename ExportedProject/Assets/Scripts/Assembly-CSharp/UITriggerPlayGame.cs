using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(UIButton))]
public class UITriggerPlayGame : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public UIButton uiButton;

	public UIButtonIcon uiIcon;

	private void Awake()
	{
		uiButton = GetComponent<UIButton>();
		uiIcon = GetComponentInChildren<UIButtonIcon>();
		uiButton.OnClick += Activate;
		uiButton.interactable = false;
		uiButton.transform.localScale = Vector3.zero;
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		square.OnStateChange += SetReady;
		uiButton.interactable = true;
		uiButton.transform.DOScale(1f, UIAnimationManager.speedFast);
	}

	public void Activate()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		BloxelGame bloxelGame = square.project as BloxelGame;
		BloxelServerInterface.instance.Emit_PlayGame(square.serverSquare.projectID, bloxelGame.title, square.serverSquare.owner, square.serverSquare.coordinate.ToString());
		CanvasTileInfo.instance.StopAllCoroutines();
		PrefsManager.instance.shouldActiveAfterWarp = false;
		PrefsManager.instance.iWallWarpLocation = CanvasStreamingInterface.instance.screenCenter.CoordinateConvert();
		BloxelsSceneManager.instance.GameplayWithGame(bloxelGame, square.serverSquare);
	}

	public void SetReady(CanvasTileInfo.ReadyState state)
	{
		switch (state)
		{
		case CanvasTileInfo.ReadyState.Requested:
			break;
		case CanvasTileInfo.ReadyState.Downloading:
			break;
		case CanvasTileInfo.ReadyState.Waiting:
			break;
		case CanvasTileInfo.ReadyState.Ready:
			if (square.project != null)
			{
				uiButton.interactable = true;
				uiButton.transform.DOScale(1f, UIAnimationManager.speedFast);
			}
			break;
		case CanvasTileInfo.ReadyState.Failed:
			uiButton.transform.localScale = Vector3.zero;
			uiButton.interactable = false;
			break;
		}
	}
}
