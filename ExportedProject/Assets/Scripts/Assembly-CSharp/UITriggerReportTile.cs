using UnityEngine;

[RequireComponent(typeof(UIButton))]
public class UITriggerReportTile : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public UIButton uiButton;

	public UIButtonIcon uiIcon;

	public Object reportPrefab;

	private void Awake()
	{
		uiButton = GetComponent<UIButton>();
		uiIcon = GetComponentInChildren<UIButtonIcon>();
		uiButton.OnClick += Activate;
		reportPrefab = Resources.Load("Prefabs/UIPopupReport");
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
	}

	public void Activate()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(reportPrefab);
		UIPopupReport component = gameObject.GetComponent<UIPopupReport>();
		component.Init(square);
	}
}
