using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]
public class UITriggerShareTile : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public Button uiButton;

	public UIButtonIcon uiIcon;

	public Object sharePrefab;

	private void Awake()
	{
		uiButton = GetComponent<Button>();
		uiIcon = GetComponentInChildren<UIButtonIcon>();
		uiButton.onClick.AddListener(Activate);
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		if (square.serverSquare.type == ProjectType.Game)
		{
			sharePrefab = UIOverlayCanvas.instance.shareGamePrefab;
		}
		else
		{
			sharePrefab = UIOverlayCanvas.instance.shareCoordinatePrefab;
		}
	}

	public void Activate()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(sharePrefab);
		UIPopupShare component = gameObject.GetComponent<UIPopupShare>();
		component.Init(square);
	}
}
