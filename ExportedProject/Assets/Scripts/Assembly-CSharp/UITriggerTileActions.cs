using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UITriggerTileActions : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public Button uiButton;

	public UIButtonIcon uiIcon;

	public RectTransform rectMenu;

	public Object reportPrefab;

	public UITriggerReportTile uiTriggerReport;

	public UITriggerDeleteTile uiTriggerDelete;

	private RectTransform iconRect;

	[Header("| ========= State Tracking ========= |")]
	public bool isActive;

	public Vector3 stateClosed;

	public Vector3 stateOpen;

	private void Awake()
	{
		if (uiButton == null)
		{
			uiButton = GetComponent<Button>();
		}
		if (uiIcon == null)
		{
			uiIcon = GetComponentInChildren<UIButtonIcon>();
		}
		if (rectMenu == null)
		{
			rectMenu = GetComponent<RectTransform>();
		}
		iconRect = uiIcon.GetComponent<RectTransform>();
		uiButton.onClick.AddListener(Toggle);
		reportPrefab = Resources.Load("Prefabs/UIPopupReport");
		rectMenu.localScale = stateClosed;
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		uiTriggerReport.Init(square);
		uiTriggerDelete.Init(square);
	}

	public void Toggle()
	{
		if (isActive)
		{
			DeActivate();
		}
		else
		{
			Activate();
		}
	}

	private void Activate()
	{
		iconRect.DORotate(new Vector3(0f, 0f, 180f), UIAnimationManager.speedMedium);
		rectMenu.DOScaleY(1f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			isActive = true;
		});
	}

	private void DeActivate()
	{
		iconRect.DORotate(new Vector3(0f, 0f, 0f), UIAnimationManager.speedMedium);
		rectMenu.DOScaleY(0f, UIAnimationManager.speedMedium).OnComplete(delegate
		{
			isActive = false;
		});
	}
}
