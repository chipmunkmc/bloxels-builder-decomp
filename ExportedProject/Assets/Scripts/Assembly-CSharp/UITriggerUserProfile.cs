using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(UIButton))]
public class UITriggerUserProfile : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public UserAccount user;

	[Header("| ========= UI ========= |")]
	public UIButton uiButton;

	public Image uiImageUserDefault;

	public RawImage uiRawImageUserAvatar;

	public CoverBoard cover;

	public TextMeshProUGUI uiTextUserName;

	public UITransparentClickableLayer transparentLayer;

	private void Awake()
	{
		if (uiButton == null)
		{
			uiButton = GetComponent<UIButton>();
		}
		if (uiImageUserDefault == null)
		{
			uiImageUserDefault = GetComponentInChildren<Image>();
		}
		if (uiRawImageUserAvatar == null)
		{
			uiRawImageUserAvatar = GetComponentInChildren<RawImage>();
		}
		uiRawImageUserAvatar.gameObject.SetActive(false);
		uiButton.OnClick += Activate;
		cover = new CoverBoard(Color.clear);
	}

	public void Activate()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		UIUserProfile.instance.Init(user.serverID);
	}

	public void Init(string userID)
	{
		if (CurrentUser.instance.account != null && CurrentUser.instance.account.serverID == userID && CurrentUser.instance.account.avatar != null)
		{
			user = CurrentUser.instance.account;
			uiImageUserDefault.gameObject.SetActive(false);
			uiRawImageUserAvatar.gameObject.SetActive(true);
			uiRawImageUserAvatar.texture = CurrentUser.instance.account.avatar.UpdateTexture2D(ref CurrentUser.instance.cover.colors, ref CurrentUser.instance.cover.texture, CurrentUser.instance.cover.style);
			uiTextUserName.text = user.userName;
			return;
		}
		StartCoroutine(BloxelServerRequests.instance.FindSimpleUserByID(userID, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				user = new UserAccount(response);
				if (user.userName != null)
				{
					FadeButtonIn(uiButton);
					uiTextUserName.text = user.userName;
				}
				else
				{
					FadeButtonOut(uiButton);
				}
				if (user.avatar != null)
				{
					uiImageUserDefault.gameObject.SetActive(false);
					uiRawImageUserAvatar.gameObject.SetActive(true);
					uiRawImageUserAvatar.texture = user.avatar.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
				}
				else
				{
					uiImageUserDefault.gameObject.SetActive(true);
					uiRawImageUserAvatar.gameObject.SetActive(false);
				}
			}
			else
			{
				FadeButtonOut(uiButton);
			}
		}));
	}

	private void FadeButtonOut(UIButton btn, bool _interactable = false)
	{
		btn.interactable = _interactable;
		Graphic[] componentsInChildren = btn.GetComponentsInChildren<Graphic>();
		Graphic[] array = componentsInChildren;
		foreach (Graphic target in array)
		{
			target.DOFade(0.25f, 0f);
		}
	}

	private void FadeButtonIn(UIButton btn, bool _interactable = true)
	{
		btn.interactable = _interactable;
		Graphic[] componentsInChildren = btn.GetComponentsInChildren<Graphic>();
		Graphic[] array = componentsInChildren;
		foreach (Graphic target in array)
		{
			if (!transparentLayer)
			{
				target.DOFade(1f, 0f);
			}
		}
	}
}
