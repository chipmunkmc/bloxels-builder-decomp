using TMPro;
using UnityEngine;

[RequireComponent(typeof(UIButton))]
public class UITriggerViewGameAssets : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public CanvasSquare square;

	[Header("| ========= UI ========= |")]
	public UIButton uiButton;

	public TextMeshProUGUI uiText;

	public string string_viewAssets = "View Assets";

	public string string_loading = "Downloading...";

	public string string_parsing = "Un-Packing...";

	public string string_requested = "Requested...";

	public string string_failed = "Failed";

	private bool _isReady;

	private void Awake()
	{
		string_viewAssets = LocalizationManager.getInstance().getLocalizedText("iWall22", string_viewAssets);
		string_loading = LocalizationManager.getInstance().getLocalizedText("iWall117", string_loading);
		string_parsing = LocalizationManager.getInstance().getLocalizedText("iWall116", string_parsing);
		string_requested = LocalizationManager.getInstance().getLocalizedText("iWall49", string_requested);
		string_failed = LocalizationManager.getInstance().getLocalizedText("iWall118", string_failed);
		uiButton = GetComponent<UIButton>();
		uiText.text = string_loading;
		uiButton.interactable = false;
	}

	private void OnDestroy()
	{
		uiButton.OnClick -= Activate;
		if (square != null)
		{
			square.OnStateChange -= SetReady;
		}
	}

	public void Init(CanvasSquare _square)
	{
		square = _square;
		uiButton.interactable = true;
		uiText.text = string_viewAssets;
		uiButton.OnClick += Activate;
	}

	public void Activate()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		GameAssetsMenu.instance.Init(square);
	}

	public void SetReady(CanvasTileInfo.ReadyState state)
	{
		switch (state)
		{
		case CanvasTileInfo.ReadyState.Requested:
			uiText.text = string_requested;
			uiButton.interactable = false;
			break;
		case CanvasTileInfo.ReadyState.Downloading:
			uiText.text = string_loading;
			uiButton.interactable = false;
			break;
		case CanvasTileInfo.ReadyState.Waiting:
			uiText.text = string_parsing;
			uiButton.interactable = false;
			break;
		case CanvasTileInfo.ReadyState.Ready:
			if (square.project != null)
			{
				uiButton.OnClick += Activate;
				uiText.text = string_viewAssets;
				uiButton.interactable = true;
			}
			break;
		case CanvasTileInfo.ReadyState.Failed:
			uiText.text = string_failed;
			uiButton.interactable = false;
			break;
		}
	}
}
