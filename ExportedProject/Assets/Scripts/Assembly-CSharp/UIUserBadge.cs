using DG.Tweening;
using UnityEngine;

public class UIUserBadge : UIUserGems
{
	private RectTransform rect;

	public new bool isVisible = true;

	private Vector2 positionVisible;

	private Vector2 positionHidden;

	private bool hasDeterminedPosition;

	private new void Start()
	{
		base.Start();
		CurrentUser.instance.OnUpdate += HandleUserUpdate;
		rect = GetComponent<RectTransform>();
	}

	private new void OnDestroy()
	{
		CurrentUser.instance.OnUpdate -= HandleUserUpdate;
		base.OnDestroy();
	}

	private void HandleUserUpdate(UserAccount acct)
	{
		if (acct == null)
		{
			uiTextUserName.SetText("Guest");
			return;
		}
		uiBoardAvatar.cover.style = new CoverStyle(Color.clear);
		CheckUser();
		uiTextUserName.SetText(acct.userName);
	}

	public new void Hide()
	{
		if (isVisible)
		{
			if (!hasDeterminedPosition)
			{
				positionVisible = rect.anchoredPosition;
				positionHidden = new Vector2(rect.anchoredPosition.x - 600f, rect.anchoredPosition.y);
				hasDeterminedPosition = true;
			}
			rect.DOAnchorPos(positionHidden, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = false;
			});
		}
	}

	public new void Show()
	{
		if (!isVisible)
		{
			rect.DOAnchorPos(positionVisible, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = true;
			});
		}
	}
}
