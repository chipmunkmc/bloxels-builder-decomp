using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UIUserCard : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	[Header("| ========= Data ========= |")]
	public UserAccount account;

	[Header("| ========= UI ========= |")]
	public RectTransform rect;

	public Text uiTextUserName;

	public Image uiImageDefaultAvatar;

	public RawImage uiRawImageAvatar;

	public Button uiButtonFollow;

	public GridLayoutGroup uiGroupTileGrid;

	public Object simpleSquarePrefab;

	public CoverBoard cover;

	public void Awake()
	{
		if (rect == null)
		{
			rect = GetComponent<RectTransform>();
		}
		uiButtonFollow.onClick.AddListener(FollowPressed);
		cover = new CoverBoard(Color.clear);
	}

	public void Init(UserAccount user)
	{
		account = user;
		Populate();
	}

	private void Populate()
	{
		uiTextUserName.text = account.userName;
		if (account.avatar != null)
		{
			uiImageDefaultAvatar.gameObject.SetActive(false);
			uiRawImageAvatar.gameObject.SetActive(true);
			uiRawImageAvatar.texture = account.avatar.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
		}
		else
		{
			uiImageDefaultAvatar.gameObject.SetActive(true);
			uiRawImageAvatar.gameObject.SetActive(false);
		}
		StartCoroutine(CreateLatestSquares());
		SetupFollowState();
	}

	private void SetupFollowState()
	{
		bool flag = false;
		if (CurrentUser.instance.account != null && CurrentUser.instance.account.serverID == account.serverID)
		{
			flag = true;
		}
		if (CurrentUser.instance.account != null && !flag && !CurrentUser.instance.account.following.Contains(account.serverID))
		{
			UpdateFollowState(UIUserProfile.FollowState.Follow);
		}
		else if (CurrentUser.instance.account != null && !flag)
		{
			UpdateFollowState(UIUserProfile.FollowState.UnFollow);
		}
		else if (flag)
		{
			UpdateFollowState(UIUserProfile.FollowState.CurrentUser);
		}
		else
		{
			UpdateFollowState(UIUserProfile.FollowState.NotLoggedIn);
		}
	}

	private IEnumerator CreateLatestSquares()
	{
		List<string> theSquares2 = new List<string>();
		theSquares2 = ((account.publishedSquares.Count <= 0) ? account.ownedSquares : account.publishedSquares);
		int i = theSquares2.Count - 1;
		while (i >= 0 && i >= theSquares2.Count - 3)
		{
			yield return StartCoroutine(BloxelServerRequests.instance.FindSquareByID(theSquares2[i], delegate(string response)
			{
				if (!string.IsNullOrEmpty(response) && response != "ERROR")
				{
					ServerSquare serverSquare = new ServerSquare(response);
					GameObject gameObject = Object.Instantiate(simpleSquarePrefab) as GameObject;
					gameObject.transform.SetParent(uiGroupTileGrid.transform);
					gameObject.transform.localScale = Vector3.one;
					UISimpleSquare component = gameObject.GetComponent<UISimpleSquare>();
					component.Init(serverSquare);
				}
			}));
			i--;
		}
	}

	private void UpdateFollowState(UIUserProfile.FollowState state)
	{
		UIButtonIcon componentInChildren = uiButtonFollow.GetComponentInChildren<UIButtonIcon>();
		switch (state)
		{
		case UIUserProfile.FollowState.CurrentUser:
			uiButtonFollow.interactable = false;
			componentInChildren.sprite = AssetManager.instance.iconCheckmark;
			break;
		case UIUserProfile.FollowState.Follow:
			uiButtonFollow.interactable = true;
			componentInChildren.sprite = AssetManager.instance.iconAdd;
			break;
		case UIUserProfile.FollowState.UnFollow:
			uiButtonFollow.interactable = true;
			componentInChildren.sprite = AssetManager.instance.iconCheckmark;
			break;
		case UIUserProfile.FollowState.NotLoggedIn:
			uiButtonFollow.interactable = false;
			componentInChildren.sprite = AssetManager.instance.iconNull;
			break;
		}
	}

	public void FollowPressed()
	{
		if (!CurrentUser.instance.account.following.Contains(account.serverID))
		{
			StartCoroutine(CurrentUser.instance.UpdateServer_FollowUser(account.serverID, delegate(string response)
			{
				if (!string.IsNullOrEmpty(response) && response != "ERROR")
				{
					UpdateFollowState(UIUserProfile.FollowState.UnFollow);
					CurrentUser.instance.account.following.Add(account.serverID);
					CurrentUser.instance.Save();
					UIUserProfile.instance.AddCard(account);
				}
			}));
			return;
		}
		StartCoroutine(CurrentUser.instance.UpdateServer_UnFollowUser(account.serverID, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				UpdateFollowState(UIUserProfile.FollowState.Follow);
				CurrentUser.instance.account.following.Remove(account.serverID);
				CurrentUser.instance.Save();
				UIUserProfile.instance.RemoveCard(account);
			}
		}));
	}

	public void OnPointerClick(PointerEventData eData)
	{
		UIUserProfile.instance.Hide().OnComplete(delegate
		{
			UIUserProfile.instance.InitWithAccount(account);
		});
	}
}
