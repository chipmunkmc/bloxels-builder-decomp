using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIUserFeed : MonoBehaviour
{
	public static UIUserFeed instance;

	public RectTransform rect;

	public bool isVisible;

	public Button uiButtonPullTab;

	public Vector2 openPosition;

	public Vector2 closedPosition;

	public Object uiNotificationPrefab;

	public Object uiNotificationCoinPrefab;

	public Object uiNotificationGameplayPrefab;

	public ScrollRect scrollRect;

	public RectTransform scrollRectContent;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
	}

	private void Start()
	{
		uiButtonPullTab.onClick.AddListener(Toggle);
	}

	private void OnDestroy()
	{
		uiButtonPullTab.onClick.RemoveListener(Toggle);
	}

	public void Toggle()
	{
		if (isVisible)
		{
			Hide();
		}
		else
		{
			Show();
		}
	}

	private void Hide()
	{
		if (isVisible)
		{
			rect.DOAnchorPos(closedPosition, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = false;
			}).OnComplete(delegate
			{
			});
		}
	}

	private void Show()
	{
		if (!isVisible)
		{
			rect.DOAnchorPos(openPosition, UIAnimationManager.speedMedium).OnStart(delegate
			{
				isVisible = true;
			}).OnComplete(delegate
			{
			});
		}
	}

	public void CreateGameplayNotification(JSONObject msg)
	{
		GameObject gameObject = Object.Instantiate(uiNotificationGameplayPrefab) as GameObject;
		gameObject.transform.SetParent(scrollRectContent);
		gameObject.transform.SetAsFirstSibling();
		gameObject.transform.localScale = Vector3.one;
		UIFeedNotification component = gameObject.GetComponent<UIFeedNotification>();
		component.message = msg;
		component.Init();
	}

	public void CreateCoinNotification(JSONObject msg)
	{
		GameObject gameObject = Object.Instantiate(uiNotificationCoinPrefab) as GameObject;
		gameObject.transform.SetParent(scrollRectContent);
		gameObject.transform.SetAsFirstSibling();
		gameObject.transform.localScale = Vector3.one;
		UIFeedNotificationReward component = gameObject.GetComponent<UIFeedNotificationReward>();
		component.message = msg;
		component.Init();
	}
}
