using TMPro;

public class UIUserGems : UIGemBank
{
	public UIButton uiButtonUser;

	public TextMeshProUGUI uiTextUserName;

	public new void Start()
	{
		base.Start();
		SetUserName();
		uiButtonUser.OnClick += HandleUserPress;
		PlayerBankManager.instance.OnGemUpdate += HandleGemUpdate;
	}

	public void OnDestroy()
	{
		uiButtonUser.OnClick -= HandleUserPress;
		PlayerBankManager.instance.OnGemUpdate -= HandleGemUpdate;
	}

	public virtual void SetUserName()
	{
		if (CurrentUser.instance.account != null)
		{
			uiTextUserName.SetText(CurrentUser.instance.account.userName);
		}
	}

	private void HandleGemUpdate(int gems)
	{
		UpdateFunds(gems);
	}

	private void HandleUserPress()
	{
		UINavMenu.instance.UserAction();
	}
}
