using System.Collections;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIUserProfile : MonoBehaviour
{
	public enum FollowState
	{
		Follow = 0,
		UnFollow = 1,
		NotLoggedIn = 2,
		CurrentUser = 3
	}

	public static UIUserProfile instance;

	[Header("| ========= Movable ========= |")]
	public RectTransform rect;

	public Vector2 basePosition;

	public Vector2 hiddenPosition;

	[Header("| ========= Data ========= |")]
	public UserAccount profileAccount;

	[Header("| ========= UI ========= |")]
	public RawImage avatar;

	public Text uiTextUserName;

	public Text uiTextFollowerCount;

	public Text uiTextFollowingCount;

	public Text uiTextSquareCount;

	public Text uiTextAvatarLoading;

	public UIButton uiButtonFollow;

	public Button uiButtonReport;

	public UIButton uiButtonDismiss;

	public Button uiButtonEdit;

	public Button uiButtonResendEmail;

	public TextMeshProUGUI uiTextResendSuccess;

	public Color emailSentSuccess;

	public Color emailSentFail;

	public UIUserStats userStats;

	public FilterableSquares filterableSquares;

	public UIFollowCardController followingController;

	public UIFollowCardController followerController;

	public Object editMenuPrefab;

	public Object reportPrefab;

	public Object accountMenuPrefab;

	public Text uiTextFollowState;

	public Text uiTextFollowUserName;

	public CoverBoard cover;

	[Header("| ========= State Tracking ========= |")]
	public bool isVisible;

	private bool _isCurrentUser;

	[Header("| ========= Player Registration ========= |")]
	public RectTransform rectIncomplete;

	public TextMeshProUGUI uiTextParentEmail;

	public UIButton uiButtonFinish;

	public bool isCurrentUser
	{
		get
		{
			return _isCurrentUser;
		}
		set
		{
			_isCurrentUser = value;
			VariableUISetup();
		}
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		if (editMenuPrefab == null)
		{
			editMenuPrefab = Resources.Load("Prefabs/UIPopupUser");
		}
		if (reportPrefab == null)
		{
			reportPrefab = Resources.Load("Prefabs/UIPopupReport");
		}
		cover = new CoverBoard(BloxelBoard.baseUIBoardColor);
		uiButtonDismiss.OnClick += Dismiss;
	}

	private void Start()
	{
		uiButtonReport.onClick.AddListener(ReportUserPressed);
		uiButtonEdit.onClick.AddListener(ShowEditMenu);
		uiButtonFollow.OnClick += ToggleFollow;
		CurrentUser.instance.OnUpdate += LoggedInUserUpdate;
		Reset();
		base.transform.localScale = Vector3.zero;
		uiButtonFinish.OnClick += FinishAccountProcess;
		uiButtonResendEmail.onClick.AddListener(ResendParentEmail);
	}

	private void OnDestroy()
	{
		CurrentUser.instance.OnUpdate -= LoggedInUserUpdate;
		uiButtonFinish.OnClick -= FinishAccountProcess;
		uiButtonDismiss.OnClick -= Dismiss;
		uiButtonFollow.OnClick -= ToggleFollow;
	}

	private void Dismiss()
	{
		Hide();
	}

	public void Init(string userID)
	{
		if (CurrentUser.instance.account != null && CurrentUser.instance.account.serverID == userID)
		{
			isCurrentUser = true;
		}
		else
		{
			isCurrentUser = false;
		}
		if (profileAccount != null && profileAccount.serverID == userID)
		{
			if (filterableSquares.squareIDs.Count > filterableSquares.uiSquares.Count)
			{
				filterableSquares.Init(profileAccount.uniqueSquares);
			}
			Show();
		}
		else
		{
			Reset();
			GetUserAccount(userID);
			Show();
		}
		CheckComplete();
		UserProfileTabController.instance.SwitchSection(UserProfileTabController.TabSection.iWallTiles);
	}

	public void InitWithAccount(UserAccount user)
	{
		if (CurrentUser.instance.account != null && CurrentUser.instance.account.serverID == user.serverID)
		{
			isCurrentUser = true;
		}
		else
		{
			isCurrentUser = false;
		}
		bool flag = false;
		if (profileAccount != null && profileAccount.serverID == user.serverID)
		{
			if (filterableSquares.squareIDs.Count > filterableSquares.uiSquares.Count)
			{
				filterableSquares.Init(profileAccount.uniqueSquares);
			}
			Show();
		}
		else
		{
			Reset();
			profileAccount = user;
			Populate();
			Show();
		}
		CheckComplete();
		UserProfileTabController.instance.SwitchSection(UserProfileTabController.TabSection.iWallTiles);
	}

	private void VariableUISetup()
	{
		if (isCurrentUser && CurrentUser.instance.active)
		{
			uiButtonEdit.gameObject.SetActive(true);
		}
		else
		{
			uiButtonEdit.gameObject.SetActive(false);
		}
	}

	public void CheckComplete()
	{
		if (!isCurrentUser)
		{
			rectIncomplete.localScale = Vector3.zero;
		}
		else if (CurrentUser.instance.account.status == UserAccount.Status.unverified || !CurrentUser.instance.account.completed)
		{
			rectIncomplete.localScale = Vector3.one;
			uiTextParentEmail.SetText(CurrentUser.instance.account.parentEmail);
		}
		else
		{
			rectIncomplete.localScale = Vector3.zero;
		}
	}

	public void FinishAccountProcess()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(accountMenuPrefab);
		UIPopupAccount component = gameObject.GetComponent<UIPopupAccount>();
		if (CurrentUser.instance.account != null)
		{
			component.menuIndex.initComplete = true;
			StartCoroutine(DelayedSwitchMenu(component));
		}
	}

	private IEnumerator DelayedSwitchMenu(UIPopupAccount accountMenu)
	{
		while (accountMenu.isAnimating)
		{
			yield return new WaitForEndOfFrame();
		}
		accountMenu.OnInit += delegate
		{
			accountMenu.menuIndex.FastHide();
			if (CurrentUser.instance.account.status == UserAccount.Status.active)
			{
				accountMenu.registerController.SwitchStep(RegistrationController.Step.EmailPassword);
			}
			else
			{
				accountMenu.registerController.SwitchStep(RegistrationController.Step.VerifyCode);
			}
		};
	}

	public void ResendParentEmail()
	{
		StartCoroutine(BloxelServerRequests.instance.ResendParentEmail(CurrentUser.instance.account.serverID, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				uiTextResendSuccess.color = emailSentSuccess;
				uiTextResendSuccess.text = LocalizationManager.getInstance().getLocalizedText("iWall87", "Resent to") + " " + CurrentUser.instance.account.parentEmail;
				uiTextResendSuccess.transform.localScale = Vector3.one;
			}
			else
			{
				uiTextResendSuccess.color = emailSentFail;
				uiTextResendSuccess.text = LocalizationManager.getInstance().getLocalizedText("iWall98", "Error Sending to") + " " + CurrentUser.instance.account.parentEmail;
				uiTextResendSuccess.transform.localScale = Vector3.one;
			}
		}));
	}

	public void ShowEditMenu()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(editMenuPrefab);
	}

	private void ReportUserPressed()
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(reportPrefab);
		UIPopupReport component = gameObject.GetComponent<UIPopupReport>();
		component.Init(profileAccount);
	}

	public void LoggedInUserUpdate(UserAccount acct)
	{
		if (isVisible)
		{
			if (acct == null)
			{
				Hide();
			}
			else if (!(profileAccount.serverID != acct.serverID))
			{
				avatar.texture = acct.avatar.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
				uiTextAvatarLoading.gameObject.SetActive(false);
				uiTextUserName.text = CurrentUser.instance.account.userName;
				uiTextFollowUserName.text = uiTextUserName.text;
				CheckComplete();
				VariableUISetup();
			}
		}
	}

	private void Reset()
	{
		uiTextUserName.text = "---";
		uiTextFollowUserName.text = uiTextUserName.text;
		uiTextFollowerCount.text = "--";
		uiTextFollowingCount.text = "--";
		uiTextSquareCount.text = "--";
		uiTextAvatarLoading.text = LocalizationManager.getInstance().getLocalizedText("iWall97", "Avatar Loading...");
		avatar.texture = AssetManager.instance.boardSolidBlank;
		uiTextAvatarLoading.gameObject.SetActive(true);
		uiTextResendSuccess.transform.localScale = Vector3.zero;
	}

	public void GetUserAccount(string userID)
	{
		StartCoroutine(BloxelServerRequests.instance.FindUserByID(userID, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				profileAccount = new UserAccount(response);
				Populate();
			}
		}));
	}

	private void Populate()
	{
		uiTextUserName.text = profileAccount.userName;
		uiTextFollowUserName.text = uiTextUserName.text;
		uiTextFollowerCount.text = profileAccount.followers.Count.ToString();
		uiTextFollowingCount.text = profileAccount.following.Count.ToString();
		uiTextSquareCount.text = profileAccount.uniqueSquares.Count.ToString();
		BuildAvatar();
		if (CurrentUser.instance.account != null && !isCurrentUser && !CurrentUser.instance.account.following.Contains(profileAccount.serverID))
		{
			UpdateFollowState(FollowState.Follow);
		}
		else if (CurrentUser.instance.account != null && !isCurrentUser)
		{
			UpdateFollowState(FollowState.UnFollow);
		}
		else if (isCurrentUser)
		{
			UpdateFollowState(FollowState.CurrentUser);
		}
		else
		{
			UpdateFollowState(FollowState.NotLoggedIn);
		}
		userStats.Init(profileAccount);
		filterableSquares.Init(profileAccount.uniqueSquares);
		followingController.Init(profileAccount);
		followerController.Init(profileAccount);
	}

	private void UpdateFollowState(FollowState state)
	{
		UIButtonIcon componentInChildren = uiButtonFollow.GetComponentInChildren<UIButtonIcon>();
		switch (state)
		{
		case FollowState.CurrentUser:
			uiTextFollowState.text = LocalizationManager.getInstance().getLocalizedText("iWall79", "It's You!");
			uiButtonFollow.uiImageClickReceiver.color = AssetManager.instance.bloxelGreen;
			uiButtonFollow.interactable = false;
			componentInChildren.sprite = AssetManager.instance.iconCheckmark;
			break;
		case FollowState.Follow:
			uiTextFollowState.text = LocalizationManager.getInstance().getLocalizedText("iWall61", "Follow");
			uiButtonFollow.uiImageClickReceiver.color = AssetManager.instance.bloxelBlue;
			uiButtonFollow.interactable = true;
			componentInChildren.sprite = AssetManager.instance.iconAdd;
			break;
		case FollowState.UnFollow:
			uiTextFollowState.text = LocalizationManager.getInstance().getLocalizedText("iWall62", "Following");
			uiButtonFollow.uiImageClickReceiver.color = AssetManager.instance.bloxelGreen;
			uiButtonFollow.interactable = true;
			componentInChildren.sprite = AssetManager.instance.iconCheckmark;
			break;
		case FollowState.NotLoggedIn:
			uiTextFollowState.text = LocalizationManager.getInstance().getLocalizedText("iWall80", "Login to Follow");
			uiButtonFollow.uiImageClickReceiver.color = AssetManager.instance.bloxelRed;
			uiButtonFollow.interactable = false;
			componentInChildren.sprite = AssetManager.instance.iconNull;
			break;
		}
	}

	public void BuildAvatar()
	{
		if (profileAccount.avatar == null)
		{
			uiTextAvatarLoading.text = LocalizationManager.getInstance().getLocalizedText("iWall63", "No Avatar");
			avatar.texture = AssetManager.instance.boardSolidBlank;
			uiTextAvatarLoading.gameObject.SetActive(true);
		}
		else
		{
			avatar.texture = profileAccount.avatar.UpdateTexture2D(ref cover.colors, ref cover.texture, cover.style);
			uiTextAvatarLoading.gameObject.SetActive(false);
		}
	}

	public void ToggleFollow()
	{
		SoundManager.PlayOneShot(SoundManager.instance.flip);
		if (!CurrentUser.instance.account.following.Contains(profileAccount.serverID))
		{
			StartCoroutine(CurrentUser.instance.UpdateServer_FollowUser(profileAccount.serverID, delegate(string response)
			{
				if (!string.IsNullOrEmpty(response) && response != "ERROR")
				{
					UpdateFollowState(FollowState.UnFollow);
					CurrentUser.instance.account.following.Add(profileAccount.serverID);
					CurrentUser.instance.Save();
					AddCard(profileAccount);
				}
			}));
			return;
		}
		StartCoroutine(CurrentUser.instance.UpdateServer_UnFollowUser(profileAccount.serverID, delegate(string response)
		{
			if (!string.IsNullOrEmpty(response) && response != "ERROR")
			{
				UpdateFollowState(FollowState.Follow);
				CurrentUser.instance.account.following.Remove(profileAccount.serverID);
				CurrentUser.instance.Save();
				RemoveCard(profileAccount);
			}
		}));
	}

	public void AddCard(UserAccount u)
	{
		if (isCurrentUser)
		{
			followingController.CreateCard(u, true);
			uiTextFollowingCount.text = (int.Parse(uiTextFollowingCount.text) + 1).ToString();
		}
		else
		{
			followerController.CreateCard(CurrentUser.instance.account, true);
			uiTextFollowerCount.text = (int.Parse(uiTextFollowerCount.text) + 1).ToString();
		}
	}

	public void RemoveCard(UserAccount u)
	{
		if (isCurrentUser)
		{
			followingController.RemoveCard(u, true);
			uiTextFollowingCount.text = (int.Parse(uiTextFollowingCount.text) - 1).ToString();
		}
		else
		{
			followerController.RemoveCard(CurrentUser.instance.account, true);
			uiTextFollowerCount.text = (int.Parse(uiTextFollowerCount.text) - 1).ToString();
		}
	}

	public Tweener Show()
	{
		if (isVisible)
		{
			return null;
		}
		Tweener tweener = null;
		base.transform.localScale = Vector3.one;
		return rect.DOAnchorPos(basePosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isVisible = true;
		}).OnComplete(delegate
		{
			BloxelLibrary.instance.Hide();
		});
	}

	public Tweener Hide()
	{
		if (!isVisible)
		{
			return null;
		}
		Tweener tweener = null;
		return rect.DOAnchorPos(hiddenPosition, UIAnimationManager.speedMedium).OnStart(delegate
		{
			isVisible = false;
			filterableSquares.StopLoading();
			followingController.StopLoading();
			followerController.StopLoading();
		}).OnComplete(delegate
		{
			base.transform.localScale = Vector3.zero;
			BloxelLibrary.instance.Show();
		});
	}
}
