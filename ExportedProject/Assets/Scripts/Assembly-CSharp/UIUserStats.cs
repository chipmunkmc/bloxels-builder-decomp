using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIUserStats : MonoBehaviour
{
	[Header("| ========= UI ========= |")]
	public Text uiTextDashboardLabel;

	public Text uiTextLikesCount;

	public Text uiTextDownloadsCount;

	public Text uiTextViewsCount;

	public Text uiTextEarningsCount;

	public Text uiTextPlaysCount;

	public ScrollRect scrollRect;

	public Transform grid;

	public List<UISquareStats> statCards = new List<UISquareStats>();

	public Object uiStatCardPrefab;

	public Transform transformOverlay;

	public Text uiTextLoadingCount;

	private int _totalLikes;

	private int _totalDownloads;

	private int _totalViews;

	private int _totalEarnings;

	private int _totalPlays;

	private void Awake()
	{
		if (uiStatCardPrefab == null)
		{
			uiStatCardPrefab = Resources.Load("Prefabs/UISquareStats");
		}
	}

	public void Init(UserAccount user)
	{
		Reset();
		ShowOverlay();
		UIUserProfile.instance.filterableSquares.OnSquare += CreateSquareStat;
		UIUserProfile.instance.filterableSquares.OnPopulateComplete += PopulateComplete;
	}

	public void Reset()
	{
		UIUserProfile.instance.filterableSquares.OnSquare -= CreateSquareStat;
		UIUserProfile.instance.filterableSquares.OnPopulateComplete -= PopulateComplete;
		statCards.Clear();
		foreach (Transform item in grid)
		{
			Object.Destroy(item.gameObject);
		}
		_totalLikes = 0;
		_totalDownloads = 0;
		_totalViews = 0;
		_totalEarnings = 0;
		_totalPlays = 0;
		UpdateStatText();
	}

	private void CreateSquareStat(UISimpleSquare square)
	{
		GameObject gameObject = Object.Instantiate(uiStatCardPrefab) as GameObject;
		gameObject.transform.SetParent(grid);
		gameObject.transform.localScale = Vector3.one;
		UISquareStats component = gameObject.GetComponent<UISquareStats>();
		component.Init(square);
		statCards.Add(component);
		UpdateStats(square.dataModel);
	}

	private void PopulateComplete()
	{
		AdjustScrollRectHeight(grid);
		HideOverlay();
	}

	private void UpdateStats(ServerSquare dataModel)
	{
		_totalLikes += dataModel.likes.Length;
		_totalDownloads += dataModel.downloads.Length;
		_totalViews += dataModel.views;
		_totalEarnings += dataModel.earnings;
		_totalPlays += dataModel.associatedData.game.plays;
		UpdateStatText();
	}

	private void UpdateStatText()
	{
		uiTextLikesCount.text = _totalLikes.ToString();
		uiTextDownloadsCount.text = _totalDownloads.ToString();
		uiTextViewsCount.text = _totalViews.ToString();
		uiTextEarningsCount.text = _totalEarnings.ToString();
		uiTextPlaysCount.text = _totalPlays.ToString();
	}

	public void ShowOverlay()
	{
		transformOverlay.gameObject.SetActive(true);
		transformOverlay.DOScale(1f, UIAnimationManager.speedFast);
	}

	public void HideOverlay()
	{
		transformOverlay.DOScale(0f, UIAnimationManager.speedFast).OnComplete(delegate
		{
			transformOverlay.gameObject.SetActive(false);
		});
	}

	private void AdjustScrollRectHeight(Transform t)
	{
		GridLayoutGroup component = t.GetComponent<GridLayoutGroup>();
		RectTransform component2 = t.GetComponent<RectTransform>();
		int num = 0;
		for (int i = 0; i < statCards.Count && !(Mathf.Abs(statCards[i].rect.anchoredPosition.y) >= Mathf.Abs(component.cellSize.y + component.spacing.y + (float)component.padding.top)); i++)
		{
			num++;
		}
		if (num == 0)
		{
			num = 1;
		}
		int num2 = Mathf.CeilToInt(t.childCount / num);
		float num3 = component.spacing.y + component.cellSize.y;
		float y = Mathf.Abs((float)num2 * num3) + Mathf.Abs(num3);
		component2.sizeDelta = new Vector2(component2.sizeDelta.x, y);
		component2.anchoredPosition = Vector2.zero;
	}
}
