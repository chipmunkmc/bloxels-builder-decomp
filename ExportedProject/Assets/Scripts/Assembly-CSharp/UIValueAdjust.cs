using UnityEngine;
using UnityEngine.UI;

public class UIValueAdjust : MonoBehaviour
{
	public UIButton uiButtonIncrement;

	public UIButton uiButtonDecrement;

	public Text uiTextValue;

	public float val;

	public float minValue;

	public float maxValue;

	public float stepAmount = 1f;

	public void OnEnable()
	{
		uiTextValue.text = val.ToString();
	}

	public void OnDisable()
	{
	}

	public void SetValue(float _val)
	{
		if (_val < minValue)
		{
			val = minValue;
		}
		else
		{
			val = _val;
		}
		uiTextValue.text = val.ToString();
	}

	public void Increment()
	{
		if (val >= maxValue)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.popB);
			return;
		}
		SoundManager.instance.PlaySound(SoundManager.instance.drawOnBoard);
		val += stepAmount;
		uiTextValue.text = val.ToString();
	}

	public void Decrement()
	{
		if (val <= minValue)
		{
			SoundManager.instance.PlaySound(SoundManager.instance.popB);
			return;
		}
		SoundManager.instance.PlaySound(SoundManager.instance.drawOnBoard);
		val -= stepAmount;
		uiTextValue.text = val.ToString();
	}
}
