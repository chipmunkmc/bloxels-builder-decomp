using System.Collections;
using DG.Tweening;
using UnityEngine;
using UnityEngine.UI;

public class UIWarpDrive : UIMoveable
{
	public static UIWarpDrive instance;

	public InputField uiInputColumn;

	public InputField uiInputRow;

	public UIButton uiButtonWarp;

	public RectTransform rectVooDoo;

	private Coroutine warpTimer;

	public bool canWarp = true;

	public float warpDelay = 2f;

	private WaitForSeconds wait;

	private new void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		base.Awake();
	}

	private new void Start()
	{
		wait = new WaitForSeconds(warpDelay);
		uiButtonWarp.OnClick += WarpPressed;
		string iWallWarpLocation = PrefsManager.instance.iWallWarpLocation;
		if (!string.IsNullOrEmpty(iWallWarpLocation))
		{
			string[] array = iWallWarpLocation.Split('|');
			if (array.Length > 1)
			{
				uiInputColumn.text = array[0];
				uiInputRow.text = array[1];
			}
			Warp();
			PrefsManager.instance.iWallWarpLocation = string.Empty;
		}
	}

	private void OnDestroy()
	{
		uiButtonWarp.OnClick -= WarpPressed;
	}

	public void ManualWarp(int x, int y)
	{
		uiInputColumn.text = x.ToString();
		uiInputRow.text = y.ToString();
		PrefsManager.instance.shouldActiveAfterWarp = true;
		Warp();
	}

	public void WarpPressed()
	{
		SoundManager.PlayEventSound(SoundEvent.ButtonsUniversalB);
		PrefsManager.instance.shouldActiveAfterWarp = true;
		Warp();
	}

	public void Warp()
	{
		if (canWarp)
		{
			canWarp = false;
			uiButtonWarp.interactable = false;
			VooDoo();
			int xCoordinate = 0;
			int yCoordinate = 0;
			if (!string.IsNullOrEmpty(uiInputColumn.text))
			{
				xCoordinate = int.Parse(uiInputColumn.text);
			}
			if (!string.IsNullOrEmpty(uiInputRow.text))
			{
				yCoordinate = int.Parse(uiInputRow.text);
			}
			bool shouldActiveAfterWarp = PrefsManager.instance.shouldActiveAfterWarp;
			CanvasStreamingInterface.instance.GoToCoordinates(xCoordinate, yCoordinate, shouldActiveAfterWarp);
			warpTimer = StartCoroutine(WarpTimer());
		}
	}

	private IEnumerator WarpTimer()
	{
		yield return wait;
		canWarp = true;
		uiButtonWarp.interactable = true;
	}

	private void VooDoo()
	{
		rectVooDoo.DOLocalRotate(new Vector3(0f, 0f, 360f), UIAnimationManager.speedSlow, RotateMode.FastBeyond360).SetLoops(3);
	}
}
