using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using Newtonsoft.Json;

[Serializable]
public class UserAccount
{
	public enum Status
	{
		active = 0,
		unverified = 1,
		archived = 2
	}

	public enum EduStatus
	{
		None = 0,
		Current = 1,
		ReleasedInComplete = 2,
		ReleasedComplete = 3
	}

	public static DateTime squareConversionDate = new DateTime(2016, 1, 22, 23, 59, 59);

	public static string statusActive = "active";

	public static string statusUnVerified = "unverified";

	public static string statusArchived = "archived";

	private string _serverID;

	private string _userName;

	private string _email;

	private string _userToken;

	private BloxelBoard _avatar;

	private DateTime _createdDate;

	private int _coins;

	private string _gemBool;

	private string _featuredBool;

	private string _spendBoolv2;

	private Status _status;

	private int _role;

	private string _verifyCode;

	private string _parentEmail;

	private bool _isOfAge;

	private bool _completed;

	private EduStatus _eduAccount;

	private bool _ownsBoard;

	public List<string> publishedSquares;

	public List<string> ownedSquares;

	public List<string> uniqueSquares;

	public List<string> following;

	public List<string> followers;

	public List<string> likes;

	public List<string> downloads;

	public List<string> devices;

	[CompilerGenerated]
	private static Dictionary<string, int> _003C_003Ef__switch_0024map16;

	public string serverID
	{
		get
		{
			return _serverID;
		}
		set
		{
			_serverID = value;
		}
	}

	public string userName
	{
		get
		{
			return _userName;
		}
		set
		{
			_userName = value;
		}
	}

	public string email
	{
		get
		{
			return _email;
		}
		set
		{
			_email = value;
		}
	}

	public string userToken
	{
		get
		{
			return _userToken;
		}
		set
		{
			_userToken = value;
		}
	}

	public BloxelBoard avatar
	{
		get
		{
			return _avatar;
		}
		set
		{
			_avatar = value;
		}
	}

	public DateTime createdDate
	{
		get
		{
			return _createdDate;
		}
		set
		{
			_createdDate = value;
		}
	}

	public int coins
	{
		get
		{
			return _coins;
		}
		set
		{
			_coins = value;
		}
	}

	public string gemBool
	{
		get
		{
			return _gemBool;
		}
		set
		{
			_gemBool = value;
		}
	}

	public string featuredBool
	{
		get
		{
			return _featuredBool;
		}
		set
		{
			_featuredBool = value;
		}
	}

	public string spendBoolv2
	{
		get
		{
			return _spendBoolv2;
		}
		set
		{
			_spendBoolv2 = value;
		}
	}

	public Status status
	{
		get
		{
			return _status;
		}
		set
		{
			_status = value;
		}
	}

	public int role
	{
		get
		{
			return _role;
		}
		set
		{
			_role = value;
		}
	}

	public string verifyCode
	{
		get
		{
			return _verifyCode;
		}
		set
		{
			_verifyCode = value;
		}
	}

	public string parentEmail
	{
		get
		{
			return _parentEmail;
		}
		set
		{
			_parentEmail = value;
		}
	}

	public bool isOfAge
	{
		get
		{
			return _isOfAge;
		}
		set
		{
			_isOfAge = value;
		}
	}

	public bool completed
	{
		get
		{
			return _completed;
		}
		set
		{
			_completed = value;
		}
	}

	public EduStatus eduAccount
	{
		get
		{
			return _eduAccount;
		}
		set
		{
			_eduAccount = value;
		}
	}

	public bool ownsBoard
	{
		get
		{
			return _ownsBoard;
		}
		set
		{
			_ownsBoard = value;
		}
	}

	public UserAccount(string jsonString)
	{
		publishedSquares = new List<string>();
		ownedSquares = new List<string>();
		uniqueSquares = new List<string>();
		following = new List<string>();
		followers = new List<string>();
		likes = new List<string>();
		downloads = new List<string>();
		devices = new List<string>();
		FromJSONString(jsonString);
	}

	public void FromJSONString(string jsonString)
	{
		JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
		FromReader(reader);
		SetUniqueSquares();
	}

	private void FromReader(JsonTextReader reader)
	{
		string text = string.Empty;
		while (reader.Read())
		{
			if (reader.Value != null)
			{
				if (reader.TokenType == JsonToken.PropertyName)
				{
					text = reader.Value.ToString();
					if (text != null && text == "userAvatar")
					{
						ParseAvatar(reader);
					}
				}
				else
				{
					if (text == null)
					{
						continue;
					}
					if (_003C_003Ef__switch_0024map16 == null)
					{
						Dictionary<string, int> dictionary = new Dictionary<string, int>(25);
						dictionary.Add("_id", 0);
						dictionary.Add("userName", 1);
						dictionary.Add("userEmail", 2);
						dictionary.Add("userToken", 3);
						dictionary.Add("loginToken", 4);
						dictionary.Add("createdAt", 5);
						dictionary.Add("publishedSquares", 6);
						dictionary.Add("devices", 7);
						dictionary.Add("squares", 8);
						dictionary.Add("following", 9);
						dictionary.Add("followers", 10);
						dictionary.Add("gemBool", 11);
						dictionary.Add("featuredBool", 12);
						dictionary.Add("spendBoolv2", 13);
						dictionary.Add("coins", 14);
						dictionary.Add("userStatus", 15);
						dictionary.Add("userRole", 16);
						dictionary.Add("likes", 17);
						dictionary.Add("parentEmail", 18);
						dictionary.Add("verifyCode", 19);
						dictionary.Add("isOfAge", 20);
						dictionary.Add("completed", 21);
						dictionary.Add("eduAccount", 22);
						dictionary.Add("ownsBoard", 23);
						dictionary.Add("downloads", 24);
						_003C_003Ef__switch_0024map16 = dictionary;
					}
					int value;
					if (!_003C_003Ef__switch_0024map16.TryGetValue(text, out value))
					{
						continue;
					}
					switch (value)
					{
					case 0:
						_serverID = reader.Value.ToString();
						break;
					case 1:
						userName = reader.Value.ToString();
						break;
					case 2:
						email = reader.Value.ToString();
						break;
					case 3:
						userToken = reader.Value.ToString();
						break;
					case 4:
						userToken = reader.Value.ToString();
						break;
					case 5:
						createdDate = Convert.ToDateTime(reader.Value.ToString());
						break;
					case 6:
						publishedSquares.Add(reader.Value.ToString());
						break;
					case 7:
						devices.Add(reader.Value.ToString());
						break;
					case 8:
						ownedSquares.Add(reader.Value.ToString());
						break;
					case 9:
						following.Add(reader.Value.ToString());
						break;
					case 10:
						followers.Add(reader.Value.ToString());
						break;
					case 11:
						gemBool = reader.Value.ToString();
						break;
					case 12:
						featuredBool = reader.Value.ToString();
						break;
					case 13:
						spendBoolv2 = reader.Value.ToString();
						break;
					case 14:
						coins = int.Parse(reader.Value.ToString());
						break;
					case 15:
					{
						string text2 = reader.Value.ToString();
						if (text2 == "active")
						{
							status = Status.active;
						}
						else if (text2 == "unverified")
						{
							status = Status.unverified;
						}
						else
						{
							status = Status.archived;
						}
						break;
					}
					case 16:
						role = int.Parse(reader.Value.ToString());
						break;
					case 17:
						likes.Add(reader.Value.ToString());
						break;
					case 18:
						parentEmail = reader.Value.ToString();
						break;
					case 19:
						verifyCode = reader.Value.ToString();
						break;
					case 20:
						isOfAge = bool.Parse(reader.Value.ToString());
						break;
					case 21:
						completed = bool.Parse(reader.Value.ToString());
						break;
					case 22:
					{
						bool result = false;
						int result2 = 0;
						if (bool.TryParse(reader.Value.ToString(), out result))
						{
							eduAccount = EduStatus.None;
						}
						else if (int.TryParse(reader.Value.ToString(), out result2))
						{
							eduAccount = (EduStatus)result2;
						}
						break;
					}
					case 23:
						ownsBoard = bool.Parse(reader.Value.ToString());
						break;
					case 24:
						downloads.Add(reader.Value.ToString());
						break;
					}
				}
			}
			else if (reader.TokenType == JsonToken.EndObject)
			{
				break;
			}
		}
	}

	private void ParseAvatar(JsonTextReader reader)
	{
		while (reader.Read())
		{
			if (reader.TokenType == JsonToken.StartObject)
			{
				avatar = new BloxelBoard(reader);
				break;
			}
		}
	}

	private void SetUniqueSquares()
	{
		if (createdDate > squareConversionDate)
		{
			uniqueSquares.AddRange(publishedSquares);
			return;
		}
		for (int i = 0; i < publishedSquares.Count; i++)
		{
			if (!uniqueSquares.Contains(publishedSquares[i]))
			{
				uniqueSquares.Add(publishedSquares[i]);
			}
		}
		for (int j = 0; j < ownedSquares.Count; j++)
		{
			if (!uniqueSquares.Contains(ownedSquares[j]))
			{
				uniqueSquares.Add(ownedSquares[j]);
			}
		}
	}

	public JSONObject Serialize()
	{
		JSONObject jSONObject = new JSONObject();
		JSONObject obj = ListToJSONArray_String(publishedSquares);
		JSONObject obj2 = ListToJSONArray_String(devices);
		JSONObject obj3 = ListToJSONArray_String(ownedSquares);
		JSONObject obj4 = ListToJSONArray_String(following);
		JSONObject obj5 = ListToJSONArray_String(followers);
		JSONObject obj6 = ListToJSONArray_String(likes);
		JSONObject obj7 = ListToJSONArray_String(downloads);
		JSONObject jSONObject2 = null;
		jSONObject2 = ((avatar == null) ? new JSONObject(new BloxelBoard().ToJSON()) : new JSONObject(avatar.ToJSON()));
		jSONObject.AddField("_id", serverID);
		jSONObject.AddField("userName", userName);
		jSONObject.AddField("userEmail", email);
		jSONObject.AddField("userAvatar", jSONObject2);
		jSONObject.AddField("likes", obj6);
		jSONObject.AddField("downloads", obj7);
		jSONObject.AddField("createdAt", createdDate.ToString());
		jSONObject.AddField("publishedSquares", obj);
		jSONObject.AddField("devices", obj2);
		jSONObject.AddField("squares", obj3);
		jSONObject.AddField("following", obj4);
		jSONObject.AddField("followers", obj5);
		jSONObject.AddField("coins", coins);
		jSONObject.AddField("spendBoolv2", spendBoolv2);
		jSONObject.AddField("gemBool", gemBool);
		jSONObject.AddField("featuredBool", featuredBool);
		jSONObject.AddField("userToken", userToken);
		jSONObject.AddField("userStatus", status.ToString());
		jSONObject.AddField("userRole", role);
		jSONObject.AddField("completed", completed);
		jSONObject.AddField("eduAccount", (int)eduAccount);
		jSONObject.AddField("ownsBoard", ownsBoard);
		return jSONObject;
	}

	public string ToJSONString()
	{
		return Serialize().ToString();
	}

	private JSONObject ListToJSONArray_String(List<string> list)
	{
		JSONObject jSONObject = new JSONObject(JSONObject.Type.ARRAY);
		for (int i = 0; i < list.Count; i++)
		{
			jSONObject.Add(list[i]);
		}
		return jSONObject;
	}

	public void PublishedSquare(string squareID)
	{
		if (!publishedSquares.Contains(squareID))
		{
			publishedSquares.Add(squareID);
		}
		if (!ownedSquares.Contains(squareID))
		{
			ownedSquares.Add(squareID);
		}
	}

	public void RemovedSquare(string squareID)
	{
		if (!publishedSquares.Contains(squareID))
		{
			publishedSquares.Remove(squareID);
		}
		if (!ownedSquares.Contains(squareID))
		{
			ownedSquares.Remove(squareID);
		}
	}
}
