using UnityEngine;
using UnityEngine.UI;

public class UserInputIndicator : MonoBehaviour
{
	public static UserInputIndicator instance;

	private CanvasGroup uiGroup;

	private Image indicator;

	private Vector3 deadPosition = new Vector3(-9999f, 0f, 0f);

	private bool _enabled;

	public new bool enabled
	{
		get
		{
			return _enabled;
		}
		set
		{
			_enabled = value;
			if (!_enabled)
			{
				indicator.transform.position = deadPosition;
			}
		}
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		else if (instance != null && instance != this)
		{
			Object.Destroy(base.gameObject);
		}
		uiGroup = GetComponent<CanvasGroup>();
		indicator = GetComponent<Image>();
	}

	private void Start()
	{
		indicator.transform.position = deadPosition;
	}

	private void LateUpdate()
	{
		if (enabled)
		{
			if (Input.touchCount > 0 || Input.GetMouseButton(0))
			{
				indicator.transform.position = PPUtilities.GetPositionInCanvasFromMousePosition(UIOverlayCanvas.instance.canvas, Camera.main);
			}
			else
			{
				indicator.transform.position = deadPosition;
			}
		}
	}
}
