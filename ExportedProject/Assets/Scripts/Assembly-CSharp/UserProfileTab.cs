using DG.Tweening;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class UserProfileTab : MonoBehaviour, IPointerClickHandler, IEventSystemHandler
{
	[Header("| ========= Data ========= |")]
	public UserProfileTabController.TabSection section;

	[Header("| ========= UI ========= |")]
	public Text uiTextTitle;

	public Text uiTextCount;

	public Color colorActive;

	public Color colorInActive;

	public Image uiImageTab;

	public Image uiImageCover;

	public Outline outline;

	[Header("| ========= State Tracking ========= |")]
	public bool isActive;

	public void Activate()
	{
		if (!isActive)
		{
			isActive = true;
			base.transform.SetAsLastSibling();
			uiImageTab.DOColor(colorActive, UIAnimationManager.speedFast).OnStart(delegate
			{
				uiImageCover.enabled = true;
				uiTextTitle.DOColor(colorInActive, UIAnimationManager.speedFast);
				uiTextCount.DOColor(colorInActive, UIAnimationManager.speedFast);
			}).OnComplete(delegate
			{
				outline.enabled = true;
			});
		}
	}

	public void DeActivate()
	{
		if (isActive)
		{
			isActive = false;
			base.transform.SetAsFirstSibling();
			uiImageTab.DOColor(colorInActive, UIAnimationManager.speedFast).OnStart(delegate
			{
				uiImageCover.enabled = false;
				uiTextTitle.DOColor(colorActive, UIAnimationManager.speedFast);
				uiTextCount.DOColor(colorActive, UIAnimationManager.speedFast);
			}).OnComplete(delegate
			{
				outline.enabled = false;
			});
		}
	}

	public void OnPointerClick(PointerEventData eData)
	{
		SoundManager.instance.PlaySound(SoundManager.instance.confirmB);
		UserProfileTabController.instance.SwitchSection(section);
	}
}
