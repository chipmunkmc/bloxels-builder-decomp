using UnityEngine;

public class UserProfileTabController : MonoBehaviour
{
	public enum TabSection
	{
		iWallTiles = 0,
		Following = 1,
		Followers = 2,
		Stats = 3
	}

	public static UserProfileTabController instance;

	public Transform selfTransform;

	public TabSection currentSection;

	public UserProfileTab[] tabs;

	public UserProfileTabPane[] panes;

	private void Awake()
	{
		selfTransform = base.transform;
		if (instance == null)
		{
			instance = this;
		}
		if (tabs == null)
		{
			tabs = GetComponentsInChildren<UserProfileTab>();
		}
		if (panes == null)
		{
			panes = GetComponentsInChildren<UserProfileTabPane>();
		}
	}

	public void SwitchSection(TabSection section)
	{
		for (int i = 0; i < tabs.Length; i++)
		{
			if (tabs[i].section != section)
			{
				tabs[i].DeActivate();
			}
			else
			{
				tabs[i].Activate();
			}
			if (panes[i].section != section)
			{
				panes[i].DeActivate();
			}
			else
			{
				panes[i].Activate();
			}
		}
	}
}
