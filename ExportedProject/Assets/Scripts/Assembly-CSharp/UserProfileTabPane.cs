using DG.Tweening;
using UnityEngine;

public class UserProfileTabPane : MonoBehaviour
{
	[Header("| ========= Data ========= |")]
	public UserProfileTabController.TabSection section;

	[Header("| ========= UI ========= |")]
	public CanvasGroup uiCanvasGroup;

	[Header("| ========= State Tracking ========= |")]
	public bool isActive;

	public void Activate()
	{
		if (!isActive)
		{
			isActive = true;
			uiCanvasGroup.DOFade(1f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				uiCanvasGroup.blocksRaycasts = true;
				uiCanvasGroup.interactable = true;
			});
		}
	}

	public void DeActivate()
	{
		if (isActive)
		{
			isActive = false;
			uiCanvasGroup.DOFade(0f, UIAnimationManager.speedFast).OnComplete(delegate
			{
				uiCanvasGroup.blocksRaycasts = false;
				uiCanvasGroup.interactable = false;
			});
		}
	}
}
