namespace Util
{
	public static class Noise
	{
		private static int[][] grad3;

		private static int[] p;

		private static int[] perm;

		static Noise()
		{
			grad3 = new int[12][]
			{
				new int[3] { 1, 1, 0 },
				new int[3] { -1, 1, 0 },
				new int[3] { 1, -1, 0 },
				new int[3] { -1, -1, 0 },
				new int[3] { 1, 0, 1 },
				new int[3] { -1, 0, 1 },
				new int[3] { 1, 0, -1 },
				new int[3] { -1, 0, -1 },
				new int[3] { 0, 1, 1 },
				new int[3] { 0, -1, 1 },
				new int[3] { 0, 1, -1 },
				new int[3] { 0, -1, -1 }
			};
			p = new int[256]
			{
				151, 160, 137, 91, 90, 15, 131, 13, 201, 95,
				96, 53, 194, 233, 7, 225, 140, 36, 103, 30,
				69, 142, 8, 99, 37, 240, 21, 10, 23, 190,
				6, 148, 247, 120, 234, 75, 0, 26, 197, 62,
				94, 252, 219, 203, 117, 35, 11, 32, 57, 177,
				33, 88, 237, 149, 56, 87, 174, 20, 125, 136,
				171, 168, 68, 175, 74, 165, 71, 134, 139, 48,
				27, 166, 77, 146, 158, 231, 83, 111, 229, 122,
				60, 211, 133, 230, 220, 105, 92, 41, 55, 46,
				245, 40, 244, 102, 143, 54, 65, 25, 63, 161,
				1, 216, 80, 73, 209, 76, 132, 187, 208, 89,
				18, 169, 200, 196, 135, 130, 116, 188, 159, 86,
				164, 100, 109, 198, 173, 186, 3, 64, 52, 217,
				226, 250, 124, 123, 5, 202, 38, 147, 118, 126,
				255, 82, 85, 212, 207, 206, 59, 227, 47, 16,
				58, 17, 182, 189, 28, 42, 223, 183, 170, 213,
				119, 248, 152, 2, 44, 154, 163, 70, 221, 153,
				101, 155, 167, 43, 172, 9, 129, 22, 39, 253,
				19, 98, 108, 110, 79, 113, 224, 232, 178, 185,
				112, 104, 218, 246, 97, 228, 251, 34, 242, 193,
				238, 210, 144, 12, 191, 179, 162, 241, 81, 51,
				145, 235, 249, 14, 239, 107, 49, 192, 214, 31,
				181, 199, 106, 157, 184, 84, 204, 176, 115, 121,
				50, 45, 127, 4, 150, 254, 138, 236, 205, 93,
				222, 114, 67, 29, 24, 72, 243, 141, 128, 195,
				78, 66, 215, 61, 156, 180
			};
			perm = new int[512];
			for (int i = 0; i < 512; i++)
			{
				perm[i] = p[i & 0xFF];
			}
		}

		private static int fastfloor(double x)
		{
			return (!(x > 0.0)) ? ((int)x - 1) : ((int)x);
		}

		private static double dot(int[] g, double x, double y, double z)
		{
			return (double)g[0] * x + (double)g[1] * y + (double)g[2] * z;
		}

		public static float GetNoise(double pX, double pY, double pZ)
		{
			double num = 1.0 / 3.0;
			double num2 = (pX + pY + pZ) * num;
			int num3 = fastfloor(pX + num2);
			int num4 = fastfloor(pY + num2);
			int num5 = fastfloor(pZ + num2);
			double num6 = 1.0 / 6.0;
			double num7 = (double)(num3 + num4 + num5) * num6;
			double num8 = (double)num3 - num7;
			double num9 = (double)num4 - num7;
			double num10 = (double)num5 - num7;
			double num11 = pX - num8;
			double num12 = pY - num9;
			double num13 = pZ - num10;
			int num14;
			int num15;
			int num16;
			int num17;
			int num18;
			int num19;
			if (num11 >= num12)
			{
				if (num12 >= num13)
				{
					num14 = 1;
					num15 = 0;
					num16 = 0;
					num17 = 1;
					num18 = 1;
					num19 = 0;
				}
				else if (num11 >= num13)
				{
					num14 = 1;
					num15 = 0;
					num16 = 0;
					num17 = 1;
					num18 = 0;
					num19 = 1;
				}
				else
				{
					num14 = 0;
					num15 = 0;
					num16 = 1;
					num17 = 1;
					num18 = 0;
					num19 = 1;
				}
			}
			else if (num12 < num13)
			{
				num14 = 0;
				num15 = 0;
				num16 = 1;
				num17 = 0;
				num18 = 1;
				num19 = 1;
			}
			else if (num11 < num13)
			{
				num14 = 0;
				num15 = 1;
				num16 = 0;
				num17 = 0;
				num18 = 1;
				num19 = 1;
			}
			else
			{
				num14 = 0;
				num15 = 1;
				num16 = 0;
				num17 = 1;
				num18 = 1;
				num19 = 0;
			}
			double num20 = num11 - (double)num14 + num6;
			double num21 = num12 - (double)num15 + num6;
			double num22 = num13 - (double)num16 + num6;
			double num23 = num11 - (double)num17 + 2.0 * num6;
			double num24 = num12 - (double)num18 + 2.0 * num6;
			double num25 = num13 - (double)num19 + 2.0 * num6;
			double num26 = num11 - 1.0 + 3.0 * num6;
			double num27 = num12 - 1.0 + 3.0 * num6;
			double num28 = num13 - 1.0 + 3.0 * num6;
			int num29 = num3 & 0xFF;
			int num30 = num4 & 0xFF;
			int num31 = num5 & 0xFF;
			int num32 = perm[num29 + perm[num30 + perm[num31]]] % 12;
			int num33 = perm[num29 + num14 + perm[num30 + num15 + perm[num31 + num16]]] % 12;
			int num34 = perm[num29 + num17 + perm[num30 + num18 + perm[num31 + num19]]] % 12;
			int num35 = perm[num29 + 1 + perm[num30 + 1 + perm[num31 + 1]]] % 12;
			double num36 = 0.6 - num11 * num11 - num12 * num12 - num13 * num13;
			double num37;
			if (num36 < 0.0)
			{
				num37 = 0.0;
			}
			else
			{
				num36 *= num36;
				num37 = num36 * num36 * dot(grad3[num32], num11, num12, num13);
			}
			double num38 = 0.6 - num20 * num20 - num21 * num21 - num22 * num22;
			double num39;
			if (num38 < 0.0)
			{
				num39 = 0.0;
			}
			else
			{
				num38 *= num38;
				num39 = num38 * num38 * dot(grad3[num33], num20, num21, num22);
			}
			double num40 = 0.6 - num23 * num23 - num24 * num24 - num25 * num25;
			double num41;
			if (num40 < 0.0)
			{
				num41 = 0.0;
			}
			else
			{
				num40 *= num40;
				num41 = num40 * num40 * dot(grad3[num34], num23, num24, num25);
			}
			double num42 = 0.6 - num26 * num26 - num27 * num27 - num28 * num28;
			double num43;
			if (num42 < 0.0)
			{
				num43 = 0.0;
			}
			else
			{
				num42 *= num42;
				num43 = num42 * num42 * dot(grad3[num35], num26, num27, num28);
			}
			return (32f * (float)(num37 + num39 + num41 + num43) + 1f) * 0.5f;
		}
	}
}
