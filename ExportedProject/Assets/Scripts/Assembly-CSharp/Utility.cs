using UnityEngine;

public class Utility : MonoBehaviour
{
	public static Utility instance;

	private bool initComplete;

	private void Awake()
	{
		if (!initComplete)
		{
			if (instance != null)
			{
				Object.Destroy(base.gameObject);
				return;
			}
			instance = this;
			Object.DontDestroyOnLoad(base.gameObject);
			initComplete = true;
		}
	}

	public void urlHandler(string urlToHandle)
	{
		string[] array = urlToHandle.Split('/');
		string text = array[2];
		if (text != null && text == "loadlocation")
		{
			loadLocation(array[3]);
		}
	}

	private void loadLocation(string loc)
	{
		loc = loc.Replace("%7C", "|");
		PrefsManager.instance.iWallWarpLocation = loc;
		PrefsManager.instance.shouldActiveAfterWarp = true;
		BloxelsSceneManager.instance.GoTo(SceneName.Viewer);
	}
}
