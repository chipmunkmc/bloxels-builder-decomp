using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))]
public class ValidationMessage : MonoBehaviour
{
	public Text uiTextMessage;

	private void Awake()
	{
		uiTextMessage = GetComponent<Text>();
	}
}
