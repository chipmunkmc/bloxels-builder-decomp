using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Runtime.CompilerServices;
using System.Threading;
using SocketIO;
using UnityEngine;
using VoxelEngine;

public class Viewer : MonoBehaviour
{
	public delegate void HandleZoom(bool zoomed);

	public static Viewer instance;

	[Header("Prefabs")]
	public UnityEngine.Object uploadPrefab;

	public UnityEngine.Object accountPrefab;

	public UnityEngine.Object uploadConfirmPrefab;

	[Header("Data")]
	public CanvasSquare activeGameSquare;

	public UIWarpDrive WarpDrive;

	public UITabGroup tabGroup;

	public BloxelGame currentGame;

	public BloxelAnimation currentAnimation;

	public BloxelMegaBoard currentMegaboard;

	public BloxelCharacter currentCharacter;

	public BloxelBoard selectedBoard;

	[Header("State Tracking")]
	public bool updatingFromServer;

	[Header("Socket")]
	public List<JSONObject> changedSquaresToUpdate = new List<JSONObject>(128);

	public Dictionary<WorldPos2D, JSONObject> squarePositionToBarf = new Dictionary<WorldPos2D, JSONObject>();

	public Dictionary<string, string> serverSquares = new Dictionary<string, string>();

	public SocketIOEvent socketMessage;

	private Stopwatch timer;

	[CompilerGenerated]
	[DebuggerBrowsable(DebuggerBrowsableState.Never)]
	private HandleZoom m_OnZoom;

	private int countMe = 1;

	public event HandleZoom OnZoom
	{
		add
		{
			HandleZoom handleZoom = this.m_OnZoom;
			HandleZoom handleZoom2;
			do
			{
				handleZoom2 = handleZoom;
				handleZoom = Interlocked.CompareExchange(ref this.m_OnZoom, (HandleZoom)Delegate.Combine(handleZoom2, value), handleZoom);
			}
			while (handleZoom != handleZoom2);
		}
		remove
		{
			HandleZoom handleZoom = this.m_OnZoom;
			HandleZoom handleZoom2;
			do
			{
				handleZoom2 = handleZoom;
				handleZoom = Interlocked.CompareExchange(ref this.m_OnZoom, (HandleZoom)Delegate.Remove(handleZoom2, value), handleZoom);
			}
			while (handleZoom != handleZoom2);
		}
	}

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		Resources.UnloadUnusedAssets();
		base.gameObject.AddComponent<ProjectUploadManager>();
		timer = new Stopwatch();
		base.gameObject.AddComponent<DoOnMainThread>();
	}

	private void Start()
	{
		uploadConfirmPrefab = Resources.Load("Prefabs/UIPopupUploadGame");
		AppStateManager.instance.SetCurrentScene(SceneName.Viewer);
		AnalyticsManager.SendEventString(AnalyticsManager.Event.SceneOpen, AppStateManager.instance.currentScene.ToString());
		SoundManager.instance.PlayMusic(SoundManager.instance.canvasMusic);
		UIHelpLink.instance.location = UIHelpLink.Location.iWall;
		BloxelServerInterface.instance.OnSquareUpdate += HandleSquareUpdate;
		BloxelServerInterface.instance.OnCoinsEarned += HandleCoinsEarned;
		BloxelServerInterface.instance.OnUserFollowed += HandleUserFollowed;
		BloxelServerInterface.instance.OnUserUnFollowed += HandleUserUnFollowed;
		BloxelBoardLibrary.instance.OnProjectSelect += HandleOnBoardChange;
		BloxelGameLibrary.instance.OnProjectSelect += HandleOnGameChange;
		BloxelAnimationLibrary.instance.OnProjectSelect += HandleOnAnimationChange;
		BloxelCharacterLibrary.instance.OnProjectSelect += HandleOnCharacterChange;
		BloxelMegaBoardLibrary.instance.OnProjectSelect += HandleOnMegaboardChange;
		if ((bool)BloxelLibrary.instance)
		{
			tabGroup = BloxelLibrary.instance.uiTabGroup;
			UITab[] tabsInGroup = tabGroup.tabsInGroup;
			foreach (UITab uITab in tabsInGroup)
			{
				uITab.MakeAvailable();
			}
			BloxelLibrary.instance.SetPullTabSpriteForInfinityWall();
			BloxelLibrary.instance.Show();
			if (!BloxelLibrary.instance.isVisible)
			{
				BloxelLibrary.instance.gameObject.SetActive(true);
				BloxelLibrary.instance.libraryToggle.Hide();
			}
			if (BloxelLibrary.instance.initComplete)
			{
				BloxelLibrary.instance.SwitchTabWindows(0);
			}
			if (!BloxelLibrary.instance.libraryToggle.isClosed)
			{
				BloxelLibrary.instance.libraryToggle.Toggle();
			}
		}
		if (!BloxelServerInterface.instance.socket.IsConnected)
		{
			BloxelServerInterface.instance.socket.Connect();
		}
		if (CanvasStreamingInterface.instance != null && BloxelServerInterface.instance != null)
		{
			WorldPos2D screenCenter = CanvasStreamingInterface.instance.screenCenter;
			screenCenter.x++;
			BloxelServerInterface.instance.Emit_CoordinateUpdate(screenCenter);
		}
		if (AppStateManager.ActivateFeatured)
		{
			StartCoroutine(DelayedShowFeaturedContent());
		}
		else if (CurrentUser.instance.account == null)
		{
			StartCoroutine(PopupDelay());
		}
		GestureTutorialManager.instance.DetermineIfUserIsReadyToSeePinchInfinityViewTutorial();
	}

	private void OnDestroy()
	{
		BloxelServerInterface.instance.OnSquareUpdate -= HandleSquareUpdate;
		BloxelServerInterface.instance.OnCoinsEarned -= HandleCoinsEarned;
		BloxelServerInterface.instance.OnUserFollowed -= HandleUserFollowed;
		BloxelServerInterface.instance.OnUserUnFollowed -= HandleUserUnFollowed;
		BloxelBoardLibrary.instance.OnProjectSelect -= HandleOnBoardChange;
		BloxelGameLibrary.instance.OnProjectSelect -= HandleOnGameChange;
		BloxelAnimationLibrary.instance.OnProjectSelect -= HandleOnAnimationChange;
		BloxelCharacterLibrary.instance.OnProjectSelect -= HandleOnCharacterChange;
		BloxelMegaBoardLibrary.instance.OnProjectSelect -= HandleOnMegaboardChange;
		Unload();
	}

	private IEnumerator PopupDelay()
	{
		yield return new WaitForSeconds(0.5f);
		GameObject obj = UIOverlayCanvas.instance.Popup(accountPrefab);
	}

	private void Unload()
	{
		instance = null;
		uploadPrefab = null;
		uploadConfirmPrefab = null;
		activeGameSquare = null;
		WarpDrive = null;
		tabGroup = null;
		currentGame = null;
		currentAnimation = null;
		currentMegaboard = null;
		currentCharacter = null;
		selectedBoard = null;
		changedSquaresToUpdate = null;
		squarePositionToBarf = null;
		serverSquares = null;
		socketMessage = null;
	}

	private void HandleOnGameChange(SavedProject item)
	{
		if (AppStateManager.instance.currentScene != SceneName.Viewer || UIPopup.instance != null)
		{
			return;
		}
		if (countMe > 1)
		{
			countMe = 1;
			return;
		}
		countMe++;
		if (activeGameSquare == null)
		{
			return;
		}
		if (item.dataModel.ID() == AssetManager.demoGameID || item.dataModel.ID() == AssetManager.poultryPanicVersion2 || item.dataModel.ID() == AssetManager.poultryPanicID || item.dataModel.ID() == AssetManager.homeMiniGameID)
		{
			activeGameSquare.NoDemoGame();
			return;
		}
		GameObject gameObject = UIOverlayCanvas.instance.Popup(uploadConfirmPrefab);
		UIPopupUploadGame component = gameObject.GetComponent<UIPopupUploadGame>();
		component.onConfirm += delegate
		{
			timer.Reset();
			timer.Start();
			UIPopupUploading loading = ShowUploadingNotification(item);
			loading.progressBar.FakeIncrement();
			SoundManager.PlaySoundClip(SoundClip.publish);
			selectedBoard = item.dataModel.coverBoard;
			currentGame = item.dataModel as BloxelGame;
			ProjectUploadManager.instance.Upload(currentGame, delegate(string serverID, string S3URL)
			{
				if (!string.IsNullOrEmpty(serverID))
				{
					PopulateActiveSquare(item, serverID, S3URL);
				}
				else
				{
					activeGameSquare.ErrorUploading();
				}
				loading.Finished();
				BloxelServerRequests.instance.Reset();
				timer.Stop();
			});
		};
	}

	private void HandleOnBoardChange(SavedProject item)
	{
		if (UIPopup.instance != null || activeGameSquare == null)
		{
			return;
		}
		UIPopupUploading loading = ShowUploadingNotification(item);
		loading.progressBar.FakeIncrement();
		activeGameSquare.uiTextInstructions.text = activeGameSquare.string_loadingText;
		selectedBoard = item.dataModel as BloxelBoard;
		ProjectUploadManager.instance.Upload(selectedBoard, delegate(string serverID, string S3URL)
		{
			if (!string.IsNullOrEmpty(serverID))
			{
				PopulateActiveSquare(item, serverID, S3URL);
			}
			else
			{
				activeGameSquare.ErrorUploading();
			}
			loading.Finished();
			BloxelServerRequests.instance.Reset();
		});
	}

	private void HandleOnCharacterChange(SavedProject item)
	{
		if (UIPopup.instance != null || activeGameSquare == null || ((BloxelCharacter)item.dataModel).animations.Count <= 0)
		{
			return;
		}
		timer.Reset();
		timer.Start();
		UIPopupUploading loading = ShowUploadingNotification(item);
		loading.progressBar.FakeIncrement();
		selectedBoard = item.dataModel.coverBoard;
		currentCharacter = item.dataModel as BloxelCharacter;
		ProjectUploadManager.instance.Upload(currentCharacter, delegate(string serverID, string S3URL)
		{
			if (!string.IsNullOrEmpty(serverID))
			{
				PopulateActiveSquare(item, serverID, S3URL);
			}
			else
			{
				activeGameSquare.ErrorUploading();
			}
			loading.Finished();
			BloxelServerRequests.instance.Reset();
			timer.Stop();
		});
	}

	private void HandleOnMegaboardChange(SavedProject item)
	{
		if (UIPopup.instance != null || activeGameSquare == null || ((BloxelMegaBoard)item.dataModel).boards.Count <= 0)
		{
			return;
		}
		UIPopupUploading loading = ShowUploadingNotification(item);
		loading.progressBar.FakeIncrement();
		selectedBoard = item.dataModel.coverBoard;
		currentMegaboard = item.dataModel as BloxelMegaBoard;
		ProjectUploadManager.instance.Upload(currentMegaboard, delegate(string serverID, string S3URL)
		{
			if (!string.IsNullOrEmpty(serverID))
			{
				PopulateActiveSquare(item, serverID, S3URL);
			}
			else
			{
				activeGameSquare.ErrorUploading();
			}
			loading.Finished();
			BloxelServerRequests.instance.Reset();
		});
	}

	private void HandleOnAnimationChange(SavedProject item)
	{
		if (UIPopup.instance != null || activeGameSquare == null || ((BloxelAnimation)item.dataModel).boards.Count <= 0)
		{
			return;
		}
		UIPopupUploading loading = ShowUploadingNotification(item);
		loading.progressBar.FakeIncrement();
		selectedBoard = item.dataModel.coverBoard;
		currentAnimation = item.dataModel as BloxelAnimation;
		ProjectUploadManager.instance.Upload(currentAnimation, delegate(string serverID, string S3URL)
		{
			if (!string.IsNullOrEmpty(serverID))
			{
				PopulateActiveSquare(item, serverID, S3URL);
			}
			else
			{
				activeGameSquare.ErrorUploading();
			}
			loading.Finished();
			BloxelServerRequests.instance.Reset();
		});
	}

	public void HandleSquareUpdate(SocketIOEvent msg)
	{
		socketMessage = msg;
		ConvertServerBarf();
	}

	public void ConvertServerBarf()
	{
		if (updatingFromServer)
		{
			return;
		}
		updatingFromServer = true;
		if (socketMessage != null && socketMessage.data != null)
		{
			squarePositionToBarf.Clear();
			JSONObject data = socketMessage.data;
			for (int i = 0; i < data["squares"].list.Count; i++)
			{
				int x = (int)data["squares"][i]["coordinate"][0].n;
				int y = (int)data["squares"][i]["coordinate"][1].n;
				WorldPos2D key = new WorldPos2D(x, y);
				squarePositionToBarf[key] = data["squares"][i];
			}
			updatingFromServer = false;
		}
	}

	public void HandleCoinsEarned(SocketIOEvent msg)
	{
		if (CurrentUser.instance.account == null)
		{
			return;
		}
		string str = msg.data["user"]["id"].str;
		int coins = (int)msg.data["coins"]["amount"].n;
		string str2 = msg.data["from"]["id"].str;
		ProjectType projectType = (ProjectType)msg.data["projectInfo"]["type"].n;
		string str3 = msg.data["projectInfo"]["title"].str;
		if (str == CurrentUser.instance.account.serverID)
		{
			PlayerBankManager.instance.AddCoins(coins);
			if (!PlayerBankManager.instance.shouldUpdateServer)
			{
				PlayerBankManager.instance.shouldUpdateServer = true;
			}
		}
	}

	public void HandleUserFollowed(SocketIOEvent msg)
	{
		string[] array = msg.data["info"].str.Split('|');
		if (CurrentUser.instance.account != null && array[0] == CurrentUser.instance.account.serverID && !CurrentUser.instance.account.followers.Contains(array[1]))
		{
			CurrentUser.instance.account.followers.Add(array[1]);
		}
	}

	public void HandleUserUnFollowed(SocketIOEvent msg)
	{
		string[] array = msg.data["info"].str.Split('|');
		if (CurrentUser.instance.account != null && array[0] == CurrentUser.instance.account.serverID && CurrentUser.instance.account.followers.Contains(array[1]))
		{
			CurrentUser.instance.account.followers.Remove(array[1]);
		}
	}

	private UIPopupUploading ShowUploadingNotification(SavedProject item)
	{
		GameObject gameObject = UIOverlayCanvas.instance.Popup(uploadPrefab);
		UIPopupUploading component = gameObject.GetComponent<UIPopupUploading>();
		component.square = activeGameSquare;
		component.projectOnDisk = item;
		return component;
	}

	public void TakeSquare(CanvasSquare square)
	{
		if (activeGameSquare == square)
		{
			LeaveSquare(square);
			BloxelLibrary.instance.libraryToggle.Hide();
			return;
		}
		if (activeGameSquare != null)
		{
			LeaveSquare(activeGameSquare);
		}
		activeGameSquare = square;
		square.uiColorBar.color = ColorUtilities.blockRGB[BlockColor.Yellow];
		square.ShowInstructions();
		BloxelLibrary.instance.libraryToggle.Show();
		serverSquares[square.iWallCoordinate.ToString()] = "PLACEHOLDER";
	}

	public void LeaveSquare(CanvasSquare square)
	{
		if (serverSquares.ContainsKey(square.iWallCoordinate.ToString()))
		{
			serverSquares.Remove(square.iWallCoordinate.ToString());
		}
		activeGameSquare = null;
		square.uiColorBar.color = Color.gray;
		square.HideInstructions();
		square.Reset();
	}

	public void SquareAction(CanvasSquare gameSquare)
	{
		if (gameSquare.taken)
		{
			if (activeGameSquare != null)
			{
				LeaveSquare(activeGameSquare);
			}
			gameSquare.Toggle();
			BloxelLibrary.instance.libraryToggle.Hide();
		}
		else if (!CurrentUser.instance.active)
		{
			UINavMenu.instance.UserAction();
		}
		else
		{
			TakeSquare(gameSquare);
		}
	}

	public void PopulateActiveSquare(SavedProject item, string serverProjectID, string S3URL = null)
	{
		if (!(activeGameSquare == null))
		{
			activeGameSquare.SetNewCoordinates(activeGameSquare.iWallCoordinate);
			if (S3URL == null)
			{
				BloxelServerInterface.instance.Emit_TakeSquare(activeGameSquare.iWallCoordinate.ToString(), BloxelServerRequests.instance.coverBoardS3URL, item.dataModel.type, serverProjectID, item.dataModel.owner);
			}
			else
			{
				BloxelServerInterface.instance.Emit_TakeSquare(activeGameSquare.iWallCoordinate.ToString(), S3URL, item.dataModel.type, serverProjectID, item.dataModel.owner);
			}
			activeGameSquare.uiColorBar.color = BloxelProject.GetColorFromType(item.dataModel.type);
			activeGameSquare.HideInstructions();
			activeGameSquare = null;
		}
	}

	private IEnumerator DelayedShowFeaturedContent()
	{
		yield return new WaitForSeconds(0.5f);
		FeaturedSquaresController.instance.Show();
		AppStateManager.ActivateFeatured = false;
	}
}
