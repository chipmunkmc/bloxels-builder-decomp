using UnityEngine;

namespace VoxelEngine
{
	public class Block2D
	{
		public struct Tile
		{
			public int x;

			public int y;
		}

		public static BlockAir2D EmptyBlock = new BlockAir2D();

		protected static readonly float tileSize = 0.125f;

		protected static Vector2[] uvVertexBuffer = new Vector2[4];

		public GridLocation locationInPalette;

		public virtual bool IsFaceSolid(VoxelDirection direction)
		{
			if (direction == VoxelDirection.north)
			{
				return false;
			}
			return true;
		}

		public virtual void SetBlockData(Chunk2D chunk, int x, int y, MeshData2D meshData)
		{
			if (!chunk.GetBlock(x, y + 1).IsFaceSolid(VoxelDirection.down))
			{
				SetFaceDataUp(x, y, meshData);
			}
			if (!chunk.GetBlock(x, y - 1).IsFaceSolid(VoxelDirection.up))
			{
				SetFaceDataDown(x, y, meshData);
			}
			SetFaceDataSouth(x, y, meshData);
			if (!chunk.GetBlock(x + 1, y).IsFaceSolid(VoxelDirection.west))
			{
				SetFaceDataEast(x, y, meshData);
			}
			if (!chunk.GetBlock(x - 1, y).IsFaceSolid(VoxelDirection.east))
			{
				SetFaceDataWest(x, y, meshData);
			}
		}

		protected virtual void SetFaceDataUp(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, -0.5f));
			FaceUVsNonAlloc(VoxelDirection.up, ref uvVertexBuffer);
			MeshData2D.uv.Add(uvVertexBuffer[0]);
			MeshData2D.uv.Add(uvVertexBuffer[1]);
			MeshData2D.uv.Add(uvVertexBuffer[2]);
			MeshData2D.uv.Add(uvVertexBuffer[3]);
		}

		protected virtual void SetFaceDataDown(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, 0.5f));
			FaceUVsNonAlloc(VoxelDirection.down, ref uvVertexBuffer);
			MeshData2D.uv.Add(uvVertexBuffer[0]);
			MeshData2D.uv.Add(uvVertexBuffer[1]);
			MeshData2D.uv.Add(uvVertexBuffer[2]);
			MeshData2D.uv.Add(uvVertexBuffer[3]);
		}

		protected virtual void SetFaceDataNorth(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, 0.5f));
			FaceUVsNonAlloc(VoxelDirection.north, ref uvVertexBuffer);
			MeshData2D.uv.Add(uvVertexBuffer[0]);
			MeshData2D.uv.Add(uvVertexBuffer[1]);
			MeshData2D.uv.Add(uvVertexBuffer[2]);
			MeshData2D.uv.Add(uvVertexBuffer[3]);
		}

		protected virtual void SetFaceDataEast(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, 0.5f));
			FaceUVsNonAlloc(VoxelDirection.east, ref uvVertexBuffer);
			MeshData2D.uv.Add(uvVertexBuffer[0]);
			MeshData2D.uv.Add(uvVertexBuffer[1]);
			MeshData2D.uv.Add(uvVertexBuffer[2]);
			MeshData2D.uv.Add(uvVertexBuffer[3]);
		}

		protected virtual void SetFaceDataSouth(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, -0.5f));
			FaceUVsNonAlloc(VoxelDirection.south, ref uvVertexBuffer);
			MeshData2D.uv.Add(uvVertexBuffer[0]);
			MeshData2D.uv.Add(uvVertexBuffer[1]);
			MeshData2D.uv.Add(uvVertexBuffer[2]);
			MeshData2D.uv.Add(uvVertexBuffer[3]);
		}

		protected virtual void SetFaceDataWest(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, -0.5f));
			FaceUVsNonAlloc(VoxelDirection.west, ref uvVertexBuffer);
			MeshData2D.uv.Add(uvVertexBuffer[0]);
			MeshData2D.uv.Add(uvVertexBuffer[1]);
			MeshData2D.uv.Add(uvVertexBuffer[2]);
			MeshData2D.uv.Add(uvVertexBuffer[3]);
		}

		public virtual Tile TexturePosition(VoxelDirection direction)
		{
			Tile result = default(Tile);
			result.x = 0;
			result.y = 0;
			return result;
		}

		public virtual Vector2[] FaceUVs(VoxelDirection direction)
		{
			Vector2[] array = new Vector2[4];
			Tile tile = TexturePosition(direction);
			array[0] = new Vector2(tileSize * (float)tile.x + tileSize, tileSize * (float)tile.y);
			array[1] = new Vector2(tileSize * (float)tile.x + tileSize, tileSize * (float)tile.y + tileSize);
			array[2] = new Vector2(tileSize * (float)tile.x, tileSize * (float)tile.y + tileSize);
			array[3] = new Vector2(tileSize * (float)tile.x, tileSize * (float)tile.y);
			return array;
		}

		public virtual void FaceUVsNonAlloc(VoxelDirection direction, ref Vector2[] UVs)
		{
			Tile tile = TexturePosition(direction);
			UVs[0] = new Vector2(tileSize * (float)tile.x + tileSize, tileSize * (float)tile.y);
			UVs[1] = new Vector2(tileSize * (float)tile.x + tileSize, tileSize * (float)tile.y + tileSize);
			UVs[2] = new Vector2(tileSize * (float)tile.x, tileSize * (float)tile.y + tileSize);
			UVs[3] = new Vector2(tileSize * (float)tile.x, tileSize * (float)tile.y);
		}
	}
}
