namespace VoxelEngine
{
	public sealed class BlockAir2D : Block2D
	{
		public override void SetBlockData(Chunk2D chunk, int x, int y, MeshData2D meshData)
		{
		}

		public override bool IsFaceSolid(VoxelDirection direction)
		{
			return false;
		}
	}
}
