using UnityEngine;

namespace VoxelEngine
{
	public sealed class BlockSolidColor2D : Block2D
	{
		public static BlockSolidColor2D DefaultBlock = new BlockSolidColor2D(BlockColor.Red, new GridLocation(0, 0));

		public BlockColor baseBlockColor;

		public static readonly float UVEdgeOffset = 0.01f;

		public bool isBlank;

		public BlockSolidColor2D(BlockColor blockColor, GridLocation texCoords)
		{
			if (blockColor == BlockColor.Blank)
			{
				isBlank = true;
			}
			else
			{
				isBlank = false;
			}
			baseBlockColor = blockColor;
			locationInPalette = texCoords;
		}

		public void Init(BlockColor blockColor, GridLocation texCoords)
		{
			if (blockColor == BlockColor.Blank)
			{
				isBlank = true;
			}
			else
			{
				isBlank = false;
			}
			baseBlockColor = blockColor;
			locationInPalette = texCoords;
		}

		public override Tile TexturePosition(VoxelDirection direction)
		{
			Tile result = default(Tile);
			result.x = locationInPalette.c;
			result.y = locationInPalette.r;
			return result;
		}

		public override bool IsFaceSolid(VoxelDirection direction)
		{
			if (isBlank)
			{
				return false;
			}
			if (direction == VoxelDirection.north)
			{
				return false;
			}
			return true;
		}

		public override void SetBlockData(Chunk2D chunk, int x, int y, MeshData2D meshData)
		{
			if (!isBlank)
			{
				if (!chunk.GetBlock(x, y + 1).IsFaceSolid(VoxelDirection.down))
				{
					SetFaceDataUp(x, y, meshData);
				}
				if (!chunk.GetBlock(x, y - 1).IsFaceSolid(VoxelDirection.up))
				{
					SetFaceDataDown(x, y, meshData);
				}
				SetFaceDataSouth(x, y, meshData);
				if (!chunk.GetBlock(x + 1, y).IsFaceSolid(VoxelDirection.west))
				{
					SetFaceDataEast(x, y, meshData);
				}
				if (!chunk.GetBlock(x - 1, y).IsFaceSolid(VoxelDirection.east))
				{
					SetFaceDataWest(x, y, meshData);
				}
			}
		}

		protected override void SetFaceDataUp(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, -0.5f));
			FaceUVsNonAlloc(meshData);
		}

		protected override void SetFaceDataDown(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, 0.5f));
			FaceUVsNonAlloc(meshData);
		}

		protected override void SetFaceDataNorth(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, 0.5f));
			FaceUVsNonAlloc(meshData);
		}

		protected override void SetFaceDataEast(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, 0.5f));
			FaceUVsNonAlloc(meshData);
		}

		protected override void SetFaceDataSouth(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x + 0.5f, (float)y - 0.5f, -0.5f));
			FaceUVsNonAlloc(meshData);
		}

		protected override void SetFaceDataWest(int x, int y, MeshData2D meshData)
		{
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, 0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y + 0.5f, -0.5f));
			MeshData2D.vertices.Add(new Vector3((float)x - 0.5f, (float)y - 0.5f, -0.5f));
			FaceUVsNonAlloc(meshData);
		}

		public override Vector2[] FaceUVs(VoxelDirection direction)
		{
			return new Vector2[4]
			{
				new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + ColorPalette.tileSize - UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + UVEdgeOffset),
				new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + ColorPalette.tileSize - UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + ColorPalette.tileSize - UVEdgeOffset),
				new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + ColorPalette.tileSize - UVEdgeOffset),
				new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + UVEdgeOffset)
			};
		}

		public override void FaceUVsNonAlloc(VoxelDirection direction, ref Vector2[] UVs)
		{
			UVs[0] = new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + ColorPalette.tileSize - UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + UVEdgeOffset);
			UVs[1] = new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + ColorPalette.tileSize - UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + ColorPalette.tileSize - UVEdgeOffset);
			UVs[2] = new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + ColorPalette.tileSize - UVEdgeOffset);
			UVs[3] = new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + UVEdgeOffset);
		}

		public void FaceUVsNonAlloc(MeshData2D meshData)
		{
			MeshData2D.uv.Add(new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + ColorPalette.tileSize - UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + UVEdgeOffset));
			MeshData2D.uv.Add(new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + ColorPalette.tileSize - UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + ColorPalette.tileSize - UVEdgeOffset));
			MeshData2D.uv.Add(new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + ColorPalette.tileSize - UVEdgeOffset));
			MeshData2D.uv.Add(new Vector2(ColorPalette.tileSize * (float)locationInPalette.c + UVEdgeOffset, ColorPalette.tileSize * (float)locationInPalette.r + UVEdgeOffset));
		}
	}
}
