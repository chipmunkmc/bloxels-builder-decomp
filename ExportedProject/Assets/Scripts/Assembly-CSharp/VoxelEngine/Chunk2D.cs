using UnityEngine;

namespace VoxelEngine
{
	public abstract class Chunk2D : PoolableObject
	{
		public Block2D[] blocks;

		public static int chunkSize = 13;

		public BlockColor[,] blockColors;

		public bool update = true;

		public bool initialized;

		public bool rendered;

		public MeshData2D meshData;

		public WorldPos2D worldPos;

		public int numSolidBlocks;

		public abstract void InitWithBoard(BloxelBoard board, bool generatePolyCollider = true);

		public abstract Block2D GetBlock(int x, int y);

		public abstract Block2D GetBlockLocal(int x, int y);

		public abstract void SetBlock(int x, int y, Block2D block, bool incrementSolidBlockCount = true);

		public override void EarlyAwake()
		{
			spawned = false;
			meshData = new MeshData2D();
			blocks = new Block2D[chunkSize * chunkSize];
			for (int i = 0; i < blocks.Length; i++)
			{
				blocks[i] = new BlockSolidColor2D(BlockColor.Blank, new GridLocation(0, 0));
			}
		}

		public Block2D GetBlockLocalUnsafe(int x, int y)
		{
			return blocks[y * chunkSize + x];
		}

		public static bool InRange(int index)
		{
			if (index < 0 || index >= chunkSize)
			{
				return false;
			}
			return true;
		}

		public Mesh GetChunkMesh(int boardLocalID)
		{
			CachedMesh value = null;
			if (MeshCacheManager.Instance.cachedMeshes.TryGetValue(boardLocalID, out value))
			{
				value.expirationTime = Time.time + 6f;
				return value.sharedMesh;
			}
			meshData.Clear();
			for (int i = 0; i < chunkSize; i++)
			{
				for (int j = 0; j < chunkSize; j++)
				{
					blocks[i * chunkSize + j].SetBlockData(this, j, i, meshData);
				}
			}
			meshData.AddAllQuadTriangles();
			Mesh mesh = new Mesh();
			mesh.SetVertices(MeshData2D.vertices);
			mesh.SetTriangles(MeshData2D.triangles, 0);
			mesh.SetUVs(0, MeshData2D.uv);
			value = new CachedMesh(boardLocalID, mesh);
			PolygonColliderGenerator2D.GenerateColliderForCache(blockColors, value.colliderPaths);
			rendered = true;
			return value.sharedMesh;
		}

		public virtual void UpdateChunk(int boardLocalID, ref MeshFilter primaryFilter, ref PolygonCollider2D primaryCollider, bool colliderGen = true)
		{
			CachedMesh value = null;
			if (MeshCacheManager.Instance.cachedMeshes.TryGetValue(boardLocalID, out value))
			{
				value.expirationTime = Time.time + 6f;
				primaryFilter.sharedMesh = value.sharedMesh;
				if (colliderGen)
				{
					int count = value.colliderPaths.Count;
					primaryCollider.pathCount = count;
					for (int i = 0; i < count; i++)
					{
						primaryCollider.SetPath(i, value.colliderPaths[i]);
					}
				}
				return;
			}
			meshData.Clear();
			for (int j = 0; j < chunkSize; j++)
			{
				for (int k = 0; k < chunkSize; k++)
				{
					blocks[j * chunkSize + k].SetBlockData(this, k, j, meshData);
				}
			}
			meshData.AddAllQuadTriangles();
			Mesh mesh = new Mesh();
			mesh.SetVertices(MeshData2D.vertices);
			mesh.SetTriangles(MeshData2D.triangles, 0);
			mesh.SetUVs(0, MeshData2D.uv);
			value = new CachedMesh(boardLocalID, mesh);
			primaryFilter.sharedMesh = value.sharedMesh;
			PolygonColliderGenerator2D.GenerateColliderForCache(blockColors, value.colliderPaths);
			if (colliderGen)
			{
				int count2 = value.colliderPaths.Count;
				primaryCollider.pathCount = count2;
				for (int l = 0; l < count2; l++)
				{
					primaryCollider.SetPath(l, value.colliderPaths[l]);
				}
			}
			rendered = true;
		}

		public virtual Mesh CreateMeshForPrefab()
		{
			meshData.Clear();
			for (int i = 0; i < chunkSize; i++)
			{
				for (int j = 0; j < chunkSize; j++)
				{
					blocks[i * chunkSize + j].SetBlockData(this, j, i, meshData);
				}
			}
			meshData.AddAllQuadTriangles();
			Mesh mesh = new Mesh();
			mesh.SetVertices(MeshData2D.vertices);
			mesh.SetTriangles(MeshData2D.triangles, 0);
			mesh.SetUVs(0, MeshData2D.uv);
			mesh.RecalculateNormals();
			return mesh;
		}
	}
}
