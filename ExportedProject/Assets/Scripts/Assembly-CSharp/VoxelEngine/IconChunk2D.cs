using System;
using UnityEngine;

namespace VoxelEngine
{
	public sealed class IconChunk2D : Chunk2D
	{
		public Mesh mesh;

		public Icon iconDataModel { get; private set; }

		public override void EarlyAwake()
		{
			spawned = false;
			meshData = new MeshData2D();
		}

		public override void PrepareSpawn()
		{
			spawned = true;
			initialized = false;
			rendered = false;
		}

		public override void PrepareDespawn()
		{
			spawned = false;
		}

		public override void Despawn()
		{
			PrepareDespawn();
			IconChunk2DPool.instance.pool.Push(this);
		}

		public override void InitWithBoard(BloxelBoard board, bool generatePolyCollider = true)
		{
			throw new NotImplementedException();
		}

		public override Block2D GetBlock(int x, int y)
		{
			if (InRangeXY(x, y))
			{
				return blocks[y * iconDataModel.iconDimensionX + x];
			}
			return Block2D.EmptyBlock;
		}

		public override Block2D GetBlockLocal(int x, int y)
		{
			throw new NotImplementedException();
		}

		public override void SetBlock(int x, int y, Block2D block, bool incrementSolidBlockCount = true)
		{
			if (InRangeXY(x, y))
			{
				blocks[y * iconDataModel.iconDimensionX + x] = block;
				update = true;
				if (incrementSolidBlockCount)
				{
					numSolidBlocks++;
				}
			}
		}

		public void InitWithIcon(Icon data)
		{
			iconDataModel = data;
			blocks = new Block2D[iconDataModel.iconDimensionX * iconDataModel.iconDimensionY];
			for (int i = 0; i < blocks.Length; i++)
			{
				blocks[i] = new BlockIcon2D(iconDataModel.paletteCoordinates[i]);
			}
			UpdateChunkMesh();
		}

		public void UpdateBlockAtLocation(int x, int y, GridLocation newPaletteCoordinate)
		{
			int num = y * iconDataModel.iconDimensionX + x;
			iconDataModel.paletteCoordinates[num] = newPaletteCoordinate;
			((BlockIcon2D)blocks[num]).Init(newPaletteCoordinate);
			UpdateChunkMesh();
		}

		public void UpdateChunkMesh()
		{
			meshData.Clear();
			for (int i = 0; i < iconDataModel.iconDimensionY; i++)
			{
				for (int j = 0; j < iconDataModel.iconDimensionX; j++)
				{
					blocks[i * iconDataModel.iconDimensionX + j].SetBlockData(this, j, i, meshData);
				}
			}
			meshData.AddAllQuadTriangles();
			mesh.SetVertices(MeshData2D.vertices);
			mesh.SetTriangles(MeshData2D.triangles, 0);
			mesh.SetUVs(0, MeshData2D.uv);
			rendered = true;
		}

		public Mesh CreateOptimizedMesh()
		{
			Mesh mesh = new Mesh();
			mesh.SetVertices(MeshData2D.vertices);
			mesh.SetTriangles(MeshData2D.triangles, 0);
			mesh.SetUVs(0, MeshData2D.uv);
			mesh.RecalculateBounds();
			mesh.RecalculateNormals();
			return mesh;
		}

		private bool InRangeXY(int x, int y)
		{
			if (x < 0 || x >= iconDataModel.iconDimensionX)
			{
				return false;
			}
			if (y < 0 || y >= iconDataModel.iconDimensionY)
			{
				return false;
			}
			return true;
		}

		public Block2D GetIconBlockUnsafe(int x, int y)
		{
			return blocks[y * iconDataModel.iconDimensionX + x];
		}
	}
}
