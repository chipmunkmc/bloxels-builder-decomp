using System.Collections.Generic;
using UnityEngine;

namespace VoxelEngine
{
	public class MeshData2D
	{
		public static List<Vector3> vertices = new List<Vector3>(1700);

		public static List<int> triangles = new List<int>(1700);

		public static List<Vector2> uv = new List<Vector2>(1700);

		public void Clear()
		{
			vertices.Clear();
			triangles.Clear();
			uv.Clear();
		}

		public void AddAllQuadTriangles()
		{
			int count = vertices.Count;
			for (int i = 4; i <= count; i += 4)
			{
				triangles.Add(i - 4);
				triangles.Add(i - 3);
				triangles.Add(i - 2);
				triangles.Add(i - 4);
				triangles.Add(i - 2);
				triangles.Add(i - 1);
			}
		}
	}
}
