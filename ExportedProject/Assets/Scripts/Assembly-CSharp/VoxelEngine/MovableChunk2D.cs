namespace VoxelEngine
{
	public sealed class MovableChunk2D : Chunk2D
	{
		public override void PrepareSpawn()
		{
			spawned = true;
			initialized = false;
			rendered = false;
		}

		public override void PrepareDespawn()
		{
			spawned = false;
		}

		public override void Despawn()
		{
			PrepareDespawn();
			MovableChunk2DPool.instance.pool.Push(this);
		}

		public override void InitWithBoard(BloxelBoard bloxelBoard, bool generatePolyCollider = true)
		{
			blockColors = bloxelBoard.blockColors;
			numSolidBlocks = 0;
			for (int i = 0; i < Chunk2D.chunkSize; i++)
			{
				for (int j = 0; j < Chunk2D.chunkSize; j++)
				{
					BlockColor blockColor = blockColors[j, i];
					BlockSolidColor2D blockSolidColor2D = blocks[i * Chunk2D.chunkSize + j] as BlockSolidColor2D;
					if (blockColor == BlockColor.Blank)
					{
						blockSolidColor2D.Init(blockColor, new GridLocation(0, 0));
						SetBlock(j, i, blockSolidColor2D, false);
					}
					else
					{
						blockSolidColor2D.Init(blockColor, bloxelBoard.paletteChoiceTexCoordArray[(uint)blockColor]);
						SetBlock(j, i, blockSolidColor2D);
					}
				}
			}
			initialized = true;
		}

		public override void SetBlock(int x, int y, Block2D block, bool incrementSolidBlockCount = true)
		{
			if (Chunk2D.InRange(x) && Chunk2D.InRange(y))
			{
				blocks[y * Chunk2D.chunkSize + x] = block;
				update = true;
				if (incrementSolidBlockCount)
				{
					numSolidBlocks++;
				}
			}
		}

		public override Block2D GetBlock(int x, int y)
		{
			if (Chunk2D.InRange(x) && Chunk2D.InRange(y))
			{
				return blocks[y * Chunk2D.chunkSize + x];
			}
			return Block2D.EmptyBlock;
		}

		public override Block2D GetBlockLocal(int x, int y)
		{
			if (Chunk2D.InRange(x) && Chunk2D.InRange(y))
			{
				return blocks[y * Chunk2D.chunkSize + x];
			}
			return Block2D.EmptyBlock;
		}
	}
}
