namespace VoxelEngine
{
	public enum VoxelDirection
	{
		north = 0,
		east = 1,
		south = 2,
		west = 3,
		up = 4,
		down = 5
	}
}
