using System;
using UnityEngine;

namespace VoxelEngine
{
	public struct WorldPos2D : IEquatable<WorldPos2D>
	{
		public int x;

		public int y;

		public WorldPos2D(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public override int GetHashCode()
		{
			int num = 23;
			num = num * 5881 + x.GetHashCode();
			return num * 6827 + y.GetHashCode();
		}

		public override bool Equals(object obj)
		{
			if (!(obj is WorldPos2D))
			{
				return false;
			}
			return Equals((WorldPos2D)obj);
		}

		public bool Equals(WorldPos2D pos)
		{
			return x == pos.x && y == pos.y;
		}

		public int CompareDistanceToOrigin(WorldPos2D otherPos)
		{
			int num = Mathf.Abs(otherPos.x) + Mathf.Abs(otherPos.y);
			int num2 = Mathf.Abs(x) + Mathf.Abs(y);
			if (num2 < num)
			{
				return -1;
			}
			return 0;
		}

		public string CoordinateConvert()
		{
			return x + "|" + y;
		}

		public override string ToString()
		{
			return x + "|" + y;
		}
	}
}
