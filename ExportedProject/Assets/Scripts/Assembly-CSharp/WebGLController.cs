using System.IO;
using Newtonsoft.Json;
using UnityEngine;

public class WebGLController : MonoBehaviour
{
	public static WebGLController instance;

	public GameHUD hud;

	public GameObject inputController;

	private bool initComplete;

	private bool hasGameLoaded;

	private void Awake()
	{
		if (instance == null)
		{
			instance = this;
		}
		inputController.SetActive(false);
	}

	private void Start()
	{
		AppStateManager.instance.SetCurrentScene(SceneName.WebGLPlayer);
		GameplayController.instance.gameplayActive = true;
		WorldWrapper.instance.Show();
		GameplayController.instance.currentMode = GameplayController.Mode.Gameplay;
		initComplete = true;
		Application.ExternalCall("initComplete", true);
		LoadGame("5696d206c1125a3a66599b08");
	}

	public void UnloadGame()
	{
		GameHUD.instance.Reset(GameHUD.ResetMethod.Full);
		SoundManager.instance.StopMusic();
		LevelBuilder.instance.gameBackground.Reset();
		if (GameplayBuilder.instance.isStreaming)
		{
			GameplayBuilder.instance.Reset();
			GameplayBuilder.instance.StopStreaming();
			GameplayBuilder.instance.isStreaming = false;
		}
		WorldWrapper.instance.ClearRuntimeContainer();
		if (GameplayController.instance.heroObject != null)
		{
			Object.Destroy(GameplayController.instance.heroObject);
		}
		hud.transform.parent.gameObject.SetActive(false);
	}

	public void LoadGame(string projectID)
	{
		if (!initComplete)
		{
			return;
		}
		if (hasGameLoaded)
		{
			UnloadGame();
		}
		StartCoroutine(BloxelServerRequests.instance.GetFullGameFromURL("http://api.bloxelsbuilder.com/api/fast-game/" + projectID, delegate(string jsonString)
		{
			if (!string.IsNullOrEmpty(jsonString))
			{
				JsonTextReader reader = new JsonTextReader(new StringReader(jsonString));
				BloxelGame game = new BloxelGame(reader);
				GameplayController.instance.LoadGame(game);
				BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.None);
				BloxelCamera.instance.SwitchCameraMode(BloxelCamera.CameraMode.Gameplay);
				if (!hud.gameObject.activeInHierarchy)
				{
					hud.transform.parent.gameObject.SetActive(true);
				}
				GameplayBuilder.instance.enabled = true;
				GameplayBuilder.instance.StartStreaming(GameplayController.instance.currentGame);
				GameplayBuilder.instance.InitWorldTileData();
				Application.ExternalCall("gameReady", true);
				hasGameLoaded = true;
			}
		}));
	}
}
