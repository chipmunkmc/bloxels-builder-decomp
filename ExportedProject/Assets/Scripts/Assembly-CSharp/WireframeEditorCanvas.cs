using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using VoxelEngine;

public class WireframeEditorCanvas : MonoBehaviour, IPointerClickHandler, IPointerDownHandler, IPointerUpHandler, IBeginDragHandler, IDragHandler, IEndDragHandler, IEventSystemHandler
{
	public SavedProject currentProject;

	private bool isDragging;

	private int numberOfTiles = 169;

	private bool[] changedSpaces;

	private bool shouldSave;

	private bool _blockIsMoving;

	private Vector2 currentTouchPosition;

	private static int _NoTouchID = -99;

	private int currentTouchID = _NoTouchID;

	public GameObject cubePrefab;

	private TileInfo currentTileInfo;

	private GridLocation currentDropLocation;

	private ConfigIndicator currentConfigIndicator;

	private BloxelsBlock3D currentBlock;

	private Vector3 currentBlockTargetPosition;

	private static float blockWorldOffsetZ = -60f;

	private void Awake()
	{
		changedSpaces = new bool[169];
	}

	private void Start()
	{
		BloxelBoardLibrary.instance.OnProjectSelect += HandleOnBoardChange;
		BloxelAnimationLibrary.instance.OnProjectSelect += HandleOnBoardChange;
	}

	private void Update()
	{
		if (currentBlock != null)
		{
			Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.activeController.cam, new Vector3(currentTouchPosition.x, currentTouchPosition.y, 0f), WorldWrapper.WorldZ + blockWorldOffsetZ);
			currentBlock.transform.localPosition = Vector3.Lerp(currentBlock.transform.localPosition, worldPositionOnXYPlane, Time.deltaTime * 5f);
		}
	}

	private void OnDestroy()
	{
		BloxelBoardLibrary.instance.OnProjectSelect -= HandleOnBoardChange;
		BloxelAnimationLibrary.instance.OnProjectSelect -= HandleOnBoardChange;
	}

	private void HandleOnBoardChange(SavedProject project)
	{
		if (PixelEditorController.instance.mode == CanvasMode.LevelEditor)
		{
			currentProject = project;
		}
	}

	public void OnDisable()
	{
		currentTouchID = _NoTouchID;
		isDragging = false;
		_blockIsMoving = false;
		if (currentBlock != null && currentBlock.gameObject != null)
		{
			UnityEngine.Object.Destroy(currentBlock.gameObject);
		}
	}

	public void OnPointerClick(PointerEventData eventData)
	{
		if (!isDragging)
		{
			if (CheckValidConfigState())
			{
				ConfigItem(eventData.pressPosition);
			}
			if (shouldSave)
			{
				GameBuilderCanvas.instance.currentBloxelLevel.Save(false);
			}
		}
	}

	public void OnPointerDown(PointerEventData eventData)
	{
		if (currentTouchID == _NoTouchID || currentTouchID == eventData.pointerId)
		{
			currentTouchID = eventData.pointerId;
			shouldSave = false;
			Array.Clear(changedSpaces, 0, changedSpaces.Length);
			if (!CheckValidConfigState())
			{
				PaintSelectedBoardIntoWireframe();
			}
		}
	}

	public void OnPointerUp(PointerEventData eventData)
	{
		if (!isDragging && currentTouchID == eventData.pointerId)
		{
			currentTouchID = _NoTouchID;
		}
	}

	public void OnBeginDrag(PointerEventData eventData)
	{
		if (currentTouchID == eventData.pointerId)
		{
			isDragging = true;
			if (CheckValidConfigState())
			{
				currentTouchPosition = new Vector3(eventData.pressPosition.x, eventData.pressPosition.y, 0f);
				PickUpBlockToMove();
			}
		}
	}

	public void OnDrag(PointerEventData eventData)
	{
		if (currentTouchID == eventData.pointerId)
		{
			PaintSelectedBoardIntoWireframe();
			if (_blockIsMoving)
			{
				currentTouchPosition = eventData.position;
				currentDropLocation = GetGridLocationFromScreenPosition(eventData.position);
			}
		}
	}

	public void OnEndDrag(PointerEventData eventData)
	{
		if (currentTouchID == eventData.pointerId)
		{
			currentTouchID = _NoTouchID;
			isDragging = false;
			if (_blockIsMoving)
			{
				DropMovedBlock();
			}
			if (shouldSave)
			{
				GameBuilderCanvas.instance.currentBloxelLevel.Save(false);
			}
		}
	}

	private void PickUpBlockToMove()
	{
		Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(BloxelCamera.instance.activeController.cam, currentTouchPosition, WorldWrapper.WorldZ);
		int num = Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane.x / 13f), 0, 168);
		int num2 = Mathf.Clamp(Mathf.FloorToInt(worldPositionOnXYPlane.y / 13f), 0, 168);
		List<TileInfo> list = GameplayBuilder.instance.worldTileData[num, num2];
		if (list.Count == 0)
		{
			currentTileInfo = null;
			return;
		}
		currentTileInfo = list[0];
		if (currentTileInfo.bloxelLevel.location != GameBuilderCanvas.instance.currentBloxelLevel.location)
		{
			return;
		}
		BloxelTile value = null;
		if (GameplayBuilder.instance.tiles.TryGetValue(currentTileInfo.worldLocation, out value))
		{
			currentConfigIndicator = ConfigIndicatorContainer.ActiveIndicators[currentTileInfo.locationInLevel.c, currentTileInfo.locationInLevel.r];
			if (currentConfigIndicator != null)
			{
				currentConfigIndicator.gameObject.SetActive(false);
			}
			_blockIsMoving = true;
			GameObject gameObject = UnityEngine.Object.Instantiate(cubePrefab);
			currentBlock = gameObject.GetComponent<BloxelsBlock3D>();
			currentBlock.selfTransform.parent = WorldWrapper.instance.runtimeObjectContainer;
			currentBlock.selfTransform.localPosition = worldPositionOnXYPlane;
			BloxelProject projectModelAtLocation = GameBuilderCanvas.instance.currentBloxelLevel.GetProjectModelAtLocation(currentTileInfo.locationInLevel);
			currentBlock.Init(projectModelAtLocation, value.tileRenderer, (int)currentTileInfo.gameBehavior);
			currentBlock.rigidbodyComponent.angularVelocity = UnityEngine.Random.insideUnitSphere * 2.5f;
			currentBlockTargetPosition = currentBlock.transform.localPosition + new Vector3(0f, 0f, blockWorldOffsetZ);
			GameplayBuilder.instance.worldTileData[num, num2].Clear();
			GameplayBuilder.instance.UnloadTile(value);
		}
	}

	private void DropMovedBlock()
	{
		_blockIsMoving = false;
		if (currentBlock != null && currentBlock.gameObject != null)
		{
			UnityEngine.Object.Destroy(currentBlock.gameObject);
		}
		if (currentConfigIndicator != null)
		{
			currentConfigIndicator.gameObject.SetActive(true);
		}
		if (!CheckValidBlockDropLocation(currentDropLocation))
		{
			GameplayBuilder.instance.worldTileData[currentTileInfo.xTileDataIndex, currentTileInfo.yTileDataIndex].Add(currentTileInfo);
		}
		else if (GameBuilderCanvas.instance.currentBloxelLevel.MoveItem(currentTileInfo.locationInLevel, currentDropLocation))
		{
			GameplayBuilder.instance.worldTileData[currentTileInfo.xTileDataIndex, currentTileInfo.yTileDataIndex].Clear();
			GameplayController.GameStateInfo gameStateInfo = GameplayController.gameState[currentTileInfo.xTileDataIndex, currentTileInfo.yTileDataIndex];
			GameplayController.gameState[currentTileInfo.xTileDataIndex, currentTileInfo.yTileDataIndex].Clear();
			currentTileInfo.ResetLocation(currentDropLocation, GameBuilderCanvas.instance.currentBloxelLevel.location);
			if (currentConfigIndicator != null)
			{
				currentConfigIndicator.MoveToLocation(currentDropLocation);
			}
			List<TileInfo> list = GameplayBuilder.instance.worldTileData[currentTileInfo.xTileDataIndex, currentTileInfo.yTileDataIndex];
			list.Clear();
			list.Add(currentTileInfo);
			GameplayController.gameState[currentTileInfo.xTileDataIndex, currentTileInfo.yTileDataIndex] = gameStateInfo;
			shouldSave = true;
		}
	}

	private bool CheckValidBlockDropLocation(GridLocation locationInLevel)
	{
		if (locationInLevel.c < 0 || locationInLevel.c > 12 || locationInLevel.r < 0 || locationInLevel.r > 12 || GameBuilderCanvas.instance.currentBloxelLevel.BlockExistsAtLocation(locationInLevel))
		{
			return false;
		}
		return true;
	}

	private void ConfigItem(Vector2 touchPosition)
	{
		WorldPos2D tileWorldPositionFromScreenPosition = GetTileWorldPositionFromScreenPosition(touchPosition);
		BloxelTile value = null;
		GameplayBuilder.instance.tiles.TryGetValue(tileWorldPositionFromScreenPosition, out value);
		if (!(value == null) && GameBuilderCanvas.instance.currentMode == GameBuilderMode.ConfigBlocks && value.configurable)
		{
			SoundManager.PlayOneShot(SoundManager.instance.confirmB);
			switch (value.tileInfo.wireframeColor)
			{
			case BlockColor.White:
				GameBuilderCanvas.instance.ConfigNPCBlock(value);
				break;
			case BlockColor.Purple:
				GameBuilderCanvas.instance.ConfigEnemy(value);
				break;
			case BlockColor.Pink:
				GameBuilderCanvas.instance.ConfigPowerUp(value);
				break;
			}
		}
	}

	private void PaintSelectedBoardIntoWireframe()
	{
		if (!CheckValidPaintState())
		{
			return;
		}
		GridLocation gridLocationFromScreenPosition = GetGridLocationFromScreenPosition();
		if (CheckValidPaintLocation(gridLocationFromScreenPosition))
		{
			SoundManager.PlayOneShot(SoundManager.instance.popC);
			switch (currentProject.dataModel.type)
			{
			case ProjectType.Board:
				GameBuilderCanvas.instance.currentBloxelLevel.SetBoardAtLocation(gridLocationFromScreenPosition, currentProject.dataModel as BloxelBoard, false);
				break;
			case ProjectType.Animation:
				GameBuilderCanvas.instance.currentBloxelLevel.SetAnimationAtLocation(gridLocationFromScreenPosition, currentProject.dataModel as BloxelAnimation, false);
				break;
			}
			GameplayBuilder.instance.UpdateTileAtLocation(gridLocationFromScreenPosition, currentProject.dataModel);
			shouldSave = true;
		}
	}

	private bool CheckValidPaintState()
	{
		if (currentProject != null && GameBuilderCanvas.instance.currentMode == GameBuilderMode.PaintArt)
		{
			return true;
		}
		return false;
	}

	private bool CheckValidConfigState()
	{
		if (GameBuilderCanvas.instance.currentMode == GameBuilderMode.ConfigBlocks)
		{
			return true;
		}
		return false;
	}

	private bool CheckValidPaintLocation(GridLocation locationInLevel)
	{
		if (locationInLevel.c < 0 || locationInLevel.c > 12 || locationInLevel.r < 0 || locationInLevel.r > 12)
		{
			return false;
		}
		int num = locationInLevel.r * 13 + locationInLevel.c;
		if (changedSpaces[num])
		{
			return false;
		}
		if (!GameBuilderCanvas.instance.currentBloxelLevel.BlockExistsAtLocation(locationInLevel))
		{
			return false;
		}
		changedSpaces[num] = true;
		return true;
	}

	public static GridLocation GetGridLocationFromScreenPosition()
	{
		Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(Camera.main, Input.mousePosition, WorldWrapper.WorldZ);
		int col = Mathf.FloorToInt((worldPositionOnXYPlane.x - (float)(GameBuilderCanvas.instance.currentBloxelLevel.location.c * 169)) / 13f);
		int row = Mathf.FloorToInt((worldPositionOnXYPlane.y - (float)(GameBuilderCanvas.instance.currentBloxelLevel.location.r * 169)) / 13f);
		return new GridLocation(col, row);
	}

	public static GridLocation GetGridLocationFromScreenPosition(Vector2 touchPosition)
	{
		Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(Camera.main, new Vector3(touchPosition.x, touchPosition.y, 0f), WorldWrapper.WorldZ);
		int col = Mathf.FloorToInt((worldPositionOnXYPlane.x - (float)(GameBuilderCanvas.instance.currentBloxelLevel.location.c * 169)) / 13f);
		int row = Mathf.FloorToInt((worldPositionOnXYPlane.y - (float)(GameBuilderCanvas.instance.currentBloxelLevel.location.r * 169)) / 13f);
		return new GridLocation(col, row);
	}

	public static WorldPos2D GetTileWorldPositionFromScreenPosition()
	{
		Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(Camera.main, Input.mousePosition, WorldWrapper.WorldZ);
		int value = Mathf.FloorToInt(worldPositionOnXYPlane.x / 13f) * 13;
		int value2 = Mathf.FloorToInt(worldPositionOnXYPlane.y / 13f) * 13;
		value = Mathf.Clamp(value, 0, 2196);
		value2 = Mathf.Clamp(value2, 0, 2196);
		return new WorldPos2D(value, value2);
	}

	public static WorldPos2D GetTileWorldPositionFromScreenPosition(Vector2 touchPosition)
	{
		Vector3 worldPositionOnXYPlane = PPUtilities.GetWorldPositionOnXYPlane(Camera.main, new Vector3(touchPosition.x, touchPosition.y, 0f), WorldWrapper.WorldZ);
		int value = Mathf.FloorToInt(worldPositionOnXYPlane.x / 13f) * 13;
		int value2 = Mathf.FloorToInt(worldPositionOnXYPlane.y / 13f) * 13;
		value = Mathf.Clamp(value, 0, 2196);
		value2 = Mathf.Clamp(value2, 0, 2196);
		return new WorldPos2D(value, value2);
	}
}
