using System;

public struct WorldLocation
{
	public Guid id;

	public GridLocation locationInBoard;

	public GridLocation levelLocationInWorld;

	public static Guid INVALID_ID = new Guid("00000000-0000-0000-0000-000000000000");

	public WorldLocation(Guid _levelID, GridLocation _locationInBoard, GridLocation _levelLocationInWorld)
	{
		id = _levelID;
		locationInBoard = _locationInBoard;
		levelLocationInWorld = _levelLocationInWorld;
	}

	public static WorldLocation InvalidLocation()
	{
		return new WorldLocation(INVALID_ID, GridLocation.InvalidLocation, GridLocation.InvalidLocation);
	}

	public static bool operator ==(WorldLocation left, WorldLocation right)
	{
		return left.id == right.id && left.locationInBoard == right.locationInBoard;
	}

	public static bool operator !=(WorldLocation left, WorldLocation right)
	{
		return left.id != right.id || left.locationInBoard != right.locationInBoard;
	}

	public override string ToString()
	{
		return string.Concat("WorldLocation - [Level: ", id, ", board column: ", locationInBoard.c, ", board row: ", locationInBoard.r, ", level column: ", levelLocationInWorld.c, ", level row: ", levelLocationInWorld.r, "]");
	}
}
