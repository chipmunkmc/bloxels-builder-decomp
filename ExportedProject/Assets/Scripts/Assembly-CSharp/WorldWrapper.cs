using System.Collections.Generic;
using UnityEngine;

public class WorldWrapper : MonoBehaviour
{
	public static WorldWrapper instance;

	public static float WorldZ = 100f;

	public Transform selfTransform;

	public Transform characterPlacementGrid;

	public Transform currentLevelBounds;

	public Transform runtimeObjectContainer;

	public Transform configIndicatorContainer;

	public Transform effectsContainer;

	private static List<PoolableComponent> poolableRuntimeObjects = new List<PoolableComponent>(32);

	private void Awake()
	{
		if (instance != null)
		{
			Object.Destroy(base.gameObject);
			return;
		}
		instance = this;
		selfTransform = GetComponent<Transform>();
		selfTransform.position = new Vector3(26f, 20f, WorldZ);
		base.gameObject.SetActive(false);
	}

	private void OnEnable()
	{
		if (GameplayBuilder.instance != null)
		{
			GameplayBuilder.instance.ResetAnimations();
		}
	}

	public void ClearRuntimeContainer()
	{
		GameplayController.EnemiesToKill.Clear();
		if (runtimeObjectContainer.childCount == 0)
		{
			UnityEngine.Debug.LogError("Noting to clear from runtime container.");
			return;
		}
		poolableRuntimeObjects.Clear();
		runtimeObjectContainer.GetComponentsInChildren(true, poolableRuntimeObjects);
		for (int i = 0; i < poolableRuntimeObjects.Count; i++)
		{
			PoolableComponent poolableComponent = poolableRuntimeObjects[i];
			poolableComponent.Despawn();
		}
		UnityEngine.Debug.LogError("Cleared " + poolableRuntimeObjects.Count + " recyclable objects from runtime container. Destroying " + runtimeObjectContainer.childCount + " others that are remaining.");
		foreach (Transform item in runtimeObjectContainer)
		{
			UnityEngine.Debug.LogError("Destroying : " + item.gameObject.name);
			item.gameObject.Recycle();
		}
	}

	public void Clear()
	{
		ClearRuntimeContainer();
		GameplayBuilder.instance.UnloadGameplay();
	}

	public void Show()
	{
		if (!base.gameObject.activeInHierarchy)
		{
			base.gameObject.SetActive(true);
		}
	}

	public void Hide()
	{
		if (base.gameObject.activeInHierarchy)
		{
			base.gameObject.SetActive(false);
		}
	}

	public void MoveLevelBounds()
	{
		if (!currentLevelBounds.gameObject.activeInHierarchy)
		{
			currentLevelBounds.gameObject.SetActive(true);
		}
		currentLevelBounds.localPosition = new Vector3((float)(GameBuilderCanvas.instance.currentBloxelLevel.location.c * 169) + 84.5f - 0.5f, (float)(GameBuilderCanvas.instance.currentBloxelLevel.location.r * 169) + 84.5f - 0.5f, -10f);
	}

	public void MoveCharacterPlacementGrid()
	{
		characterPlacementGrid.localPosition = new Vector3((float)(GameBuilderCanvas.instance.currentBloxelLevel.location.c * 169) + 84.5f - 0.5f, (float)(GameBuilderCanvas.instance.currentBloxelLevel.location.r * 169) + 84.5f - 0.5f, characterPlacementGrid.localPosition.z);
	}
}
