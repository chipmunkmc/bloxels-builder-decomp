using System;
using System.Collections.Generic;
using System.IO;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Zip;
using UnityEngine;

public class Zipper : MonoBehaviour
{
	public static List<string> unZippedFiles = new List<string>();

	public static void ZipFolder(string root, string currentFolder, ZipOutputStream zStream)
	{
		string[] directories = Directory.GetDirectories(currentFolder);
		string[] array = directories;
		foreach (string currentFolder2 in array)
		{
			ZipFolder(root, currentFolder2, zStream);
		}
		string text = currentFolder.Substring(root.Length) + "/";
		if (text.Length > 1)
		{
			ZipEntry zipEntry = new ZipEntry(text);
			zipEntry.DateTime = DateTime.Now;
		}
		string[] files = Directory.GetFiles(currentFolder);
		foreach (string file in files)
		{
			AddFileToZip(zStream, text, file);
		}
	}

	private static void AddFileToZip(ZipOutputStream zStream, string relativePath, string file)
	{
		byte[] array = new byte[4096];
		string text = ((relativePath.Length <= 1) ? string.Empty : relativePath) + Path.GetFileName(file);
		ZipEntry zipEntry = new ZipEntry(text);
		zipEntry.DateTime = DateTime.Now;
		zStream.PutNextEntry(zipEntry);
		using (FileStream fileStream = File.OpenRead(file))
		{
			int num;
			do
			{
				num = fileStream.Read(array, 0, array.Length);
				zStream.Write(array, 0, num);
			}
			while (num > 0);
		}
	}

	public static byte[] GetZip(string[] files)
	{
		using (MemoryStream memoryStream = new MemoryStream())
		{
			using (ZipOutputStream zipOutputStream = new ZipOutputStream(memoryStream))
			{
				zipOutputStream.SetLevel(0);
				byte[] array = null;
				foreach (string path in files)
				{
					ZipEntry zipEntry = new ZipEntry(Path.GetFileName(path));
					zipEntry.DateTime = DateTime.Now;
					zipOutputStream.UseZip64 = UseZip64.Off;
					zipOutputStream.PutNextEntry(zipEntry);
					FileStream fileStream = File.OpenRead(path);
					byte[] array2 = new byte[4096];
					int num;
					do
					{
						num = fileStream.Read(array2, 0, array2.Length);
						zipOutputStream.Write(array2, 0, num);
					}
					while (num > 0);
					fileStream.Close();
				}
				zipOutputStream.CloseEntry();
				zipOutputStream.IsStreamOwner = false;
				zipOutputStream.Close();
				return memoryStream.ToArray();
			}
		}
	}

	public static string CreateBase64Stream(string[] files)
	{
		using (MemoryStream memoryStream = new MemoryStream())
		{
			using (ZipOutputStream zipOutputStream = new ZipOutputStream(memoryStream))
			{
				zipOutputStream.SetLevel(0);
				byte[] array = null;
				foreach (string path in files)
				{
					ZipEntry zipEntry = new ZipEntry(Path.GetFileName(path));
					zipEntry.DateTime = DateTime.Now;
					zipOutputStream.UseZip64 = UseZip64.Off;
					zipOutputStream.PutNextEntry(zipEntry);
					FileStream fileStream = File.OpenRead(path);
					byte[] array2 = new byte[4096];
					int num;
					do
					{
						num = fileStream.Read(array2, 0, array2.Length);
						zipOutputStream.Write(array2, 0, num);
					}
					while (num > 0);
					fileStream.Close();
				}
				zipOutputStream.CloseEntry();
				zipOutputStream.IsStreamOwner = false;
				zipOutputStream.Close();
				byte[] inArray = memoryStream.ToArray();
				return Convert.ToBase64String(inArray);
			}
		}
	}

	public static string CreateString(string[] files, string intendedFilename)
	{
		using (ZipOutputStream zipOutputStream = new ZipOutputStream(File.Create(intendedFilename)))
		{
			zipOutputStream.SetLevel(0);
			byte[] array = new byte[4096];
			for (int i = 0; i < files.Length; i++)
			{
				ZipEntry zipEntry = new ZipEntry(Path.GetFileName(files[i]));
				zipEntry.DateTime = DateTime.Now;
				zipOutputStream.PutNextEntry(zipEntry);
				using (FileStream fileStream = File.OpenRead(files[i]))
				{
					int num;
					do
					{
						num = fileStream.Read(array, 0, array.Length);
						zipOutputStream.Write(array, 0, num);
					}
					while (num > 0);
				}
			}
			zipOutputStream.Finish();
			zipOutputStream.Close();
			return intendedFilename;
		}
	}

	public static void Create(string[] files, string[] extentions, string intendedFilename)
	{
		using (ZipOutputStream zipOutputStream = new ZipOutputStream(File.Create(intendedFilename)))
		{
			zipOutputStream.SetLevel(5);
			byte[] array = new byte[4096];
			for (int i = 0; i < files.Length; i++)
			{
				ZipEntry zipEntry = new ZipEntry(Path.GetFileName(files[i] + "|" + extentions[i]));
				zipEntry.DateTime = DateTime.Now;
				zipOutputStream.PutNextEntry(zipEntry);
				if (files[i] == null)
				{
				}
				using (FileStream fileStream = File.OpenRead(files[i]))
				{
					int num;
					do
					{
						num = fileStream.Read(array, 0, array.Length);
						zipOutputStream.Write(array, 0, num);
					}
					while (num > 0);
				}
			}
			zipOutputStream.Finish();
			zipOutputStream.Close();
		}
	}

	public static byte[] CreateBytes(string[] files, string[] extentions)
	{
		using (MemoryStream memoryStream = new MemoryStream())
		{
			using (ZipOutputStream zipOutputStream = new ZipOutputStream(memoryStream))
			{
				zipOutputStream.SetLevel(0);
				byte[] array = new byte[4096];
				for (int i = 0; i < files.Length; i++)
				{
					ZipEntry zipEntry = new ZipEntry(Path.GetFileName(files[i] + "|" + extentions[i]));
					zipEntry.DateTime = DateTime.Now;
					zipOutputStream.UseZip64 = UseZip64.Off;
					zipOutputStream.PutNextEntry(zipEntry);
					if (files[i] == null)
					{
					}
					FileStream fileStream = File.OpenRead(files[i]);
					byte[] array2 = new byte[4096];
					int num;
					do
					{
						num = fileStream.Read(array2, 0, array2.Length);
						zipOutputStream.Write(array2, 0, num);
					}
					while (num > 0);
					fileStream.Close();
				}
				zipOutputStream.CloseEntry();
				zipOutputStream.IsStreamOwner = false;
				zipOutputStream.Close();
				return memoryStream.ToArray();
			}
		}
	}

	public static void Extract(string pathToZipFile, string outFolder)
	{
		ZipFile zipFile = null;
		try
		{
			FileStream file = File.OpenRead(pathToZipFile);
			zipFile = new ZipFile(file);
			foreach (ZipEntry item in zipFile)
			{
				if (item.IsFile)
				{
					string path = item.Name;
					byte[] buffer = new byte[4096];
					Stream inputStream = zipFile.GetInputStream(item);
					string path2 = Path.Combine(outFolder, path);
					string directoryName = Path.GetDirectoryName(path2);
					if (directoryName.Length > 0)
					{
						Directory.CreateDirectory(directoryName);
					}
					using (FileStream destination = File.Create(path2))
					{
						StreamUtils.Copy(inputStream, destination, buffer);
					}
				}
			}
		}
		finally
		{
			if (zipFile != null)
			{
				zipFile.IsStreamOwner = true;
				zipFile.Close();
			}
		}
	}

	public static void ExtractFromStream(byte[] data, string outFolder, bool shouldCheckForIsFile = true)
	{
		ZipFile zipFile = null;
		unZippedFiles = new List<string>();
		bool flag = false;
		try
		{
			MemoryStream stream = new MemoryStream(data);
			zipFile = new ZipFile(stream);
			foreach (ZipEntry item in zipFile)
			{
				if (!shouldCheckForIsFile || item.IsFile)
				{
					string path = item.Name;
					unZippedFiles.Add(Path.Combine(outFolder, item.Name));
					byte[] buffer = new byte[4096];
					Stream inputStream = zipFile.GetInputStream(item);
					string path2 = Path.Combine(outFolder, path);
					string directoryName = Path.GetDirectoryName(path2);
					if (directoryName.Length > 0)
					{
						Directory.CreateDirectory(directoryName);
					}
					using (FileStream destination = File.Create(path2))
					{
						StreamUtils.Copy(inputStream, destination, buffer);
					}
				}
			}
		}
		catch (Exception)
		{
		}
		finally
		{
			if (zipFile != null)
			{
				zipFile.IsStreamOwner = true;
				zipFile.Close();
				flag = true;
			}
		}
	}

	public static string ZipStreamToString(byte[] data)
	{
		ZipFile zipFile = null;
		string result = null;
		try
		{
			MemoryStream stream = new MemoryStream(data);
			zipFile = new ZipFile(stream);
			foreach (ZipEntry item in zipFile)
			{
				string text = item.Name;
				Stream inputStream = zipFile.GetInputStream(item);
				StreamReader streamReader = new StreamReader(inputStream);
				result = streamReader.ReadToEnd();
			}
		}
		catch (Exception)
		{
		}
		finally
		{
			if (zipFile != null)
			{
				zipFile.IsStreamOwner = true;
				zipFile.Close();
			}
		}
		return result;
	}
}
