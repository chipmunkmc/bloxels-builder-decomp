using System;
using System.Collections.Generic;
using UnityEngine;

namespace Crosstales.BWF
{
	public static class CTExtensionMethods
	{
		private static readonly System.Random rd = new System.Random();

		public static void CTAddRange<T, S>(this Dictionary<T, S> source, Dictionary<T, S> collection)
		{
			if (collection == null)
			{
				throw new ArgumentNullException("collection");
			}
			foreach (KeyValuePair<T, S> item in collection)
			{
				if (!source.ContainsKey(item.Key))
				{
					source.Add(item.Key, item.Value);
				}
				else
				{
					Debug.LogWarning("Duplicate key found: " + item.Key);
				}
			}
		}

		public static bool CTEquals(this string str, string toCheck, StringComparison comp = StringComparison.OrdinalIgnoreCase)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			if (toCheck == null)
			{
				throw new ArgumentNullException("toCheck");
			}
			return str.Equals(toCheck, comp);
		}

		public static bool CTContains(this string str, string toCheck, StringComparison comp = StringComparison.OrdinalIgnoreCase)
		{
			if (str == null)
			{
				throw new ArgumentNullException("str");
			}
			if (toCheck == null)
			{
				throw new ArgumentNullException("toCheck");
			}
			return str.IndexOf(toCheck, comp) >= 0;
		}
	}
}
