using System;
using System.Collections.Generic;
using Crosstales.BWF.Filter;
using Crosstales.BWF.Model;
using Crosstales.BWF.Provider;
using Crosstales.BWF.Util;
using UnityEngine;

namespace Crosstales.BWF.Manager
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_manager_1_1_bad_word_manager.html")]
	[DisallowMultipleComponent]
	public class BadWordManager : BaseManager
	{
		[Header("Bad Word Provider")]
		[Tooltip("List of all left-to-right providers.")]
		public List<BadWordProvider> BadWordProviderLTR;

		[Tooltip("List of all right-to-left providers.")]
		public List<BadWordProvider> BadWordProviderRTL;

		[Tooltip("Replace characters for bad words (default: *).")]
		[Header("Settings")]
		public string ReplaceChars = "*";

		[Tooltip("Defines how exact the match will be. Without fuzziness, only exact matches are detected. Important: “Fuzzy” is much more performance consuming – so be careful (default: off).")]
		public bool Fuzzy;

		private GameObject root;

		private static bool initalized;

		private static BadWordFilter filter;

		private static BadWordManager manager;

		private static bool loggedFilterIsNull;

		private static bool loggedOnlyOneInstance;

		private const string clazz = "BadWordManager";

		public static BadWordFilter Filter
		{
			get
			{
				return filter;
			}
		}

		public static bool isReady
		{
			get
			{
				bool result = false;
				if (filter != null)
				{
					result = filter.isReady;
				}
				else
				{
					logFilterIsNull("BadWordManager");
				}
				return result;
			}
		}

		public static List<Source> Sources
		{
			get
			{
				List<Source> result = new List<Source>();
				if (filter != null)
				{
					result = filter.Sources;
				}
				else
				{
					logFilterIsNull("BadWordManager");
				}
				return result;
			}
		}

		public void OnEnable()
		{
			if (Helper.isEditorMode || !initalized)
			{
				manager = this;
				Load();
				root = base.transform.root.gameObject;
				if (!Helper.isEditorMode && Config.DONT_DESTROY_ON_LOAD)
				{
					UnityEngine.Object.DontDestroyOnLoad(root);
					initalized = true;
				}
			}
			else if (!Helper.isEditorMode && Config.DONT_DESTROY_ON_LOAD)
			{
				if (!loggedOnlyOneInstance)
				{
					loggedOnlyOneInstance = true;
					Debug.LogWarning("Only one active instance of 'BadWordManager' allowed in all scenes!" + Environment.NewLine + "This object will now be destroyed.");
				}
				UnityEngine.Object.Destroy(root, 0.2f);
			}
		}

		public static void Load()
		{
			if (manager != null)
			{
				filter = new BadWordFilter(manager.BadWordProviderLTR, manager.BadWordProviderRTL, manager.ReplaceChars, manager.Fuzzy, manager.MarkPrefix, manager.MarkPostfix);
			}
		}

		public static bool Contains(string testString, params string[] sources)
		{
			bool result = false;
			if (filter != null)
			{
				result = filter.Contains(testString, sources);
			}
			else
			{
				logFilterIsNull("BadWordManager");
			}
			return result;
		}

		public static void ContainsMT(out bool result, string testString, params string[] sources)
		{
			result = Contains(testString, sources);
		}

		public static List<string> GetAll(string testString, params string[] sources)
		{
			List<string> result = new List<string>();
			if (filter != null)
			{
				result = filter.GetAll(testString, sources);
			}
			else
			{
				logFilterIsNull("BadWordManager");
			}
			return result;
		}

		public static void GetAllMT(out List<string> result, string testString, params string[] sources)
		{
			result = GetAll(testString, sources);
		}

		public static string ReplaceAll(string testString, params string[] sources)
		{
			string result = testString;
			if (filter != null)
			{
				result = filter.ReplaceAll(testString, sources);
			}
			else
			{
				logFilterIsNull("BadWordManager");
			}
			return result;
		}

		public static void ReplaceAllMT(out string result, string testString, params string[] sources)
		{
			result = ReplaceAll(testString, sources);
		}

		public static string Replace(string text, List<string> badWords)
		{
			string result = text;
			if (filter != null)
			{
				result = filter.Replace(text, badWords);
			}
			else
			{
				logFilterIsNull("BadWordManager");
			}
			return result;
		}

		public static string Mark(string text, List<string> badWords, string prefix = "<b><color=red>", string postfix = "</color></b>")
		{
			string result = text;
			if (filter != null)
			{
				result = filter.Mark(text, badWords, prefix, postfix);
			}
			else
			{
				logFilterIsNull("BadWordManager");
			}
			return result;
		}

		public static string Unmark(string text, string prefix = "<b><color=red>", string postfix = "</color></b>")
		{
			string result = text;
			if (filter != null)
			{
				result = filter.Unmark(text, prefix, postfix);
			}
			else
			{
				logFilterIsNull("BadWordManager");
			}
			return result;
		}

		private static void logFilterIsNull(string clazz)
		{
			if (!loggedFilterIsNull)
			{
				Debug.LogWarning("'filter' is null!" + Environment.NewLine + "Did you add the '" + clazz + "' to the current scene?");
				loggedFilterIsNull = true;
			}
		}
	}
}
