using System;
using System.Collections;
using System.Collections.Generic;
using Crosstales.BWF.Model;
using Crosstales.BWF.Util;
using UnityEngine;

namespace Crosstales.BWF.Provider
{
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_provider_1_1_bad_word_provider_text.html")]
	public class BadWordProviderText : BadWordProvider
	{
		public override void Load()
		{
			base.Load();
			if (Sources == null)
			{
				return;
			}
			loading = true;
			Source[] sources;
			if (Helper.isEditorMode)
			{
				sources = Sources;
				foreach (Source source in sources)
				{
					if (source.Resource != null)
					{
						loadResourceInEditor(source);
					}
					if (!string.IsNullOrEmpty(source.URL))
					{
						loadWebInEditor(source);
					}
				}
				init();
				return;
			}
			sources = Sources;
			foreach (Source source2 in sources)
			{
				if (source2.Resource != null)
				{
					StartCoroutine(loadResource(source2));
				}
				if (!string.IsNullOrEmpty(source2.URL))
				{
					StartCoroutine(loadWeb(source2));
				}
			}
		}

		public override void Save()
		{
			Debug.LogWarning("Save not implemented!");
		}

		private IEnumerator loadWeb(Source src)
		{
			string uid = Guid.NewGuid().ToString();
			coRoutines.Add(uid);
			if (!string.IsNullOrEmpty(src.URL))
			{
				using (WWW www = new WWW(src.URL.Trim()))
				{
					do
					{
						yield return www;
					}
					while (!www.isDone);
					if (string.IsNullOrEmpty(www.error) && !string.IsNullOrEmpty(www.text))
					{
						List<string> list = Helper.SplitStringToLines(www.text);
						yield return null;
						if (list.Count > 0)
						{
							badwords.Add(new BadWords(src, list));
						}
						else
						{
							Debug.LogWarning("Source: '" + src.URL + "' does not contain any active bad words!");
						}
					}
					else
					{
						Debug.LogWarning("Could not load source: '" + src.URL + "'" + Environment.NewLine + www.error + Environment.NewLine + "Did you set the correct 'URL'?");
					}
				}
			}
			else
			{
				Debug.LogWarning("'URL' is null or empty!" + Environment.NewLine + "Please add a valid URL.");
			}
			coRoutines.Remove(uid);
			if (loading && coRoutines.Count == 0)
			{
				loading = false;
				init();
			}
		}

		private IEnumerator loadResource(Source src)
		{
			string uid = Guid.NewGuid().ToString();
			coRoutines.Add(uid);
			if (src.Resource != null)
			{
				List<string> list = Helper.SplitStringToLines(src.Resource.text);
				yield return null;
				if (list.Count > 0)
				{
					badwords.Add(new BadWords(src, list));
				}
				else
				{
					Debug.LogWarning(string.Concat("Resource: '", src.Resource, "' does not contain any active bad words!"));
				}
			}
			else
			{
				Debug.LogWarning("Resource field 'Source' is null or empty!" + Environment.NewLine + "Please add a valid resource.");
			}
			coRoutines.Remove(uid);
			if (loading && coRoutines.Count == 0)
			{
				loading = false;
				init();
			}
		}

		private void loadWebInEditor(Source src)
		{
			if (!string.IsNullOrEmpty(src.URL))
			{
				using (WWW wWW = new WWW(src.URL.Trim()))
				{
					float realtimeSinceStartup = Time.realtimeSinceStartup;
					float num = 3f;
					bool flag = false;
					while (!wWW.isDone)
					{
						if (Time.realtimeSinceStartup > realtimeSinceStartup + num)
						{
							flag = true;
							break;
						}
					}
					if (!flag && string.IsNullOrEmpty(wWW.error) && !string.IsNullOrEmpty(wWW.text))
					{
						List<string> list = Helper.SplitStringToLines(wWW.text);
						if (list.Count > 0)
						{
							badwords.Add(new BadWords(src, list));
						}
						else
						{
							Debug.LogWarning("Source: '" + src.URL + "' does not contain any active bad words!");
						}
					}
					else
					{
						Debug.LogWarning("Could not load source: '" + src.URL + "'" + Environment.NewLine + wWW.error + Environment.NewLine + "Did you set the correct 'URL'?");
					}
					return;
				}
			}
			Debug.LogWarning("'URL' is null or empty!" + Environment.NewLine + "Please add a valid URL.");
		}

		private void loadResourceInEditor(Source src)
		{
			if (src.Resource != null)
			{
				List<string> list = Helper.SplitStringToLines(src.Resource.text);
				if (list.Count > 0)
				{
					badwords.Add(new BadWords(src, list));
				}
				else
				{
					Debug.LogWarning(string.Concat("Resource: '", src.Resource, "' does not contain any active bad words!"));
				}
			}
			else
			{
				Debug.LogWarning("Resource field 'Source' is null or empty!" + Environment.NewLine + "Please add a valid resource.");
			}
		}
	}
}
