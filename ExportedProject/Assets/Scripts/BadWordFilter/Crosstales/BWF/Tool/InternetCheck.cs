using System;
using System.Collections;
using Crosstales.BWF.Util;
using UnityEngine;

namespace Crosstales.BWF.Tool
{
	[DisallowMultipleComponent]
	[HelpURL("https://www.crosstales.com/media/data/assets/badwordfilter/api/class_crosstales_1_1_b_w_f_1_1_tool_1_1_internet_check.html")]
	[ExecuteInEditMode]
	public class InternetCheck : MonoBehaviour
	{
		[Tooltip("Optimized check routines (default: on)")]
		public bool Optimized = true;

		private const float checkIntervalMin = 4f;

		private const float checkIntervalMax = 10f;

		private const bool runOnStart = true;

		private const bool endlessMode = true;

		private static bool internetAvailable;

		private static GameObject go;

		private static InternetCheck instance;

		private static bool initalized;

		private static bool loggedOnlyOneInstance;

		private bool isRunning;

		private float checkTime = 9999f;

		private float checkTimeCounter;

		private float burstTime = 9999f;

		private const float burstIntervalMin = 1f;

		private const float burstIntervalMax = 3f;

		private float burstTimeCounter;

		private static float lastCheckTime;

		public static bool isInternetAvailable
		{
			get
			{
				if (instance == null)
				{
					return Application.internetReachability != NetworkReachability.NotReachable;
				}
				return internetAvailable;
			}
		}

		public static DateTime LastCheck { get; private set; }

		public static int CheckCounter { get; private set; }

		public static int DownloadedData { get; private set; }

		public void OnEnable()
		{
			if (Helper.isEditorMode || !initalized)
			{
				go = base.gameObject;
				go.name = "InternetCheck";
				instance = this;
				StartCoroutine(internetCheck());
				if (!Helper.isEditorMode && Config.DONT_DESTROY_ON_LOAD)
				{
					UnityEngine.Object.DontDestroyOnLoad(base.transform.root.gameObject);
					initalized = true;
				}
				if (Constants.DEV_DEBUG)
				{
					Debug.Log("Using new instance!");
				}
			}
			else
			{
				if (!Helper.isEditorMode && Config.DONT_DESTROY_ON_LOAD && !loggedOnlyOneInstance)
				{
					Debug.LogWarning("Only one active instance of 'InternetCheck' allowed in all scenes!" + Environment.NewLine + "This object will now be destroyed.");
					UnityEngine.Object.Destroy(base.gameObject, 0.2f);
					loggedOnlyOneInstance = true;
				}
				if (Constants.DEV_DEBUG)
				{
					Debug.Log("Using old instance!");
				}
			}
		}

		public void Update()
		{
			if (Helper.isEditorMode && go != null)
			{
				go.name = "InternetCheck";
			}
			if (isRunning)
			{
				return;
			}
			checkTimeCounter += Time.deltaTime;
			burstTimeCounter += Time.deltaTime;
			if (isInternetAvailable)
			{
				if (checkTimeCounter > checkTime)
				{
					if (Constants.DEV_DEBUG)
					{
						Debug.Log("Normal-Mode!");
					}
					checkTimeCounter = 0f;
					burstTimeCounter = 0f;
					StartCoroutine(internetCheck());
				}
			}
			else if (burstTimeCounter > burstTime)
			{
				if (Constants.DEV_DEBUG)
				{
					Debug.Log("Burst-Mode!");
				}
				checkTimeCounter = 0f;
				burstTimeCounter = 0f;
				StartCoroutine(internetCheck());
			}
		}

		public void OnApplicationQuit()
		{
			if (instance != null)
			{
				instance.StopAllCoroutines();
			}
		}

		public static void Refresh()
		{
			if (instance != null && !instance.isRunning && lastCheckTime + 1f < Time.realtimeSinceStartup)
			{
				instance.StartCoroutine(instance.internetCheck());
			}
		}

		private IEnumerator internetCheck()
		{
			if (!isRunning)
			{
				isRunning = true;
				CheckCounter++;
				bool available = false;
				if (Helper.isWebPlatform || Helper.isEditorMode)
				{
					available = Application.internetReachability != NetworkReachability.NotReachable;
				}
				else
				{
					if (Optimized || Helper.isWindowsBasedPlatform)
					{
						if (Config.DEBUG)
						{
							Debug.Log("Testing the Internet availability for Optimized/Windows-based systems: " + DateTime.Now);
						}
						WWW wWW;
						WWW www = (wWW = new WWW("http://www.msftncsi.com/ncsi.txt"));
						using (wWW)
						{
							do
							{
								yield return www;
							}
							while (!www.isDone);
							if (string.IsNullOrEmpty(www.error))
							{
								if (Constants.DEV_DEBUG)
								{
									Debug.Log("Content for Windows-based systems: " + www.text);
								}
								available = !string.IsNullOrEmpty(www.text) && www.text.CTEquals("Microsoft NCSI");
								DownloadedData += www.bytesDownloaded;
							}
							else if (Constants.DEV_DEBUG)
							{
								Debug.LogError("Error getting content for Windows-based systems: " + www.error);
							}
						}
					}
					if (!Optimized && Helper.isAppleBasedPlatform)
					{
						if (Config.DEBUG)
						{
							Debug.Log("Testing the Internet availability for Apple-based systems: " + DateTime.Now);
						}
						WWW wWW;
						WWW www = (wWW = new WWW("https://www.apple.com/library/test/success.html"));
						using (wWW)
						{
							do
							{
								yield return www;
							}
							while (!www.isDone);
							if (string.IsNullOrEmpty(www.error))
							{
								if (Constants.DEV_DEBUG)
								{
									Debug.Log("Content for Apple-based systems: " + www.text);
								}
								available = !string.IsNullOrEmpty(www.text) && www.text.CTContains("<TITLE>Success</TITLE>");
								DownloadedData += www.bytesDownloaded;
							}
							else if (Constants.DEV_DEBUG)
							{
								Debug.LogError("Error getting content for Apple-based systems: " + www.error);
							}
						}
					}
					if (!available)
					{
						if (Config.DEBUG)
						{
							Debug.Log("Testing with default Internet availability: " + DateTime.Now);
						}
						WWW wWW;
						WWW www = (wWW = new WWW("http://start.ubuntu.com/connectivity-check"));
						using (wWW)
						{
							do
							{
								yield return www;
							}
							while (!www.isDone);
							if (string.IsNullOrEmpty(www.error))
							{
								if (Constants.DEV_DEBUG)
								{
									Debug.Log("Content for default: " + www.text);
								}
								available = !string.IsNullOrEmpty(www.text) && www.text.CTContains("<TITLE>Lorem Ipsum</TITLE>");
								DownloadedData += www.bytesDownloaded;
							}
							else if (Constants.DEV_DEBUG)
							{
								Debug.LogError("Error getting content for default: " + www.error);
							}
						}
					}
					if (!available)
					{
						if (Config.DEBUG)
						{
							Debug.Log("Testing with fallback Internet availability: " + DateTime.Now);
						}
						WWW wWW;
						WWW www = (wWW = new WWW("https://crosstales.com/media/downloads/up.txt"));
						using (wWW)
						{
							do
							{
								yield return www;
							}
							while (!www.isDone);
							if (string.IsNullOrEmpty(www.error))
							{
								if (Constants.DEV_DEBUG)
								{
									Debug.Log("Content for fallback: " + www.text);
								}
								available = !string.IsNullOrEmpty(www.text) && www.text.CTEquals("crosstales rulez!");
								DownloadedData += www.bytesDownloaded;
							}
							else if (Constants.DEV_DEBUG)
							{
								Debug.LogError("Error getting content for fallback: " + www.error);
							}
						}
					}
				}
				internetAvailable = available;
				checkTime = UnityEngine.Random.Range(4f, 10f);
				burstTime = UnityEngine.Random.Range(1f, 3f);
				LastCheck = DateTime.Now;
				lastCheckTime = Time.realtimeSinceStartup;
				isRunning = false;
			}
			else
			{
				Debug.LogError("Internet check already running!");
			}
		}
	}
}
