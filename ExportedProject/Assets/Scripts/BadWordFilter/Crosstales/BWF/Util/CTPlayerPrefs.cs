using System;
using UnityEngine;

namespace Crosstales.BWF.Util
{
	public static class CTPlayerPrefs
	{
		public static bool HasKey(string key)
		{
			return PlayerPrefs.HasKey(key);
		}

		public static string GetString(string key)
		{
			return PlayerPrefs.GetString(key);
		}

		public static bool GetBool(string key)
		{
			if (string.IsNullOrEmpty(key))
			{
				throw new ArgumentNullException("key");
			}
			if (!"true".CTEquals(PlayerPrefs.GetString(key)))
			{
				return false;
			}
			return true;
		}
	}
}
