using UnityEngine;

namespace Crosstales.BWF.Util
{
	public static class Config
	{
		public static string ASSET_PATH;

		public static bool DEBUG;

		public static bool DEBUG_BADWORDS;

		public static bool DEBUG_DOMAINS;

		public static bool UPDATE_CHECK;

		public static bool UPDATE_OPEN_UAS;

		public static bool REMINDER_CHECK;

		public static bool DONT_DESTROY_ON_LOAD;

		public static bool PREFAB_AUTOLOAD;

		public static bool HIERARCHY_ICON;

		static Config()
		{
			ASSET_PATH = "/crosstales/BadWordFilter/";
			DEBUG = false;
			DEBUG_BADWORDS = false;
			DEBUG_DOMAINS = false;
			UPDATE_CHECK = true;
			UPDATE_OPEN_UAS = false;
			REMINDER_CHECK = true;
			DONT_DESTROY_ON_LOAD = true;
			PREFAB_AUTOLOAD = false;
			HIERARCHY_ICON = true;
			Load();
			if (DEBUG)
			{
				Debug.Log("Config data loaded");
			}
		}

		public static void Load()
		{
			if (CTPlayerPrefs.HasKey("BWF_CFG_ASSET_PATH"))
			{
				ASSET_PATH = CTPlayerPrefs.GetString("BWF_CFG_ASSET_PATH");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_DEBUG"))
			{
				DEBUG = CTPlayerPrefs.GetBool("BWF_CFG_DEBUG");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_DEBUG_BADWORDS"))
			{
				DEBUG_BADWORDS = CTPlayerPrefs.GetBool("BWF_CFG_DEBUG_BADWORDS");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_DEBUG_DOMAINS"))
			{
				DEBUG_DOMAINS = CTPlayerPrefs.GetBool("BWF_CFG_DEBUG_DOMAINS");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_UPDATE_CHECK"))
			{
				UPDATE_CHECK = CTPlayerPrefs.GetBool("BWF_CFG_UPDATE_CHECK");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_UPDATE_OPEN_UAS"))
			{
				UPDATE_OPEN_UAS = CTPlayerPrefs.GetBool("BWF_CFG_UPDATE_OPEN_UAS");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_REMINDER_CHECK"))
			{
				REMINDER_CHECK = CTPlayerPrefs.GetBool("BWF_CFG_REMINDER_CHECK");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_PREFAB_AUTOLOAD"))
			{
				PREFAB_AUTOLOAD = CTPlayerPrefs.GetBool("BWF_CFG_PREFAB_AUTOLOAD");
			}
			if (CTPlayerPrefs.HasKey("BWF_CFG_HIERARCHY_ICON"))
			{
				HIERARCHY_ICON = CTPlayerPrefs.GetBool("BWF_CFG_HIERARCHY_ICON");
			}
		}
	}
}
