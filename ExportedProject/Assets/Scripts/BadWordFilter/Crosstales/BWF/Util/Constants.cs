using System;

namespace Crosstales.BWF.Util
{
	public static class Constants
	{
		public static readonly bool isPro = true;

		public static readonly DateTime ASSET_CREATED = new DateTime(2015, 1, 3);

		public static readonly DateTime ASSET_CHANGED = new DateTime(2017, 6, 1);

		public static bool DEV_DEBUG = false;

		public static string PREFAB_SUBPATH = "Prefabs/";

		public static string TEXT_TOSTRING_START = " {";

		public static string TEXT_TOSTRING_END = "}";

		public static string TEXT_TOSTRING_DELIMITER = "', ";

		public static string TEXT_TOSTRING_DELIMITER_END = "'";
	}
}
