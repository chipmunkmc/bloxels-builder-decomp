using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using UnityEngine;

namespace Crosstales.BWF.Util
{
	public static class Helper
	{
		private static readonly Regex lineEndingsRegex = new Regex("\\r\\n|\\r|\\n");

		private static readonly System.Random rd = new System.Random();

		public static bool isWindowsPlatform
		{
			get
			{
				if (Application.platform != RuntimePlatform.WindowsPlayer)
				{
					return Application.platform == RuntimePlatform.WindowsEditor;
				}
				return true;
			}
		}

		public static bool isMacOSPlatform
		{
			get
			{
				if (Application.platform != RuntimePlatform.OSXPlayer && Application.platform != 0)
				{
					return Application.platform == RuntimePlatform.OSXDashboardPlayer;
				}
				return true;
			}
		}

		public static bool isIOSPlatform
		{
			get
			{
				if (Application.platform != RuntimePlatform.IPhonePlayer)
				{
					return Application.platform == RuntimePlatform.tvOS;
				}
				return true;
			}
		}

		public static bool isWSAPlatform
		{
			get
			{
				if (Application.platform != RuntimePlatform.MetroPlayerARM && Application.platform != RuntimePlatform.MetroPlayerX86 && Application.platform != RuntimePlatform.MetroPlayerX64)
				{
					return Application.platform == RuntimePlatform.XboxOne;
				}
				return true;
			}
		}

		public static bool isWebGLPlatform
		{
			get
			{
				return Application.platform == RuntimePlatform.WebGLPlayer;
			}
		}

		public static bool isWebPlayerPlatform
		{
			get
			{
				return false;
			}
		}

		public static bool isWebPlatform
		{
			get
			{
				if (!isWebPlayerPlatform)
				{
					return isWebGLPlatform;
				}
				return true;
			}
		}

		public static bool isWindowsBasedPlatform
		{
			get
			{
				if (!isWindowsPlatform)
				{
					return isWSAPlatform;
				}
				return true;
			}
		}

		public static bool isAppleBasedPlatform
		{
			get
			{
				if (!isMacOSPlatform)
				{
					return isIOSPlatform;
				}
				return true;
			}
		}

		public static bool isEditorMode
		{
			get
			{
				if (Application.platform == RuntimePlatform.OSXEditor || Application.platform == RuntimePlatform.WindowsEditor)
				{
					return !Application.isPlaying;
				}
				return false;
			}
		}

		public static List<string> SplitStringToLines(string text, int skipHeaderLines = 0, int skipFooterLines = 0, char splitChar = '#')
		{
			List<string> list = new List<string>(200);
			if (string.IsNullOrEmpty(text))
			{
				Debug.LogWarning("Parameter 'text' is null or empty!" + Environment.NewLine + "=> 'SplitStringToLines()' will return an empty string list.");
			}
			else
			{
				string[] array = lineEndingsRegex.Split(text);
				for (int i = 0; i < array.Length; i++)
				{
					if (i + 1 > skipHeaderLines && i < array.Length - skipFooterLines && !string.IsNullOrEmpty(array[i]) && !array[i].StartsWith("#", StringComparison.OrdinalIgnoreCase))
					{
						list.Add(array[i].Split(splitChar)[0]);
					}
				}
			}
			return list;
		}

		public static string CreateReplaceString(string replaceChars, int stringLength)
		{
			if (replaceChars.Length > 1)
			{
				char[] array = new char[stringLength];
				for (int i = 0; i < stringLength; i++)
				{
					array[i] = replaceChars[rd.Next(0, replaceChars.Length)];
				}
				return new string(array);
			}
			if (replaceChars.Length == 1)
			{
				return new string(replaceChars[0], stringLength);
			}
			return string.Empty;
		}

		public static Color HSVToRGB(float h, float s, float v, float a = 1f)
		{
			if (s == 0f)
			{
				return new Color(v, v, v, a);
			}
			h /= 60f;
			int num = Mathf.FloorToInt(h);
			float num2 = h - (float)num;
			float num3 = v * (1f - s);
			float num4 = v * (1f - s * num2);
			float num5 = v * (1f - s * (1f - num2));
			switch (num)
			{
			case 0:
				return new Color(v, num5, num3, a);
			case 1:
				return new Color(num4, v, num3, a);
			case 2:
				return new Color(num3, v, num5, a);
			case 3:
				return new Color(num3, num4, v, a);
			case 4:
				return new Color(num5, num3, v, a);
			default:
				return new Color(v, num3, num4, a);
			}
		}
	}
}
