using System;
using UnityEngine;

namespace DG.Tweening.Core
{
	internal static class Utils
	{
		internal static Vector3 Vector3FromAngle(float degrees, float magnitude)
		{
			float f = degrees * ((float)Math.PI / 180f);
			return new Vector3(magnitude * Mathf.Cos(f), magnitude * Mathf.Sin(f), 0f);
		}
	}
}
