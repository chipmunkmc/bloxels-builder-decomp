using System.Text;
using System.Threading;

namespace ICSharpCode.SharpZipLib.Zip
{
	public sealed class ZipConstants
	{
		private static int defaultCodePage = Thread.CurrentThread.CurrentCulture.TextInfo.OEMCodePage;

		public static int DefaultCodePage
		{
			get
			{
				return defaultCodePage;
			}
		}

		public static string ConvertToString(byte[] data, int count)
		{
			if (data == null)
			{
				return string.Empty;
			}
			return Encoding.GetEncoding(DefaultCodePage).GetString(data, 0, count);
		}

		public static string ConvertToString(byte[] data)
		{
			if (data == null)
			{
				return string.Empty;
			}
			return ConvertToString(data, data.Length);
		}

		public static string ConvertToStringExt(int flags, byte[] data, int count)
		{
			if (data == null)
			{
				return string.Empty;
			}
			if (((uint)flags & 0x800u) != 0)
			{
				return Encoding.UTF8.GetString(data, 0, count);
			}
			return ConvertToString(data, count);
		}

		public static string ConvertToStringExt(int flags, byte[] data)
		{
			if (data == null)
			{
				return string.Empty;
			}
			if (((uint)flags & 0x800u) != 0)
			{
				return Encoding.UTF8.GetString(data, 0, data.Length);
			}
			return ConvertToString(data, data.Length);
		}

		public static byte[] ConvertToArray(string str)
		{
			if (str == null)
			{
				return new byte[0];
			}
			return Encoding.GetEncoding(DefaultCodePage).GetBytes(str);
		}

		public static byte[] ConvertToArray(int flags, string str)
		{
			if (str == null)
			{
				return new byte[0];
			}
			if (((uint)flags & 0x800u) != 0)
			{
				return Encoding.UTF8.GetBytes(str);
			}
			return ConvertToArray(str);
		}
	}
}
