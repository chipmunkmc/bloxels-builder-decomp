using System;
using ICSharpCode.SharpZipLib.Core;

namespace ICSharpCode.SharpZipLib.Zip
{
	public class ZipEntryFactory : IEntryFactory
	{
		private INameTransform nameTransform_;

		private DateTime fixedDateTime_ = DateTime.Now;

		private int getAttributes_ = -1;

		public ZipEntryFactory()
		{
			nameTransform_ = new ZipNameTransform();
		}
	}
}
