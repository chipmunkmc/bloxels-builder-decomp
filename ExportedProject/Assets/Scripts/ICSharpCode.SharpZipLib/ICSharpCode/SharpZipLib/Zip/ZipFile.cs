using System;
using System.Collections;
using System.Globalization;
using System.IO;
using System.Reflection;
using System.Security.Cryptography;
using ICSharpCode.SharpZipLib.Core;
using ICSharpCode.SharpZipLib.Encryption;
using ICSharpCode.SharpZipLib.Zip.Compression;
using ICSharpCode.SharpZipLib.Zip.Compression.Streams;

namespace ICSharpCode.SharpZipLib.Zip
{
	[DefaultMember("EntryByIndex")]
	public class ZipFile : IEnumerable, IDisposable
	{
		public delegate void KeysRequiredEventHandler(object sender, KeysRequiredEventArgs e);

		[Flags]
		private enum HeaderTest
		{
			Extract = 1,
			Header = 2
		}

		private class ZipEntryEnumerator : IEnumerator
		{
			private ZipEntry[] array;

			private int index = -1;

			public object Current
			{
				get
				{
					return array[index];
				}
			}

			public ZipEntryEnumerator(ZipEntry[] entries)
			{
				array = entries;
			}

			public void Reset()
			{
				index = -1;
			}

			public bool MoveNext()
			{
				return ++index < array.Length;
			}
		}

		private class PartialInputStream : Stream
		{
			private ZipFile zipFile_;

			private Stream baseStream_;

			private long start_;

			private long length_;

			private long readPos_;

			private long end_;

			public override long Position
			{
				get
				{
					return readPos_ - start_;
				}
				set
				{
					long num = start_ + value;
					if (num < start_)
					{
						throw new ArgumentException("Negative position is invalid");
					}
					if (num >= end_)
					{
						throw new InvalidOperationException("Cannot seek past end");
					}
					readPos_ = num;
				}
			}

			public override long Length
			{
				get
				{
					return length_;
				}
			}

			public override bool CanWrite
			{
				get
				{
					return false;
				}
			}

			public override bool CanSeek
			{
				get
				{
					return true;
				}
			}

			public override bool CanRead
			{
				get
				{
					return true;
				}
			}

			public PartialInputStream(ZipFile zipFile, long start, long length)
			{
				start_ = start;
				length_ = length;
				zipFile_ = zipFile;
				baseStream_ = zipFile_.baseStream_;
				readPos_ = start;
				end_ = start + length;
			}

			public override int ReadByte()
			{
				if (readPos_ >= end_)
				{
					return -1;
				}
				lock (baseStream_)
				{
					baseStream_.Seek(readPos_++, SeekOrigin.Begin);
					return baseStream_.ReadByte();
				}
			}

			public override void Close()
			{
			}

			public override int Read(byte[] buffer, int offset, int count)
			{
				lock (baseStream_)
				{
					if (count > end_ - readPos_)
					{
						count = (int)(end_ - readPos_);
						if (count == 0)
						{
							return 0;
						}
					}
					baseStream_.Seek(readPos_, SeekOrigin.Begin);
					int num = baseStream_.Read(buffer, offset, count);
					if (num > 0)
					{
						readPos_ += num;
					}
					return num;
				}
			}

			public override void Write(byte[] buffer, int offset, int count)
			{
				throw new NotSupportedException();
			}

			public override void SetLength(long value)
			{
				throw new NotSupportedException();
			}

			public override long Seek(long offset, SeekOrigin origin)
			{
				long num = readPos_;
				switch (origin)
				{
				case SeekOrigin.Begin:
					num = start_ + offset;
					break;
				case SeekOrigin.Current:
					num = readPos_ + offset;
					break;
				case SeekOrigin.End:
					num = end_ + offset;
					break;
				}
				if (num < start_)
				{
					throw new ArgumentException("Negative position is invalid");
				}
				if (num >= end_)
				{
					throw new IOException("Cannot seek past end");
				}
				readPos_ = num;
				return readPos_;
			}

			public override void Flush()
			{
			}
		}

		public KeysRequiredEventHandler KeysRequired;

		private bool isDisposed_;

		private string name_;

		private string comment_;

		private string rawPassword_;

		private Stream baseStream_;

		private bool isStreamOwner;

		private long offsetOfFirstEntry;

		private ZipEntry[] entries_;

		private byte[] key;

		private bool isNewArchive_;

		private UseZip64 useZip64_ = UseZip64.Dynamic;

		private ArrayList updates_;

		private Hashtable updateIndex_;

		private IArchiveStorage archiveStorage_;

		private IDynamicDataSource updateDataSource_;

		private int bufferSize_ = 4096;

		private IEntryFactory updateEntryFactory_ = new ZipEntryFactory();

		private bool HaveKeys
		{
			get
			{
				return key != null;
			}
		}

		public bool IsStreamOwner
		{
			get
			{
				return isStreamOwner;
			}
			set
			{
				isStreamOwner = value;
			}
		}

		public ZipFile(FileStream file)
		{
			if (file == null)
			{
				throw new ArgumentNullException("file");
			}
			if (!file.CanSeek)
			{
				throw new ArgumentException("Stream is not seekable", "file");
			}
			baseStream_ = file;
			name_ = file.Name;
			isStreamOwner = true;
			try
			{
				ReadEntries();
			}
			catch
			{
				DisposeInternal(true);
				throw;
			}
		}

		public ZipFile(Stream stream)
		{
			if (stream == null)
			{
				throw new ArgumentNullException("stream");
			}
			if (!stream.CanSeek)
			{
				throw new ArgumentException("Stream is not seekable", "stream");
			}
			baseStream_ = stream;
			isStreamOwner = true;
			if (baseStream_.Length > 0)
			{
				try
				{
					ReadEntries();
					return;
				}
				catch
				{
					DisposeInternal(true);
					throw;
				}
			}
			entries_ = new ZipEntry[0];
			isNewArchive_ = true;
		}

		private void OnKeysRequired(string fileName)
		{
			if (KeysRequired != null)
			{
				KeysRequiredEventArgs keysRequiredEventArgs = new KeysRequiredEventArgs(fileName, key);
				KeysRequired(this, keysRequiredEventArgs);
				key = keysRequiredEventArgs.Key;
			}
		}

		~ZipFile()
		{
			Dispose(false);
		}

		public void Close()
		{
			DisposeInternal(true);
			GC.SuppressFinalize(this);
		}

		public IEnumerator GetEnumerator()
		{
			if (isDisposed_)
			{
				throw new ObjectDisposedException("ZipFile");
			}
			return new ZipEntryEnumerator(entries_);
		}

		public int FindEntry(string name, bool ignoreCase)
		{
			if (isDisposed_)
			{
				throw new ObjectDisposedException("ZipFile");
			}
			for (int i = 0; i < entries_.Length; i++)
			{
				if (string.Compare(name, entries_[i].Name, ignoreCase, CultureInfo.InvariantCulture) == 0)
				{
					return i;
				}
			}
			return -1;
		}

		public Stream GetInputStream(ZipEntry entry)
		{
			if (entry == null)
			{
				throw new ArgumentNullException("entry");
			}
			if (isDisposed_)
			{
				throw new ObjectDisposedException("ZipFile");
			}
			long num = entry.ZipFileIndex;
			if (num < 0 || num >= entries_.Length || entries_[num].Name != entry.Name)
			{
				num = FindEntry(entry.Name, true);
				if (num < 0)
				{
					throw new ZipException("Entry cannot be found");
				}
			}
			return GetInputStream(num);
		}

		public Stream GetInputStream(long entryIndex)
		{
			if (isDisposed_)
			{
				throw new ObjectDisposedException("ZipFile");
			}
			long start = LocateEntry(entries_[entryIndex]);
			CompressionMethod compressionMethod = entries_[entryIndex].CompressionMethod;
			Stream stream = new PartialInputStream(this, start, entries_[entryIndex].CompressedSize);
			if (entries_[entryIndex].IsCrypted)
			{
				stream = CreateAndInitDecryptionStream(stream, entries_[entryIndex]);
				if (stream == null)
				{
					throw new ZipException("Unable to decrypt this entry");
				}
			}
			switch (compressionMethod)
			{
			case CompressionMethod.Deflated:
				stream = new InflaterInputStream(stream, new Inflater(true));
				break;
			default:
				throw new ZipException("Unsupported compression method " + compressionMethod);
			case CompressionMethod.Stored:
				break;
			}
			return stream;
		}

		private long TestLocalHeader(ZipEntry entry, HeaderTest tests)
		{
			lock (baseStream_)
			{
				bool flag = (tests & HeaderTest.Header) != 0;
				bool flag2 = (tests & HeaderTest.Extract) != 0;
				baseStream_.Seek(offsetOfFirstEntry + entry.Offset, SeekOrigin.Begin);
				if (ReadLEUint() != 67324752)
				{
					throw new ZipException(string.Format("Wrong local header signature @{0:X}", offsetOfFirstEntry + entry.Offset));
				}
				short num = (short)ReadLEUshort();
				short num2 = (short)ReadLEUshort();
				short num3 = (short)ReadLEUshort();
				short num4 = (short)ReadLEUshort();
				short num5 = (short)ReadLEUshort();
				uint num6 = ReadLEUint();
				long num7 = ReadLEUint();
				long num8 = ReadLEUint();
				int num9 = ReadLEUshort();
				int num10 = ReadLEUshort();
				byte[] array = new byte[num9];
				StreamUtils.ReadFully(baseStream_, array);
				byte[] array2 = new byte[num10];
				StreamUtils.ReadFully(baseStream_, array2);
				ZipExtraData zipExtraData = new ZipExtraData(array2);
				if (zipExtraData.Find(1))
				{
					num8 = zipExtraData.ReadLong();
					num7 = zipExtraData.ReadLong();
					if (((uint)num2 & 8u) != 0)
					{
						if (num8 != -1 && num8 != entry.Size)
						{
							throw new ZipException("Size invalid for descriptor");
						}
						if (num7 != -1 && num7 != entry.CompressedSize)
						{
							throw new ZipException("Compressed size invalid for descriptor");
						}
					}
				}
				else if (num >= 45 && ((int)num8 == -1 || (int)num7 == -1))
				{
					throw new ZipException("Required Zip64 extended information missing");
				}
				if (flag2 && entry.IsFile)
				{
					if (!entry.IsCompressionMethodSupported())
					{
						throw new ZipException("Compression method not supported");
					}
					if (num > 51 || (num > 20 && num < 45))
					{
						throw new ZipException(string.Format("Version required to extract this entry not supported ({0})", num));
					}
					if (((uint)num2 & 0x3060u) != 0)
					{
						throw new ZipException("The library does not support the zip version required to extract this entry");
					}
				}
				if (flag)
				{
					if (num <= 63 && num != 10 && num != 11 && num != 20 && num != 21 && num != 25 && num != 27 && num != 45 && num != 46 && num != 50 && num != 51 && num != 52 && num != 61 && num != 62 && num != 63)
					{
						throw new ZipException(string.Format("Version required to extract this entry is invalid ({0})", num));
					}
					if (((uint)num2 & 0xC010u) != 0)
					{
						throw new ZipException("Reserved bit flags cannot be set.");
					}
					if (((uint)num2 & (true ? 1u : 0u)) != 0 && num < 20)
					{
						throw new ZipException(string.Format("Version required to extract this entry is too low for encryption ({0})", num));
					}
					if (((uint)num2 & 0x40u) != 0)
					{
						if ((num2 & 1) == 0)
						{
							throw new ZipException("Strong encryption flag set but encryption flag is not set");
						}
						if (num < 50)
						{
							throw new ZipException(string.Format("Version required to extract this entry is too low for encryption ({0})", num));
						}
					}
					if (((uint)num2 & 0x20u) != 0 && num < 27)
					{
						throw new ZipException(string.Format("Patched data requires higher version than ({0})", num));
					}
					if (num2 != entry.Flags)
					{
						throw new ZipException("Central header/local header flags mismatch");
					}
					if (entry.CompressionMethod != (CompressionMethod)num3)
					{
						throw new ZipException("Central header/local header compression method mismatch");
					}
					if (entry.Version != num)
					{
						throw new ZipException("Extract version mismatch");
					}
					if (((uint)num2 & 0x40u) != 0 && num < 62)
					{
						throw new ZipException("Strong encryption flag set but version not high enough");
					}
					if (((uint)num2 & 0x2000u) != 0 && (num4 != 0 || num5 != 0))
					{
						throw new ZipException("Header masked set but date/time values non-zero");
					}
					if ((num2 & 8) == 0 && num6 != (uint)entry.Crc)
					{
						throw new ZipException("Central header/local header crc mismatch");
					}
					if (num8 == 0 && num7 == 0 && num6 != 0)
					{
						throw new ZipException("Invalid CRC for empty entry");
					}
					if (entry.Name.Length > num9)
					{
						throw new ZipException("File name length mismatch");
					}
					string text = ZipConstants.ConvertToStringExt(num2, array);
					if (text != entry.Name)
					{
						throw new ZipException("Central header and local header file name mismatch");
					}
					if (entry.IsDirectory)
					{
						if (num8 > 0)
						{
							throw new ZipException("Directory cannot have size");
						}
						if (entry.IsCrypted)
						{
							if (num7 > 14)
							{
								throw new ZipException("Directory compressed size invalid");
							}
						}
						else if (num7 > 2)
						{
							throw new ZipException("Directory compressed size invalid");
						}
					}
					if (!ZipNameTransform.IsValidName(text, true))
					{
						throw new ZipException("Name is invalid");
					}
				}
				if ((num2 & 8) == 0 || num8 > 0 || num7 > 0)
				{
					if (num8 != entry.Size)
					{
						throw new ZipException(string.Format("Size mismatch between central header({0}) and local header({1})", entry.Size, num8));
					}
					if (num7 != entry.CompressedSize && num7 != uint.MaxValue && num7 != -1)
					{
						throw new ZipException(string.Format("Compressed size mismatch between central header({0}) and local header({1})", entry.CompressedSize, num7));
					}
				}
				int num11 = num9 + num10;
				return offsetOfFirstEntry + entry.Offset + 30 + num11;
			}
		}

		private void PostUpdateCleanup()
		{
			updateDataSource_ = null;
			updates_ = null;
			updateIndex_ = null;
			if (archiveStorage_ != null)
			{
				archiveStorage_.Dispose();
				archiveStorage_ = null;
			}
		}

		void IDisposable.Dispose()
		{
			Close();
		}

		private void DisposeInternal(bool disposing)
		{
			if (isDisposed_)
			{
				return;
			}
			isDisposed_ = true;
			entries_ = new ZipEntry[0];
			if (IsStreamOwner && baseStream_ != null)
			{
				lock (baseStream_)
				{
					baseStream_.Close();
				}
			}
			PostUpdateCleanup();
		}

		protected virtual void Dispose(bool disposing)
		{
			DisposeInternal(disposing);
		}

		private ushort ReadLEUshort()
		{
			int num = baseStream_.ReadByte();
			if (num < 0)
			{
				throw new EndOfStreamException("End of stream");
			}
			int num2 = baseStream_.ReadByte();
			if (num2 < 0)
			{
				throw new EndOfStreamException("End of stream");
			}
			return (ushort)((ushort)num | (ushort)(num2 << 8));
		}

		private uint ReadLEUint()
		{
			return (uint)(ReadLEUshort() | (ReadLEUshort() << 16));
		}

		private ulong ReadLEUlong()
		{
			return ReadLEUint() | ((ulong)ReadLEUint() << 32);
		}

		private long LocateBlockWithSignature(int signature, long endLocation, int minimumBlockSize, int maximumVariableData)
		{
			using (ZipHelperStream zipHelperStream = new ZipHelperStream(baseStream_))
			{
				return zipHelperStream.LocateBlockWithSignature(signature, endLocation, minimumBlockSize, maximumVariableData);
			}
		}

		private void ReadEntries()
		{
			if (!baseStream_.CanSeek)
			{
				throw new ZipException("ZipFile stream must be seekable");
			}
			long num = LocateBlockWithSignature(101010256, baseStream_.Length, 22, 65535);
			if (num < 0)
			{
				throw new ZipException("Cannot find central directory");
			}
			ushort num2 = ReadLEUshort();
			ushort num3 = ReadLEUshort();
			ulong num4 = ReadLEUshort();
			ulong num5 = ReadLEUshort();
			ulong num6 = ReadLEUint();
			long num7 = ReadLEUint();
			uint num8 = ReadLEUshort();
			if (num8 != 0)
			{
				byte[] array = new byte[num8];
				StreamUtils.ReadFully(baseStream_, array);
				comment_ = ZipConstants.ConvertToString(array);
			}
			else
			{
				comment_ = string.Empty;
			}
			bool flag = false;
			if (num2 == ushort.MaxValue || num3 == ushort.MaxValue || num4 == 65535 || num5 == 65535 || num6 == uint.MaxValue || num7 == uint.MaxValue)
			{
				flag = true;
				long num9 = LocateBlockWithSignature(117853008, num, 0, 4096);
				if (num9 < 0)
				{
					throw new ZipException("Cannot find Zip64 locator");
				}
				ReadLEUint();
				ulong num10 = ReadLEUlong();
				uint num11 = ReadLEUint();
				baseStream_.Position = (long)num10;
				long num12 = ReadLEUint();
				if (num12 != 101075792)
				{
					throw new ZipException(string.Format("Invalid Zip64 Central directory signature at {0:X}", num10));
				}
				ulong num13 = ReadLEUlong();
				int num14 = ReadLEUshort();
				int num15 = ReadLEUshort();
				uint num16 = ReadLEUint();
				uint num17 = ReadLEUint();
				num4 = ReadLEUlong();
				num5 = ReadLEUlong();
				num6 = ReadLEUlong();
				num7 = (long)ReadLEUlong();
			}
			entries_ = new ZipEntry[num4];
			if (!flag && num7 < num - (long)(4 + num6))
			{
				offsetOfFirstEntry = num - ((long)(4 + num6) + num7);
				if (offsetOfFirstEntry <= 0)
				{
					throw new ZipException("Invalid embedded zip archive");
				}
			}
			baseStream_.Seek(offsetOfFirstEntry + num7, SeekOrigin.Begin);
			for (ulong num18 = 0uL; num18 < num4; num18++)
			{
				if (ReadLEUint() != 33639248)
				{
					throw new ZipException("Wrong Central Directory signature");
				}
				int madeByInfo = ReadLEUshort();
				int versionRequiredToExtract = ReadLEUshort();
				int num19 = ReadLEUshort();
				int method = ReadLEUshort();
				uint num20 = ReadLEUint();
				uint num21 = ReadLEUint();
				long num22 = ReadLEUint();
				long num23 = ReadLEUint();
				int num24 = ReadLEUshort();
				int num25 = ReadLEUshort();
				int num26 = ReadLEUshort();
				int num27 = ReadLEUshort();
				int num28 = ReadLEUshort();
				uint externalFileAttributes = ReadLEUint();
				long offset = ReadLEUint();
				byte[] array2 = new byte[Math.Max(num24, num26)];
				StreamUtils.ReadFully(baseStream_, array2, 0, num24);
				string name = ZipConstants.ConvertToStringExt(num19, array2, num24);
				ZipEntry zipEntry = new ZipEntry(name, versionRequiredToExtract, madeByInfo, (CompressionMethod)method);
				zipEntry.Crc = (long)num21 & 0xFFFFFFFFL;
				zipEntry.Size = num23 & 0xFFFFFFFFu;
				zipEntry.CompressedSize = num22 & 0xFFFFFFFFu;
				zipEntry.Flags = num19;
				zipEntry.DosTime = num20;
				zipEntry.ZipFileIndex = (long)num18;
				zipEntry.Offset = offset;
				zipEntry.ExternalFileAttributes = (int)externalFileAttributes;
				if ((num19 & 8) == 0)
				{
					zipEntry.CryptoCheckValue = (byte)(num21 >> 24);
				}
				else
				{
					zipEntry.CryptoCheckValue = (byte)((num20 >> 8) & 0xFFu);
				}
				if (num25 > 0)
				{
					byte[] array3 = new byte[num25];
					StreamUtils.ReadFully(baseStream_, array3);
					zipEntry.ExtraData = array3;
				}
				zipEntry.ProcessExtraData(false);
				if (num26 > 0)
				{
					StreamUtils.ReadFully(baseStream_, array2, 0, num26);
					zipEntry.Comment = ZipConstants.ConvertToStringExt(num19, array2, num26);
				}
				entries_[num18] = zipEntry;
			}
		}

		private long LocateEntry(ZipEntry entry)
		{
			return TestLocalHeader(entry, HeaderTest.Extract);
		}

		private Stream CreateAndInitDecryptionStream(Stream baseStream, ZipEntry entry)
		{
			CryptoStream cryptoStream = null;
			if (entry.Version < 50 || (entry.Flags & 0x40) == 0)
			{
				PkzipClassicManaged pkzipClassicManaged = new PkzipClassicManaged();
				OnKeysRequired(entry.Name);
				if (!HaveKeys)
				{
					throw new ZipException("No password available for encrypted stream");
				}
				cryptoStream = new CryptoStream(baseStream, pkzipClassicManaged.CreateDecryptor(key, null), CryptoStreamMode.Read);
				CheckClassicPassword(cryptoStream, entry);
			}
			else
			{
				if (entry.Version != 51)
				{
					throw new ZipException("Decryption method not supported");
				}
				OnKeysRequired(entry.Name);
				if (!HaveKeys)
				{
					throw new ZipException("No password available for AES encrypted stream");
				}
				int aESSaltLen = entry.AESSaltLen;
				byte[] array = new byte[aESSaltLen];
				int num = baseStream.Read(array, 0, aESSaltLen);
				if (num != aESSaltLen)
				{
					throw new ZipException("AES Salt expected " + aESSaltLen + " got " + num);
				}
				byte[] array2 = new byte[2];
				baseStream.Read(array2, 0, 2);
				int blockSize = entry.AESKeySize / 8;
				ZipAESTransform zipAESTransform = new ZipAESTransform(rawPassword_, array, blockSize, false);
				byte[] pwdVerifier = zipAESTransform.PwdVerifier;
				if (pwdVerifier[0] != array2[0] || pwdVerifier[1] != array2[1])
				{
					throw new Exception("Invalid password for AES");
				}
				cryptoStream = new ZipAESStream(baseStream, zipAESTransform, CryptoStreamMode.Read);
			}
			return cryptoStream;
		}

		private static void CheckClassicPassword(CryptoStream classicCryptoStream, ZipEntry entry)
		{
			byte[] array = new byte[12];
			StreamUtils.ReadFully(classicCryptoStream, array);
			if (array[11] != entry.CryptoCheckValue)
			{
				throw new ZipException("Invalid password");
			}
		}
	}
}
