using System;
using System.IO;

namespace ICSharpCode.SharpZipLib.Zip
{
	internal class ZipHelperStream : Stream
	{
		private bool isOwner_;

		private Stream stream_;

		public override bool CanRead
		{
			get
			{
				return stream_.CanRead;
			}
		}

		public override bool CanSeek
		{
			get
			{
				return stream_.CanSeek;
			}
		}

		public override long Length
		{
			get
			{
				return stream_.Length;
			}
		}

		public override long Position
		{
			get
			{
				return stream_.Position;
			}
			set
			{
				stream_.Position = value;
			}
		}

		public override bool CanWrite
		{
			get
			{
				return stream_.CanWrite;
			}
		}

		public ZipHelperStream(Stream stream)
		{
			stream_ = stream;
		}

		public override void Flush()
		{
			stream_.Flush();
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return stream_.Seek(offset, origin);
		}

		public override void SetLength(long value)
		{
			stream_.SetLength(value);
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			return stream_.Read(buffer, offset, count);
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			stream_.Write(buffer, offset, count);
		}

		public override void Close()
		{
			Stream stream = stream_;
			stream_ = null;
			if (isOwner_ && stream != null)
			{
				isOwner_ = false;
				stream.Close();
			}
		}

		public long LocateBlockWithSignature(int signature, long endLocation, int minimumBlockSize, int maximumVariableData)
		{
			long num = endLocation - minimumBlockSize;
			if (num < 0)
			{
				return -1L;
			}
			long num2 = Math.Max(num - maximumVariableData, 0L);
			do
			{
				if (num < num2)
				{
					return -1L;
				}
				Seek(num--, SeekOrigin.Begin);
			}
			while (ReadLEInt() != signature);
			return Position;
		}

		public void WriteZip64EndOfCentralDirectory(long noOfEntries, long sizeEntries, long centralDirOffset)
		{
			long position = stream_.Position;
			WriteLEInt(101075792);
			WriteLELong(44L);
			WriteLEShort(51);
			WriteLEShort(45);
			WriteLEInt(0);
			WriteLEInt(0);
			WriteLELong(noOfEntries);
			WriteLELong(noOfEntries);
			WriteLELong(sizeEntries);
			WriteLELong(centralDirOffset);
			WriteLEInt(117853008);
			WriteLEInt(0);
			WriteLELong(position);
			WriteLEInt(1);
		}

		public void WriteEndOfCentralDirectory(long noOfEntries, long sizeEntries, long startOfCentralDirectory, byte[] comment)
		{
			if (noOfEntries >= 65535 || startOfCentralDirectory >= uint.MaxValue || sizeEntries >= uint.MaxValue)
			{
				WriteZip64EndOfCentralDirectory(noOfEntries, sizeEntries, startOfCentralDirectory);
			}
			WriteLEInt(101010256);
			WriteLEShort(0);
			WriteLEShort(0);
			if (noOfEntries >= 65535)
			{
				WriteLEUshort(ushort.MaxValue);
				WriteLEUshort(ushort.MaxValue);
			}
			else
			{
				WriteLEShort((short)noOfEntries);
				WriteLEShort((short)noOfEntries);
			}
			if (sizeEntries >= uint.MaxValue)
			{
				WriteLEUint(uint.MaxValue);
			}
			else
			{
				WriteLEInt((int)sizeEntries);
			}
			if (startOfCentralDirectory >= uint.MaxValue)
			{
				WriteLEUint(uint.MaxValue);
			}
			else
			{
				WriteLEInt((int)startOfCentralDirectory);
			}
			int num = ((comment != null) ? comment.Length : 0);
			if (num > 65535)
			{
				throw new ZipException(string.Format("Comment length({0}) is too long can only be 64K", num));
			}
			WriteLEShort(num);
			if (num > 0)
			{
				Write(comment, 0, comment.Length);
			}
		}

		public int ReadLEShort()
		{
			int num = stream_.ReadByte();
			if (num < 0)
			{
				throw new EndOfStreamException();
			}
			int num2 = stream_.ReadByte();
			if (num2 < 0)
			{
				throw new EndOfStreamException();
			}
			return num | (num2 << 8);
		}

		public int ReadLEInt()
		{
			return ReadLEShort() | (ReadLEShort() << 16);
		}

		public void WriteLEShort(int value)
		{
			stream_.WriteByte((byte)((uint)value & 0xFFu));
			stream_.WriteByte((byte)((uint)(value >> 8) & 0xFFu));
		}

		public void WriteLEUshort(ushort value)
		{
			stream_.WriteByte((byte)(value & 0xFFu));
			stream_.WriteByte((byte)(value >> 8));
		}

		public void WriteLEInt(int value)
		{
			WriteLEShort(value);
			WriteLEShort(value >> 16);
		}

		public void WriteLEUint(uint value)
		{
			WriteLEUshort((ushort)(value & 0xFFFFu));
			WriteLEUshort((ushort)(value >> 16));
		}

		public void WriteLELong(long value)
		{
			WriteLEInt((int)value);
			WriteLEInt((int)(value >> 32));
		}
	}
}
