using System;
using System.Runtime.InteropServices;

namespace Ionic.Crc
{
	[ClassInterface(ClassInterfaceType.AutoDispatch)]
	[Guid("ebc25cf6-9120-4283-b972-0e5520d0000C")]
	[ComVisible(true)]
	public class CRC32
	{
		private uint dwPolynomial;

		private long _TotalBytesRead;

		private bool reverseBits;

		private uint[] crc32Table;

		private uint _register = uint.MaxValue;

		public long TotalBytesRead
		{
			get
			{
				return _TotalBytesRead;
			}
		}

		public int Crc32Result
		{
			get
			{
				return (int)(~_register);
			}
		}

		public CRC32()
			: this(false)
		{
		}

		public CRC32(bool reverseBits)
			: this(-306674912, reverseBits)
		{
		}

		public CRC32(int polynomial, bool reverseBits)
		{
			this.reverseBits = reverseBits;
			dwPolynomial = (uint)polynomial;
			GenerateLookupTable();
		}

		public void SlurpBlock(byte[] block, int offset, int count)
		{
			if (block == null)
			{
				throw new Exception("The data buffer must not be null.");
			}
			for (int i = 0; i < count; i++)
			{
				int num = offset + i;
				byte b = block[num];
				if (reverseBits)
				{
					uint num2 = (_register >> 24) ^ b;
					_register = (_register << 8) ^ crc32Table[num2];
				}
				else
				{
					uint num3 = (_register & 0xFFu) ^ b;
					_register = (_register >> 8) ^ crc32Table[num3];
				}
			}
			_TotalBytesRead += count;
		}

		private static uint ReverseBits(uint data)
		{
			uint num = data;
			num = ((num & 0x55555555) << 1) | ((num >> 1) & 0x55555555u);
			num = ((num & 0x33333333) << 2) | ((num >> 2) & 0x33333333u);
			num = ((num & 0xF0F0F0F) << 4) | ((num >> 4) & 0xF0F0F0Fu);
			return (num << 24) | ((num & 0xFF00) << 8) | ((num >> 8) & 0xFF00u) | (num >> 24);
		}

		private static byte ReverseBits(byte data)
		{
			uint num = (uint)(data * 131586);
			uint num2 = 17055760u;
			uint num3 = num & num2;
			uint num4 = (num << 2) & (num2 << 1);
			return (byte)(16781313 * (num3 + num4) >> 24);
		}

		private void GenerateLookupTable()
		{
			crc32Table = new uint[256];
			byte b = 0;
			do
			{
				uint num = b;
				for (byte b2 = 8; b2 > 0; b2--)
				{
					num = (((num & 1) != 1) ? (num >> 1) : ((num >> 1) ^ dwPolynomial));
				}
				if (reverseBits)
				{
					crc32Table[ReverseBits(b)] = ReverseBits(num);
				}
				else
				{
					crc32Table[b] = num;
				}
				b++;
			}
			while (b != 0);
		}
	}
}
