using System;
using System.Collections;
using UnityEngine;

internal class UniExtender : MonoBehaviour
{
	private static UniExtender _instance;

	public static UniExtender Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = new GameObject("UniExtender", typeof(UniExtender)).GetComponent<UniExtender>();
				_instance.gameObject.hideFlags = HideFlags.HideInHierarchy | HideFlags.DontSaveInEditor | HideFlags.NotEditable;
			}
			return _instance;
		}
	}

	public static IEnumerator Step(float T, Action<float> Step)
	{
		float P = 0f;
		while (P <= 1f)
		{
			P = Mathf.Clamp01(P + Time.deltaTime / T);
			Step(Mathf.SmoothStep(0f, 1f, P));
			yield return null;
		}
	}
}
