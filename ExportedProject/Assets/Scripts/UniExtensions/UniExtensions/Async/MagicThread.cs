using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace UniExtensions.Async
{
	public class MagicThread : MonoBehaviour
	{
		private static MagicThread _instance;

		private List<IEnumerator> foregroundTasks = new List<IEnumerator>();

		private List<IEnumerator> backgroundTasks = new List<IEnumerator>();

		private static MagicThread instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new GameObject("MagicThreads", typeof(MagicThread)).GetComponent<MagicThread>();
					_instance.gameObject.hideFlags = HideFlags.HideInHierarchy | HideFlags.DontSaveInEditor | HideFlags.NotEditable;
				}
				return _instance;
			}
		}

		public static void Start(IEnumerator task, bool startInBackground = true)
		{
			if (startInBackground)
			{
				instance.backgroundTasks.Add(task);
			}
			else
			{
				instance.foregroundTasks.Add(task);
			}
		}

		private IEnumerator Start()
		{
			while (true)
			{
				yield return null;
				if (foregroundTasks.Count > 0)
				{
					IEnumerator[] array;
					lock (foregroundTasks)
					{
						array = foregroundTasks.ToArray();
						foregroundTasks.Clear();
					}
					IEnumerator[] array2 = array;
					foreach (IEnumerator task in array2)
					{
						StartCoroutine(HandleCoroutine(task));
					}
				}
				if (backgroundTasks.Count <= 0)
				{
					continue;
				}
				foreach (IEnumerator i in backgroundTasks)
				{
					ThreadPool.QueueUserWorkItem(delegate
					{
						HandleThread(i);
					});
				}
				backgroundTasks.Clear();
			}
		}

		private IEnumerator HandleCoroutine(IEnumerator task)
		{
			while (task.MoveNext())
			{
				object t = task.Current;
				if (!(t is BackgroundTask))
				{
					yield return t;
					continue;
				}
				backgroundTasks.Add(task);
				break;
			}
		}

		private void HandleThread(IEnumerator task)
		{
			try
			{
				while (task.MoveNext())
				{
					object current = task.Current;
					if (current is ForegroundTask)
					{
						lock (foregroundTasks)
						{
							foregroundTasks.Add(task);
							break;
						}
					}
				}
			}
			catch (ThreadAbortException)
			{
			}
			catch (Exception ex2)
			{
				Debug.LogError("Exception in MagicThread: " + ex2.ToString());
			}
		}
	}
}
