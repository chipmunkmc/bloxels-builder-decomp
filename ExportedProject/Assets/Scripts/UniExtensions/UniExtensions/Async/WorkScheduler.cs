using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace UniExtensions.Async
{
	public class WorkScheduler : MonoBehaviour
	{
		private Dictionary<string, List<IEnumerator>> queues = new Dictionary<string, List<IEnumerator>>();

		public void ScheduleWorkItem(string name, IEnumerator task)
		{
			if (!queues.ContainsKey(name))
			{
				queues[name] = new List<IEnumerator>();
				StartCoroutine(ProcessQueue(name));
			}
			queues[name].Add(task);
		}

		private IEnumerator ProcessQueue(string name)
		{
			List<IEnumerator> tasks = queues[name];
			do
			{
				yield return null;
				if (tasks.Count > 0)
				{
					IEnumerator task = tasks.Pop(0);
					while (task.MoveNext())
					{
						yield return task.Current;
					}
				}
			}
			while (tasks.Count != 0);
			queues.Remove(name);
		}
	}
}
