using System.Collections.Generic;

namespace UniExtensions
{
	public static class CollectionExtensionMethods
	{
		public static T Pop<T>(this IList<T> list, int index)
		{
			while (index > list.Count)
			{
				index -= list.Count;
			}
			while (index < 0)
			{
				index += list.Count;
			}
			T result = list[index];
			list.RemoveAt(index);
			return result;
		}
	}
}
